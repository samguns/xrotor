#ifndef CONFIGBLHELILINKERWIDGET_H
#define CONFIGBLHELILINKERWIDGET_H

#include "ui_blhelilinker.h"
#include "../uavobjectwidgetutils/configtaskwidget.h"
#include "extensionsystem/pluginmanager.h"
#include "uavobjectmanager.h"
#include "uavobject.h"
#include <QWidget>
#include <QList>

class ConfigBLHeliLinkerWidget : public ConfigTaskWidget {
    Q_OBJECT

public:
    ConfigBLHeliLinkerWidget(QWidget *parent = 0);
    ~ConfigBLHeliLinkerWidget();

private:
    quint8 m_firmwareBin[8 * 1024];
    int tryToConnect;
    Ui_BLHeliLinkerWidget *m_ui;

    QString setOpenFileName();
    QString getFirmwarePath();
    void readHexFile(char *);

protected slots:
    // void refreshWidgetsValues(UAVObject *obj = NULL);
    // void updateObjectsFromWidgets();

private slots:
    void loadBLFirmware();
    void flashBLFirmware();
    void connectBLEsc();
    void readBLEsc();
    void writeBLEsc();
    void changeServoPin(int);
    // void mainPortChanged(int index);
    // void openHelp();
};

#endif /* CONFIGBLHELILINKERWIDGET_H */
