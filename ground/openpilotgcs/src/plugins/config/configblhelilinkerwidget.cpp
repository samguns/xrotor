#include "configblhelilinkerwidget.h"

#include <QDebug>
#include <extensionsystem/pluginmanager.h>
#include <coreplugin/generalsettings.h>
#include "blhelicommand.h"
#include "softwareuartsettings.h"
#include <QDesktopServices>
#include <QUrl>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QErrorMessage>

/* General definition of the Intel HEX8 specification */
enum _IHexDefinitions {
    /* 768 should be plenty of space to read in a Intel HEX8 record */
    IHEX_RECORD_BUFF_SIZE = 768,
    /* Offsets and lengths of various fields in an Intel HEX8 record */
    IHEX_COUNT_OFFSET     = 1,
    IHEX_COUNT_LEN = 2,
    IHEX_ADDRESS_OFFSET   = 3,
    IHEX_ADDRESS_LEN  = 4,
    IHEX_TYPE_OFFSET  = 7,
    IHEX_TYPE_LEN     = 2,
    IHEX_DATA_OFFSET  = 9,
    IHEX_CHECKSUM_LEN = 2,
    IHEX_MAX_DATA_LEN = 512,
    /* Ascii hex encoded length of a single byte */
    IHEX_ASCII_HEX_BYTE_LEN = 2,
    /* Start code offset and value */
    IHEX_START_CODE_OFFSET  = 0,
    IHEX_START_CODE = ':',
};

ConfigBLHeliLinkerWidget::ConfigBLHeliLinkerWidget(QWidget *parent)
    : ConfigTaskWidget(parent), tryToConnect(0)
{
    m_ui = new Ui_BLHeliLinkerWidget();
    m_ui->setupUi(this);

    // ExtensionSystem::PluginManager *pm = ExtensionSystem::PluginManager::instance();
    // Core::Internal::GeneralSettings *settings = pm->getObject<Core::Internal::GeneralSettings>();

    addWidgetBinding("BLHeliCommand", "PWM_Frequency", m_ui->cbPwmFrequency);
    addWidgetBinding("BLHeliCommand", "Comm_Timing", m_ui->cbCommTiming);

    connect(m_ui->btOpen, SIGNAL(clicked()), this, SLOT(loadBLFirmware()));
    connect(m_ui->btFlash, SIGNAL(clicked()), this, SLOT(flashBLFirmware()));
    connect(m_ui->btProgramAtmelEEPROM, SIGNAL(clicked()), this, SLOT(flashAtmelEEPROM()));
    connect(m_ui->btConnect, SIGNAL(clicked()), this, SLOT(connectBLEsc()));
    connect(m_ui->btRead, SIGNAL(clicked()), this, SLOT(readBLEsc()));
    connect(m_ui->btWrite, SIGNAL(clicked()), this, SLOT(writeBLEsc()));

    connect(m_ui->cbServoPins, SIGNAL(currentIndexChanged(int)), this, SLOT(changeServoPin(int)));

    m_ui->pbFlash->reset();
}

ConfigBLHeliLinkerWidget::~ConfigBLHeliLinkerWidget()
{
    // Do nothing
}

void ConfigBLHeliLinkerWidget::loadBLFirmware()
{
    QString strFile = setOpenFileName();

    QFile file(strFile);

    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    }

    memset(m_firmwareBin, 0xff, 8192);

    char buf[768];
    while (file.readLine(buf, sizeof(buf)) != -1) {
        readHexFile(buf);
    }

    m_ui->pbFlash->reset();
}

void ConfigBLHeliLinkerWidget::flashBLFirmware()
{
    quint16 i = 0;

    BLHeliCommand *BLHeliCommandObj = BLHeliCommand::GetInstance(getObjectManager());
    BLHeliCommand::DataFields blCommandData;

    blCommandData = BLHeliCommandObj->getData();
    if (blCommandData.MCUType == BLHeliCommand::MCUTYPE_SILAB) {
        for (i = 0; i < 14; i++) {
            do {
                QApplication::processEvents();

                blCommandData = BLHeliCommandObj->getData();
            } while (blCommandData.Operations != BLHeliCommand::OPERATIONS_NONE);

            blCommandData.MemAddress = i * 512;
            blCommandData.Operations = BLHeliCommand::OPERATIONS_ERASE;
            BLHeliCommandObj->setData(blCommandData);
        }

        for (i = 0; i < 54; i++) {
            do {
                QApplication::processEvents();

                blCommandData = BLHeliCommandObj->getData();
            } while (blCommandData.Operations != BLHeliCommand::OPERATIONS_NONE);

            blCommandData.MemAddress    = i * 128;
            blCommandData.MemBufferSize = 128;
            memcpy(blCommandData.MemBuffer, &(m_firmwareBin[i * 128]), 128);
            blCommandData.Operations    = BLHeliCommand::OPERATIONS_FLASH;
            BLHeliCommandObj->setData(blCommandData);

            m_ui->pbFlash->setValue(i);
        }
    } else {
        for (i = 0; i < 57; i++) {
            do {
                QApplication::processEvents();

                blCommandData = BLHeliCommandObj->getData();
            } while (blCommandData.Operations != BLHeliCommand::OPERATIONS_NONE);

            blCommandData.MemAddress    = i * 128;
            blCommandData.MemBufferSize = 128;
            memcpy(blCommandData.MemBuffer, &(m_firmwareBin[i * 128]), 128);
            blCommandData.Operations    = BLHeliCommand::OPERATIONS_FLASH;
            BLHeliCommandObj->setData(blCommandData);

            m_ui->pbFlash->setValue(i);
        }
    }

    do {
        QApplication::processEvents();

        blCommandData = BLHeliCommandObj->getData();
    } while (blCommandData.Operations != BLHeliCommand::OPERATIONS_NONE);

    blCommandData.Operations = BLHeliCommand::OPERATIONS_STOP;
    BLHeliCommandObj->setData(blCommandData);
}

void ConfigBLHeliLinkerWidget::connectBLEsc()
{
    BLHeliCommand *BLHeliCommandObj = BLHeliCommand::GetInstance(getObjectManager());
    BLHeliCommand::DataFields blCommandData = BLHeliCommandObj->getData();

    blCommandData.Operations = BLHeliCommand::OPERATIONS_CONNECT;
    BLHeliCommandObj->setData(blCommandData);

    do {
        QApplication::processEvents();

        blCommandData = BLHeliCommandObj->getData();
    } while (blCommandData.Operations != BLHeliCommand::OPERATIONS_NONE);

    if (blCommandData.Connection == BLHeliCommand::CONNECTION_CONNECTED) {
        m_ui->btConnect->setEnabled(false);
        m_ui->pbFlash->reset();
        return;
    } else {
        tryToConnect++;
        if (tryToConnect > 4) {
            return;
        }

        connectBLEsc();
    }
}

void ConfigBLHeliLinkerWidget::readBLEsc()
{
    BLHeliCommand *BLHeliCommandObj = BLHeliCommand::GetInstance(getObjectManager());
    BLHeliCommand::DataFields blCommandData = BLHeliCommandObj->getData();

    blCommandData.Operations = BLHeliCommand::OPERATIONS_READ;
    BLHeliCommandObj->setData(blCommandData);

    do {
        QApplication::processEvents();

        blCommandData = BLHeliCommandObj->getData();
    } while (blCommandData.Operations != BLHeliCommand::OPERATIONS_NONE);

    if (blCommandData.Connection == BLHeliCommand::CONNECTION_CONNECTED) {
        m_ui->btConnect->setEnabled(false);
    } else {
        m_ui->btConnect->setEnabled(true);
    }

    char escTag[16];
    int pos = 0;
    memset(escTag, 0, 16);
    for (int i = 0; i < 16; i++) {
        if ((blCommandData.ESCTag[i] != '#') && (blCommandData.ESCTag[i] != ' ')) {
            escTag[pos++] = blCommandData.ESCTag[i];
        }
    }
    m_ui->escTag->setText(QString(escTag));
    m_ui->escVersion->setText(QString("%1.%2").arg(blCommandData.Main_Rev).arg(blCommandData.Sub_Rev));

    if ((blCommandData.Initialized_Low_Byte == 0xa5) &&
        (blCommandData.Initialized_High_Byte == 0x5a)) {
        m_ui->escType->setText(QString("Main"));
    } else if ((blCommandData.Initialized_Low_Byte == 0x5a) &&
               (blCommandData.Initialized_High_Byte == 0xa5)) {
        m_ui->escType->setText(QString("Tail"));
    } else if ((blCommandData.Initialized_Low_Byte == 0x55) &&
               (blCommandData.Initialized_High_Byte == 0xaa)) {
        m_ui->escType->setText(QString("Multi"));
    } else {
        m_ui->escType->setText(QString("Unknown"));
    }
}

void ConfigBLHeliLinkerWidget::writeBLEsc()
{
    BLHeliCommand *BLHeliCommandObj = BLHeliCommand::GetInstance(getObjectManager());
    BLHeliCommand::DataFields blCommandData = BLHeliCommandObj->getData();

    blCommandData.PWM_Frequency = m_ui->cbPwmFrequency->currentIndex();
    blCommandData.Comm_Timing   = m_ui->cbCommTiming->currentIndex();
    blCommandData.Operations    = BLHeliCommand::OPERATIONS_WRITE;
    BLHeliCommandObj->setData(blCommandData);
}

void ConfigBLHeliLinkerWidget::changeServoPin(int index)
{
    SoftwareUARTSettings *SoftwareUARTSettingsObj = SoftwareUARTSettings::GetInstance(getObjectManager());
    SoftwareUARTSettings::DataFields SoftwareUARTSettingsData = SoftwareUARTSettingsObj->getData();

    switch (index) {
    case 0:
        SoftwareUARTSettingsData.TxPin = SoftwareUARTSettings::TXPIN_SERVOPIN1;
        SoftwareUARTSettingsData.RxPin = SoftwareUARTSettings::RXPIN_SERVOPIN1;
        break;

    case 1:
        SoftwareUARTSettingsData.TxPin = SoftwareUARTSettings::TXPIN_SERVOPIN2;
        SoftwareUARTSettingsData.RxPin = SoftwareUARTSettings::RXPIN_SERVOPIN2;
        break;

    case 2:
        SoftwareUARTSettingsData.TxPin = SoftwareUARTSettings::TXPIN_SERVOPIN3;
        SoftwareUARTSettingsData.RxPin = SoftwareUARTSettings::RXPIN_SERVOPIN3;
        break;

    case 3:
        SoftwareUARTSettingsData.TxPin = SoftwareUARTSettings::TXPIN_SERVOPIN4;
        SoftwareUARTSettingsData.RxPin = SoftwareUARTSettings::RXPIN_SERVOPIN4;
        break;

    case 4:
        SoftwareUARTSettingsData.TxPin = SoftwareUARTSettings::TXPIN_SERVOPIN5;
        SoftwareUARTSettingsData.RxPin = SoftwareUARTSettings::RXPIN_SERVOPIN5;
        break;

    case 5:
        SoftwareUARTSettingsData.TxPin = SoftwareUARTSettings::TXPIN_SERVOPIN6;
        SoftwareUARTSettingsData.RxPin = SoftwareUARTSettings::RXPIN_SERVOPIN6;
        break;

    default:
        break;
    }

    SoftwareUARTSettingsObj->setData(SoftwareUARTSettingsData);
    tryToConnect = 0;
    m_ui->btConnect->setEnabled(true);
}

void ConfigBLHeliLinkerWidget::readHexFile(char *buf)
{
    char hexBuff[IHEX_ADDRESS_LEN + 1];

    if (strlen(buf) < (1 + IHEX_COUNT_LEN + IHEX_ADDRESS_LEN + IHEX_TYPE_LEN)) {
        return;
    }

    if (buf[IHEX_START_CODE_OFFSET] != IHEX_START_CODE) {
        return;
    }

    strncpy(hexBuff, buf + IHEX_COUNT_OFFSET, IHEX_COUNT_LEN);
    hexBuff[IHEX_COUNT_LEN] = 0;
    int dataCount = strtol(hexBuff, (char * *)NULL, 16);

    strncpy(hexBuff, buf + IHEX_ADDRESS_OFFSET, IHEX_ADDRESS_LEN);
    hexBuff[IHEX_ADDRESS_LEN] = 0;
    unsigned short address = (unsigned short)strtol(hexBuff, (char * *)NULL, 16);

    quint8 data;

    for (int i = 0; i < dataCount; i++) {
        strncpy(hexBuff, buf + IHEX_DATA_OFFSET + 2 * i, IHEX_ASCII_HEX_BYTE_LEN);
        hexBuff[IHEX_ASCII_HEX_BYTE_LEN] = 0;
        data = (unsigned char)strtol(hexBuff, (char * *)NULL, 16);
        m_firmwareBin[address + i] = data;
    }
}

QString ConfigBLHeliLinkerWidget::setOpenFileName()
{
    QString fwDirectoryStr = getFirmwarePath();

    // Format filename for file chooser
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Select BLHeli firmware file"),
                                                    fwDirectoryStr,
                                                    tr("Firmware Files (*.hex)"));

    return fileName;
}

QString ConfigBLHeliLinkerWidget::getFirmwarePath()
{
    QDir fwDirectory;
    QString fwDirectoryStr;

    fwDirectoryStr = QCoreApplication::applicationDirPath();
    fwDirectory    = QDir(fwDirectoryStr);
#ifdef Q_OS_WIN
    fwDirectory.cd("../..");
    fwDirectoryStr = fwDirectory.absolutePath();
#elif defined Q_OS_LINUX
    fwDirectory.cd("../..");
    fwDirectoryStr = fwDirectory.absolutePath();
#elif defined Q_OS_MAC
    fwDirectory.cd("../../../../../..");
    fwDirectoryStr = fwDirectory.absolutePath();
#endif
    return fwDirectoryStr;
}
