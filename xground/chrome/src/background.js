// Using outerbounds will save the last setting used
// by the user plus allow us to not be force to a
// min size

chrome.app.runtime.onLaunched.addListener(function() {
  chrome.app.window.create('index.html', {
    id: 'main',
    outerBounds: {
      width: 1366,
      height: 768
    }
  });
});


