angular.module('gettext').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('zh_CN', {"Change language":"更改界面显示语言","Dashboard":"控制台","Erase":"重置","I'm Dashboard!":"我是控制台！","Language change dialog":"界面显示语言改变对话框","Language changed!":"改变显示语言！","Load":"载入","OK":"确定","Please reload to display ChromeGCS in changed language.":"ChromeGCS显示语言将在程序重启后生效。","Request":"请求","Save":"保存","Select a serial port":"选择串口通讯端口","Send":"上传","UAVO Browser":"UAVO浏览器"});
/* jshint +W100 */
}]);