(function() {
  'use strict';

  angular
    .module('chromeGcs')
    .run(defaultView)
    .run(setLocale)
    .run(runBlock);

  /** @ngInject */
  function defaultView($state, appMainRouterConstants) {
      $state.go(appMainRouterConstants.DashboardState);
  }

  function setLocale(gettextCatalog) {
      gettextCatalog.currentLanguage = global_locale;
  }

  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
