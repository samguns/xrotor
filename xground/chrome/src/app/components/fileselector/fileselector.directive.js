(function() {
    'use strict';

    angular
        .module('components')
        .directive('fileSelector', fileSelector);

    /* @ngInject */
    function fileSelector () {
        var directive = {
            bindToController: true,
            link: link,
            restrict: 'A'
        };
        return directive;

        function link($scope, $element, attrs) {
            $element.bind("change", function(e) {
                $scope.file = (e.srcElement || e.target).files[0];
                $scope.vm.getFile($scope.file);
            });
        }
    }
})();
