(function() {
    'use strict';

    angular
        .module('components')
        .service('systemHealthService', systemHealthService);

    /** @ngInject */
    function systemHealthService(GCSTelemetryStatsObjService,
                                 SystemAlarmsObjService,
                                 SerialPortService) {
        var self = this;

        GCSTelemetryStatsObjService.onObjectUpdated = onGCSTelemetryStatsUpdated;
        SystemAlarmsObjService.onObjectUpdated = onSystemAlarmsUpdated;

        var serialConnectionEventListener = new ConnectionEventListener();
        serialConnectionEventListener.portDisconnected = portDisconnected;
        SerialPortService.removeConnectionEventListener('SystemHealthWidget');
        SerialPortService.addConnectionEventListener('SystemHealthWidget',
            serialConnectionEventListener);

        function onGCSTelemetryStatsUpdated () {
            var telemetryStatus = GCSTelemetryStatsObjService.obj.getField("Status").getValue();
            if (telemetryStatus == "Connected") {
                if (self.onUpdateGCSTelemetryConnectedStatus) {
                    self.onUpdateGCSTelemetryConnectedStatus(true);
                }
            } else {
                if (self.onUpdateGCSTelemetryConnectedStatus) {
                    self.onUpdateGCSTelemetryConnectedStatus(false);
                }
            }
        }

        function onSystemAlarmsUpdated () {
            if (self.onUpdateSystemAlarmsObject) {
                self.onUpdateSystemAlarmsObject(SystemAlarmsObjService.obj.getFields());
            }
        }

        function portDisconnected () {
            if (self.onUpdateGCSTelemetryConnectedStatus) {
                self.onUpdateGCSTelemetryConnectedStatus(false);
            }
        }
    }

})();
