(function() {
    'use strict';

    angular
        .module('components')
        .directive('systemHealth', systemHealth);

    /** @ngInject */
    function systemHealth(systemHealthService) {
        var directive = {
            restrict: 'A',
            templateUrl: 'assets/images/systemhealth/system-health.svg',
            link: systemHealthFunc
        };

        return directive;

        function systemHealthFunc (scope, element) {
            var noLink = element[0].querySelector("[inkscape\\:label='No Link']");

            systemHealthService.onUpdateGCSTelemetryConnectedStatus =
                onGCSTelemetryConnectedStatusUpdated;
            systemHealthService.onUpdateSystemAlarmsObject =
                onSystemAlarmsUpdated;

            function onGCSTelemetryConnectedStatusUpdated (isConnected) {
                if (isConnected == true) {
                    showHealthElement(noLink, false);
                } else {
                    showHealthElement(noLink, true);
                }
            }

            function onSystemAlarmsUpdated (systemAlarmsFields) {
                if (onSystemAlarmsUpdated == false) {
                    return;
                }

                angular.forEach(systemAlarmsFields, function (field, key) {
                    var elementNames = field.getElementNames();

                    angular.forEach(elementNames, function (elementName, key) {
                        var value = field.getValue(elementName);
                        var svgElementNameOK = elementName + '-OK';
                        var svgElementNameWarning = elementName + '-Warning';
                        var svgElementNameError = elementName + '-Error';
                        var svgElementNameCritical = elementName + '-Critical';
                        var svgElementOK =
                            element[0].querySelector("[inkscape\\:label='" + svgElementNameOK + "']");
                        var svgElementWarning =
                            element[0].querySelector("[inkscape\\:label='" + svgElementNameWarning + "']");
                        var svgElementError =
                            element[0].querySelector("[inkscape\\:label='" + svgElementNameError + "']");
                        var svgElementCritical =
                            element[0].querySelector("[inkscape\\:label='" + svgElementNameCritical + "']");

                        if ((value != "Uninitialised") &&
                            (svgElementOK != null) &&
                            (svgElementWarning != null) &&
                            (svgElementError != null) &&
                            (svgElementCritical != null)) {
                            switch (value) {
                                case "OK":
                                    showHealthElement(svgElementWarning, false);
                                    showHealthElement(svgElementError, false);
                                    showHealthElement(svgElementCritical, false);

                                    showHealthElement(svgElementOK, true);
                                    break;

                                case "Warning":
                                    showHealthElement(svgElementOK, false);
                                    showHealthElement(svgElementError, false);
                                    showHealthElement(svgElementCritical, false);

                                    showHealthElement(svgElementWarning, true);
                                    break;

                                case "Error":
                                    showHealthElement(svgElementWarning, false);
                                    showHealthElement(svgElementOK, false);
                                    showHealthElement(svgElementCritical, false);

                                    showHealthElement(svgElementError, true);
                                    break;

                                case "Critical":
                                    showHealthElement(svgElementOK, false);
                                    showHealthElement(svgElementError, false);
                                    showHealthElement(svgElementWarning, false);

                                    showHealthElement(svgElementCritical, true);
                                    break;

                                default:
                                    break;
                            }
                        }
                    });
                });
            }

            function showHealthElement (element, isShow) {
                if (isShow) {
                    element.setAttribute("style", "display:inline");
                } else {
                    element.setAttribute("style", "display:none");
                }
            }
        }
    }

})();
