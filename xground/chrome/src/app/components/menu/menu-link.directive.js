(function() {
    'use strict';

    angular
        .module('components')
        .directive('menuLink', menuLink);

    /** @ngInject */
    function menuLink() {
        var directive = {
            scope: {
                section: '='
            },
            restrict: 'AE',
            templateUrl: 'app/components/menu/menu-link.template.html',
            link: menuLinkFunc
        };

        return directive;

        function menuLinkFunc (scope, element) {
            var controller = element.parent().controller();

            scope.focusSection = function () {
                controller.autoFocusContent = true;
                if (controller.linkSelected) {
                    controller.linkSelected(scope.section);
                }
            };
        }
    }

})();
