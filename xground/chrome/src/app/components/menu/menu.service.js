(function() {
    'use strict';

    angular
        .module('components')
        .factory('menuService', menuService);

    /** @ngInject */
    function menuService($location) {
        var menuService = function() {
            this.sections = [];
        };

        menuService.prototype.selectSection = function(section) {
            this.openedSection = section;
        };

        menuService.prototype.toggleSelectSection = function (section) {
            this.openedSection = (this.openedSection === section ? null : section);
        };

        menuService.prototype.isSectionSelected = function (section) {
            return this.openedSection === section;
        };

        menuService.prototype.selectPage = function (section, page) {
            page && page.url && $location.path(page.url);
            this.currentSection = section;
            this.currentPage = page;
        };

        menuService.prototype.isPageSelected = function (page) {
            return this.currentPage === page;
        };

        menuService.prototype.addMenuItem = function (itemObj) {
            if ((itemObj == undefined) ||
                (itemObj == '')) {
                return;
            }

            this.sections.push(itemObj);
        };

        return {
            getInstance: function () {
                return new menuService();
            }
        }
    }
})();
