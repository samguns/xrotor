(function() {
    'use strict';

    angular
        .module('components')
        .service('telemetryWidgetService', telemetryWidgetService);

    function telemetryWidgetService(TelemetryMonitorService,
                                    GCSTelemetryStatsObjService,
                                    SerialPortService) {
        var connected = false;
        var self = this;

        var serialConnectionEventListener = new ConnectionEventListener();
        serialConnectionEventListener.portDisconnected = portDisconnected;
        SerialPortService.removeConnectionEventListener('TelemetryWidget');
        SerialPortService.addConnectionEventListener('TelemetryWidget',
            serialConnectionEventListener);

        updateTelemetryWidget(0, 0, connected);

        onGCSTelemetryStatsUpdated(GCSTelemetryStatsObjService.obj);
        var eventListener = new UAVObjectEventListener();
        eventListener.objectUpdated = onGCSTelemetryStatsUpdated;
        GCSTelemetryStatsObjService.obj.addUAVObjectEventListener(eventListener);

        function onGCSTelemetryStatsUpdated(obj) {
            var telemetryStatus = obj.getField("Status").getValue();
            if (telemetryStatus == "Connected") {
                var txRate = obj.getField("TxDataRate").getValue();
                var rxRate = obj.getField("RxDataRate").getValue();

                connected = true;
                updateTelemetryWidget(txRate, rxRate, connected);
                return;
            }

            connected = false;
            updateTelemetryWidget(0, 0, connected);
        }

        function updateTelemetryWidget(txRate, rxRate, isVisible) {
            if (self.onUpdateTelemetryRates) {
                self.onUpdateTelemetryRates(txRate, rxRate, isVisible);
            }
        }

        function portDisconnected() {
            connected = false;
            updateTelemetryWidget(0, 0, connected);
        }
    }

})();
