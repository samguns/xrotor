(function() {
    'use strict';

    angular
        .module('components')
        .directive('telemetryWidget', telemetryWidget);

    /** @ngInject */
    function telemetryWidget (telemetryWidgetService) {
        var directive = {
            templateUrl: 'assets/images/telemetrywidget/tx-rx.svg',
            link: link,
            restrict: 'A'
        };
        return directive;

        function link($scope, $element) {
            var txNodes = $element[0].querySelectorAll('.txNode');
            var rxNodes = $element[0].querySelectorAll('.rxNode');
            var txSpeed = $element[0].querySelector('#txSpeed');
            var rxSpeed = $element[0].querySelector('#rxSpeed');
            var minValue = 0;
            var maxValue = 1200;

            telemetryWidgetService.onUpdateTelemetryRates = telemetryUpdated;

            function telemetryUpdated(txRate, rxRate, visible) {
                var txNodesNum = txNodes.length;
                var rxNodesNum = rxNodes.length;
                var range = maxValue - minValue;
                var txIndex = ((txRate - minValue) / range | 0) * txNodesNum;
                var rxIndex = ((rxRate - minValue) / range | 0) * rxNodesNum;

                angular.forEach(txNodes, function (txNode, key) {
                    var isNodeVisible = ((key <= txIndex) && (visible));

                    if (isVisible(txNode) != isNodeVisible) {
                        setVisible(txNode, isNodeVisible);
                    }
                });

                angular.forEach(rxNodes, function (rxNode, key) {
                    var isNodeVisible = ((key <= rxIndex) && (visible));

                    if (isVisible(rxNode) != isNodeVisible) {
                        setVisible(rxNode, isNodeVisible);
                    }
                });

                txSpeed.innerHTML = (txRate | 0) + ' Bytes/s';
                rxSpeed.innerHTML = (rxRate | 0) + ' Bytes/s';

                setVisible(txSpeed, visible);
                setVisible(rxSpeed, visible);
            }

            function isVisible(node) {
                var visibility = node.getAttribute("visibility");
                return visibility == "visible";
            }

            function setVisible(node, visible) {
                if (visible) {
                    node.setAttribute("visibility", "visible");
                    return;
                }

                node.setAttribute("visibility", "hidden");
            }
        }
    }
})();
