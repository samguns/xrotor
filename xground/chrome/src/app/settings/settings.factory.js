(function() {
    'use strict';

    angular
        .module('settings')
        .factory('globalSettings', globalSettings);

    /** @ngInject */
    function globalSettings() {
        var appLogo = "assets/images/angular-logo.svg";
        var appName = "NextGen Rotors";

        var factory = {
            getAppLogo: getAppLogo,
            getAppName: getAppName
        };

        return factory;

        function getAppLogo() {
            return appLogo;
        }

        function getAppName() {
            return appName;
        }
    }
})();
