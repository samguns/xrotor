(function() {
  'use strict';

  angular
    .module('chromeGcs')
    .config(config);

  /** @ngInject */
  function config($logProvider, $compileProvider) {
    // Enable log
    $logProvider.debugEnabled(true);

    $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|local|data|blob:chrome-extension|filesystem:chrome-extension|file|chrome-extension):/);
  }

})();
