"use strict";

var UAVObject;
UAVObject = function(obj) {
    Object.assign(this, obj);
    this.instID = 0;
    this.eventListeners = [];

    this.initializeFields();
};

UAVObject.prototype.initializeFields = function () {
    var numBytes = 0;
    for (var idx in this.fields) {
        var curUAVObjField = new UAVObjectField(this.fields[idx]);
        numBytes += curUAVObjField.getNumBytes();
    }

    this.numBytes = numBytes;
    this.data = new ArrayBuffer(numBytes);

    var offset = 0;
    for (idx in this.fields) {
        curUAVObjField = new UAVObjectField(this.fields[idx]);
        curUAVObjField.initialize(this.data, offset, this);
        this.fields[idx] = curUAVObjField;
        offset += curUAVObjField.getNumBytes();
    }
};

UAVObject.prototype.initialize = function(instID) {
    this.instID = instID;
};

UAVObject.prototype.clone = function (instID) {
    var copy = new UAVObject(this);
    for (var attr in this) {
        if (this.hasOwnProperty(attr)) {
            copy[attr] = this[attr];
        }
    }

    copy.instID = instID;
    return copy;
};

UAVObject.prototype.getMetaData = function () {
    if (this.metaObject != null) {
        return this.metaObject.getMetaData();
    }

    if (this.defaultMetaData != undefined) {
        return this.defaultMetaData;
    }
};

UAVObject.prototype.getObjID = function() {
    return this.objID;
};

UAVObject.prototype.getInstID = function() {
    return this.instID;
};

UAVObject.prototype.isSingleInstance = function() {
    return (this.isSingleInst == types.TRUE);
};

UAVObject.prototype.isSettingsObject = function () {
    return (this.isSettings == types.TRUE);
};

UAVObject.prototype.GetFlightTelemetryUpdateMode = function (metaData) {
    return ((metaData.flags >> UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT) & UAVOBJ_UPDATE_MODE_MASK);
};

UAVObject.prototype.GetGcsTelemetryUpdateMode = function (metaData) {
    return ((metaData.flags >> UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT) & UAVOBJ_UPDATE_MODE_MASK);
};

UAVObject.prototype.GetGcsTelemetryAcked = function (metaData) {
    return ((metaData.flags >> UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT) & 1);
};

UAVObject.prototype.getName = function() {
    return this.name;
};

UAVObject.prototype.getDescription = function() {
    return this.description;
};

UAVObject.prototype.getCategory = function() {
    return this.category;
};

UAVObject.prototype.getNumFields = function() {
    return this.fields.length;
};

UAVObject.prototype.getNumBytes = function () {
    return this.numBytes;
};

UAVObject.prototype.getFields = function() {
    return this.fields;
};

UAVObject.prototype.getGcsAccess = function (metaData) {
    return (((metaData.flags >> UAVOBJ_GCS_ACCESS_SHIFT) & 1) == 0);
};

UAVObject.prototype.isMetaObject = function () {
    return false;
};

UAVObject.prototype.getField = function(name) {
    for (var i = 0; i < this.fields.length; i++) {
        var field = this.fields[i];
        if (field.name == name) {
            return field;
        }
    }

    return null;
};

UAVObject.prototype.pack = function (dataOut) {
    var numBytes = 0;

    for (var n = 0; n < this.fields.length; n++) {
        var fieldObj = this.fields[n];
        numBytes += fieldObj.pack(dataOut);
    }

    return numBytes;
};

UAVObject.prototype.unpack = function(data) {
    if ((data.length <= 0) || (undefined == data)) {
        return 0;
    }

    var numBytes = 0;
    for (var n = 0; n < this.fields.length; n++) {
        var fieldObj = this.fields[n];
        numBytes += fieldObj.unpack(data);
    }

    // Trigger all the listeners for the unpack event
    this.fireUnpacked();
    this.fireUpdated(false);

    return numBytes;
};

UAVObject.prototype.addUAVObjectEventListener = function (listener) {
    var index = this.eventListeners.indexOf(listener);
    if (index < 0) {
        this.eventListeners.push(listener);
    }
};

UAVObject.prototype.removeUAVObjectEventListener = function (listener) {
    var index = this.eventListeners.indexOf(listener);
    if (index > -1) {
        this.eventListeners.splice(index, 1);
    }
};

UAVObject.prototype.fireUpdated = function (manually) {
    for (var i = 0; i < this.eventListeners.length; i++) {
        var curListener = this.eventListeners[i];
        if (manually == true) {
            curListener.objectUpdatedManual(this, false);
        }

        curListener.objectUpdated(this);
    }
};

UAVObject.prototype.fireUnpacked = function () {
    for (var i = 0; i < this.eventListeners.length; i++) {
        var curListener = this.eventListeners[i];

        curListener.objectUnpacked(this);
    }
};

UAVObject.prototype.fireUpdateRequested = function () {
    for (var i = 0; i < this.eventListeners.length; i++) {
        var curListener = this.eventListeners[i];

        curListener.updateRequested(this, false);
    }
};

UAVObject.prototype.fireUpdateRequestedAll = function () {
    for (var i = 0; i < this.eventListeners.length; i++) {
        var curListener = this.eventListeners[i];

        curListener.updateRequested(this, true);
    }
};

UAVObject.prototype.fireTransactionCompleted = function (status) {
    for (var i = 0; i < this.eventListeners.length; i++) {
        var curListener = this.eventListeners[i];

        curListener.transactionCompleted(this, status);
    }
};

UAVObject.prototype.requestUpdate = function () {
    // console.log("requestUpdate");
    this.fireUpdateRequested();
};

UAVObject.prototype.updated = function () {
    this.fireUpdated(true);
};
