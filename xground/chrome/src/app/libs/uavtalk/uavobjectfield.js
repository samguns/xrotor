"use strict";

var UAVObjectField;
UAVObjectField = function(fieldObj) {
    Object.assign(this, fieldObj);
    var self = this;
    self.constructorInitialize();
};

UAVObjectField.prototype.initialize = function (data, offset, obj) {
    this.obj = obj;
    this.offset = offset;
    this.data = data;

    var dv = new DataView(this.data);
    for (var i = 0; i < (this.numBytesPerElement * this.numElements); i++) {
        dv.setUint8(offset + i, 0);
    }
};

UAVObjectField.prototype.constructorInitialize = function() {
    switch (this.type) {
        case "int8":
        case "uint8":
        case "enum":
            this.numBytesPerElement = 1;
            break;
        case "int16":
        case "uint16":
            this.numBytesPerElement = 2;
            break;
        case "int32":
        case "uint32":
        case "float":
            this.numBytesPerElement = 4;
            break;
        default:
            this.numBytesPerElement = 0;
    }
};

UAVObjectField.prototype.getName = function() {
    return this.name;
};

UAVObjectField.prototype.getType = function() {
    return this.type;
};

UAVObjectField.prototype.getUnits = function () {
    return this.units;
};

UAVObjectField.prototype.getNumElements = function() {
    return this.numElements;
};

UAVObjectField.prototype.getElementNames = function () {
    return this.elements;
};

UAVObjectField.prototype.getOptions = function() {
    return this.options;
};

UAVObjectField.prototype.pack = function(data) {
    var dv_out = new DataView(data);
    var dv = new DataView(this.data);
    var index;
    var position;
    switch (this.type) {
        case "uint8":
        case "enum":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv_out.setUint8(position, dv.getUint8(position));
            }
            break;

        case "int8":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv_out.setInt8(position, dv.getInt8(position));
            }
            break;

        case "uint16":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv_out.setUint16(position, dv.getUint16(position, true), true);
            }
            break;

        case "int16":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv_out.setInt16(position, dv.getInt16(position, true), true);
            }
            break;

        case "uint32":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv_out.setUint32(position, dv.getUint32(position, true), true);
            }
            break;

        case "int32":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv_out.setInt32(position, dv.getInt32(position, true), true);
            }
            break;

        case "float":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv_out.setUint32(position, dv.getUint32(position, true), true);
            }
            break;

        default:
            break;
    }

    return this.getNumBytes();
};

UAVObjectField.prototype.unpack = function (data) {
    var dv_in = new DataView(data);
    var dv = new DataView(this.data);
    var index;
    var position;
    switch (this.type) {
        case "uint8":
        case "enum":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv.setUint8(position, dv_in.getUint8(position));
            }
            break;

        case "int8":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv.setInt8(position, dv_in.getInt8(position));
            }
            break;

        case "uint16":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv.setUint16(position, dv_in.getUint16(position, true), true);
            }
            break;

        case "int16":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv.setInt16(position, dv_in.getInt16(position, true), true);
            }
            break;

        case "uint32":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv.setUint32(position, dv_in.getUint32(position, true), true);
            }
            break;

        case "int32":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv.setInt32(position, dv_in.getInt32(position, true), true);
            }
            break;

        case "float":
            for (index = 0; index < this.numElements; index++) {
                position = this.offset + (index * this.numBytesPerElement);
                dv.setUint32(position, dv_in.getUint32(position, true), true);
            }
            break;

        default:
            break;
    }

    return this.getNumBytes();
};

UAVObjectField.prototype.getNumBytes = function() {
    return this.numBytesPerElement * this.numElements;
};

UAVObjectField.prototype.getValue = function (elementName) {

	if(typeof(elementName) == "number") {
		var index = elementName;
		if (index >= this.numElements) {
			return false;
		}
	} else if(elementName && this.numElements > 1){
		var index = this.elements.indexOf(elementName);
	} else {
		var index = 0;
	}

    var data;
    var dv = new DataView(this.data);
    var location = index * this.numBytesPerElement;

    switch (this.type) {
        case "enum":
            var val = dv.getUint8(this.offset + location);
            data = this.options[val];
            break;

        case "uint8":
            data = dv.getUint8(this.offset + location);
            break;

        case "int8":
            data = dv.getInt8(this.offset + location);
            break;

        case "uint16":
            data = dv.getUint16(this.offset + location, true);
            break;

        case "int16":
            data = dv.getInt16(this.offset + location, true);
            break;

        case "uint32":
            data = dv.getUint32(this.offset + location, true);
            break;

        case "int32":
            data = dv.getInt32(this.offset + location, true);
            break;

        case "float":
            data = dv.getFloat32(this.offset + location, true).toFixed(6);
            break;

        default:
            data = null;
    }

    return data;
};

UAVObjectField.prototype.setValue = function (value, elementName) {

	if(typeof(elementName) == "number") {
		var index = elementName;
		if (index >= this.numElements) {
			return false;
		}
	} else if(elementName && this.numElements > 1){
		var index = this.elements.indexOf(elementName);
	} else {
		var index = 0;
	}

    if ((this.obj.getGcsAccess(this.obj.getMetaData()) == false) &&
        (this.obj.isMetaObject() == false)) {
        return false;
    }

    var dv = new DataView(this.data);
    var location = index * this.numBytesPerElement;

    switch (this.type) {
        case "enum":
            if (typeof(value) == "number") {
                dv.setUint8(this.offset + location, value);
            } else if (typeof(value) == "string") {
                var val = this.options.indexOf(value);
                dv.setUint8(this.offset + location, val);
            }
            break;

        case "uint8":
            dv.setUint8(this.offset + location, value);
            break;

        case "int8":
            dv.setInt8(this.offset + location, value);
            break;

        case "uint16":
            dv.setUint16(this.offset + location, value, true);
            break;

        case "int16":
            dv.setInt16(this.offset + location, value, true);
            break;

        case "uint32":
            dv.setUint32(this.offset + location, value, true);
            break;

        case "int32":
            dv.setInt32(this.offset + location, value, true);
            break;

        case "float":
            var tmp_buf = new ArrayBuffer(4);
            var tmp_dv = new DataView(tmp_buf);
            var tmp=Number(value);
            tmp_dv.setFloat32(0, tmp.toFixed(6), true);
            dv.setUint32(this.offset + location, tmp_dv.getUint32(0, true), true);
            break;

        default:
            return false;
    }

    return true;
};
