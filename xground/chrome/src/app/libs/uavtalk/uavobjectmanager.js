"use strict";

var MAX_INSTANCES = 10;

var UAVObjectManagerEventListener;
UAVObjectManagerEventListener = function () {};

UAVObjectManagerEventListener.prototype.newInstance = function (obj) {};
UAVObjectManagerEventListener.prototype.newObject = function (obj) {};

var UAVObjectManager;
UAVObjectManager = function() {
    this.UAVObjects = [[]];
    this.eventListeners = [];
};

UAVObjectManager.prototype.addUAVObjectManagerEventListener = function (listener) {
    this.eventListeners.push(listener);
};

UAVObjectManager.prototype.fireNewObjectEvent = function (obj) {
    for (var i = 0; i < this.eventListeners.length; i++) {
        var curListener = Object.create(UAVObjectEventListener, this.eventListeners[i]);
        curListener.newObject(obj);
    }
};

UAVObjectManager.prototype.fireNewInstanceEvent = function (obj) {
    for (var i = 0; i < this.eventListeners.length; i++) {
        var curListener = Object.create(UAVObjectEventListener, this.eventListeners[i]);
        curListener.newInstance(obj);
    }
};

UAVObjectManager.prototype.Initialize = function() {
};

UAVObjectManager.prototype.registerJSON = function(json) {
    var xhr = new XMLHttpRequest();
    var json_file = "uavobjects/" + json;
    xhr.open("get", json_file, true);
    xhr.addEventListener('load', this.reqListener);
    xhr.send();
};

UAVObjectManager.prototype.reqListener = function () {
    var demo = this.response.toString();
    var obj = new UAVObject(JSON.parse(demo));
    SystemAlarms.setDefaultFieldValues(obj);
    SystemAlarms.createObj(obj);
    UAVObjectMgmt.UAVObjects[0].push(obj);
    var fieldObj = new UAVObjectField(obj.getField("Alarm"));
    fieldObj.getName();

    // UAVObjectMgmt.getObjectByID(obj.getObjID());
    // UAVObjectMgmt.getObjectByIdAndInstId(obj.getObjID(), 1);

};

UAVObjectManager.prototype.registerObject = function (instObj) {
    var instID = instObj.getInstID();

    var obj = this.getObjectByID(instObj.objID);
    if (null != obj) {
        if (instObj.isSingleInstance() == true) {
            return false;
        }
    }

    if (undefined == this.UAVObjects[instID]) {
        this.UAVObjects[instID] = [];
        this.fireNewInstanceEvent(instObj);
    }

    this.UAVObjects[instID].push(instObj);

    if (instID == 0) {
        var mname = instObj.name + types.META_PREFIX;
        instObj.metaObject = new UAVMetaObject(instObj.getObjID() + 1, mname, instObj);
        instObj.metaObject.setDefaultFieldValues();
        this.UAVObjects[instID].push(instObj.metaObject);
    }

    this.UAVObjects[instID].sort(function (a, b) {
        if (a.name.toLowerCase() < b.name.toLowerCase()) {
            return -1;
        } else if (a.name.toLowerCase() > b.name.toLowerCase()) {
            return 1;
        }

        return 0;
    });

    this.fireNewObjectEvent(instObj);

    return true;
};

UAVObjectManager.prototype.getObjectInstances = function (objId) {
    var objInstances = [];

    for (var i = 0; i < this.UAVObjects.length; i++) {
        if (this.UAVObjects[i].length > 0) {
            for (var j = 0; j < this.UAVObjects[i].length; j++) {
                var obj = this.UAVObjects[i][j];
                if (obj.objID == objId) {
                    objInstances.push(obj);
                }
            }
        }
    }

    return objInstances;
};

UAVObjectManager.prototype.getObjectByID = function (objId) {
    for (var i = 0; i < this.UAVObjects[0].length; i++) {
        var object = this.UAVObjects[0][i];
        if (object.objID == objId) {
            return object;
        }
    }

    return null;
};

UAVObjectManager.prototype.getObjectByName = function (name) {
    for (var i = 0; i < this.UAVObjects[0].length; i++) {
        var object = this.UAVObjects[0][i];
        if (object.name == name) {
            return object;
        }
    }

    return null;
};

UAVObjectManager.prototype.getObjectByIdAndInstId = function (objId, instId) {
    if (undefined == this.UAVObjects[instId]) {
        return null;
    }

    var object_instances = this.UAVObjects[instId];
    for (var i = 0; i < object_instances.length; i++) {
        var object = object_instances[i];
        if (object.objID == objId) {
            return object;
        }
    }

    return null;
};

UAVObjectManager.prototype.getObjectByNameAndInstId = function (name, instId) {
    if (undefined == this.UAVObjects[instId]) {
        return null;
    }

    var object_instances = this.UAVObjects[instId];
    for (var i = 0; i < object_instances.length; i++) {
        var object = object_instances[i];
        if (object.name == name) {
            return object;
        }
    }

    return null;
};

UAVObjectManager.prototype.getObjects = function () {
    return this.UAVObjects;
};

var UAVObjectMgmt = new UAVObjectManager();
