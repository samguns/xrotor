'use strict';

var UAVObjectEventListener;
UAVObjectEventListener = function () {};

UAVObjectEventListener.prototype.objectUpdated = function (obj) {};
UAVObjectEventListener.prototype.objectUpdatedAuto = function (obj) {};
UAVObjectEventListener.prototype.objectUpdatedManual = function (obj, all) {};
UAVObjectEventListener.prototype.objectUpdatedPeriodic = function (obj) {};
UAVObjectEventListener.prototype.objectUnpacked = function (obj) {};
UAVObjectEventListener.prototype.updateRequested = function (obj, all) {};
UAVObjectEventListener.prototype.transactionCompleted = function (obj, success) {};
UAVObjectEventListener.prototype.newInstance = function (obj) {};
