"use strict";

/**
 * Object metadata, each object has a meta object that holds its metadata. The metadata define
 * properties for each object and can be used by multiple modules (e.g. telemetry and logger)
 *
 * The object metadata flags are packed into a single 16 bit integer.
 * The bits in the flag field are defined as:
 *
 *   Bit(s)  Name                       Meaning
 *   ------  ----                       -------
 *      0    access                     Defines the access level for the local flight transactions (readonly=1 and readwrite=0)
 *      1    gcsAccess                  Defines the access level for the local GCS transactions (readonly=1 and readwrite=0)
 *      2    telemetryAcked             Defines if an ack is required for the transactions of this object (1:acked, 0:not acked)
 *      3    gcsTelemetryAcked          Defines if an ack is required for the transactions of this object (1:acked, 0:not acked)
 *    4-5    telemetryUpdateMode        Update mode used by the telemetry module (UAVObjUpdateMode)
 *    6-7    gcsTelemetryUpdateMode     Update mode used by the GCS (UAVObjUpdateMode)
 *    8-9    loggingUpdateMode          Update mode used by the logging module (UAVObjUpdateMode)
 */

var UAVOBJ_ACCESS_SHIFT                    = 0;
var UAVOBJ_GCS_ACCESS_SHIFT                = 1;
var UAVOBJ_TELEMETRY_ACKED_SHIFT           = 2;
var UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT       = 3;
var UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT     = 4;
var UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT = 6;
var UAVOBJ_LOGGING_UPDATE_MODE_SHIFT       = 8;
var UAVOBJ_UPDATE_MODE_MASK                = 0x03;

var UPDATEMODE_MANUAL       = 0;
var UPDATEMODE_PERIODIC     = 1;
var UPDATEMODE_ONCHANGE     = 2;
var UPDATEMODE_THROTTLED    = 3;

var ACCESS_READWRITE = 0;
var ACCESS_READONLY  = 1;

var UAVMetaObject;
UAVMetaObject = function (objID, name, parent) {
    this.name = name;
    this.objID = objID;
    this.instID = 0;
    this.parent = parent;
    this.fields = [
        {
            "name": "flags",
            "type": "uint16",
            "numElements": "1"
        },
        {
            "name": "flightTelemetryUpdatePeriod",
            "type": "uint16",
            "numElements": "1"
        },
        {
            "name": "gcsTelemetryUpdatePeriod",
            "type": "uint16",
            "numElements": "1"
        },
        {
            "name": "loggingUpdatePeriod",
            "type": "uint16",
            "numElements": "1"
        }
    ];

    this.eventListeners = [];
    this.initializeFields();
};

UAVMetaObject.prototype = Object.create(UAVObject.prototype);

UAVMetaObject.prototype.setDefaultFieldValues = function () {
    this.getField("flags").setValue(this.parent.defaultMetaData.flags);
    this.getField("flightTelemetryUpdatePeriod").setValue(this.parent.defaultMetaData.flightTelemetryUpdatePeriod);
    this.getField("gcsTelemetryUpdatePeriod").setValue(this.parent.defaultMetaData.gcsTelemetryUpdatePeriod);
    this.getField("loggingUpdatePeriod").setValue(this.parent.defaultMetaData.loggingUpdatePeriod);
};

UAVMetaObject.prototype.getMetaData = function () {
    var metadata = {};

    metadata.flags = this.getField("flags").getValue();
    metadata.flightTelemetryUpdatePeriod = this.getField("flightTelemetryUpdatePeriod").getValue();
    metadata.gcsTelemetryUpdatePeriod = this.getField("gcsTelemetryUpdatePeriod").getValue();
    metadata.loggingUpdatePeriod = this.getField("loggingUpdatePeriod").getValue();

    return metadata;
};

UAVMetaObject.prototype.isMetaObject = function () {
    return true;
};

UAVMetaObject.prototype.getParentObject = function () {
    return this.parent;
};
