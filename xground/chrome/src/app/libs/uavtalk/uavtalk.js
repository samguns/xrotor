'use strict';

var UAVTALK_CANARI = 0xCA;
var UAVTALK_SYNC_VAL = 0x3C;

var UAVTALK_TYPE_MASK = 0x78;
var UAVTALK_TYPE_VER = 0x20;
var UAVTALK_TIMESTAMPED = 0x80;
var UAVTALK_TYPE_OBJ = (UAVTALK_TYPE_VER | 0x00);
var UAVTALK_TYPE_OBJ_REQ = (UAVTALK_TYPE_VER | 0x01);
var UAVTALK_TYPE_OBJ_ACK = (UAVTALK_TYPE_VER | 0x02);
var UAVTALK_TYPE_ACK = (UAVTALK_TYPE_VER | 0x03);
var UAVTALK_TYPE_NACK = (UAVTALK_TYPE_VER | 0x04);
var UAVTALK_TYPE_OBJ_TS = (UAVTALK_TIMESTAMPED | UAVTALK_TYPE_OBJ);
var UAVTALK_TYPE_OBJ_ACK_TS = (UAVTALK_TIMESTAMPED | UAVTALK_TYPE_OBJ_ACK);

var UAVTALK_STATE_ERROR = 0;
var UAVTALK_STATE_SYNC = 1;
var UAVTALK_STATE_TYPE = 2;
var UAVTALK_STATE_SIZE = 3;
var UAVTALK_STATE_OBJID = 4;
var UAVTALK_STATE_INSTID = 5;
var UAVTALK_STATE_TIMESTAMP = 6;
var UAVTALK_STATE_DATA = 7;
var UAVTALK_STATE_CS = 8;
var UAVTALK_STATE_COMPLETE = 9;

var UAVTALK_HEADER_LENGTH = 10;
var UAVTALK_MIN_HEADER_LENGTH = 10;
var UAVTALK_MAX_HEADER_LENGTH = 12;
var UAVTALK_CHECKSUM_LENGTH = 1;
var UAVTALK_MAX_PAYLOAD_LENGTH = 254;
var UAVTALK_MIN_PACKET_LENGTH = UAVTALK_MAX_HEADER_LENGTH + UAVTALK_CHECKSUM_LENGTH;
var UAVTALK_MAX_PACKET_LENGTH = UAVTALK_MIN_PACKET_LENGTH + UAVTALK_MAX_PAYLOAD_LENGTH;

var UAVOBJ_ALL_INSTANCES = 0xFFFF;
var UAVOBJ_MAX_INSTANCES = 1000;

var Transaction;
Transaction = function () {
    this.respType = 0;
    this.respObjId = 0;
    this.respInstId = 0;
};

var transactionListener;
transactionListener = function () {};

transactionListener.prototype.TransactionSucceeded = function (obj) {};
transactionListener.prototype.TransactionFailed = function (obj) {};

var UAVTalkStats;
UAVTalkStats = function() {
    this.resetStats();
};

UAVTalkStats.prototype.resetStats = function () {
    this.txBytes = 0;
    this.rxBytes = 0;
    this.txObjectBytes = 0;
    this.rxObjectBytes = 0;
    this.txObjects = 0;
    this.rxObjects = 0;
    this.txErrors = 0;
    this.rxErrors = 0;
    this.rxSyncErrors = 0;
    this.rxCrcErrors = 0;
};

var UAVTalkInputProcessor;
UAVTalkInputProcessor = function() {
    this.type = 0;
    this.rxPacketLength = 0;
    this.processedBytes = 0;
    this.state = UAVTALK_STATE_SYNC;
    this.cs = 0;
    this.packet_size = 0;
    this.objId = 0;
    this.instId = 0;
};

var UAVTalk;
UAVTalk = function() {
    this.canari = UAVTALK_CANARI;
    this.iproc = new UAVTalkInputProcessor();
    this.stats = new UAVTalkStats();
    this.rxCRC = 0;
    this.rxCount = 0;
    this.rxLength = 0;
    this.rxTmpBuffer = new ArrayBuffer(4);
    this.rxBuffer = new ArrayBuffer(UAVTALK_MAX_PAYLOAD_LENGTH);
    this.transactionListeners = [];

    var firstMap = new Map();
    var trans = new Transaction();
    var instId = 0;
    var objId = 0;
    firstMap.set(instId, trans);
    this.transMap = new Map();
    this.transMap.set(objId, firstMap);
};

UAVTalk.prototype.init = function (outputStream) {
    this.outputStream = outputStream;
};

UAVTalk.prototype.getStats = function () {
    return this.stats;
};

UAVTalk.prototype.UAVTalkProcessInputStream = function(rxbuffer, length) {
    var state = UAVTALK_STATE_ERROR;
    var self = this;

    for (var i = 0; i < length; i++) {
        var rxByte = rxbuffer[i];
        state = self.UAVTalkProcessInputStreamQuiet(rxByte);

        if (state == UAVTALK_STATE_COMPLETE) {
            self.UAVTalkReceiveObject(this.iproc.type,
                this.iproc.objId,
                this.iproc.instId);
        }
    }
};

UAVTalk.prototype.UAVTalkProcessInputStreamQuiet = function(rxByte) {
    var self = this;

    if (this.iproc.state == UAVTALK_STATE_ERROR || this.iproc.state == UAVTALK_STATE_COMPLETE) {
        this.iproc.state = UAVTALK_STATE_SYNC;
    }

    this.stats.rxBytes++;
    this.rxPacketLength++;

    switch (this.iproc.state) {
        case UAVTALK_STATE_SYNC:
            self.UAVTalkProcess_SYNC(rxByte);
            break;

        case UAVTALK_STATE_TYPE:
            self.UAVTalkProcess_TYPE(rxByte);
            break;

        case UAVTALK_STATE_SIZE:
            self.UAVTalkProcess_SIZE(rxByte);
            break;

        case UAVTALK_STATE_OBJID:
            self.UAVTalkProcess_OBJID(rxByte);
            break;

        case UAVTALK_STATE_INSTID:
            self.UAVTalkProcess_INSTID(rxByte);
            break;

        case UAVTALK_STATE_DATA:
            self.UAVTalkProcess_DATA(rxByte);
            break;

        case UAVTALK_STATE_CS:
            self.UAVTalkProcess_CS(rxByte);
            break;

        default:
            this.iproc.state = UAVTALK_STATE_ERROR;
            break;
    }

    return this.iproc.state;
};

UAVTalk.prototype.UAVTalkProcess_SYNC = function(rxByte) {
    if (rxByte != UAVTALK_SYNC_VAL) {
        this.stats.rxSyncErrors++;
        return;
    }

    this.iproc.cs = CRC.updateByte(0, rxByte);
    this.rxPacketLength = 1;
    this.rxCount = 0;
    this.iproc.state = UAVTALK_STATE_TYPE;
    //console.log("UAVTalkProcess_SYNC");
};

UAVTalk.prototype.UAVTalkProcess_TYPE = function(rxByte) {
    if ((rxByte & UAVTALK_TYPE_MASK) != UAVTALK_TYPE_VER) {
        this.stats.rxErrors++;
        this.iproc.state = UAVTALK_STATE_SYNC;
        return;
    }

    this.iproc.cs = CRC.updateByte(this.iproc.cs, rxByte);
    this.iproc.type = rxByte;
    this.iproc.packet_size = 0;
    this.iproc.state = UAVTALK_STATE_SIZE;
    //console.log("UAVTalkProcess_TYPE 0x" + rxByte.toString(16));
};

UAVTalk.prototype.UAVTalkProcess_SIZE = function (rxByte) {
    this.iproc.cs = CRC.updateByte(this.iproc.cs, rxByte);

    if (this.rxCount == 0) {
        this.iproc.packet_size += rxByte;
        this.rxCount++;
        return;
    }

    this.rxCount = 0;
    this.iproc.packet_size += (rxByte << 8) & 0xff00;

    if ((this.iproc.packet_size < UAVTALK_MIN_HEADER_LENGTH) ||
        (this.iproc.packet_size > UAVTALK_MAX_HEADER_LENGTH + UAVTALK_MAX_PAYLOAD_LENGTH)) {
        this.stats.rxErrors++;
        this.iproc.state = UAVTALK_STATE_ERROR;
        return;
    }

    this.iproc.state = UAVTALK_STATE_OBJID;

    //console.log('UAVTalkProcess_SIZE ' + this.iproc.packet_size);
};

UAVTalk.prototype.UAVTalkProcess_OBJID = function (rxByte) {
    this.iproc.cs = CRC.updateByte(this.iproc.cs, rxByte);
    var dv = new DataView(this.rxTmpBuffer);
    dv.setUint8(this.rxCount, rxByte);
    this.rxCount++;
    if (this.rxCount < 4)
        return;

    this.rxCount = 0;

    this.iproc.objId = dv.getUint32(0, true);
    this.iproc.state = UAVTALK_STATE_INSTID;

    //console.log("UAVTalkProcess_OBJID 0x" + this.iproc.objId.toString(16));
};

UAVTalk.prototype.UAVTalkProcess_INSTID = function (rxByte) {
    this.iproc.cs = CRC.updateByte(this.iproc.cs, rxByte);
    var dv = new DataView(this.rxTmpBuffer);
    dv.setUint8(this.rxCount, rxByte);
    this.rxCount++;
    if (this.rxCount < 2)
        return;

    this.rxCount = 0;
    this.iproc.instId = dv.getUint16(0, true);

    var rxObj = UAVObjectMgmt.getObjectByID(this.iproc.objId);
    if (rxObj == null) {
        //console.log("Unknown ID: 0x" + this.iproc.objId.toString(16));
        this.stats.rxErrors++;
        this.iproc.state = UAVTALK_STATE_ERROR;
        return;
    }

    if ((this.iproc.type == UAVTALK_TYPE_OBJ_REQ) ||
        (this.iproc.type == UAVTALK_TYPE_ACK) ||
        (this.iproc.type == UAVTALK_TYPE_NACK)) {
        this.rxLength = 0;
    } else {
        this.rxLength = this.iproc.packet_size - this.rxPacketLength;
    }

    if (this.rxLength >= UAVTALK_MAX_PAYLOAD_LENGTH) {
        console.log("Exceeded payload max length");
        this.stats.rxErrors++;
        this.iproc.state = UAVTALK_STATE_ERROR;
        return;
    }

    if ((this.rxLength + this.rxPacketLength) != this.iproc.packet_size) {
        console.log("Mismatched packet size");
        this.stats.rxErrors++;
        this.iproc.state = UAVTALK_STATE_ERROR;
        return;
    }

    if (this.rxLength > 0) {
        this.iproc.state = UAVTALK_STATE_DATA;
    } else {
        this.iproc.state = UAVTALK_STATE_CS;
    }

    //console.log("UAVTalkProcess_INSTID 0x" + this.iproc.instId.toString(16));
};

UAVTalk.prototype.UAVTalkProcess_DATA = function (rxByte) {
    this.iproc.cs = CRC.updateByte(this.iproc.cs, rxByte);
    var dv = new DataView(this.rxBuffer);
    dv.setUint8(this.rxCount, rxByte);
    this.rxCount++;
    if (this.rxCount < this.rxLength)
        return;

    this.rxCount = 0;
    this.iproc.state = UAVTALK_STATE_CS;

    //console.log("UAVTalkProcess_DATA");
};

UAVTalk.prototype.UAVTalkProcess_CS = function (rxByte) {
    if (rxByte != this.iproc.cs) {
        this.stats.rxCrcErrors++;
        this.stats.rxErrors++;
        this.iproc.state = UAVTALK_STATE_ERROR;
        console.log("Bad crc: 0x" + this.iproc.cs.toString(16) + " actual: 0x" + rxByte.toString(16));
        return;
    }

    if (this.rxPacketLength != (this.iproc.packet_size + UAVTALK_CHECKSUM_LENGTH)) {
        this.stats.rxErrors++;
        this.iproc.state = UAVTALK_STATE_ERROR;
        console.log("Bad size: 0x" + this.rxPacketLength.toString(16) +
                    "(" + this.iproc.packet_size.toString(16) + ")");
        return;
    }

    this.stats.rxObjects++;
    this.stats.rxObjectBytes += this.rxLength;
    this.iproc.state = UAVTALK_STATE_COMPLETE;
};

UAVTalk.prototype.UAVTalkReceiveObject = function (type, objId, instId) {
    var allInstances = (instId == UAVOBJ_ALL_INSTANCES);
    var obj;
    var error = false;

    switch (type) {
        case UAVTALK_TYPE_OBJ:
            if (allInstances == false) {
                obj = this.updateObject(objId, instId, this.rxBuffer);

                if (obj != null) {
                    // Check if this object acks a pending OBJ_REQ message
                    // any OBJ message can ack a pending OBJ_REQ message
                    // even one that was not sent in response to the OBJ_REQ message
                    this.updateAck(type, objId, instId, obj);
                } else {
                    error = true;
                }
            } else {
                error = true;
            }
            break;

        case UAVTALK_TYPE_OBJ_ACK:
            if (allInstances == false) {
                obj = this.updateObject(objId, instId, this.rxBuffer);
                if (null == obj) {
                    error = true;
                }

                if (this.transmitObject(UAVTALK_TYPE_ACK, objId, instId, obj) == false) {
                    error = true;
                }
            } else {
                error = true;
            }

            if (error) {
                // failed to update object, transmit NACK
                this.transmitObject(UAVTALK_TYPE_NACK, objId, instId, null);
            }
            break;

        case UAVTALK_TYPE_OBJ_REQ:
            // Get object, if all instances are requested get instance 0 of the object
            if (allInstances) {
                obj = UAVObjectMgmt.getObjectByID(objId);
            } else {
                obj = UAVObjectMgmt.getObjectByIdAndInstId(objId, instId);
            }

            if (null == obj) {
                error = true;
            }

            if (this.transmitObject(UAVTALK_TYPE_OBJ, objId, instId, obj) == false) {
                error = true;
            }

            if (error) {
                // failed to send object, transmit NACK
                this.transmitObject(UAVTALK_TYPE_NACK, objId, instId, null);
            }
            break;

        case UAVTALK_TYPE_ACK:
            if (allInstances == false) {
                obj = UAVObjectMgmt.getObjectByIdAndInstId(objId, instId);
                if (null != obj) {
                    this.updateAck(type, objId, instId, obj);
                } else {
                    error = true;
                }
            }
            break;

        case UAVTALK_TYPE_NACK:
            if (allInstances == false) {
                obj = UAVObjectMgmt.getObjectByIdAndInstId(objId, instId);
                if (null != obj) {
                    this.updateNack(objId, instId, obj);
                } else {
                    error = true;
                }
            }
            break;

        default:
            error = true;
    }

    return !error;
};

UAVTalk.prototype.updateObject = function (objId, instId, data) {
    var obj = UAVObjectMgmt.getObjectByIdAndInstId(objId, instId);
    if (obj == null) {
        var typeObj = UAVObjectMgmt.getObjectByID(objId);
        if (typeObj == null) {
            return null;
        }

        var instObj = typeObj.clone(instId);
        UAVObjectMgmt.registerObject(instObj);
        instObj.unpack(data);

        return instObj;
    }

    obj.unpack(data);

    return obj;
};

UAVTalk.prototype.updateAck = function (type, objId, instId, obj) {
    var trans = this.findTransaction(objId, instId);
    if ((undefined == trans) || (null == trans)) {
        return;
    }

    if (trans.respType != type) {
        return;
    }

    if (trans.respInstId == UAVOBJ_ALL_INSTANCES) {
        if (instId == 0) {
            // last instance received, complete transaction
            this.closeTransaction(trans);
            this.fireTransactionSucceeded(obj);
        }
    } else {
        this.closeTransaction(trans);
        this.fireTransactionSucceeded(obj);
    }
};

UAVTalk.prototype.updateNack = function (objId, instId, obj) {
    var trans = this.findTransaction(objId, instId);
    if ((undefined != trans) || (null != trans)) {
        this.closeTransaction(trans);
        this.fireTransactionFailed(obj);
    }
};

UAVTalk.prototype.transmitObject = function (type, objId, instId, obj) {
    var allInstances = (instId == UAVOBJ_ALL_INSTANCES);

    if ((obj != null) && (instId == UAVOBJ_ALL_INSTANCES) && obj.isSingleInstance()) {
        instId = 0;
    }

    // Process message type
    var ret = false;
    if ((type == UAVTALK_TYPE_OBJ) || (type == UAVTALK_TYPE_OBJ_ACK)) {
        if (allInstances) {
            // Send all instances in reverse order
            // This allows the receiver to detect when the last object has been received (i.e. when instance 0 is received)
            ret = true;
            //var numInst = UAVObjectMgmt.getNumInstances(objId);
        } else {
            ret = this.transmitSingleObject(type, objId, instId, obj);
        }
    } else if (type == UAVTALK_TYPE_OBJ_REQ) {
        ret = this.transmitSingleObject(UAVTALK_TYPE_OBJ_REQ, objId, instId, null);
    } else if ((type == UAVTALK_TYPE_ACK) || (type == UAVTALK_TYPE_NACK)) {
        ret = !allInstances && this.transmitSingleObject(type, objId, instId, null);
    } else {
        return false;
    }

    return ret;
};

UAVTalk.prototype.transmitSingleObject = function (type, objId, instId, obj) {
    var length = 0;

    var header_ab = new ArrayBuffer(UAVTALK_HEADER_LENGTH);
    var header = new Uint8Array(header_ab);
    var dv_header = new DataView(header_ab);

    if ((type == UAVTALK_TYPE_OBJ_REQ) || (type == UAVTALK_TYPE_ACK) ||
        (type == UAVTALK_TYPE_NACK)) {
        length = 0;
    } else {
        length = obj.getNumBytes();
    }

    if (length >= UAVTALK_MAX_PAYLOAD_LENGTH) {
        this.stats.txErrors++;
        return false;
    }

    // Set sync byte
    dv_header.setUint8(0, UAVTALK_SYNC_VAL);
    // Set type
    dv_header.setUint8(1, (type & 0xff));
    // Set length
    dv_header.setUint16(2, (UAVTALK_HEADER_LENGTH + length), true);
    // Set objectID
    dv_header.setUint32(4, objId, true);
    // Set instance ID
    dv_header.setUint16(8, instId, true);

    if (length > 0) {
        var payload_ab = new ArrayBuffer(length);
        var payload = new Uint8Array(payload_ab);
        obj.pack(payload_ab);
    }

    // Calculate checksum
    var crc = CRC.updateCRC(0, header, UAVTALK_HEADER_LENGTH);
    crc = CRC.updateCRC(crc, payload, length);

    var buffer_out = new ArrayBuffer(UAVTALK_HEADER_LENGTH + length + UAVTALK_CHECKSUM_LENGTH);
    var buffer = new Uint8Array(buffer_out);
    buffer.set(header);
    if (length > 0) {
        buffer.set(payload, UAVTALK_HEADER_LENGTH);
    }
    buffer[UAVTALK_HEADER_LENGTH + length] = crc;
    //buffer.set(crc, UAVTALK_HEADER_LENGTH + length);

    this.outputStream.send(buffer_out);
    this.stats.txObjects++;
    this.stats.txObjectBytes += length;
    this.stats.txBytes += UAVTALK_HEADER_LENGTH + length + UAVTALK_CHECKSUM_LENGTH;

    return true;
};

UAVTalk.prototype.openTransaction = function (type, objId, instId) {
    var trans = new Transaction();
    trans.respType = (type === UAVTALK_TYPE_OBJ_REQ) ?
        UAVTALK_TYPE_OBJ : UAVTALK_TYPE_ACK;
    trans.respObjId = objId;
    trans.respInstId = instId;

    // console.log("Open transaction for " + objId);

    var objTransactions = this.transMap.get(trans.respObjId);
    if (undefined == objTransactions) {
        objTransactions = new Map();
        this.transMap.set(trans.respObjId, objTransactions);
    }

    objTransactions.set(trans.respInstId, trans);
};

UAVTalk.prototype.findTransaction = function (objId, instId) {
    // Lookup the transaction in the transaction map
    var objTransactions = this.transMap.get(objId);
    if (undefined != objTransactions) {
        var trans = objTransactions.get(instId);
        if (undefined == trans) {
            // see if there is an ALL_INSTANCES transaction
            trans = objTransactions.get(UAVOBJ_ALL_INSTANCES);
        }

        return trans;
    }

    return null;
};

UAVTalk.prototype.closeTransaction = function (trans) {
    var objTransactions = this.transMap.get(trans.respObjId);
    if (undefined != objTransactions) {
        objTransactions.delete(trans.respInstId);
    }
};

UAVTalk.prototype.addTransactionListener = function (listener) {
    this.transactionListeners.push(listener);
};

UAVTalk.prototype.fireTransactionSucceeded = function (obj) {
    for (var i = 0; i < this.transactionListeners.length; i++) {
        var curListener = this.transactionListeners[i];

        curListener.TransactionSucceeded(obj);
    }
};

UAVTalk.prototype.fireTransactionFailed = function (obj) {
    for (var i = 0; i < this.transactionListeners.length; i++) {
        var curListener = this.transactionListeners[i];

        curListener.TransactionFailed(obj);
    }
};

UAVTalk.prototype.sendObjectRequest = function (obj, allInstances) {
    var instId = allInstances ? UAVOBJ_ALL_INSTANCES : obj.getInstID();

    return this.objectTransaction(UAVTALK_TYPE_OBJ_REQ, obj.getObjID(), instId, obj);
};

UAVTalk.prototype.sendObject = function (obj, acked, allInstances) {
    var retVal = false;

    if ((null != obj) || (undefined != obj)) {
        var instId = allInstances ? UAVOBJ_ALL_INSTANCES : obj.getInstID();

        if (acked) {
            retVal = this.objectTransaction(UAVTALK_TYPE_OBJ_ACK, obj.getObjID(), instId, obj);
        } else {
            retVal = this.objectTransaction(UAVTALK_TYPE_OBJ, obj.getObjID(), instId, obj);
        }
    }

    return retVal;
};

UAVTalk.prototype.objectTransaction = function (type, objectId, instanceId, obj) {
    if ((type == UAVTALK_TYPE_OBJ_ACK) || (type == UAVTALK_TYPE_OBJ_REQ)) {
        if (this.transmitObject(type, objectId, instanceId, obj)) {
            this.openTransaction(type, objectId, instanceId);
            return true;
        } else {
            return false;
        }
    } else {
        if (type == UAVTALK_TYPE_OBJ) {
            return this.transmitObject(type, objectId, instanceId, obj);
        }
    }

    return false;
};

UAVTalk.prototype.cancelPendingTransaction = function (obj) {
    var trans = this.findTransaction(obj.getObjID(), obj.getInstID());
    if (null != trans) {
        this.closeTransaction(trans);
    }
};

UAVTalk.prototype.resetStats = function () {
    this.stats.resetStats();
};
