if (typeof types == "undefined") {
    var types = {
        TRUE: "TRUE",
        FALSE: "FALSE",

        MENU_HEADING: "heading",
        MENU_TOGGLE: "toggle",
        MENU_LINK: "link",

        OBJ_CATALOG_SETTING: "SETTING",
        OBJ_CATALOG_DATA: "DATA",

        META_PREFIX: "Meta"
    };

    var object_operation = {
        LOAD: "Load",
        SAVE: "Save",
        DELETE: "Delete"
    }
}
