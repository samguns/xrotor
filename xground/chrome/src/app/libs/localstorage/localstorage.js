'use strict';

localSave = function (object, callback) {
    if ((object != undefined) && (object != '')) {
        chrome.storage.local.set(object, function () {
            if (callback) {
                callback();
            }
        });
    }
};

localLoad = function (key, callback) {
    chrome.storage.local.get(key, function (result) {
        if (callback) {
            callback(result);
        }
    });
};
