'use strict';

var global_locale = 'en';

angular.element(document).ready(function() {
    UAVObjectsInitialize.register(UAVObjectMgmt);
    localLoad("locale", function (result) {
        global_locale = result.locale;
        angular.bootstrap(document, ['chromeGcs']);
    });

});

