(function() {
    'use strict';

    angular
        .module('services')
        .service('SerialPortService', SerialPortService);

    function SerialPortService($interval) {
        this.ports = false;
        this.btnConnectLable = "Connect";
        this.selectedPort = '';
        this.isIAPMode = false;
        var eventListeners = [];
        var listenerNames = [];
        var self = this;

        $interval(function () {
            serial.getDevices(function (current_ports) {
                if (array_difference(self.ports, current_ports).length > 0 || !self.ports) {
                    var removed_ports = array_difference(self.ports, current_ports);

                    if (self.ports != false) {
                        if (removed_ports.length > 1) {
                            console.log('PortHandler - Removed1: ' + removed_ports);
                        } else {
                            console.log('PortHandler - Removed2: ' + removed_ports[0]);
                        }
                    }

                    if (!self.ports) {
                        self.ports = current_ports;
                    } else {
                        for (var i = 0; i < removed_ports.length; i++) {
                            self.ports.splice(self.ports.indexOf(removed_ports[i]), 1);
                        }
                    }
                }

                var new_ports = array_difference(current_ports, self.ports);
                if (new_ports.length) {
                    if (new_ports.length > 1) {
                        console.log('PortHandler - Found1: ' + new_ports);
                    } else {
                        console.log('PortHandler - Found2: ' + new_ports[0]);
                    }

                    self.ports = current_ports;
                }
            });
        }, 250);

        function array_difference(firstArray, secondArray) {
            var cloneArray = [];

            // create hardcopy
            for (var i = 0; i < firstArray.length; i++) {
                cloneArray.push(firstArray[i]);
            }

            for (i = 0; i < secondArray.length; i++) {
                if (cloneArray.indexOf(secondArray[i]) != -1) {
                    cloneArray.splice(cloneArray.indexOf(secondArray[i]), 1);
                }
            }

            return cloneArray;
        }

        this.connect = function () {
            if (this.selectedPort != undefined || this.selectedPort == '') {
                TelemetryTask.connect(this.selectedPort, 38400,
                    this.isIAPMode,
                    function () {
                        self.btnConnectLable = 'Disconnect';
                        firePortConnected();
                    });

            }
        };

        this.disconnect = function () {
            TelemetryTask.disconnect(function() {
                self.btnConnectLable = 'Connect';
                self.selectedPort = '';
                firePortDisconnected();
            });
        };

        this.addConnectionEventListener = function (name, listener) {
            var index = listenerNames.indexOf(name);
            if (index < 0) {
                listenerNames.push(name);
            } else {
                return;
            }

            index = eventListeners.indexOf(listener);
            if (index < 0) {
                eventListeners.push(listener);
            }
        };

        this.removeConnectionEventListener = function (name) {
            var index = listenerNames.indexOf(name);
            if (index < 0) {
                return;
            }

            listenerNames.splice(index, 1);
            eventListeners.splice(index, 1);
        };

        function firePortConnected() {
            for (var i = 0; i < eventListeners.length; i++) {
                var curListener = eventListeners[i];

                curListener.portConnected();
            }
        }

        function firePortDisconnected() {
            for (var i = 0; i < eventListeners.length; i++) {
                var curListener = eventListeners[i];

                curListener.portDisconnected();
            }
        }
    }
})();
