(function() {
    "use strict";

    angular
        .module("services")
        .service("UAVObjectUtilService", UAVObjectUtilService);

    function UAVObjectUtilService($timeout,
                                  ObjectPersistenceObjService) {
        var failureTimer;
        var UTIL_IDLE = 0;
        var UTIL_AWAITING_ACK = 1;
        var UTIL_AWAITING_COMPLETED = 2;
        var saveState = UTIL_IDLE;
        var saveQueue = new Queue();
        var uploadQueue = new Queue();
        var objects = [];
        var self = this;

        ObjectPersistenceObjService.onObjectUpdated = objectPersistenceUpdated;
        ObjectPersistenceObjService.onTransactionCompleted = objectPersistenceTransactionCompleted;

        this.apply = function () {
            processOperation(false);
        };

        this.save = function () {
            processOperation(true);
        };

        this.addObject = function (obj) {
            if (objects.indexOf(obj) < 0) {
                objects.push(obj);
            }
        };

        this.removeObject = function (obj) {
            if (objects.indexOf(obj) > -1) {
                objects.splice(objects.indexOf(obj), 1);
            }
        };

        this.updateObjectPersistence = function(operation, object) {
            if ((operation != object_operation.LOAD) &&
                (operation != object_operation.SAVE) &&
                (operation != object_operation.DELETE)) {
                return;
            }

            if ((object == null) || (object == undefined)) {
                return;
            }

            ObjectPersistenceObjService.obj.getField("Operation").setValue(operation);
            ObjectPersistenceObjService.obj.getField("Selection").setValue("SingleObject");
            ObjectPersistenceObjService.obj.getField("ObjectID").setValue(object.getObjID());
            ObjectPersistenceObjService.obj.getField("InstanceID").setValue(object.getInstID());
            ObjectPersistenceObjService.setData();
        };

        var eventListener = new UAVObjectEventListener();
        eventListener.transactionCompleted = transaction_finished;
        var uploadMap = new Map();
        var saveMap = new Map();

        function processOperation(save) {
            for (var i = 0; i < objects.length; i++) {
                var obj = objects[i];
                if (obj.getGcsAccess(obj.getMetaData()) == false) {
                    continue;
                }

                obj.addUAVObjectEventListener(eventListener);

                var uploadTimeoutTimer = setTimeout(uploadTimeout, 3000, obj);
                uploadMap.set(obj, uploadTimeoutTimer);
                // console.log("process " + obj.getName() + " " + uploadTimeoutTimer);
                uploadObject(obj, function (obj) {
                    if (save && obj.isSettingsObject()) {
                        var saveTimeoutTimer = setTimeout(saveTimeout, 3000, obj);
                        saveMap.set(obj, saveTimeoutTimer);
                        // console.log("save " + obj.getName() + " " + saveTimeoutTimer);

                        saveObjectToSD(obj);
                    }
                });

            }

        }

        function transaction_finished(obj, success) {
            var timer = uploadMap.get(obj);
            // console.log("transaction_finished " + obj.getName() + " " + success + " " + timer);
            if (success) {
                if (timer != undefined) {
                    clearTimeout(timer);
                    uploadMap.delete(obj);
                }
                var entity = uploadQueue.dequeue();
                if (entity.saveCallback) {
                    entity.saveCallback(obj);
                }
            }

            uploadNextObject();
        }

        function uploadTimeout(obj) {
            console.log("Upload " + obj.getName() + " timed out");
        }

        function uploadObject(obj, saveCallback) {
            var uploadEntity = {obj: obj, saveCallback: saveCallback};
            uploadQueue.enqueue(uploadEntity);

            if (uploadQueue.getLength() == 1) {
                uploadNextObject();
            }
        }

        function uploadNextObject() {
            if (uploadQueue.isEmpty()) {
                return;
            }

            var entity = uploadQueue.peek();
            entity.obj.updated();
        }

        function saveTimeout(obj) {
            console.log("Saving " + obj.getName() + " timed out");
        }

        /*
         Add a new object to save in the queue
         */
        function saveObjectToSD(obj) {
            // Add to queue
            saveQueue.enqueue(obj);

            // If queue length is one, then start sending (call sendNextObject)
            // Otherwise, do nothing, it's sending anyway
            if (saveQueue.getLength() == 1) {
                saveNextObject();
            }
        }

        function saveNextObject() {
            if (saveQueue.isEmpty()) {
                return;
            }

            var obj = saveQueue.peek();
            saveState = UTIL_AWAITING_ACK;

            if (obj != undefined) {
                // console.log("Saving " + obj.getName() + " " + obj.getObjID().toString(16));
                ObjectPersistenceObjService.obj.getField("Operation").setValue("Save");
                ObjectPersistenceObjService.obj.getField("Selection").setValue("SingleObject");
                ObjectPersistenceObjService.obj.getField("ObjectID").setValue(obj.getObjID());
                ObjectPersistenceObjService.obj.getField("InstanceID").setValue(obj.getInstID());
                ObjectPersistenceObjService.setData();
            }
        }

        function objectPersistenceTransactionCompleted(success) {
            if (success) {
                if (saveState != UTIL_AWAITING_ACK) {
                    // console.log("saveState != AWAITING_ACK");
                    return;
                }

                saveState = UTIL_AWAITING_COMPLETED;

                failureTimer = $timeout(objectPersistenceOperationFailed, 2000);
            }
        }

        function objectPersistenceUpdated() {
            /*
             console.log("objectPersistenceUpdated " + saveState);
             console.log(ObjectPersistenceObjService.obj.getField("ObjectID").getValue().toString(16));
             console.log(ObjectPersistenceObjService.obj.getField("Operation").getValue());
             console.log(ObjectPersistenceObjService.obj.getField("Selection").getValue());
             console.log(ObjectPersistenceObjService.obj.getField("InstanceID").getValue());
             */
            if ((saveState == UTIL_AWAITING_COMPLETED) &&
                (ObjectPersistenceObjService.obj.getField("Operation").getValue(0) == "Error")) {
                $timeout.cancel(failureTimer);
                objectPersistenceOperationFailed();
            } else if ((saveState == UTIL_AWAITING_COMPLETED) &&
                (ObjectPersistenceObjService.obj.getField("Operation").getValue(0) == "Completed")) {
                $timeout.cancel(failureTimer);

                var savingObj = saveQueue.peek();
                // console.log(savingObj.getName() + " " + ObjectPersistenceObjService.obj.getField("ObjectID").getValue().toString(16));
                if (savingObj.getObjID() != ObjectPersistenceObjService.obj.getField("ObjectID").getValue(0)) {
                    objectPersistenceOperationFailed();
                    return;
                }

                saveQueue.dequeue();
                saveState = UTIL_IDLE;
                fireSaveCompleted(savingObj, true);

                saveNextObject();
            }
        }

        function objectPersistenceOperationFailed() {
            if (saveState == UTIL_AWAITING_COMPLETED) {
                var obj = saveQueue.dequeue();
                if (obj == undefined) {
                    return;
                }

                saveState = UTIL_IDLE;
                fireSaveCompleted(obj, false);

                saveNextObject();
            }
        }

        function fireSaveCompleted(obj, success) {
            var timer = saveMap.get(obj);
            // console.log("fireSaveCompleted " + obj.getName() + " " + success + " " + timer);
            if ((success) && (timer != undefined)) {
                clearTimeout(timer);
                saveMap.delete(obj);
                if (self.saveSuccessfull) {
                    self.saveSuccessfull(obj, true);
                }
            } else {
                if (self.saveSuccessfull) {
                    self.saveSuccessfull(obj, false);
                }
            }
        }
    }
})();
