(function() {
    "use strict";

    angular
        .module("services")
        .service("TelemetryMonitorService", TelemetryMonitorService);

    function TelemetryMonitorService($timeout,
                                     SerialPortService,
                                     GCSTelemetryStatsObjService,
                                     FlightTelemetryStatsObjService) {
        var STATS_UPDATE_PERIOD_MS = 4000;
        var STATS_CONNECT_PERIOD_MS = 2000;
        var CONNECTION_TIMEOUT_MS = 8000;
        var timerInterval = STATS_CONNECT_PERIOD_MS;
        var connectionTimeout = false;
        var curTick = 0;
        var statsTimer;

        var serialConnectionEventListener = new ConnectionEventListener();
        serialConnectionEventListener.portConnected = portConnected;
        serialConnectionEventListener.portDisconnected = portDisconnected;
        SerialPortService.addConnectionEventListener('TelemetryMonitor',
            serialConnectionEventListener);

        function flightStatsUpdated() {
            var flightStatus = FlightTelemetryStatsObjService
                .obj.getField("Status").getValue();
            var gcsStatus = GCSTelemetryStatsObjService
                .obj.getField("Status").getValue();

            if ((flightStatus != "Connected") ||
                (gcsStatus != "Connected")) {
                processStatsUpdates();
            }
        }

        function processStatsUpdates() {
            var telStats = TelemetryTask.getStats();

            TelemetryTask.resetStats();

            var TxDataRate = (telStats.txBytes * 1000) / timerInterval;
            GCSTelemetryStatsObjService.obj.getField("TxDataRate").setValue(TxDataRate);

            var TxBytes = GCSTelemetryStatsObjService.obj.getField("TxBytes").getValue() +
                telStats.txBytes;
            GCSTelemetryStatsObjService.obj.getField("TxBytes").setValue(TxBytes);

            var TxFailures = GCSTelemetryStatsObjService.obj.getField("TxFailures").getValue() +
                telStats.txErrors;
            GCSTelemetryStatsObjService.obj.getField("TxFailures").setValue(TxFailures);

            var TxRetries = GCSTelemetryStatsObjService.obj.getField("TxRetries").getValue() +
                telStats.txRetries;
            GCSTelemetryStatsObjService.obj.getField("TxRetries").setValue(TxRetries);

            var RxDataRate = (telStats.rxBytes * 1000) / timerInterval;
            GCSTelemetryStatsObjService.obj.getField("RxDataRate").setValue(RxDataRate);

            var RxBytes = GCSTelemetryStatsObjService.obj.getField("RxBytes").getValue() +
                telStats.rxBytes;
            GCSTelemetryStatsObjService.obj.getField("RxBytes").setValue(RxBytes);

            var RxFailures = GCSTelemetryStatsObjService.obj.getField("RxFailures").getValue() +
                telStats.rxErrors;
            GCSTelemetryStatsObjService.obj.getField("RxFailures").setValue(RxFailures);

            var RxSyncErrors = GCSTelemetryStatsObjService.obj.getField("RxSyncErrors").getValue() +
                telStats.rxSyncErrors;
            GCSTelemetryStatsObjService.obj.getField("RxSyncErrors").setValue(RxSyncErrors);

            var RxCrcErrors = GCSTelemetryStatsObjService.obj.getField("RxCrcErrors").getValue() +
                telStats.rxCrcErrors;
            GCSTelemetryStatsObjService.obj.getField("RxCrcErrors").setValue(RxCrcErrors);

            if (telStats.rxObjects > 0) {
                curTick = Date.now();
            }
            var elapsedTick = Date.now() - curTick;
            connectionTimeout = elapsedTick > CONNECTION_TIMEOUT_MS;

            var oldGCSStatus = GCSTelemetryStatsObjService.obj.getField("Status").getValue();
            var oldFlightStatus = FlightTelemetryStatsObjService.obj.getField("Status").getValue();
            var needUpdate = false;
            if (oldGCSStatus == "Disconnected") {
                // Request connection
                GCSTelemetryStatsObjService.obj.getField("Status").setValue("HandshakeReq");
                var newGCSStatus = "HandshakeReq";
                needUpdate = true;
            } else if (oldGCSStatus == "HandshakeReq") {
                // Check for connection acknowledge
                if (oldFlightStatus == "HandshakeAck") {
                    GCSTelemetryStatsObjService.obj.getField("Status").setValue("Connected");
                    newGCSStatus = "Connected";
                    needUpdate = true;
                }
            } else if (oldGCSStatus == "Connected") {
                // Check if the connection is still active and the the autopilot is still connected
                if ((oldFlightStatus == "Disconnected") || (connectionTimeout == true)) {
                    GCSTelemetryStatsObjService.obj.getField("Status").setValue("Disconnected");
                    newGCSStatus = "Disconnected";
                    needUpdate = true;
                }
            }

            GCSTelemetryStatsObjService.setData();

            if ((newGCSStatus == "Connected") && (needUpdate == true)) {
                timerInterval = STATS_UPDATE_PERIOD_MS;
                $timeout.cancel(statsTimer);
                statsTimer = $timeout(function () {
                    processStatsUpdates();
                }, STATS_UPDATE_PERIOD_MS);

                startRetrievingObjects();
            }

            if (oldGCSStatus == "Connected") {
                timerInterval = STATS_UPDATE_PERIOD_MS;
                $timeout.cancel(statsTimer);
                statsTimer = $timeout(function () {
                    processStatsUpdates();
                }, STATS_UPDATE_PERIOD_MS);
            }

            if ((newGCSStatus == "Disconnected") && (needUpdate == true)) {
                timerInterval = STATS_CONNECT_PERIOD_MS;
                $timeout.cancel(statsTimer);
                statsTimer = $timeout(function () {
                    processStatsUpdates();
                }, STATS_CONNECT_PERIOD_MS);
            }
        }

        var objPending;
        var queue = new Queue();
        var eventListener = new UAVObjectEventListener();
        eventListener.transactionCompleted = transactionCompleted;

        function startRetrievingObjects() {
            // Clear object queue
            while (queue.dequeue() != undefined) {
            }

            var objList = UAVObjectMgmt.getObjects();
            for (var i = 0; i < objList[0].length; i++) {
                var curObj = objList[0][i];

                if (curObj.isMetaObject()) {
                    queue.enqueue(curObj);
                } else if (curObj.isSettingsObject()) {
                    queue.enqueue(curObj);
                } else {
                    var metaData = curObj.getMetaData();
                    if (curObj.GetFlightTelemetryUpdateMode(metaData) == UPDATEMODE_ONCHANGE) {
                        queue.enqueue(curObj);
                    }
                }
            }

            retrieveNextObject();
        }

        function retrieveNextObject() {
            if (queue.isEmpty()) {
                return;
            }

            var obj = queue.dequeue();
            obj.addUAVObjectEventListener(eventListener);

            obj.requestUpdate();
            objPending = obj;
        }

        function transactionCompleted(obj, success) {
            if (obj == objPending) {
                obj.removeUAVObjectEventListener(eventListener);
                objPending = null;

                var gcsStatsStatus = GCSTelemetryStatsObjService.obj.getField("Status").getValue();
                if (gcsStatsStatus == "Connected") {
                    retrieveNextObject();
                } else {
                    stopRetrievingObjects();
                }
            }
        }

        function stopRetrievingObjects() {
            while (queue.dequeue() != undefined) {
            }
        }

        function portConnected () {
            if (SerialPortService.isIAPMode) {
                return;
            }

            FlightTelemetryStatsObjService.onObjectUpdated = flightStatsUpdated;

            statsTimer = $timeout(function () {
                processStatsUpdates();
            }, STATS_CONNECT_PERIOD_MS);
        }

        function portDisconnected () {
            $timeout.cancel(statsTimer);

            GCSTelemetryStatsObjService.obj.getField("Status").setValue("Disconnected");

            FlightTelemetryStatsObjService.onObjectUpdated = null;
        }
    }
})();
