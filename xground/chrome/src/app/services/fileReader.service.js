(function() {
    'use strict';

    angular
        .module('services')
        .service('fileReaderService', fileReaderService);

    function fileReaderService($q) {
        function onLoad (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        }

        function onError (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        }

        function getReader (deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);

            return reader;
        }

        this.readAsArrayBuffer = function (file, scope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, scope);

            reader.readAsArrayBuffer(file);
            return deferred.promise;
        };

        this.readAsText = function (file, scope) {
            var deferred = $q.defer();
            var reader = getReader(deferred, scope);

            reader.readAsText(file);
            return deferred.promise;
        };
    }
})();
