(function() {
    "use strict";

    angular
        .module("services")
        .service("UploaderService", UploaderService);

    function UploaderService () {
        this.command = {
            REQ_CAPABILITIES:       0xb00b0001,
            REP_CAPABILITIES:       0xb00b0002,
            ENTER_IAP:              0xb00b0003,
            JUMP_TO_FW:             0xb00b0004,
            RESET:                  0xb00b0005,
            ABORT_OPERATION:        0xb00b0006,
            UPLOAD_START:           0xb00b0107,
            UPLOAD:                 0xb00b0007,
            OP_END:                 0xb00b0008,
            DOWNLOAD_REQ:           0xb00b0009,
            DOWNLOAD:               0xb00b000a,
            STATUS_REQUEST:         0xb00b000b
        };

        this.status = {
            IAPidle:                0,
            uploading:              1,
            wrong_packet_received:  2,
            too_many_packets:       3,
            too_few_packets:        4,
            Last_operation_Success: 5,
            downloading:            6,
            idle:                   7,
            Last_operation_failed:  8,
            uploadingStarting:      9,
            outsideDevCapabilities: 10,
            CRC_Fail:               11,
            failed_jump:            12,
            abort:                  13
        };

        this.devID = 0;
        this.sizeOfCode = 0;
        this.blVersion = 0;

        var jobQueue = new Queue();
        var PKT_SIZE = 64;
        var COMMAND_OFFSET = 0;
        var COUNTER_OFFSET = 4;
        var DATA_OFFSET = 8;
        var OPT_OFFSET = 12;
        var self = this;

        function initReceiveListener () {
            serial.transmitting = false;
            serial.onReceive.addListener(serial_read);
        }

        function serial_read (readInfo) {
            if (jobQueue.isEmpty()) {
                return;
            }

            var handler = jobQueue.dequeue();
            handler(readInfo.data);
        }

        this.crc32Fast = function (crc, data) {
            var crcTable = [0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9, 0x130476DC, 0x17C56B6B, 0x1A864DB2, 0x1E475005,
                0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61, 0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD];

            crc = crc ^ data;

            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];
            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];
            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];
            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];
            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];
            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];
            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];
            crc = (crc << 4) ^ crcTable[(crc >> 28) & 0x0F];

            return crc;
        };

        this.enterIAP = function (devNumber) {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);

            dv_buf.setUint32(COMMAND_OFFSET, self.command.ENTER_IAP);
            dv_buf.setUint32(COUNTER_OFFSET, 0);
            dv_buf.setUint32(DATA_OFFSET, 1);
            dv_buf.setUint8(DATA_OFFSET, devNumber);

            serial.send(buf, function(writeInfo) {});
        };

        this.resetDevice = function () {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);

            dv_buf.setUint32(COMMAND_OFFSET, self.command.RESET);
            dv_buf.setUint32(COUNTER_OFFSET, 0);
            dv_buf.setUint32(DATA_OFFSET, 0);

            serial.send(buf, function(writeInfo) {});
        };

        this.endOperation = function () {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);

            dv_buf.setUint32(COMMAND_OFFSET, self.command.OP_END);
            dv_buf.setUint32(COUNTER_OFFSET, 0);
            dv_buf.setUint32(DATA_OFFSET, 0);

            serial.send(buf, function(writeInfo) {})
        };

        this.abortOperation = function () {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);

            dv_buf.setUint32(COMMAND_OFFSET, self.command.ABORT_OPERATION);
            dv_buf.setUint32(COUNTER_OFFSET, 0);
            dv_buf.setUint32(DATA_OFFSET, 0);

            serial.send(buf, function(writeInfo) {})
        };

        this.jumpToApp = function (safeboot, erase) {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);

            dv_buf.setUint32(COMMAND_OFFSET, self.command.JUMP_TO_FW);
            dv_buf.setUint32(COUNTER_OFFSET, 0);
            dv_buf.setUint32(DATA_OFFSET, 0);

            if (safeboot) {
                var safeboot_data = 0x00005afe;
                // force system to safe boot mode (hwsettings == defaults)
                dv_buf.setUint32(DATA_OFFSET, safeboot_data);
            }

            dv_buf.setUint32(OPT_OFFSET, 0);
            dv_buf.setUint32(OPT_OFFSET + 4, 0);
            dv_buf.setUint32(OPT_OFFSET + 8, 0);

            if (erase) {
                var first_opt = 0x0000fa5f;
                var second_opt = 0x00000001;
                dv_buf.setUint32(OPT_OFFSET, first_opt);
                dv_buf.setUint32(OPT_OFFSET + 4, second_opt);
            }

            serial.send(buf, function(writeInfo) {})
        };

        this.startUpload = function (numberOfBytes, type, crc) {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);
            var lastPacketCount;
            var numberOfPackets = (numberOfBytes / 4 / 14) | 0;
            var pad = ((numberOfBytes - numberOfPackets * 4 * 14) / 4) | 0;

            if (pad == 0) {
                lastPacketCount = 14;
            } else {
                ++numberOfPackets;
                lastPacketCount = pad;
            }

            dv_buf.setUint32(COMMAND_OFFSET, self.command.UPLOAD_START);
            dv_buf.setUint32(COUNTER_OFFSET, numberOfPackets);
            dv_buf.setUint8(DATA_OFFSET, type & 0xff);
            dv_buf.setUint8(DATA_OFFSET + 1, lastPacketCount);
            dv_buf.setUint32(DATA_OFFSET + 2, crc);

            serial.send(buf, function(writeInfo) {})
        };

        this.uploadData = function (data, pktCount, pktSize, callback) {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);
            var buf_array = new Uint8Array(buf);
            var uploadDataTimer = null;

            dv_buf.setUint32(COMMAND_OFFSET, self.command.UPLOAD);
            dv_buf.setUint32(COUNTER_OFFSET, pktCount);

            var pos = 14 * 4 * pktCount;
            for (var i = 0; i < pktSize; i++) {
                buf_array[DATA_OFFSET + i * 4] = data[i * 4 + pos + 3];
                buf_array[DATA_OFFSET + i * 4 + 1] = data[i * 4 + pos + 2];
                buf_array[DATA_OFFSET + i * 4 + 2] = data[i * 4 + pos + 1];
                buf_array[DATA_OFFSET + i * 4 + 3] = data[i * 4 + pos];
            }

            serial.send(buf, function(writeInfo) {
                jobQueue.enqueue(uploadDataHandler);
                uploadDataTimer = setTimeout(function () {
                    if (callback) {
                        callback(false, null, null, null);
                    }
                }, 500);
            });

            function uploadDataHandler (data) {
                if (null != uploadDataTimer) {
                    clearTimeout(uploadDataTimer);
                }

                var dv_rxBuf = new DataView(data);

                var count = dv_rxBuf.getUint32(COMMAND_OFFSET);
                var expected_count = dv_rxBuf.getUint32(COUNTER_OFFSET);
                var state = dv_rxBuf.getUint8(DATA_OFFSET);

                if (callback) {
                    callback(true, count, expected_count, state);
                }
            }
        };

        this.findDevice = function (callback) {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);
            var dev_number = 1;
            var findDeviceTimer = null;

            dv_buf.setUint32(COMMAND_OFFSET, self.command.REQ_CAPABILITIES);
            dv_buf.setUint32(COUNTER_OFFSET, 0);
            dv_buf.setUint32(DATA_OFFSET, 0);
            dv_buf.setUint8(DATA_OFFSET, dev_number);

            initReceiveListener();

            serial.send(buf, function(writeInfo) {
                jobQueue.enqueue(findDeviceHandler);
                findDeviceTimer = setTimeout(function () {
                    if (callback) {
                        callback(false);
                    }
                }, 500);
            });

            function findDeviceHandler(data) {
                if (findDeviceTimer != null) {
                    clearTimeout(findDeviceTimer);
                }

                var dv_rxBuf = new DataView(data);
                var ID_OFFSET = 13;
                var SIZEOFCODE_OFFSET = 1;
                var BLVERSION_OFFSET = 6;

                self.devID = dv_rxBuf.getUint16(ID_OFFSET);
                self.sizeOfCode = dv_rxBuf.getUint32(SIZEOFCODE_OFFSET);
                self.blVersion = dv_rxBuf.getUint8(BLVERSION_OFFSET);

                if (callback) {
                    callback(true);
                }
            }
        };

        this.statusRequest = function (callback) {
            var buf = new ArrayBuffer(PKT_SIZE);
            var dv_buf = new DataView(buf);
            var status = this.status.abort;
            var statusRequestTimer = null;

            dv_buf.setUint32(COMMAND_OFFSET, self.command.STATUS_REQUEST);
            dv_buf.setUint32(COUNTER_OFFSET, 0);
            dv_buf.setUint32(DATA_OFFSET, 0);

            serial.send(buf, function(writeInfo) {
                jobQueue.enqueue(statusRequestHandler);
                statusRequestTimer = setTimeout(function () {
                    if (callback) {
                        callback(false, self.status.abort);
                    }
                }, 500);
            });

            function statusRequestHandler (data) {
                if (null != statusRequestTimer) {
                    clearTimeout(statusRequestTimer);
                }

                var dv_rxBuf = new DataView(data);
                var STATUS_OFFSET = 5;

                if (dv_rxBuf.getUint8(0) == 0x01) {
                    status = dv_rxBuf.getUint8(STATUS_OFFSET);
                }

                if (callback) {
                    callback(true, status);
                }
            }
        };
    }
})();
