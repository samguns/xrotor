(function() {
  'use strict';

  angular
    .module('chromeGcs', ['ngAnimate', 'ngCookies', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router',
          'ngMaterial', 'settings', 'components', 'services', 'chromeGcs.modules', 'gettext'])
})();
