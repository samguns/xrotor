(function() {
  'use strict';

  angular
    .module('chromeGcs')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout,
                          $mdSidenav,
                          $mdDialog,
                          languageChangeDialog,
                          i18nGettextService,
                          globalSettings,
                          tooltips,
                          navMenuService,
                          dashBoardService,
                          uavoBrowserService,
                          wizardService,
                          firmwareService,
                          SerialPortService) {
      var vm = this;

      vm.appLogo = globalSettings.getAppLogo();
      vm.appName = globalSettings.getAppName();
      vm.port_service = SerialPortService;
      vm.autoFocusContent = false;

      vm.tooltipChangeLang = tooltips.tooltipChangeLang;

      vm.navMenu = navMenuService.getMenu();
      vm.navMenuSections = vm.navMenu.sections;
      vm.i18nLanguages = i18nGettextService.languages;

      vm.isOpen = function (section) {
          return vm.navMenu.isSectionSelected(section);
      };

      vm.onConnect = onSerialConnect;
      vm.openMenu = openMenu;
      vm.closeMenu = closeMenu;
      vm.setCurrentLanguage = onSetCurrentLanguage;

      function onSerialConnect() {
          if (SerialPortService.btnConnectLable == "Connect") {
              SerialPortService.connect();
          } else {
              SerialPortService.disconnect();
          }
      }

      function onSetCurrentLanguage (lang) {
          if (!lang) {
              return;
          }

          localSave({'locale': lang}, popupLanguageChangeDialog());
      }

      function popupLanguageChangeDialog () {
          var dialog = $mdDialog.alert()
              .title(languageChangeDialog.Title)
              .textContent(languageChangeDialog.Content)
              .ariaLabel(languageChangeDialog.ariaLabel)
              .targetEvent()
              .ok(languageChangeDialog.OK);

          $mdDialog.show(dialog);
      }

      function closeMenu() {
          $timeout(function() { $mdSidenav('left').close(); });
      }

      function openMenu() {
          $timeout(function() { $mdSidenav('left').open(); });
      }
  }

})();
