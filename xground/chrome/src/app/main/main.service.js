(function() {
    'use strict';

    angular
        .module('chromeGcs')
        .service('navMenuService', navMenuService)
        .service('i18nGettextService', i18nGettextService);

    function navMenuService(menuService) {
        var menu = menuService.getInstance();

        this.addMenuItem = function (itemObj) {
            if ((itemObj.name == undefined) || (itemObj.name == '') ||
                (itemObj.state == undefined) || (itemObj.state == '')) {
                return;
            }

            menu.addMenuItem(itemObj);
        };

        this.getMenu = function () {
            return menu;
        }

    }

    function i18nGettextService() {
        this.languages = {
            'zh_CN': '中文（简体）',
            'en': 'English'
        };
    }

})();
