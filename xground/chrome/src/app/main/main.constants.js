(function() {
    'use strict';

    angular
        .module('chromeGcs')
        .service('languageChangeDialog', languageChangeDialog)
        .service('tooltips', tooltips);

    function languageChangeDialog(gettextCatalog) {
        this.Title = gettextCatalog.getString("Language changed!");
        this.Content = gettextCatalog.getString("Please reload to display ChromeGCS in changed language.");
        this.ariaLabel = gettextCatalog.getString("Language change dialog");
        this.OK = gettextCatalog.getString("OK");
    }

    function tooltips(gettextCatalog) {
        this.tooltipChangeLang = gettextCatalog.getString("Change language");
    }
})();
