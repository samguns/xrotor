(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .config(uavoBrowserRouterConfig)
        .config(objectViewRouterConfig);

    function uavoBrowserRouterConfig($stateProvider, appMainRouterConstants) {
        $stateProvider
            .state(appMainRouterConstants.UAVOBrowserState, {
                url: appMainRouterConstants.UAVOBrowserUrl,
                views: {
                    'module@appMain': {
                        controller: 'UAVOBrowserController',
                        controllerAs: 'vm',
                        templateUrl: appMainRouterConstants.UAVOBrowserTemplate
                    }
                }
            });
    }

    function objectViewRouterConfig($stateProvider, objViewRouterConstants) {
        $stateProvider
            .state(objViewRouterConstants.objectState, {
                url: objViewRouterConstants.objectUrl,
                views: {
                    'objectView@appMain.UAVOBrowser': {
                        templateUrl: objViewRouterConstants.objectTemplate
                    }
                }
            })
            .state(objViewRouterConstants.metaObjectState, {
                url: objViewRouterConstants.metaObjectUrl,
                views: {
                    'objectView@appMain.UAVOBrowser': {
                        templateUrl: objViewRouterConstants.metaObjectTemplate
                    }
                }
            });
    }

})();
