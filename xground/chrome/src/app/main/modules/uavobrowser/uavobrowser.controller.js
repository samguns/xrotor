(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .controller('UAVOBrowserController', UAVOBrowserController);

    /** @ngInject */
    function UAVOBrowserController($injector,
                                   UAVObjectUtilService,
                                   viewLabels,
                                   objNavMenuService) {
        var vm = this;
        var openedSection = null;

        vm.btnLabelRequest = viewLabels.btnLabelRequest;
        vm.btnLabelSend = viewLabels.btnLabelSend;
        vm.btnLabelLoad = viewLabels.btnLabelLoad;
        vm.btnLabelSave = viewLabels.btnLabelSave;
        vm.btnLabelErase = viewLabels.btnLabelErase;

        var objNavMenu = objNavMenuService.getObjNavMenu();
        vm.sections = objNavMenu.sections;

        vm.isOpen = function (section) {
            return objNavMenu.isSectionSelected(section);
        };

        vm.toggleOpen = function (section) {
            objNavMenu.toggleSelectSection(section);
            showObjectFields(section.name);
        };

        vm.linkSelected = function (section) {
            showObjectFields(section.name);
        };

        vm.isSectionOpen = function (section) {
            return openedSection === section;
        };

        vm.toggleSectionOpen = function (section) {
            openedSection = (openedSection === section ? null : section);
        };

        vm.onFlagUpdatePeriodicClicked = function () {
            vm.flagUpdateOnChange = false;
        };

        vm.onFlagGCSUpdatePeriodicClicked = function () {
            vm.flagGCSUpdateOnChange = false;
        };

        vm.onFlagUpdateOnChangeClicked = function () {
            vm.flagUpdatePeriodic = false;
        };

        vm.onFlagGCSUpdateOnChangeClicked = function () {
            vm.flagGCSUpdatePeriodic = false;
        };

        vm.Reqeust = function () {
            if (vm.currentObject == undefined) {
                return;
            }

            vm.currentObject.requestUpdate();
        };

        vm.Send = function () {
            if (vm.currentObject == undefined) {
                return;
            }

            if (vm.currentObject.isMetaObject() == true) {
                sendMetaObject();
                return;
            }

            sendGenericObject();
        };

        vm.Load = function () {
            if (vm.currentObject == undefined) {
                return;
            }

            UAVObjectUtilService
                .updateObjectPersistence(object_operation.LOAD, vm.currentObject);
            vm.currentObject.requestUpdate();
        };

        vm.Save = function () {
            if (vm.currentObject == undefined) {
                return;
            }

            vm.Send();
            if (vm.currentObject.isSettingsObject()) {
                UAVObjectUtilService
                    .updateObjectPersistence(object_operation.SAVE, vm.currentObject);
            }
        };

        vm.Erase = function () {
            if (vm.currentObject == undefined) {
                return;
            }

            UAVObjectUtilService
                .updateObjectPersistence(object_operation.DELETE, vm.currentObject);
        };

        vm.openUAVOOperationMenu = function ($mdOpenMenu, ev) {
            $mdOpenMenu(ev);
        };

        function showObjectFields(objName) {
            if ((types.OBJ_CATALOG_SETTING == objName) ||
                (types.OBJ_CATALOG_DATA == objName)) {
                return;
            }

            vm.currentObjName = objName;
            var currentObjService = $injector.get(objName + 'ObjService');

            if ((currentObjService == undefined) || (currentObjService == '')) {
                return;
            }

            currentObjService.onObjectUpdated = onCurrentObjectUpdated;

            vm.currentObject = currentObjService.obj;
            if (vm.currentObject.isMetaObject() == true) {
                showMetaObjectFields();
                return;
            }

            showGenericObjectFields();
        }

        function showMetaObjectFields() {
            vm.flightTelemetryUpdatePeriod = vm.currentObject.getField("flightTelemetryUpdatePeriod").getValue();
            vm.gcsTelemetryUpdatePeriod = vm.currentObject.getField("gcsTelemetryUpdatePeriod").getValue();
            var flags = vm.currentObject.getField("flags").getValue();
            vm.flagTelemetryAcked =(flags & (1 << UAVOBJ_TELEMETRY_ACKED_SHIFT)) ? true : false;
            vm.flagGCSTelemetryAcked = (flags & (1 << UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT)) ? true : false;
            vm.flagAccessReadOnly = (flags & (ACCESS_READONLY << UAVOBJ_ACCESS_SHIFT)) ? true : false;
            vm.flagGCSAccessReadOnly = (flags & (ACCESS_READONLY << UAVOBJ_GCS_ACCESS_SHIFT)) ? true : false;
            vm.flagUpdatePeriodic = (flags & (UPDATEMODE_PERIODIC << UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT)) ? true : false;
            vm.flagGCSUpdatePeriodic = (flags & (UPDATEMODE_PERIODIC << UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT)) ? true : false;
            vm.flagUpdateOnChange = (flags & (UPDATEMODE_ONCHANGE << UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT)) ? true : false;
            vm.flagGCSUpdateOnChange = (flags & (UPDATEMODE_ONCHANGE << UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT)) ? true : false;
        }

        function showGenericObjectFields() {
            vm.rows = [];
            var fields = vm.currentObject.getFields();

            for (var i = 0; i < fields.length; i++) {
                var currField = new UAVObjectField(fields[i]);
                var fieldName = currField.getName();
                var options = (currField.getOptions()) ? currField.getOptions() : 0;

                vm.rows[i] = {};
                vm.rows[i]['name'] = fieldName;
                vm.rows[i]['properties'] = [];
                vm.rows[i]['options'] = [];

                for (var j = 0; j < currField.getNumElements(); j++) {

                    if(currField.getNumElements()==1){
                        var elementName = currField.getName();
                        var value = currField.getValue(0);
                        var units = currField.getUnits();
                        var type = currField.getType();
                    } else {
                        elementName = currField.elements[j];
                        value = currField.getValue(j);
                        units = currField.getUnits(j);
                        type = currField.getType(j);
                    }

                    var properties;
                    var inputType = (type!="enum") ? "number" : "text";
                    value = (type!="enum") ? Number(value) : value;
                    if (units == "hex") {
                        value = value.toString(16).toUpperCase();
                        inputType = "text";
                    }
                    properties = {'index':j,'name':elementName,'value':value,'units':units,'type':type,'options':options,'inputType':inputType};
                    vm.rows[i]['properties'].push(properties);
                }
            }
        }

        function sendMetaObject() {
            vm.currentObject
                .getField("flightTelemetryUpdatePeriod")
                .setValue(vm.flightTelemetryUpdatePeriod);
            vm.currentObject
                .getField("gcsTelemetryUpdatePeriod")
                .setValue(vm.gcsTelemetryUpdatePeriod);

            var flags = 0;
            if (vm.flagTelemetryAcked) {
                flags |= 1 << UAVOBJ_TELEMETRY_ACKED_SHIFT;
            }

            if (vm.flagGCSTelemetryAcked) {
                flags |= 1 << UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT;
            }

            if (vm.flagAccessReadOnly) {
                flags |= ACCESS_READONLY << UAVOBJ_ACCESS_SHIFT;
            }

            if (vm.flagGCSAccessReadOnly) {
                flags |= ACCESS_READONLY << UAVOBJ_GCS_ACCESS_SHIFT;
            }

            if (vm.flagUpdatePeriodic) {
                flags |= UPDATEMODE_PERIODIC << UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT;
            }

            if (vm.flagGCSUpdatePeriodic) {
                flags |= UPDATEMODE_PERIODIC << UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT;
            }

            if (vm.flagUpdateOnChange) {
                flags |= UPDATEMODE_ONCHANGE << UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT;
            }

            if (vm.flagGCSUpdateOnChange) {
                flags |= UPDATEMODE_ONCHANGE << UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT;
            }

            vm.currentObject
                .getField("flags")
                .setValue(flags);

            vm.currentObject.updated();
        }

        function sendGenericObject() {
            vm.rows.forEach(function(row) {
                row.properties.forEach(function(property){
                    var value = property.value;
                    if (property.units == 'hex') {
                        value = parseInt(property.value, 16);
                    }

                    vm.currentObject.getField(row.name).setValue(value, property.name);
                });
            });

            vm.currentObject.updated();
        }

        function onCurrentObjectUpdated() {
            var fields = vm.currentObject.getFields();

            for (var i = 0; i < fields.length; i++) {
                var currField = new UAVObjectField(fields[i]);

                for (var j = 0; j < currField.getNumElements(); j++) {

                    if(currField.getNumElements()==1){
                        var value = currField.getValue();
                        var type = currField.getType();
                        var units = currField.getUnits();
                    } else {
                        value = currField.getValue(j);
                        type = currField.getType(j);
                        units = currField.getUnits();
                    }

                    value = (type!="enum") ? Number(value) : value;
                    value = (units == "hex") ? value.toString(16).toUpperCase() : value;
                    vm.rows[i]['properties'][j].value = value;
                }
            }
        }
    }

})();
