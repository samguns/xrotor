(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .service('uavoBrowserService', uavoBrowserService)
        .service('objNavMenuService', objNavMenuService);

    function uavoBrowserService(navMenuService,
                                menuNames,
                                appMainRouterConstants) {
        initNavMenuItem();

        function initNavMenuItem() {
            navMenuService.addMenuItem(
                {
                    name: menuNames.UAVOBrowser,
                    type: types.MENU_LINK,
                    state: appMainRouterConstants.UAVOBrowserState
                }
            );
        }
    }

    function objNavMenuService($timeout,
                               menuService,
                               objViewRouterConstants) {
        var objNavMenu = menuService.getInstance();
        var objMenuItemMap = new Map();

        this.getObjNavMenu = function () {
            return objNavMenu;
        };

        constructObjNavMenu();

        function constructObjNavMenu() {
            var settings = [];
            var datas = [];

            objNavMenu.addMenuItem(
                {
                    name: types.OBJ_CATALOG_SETTING,
                    type: types.MENU_HEADING,
                    children: settings
                });
            objNavMenu.addMenuItem(
                {
                    name: types.OBJ_CATALOG_DATA,
                    type: types.MENU_HEADING,
                    children: datas
                });

            constructChilren(settings, datas);

        }

        function constructChilren(settings, datas) {
            var uavObjects = UAVObjectMgmt.getObjects()[0];
            var object = function () {
                this.name = '';
                this.type = '';
                this.state = '';
                this.customStyle = {};
                this.pages = [];
            };

            for (var i = 0; i < uavObjects.length; i++) {
                if (uavObjects[i].isMetaObject() == true) {
                    continue;
                }

                if (uavObjects[i].isSettingsObject() == true) {
                    var settingObj = new object();

                    settingObj.name = uavObjects[i].name;
                    settingObj.type = types.MENU_TOGGLE;
                    settingObj.state = objViewRouterConstants.objectState;

                    var settingMetaObj = new object();
                    settingMetaObj.name = uavObjects[i].name + types.META_PREFIX;
                    settingMetaObj.type = types.MENU_LINK;
                    settingMetaObj.state = objViewRouterConstants.metaObjectState;
                    settingObj.pages.push(settingMetaObj);
                    settings.push(settingObj);
                } else {
                    var dataObj = new object();

                    dataObj.name = uavObjects[i].name;
                    dataObj.type = types.MENU_TOGGLE;
                    dataObj.state = objViewRouterConstants.objectState;

                    var dataMetaObj = new object();
                    dataMetaObj.name = uavObjects[i].name + types.META_PREFIX;
                    dataMetaObj.type = types.MENU_LINK;
                    dataMetaObj.state = objViewRouterConstants.metaObjectState;
                    dataObj.pages.push(dataMetaObj);
                    datas.push(dataObj);

                    objMenuItemMap.set(uavObjects[i], dataObj);
                }

                var eventListener = new UAVObjectEventListener();
                eventListener.objectUpdated = onObjectUpdated;
                uavObjects[i].addUAVObjectEventListener(eventListener);
            }
        }

        function onObjectUpdated(object) {
            var menuItemObj = objMenuItemMap.get(object);
            if (menuItemObj == undefined) {
                return;
            }

            menuItemObj.customStyle = {'background-color':'orange'};

            $timeout(function () {
                menuItemObj.customStyle = {};
            }, 200);
        }
    }

})();
