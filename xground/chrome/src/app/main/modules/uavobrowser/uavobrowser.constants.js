(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .constant('objViewRouterConstants', {
            objectState: 'appMain.UAVOBrowser.object',
            objectUrl: '/Object',
            objectTemplate: 'app/main/modules/uavobrowser/views/genericobject.template.html',

            dataObjectState: 'appMain.UAVOBrowser.data',
            dataObjectUrl: '/dataObject',
            dataObjectTemplate: 'app/main/modules/uavobrowser/views/genericobject.template.html',

            metaObjectState: 'appMain.UAVOBrowser.meta',
            metaObjectUrl: '/metaObject',
            metaObjectTemplate: 'app/main/modules/uavobrowser/views/metaobject.template.html'
        })
        .service('viewLabels', viewLabels);

    function viewLabels(gettextCatalog) {
        this.btnLabelRequest = gettextCatalog.getString("Request");
        this.btnLabelSend = gettextCatalog.getString("Send");
        this.btnLabelLoad = gettextCatalog.getString("Load");
        this.btnLabelSave = gettextCatalog.getString("Save");
        this.btnLabelErase = gettextCatalog.getString("Erase");
    }
})();
