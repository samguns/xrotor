(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .service('wizardService', wizardService);

    function wizardService(navMenuService,
                           menuNames,
                           appMainRouterConstants) {
        initNavMenuItem();

        function initNavMenuItem() {
            navMenuService.addMenuItem(
                {
                    name: menuNames.Wizard,
                    type: types.MENU_LINK,
                    state: appMainRouterConstants.WizardState
                });
        }
    }

})();
