(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .config(wizardRouterConfig);

    function wizardRouterConfig($stateProvider, appMainRouterConstants) {
        $stateProvider
            .state(appMainRouterConstants.WizardState, {
                url: appMainRouterConstants.WizardUrl,
                views: {
                    'module@appMain': {
                        controller: 'WizardController',
                        controllerAs: 'vm',
                        templateUrl: appMainRouterConstants.WizardTemplate
                    }
                }
            });
    }

})();
