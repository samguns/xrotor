(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .service('firmwareService', firmwareService);

    function firmwareService(navMenuService,
                             menuNames,
                             appMainRouterConstants) {
        initNavMenuItem();

        function initNavMenuItem() {
            navMenuService.addMenuItem(
                {
                    name: menuNames.Firmware,
                    type: types.MENU_LINK,
                    state: appMainRouterConstants.FirmwareState
                });
        }
    }

})();
