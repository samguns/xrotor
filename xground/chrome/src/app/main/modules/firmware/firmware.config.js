(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .config(firmwareRouterConfig);

    function firmwareRouterConfig($stateProvider, appMainRouterConstants) {
        $stateProvider
            .state(appMainRouterConstants.FirmwareState, {
                url: appMainRouterConstants.FirmwareUrl,
                views: {
                    'module@appMain': {
                        controller: 'FirmwareController',
                        controllerAs: 'vm',
                        templateUrl: appMainRouterConstants.FirmwareTemplate
                    }
                }
            });
    }

})();
