(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .service('menuNames', menuNames)
        .constant('appMainRouterConstants', {
            DashboardState: 'appMain.Dashboard',
            DashboardUrl: '/dashboard',
            DashboardTemplate: 'app/main/modules/dashboard/dashboard.template.html',
            FirmwareState: 'appMain.Firmware',
            FirmwareUrl: '/firmware',
            FirmwareTemplate: 'app/main/modules/firmware/firmware.template.html',
            WizardState: 'appMain.Wizard',
            WizardUrl: '/wizard',
            WizardTemplate: 'app/main/modules/wizard/wizard.template.html',
            UAVOBrowserState: 'appMain.UAVOBrowser',
            UAVOBrowserUrl: '/browser',
            UAVOBrowserTemplate: 'app/main/modules/uavobrowser/uavobrowser.template.html'
        });

    function menuNames(gettextCatalog) {
        this.Dashboard = gettextCatalog.getString("Dashboard");
        this.UAVOBrowser = gettextCatalog.getString("UAVO Browser");
        this.Firmware = gettextCatalog.getString("Firmware");
        this.Wizard = gettextCatalog.getString("Wizard");
    }
})();
