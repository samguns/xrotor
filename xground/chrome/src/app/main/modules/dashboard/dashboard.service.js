(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .service('dashBoardService', dashBoardService);

    function dashBoardService(navMenuService,
                              menuNames,
                              gettextCatalog,
                              appMainRouterConstants) {
        this.moduleName = gettextCatalog.getString("I'm Dashboard!");

        initNavMenuItem();

        function initNavMenuItem() {
            navMenuService.addMenuItem(
                {
                    name: menuNames.Dashboard,
                    type: types.MENU_LINK,
                    state: appMainRouterConstants.DashboardState
                });
        }
    }

})();
