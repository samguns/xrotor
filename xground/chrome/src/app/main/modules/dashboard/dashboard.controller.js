(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .controller('DashboardController', DashboardController);

    /** @ngInject */
    function DashboardController(dashBoardService,
                                 FlightTelemetryStatsObjService,
                                 GCSTelemetryStatsObjService) {
        var vm = this;

        vm.moduleName = dashBoardService.moduleName;
        vm.FlightTelemetryObj = FlightTelemetryStatsObjService.obj;
        vm.GCSTelemetryStatsObj = GCSTelemetryStatsObjService.obj;
    }

})();
