(function() {
    'use strict';

    angular
        .module('chromeGcs.modules')
        .config(dashBoardRouterConfig);

    function dashBoardRouterConfig($stateProvider, appMainRouterConstants) {
        $stateProvider
            .state(appMainRouterConstants.DashboardState, {
                url: appMainRouterConstants.DashboardUrl,
                views: {
                    'module@appMain': {
                        controller: 'DashboardController',
                        controllerAs: 'vm',
                        templateUrl: appMainRouterConstants.DashboardTemplate
                    }
                }
            });
    }

})();
