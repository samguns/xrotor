'use strict';

module.exports = function (grunt) {
    grunt.initConfig({
        nggettext_extract: {
            pot: {
                files: {
                    '_locale/template.pot': ['**/*.html', 'src/app/**/*.js']
                }
            }
        },

        nggettext_compile: {
            all: {
                files: {
                    'src/app/translations.js': ['_locale/*.po']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-angular-gettext');

    grunt.registerTask('default', ['nggettext_extract', 'nggettext_compile']);
};
