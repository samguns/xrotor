Get Node.js® Packet Manager (npm) from [https://www.npmjs.com/package/npm](https://www.npmjs.com/package/npm).

To get started, run the following commands:

sudo npm install -g gulp bower 

npm install

bower install

make

This will generate two directories, "dev" and "dist".

### Running

Click "Load unpacked..." and open "dev"

