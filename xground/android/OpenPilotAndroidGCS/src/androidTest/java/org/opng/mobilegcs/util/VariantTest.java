/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.test.InstrumentationTestCase;

public class VariantTest extends InstrumentationTestCase {

    public void testEmptyVariant() throws Exception {

    }

    public void testToByte() throws Exception {

    }

    public void testToUByte() throws Exception {

    }

    public void testToShort() throws Exception {

    }

    public void testToUShort() throws Exception {

    }

    public void testToInt() throws Exception {

    }

    public void testToUInt() throws Exception {

    }

    public void testToLong() throws Exception {

    }

    public void testToFloat() throws Exception {
        Variant variantOne = new Variant(6.347656E-4);
        Variant variantTwo = new Variant(variantOne.toString());
        assertEquals(variantOne.toFloat(), variantTwo.toFloat());
    }

    public void testToFloatWithExponent() throws Exception {
        Variant variantOne = new Variant(0.0006347656);
        Variant variantTwo = new Variant("6.347656E-4");
        assertEquals(variantOne.toFloat(), variantTwo.toFloat());
    }

    public void testToDouble() throws Exception {
        Variant variantOne = new Variant(6.347656E-4);
        Variant variantTwo = new Variant(variantOne.toString());
        assertEquals(variantOne.toDouble(), variantTwo.toDouble());
    }

    public void testToDoubleWithExponent() throws Exception {
        Variant variantOne = new Variant(0.0006347656);
        Variant variantTwo = new Variant("6.347656E-4");
        assertEquals(variantOne.toDouble(), variantTwo.toDouble());
    }

    public void testToNumber() throws Exception {

    }

    public void testToString() throws Exception {

    }

    public void testIsEmpty() throws Exception {

    }

    public void testIsValid() throws Exception {

    }

    public void testIsUnsigned() throws Exception {

    }
}