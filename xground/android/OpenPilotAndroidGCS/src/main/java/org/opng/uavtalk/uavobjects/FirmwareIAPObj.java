/*
 * Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.uavtalk.uavobjects;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObjectField;

/**
Queries board for SN, model, revision, and sends reset command

generated from firmwareiapobj.xml
 **/
public class FirmwareIAPObj extends UAVDataObject {

	public static final String FIELD_CRC = "crc";
	public static final String FIELD_COMMAND = "Command";
	public static final String FIELD_BOARDREVISION = "BoardRevision";
	public static final String FIELD_DESCRIPTION = "Description";
	public static final String FIELD_CPUSERIAL = "CPUSerial";
	public static final String FIELD_BOARDTYPE = "BoardType";
	public static final String FIELD_BOOTLOADERREVISION = "BootloaderRevision";
	public static final String FIELD_ARMRESET = "ArmReset";


	public FirmwareIAPObj() {
		super(OBJID, ISSINGLEINST, ISSETTINGS, NAME);
		
		List<UAVObjectField> fields = new ArrayList<>();
		

		List<String> crcElemNames = new ArrayList<>();
		crcElemNames.add("0");
		fields.add( new UAVObjectField("crc", "", UAVObjectField.FieldType.UINT32, crcElemNames, null, "") );

		List<String> CommandElemNames = new ArrayList<>();
		CommandElemNames.add("0");
		fields.add( new UAVObjectField("Command", "", UAVObjectField.FieldType.UINT16, CommandElemNames, null, "") );

		List<String> BoardRevisionElemNames = new ArrayList<>();
		BoardRevisionElemNames.add("0");
		fields.add( new UAVObjectField("BoardRevision", "", UAVObjectField.FieldType.UINT16, BoardRevisionElemNames, null, "") );

		List<String> DescriptionElemNames = new ArrayList<>();
		DescriptionElemNames.add("0");
		DescriptionElemNames.add("1");
		DescriptionElemNames.add("2");
		DescriptionElemNames.add("3");
		DescriptionElemNames.add("4");
		DescriptionElemNames.add("5");
		DescriptionElemNames.add("6");
		DescriptionElemNames.add("7");
		DescriptionElemNames.add("8");
		DescriptionElemNames.add("9");
		DescriptionElemNames.add("10");
		DescriptionElemNames.add("11");
		DescriptionElemNames.add("12");
		DescriptionElemNames.add("13");
		DescriptionElemNames.add("14");
		DescriptionElemNames.add("15");
		DescriptionElemNames.add("16");
		DescriptionElemNames.add("17");
		DescriptionElemNames.add("18");
		DescriptionElemNames.add("19");
		DescriptionElemNames.add("20");
		DescriptionElemNames.add("21");
		DescriptionElemNames.add("22");
		DescriptionElemNames.add("23");
		DescriptionElemNames.add("24");
		DescriptionElemNames.add("25");
		DescriptionElemNames.add("26");
		DescriptionElemNames.add("27");
		DescriptionElemNames.add("28");
		DescriptionElemNames.add("29");
		DescriptionElemNames.add("30");
		DescriptionElemNames.add("31");
		DescriptionElemNames.add("32");
		DescriptionElemNames.add("33");
		DescriptionElemNames.add("34");
		DescriptionElemNames.add("35");
		DescriptionElemNames.add("36");
		DescriptionElemNames.add("37");
		DescriptionElemNames.add("38");
		DescriptionElemNames.add("39");
		DescriptionElemNames.add("40");
		DescriptionElemNames.add("41");
		DescriptionElemNames.add("42");
		DescriptionElemNames.add("43");
		DescriptionElemNames.add("44");
		DescriptionElemNames.add("45");
		DescriptionElemNames.add("46");
		DescriptionElemNames.add("47");
		DescriptionElemNames.add("48");
		DescriptionElemNames.add("49");
		DescriptionElemNames.add("50");
		DescriptionElemNames.add("51");
		DescriptionElemNames.add("52");
		DescriptionElemNames.add("53");
		DescriptionElemNames.add("54");
		DescriptionElemNames.add("55");
		DescriptionElemNames.add("56");
		DescriptionElemNames.add("57");
		DescriptionElemNames.add("58");
		DescriptionElemNames.add("59");
		DescriptionElemNames.add("60");
		DescriptionElemNames.add("61");
		DescriptionElemNames.add("62");
		DescriptionElemNames.add("63");
		DescriptionElemNames.add("64");
		DescriptionElemNames.add("65");
		DescriptionElemNames.add("66");
		DescriptionElemNames.add("67");
		DescriptionElemNames.add("68");
		DescriptionElemNames.add("69");
		DescriptionElemNames.add("70");
		DescriptionElemNames.add("71");
		DescriptionElemNames.add("72");
		DescriptionElemNames.add("73");
		DescriptionElemNames.add("74");
		DescriptionElemNames.add("75");
		DescriptionElemNames.add("76");
		DescriptionElemNames.add("77");
		DescriptionElemNames.add("78");
		DescriptionElemNames.add("79");
		DescriptionElemNames.add("80");
		DescriptionElemNames.add("81");
		DescriptionElemNames.add("82");
		DescriptionElemNames.add("83");
		DescriptionElemNames.add("84");
		DescriptionElemNames.add("85");
		DescriptionElemNames.add("86");
		DescriptionElemNames.add("87");
		DescriptionElemNames.add("88");
		DescriptionElemNames.add("89");
		DescriptionElemNames.add("90");
		DescriptionElemNames.add("91");
		DescriptionElemNames.add("92");
		DescriptionElemNames.add("93");
		DescriptionElemNames.add("94");
		DescriptionElemNames.add("95");
		DescriptionElemNames.add("96");
		DescriptionElemNames.add("97");
		DescriptionElemNames.add("98");
		DescriptionElemNames.add("99");
		fields.add( new UAVObjectField("Description", "", UAVObjectField.FieldType.UINT8, DescriptionElemNames, null, "") );

		List<String> CPUSerialElemNames = new ArrayList<>();
		CPUSerialElemNames.add("0");
		CPUSerialElemNames.add("1");
		CPUSerialElemNames.add("2");
		CPUSerialElemNames.add("3");
		CPUSerialElemNames.add("4");
		CPUSerialElemNames.add("5");
		CPUSerialElemNames.add("6");
		CPUSerialElemNames.add("7");
		CPUSerialElemNames.add("8");
		CPUSerialElemNames.add("9");
		CPUSerialElemNames.add("10");
		CPUSerialElemNames.add("11");
		fields.add( new UAVObjectField("CPUSerial", "hex", UAVObjectField.FieldType.UINT8, CPUSerialElemNames, null, "") );

		List<String> BoardTypeElemNames = new ArrayList<>();
		BoardTypeElemNames.add("0");
		fields.add( new UAVObjectField("BoardType", "", UAVObjectField.FieldType.UINT8, BoardTypeElemNames, null, "") );

		List<String> BootloaderRevisionElemNames = new ArrayList<>();
		BootloaderRevisionElemNames.add("0");
		fields.add( new UAVObjectField("BootloaderRevision", "", UAVObjectField.FieldType.UINT8, BootloaderRevisionElemNames, null, "") );

		List<String> ArmResetElemNames = new ArrayList<>();
		ArmResetElemNames.add("0");
		fields.add( new UAVObjectField("ArmReset", "", UAVObjectField.FieldType.UINT8, ArmResetElemNames, null, "") );


		// Compute the number of bytes for this object
		int numBytes = 0;
		for (UAVObjectField field : fields) {
            numBytes += field.getNumBytes();
        }
		NUMBYTES = numBytes;

		// Initialize object
		initializeFields(fields, ByteBuffer.allocate(NUMBYTES), NUMBYTES);
		// Set the default field values
		setDefaultFieldValues();
		// Set the object description
		setDescription(DESCRIPTION);
	}

	/**
	 * Create a Metadata object filled with default values for this object
	 * @return Metadata object with default values
	 */
	public Metadata getDefaultMetadata() {
		UAVObject.Metadata metadata = new UAVObject.Metadata();
    	metadata.flags =
		    UAVObject.Metadata.AccessModeNum(UAVObject.AccessMode.ACCESS_READWRITE) << UAVOBJ_ACCESS_SHIFT |
		    UAVObject.Metadata.AccessModeNum(UAVObject.AccessMode.ACCESS_READWRITE) << UAVOBJ_GCS_ACCESS_SHIFT |
		    1 << UAVOBJ_TELEMETRY_ACKED_SHIFT |
		    1 << UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT |
		    UAVObject.Metadata.UpdateModeNum(UAVObject.UpdateMode.UPDATEMODE_ONCHANGE) << UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT |
		    UAVObject.Metadata.UpdateModeNum(UAVObject.UpdateMode.UPDATEMODE_MANUAL) << UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT |
		    UAVObject.Metadata.UpdateModeNum(UAVObject.UpdateMode.UPDATEMODE_MANUAL) << UAVOBJ_LOGGING_UPDATE_MODE_SHIFT;
    	metadata.flightTelemetryUpdatePeriod = 0;
    	metadata.gcsTelemetryUpdatePeriod = 0;
    	metadata.loggingUpdatePeriod = 0;
 
		return metadata;
	}

	/**
	 * Initialize object fields with the default values.
	 * If a default value is not specified the object fields
	 * will be initialized to zero.
	 */
	private void setDefaultFieldValues()
	{

	}

	/**
	 * Create a clone of this object, a new instance ID must be specified.
	 * Do not use this function directly to create new instances, the
	 * UAVObjectManager should be used instead.
	 */
	public UAVDataObject clone(long instID) {
		// TODO: Need to get specific instance to clone
		try {
			FirmwareIAPObj obj = new FirmwareIAPObj();
			obj.initialize(instID, this.getMetaObject());
			return obj;
		} catch  (Exception e) {
			return null;
		}
	}

	/**
	 * Returns a new instance of this UAVDataObject with default field
	 * values. This is intended to be used by 'reset to default' functionality.
	 * 
	 * @return new instance of this class with default values.
	 */
	@Override
	public UAVDataObject getDefaultInstance(){
		return new FirmwareIAPObj();
	}

	/**
	 * Static function to retrieve an instance of the object.
	 */
	public FirmwareIAPObj GetInstance(UAVObjectManager objMngr, long instID)
	{
	    return (FirmwareIAPObj)(objMngr.getObject(FirmwareIAPObj.OBJID, instID));
	}

	// Constants
	private static final long OBJID = 0x8328F252L;
	private static final String NAME = "FirmwareIAPObj";
	private static final String DESCRIPTION = "Queries board for SN, model, revision, and sends reset command";
	private static final boolean ISSINGLEINST = 1 > 0;
	private static final boolean ISSETTINGS = 0 > 0;
	private static int NUMBYTES = 0;


}
