/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import org.opng.mobilegcs.AppMenu;

public abstract class BaseActivity extends TelemetryActivity {

    protected AppMenuItem mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int id = -1;
        if (savedInstanceState != null)
            id = savedInstanceState.getInt(AppMenu.APP_MENU_ITEM, -1);

        if (id == -1)
            id = getIntent().getIntExtra(AppMenu.APP_MENU_ITEM, -1);

        if (id != -1) {
            mMenu = AppMenu.getMenuItem(id);
            if (mMenu != null) {
                setTitle(mMenu.mTitle);
            }
        }

        setActionProvidersToShow(ActivityTelemetryConnectionHandler.TELEMETRY_STATUS |
                ActivityTelemetryConnectionHandler.SYSTEM_ALARMS);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(AppMenu.APP_MENU_ITEM, mMenu.getId());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int id = -1;
        if (savedInstanceState != null)
            id = savedInstanceState.getInt(AppMenu.APP_MENU_ITEM, -1);

        if (id != -1) {
            mMenu = AppMenu.getMenuItem(id);
            setTitle(mMenu.mTitle);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                //NavUtils.navigateUpFromSameTask(this);

                Intent intent = NavUtils.getParentActivityIntent(this);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                NavUtils.navigateUpTo(this, intent);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void initToolbar(int toolbarId) {
        Toolbar toolbar = (Toolbar) findViewById(toolbarId);
        setSupportActionBar(toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setDisplayHomeAsUpEnabled(true);
        }
    }
}
