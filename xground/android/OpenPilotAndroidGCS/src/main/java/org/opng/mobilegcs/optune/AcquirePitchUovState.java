/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.optune;

import android.os.Parcel;
import android.util.Log;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.wizard.WizardState;
import org.opng.uavtalk.uavobjects.TxPIDSettings;

class AcquirePitchUovState extends WizardState implements OPTuneFragment.OPTuneWizardState {
    public static final Creator<AcquirePitchUovState> CREATOR = new Creator<AcquirePitchUovState>() {
        @Override
        public AcquirePitchUovState createFromParcel(Parcel source) {
            return new AcquirePitchUovState(source);
        }

        @Override
        public AcquirePitchUovState[] newArray(int size) {
            return new AcquirePitchUovState[size];
        }
    };
    private final static String TAG = AcquirePitchUovState.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private OPTuneFragment mOwner;

    AcquirePitchUovState(WizardState previousState, OPTuneFragment owner) {
        super(previousState);
        mOwner = owner;
    }

    private AcquirePitchUovState(Parcel source) {
        super(source);
    }

    @Override
    public WizardState next() {
        if (DEBUG) Log.d(TAG, "AcquirePitchUovState: next");

        // We want to restore the starting pitch rate Kp and store our
        // UOV for pitch as we're about to either process roll or calculate
        // our tuned PIDs
        mOwner.saveUovAndRestoreKp(OPTuneFragment.Axis.PITCH);

        //noinspection PointlessBooleanExpression
        if (!OPTuneFragment.PITCH_AND_ROLL_ONLY) {
            return new AcquireYawUovState(this, mOwner);
        } else {
            return new CalculateState(this, mOwner);
        }
    }

    @Override
    public void process() {
        if (DEBUG) Log.d(TAG, "AcquirePitchUovState: process");

        mOwner.setInstructions(String.format(mOwner.getString(R.string.text_optune_wizard_acquire_uov), "Pitch"));

        // We are adjusting pitch rate Kp
        mOwner.getTxPidGuiHelper().setPIDOption(TxPIDSettings.PIDS_PITCH_RATE_KP, 0);
        mOwner.getRadioGroupAxes().check(R.id.radioPitch);

        // Zero Ki & Kd. Note that this method will only do this once
        // when we start to process the first axis
        mOwner.zeroKiKd();
        mOwner.getTxPidGuiHelper().saveChangesToRam();
    }

    @Override
    public void setOwner(OPTuneFragment owner) {
        mOwner = owner;
        ((OPTuneFragment.OPTuneWizardState) previous()).setOwner(owner);
    }
}
