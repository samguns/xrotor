/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.tasks.bamboo;

import android.support.annotation.NonNull;

import org.opng.mobilegcs.firmware.downloader.bamboo.Branch;
import org.opng.mobilegcs.firmware.downloader.bamboo.Plan;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Use this task to populate the given branch (passed to execute) with data from the plan
 * it references. This is intended to be used with Branch instances originating from a call
 * to the FetchPlanDataTask.
 */
public class FetchBranchDataFromPlanTask extends FetchBranchDataTask {

    public FetchBranchDataFromPlanTask(FetchCallback fetchCallback) {
        super(fetchCallback);
    }

    @NonNull
    @Override
    protected URL constructURL(Branch branch, Plan plan) throws MalformedURLException {
        return new URL(String.format("%s/rest/api/latest/result/%s/latest.json?expand=artifacts",
                branch.getPlan().getRestUrl(), branch.getKey()));
    }
}
