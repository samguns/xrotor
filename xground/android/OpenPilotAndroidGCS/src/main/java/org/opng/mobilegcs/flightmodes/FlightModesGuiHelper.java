/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.flightmodes;

import android.view.View;

import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.FlightModeSettings;
import org.opng.uavtalk.uavobjects.ManualControlSettings;
import org.opng.uavtalk.uavobjects.StabilizationSettings;

class FlightModesGuiHelper {

    private final UAVObjectToGuiUtils mObjectToGuiUtils;

    private FlightModeSettings mFlightModeSettings;
    private StabilizationSettings mStabilizationSettings;
    private ManualControlSettings mManualControlSettings;

    public FlightModesGuiHelper(UAVObjectManager uavObjectManager, UAVObjectToGuiUtils objectToGuiUtils) {
        mObjectToGuiUtils = objectToGuiUtils;

        init(uavObjectManager);
    }

    private void init(UAVObjectManager uavObjectManager) {
        mFlightModeSettings = (FlightModeSettings) uavObjectManager.getObject("FlightModeSettings");
        mStabilizationSettings = (StabilizationSettings) uavObjectManager.getObject("StabilizationSettings");
        mManualControlSettings = (ManualControlSettings) uavObjectManager.getObject("ManualControlSettings");
    }

    public void bindNbFlightModes(View nbFlightMode) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mManualControlSettings, mManualControlSettings.getField(ManualControlSettings.FIELD_FLIGHTMODENUMBER), nbFlightMode, 0, 1, true, null, 0);
    }

    public void bindFlightPosWidgets(int flightPos, View flightMode, View bank, View assist) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mFlightModeSettings, mFlightModeSettings.getField(FlightModeSettings.FIELD_FLIGHTMODEPOSITION), flightMode, flightPos-1, 1, true, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mStabilizationSettings, mStabilizationSettings.getField(StabilizationSettings.FIELD_FLIGHTMODEMAP), bank, flightPos-1, 1, true, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mStabilizationSettings, mStabilizationSettings.getField(StabilizationSettings.FIELD_FLIGHTMODEASSISTMAP), assist, flightPos-1, 1, true, null, 0);
    }

    public void bindFlightModeSettings(int stabilizeNb, View roll, View pitch, View yaw, View thrust) {
        String field = "";
        switch (stabilizeNb) {
            case 1:
                field = FlightModeSettings.FIELD_STABILIZATION1SETTINGS;
                break;
            case 2:
                field = FlightModeSettings.FIELD_STABILIZATION2SETTINGS;
                break;
            case 3:
                field = FlightModeSettings.FIELD_STABILIZATION3SETTINGS;
                break;
            case 4:
                field = FlightModeSettings.FIELD_STABILIZATION4SETTINGS;
                break;
            case 5:
                field = FlightModeSettings.FIELD_STABILIZATION5SETTINGS;
                break;
            case 6:
                field = FlightModeSettings.FIELD_STABILIZATION6SETTINGS;
                break;
        }

        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mFlightModeSettings, mFlightModeSettings.getField(field), roll, 0, 1, true, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mFlightModeSettings, mFlightModeSettings.getField(field), pitch, 1, 1, true, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mFlightModeSettings, mFlightModeSettings.getField(field), yaw, 2, 1, true, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mFlightModeSettings, mFlightModeSettings.getField(field), thrust, 3, 1, true, null, 0);
    }

    public void refreshValues() {
        if (mFlightModeSettings != null) {
            mFlightModeSettings.requestUpdate();
        }

        if (mStabilizationSettings != null) {
            mStabilizationSettings.requestUpdate();
        }

        if (mManualControlSettings != null) {
            mManualControlSettings.requestUpdate();
        }
    }
}
