/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.CopyOnWriteArraySet;

import org.opng.mobilegcs.util.UAVObjectUtilManager;

public class UAVObjectManager {
    private static final int MAX_INSTANCES = 10;
    private final Set<UAVObjectManagerEventListener> mEventListeners = new CopyOnWriteArraySet<>();
    // Use array list to store objects since rarely added or deleted
    private final List<List<UAVObject>> mObjects = new CopyOnWriteArrayList<>();
    private UAVObjectUtilManager mUAVObjectUtilManager;

    public UAVObjectManager() {
    }

    public void addUAVObjectManagerEventListener(UAVObjectManagerEventListener listener) {
        mEventListeners.add(listener);
    }

    public void removeUAVObjectManagerEventListener(UAVObjectManagerEventListener listener) {
        mEventListeners.remove(listener);
    }

    /**
     * Register an object with the manager. This function must be called for all newly created instances.
     * A new instance can be created directly by instantiating a new object or by calling clone() of
     * an existing object. The object will be registered and will be properly initialized so that it can accept
     * updates.
     *
     * @throws Exception
     */
    public synchronized boolean registerObject(UAVDataObject obj) throws Exception {

        ListIterator<List<UAVObject>> objIt = mObjects.listIterator(0);

        // Check if this object type is already in the list
        while (objIt.hasNext()) {
            List<UAVObject> instList = objIt.next();

            // Check if the object ID is in the list
            if ((instList.size() > 0) && (instList.get(0).getObjID() == obj.getObjID())) {
                // Check if this is a single instance object, if yes we can not add a new instance
                if (obj.isSingleInstance()) {
                    return false;
                }
                // The object type has alredy been added, so now we need to initialize the new instance with the appropriate id
                // There is a single metaobject for all object instances of this type, so no need to create a new one
                // Get object type metaobject from existing instance
                UAVDataObject refObj = (UAVDataObject) instList.get(0);
                if (refObj == null) {
                    return false;
                }
                UAVMetaObject mobj = refObj.getMetaObject();

                // Make sure we aren't requesting to create too many instances
                if (obj.getInstID() >= MAX_INSTANCES || instList.size() >= MAX_INSTANCES || obj.getInstID() < 0) {
                    return false;
                }

                // If InstID is zero then we find the next open instId and create it
                if (obj.getInstID() == 0) {
                    // Assign the next available ID and initialize the object instance the nadd
                    obj.initialize(instList.size(), mobj);
                    instList.add(obj);
                    return true;
                }

                // Check if that inst ID already exists
                ListIterator<UAVObject> instIter = instList.listIterator();
                while (instIter.hasNext()) {
                    UAVObject testObj = instIter.next();
                    if (testObj.getInstID() == obj.getInstID()) {
                        return false;
                    }
                }

                // If the instance ID is specified and not at the default value (0) then we need to make sure
                // that there are no gaps in the instance list. If gaps are found then then additional instances
                // will be created.
                for (long instId = instList.size(); instId < obj.getInstID(); instId++) {
                    UAVDataObject newObj = obj.clone(instId);
                    newObj.initialize(mobj);
                    instList.add(newObj);
                    fireNewInstanceEvent(newObj);
                }
                obj.initialize(mobj);
                instList.add(obj);
                fireNewInstanceEvent(obj);

                instIter = instList.listIterator();
                while (instIter.hasNext()) {
                    UAVObject testObj = instIter.next();
                    if (testObj.getInstID() == obj.getInstID()) {
                        return false;
                    }
                }


                // Check if there are any gaps between the requested instance ID and the ones in the list,
                // if any then create the missing instances.
                for (long instId = instList.size(); instId < obj.getInstID(); ++instId) {
                    UAVDataObject cobj = obj.clone(instId);
                    cobj.initialize(mobj);
                    instList.add(cobj);
                    fireNewInstanceEvent(cobj);
                }
                // Finally, initialize the actual object instance
                obj.initialize(mobj);
                // Add the actual object instance in the list
                instList.add(obj);
                fireNewInstanceEvent(obj);
                return true;
            }

        }

        // If this point is reached then this is the first time this object type (ID) is added in the list
        // create a new list of the instances, add in the object collection and create the object's metaobject
        // Create metaobject
        String mname = obj.getName();
        mname += "Meta";

        UAVMetaObject mobj = new UAVMetaObject(obj.getObjID() + 1, mname, obj);
        // Initialize object
        obj.initialize(0, mobj);
        // Add to list
        addObject(obj);
        addObject(mobj);
        return true;
    }

    private synchronized void addObject(UAVObject obj) {
        // Add to list
        List<UAVObject> ls = new ArrayList<>();
        ls.add(obj);
        mObjects.add(ls);
        fireNewObjectEvent(obj);
    }

    /**
     * Get all objects. A two dimentional QList is returned. Objects are grouped by
     * instances of the same object type.
     */
    public List<List<UAVObject>> getObjects() {
        return mObjects;
    }

    /**
     * Same as getObjects() but will only return DataObjects.
     */
    public synchronized List<List<UAVDataObject>> getDataObjects() {
        List<List<UAVDataObject>> dObjects = new ArrayList<>();

        // Go through objects and copy to new list when types match
        ListIterator<List<UAVObject>> objIt = mObjects.listIterator(0);

        // Check if this object type is already in the list
        while (objIt.hasNext()) {
            List<UAVObject> instList = objIt.next();

            // If no instances skip
            if (instList.size() == 0)
                continue;

            // If meta data skip
            if (instList.get(0).isMetadata())
                continue;

            List<UAVDataObject> newInstList = new ArrayList<>();
            for (UAVObject anInstList : instList) {
                newInstList.add((UAVDataObject) anInstList);
            }
            dObjects.add(newInstList);
        }
        // Done
        return dObjects;
    }

    /**
     * Same as getObjects() but will only return MetaObjects.
     */
    public synchronized List<List<UAVMetaObject>> getMetaObjects() {
        List<List<UAVMetaObject>> objects = new ArrayList<>();

        // Go through objects and copy to new list when types match
        ListIterator<List<UAVObject>> objIt = mObjects.listIterator(0);

        // Check if this object type is already in the list
        while (objIt.hasNext()) {
            List<UAVObject> instList = objIt.next();

            // If no instances skip
            if (instList.size() == 0)
                continue;

            // If meta data skip
            if (!instList.get(0).isMetadata())
                continue;

            List<UAVMetaObject> newInstList = new ArrayList<>();
            for (UAVObject anInstList : instList) {
                newInstList.add((UAVMetaObject) anInstList);
            }
            objects.add(newInstList);
        }
        // Done
        return objects;
    }

    /**
     * Returns a specific object by name only, returns instance ID zero
     *
     * @param name The object name
     * @return The object or null if not found
     */
    public UAVObject getObject(String name) {
        return getObject(name, 0, 0);
    }

    /**
     * Get a specific object given its name and instance ID
     *
     * @return The object is found or NULL if not
     */
    public UAVObject getObject(String name, long instId) {
        return getObject(name, 0, instId);
    }

    /**
     * Get a specific object with given object ID and instance ID zero
     *
     * @param objId the object id
     * @return The object is found or NULL if not
     */
    public UAVObject getObject(long objId) {
        return getObject(null, objId, 0);
    }

    /**
     * Get a specific object given its object and instance ID
     *
     * @return The object is found or NULL if not
     */
    public UAVObject getObject(long objId, long instId) {
        return getObject(null, objId, instId);
    }

    /**
     * Helper function for the public getObject() functions.
     */
    public synchronized UAVObject getObject(String name, long objId, long instId) {
        // Check if this object type is already in the list
        for (List<UAVObject> instList : mObjects) {
            if (instList.size() > 0) {
                if ((name != null && instList.get(0).getName().compareTo(name) == 0) || (name == null && instList.get(0).getObjID() == objId)) {
                    // Look for the requested instance ID
                    for (UAVObject obj : instList) {
                        if (obj.getInstID() == instId) {
                            return obj;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Get all the instances of the object specified by name
     */
    public List<UAVObject> getObjectInstances(String name) {
        return getObjectInstances(name, 0);
    }

    /**
     * Get all the instances of the object specified by its ID
     */
    public List<UAVObject> getObjectInstances(long objId) {
        return getObjectInstances(null, objId);
    }

    /**
     * Helper function for the public getObjectInstances()
     */
    private synchronized List<UAVObject> getObjectInstances(String name, long objId) {
        // Check if this object type is already in the list
        for (List<UAVObject> instList : mObjects) {
            if (instList.size() > 0) {
                if ((name != null && instList.get(0).getName().compareTo(name) == 0) || (name == null && instList.get(0).getObjID() == objId)) {
                    return instList;
                }
            }
        }

        return null;
    }

    /**
     * Get the number of instances for an object given its name
     */
    public int getNumInstances(String name) {
        return getNumInstances(name, 0);
    }

    /**
     * Get the number of instances for an object given its ID
     */
    public int getNumInstances(long objId) {
        return getNumInstances(null, objId);
    }

    /**
     * Helper function for public getNumInstances
     */
    private int getNumInstances(String name, long objId) {
        final List<UAVObject> objectInstances = getObjectInstances(name, objId);
        return objectInstances != null ? objectInstances.size() : 0;
    }

    private synchronized void fireNewObjectEvent(UAVObject obj) {
        for (UAVObjectManagerEventListener curListener : mEventListeners) {
            curListener.newObject(obj);
        }
    }

    private synchronized void fireNewInstanceEvent(UAVObject obj) {
        for (UAVObjectManagerEventListener curListener : mEventListeners) {
            curListener.newInstance(obj);
        }
    }

    public UAVObjectUtilManager getObjectUtilManager() {
        if (mUAVObjectUtilManager == null) {
            mUAVObjectUtilManager = new UAVObjectUtilManager(this);
        }

        return mUAVObjectUtilManager;
    }

    public interface UAVObjectManagerEventListener extends EventListener {
        void newInstance(UAVObject obj);

        void newObject(UAVObject obj);
    }
}
