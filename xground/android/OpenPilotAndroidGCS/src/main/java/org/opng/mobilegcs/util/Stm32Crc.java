/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.util;

import android.util.Log;

import java.util.zip.Checksum;

public class Stm32Crc implements Checksum {
    private static final String TAG = "Stm32Crc";
    private static final int[] mCrcTable = { // Nibble lookup table for 0x04C11DB7 polynomial
            0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9, 0x130476DC, 0x17C56B6B, 0x1A864DB2, 0x1E475005,
            0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61, 0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD};
    private int mCrc = 0xFFFFFFFF;

    private static int crc32Slow(int crc, int data) {
        int i;

        crc = crc ^ data;

        for (i = 0; i < 32; i++)
            if ((crc & 0x80000000) > 0)
                crc = (crc << 1) ^ 0x04C11DB7; // Polynomial used in STM32
            else
                crc = (crc << 1);

        return (crc);
    }

    private static int crc32Fast(int crc, int data) {

        crc = crc ^ data; // Apply all 32-bits

        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F]; // Assumes 32-bit reg, masking index to 4-bits
        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F]; // 0x04C11DB7 Polynomial used in STM32
        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F];
        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F];
        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F];
        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F];
        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F];
        crc = (crc << 4) ^ mCrcTable[(crc >> 28) & 0x0F];

        return (crc);
    }

    @Override
    public long getValue() {
        return mCrc;
    }

    @Override
    public void reset() {
        mCrc = 0xFFFFFFFF;
    }

    public void update(byte[] buf) {
        update(buf, 0, buf.length);
    }

    @Override
    public void update(byte[] buf, int off, int nbytes) {
        // if its not divisible by 4 we are up a creek without a paddle
        if ((nbytes % 4) != 0) {
            Log.d(TAG, "Bad CRC update size: " + nbytes);
            reset();
            return;
        }
        if ((off % 4) != 0) {
            Log.d(TAG, "Bad CRC offset size: " + off);
            reset();
            return;
        }

        int hold;

        for (int i = off; i < (off + nbytes) / 4; ++i) {
            final int index = i * 4;
            hold = (buf[index] & 0xFF) +
                    ((buf[index + 1] & 0xFF) << 8) +
                    ((buf[index + 2] & 0xFF) << 16) +
                    ((buf[index + 3] & 0xFF) << 24);
            mCrc = crc32Fast(mCrc, hold); // 4-bytes at a time
        }

    }

    @Override
    public void update(int val) {
        mCrc = crc32Slow(mCrc, val);
    }
}
