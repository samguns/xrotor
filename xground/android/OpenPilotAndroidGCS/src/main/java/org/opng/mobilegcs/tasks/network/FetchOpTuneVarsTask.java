/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.tasks.network;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.optune.OpTuneVars;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * AsyncTask used to fetch OpTuneVars from a URL passed to the execute method.
 */
public class FetchOpTuneVarsTask extends AsyncTask<String, Void, OpTuneVars> {
    private final Context mContext;

    public FetchOpTuneVarsTask(Context context) {
        mContext = context;
    }

    @Override
    protected OpTuneVars doInBackground(String... strings) {
        OpTuneVars retVal = null;
        InputStream is = null;

        try {
            URL url = new URL(strings[0]);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(10000 /* milliseconds */);
            conn.setConnectTimeout(15000 /* milliseconds */);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            is = conn.getInputStream();

            // Convert the InputStream into a string
            retVal = new OpTuneVars(is);

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (retVal == null || !retVal.isValid()) {
            // Read from local resource instead
            retVal = new OpTuneVars(mContext.getString(R.string.optune_vars_json));
        }

        return retVal;
    }

    @Override
    protected void onPostExecute(OpTuneVars vars) {
        if (vars != null && vars.isValid()) {
            Toast.makeText(mContext, mContext.getString(R.string.optune_vars_download_success), Toast.LENGTH_SHORT).show();
        }
    }
}
