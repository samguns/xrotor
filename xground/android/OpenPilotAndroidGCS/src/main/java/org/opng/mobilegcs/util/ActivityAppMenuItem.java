/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import org.opng.mobilegcs.AppMenu;

public class ActivityAppMenuItem<T extends Activity> extends AppMenuItem {

    private final Class<T> mActivityClass;

    public ActivityAppMenuItem(int id, String title, int icon, Class<T> activity) {
        super(id, title, icon);
        mActivityClass = activity;
    }

    public ActivityAppMenuItem(String title, int icon, Class<T> activity) {
        super(title, icon);
        mActivityClass = activity;
    }

    public ActivityAppMenuItem(int id, String title, Class<T> activity) {
        super(id, title);
        mActivityClass = activity;
    }

    public ActivityAppMenuItem(String title, Class<T> activity) {
        super(title);
        mActivityClass = activity;
    }

    public void Open(Context context) {
        Intent intent = new Intent(context, mActivityClass);
        intent.putExtra(AppMenu.APP_MENU_ITEM, getId());
        context.startActivity(intent);
    }
}
