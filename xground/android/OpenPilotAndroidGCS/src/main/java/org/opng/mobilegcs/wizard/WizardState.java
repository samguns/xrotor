/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.wizard;

import android.os.Parcel;
import android.os.Parcelable;

public class WizardState implements Parcelable {
    public static final Creator<WizardState> CREATOR = new Creator<WizardState>() {
        @Override
        public WizardState createFromParcel(Parcel source) {
            return new WizardState(source);
        }

        @Override
        public WizardState[] newArray(int size) {
            return new WizardState[size];
        }
    };
    private WizardState mPreviousState;

    protected WizardState() {
    }

    protected WizardState(WizardState previousState) {
        mPreviousState = previousState;
    }

    protected WizardState(Parcel in) {
        mPreviousState = in.readParcelable(getClass().getClassLoader());
    }

    public WizardState next() {
        return null;
    }

    public WizardState previous() {
        return mPreviousState;
    }

    public void process() {
    }

    public boolean isStartState() {
        return false;
    }

    public boolean isEndState() {
        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mPreviousState, flags);
    }
}
