/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Spinner;

import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.uavtalk.UAVObjectManager;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

class UAVObjectManagerLoaderHandler implements LoaderManager.LoaderCallbacks<UAVObjectManager> {
    private static final int AUTOMATIC_UPDATE_RATE = 500;
    private final StabiGuiFragment mStabilizationFragment;
    private final Button mButtonApply;
    private final Button mButtonSave;
    private final Button mButtonDefault;
    private final Button mButtonRestore;
    private final int mButtonGroup;
    private final CheckBox mCheckAutoUpdate;
    private final Spinner mStabiBank;
    private final Context mContext;
    private UAVObjectToGuiUtils mObjectToGuiUtils;
    private StabilizationGuiHelper mStabilizationGuiHelper;
    private ScheduledThreadPoolExecutor mScheduledSettingsApply;

    public UAVObjectManagerLoaderHandler(Context context, StabiGuiFragment stabilizationFragment,
                                         CheckBox checkAutoUpdate, Spinner stabiBankSpinner, Button buttonApply, Button buttonSave,
                                         Button buttonDefault, Button buttonRestore, int buttonGroup) {
        mContext = context;
        mStabilizationFragment = stabilizationFragment;
        mCheckAutoUpdate = checkAutoUpdate;
        mStabiBank = stabiBankSpinner;
        mButtonApply = buttonApply;
        mButtonSave = buttonSave;
        mButtonDefault = buttonDefault;
        mButtonRestore = buttonRestore;
        mButtonGroup = buttonGroup;
    }

    public void onResume() {
        enableObjectUpdates();
    }

    public void onStop() {
        disableObjectUpdates();
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int i, Bundle bundle) {
        return new UAVObjectLoader(mContext);
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> uavObjectManagerLoader, UAVObjectManager uavObjectManager) {

        if (uavObjectManager != null) {
            // Bind widgets to uavobjects
            mObjectToGuiUtils = new UAVObjectToGuiUtils(mContext, uavObjectManager, uavObjectManager.getObjectUtilManager());
            mObjectToGuiUtils.addApplySaveButtons(mButtonApply, mButtonSave);

            if (mButtonDefault != null) {
                mObjectToGuiUtils.addDefaultButton(mButtonDefault, mButtonGroup);
            }
            if (mButtonRestore != null) {
                mObjectToGuiUtils.addDefaultButton(mButtonRestore, mButtonGroup);
            }

            // Create stabilization gui helper and set up controls to display PID settings
            mStabilizationGuiHelper = new StabilizationGuiHelper(uavObjectManager, mObjectToGuiUtils, mButtonGroup);

            if (mStabiBank != null) {
                mStabiBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        int bankNumber = i + 1;
                        mStabilizationGuiHelper.setBankNumber(bankNumber);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                mStabiBank.setSelection(0);
            }

            initAutoApplyCheckbox();

            if (mStabilizationFragment != null) {
                mStabilizationFragment.setupGuiHandler(mStabilizationGuiHelper);
            }

            // Update widgets and objects
            enableObjectUpdates();
        }

    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> uavObjectManagerLoader) {

    }

    private void enableObjectUpdates() {
        if (mObjectToGuiUtils != null) {
            mObjectToGuiUtils.enableObjUpdates();
        }

        if (mStabilizationGuiHelper != null) {
            mStabilizationGuiHelper.refresh();
            mStabilizationGuiHelper.populateWidgets();
        }
    }

    private void disableObjectUpdates() {
        if (mObjectToGuiUtils != null) {
            mObjectToGuiUtils.disableObjUpdates();
        }
    }

    private void initAutoApplyCheckbox() {
        if (mCheckAutoUpdate != null) {
            mCheckAutoUpdate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        mScheduledSettingsApply = new ScheduledThreadPoolExecutor(1);
                        mScheduledSettingsApply.scheduleWithFixedDelay(new Runnable() {
                            @Override
                            public void run() {
                                if (mObjectToGuiUtils != null) {
                                    mObjectToGuiUtils.apply();
                                }
                            }
                        }, AUTOMATIC_UPDATE_RATE, AUTOMATIC_UPDATE_RATE, TimeUnit.MILLISECONDS);
                    } else {
                        if (mScheduledSettingsApply != null) {
                            mScheduledSettingsApply.shutdownNow();
                            mScheduledSettingsApply = null;
                        }
                    }
                }
            });
        }
    }
}