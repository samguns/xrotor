/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.flight;

import android.os.Bundle;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.ConfigBaseActivity;

public class StickExpoActivity extends ConfigBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stick_expo);
        initToolbar(R.id.toolbar);
    }
}
