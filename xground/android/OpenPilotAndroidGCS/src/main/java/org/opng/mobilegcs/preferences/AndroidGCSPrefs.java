/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.opng.mobilegcs.R;

public class AndroidGCSPrefs implements GCSPreferences {

    /**
     * Default preference values
     */
    private static final boolean DEFAULT_USAGE_STATISTICS = true;
    private static final boolean DEFAULT_OFFLINE_MAP_ENABLED = false;
    private static final String DEFAULT_MAP_TYPE = "";
    private static final boolean DEFAULT_KEEP_SCREEN_ON = false;
    private static final String DEFAULT_CONNECTION_TYPE = "";

    // Public for legacy usage
    private final SharedPreferences mPrefs;
    private final Context mContext;

    public AndroidGCSPrefs(Context context) {
        this.mContext = context;
        mPrefs = PreferenceManager.getDefaultSharedPreferences(context);

    }


    public String getMapProviderName() {
        return mPrefs.getString(mContext.getString(R.string.pref_maps_providers_key), null);
    }

    /**
     * @return the selected map type (if supported by the map provider).
     */
    public String getMapType() {
        return mPrefs.getString(mContext.getString(R.string.pref_map_type_key), DEFAULT_MAP_TYPE);
    }

    /**
     * @return true if offline map is enabled (if supported by the map
     * provider).
     */
    public boolean isOfflineMapEnabled() {
        return mPrefs.getBoolean(mContext.getString(R.string.pref_advanced_use_offline_maps_key),
                DEFAULT_OFFLINE_MAP_ENABLED);
    }

    public void setFirmwareVersion(String firmwareVersion) {
        mPrefs.edit().putString("pref_firmware_version", firmwareVersion).apply();
    }

    @Override
    public boolean isAlarmsTTSEnabled() {
        return mPrefs.getBoolean("pref_tts_armed", true);
    }

    @Override
    public boolean isArmedStateTTSEnabled() {
        return mPrefs.getBoolean("pref_tts_armed", true);
    }

    @Override
    public boolean logOnConnect() {
        return mPrefs.getBoolean("pref_logging_on_connect", false);
    }


    /**
     * @return the selected  connection type.
     */
    @Override
    public String getConnectionType() {
        return mPrefs.getString(mContext.getString(R.string.pref_connection_type_key),
                DEFAULT_CONNECTION_TYPE);
    }

    /**
     * @return true if the device screen should stay on.
     */
    @Override
    public boolean keepScreenOn() {
        return mPrefs.getBoolean(mContext.getString(R.string.pref_screen_awake_key),
                DEFAULT_KEEP_SCREEN_ON);
    }


    @Override
    public String getBluetoothDeviceAddress() {
        return mPrefs.getString("pref_bt_devices", null);
    }

    public void setBluetoothDeviceAddress(String newAddress) {
        final SharedPreferences.Editor editor = mPrefs.edit();
        editor.putString(mContext.getString(R.string.pref_bluetooth_device_address_key), newAddress)
                .apply();
    }

    @Override
    public int getComBaudSpeed() {

        String baudRate = mPrefs.getString("pref_com_speed", "57600");
        return Integer.parseInt(baudRate);
    }

    @Override
    public String getIpAddress() {
        return mPrefs.getString("pref_tcp_ip_address", "127.0.0.1");

    }

    @Override
    public int getTcpPort() {
        return mPrefs.getInt("pref_tcp_port", -1);
    }
}
