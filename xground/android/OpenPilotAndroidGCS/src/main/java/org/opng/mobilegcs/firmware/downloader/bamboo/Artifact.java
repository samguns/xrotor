/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.downloader.bamboo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.common.io.Files;

import org.opng.mobilegcs.tasks.bamboo.FetchArtifactDataTask;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Artifact {
    private final Branch mBranch;
    private final String mArtifactName;
    private final String mArtifactUrl;
    private final Context mContext;
    private ByteBuffer mBytes;

    public Artifact(Branch branch, String artifactName, String artifactUrl, Context context) {
        mBranch = branch;
        mArtifactName = artifactName;
        mArtifactUrl = artifactUrl;
        mContext = context;
    }

    private String getArtifactName() {
        return mArtifactName;
    }

    public String getArtifactUrl() {
        return mArtifactUrl;
    }

    public Plan getPlan(){
        return mBranch.getPlan();
    }

    public void setBytes(ByteBuffer bytes) {
        mBytes = bytes;
    }

    public void fetch(FetchArtifactDataTask.FetchCallback fetchCallback) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            FetchArtifactDataTask task = new FetchArtifactDataTask(fetchCallback);
            task.execute(this);
        }
    }

    public void writeToFile(File file) throws IOException {
        String artifactFileName = mArtifactUrl.substring(mArtifactUrl.lastIndexOf('/'), mArtifactUrl.length());
        File saveDirectory = new File(file, mBranch.getRevisionKey());
        boolean dirExists;
        dirExists = saveDirectory.exists() || saveDirectory.mkdirs();

        if (dirExists) {
            Files.write(mBytes.array(), new File(saveDirectory, artifactFileName));
        }
    }

    @Override
    public String toString() {
        return getArtifactName();
    }
}