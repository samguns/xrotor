/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.downloader.bamboo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.JsonReader;

import org.opng.mobilegcs.tasks.bamboo.FetchBranchDataByNameTask;
import org.opng.mobilegcs.tasks.bamboo.FetchBranchDataFromPlanTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

public class Branch {
    private final Plan mPlan;
    private final String mName;
    private final Context mContext;
    private final List<Artifact> mArtifactList = new ArrayList<>();
    private String mKey;
    private Exception mException;
    private EnumSet<Type> mType;
    private String mRevisionKey;

    /**
     * Creates an instance of this class specifically for next.
     *
     * @param plan    build plan
     * @param context application context
     */
    public Branch(Plan plan, Context context) {
        mPlan = plan;
        mName = "next";
        mContext = context;
    }

    Branch(Plan plan, String key, String name, Context context) {
        mPlan = plan;
        mKey = key;
        mName = name;
        mContext = context;
    }

    public String getKey() {
        return mKey;
    }

    public String getName() {
        return mName;
    }

    public Plan getPlan() {
        return mPlan;
    }

    public String getRevisionKey() {
        return mRevisionKey;
    }

    public List<Artifact> getArtifactList() {
        return mArtifactList;
    }

    public void setType(EnumSet<Type> type) {
        mType = type;
    }

    public Exception getException() {
        return mException;
    }

    public void fetch(FetchBranchDataFromPlanTask.FetchCallback fetchCallback) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            mArtifactList.clear();

            if (mKey != null) {
                // This Branch instance came from a Plan's branch list so we can fetch its data
                // with FetchBranchDataFromPlanTask
                FetchBranchDataFromPlanTask task = new FetchBranchDataFromPlanTask(fetchCallback);
                task.execute(this);
            } else {
                // This Branch instance was created directly from a Branch name so requires a
                // different REST resource to access its data
                FetchBranchDataByNameTask task = new FetchBranchDataByNameTask(fetchCallback);
                task.execute(this);
            }
        }
    }

    public void fromJson(InputStream is) throws UnsupportedEncodingException {
        readJson(new InputStreamReader(is, "UTF-8"));
    }

    @Override
    public String toString() {
        return mName;
    }

    private void readJson(InputStreamReader inputStreamReader) {
        JsonReader jsonReader = null;
        try {
            jsonReader = new JsonReader(inputStreamReader);
            jsonReader.beginObject();

            while (jsonReader.hasNext()) {
                String name = jsonReader.nextName();

                switch (name) {
                    case "artifacts":
                        // Expect artifacts at top level when using (Plan, String, String, Context)
                        // constructor. This corresponds to REST resource of the form
                        // <restUrl>/rest/api/latest/result/<branchKey>/latest.json?expand=artifacts
                        readArtifacts(jsonReader);
                        break;
                    case "results":
                        // Expect results at top level when using (Plan, String, Context) constructor.
                        // This corresponds to REST resource of the form
                        // <restUrl>/rest/api/latest/plan/<projectKey>-<planKey>/branch/<branchName>.json?expand=results.result.artifacts&start-index=0&max-results=1
                        readResults(jsonReader);
                        break;
                    case "vcsRevisionKey":
                        mRevisionKey = jsonReader.nextString().substring(0, 8);
                        break;
                    default:
                        jsonReader.skipValue();
                        break;
                }
            }

            jsonReader.endObject();
        } catch (IOException | NumberFormatException e) {
            mException = e;
        } finally {
            if (jsonReader != null) {
                try {
                    jsonReader.close();
                } catch (IOException e) {
                    mException = e;
                }
            }
        }
    }

    private void readArtifacts(JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            if (name.equals("artifact")) {
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    readArtifact(jsonReader);
                }
                jsonReader.endArray();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }

    private void readResults(JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            if (name.equals("result")) {
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    readResult(jsonReader);
                }
                jsonReader.endArray();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }

    private void readResult(JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            switch (name) {
                case "plan":
                    readPlan(jsonReader);
                    break;
                case "artifacts":
                    readArtifacts(jsonReader);
                    break;
                case "vcsRevisionKey":
                    mRevisionKey = jsonReader.nextString().substring(0, 8);
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }
        jsonReader.endObject();
    }

    private void readPlan(JsonReader jsonReader) throws IOException {
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            if (name.equals("key")) {
                mKey = jsonReader.nextString();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }

    private void readArtifact(JsonReader jsonReader) throws IOException {
        String artifactName = "";
        String artifactUrl = "";

        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            switch (name) {
                case "name":
                    artifactName = jsonReader.nextString();
                    break;
                case "link":
                    artifactUrl = readLink(jsonReader);
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }

        jsonReader.endObject();

        if (artifactName.length() > 0 && artifactUrl.length() > 0) {
            if ((mType.contains(Type.BOOTLOADER) && artifactName.toLowerCase().contains("bl_")) ||
                    (mType.contains(Type.BOOTLOADER_UPDATER) && artifactName.toLowerCase().contains("bu_")) ||
                    (mType.contains(Type.FIRMWARE) && artifactName.toLowerCase().contains("fw_"))) {
                mArtifactList.add(new Artifact(this, artifactName, artifactUrl, mContext));
            }
        }
    }

    private String readLink(JsonReader jsonReader) throws IOException {
        String retVal = "";
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            if (name.equals("href")) {
                retVal = jsonReader.nextString();
            } else {
                jsonReader.skipValue();
            }
        }

        jsonReader.endObject();

        return retVal;
    }

    public enum Type {
        FIRMWARE,
        BOOTLOADER,
        BOOTLOADER_UPDATER
    }
}
