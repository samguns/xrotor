/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.txpid;

import android.os.Handler;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;

import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.mobilegcs.util.UAVObjectToGuiUtilsEventListenerAdapter;
import org.opng.mobilegcs.util.Variant;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.HwSettings;
import org.opng.uavtalk.uavobjects.TxPIDSettings;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class TxPidGuiHelper {
    private final Switch mSwitchEnable;
    private final UAVObjectToGuiUtils mObjectToGuiUtils;
    private final Handler mHandler;
    private final List<TxPidGuiHelperEvent> mEventList = new CopyOnWriteArrayList<>();
    private HwSettings mHwSettings;
    private TxPIDSettings mTxPIDObject;
    private int mTxPIDElementIdx;

    public TxPidGuiHelper(UAVObjectManager uavObjectManager, UAVObjectToGuiUtils objectToGuiUtils, Switch enableSwitch) {
        mObjectToGuiUtils = objectToGuiUtils;
        mSwitchEnable = enableSwitch;
        mHandler = new Handler();

        init(uavObjectManager);
    }

    private void init(UAVObjectManager uavObjectManager) {
        mSwitchEnable.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (!b) {
                    // We've just indicated that we want to disable the TxPID module
                    // so we want to disable saving of the TxPIDSettings object otherwise
                    // when upload or save are clicked, the code will attempt to update
                    // the TxPIDSettings values after the corresponding firmware module
                    // has been disabled.
                    disableTxPidSave();
                } else {
                    enableTxPidSave();
                }
            }
        });

        // This event handler will listen to before/after save events
        // to update the save enabled state of the TxPIDSettings object:
        // if the TxPID module is disabled, we will disable saves of the
        // TxPIDSettings object. If the TxPID module is enabled, we will
        // enable saving of the TxPIDSettings object.
        mObjectToGuiUtils.addEventListener(new UAVObjectToGuiUtilsEventListenerAdapter() {
            @Override
            public void beforeSave() {
                updateTxPidSaveEnablement();
            }

            @Override
            public void afterSave() {
                updateTxPidSaveEnablement();
            }
        });

        // Get objects we need
        mHwSettings = (HwSettings) uavObjectManager.getObject("HwSettings");
        mTxPIDObject = (TxPIDSettings) uavObjectManager.getObject("TxPIDSettings");

        // Wire up enabled switch
        mHwSettings.addUAVObjectEventListener(new UAVObjectEventAdapter() {
            @Override
            public void objectUpdated(UAVObject obj) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshValues();
                    }
                });
                fireHardwareSettingsChanged();
            }
        });

        // Get index of field value for TxPID enabled state
        UAVObjectField optionalModulesField = mHwSettings.getField(HwSettings.FIELD_OPTIONALMODULES);
        mTxPIDElementIdx = optionalModulesField.getElementNames().indexOf(HwSettings.OPTIONALMODULES_TXPID);

        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mHwSettings, optionalModulesField,
                mSwitchEnable, mTxPIDElementIdx, 1, false, null, 0, new String[]{HwSettings.OPTIONALMODULES_ENABLED, HwSettings.OPTIONALMODULES_DISABLED});
        mHwSettings.requestUpdate();
        mTxPIDObject.requestUpdate();
    }

    private void fireHardwareSettingsChanged() {
        for (TxPidGuiHelperEvent curEvent : mEventList) {
            curEvent.hardwareSettingsChanged();
        }
    }

    public void refreshValues() {
        mSwitchEnable.setChecked(isEnabledTxPIDModule());
    }

    private boolean isEnabledTxPIDModule() {
        UAVObjectField optionalModulesField = mHwSettings.getField(HwSettings.FIELD_OPTIONALMODULES);
        String txPidEnabled = optionalModulesField.getValue(mTxPIDElementIdx).toString();
        return txPidEnabled.equals(HwSettings.OPTIONALMODULES_ENABLED);
    }

    private void updateTxPidSaveEnablement() {
        if (isEnabledTxPIDModule() && mSwitchEnable.isChecked()) {
            enableTxPidSave();
        } else {
            disableTxPidSave();
        }
    }

    private void enableTxPidSave() {
        if (mObjectToGuiUtils != null) {
            mObjectToGuiUtils.enableSavesForObject(mTxPIDObject);
        }
    }

    private void disableTxPidSave() {
        if (mObjectToGuiUtils != null) {
            mObjectToGuiUtils.disableSavesForObject(mTxPIDObject);
        }
    }

    public void bindBankNumberWidget(View viewToBind) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_BANKNUMBER),
                viewToBind, 0, 1, true, null, 0);
    }

    public void bindPidWidgets(View viewPidOption, View viewControlSource, View viewMinValue, View viewMaxValue, int instance) {
        if (viewPidOption != null) {
            mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_PIDS),
                    viewPidOption, instance, 1, true, null, 0);
        }
        if (viewControlSource != null) {
            mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_INPUTS),
                    viewControlSource, instance, 1, true, null, 0);
        }
        if (viewMinValue != null) {
            mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_MINPID),
                    viewMinValue, instance, 1, false, null, 0);
        }
        if (viewMaxValue != null) {
            mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_MAXPID),
                    viewMaxValue, instance, 1, false, null, 0);
        }
    }

    public void setPIDOption(String pidOption, int index) {
        if (mTxPIDObject != null) {
            mTxPIDObject.getField(TxPIDSettings.FIELD_PIDS).setValue(new Variant(pidOption), index);
        }
    }

    public void saveChangesToRam() {
        if (mTxPIDObject != null) {
            mObjectToGuiUtils.saveObjectToRam(mTxPIDObject);
        }
    }

    public void addEventListener(TxPidGuiHelperEvent listener) {
        mEventList.add(listener);
    }

    public void removeEventListener(TxPidGuiHelperEvent listener) {
        mEventList.remove(listener);
    }


    void bindThrottleRangeWidgets(View viewMinThrottle, View viewMaxThrottle) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_THROTTLERANGE),
                viewMinThrottle, 0, 1, false, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_THROTTLERANGE),
                viewMaxThrottle, 1, 1, false, null, 0);
    }

    void bindUpdateModeWidget(View viewUpdateMode) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mTxPIDObject, mTxPIDObject.getField(TxPIDSettings.FIELD_UPDATEMODE),
                viewUpdateMode, 0, 1, true, null, 0);
    }

    public interface TxPidGuiHelperEvent {
        void hardwareSettingsChanged();
    }
}