/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.objectbrowser;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import org.opng.mobilegcs.R;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * This class is used to bind UAVObjects to an ExpandableList. The list will have two
 * groups: data and settings. When each group is expanded it will show the associated
 * UAVObjects.
 */
class UAVObjectExpandableListAdapter extends BaseExpandableListAdapter {
    private final LayoutInflater _inflater;
    private final int _groupClosedView;
    private final int _groupClosedTextId;
    private final int _groupExpandedView;
    private final int _groupExpandedTextId;
    private final int _childView;
    private final int _childViewTextId;
    private final List<Map.Entry<String, List<UAVObject>>> _objects = new ArrayList<>();
    private final ArrayList<UAVObject> _settingsObjects = new ArrayList<>();
    private final ArrayList<UAVObject> _dataObjects = new ArrayList<>();

    /**
     * Construct an instance of this class with the given context and the resource ids
     * for the expanded and collapsed group views and the child views that will be show
     * for an expanded group.
     *
     * @param context           application's context, usually the owning Activity
     * @param groupClosedView   resource id of the view that will show a collapsed group item
     * @param groupExpandedView resource id of the view that will show an expanded group item
     * @param childView         resource id of the view that will show a child item
     */
    public UAVObjectExpandableListAdapter(Context context, int groupClosedView, int groupClosedTextId, int groupExpandedView,
                                          int groupExpandedTextId, int childView, int childViewTextId) {
        _groupClosedView = groupClosedView;
        _groupClosedTextId = groupClosedTextId;
        _groupExpandedView = groupExpandedView;
        _groupExpandedTextId = groupExpandedTextId;
        _childView = childView;
        _childViewTextId = childViewTextId;
        Map.Entry<String, List<UAVObject>> settings =
                new AbstractMap.SimpleEntry<String, List<UAVObject>>(
                        context.getString(R.string.uavobject_list_settings_group), _settingsObjects);
        Map.Entry<String, List<UAVObject>> data =
                new AbstractMap.SimpleEntry<String, List<UAVObject>>(
                        context.getString(R.string.uavobject_list_data_group), _dataObjects);
        _objects.add(settings);
        _objects.add(data);

        _inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Add the given {@link UAVObject} to the adapter. The obect will be
     * added to either the settings or data group as appropriate.
     *
     * @param object object to add to adapter
     */
    public void addUAVObject(UAVObject object) {
        if (object instanceof UAVDataObject && ((UAVDataObject) object).isSettings()) {
            _settingsObjects.add(object);
        } else {
            _dataObjects.add(object);
        }

        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        return _objects.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return _objects.get(groupPosition).getValue().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return _objects.get(groupPosition).getKey();
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return _objects.get(groupPosition).getValue().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return Integer.valueOf(groupPosition).longValue();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return Integer.valueOf(childPosition).longValue();
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View retVal;
        TextView text;

        // Create or recycle our group view
        if (convertView == null || convertView.getId() ==
                (isExpanded ? getGroupExpandedView() : getGroupClosedView())) {
            // something has changed, update.
            retVal = _inflater.inflate(isExpanded ? getGroupExpandedView() :
                    getGroupClosedView(), parent, false);
        } else {
            retVal = convertView;
        }

        // Find the requested text view and set it's text to the group name
        if (retVal != null) {
            int textItemId = isExpanded ? _groupExpandedTextId : _groupClosedTextId;
            if (textItemId == 0) {
                // Assume the view given is just a text view, no layout
                text = (TextView) retVal;
            } else {
                text = (TextView) retVal.findViewById(textItemId);
            }

            if (text != null) {
                text.setText(getObjects().get(groupPosition).getKey());
            }
        }

        return retVal;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View retVal;
        TextView text;

        // Create or recycle our child view
        if (convertView == null) {
            // create
            retVal = _inflater.inflate(getChildView(), parent, false);
        } else {
            retVal = convertView;
        }

        // Find the requested text item and set its text to the UAVObject's name
        if (retVal != null) {
            UAVObject uavObject = getObjects().get(groupPosition).getValue().get(childPosition);
            if (_childViewTextId == 0) {
                // Assume the given child view is just a text view, no layout
                text = (TextView) retVal;
            } else {
                text = (TextView) retVal.findViewById(_childViewTextId);
            }

            if (text != null && uavObject != null) {
                text.setText(uavObject.getName());
            }
        }

        return retVal;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public boolean isEmpty() {
        return _settingsObjects.isEmpty() && _dataObjects.isEmpty();
    }

    public void clear() {
        for (Map.Entry<String, List<UAVObject>> curEntry : _objects) {
            curEntry.getValue().clear();
        }
    }

    private List<Map.Entry<String, List<UAVObject>>> getObjects() {
        return _objects;
    }

    private int getGroupClosedView() {
        return _groupClosedView;
    }

    private int getGroupExpandedView() {
        return _groupExpandedView;
    }

    private int getChildView() {
        return _childView;
    }
}
