/*
 * Eric Henaff Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.opng.mobilegcs.AppMenu;
import org.opng.mobilegcs.R;

public class ItemListFragment extends BaseFragment {

    private static final String STATE_CURRENT_SELECTION = "StatueCurrentSelection";

    private boolean mTwoPane;
    private ListView mList;
    private int mCurrentSelection = 0;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_CURRENT_SELECTION, mCurrentSelection);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        if (view.findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-large and
            // res/values-sw600dp). If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        mList = (ListView) view.findViewById(R.id.item_list);

        mList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                mList.setItemChecked(position, true);
                mCurrentSelection = position;
                onItemSelected(mMenu.getSubMenu().get(position));
            }
        });

        mList.setAdapter(new ArrayAdapter<>(
                getActivity(),
                R.layout.flow_detail_item,
                R.id.itemText,
                mMenu.getSubMenu()));

        if (savedInstanceState != null)
            mCurrentSelection = savedInstanceState.getInt(STATE_CURRENT_SELECTION, 0);

        if (mTwoPane) {
            mList.setSelection(mCurrentSelection);
            mList.setItemChecked(mCurrentSelection, true);
            final AppMenuItem menuItem = mMenu.getSubMenu().get(mCurrentSelection);
            if (!(menuItem instanceof ActivityAppMenuItem)) {
                onItemSelected(menuItem);
            }
        }

        return view;
    }

    private void onItemSelected(AppMenuItem menuItem) {

        if (menuItem instanceof ActivityAppMenuItem) {
            ((ActivityAppMenuItem) menuItem).Open(getContext());
        } else {
            if (mTwoPane) {
                // In two-pane mode, show the detail view in this activity by
                // adding or replacing the detail fragment using a
                // fragment transaction.
                Bundle arguments = new Bundle();
                arguments.putInt(AppMenu.APP_MENU_ITEM, menuItem.getId());

                if (menuItem instanceof BaseFragmentAppMenuItem) {
                    ((BaseFragmentAppMenuItem) menuItem).Open(getActivity().getSupportFragmentManager(), R.id.item_detail_container);
                }
            } else {
                // In single-pane mode, simply start the detail activity
                // for the selected item ID.
                Intent detailIntent = new Intent(this.getActivity(), ItemDetailActivity.class);
                detailIntent.putExtra(AppMenu.APP_MENU_ITEM, menuItem.getId());
                startActivity(detailIntent);
            }
        }
    }
}
