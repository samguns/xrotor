/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.flightmodes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.mobilegcs.views.HorizontalNumberPickerView;
import org.opng.uavtalk.UAVObjectManager;

public class FlightModesSelectionFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {

    private View mRootView;
    private HorizontalNumberPickerView mNbFlightModes;
    private UAVObjectToGuiUtils mUAVObjectToGuiUtils;
    private Button mButtonApply;
    private Button mButtonSave;

    public FlightModesSelectionFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_flight_modes_selection, container, false);

        getLoaderManager().initLoader(0, null, this);

        mNbFlightModes = (HorizontalNumberPickerView) mRootView.findViewById(R.id.textNbFlightModes);
        mButtonApply = (Button) mRootView.findViewById(R.id.buttonApply);
        mButtonSave = (Button) mRootView.findViewById(R.id.buttonSave);

        return mRootView;
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int id, Bundle args) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> loader, UAVObjectManager uavObjectManager) {
        if (uavObjectManager != null) {
            // Bind widgets to uavobjects
            mUAVObjectToGuiUtils = new UAVObjectToGuiUtils(getActivity(), uavObjectManager, uavObjectManager.getObjectUtilManager());
            mUAVObjectToGuiUtils.addApplySaveButtons(mButtonApply, mButtonSave);

            FlightModesGuiHelper flightModesGuiHelper = new FlightModesGuiHelper(uavObjectManager, mUAVObjectToGuiUtils);

            flightModesGuiHelper.bindNbFlightModes(mNbFlightModes);

            flightModesGuiHelper.bindFlightPosWidgets(1,
                    mRootView.findViewById(R.id.spinnerFlightMode1),
                    mRootView.findViewById(R.id.spinnerBank1),
                    mRootView.findViewById(R.id.spinnerAssist1));

            flightModesGuiHelper.bindFlightPosWidgets(2,
                    mRootView.findViewById(R.id.spinnerFlightMode2),
                    mRootView.findViewById(R.id.spinnerBank2),
                    mRootView.findViewById(R.id.spinnerAssist2));

            flightModesGuiHelper.bindFlightPosWidgets(3,
                    mRootView.findViewById(R.id.spinnerFlightMode3),
                    mRootView.findViewById(R.id.spinnerBank3),
                    mRootView.findViewById(R.id.spinnerAssist3));

            flightModesGuiHelper.bindFlightPosWidgets(4,
                    mRootView.findViewById(R.id.spinnerFlightMode4),
                    mRootView.findViewById(R.id.spinnerBank4),
                    mRootView.findViewById(R.id.spinnerAssist4));

            flightModesGuiHelper.bindFlightPosWidgets(5,
                    mRootView.findViewById(R.id.spinnerFlightMode5),
                    mRootView.findViewById(R.id.spinnerBank5),
                    mRootView.findViewById(R.id.spinnerAssist5));

            flightModesGuiHelper.bindFlightPosWidgets(6,
                    mRootView.findViewById(R.id.spinnerFlightMode6),
                    mRootView.findViewById(R.id.spinnerBank6),
                    mRootView.findViewById(R.id.spinnerAssist6));

            mUAVObjectToGuiUtils.populateViews();

            // Update widgets and objects
            enableObjectUpdates();

            flightModesGuiHelper.refreshValues();
        }
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> loader) {

    }

    @Override
    public void onResume() {
        super.onResume();
        enableObjectUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        disableObjectUpdates();
    }

    private void enableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.enableObjUpdates();
        }
    }

    private void disableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.disableObjUpdates();
        }
    }
}
