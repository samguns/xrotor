/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opng.mobilegcs.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicStabiRateFragment extends Fragment implements StabiGuiFragment {


    private View mRootView;

    public BasicStabiRateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_basic_stabi_rate, container, false);

        return mRootView;
    }


    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        helper.bindPitchRatePidWidgets(mRootView.findViewById(R.id.seekBasicPitchRateKp), 0.0001,
                mRootView.findViewById(R.id.seekBasicPitchRateKi), 0.0001, null, 1);
        helper.bindPitchRatePidWidgets(mRootView.findViewById(R.id.textBasicPitchRateKp), 0.0001,
                mRootView.findViewById(R.id.textBasicPitchRateKi), 0.0001, null, 1);
        helper.bindRollRatePidWidgets(mRootView.findViewById(R.id.seekBasicRollRateKp), 0.0001,
                mRootView.findViewById(R.id.seekBasicRollRateKi), 0.0001, null, 1);
        helper.bindRollRatePidWidgets(mRootView.findViewById(R.id.textBasicRollRateKp), 0.0001,
                mRootView.findViewById(R.id.textBasicRollRateKi), 0.0001, null, 1);
        helper.bindYawRatePidWidgets(mRootView.findViewById(R.id.seekBasicYawRateKp), 0.0001,
                mRootView.findViewById(R.id.seekBasicYawRateKi), 0.0001, null, 1);
        helper.bindYawRatePidWidgets(mRootView.findViewById(R.id.textBasicYawRateKp), 0.0001,
                mRootView.findViewById(R.id.textBasicYawRateKi), 0.0001, null, 1);
    }
}
