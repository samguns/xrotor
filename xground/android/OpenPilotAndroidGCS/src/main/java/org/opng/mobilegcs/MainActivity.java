/*
 * Eric Henaff Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import org.opng.mobilegcs.util.ActivityAppMenuItem;
import org.opng.mobilegcs.util.AppMenuItem;
import org.opng.mobilegcs.util.AppMenuItemFragmentOperations;
import org.opng.mobilegcs.util.BaseActivity;
import org.opng.mobilegcs.util.BaseFragmentAppMenuItem;
import org.opng.mobilegcs.util.FragmentAppMenuItem;

public class MainActivity extends BaseActivity {

    private DrawerLayout mDrawer;
    private Button mConnectButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (BuildConfig.USE_FABRIC_IO) {
            Fabric.with(this, new Crashlytics());
        }

        setContentView(R.layout.activity_main);
        AppMenu.getMenu(this);

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        initToolbar(R.id.toolbar);

        final ActionBar ab = getSupportActionBar();
        if (ab != null) {
            ab.setHomeAsUpIndicator(R.drawable.opicon);
        }

        final NavigationView navView = (NavigationView) findViewById(R.id.nvView);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                menuItem.setChecked(true);
                mDrawer.closeDrawers();
                onNavigationDrawerItemSelected(AppMenu.getMenuItem(menuItem.getItemId()));
                return true;
            }
        });

        View navHeader = navView.inflateHeaderView(R.layout.nav_header);

        mConnectButton = (Button) navHeader.findViewById(R.id.nav_header_connect);
        mConnectButton.setText(isConnected() ? getString(R.string.title_action_disconnect) : getString(R.string.title_action_connect));
        mConnectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isConnected()) {
                    doDisconnect();
                    mConnectButton.setText(getString(R.string.title_action_connect));
                } else {
                    doConnect();
                    mConnectButton.setText(getString(R.string.title_action_disconnect));
                }
                mDrawer.closeDrawers();
            }
        });

        if (mMenu == null) {
            mMenu = AppMenu.getMenu(this).getSubMenu().get(0);
        }

        navView.setCheckedItem(mMenu.getId());
        onNavigationDrawerItemSelected(mMenu);
    }

    private void onNavigationDrawerItemSelected(AppMenuItem menuItem) {
        // update the main content by replacing fragments

        if (menuItem instanceof BaseFragmentAppMenuItem ||
                menuItem instanceof FragmentAppMenuItem) {
            FragmentManager fragmentManager = getSupportFragmentManager();

            mMenu = menuItem;
            setTitle(menuItem.mTitle);

            ((AppMenuItemFragmentOperations) menuItem).Open(fragmentManager, R.id.container);
        } else if (menuItem instanceof ActivityAppMenuItem) {
            ((ActivityAppMenuItem) menuItem).Open(this);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (mDrawer != null) {
            switch (item.getItemId()) {
                case android.R.id.home:
                    mDrawer.openDrawer(GravityCompat.START);
                    return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onTelemetryConnected() {
        if (mConnectButton != null) {
            mConnectButton.setText(getString(R.string.title_action_disconnect));
        }
    }

    @Override
    protected void onTelemetryDisconnected() {
        if (mConnectButton != null) {
            mConnectButton.setText(getString(R.string.title_action_connect));
        }
    }
}
