/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.util;

import java.util.EventListener;

interface SmartSaveEventListener extends EventListener {
    void preProcessOperations();

    void saveSuccessful();

    void beginOp();

    void endOp();
}
