/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.flightmodes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.uavtalk.UAVObjectManager;

public class FlightModesDetailsFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {

    private UAVObjectToGuiUtils mUAVObjectToGuiUtils;
    private View mRootView;
    private Button mButtonApply;
    private Button mButtonsave;

    public FlightModesDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_flight_modes_details, container, false);

        getLoaderManager().initLoader(0, null, this);

        mButtonApply = (Button) mRootView.findViewById(R.id.buttonApply);
        mButtonsave = (Button) mRootView.findViewById(R.id.buttonSave);

        return mRootView;
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int id, Bundle args) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> loader, UAVObjectManager uavObjectManager) {
        if (uavObjectManager != null) {
            // Bind widgets to uavobjects
            mUAVObjectToGuiUtils = new UAVObjectToGuiUtils(getActivity(), uavObjectManager, uavObjectManager.getObjectUtilManager());
            mUAVObjectToGuiUtils.addApplySaveButtons(mButtonApply, mButtonsave);

            FlightModesGuiHelper flightModesGuiHelper = new FlightModesGuiHelper(uavObjectManager, mUAVObjectToGuiUtils);

            flightModesGuiHelper.bindFlightModeSettings(1,
                    mRootView.findViewById(R.id.spinnerRoll1),
                    mRootView.findViewById(R.id.spinnerPitch1),
                    mRootView.findViewById(R.id.spinnerYaw1),
                    mRootView.findViewById(R.id.spinnerThrust1));

            flightModesGuiHelper.bindFlightModeSettings(2,
                    mRootView.findViewById(R.id.spinnerRoll2),
                    mRootView.findViewById(R.id.spinnerPitch2),
                    mRootView.findViewById(R.id.spinnerYaw2),
                    mRootView.findViewById(R.id.spinnerThrust2));

            flightModesGuiHelper.bindFlightModeSettings(3,
                    mRootView.findViewById(R.id.spinnerRoll3),
                    mRootView.findViewById(R.id.spinnerPitch3),
                    mRootView.findViewById(R.id.spinnerYaw3),
                    mRootView.findViewById(R.id.spinnerThrust3));

            flightModesGuiHelper.bindFlightModeSettings(4,
                    mRootView.findViewById(R.id.spinnerRoll4),
                    mRootView.findViewById(R.id.spinnerPitch4),
                    mRootView.findViewById(R.id.spinnerYaw4),
                    mRootView.findViewById(R.id.spinnerThrust4));

            flightModesGuiHelper.bindFlightModeSettings(5,
                    mRootView.findViewById(R.id.spinnerRoll5),
                    mRootView.findViewById(R.id.spinnerPitch5),
                    mRootView.findViewById(R.id.spinnerYaw5),
                    mRootView.findViewById(R.id.spinnerThrust5));

            flightModesGuiHelper.bindFlightModeSettings(6,
                    mRootView.findViewById(R.id.spinnerRoll6),
                    mRootView.findViewById(R.id.spinnerPitch6),
                    mRootView.findViewById(R.id.spinnerYaw6),
                    mRootView.findViewById(R.id.spinnerThrust6));

            mUAVObjectToGuiUtils.populateViews();

            // Update widgets and objects
            enableObjectUpdates();

            flightModesGuiHelper.refreshValues();
        }
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> loader) {

    }

    @Override
    public void onResume() {
        super.onResume();
        enableObjectUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        disableObjectUpdates();
    }

    private void enableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.enableObjUpdates();
        }
    }

    private void disableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.disableObjUpdates();
        }
    }
}
