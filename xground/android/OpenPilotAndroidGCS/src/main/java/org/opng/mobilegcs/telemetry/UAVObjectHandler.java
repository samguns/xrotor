/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

import android.content.Context;
import android.os.Build;
import android.os.Handler;
import android.speech.tts.TextToSpeech;
import android.util.Log;
import android.widget.Toast;

import org.opng.mobilegcs.BuildConfig;
import org.opng.mobilegcs.R;
import org.opng.mobilegcs.preferences.AndroidGCSPrefs;
import org.opng.mobilegcs.util.UAVObjectUtilManager;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectEventListener;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.FirmwareIAPObj;
import org.opng.uavtalk.uavobjects.FlightStatus;
import org.opng.uavtalk.uavobjects.HomeLocation;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

class UAVObjectHandler {

    private static final String FLIGHT_STATUS = "FlightStatus";
    private static final String HOME_LOCATION = "HomeLocation";
    private static final String FIRMWARE_IAP_OBJ = "FirmwareIAPObj";

    private static final String ARMING = "Arming";
    private static final int LOGLEVEL = 1;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean INFO = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    // Logging settings
    private final String TAG = LoggingHandler.class.getSimpleName();
    private final List<UAVObject> mListenersList = new ArrayList<>();
    private Handler mHandler;
    private Context mContext;
    private TextToSpeech mTextToSpeech;
    private String mArmedStatus = "";
    private UAVObjectManager mObjectManager = null;
    private UAVObjectUtilManager mObjUtilManager = null;
    private AndroidGCSPrefs mAppPrefs;
    private final UAVObjectEventListener mObjUpdatedListener = new UAVObjectEventAdapter() {
        @Override
        public void objectUpdated(final UAVObject obj) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    processObjectUpdates(obj);
                }
            });
        }
    };

    private static boolean isTTSUserEnabled() {
        return true;
    }

    private void processObjectUpdates(UAVObject obj) {
        if (DEBUG) Log.d(TAG, "processObjectUpdates : " + obj.getName());

        String objectName = obj.getName();

        switch (objectName) {
            case FLIGHT_STATUS:

                handleFlightStatusEvent(obj);
                break;
            case FIRMWARE_IAP_OBJ:
                handleFirmwareIapObj(obj);

                break;
        }


    }

    /***
     * *
     * This looks like a binary with a description at the end:
     * 4 bytes: header: "OpFw".
     * 4 bytes: GIT commit tag (short version of SHA1).
     * 4 bytes: Unix timestamp of compile time.
     * 2 bytes: target platform. Should follow same rule as BOARD_TYPE and BOARD_REVISION in board define files.
     * 26 bytes: commit tag if it is there, otherwise branch name. '-dirty' may be added if needed. Zero-padded.
     * 20 bytes: SHA1 sum of the firmware.
     * 20 bytes: SHA1 sum of the uavo definitions.
     * 20 bytes: free for now.
     *
     * @param obj firmware IAP object instance
     */
    private void handleFirmwareIapObj(UAVObject obj) {

        FirmwareIAPObj firmwareIAPObj = (FirmwareIAPObj) obj;

        UAVObjectField desc = firmwareIAPObj.getField(FirmwareIAPObj.FIELD_DESCRIPTION);
        if(desc != null){

            firmwareIAPObj.removeUAVObjectEventListener(mObjUpdatedListener);

            final int GIT_COMMIT_TAG_SIZE = 4;
            final int GIT_COMMIT_TAG_NAME_SIZE = 26;

            String commitHash = "";
            String gitTag = "";

            for (int a = 0; a < GIT_COMMIT_TAG_SIZE; a++) {
                // Prepending each value here as hash is stored little endian
                commitHash = String.format("%02x", desc.getValue(4 + a).toByte()) + commitHash;
            }

            mAppPrefs.setFirmwareVersion(commitHash);

            for (int a = 0; a < GIT_COMMIT_TAG_NAME_SIZE; a++) {
                int t = desc.getValue(a + 14).toByte();
                gitTag += String.format("%c", t);
            }

            // replace junk characters
            gitTag = gitTag.replaceAll("\\p{C}", "");

            if (!commitHash.equals(BuildConfig.UAVO_HASH)) {
                Toast.makeText(mContext, R.string.uavo_mismatch_error, Toast.LENGTH_SHORT).show();
            }

            Log.d(TAG, "drone uavoTag = " + commitHash);
            Log.d(TAG, "build uavoTag = " + BuildConfig.UAVO_HASH);
            Log.d(TAG, "git tag = " + gitTag);
        }
    }

    private void handleFlightStatusEvent(UAVObject obj) {

        String armedStatus = obj.getField(FlightStatus.FIELD_ARMED).getValue().toString();

        // if user pref is enabled
        if (mAppPrefs.isArmedStateTTSEnabled()) {

            // speak on changes
            if (!mArmedStatus.equals(armedStatus)) {
                speakText(armedStatus);
                mArmedStatus = armedStatus;
            }
        }

        /**
         * For now, set HomeLocation to FALSE on ArmingState, this will cause HomeLocation to be set
         * at the current location. This is needed for playback in logs, the map in GCS uses NED
         * locations.
         */
        if (armedStatus.equals(ARMING)) {

            if(INFO) Log.i(TAG, "Arming state, setting HomeLocation!");
            UAVObject homeLocationObject = mObjectManager.getObject(HOME_LOCATION);
            if(homeLocationObject != null) {
                UAVObjectField setField = homeLocationObject.getField(HomeLocation.FIELD_SET);
                setField.setValue(HomeLocation.SET_FALSE);

                mObjUtilManager.saveObjectToSD((UAVDataObject) homeLocationObject);
            }
        }
    }

    public void connect(UAVObjectManager objManager, Context context) {
        // We should be on our activity's thread here, get a handler for it
        this.mHandler = new Handler();
        this.mContext = context;
        this.mObjectManager = objManager;

        mAppPrefs = new AndroidGCSPrefs(context);

        if (objManager == null)
            return;

        mTextToSpeech = new TextToSpeech(context, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    mTextToSpeech.setLanguage(Locale.getDefault());
                }
            }
        }
        );
        this.mObjUtilManager = objManager.getObjectUtilManager();

        // Register all existing objects
        List<List<UAVObject>> objects = objManager.getObjects();
        for (int i = 0; i < objects.size(); i++)
            for (int j = 0; j < objects.get(i).size(); j++) {
                UAVObject object = objects.get(i).get(j);
                registerObject(object);
            }
    }

    public void disconnect() {
        unregisterAllObjects();

        if (mTextToSpeech != null) {
            mTextToSpeech.stop();
            mTextToSpeech.shutdown();
        }
    }

    private void registerObject(UAVObject obj) {
        synchronized (mListenersList) {
            if (!mListenersList.contains(obj)) {

                obj.addUAVObjectEventListener(mObjUpdatedListener);
                mListenersList.add(obj);
            }
        }
    }

    // ! Unregister all objects from logging
    private void unregisterAllObjects() {
        synchronized (mListenersList) {
            for (int i = 0; i < mListenersList.size(); i++) {
                mListenersList.get(i).removeUAVObjectEventListener(mObjUpdatedListener);
            }
            mListenersList.clear();
        }
    }

    private void speakText(String text) {
        if (isTTSUserEnabled() && mTextToSpeech != null) {
            //Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null, "");
            } else {
                //noinspection deprecation
                mTextToSpeech.speak(text, TextToSpeech.QUEUE_FLUSH, null);
            }
        }
    }

}
