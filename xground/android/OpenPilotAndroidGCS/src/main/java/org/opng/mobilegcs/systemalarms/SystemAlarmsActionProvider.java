/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.systemalarms;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.telemetry.UAVTalkActionProvider;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectEventListener;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.SystemAlarms;


public class SystemAlarmsActionProvider extends ActionProvider implements UAVTalkActionProvider {
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final String SYSTEM_ALARMS_WIDGET_STATE_DRAWABLE_ID = "SysAlarmsWidgetStateDrawableId";
    private static boolean WARN = LOGLEVEL > 0;
    // Logging setup
    private final String TAG = "SysAlmsActionProvider";
    private final Context mContext;
    private final Handler mHandler;
    private boolean mConnected;
    private AlarmStatus mCurrentAlarmState = AlarmStatus.UNINITIALISED;
    private AlarmStatus mCurrentAlarmStateInitialValue = AlarmStatus.UNINITIALISED;
    private UAVObject mSysAlarmsObject;
    private ImageView mSysAlarmIndicator;
    private final UAVObjectEventListener mSysAlarmsListener = new UAVObjectEventAdapter() {
        @Override
        public void objectUpdated(final UAVObject obj) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    updateAlarmIndicator(obj);
                }
            });
        }
    };

    /**
     * Creates a new instance. ActionProvider classes should always implement a
     * constructor that takes a single Context parameter for inflating from menu XML.
     *
     * @param context Context for accessing resources.
     */
    public SystemAlarmsActionProvider(Context context) {
        super(context);
        mContext = context;

        // We should be on our activity's thread here, get a handler for it
        mHandler = new Handler();
    }

    @Override
    public View onCreateActionView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View retVal = inflater.inflate(R.layout.actionbar_system_alarms, null);

        mSysAlarmIndicator = (ImageView) retVal.findViewById(R.id.imageViewAlarm);

        retVal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mConnected) {
                    Intent runSysAlarmsActivity = new Intent(mContext, SystemAlarmsActivity.class);
                    mContext.startActivity(runSysAlarmsActivity);
                }
            }
        });

        return retVal;
    }

    @Override
    public void connected(UAVObjectManager objectManager) {
        mConnected = true;
        if (objectManager != null) {
            initSysAlarms(objectManager);
        }
    }

    @Override
    public void pause(boolean telemetryIsRunning) {
        cleanupSysAlarms(telemetryIsRunning);
    }

    @Override
    public void resume() {
        initSysAlarms(null);
    }

    @Override
    public void disconnected() {
        mConnected = false;
        cleanupSysAlarms(false);

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                setAlarmStatus(AlarmStatus.UNINITIALISED);
            }
        });
    }

    @Override
    public void saveState(Bundle outState) {
        if (DEBUG) Log.d(TAG, "Saving alarm state: " + mCurrentAlarmState.toString());
        outState.putString(SYSTEM_ALARMS_WIDGET_STATE_DRAWABLE_ID, mCurrentAlarmState.toString());
    }

    @Override
    public void restoreState(Bundle savedInstanceState) {
        String restoredAlarmState = savedInstanceState.getString(SYSTEM_ALARMS_WIDGET_STATE_DRAWABLE_ID);
        if (DEBUG) Log.d(TAG, "Restoring alarm state: " + restoredAlarmState);
        mCurrentAlarmStateInitialValue =
                AlarmStatus.fromString(restoredAlarmState);
        setInitialValues();
    }

    @Override
    public void setInitialValues() {
        mCurrentAlarmState = mCurrentAlarmStateInitialValue;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                setAlarmStatus(mCurrentAlarmStateInitialValue);
            }
        });
    }

    private void setAlarmStatus(AlarmStatus status) {
        int drawableId = 0;

        switch (status) {
            case CRITICAL:
                drawableId = R.drawable.ic_alarm_critical;
                break;
            case ERROR:
                drawableId = R.drawable.ic_alarm_error;
                break;
            case WARNING:
                drawableId = R.drawable.ic_alarm_warn;
                break;
            case OK:
                drawableId = R.drawable.ic_alarm_ok;
                break;
            case UNINITIALISED:
                drawableId = R.drawable.ic_alarm_none;
                break;
            default:
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mSysAlarmIndicator.setImageDrawable(mContext.getResources().getDrawable(drawableId, null));
        } else {
            //noinspection deprecation
            mSysAlarmIndicator.setImageDrawable(mContext.getResources().getDrawable(drawableId));
        }
    }

    private void initSysAlarms(UAVObjectManager objectManager) {
        if (objectManager != null) {
            mSysAlarmsObject = objectManager.getObject("SystemAlarms");
        }

        if (mSysAlarmsObject != null) {
            mSysAlarmsObject.addUAVObjectEventListener(mSysAlarmsListener);

            // Alarms only update when an alarm changes so we need to get the
            // initial value here.
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    updateAlarmIndicator(mSysAlarmsObject);
                    mCurrentAlarmStateInitialValue = mCurrentAlarmState;
                }
            });
        }
    }

    private void cleanupSysAlarms(boolean telemetryIsRunning) {
        if (mSysAlarmsObject != null) {
            mSysAlarmsObject.removeUAVObjectEventListener(mSysAlarmsListener);
        }

        if (!telemetryIsRunning) {
            mCurrentAlarmState = AlarmStatus.UNINITIALISED;
        }
    }

    private void updateAlarmIndicator(UAVObject obj) {
        AlarmStatus status;
        AlarmStatus currentWorst = AlarmStatus.OK;

        // Starting with alarm OK (lowest priority) keep looking for
        // a higher priority alarm. Stop looping if we find a critical
        // alarm
        UAVObjectField alarmField = obj.getField(SystemAlarms.FIELD_ALARM);
        for(int elementCount = 0; elementCount < alarmField.getNumElements(); ++elementCount){
            status = AlarmStatus.fromString(alarmField.getValue(elementCount).toString());

            if (status == AlarmStatus.CRITICAL) {
                // Can't get worse than this, might as well stop
                currentWorst = status;
                break;
            }

            // Keep track of current worst alarm
            if (status.compare(currentWorst) > 0) {
                currentWorst = status;
            }
        }

        mCurrentAlarmState = currentWorst;
        setAlarmStatus(mCurrentAlarmState);
    }
}
