/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.views.curve;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;

class GridData {
    private final Paint mGridPaint;
    private AxisDefinition mXAxis;
    private AxisDefinition mYAxis;
    private float[] mXPoints;
    private float[] mYPoints;
    private RectF mBounds;
    /**
     * Create a new instance of this class to be drawn with the given paint.
     *
     * @param gridPaint use this to draw the grid
     */
    public GridData(Paint gridPaint) {
        mGridPaint = gridPaint;
    }

    /**
     * Initialise the grid's data with the given string. This should be two
     * strings separated by a semi-colon. The first string defines the x-axis
     * and the second defines the y-axis. If there is only one string with no
     * separator, the same definition will be used for both the x and y axes.
     *
     * @param grid grid definition
     */
    public void initFromString(String grid) {
        String[] axesDefinitions = grid.split(";");

        if (axesDefinitions.length > 0) {
            mXAxis = new AxisDefinition(axesDefinitions[0]);
        }
        if (axesDefinitions.length == 2) {
            mYAxis = new AxisDefinition(axesDefinitions[1]);
        }
    }

    /**
     * Generate the grid to fit into the given rectangle.
     *
     * @param rectBounds rectangle for grid bounds
     */
    public void generateGrid(RectF rectBounds) {
        mBounds = rectBounds;
        mXPoints = generateAxisPoints(mXAxis, rectBounds.left, rectBounds.width());
        mYPoints = generateAxisPoints(mYAxis != null ? mYAxis : mXAxis, rectBounds.top, rectBounds.height());
    }

    /**
     * Draw the grid on the given canvas.
     *
     * @param canvas canvas to draw on
     */
    public void draw(Canvas canvas) {
        // Draw x axis
        for (float curFloat : mXPoints) {
            canvas.drawLine(curFloat, mBounds.bottom, curFloat, mBounds.top, mGridPaint);
        }

        // Draw y axis
        for (float curFloat : mYPoints) {
            canvas.drawLine(mBounds.left, curFloat, mBounds.right, curFloat, mGridPaint);
        }
    }

    /**
     * Generate the points along a single axis. The generated points will be
     * used by the drawing code to draw a line perpendicular to the axis from
     * one side of the grid bounds to the other, for each division on the axis.
     *
     * @param axis       definition of the axis we are generating points for
     * @param startCoord screen coordinate of first division
     * @param axisLength length of axis
     * @return an array of floats that will define one half of the coordinate pairs
     * that define the start and end of each grid line
     */
    private float[] generateAxisPoints(AxisDefinition axis, float startCoord, float axisLength) {
        float[] points = new float[axis.getNumDivisions()];
        float divisionSize = axisLength / axis.getNumDivisions();

        for (int count = 0; count < axis.getNumDivisions(); ++count) {
            points[count] = startCoord + (count * divisionSize);
        }

        return points;
    }

    private class AxisDefinition {
        private final int mNumDivisions;
        private float[] mValues;

        /**
         * Create an instance of this class with the given definition string.
         * An instance of this class represents one axis on the grid. The string
         * should be a single number of evenly spaced divisions.
         *
         * @param definition definitions string for this axis
         */
        AxisDefinition(String definition) {
            String[] values = definition.split(",");

            if (values.length == 1) {
                mNumDivisions = Integer.valueOf(values[0]);
            } else {
                mNumDivisions = values.length - 1;
                mValues = new float[values.length];

                int count = 0;
                for (String curValue : values) {
                    mValues[count++] = Float.valueOf(curValue);
                }
            }
        }

        /**
         * Gets the number of divisions on this axis.
         *
         * @return number of divisions
         */
        public int getNumDivisions() {
            return mNumDivisions;
        }

        public float[] getValues() {
            return mValues;
        }
    }
}
