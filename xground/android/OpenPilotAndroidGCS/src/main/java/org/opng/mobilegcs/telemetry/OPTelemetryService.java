/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.telemetry;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Process;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.io.RawSerialIO;
import org.opng.uavtalk.UAVObjectManager;

import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;

public class OPTelemetryService extends Service {
    // Intent category
    public final static String INTENT_CATEGORY_GCS = "org.opng.intent.category.GCS";
    public final static String INTENT_ACTION_CONNECTED = "org.opng.intent.action.CONNECTED";
    public final static String INTENT_ACTION_DISCONNECTED = "org.opng.intent.action.DISCONNECTED";
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final boolean WARN = LOGLEVEL > 0;
    // Intent actions
    private final static String INTENT_ACTION_TELEMETRYTASK_STARTED = "org.opng.intent.action.TELEMETRYTASK_STARTED";
    // Message ids
    private static final int MSG_START = 0;
    private static final int MSG_CONNECT = 1;
    private static final int MSG_DISCONNECT = 3;
    private static final int MSG_START_LOG = 4;
    private static final int MSG_STOP_LOG = 5;
    private static final int MSG_RECONNECT_REQUEST = 1; // Use with MSG_CONNECT to indicate reconnect request for existing telemetry task
    private static final int MSG_TOAST = 100;
    // Track lifecycle
    private static int mStartupRequests = 0;
    // Logging settings
    private final String TAG = OPTelemetryService.class.getSimpleName();
    private final IBinder mBinder = new LocalBinder();
    // Variables for local message handler thread
    private ServiceHandler mServiceHandler;
    private Thread mActiveTelemThread;
    private TelemetryTask mTelemetryTask;

    private void handleMessage(Message msg) {
        Context applicationContext = getApplicationContext();

        switch (msg.arg1) {
            case MSG_START:
                stopSelf(msg.arg2);
                break;
            case MSG_CONNECT:
                // Default to USB connection for now. Might support Bluetooth
                // and TCPIP later.
                String connection_type;
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                connection_type = prefs.getString("pref_connection_type", "0");
                Integer conn_type = Integer.parseInt(connection_type);

                boolean isReconnect = msg.arg2 == MSG_RECONNECT_REQUEST && mTelemetryTask != null;

                switch (conn_type) {
                    case UAVTalkConnectionTypes.UAVTALK_CONNECTION_BLUETOOTH:
                        Toast.makeText(getApplicationContext(), getString(R.string.message_bluetooth_connecting), Toast.LENGTH_SHORT).show();
                        if (!isReconnect) {
                            mTelemetryTask = new BluetoothUAVTalk(this);
                        }
                        mActiveTelemThread = new Thread(mTelemetryTask, "Bluetooth telemetry thread");
                        break;
                    case UAVTalkConnectionTypes.UAVTALK_CONNECTION_TCP:
                        Toast.makeText(getApplicationContext(), "Attempting TCP connection", Toast.LENGTH_SHORT).show();
                        if (!isReconnect) {
                            mTelemetryTask = new TcpUAVTalk(this);
                        }
                        mActiveTelemThread = new Thread(mTelemetryTask, "Tcp telemetry thread");
                        break;
                    case UAVTalkConnectionTypes.UAVTALK_CONNECTION_USB:
                        if (applicationContext != null) {
                            Toast.makeText(applicationContext, getString(R.string.message_usb_connecting), Toast.LENGTH_SHORT).show();
                        }
                        if (!isReconnect) {
                            mTelemetryTask = new USBUAVTalk(this);
                        }
                        mActiveTelemThread = new Thread(mTelemetryTask, "Hid telemetry thread");
                        break;
                    default:
                        throw new Error("Unsupported");
                }
                mActiveTelemThread.start();
                Intent startedIntent = new Intent();
                startedIntent.setAction(OPTelemetryService.INTENT_ACTION_TELEMETRYTASK_STARTED);
                sendBroadcast(startedIntent, null);
                break;
            case MSG_DISCONNECT:
                if (applicationContext != null) {
                    Toast.makeText(applicationContext, "Disconnect requested", Toast.LENGTH_SHORT).show();
                }
                if (DEBUG) Log.d(TAG, "Calling disconnect");
                if (mTelemetryTask != null) {
                    mTelemetryTask.disconnect();
                    mTelemetryTask = null;

                    try {
                        mActiveTelemThread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else if (mActiveTelemThread != null) {
                    mActiveTelemThread.interrupt();
                    try {
                        mActiveTelemThread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    mActiveTelemThread = null;
                }
                if (DEBUG) Log.d(TAG, "Telemetry thread terminated");
                Intent intent = new Intent();
                intent.setAction(INTENT_ACTION_DISCONNECTED);
                sendBroadcast(intent, null);

                stopSelf();

                break;
            case MSG_TOAST:
                Toast.makeText(this, (String) msg.obj, Toast.LENGTH_SHORT).show();
                break;
            case MSG_START_LOG:
                mTelemetryTask.startLogging();
                break;
            case MSG_STOP_LOG:
                mTelemetryTask.stopLogging();
                break;

            default:
                System.out.println(msg.toString());
                throw new Error("Invalid message");
        }
    }

    /**
     * Called when the service starts.  It creates a thread to handle messages (e.g. connect and disconnect)
     * and based on the stored preference will send itself a connect signal if needed.
     */
    private void startup() {
        synchronized (this) {
            mStartupRequests++;
        }
        final Context applicationContext = getApplicationContext();
        if (applicationContext != null) {
            Toast.makeText(applicationContext, "Telemetry service starting", Toast.LENGTH_SHORT).show();
        }

        HandlerThread thread = new HandlerThread("TelemetryServiceHandler", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        Looper mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(this, mServiceLooper);

//		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(OPTelemetryService.this);
        // Disabling autoconnect for now as the telemetry service handler code
        // has been modified to allow separation of binding of the service and
        // starting telemetry.
//		if(prefs.getBoolean("autoconnect", false)) {
//			Message msg = mServiceHandler.obtainMessage();
//			msg.arg1 = MSG_CONNECT;
//			msg.arg2 = 0;
//			mServiceHandler.sendMessage(msg);
//		}

    }

    @Override
    public void onCreate() {
        if (DEBUG)
            Log.d(TAG, "Telemetry service created");
        startup();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // We don't want to restart if killed. Only restart on demand from
        // user trying to connect
        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onDestroy() {

        if (mTelemetryTask != null) {
            Log.d(TAG, "onDestroy() shutting down telemetry task");
            mTelemetryTask.disconnect();
            mTelemetryTask = null;

            try {
                // Race condition - if we shut the service down before the telemetry task
                // thread has started, this will hang so we need to check thread is runnable.
                if (mActiveTelemThread.getState() == Thread.State.RUNNABLE) {
                    mActiveTelemThread.join();
                } else {
                    Log.d(TAG, "onDestroy() shut down telemetry task before it has started or it has already stopped");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        synchronized (this) {
            mStartupRequests = 0;
        }
        Log.d(TAG, "onDestroy() shut down telemetry task");
        Toast.makeText(this, "Telemetry service done", Toast.LENGTH_SHORT).show();
    }

    public void toastMessage(String msgText) {
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = MSG_TOAST;
        msg.obj = msgText;
        mServiceHandler.sendMessage(msg);
    }

    void requestReconnect() {
        if (DEBUG) Log.d(TAG, "Requesting reopen connection");
        final Context applicationContext = getApplicationContext();
        if (applicationContext != null) {
            Toast.makeText(applicationContext, "Requested reopen connection", Toast.LENGTH_SHORT).show();
        }
        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = MSG_CONNECT;
        msg.arg2 = MSG_RECONNECT_REQUEST;
        mServiceHandler.sendMessage(msg);
    }

    /**
     * This is used by other processes to get access to the UAVObject manager
     * and useful methods on the running telemetry task.
     */
    public interface TelemTask extends RawSerialIO {
        /**
         * Get access to the UAV object manager
         *
         * @return the single instance of the UAV object manager
         */
        UAVObjectManager getObjectManager();

        /**
         * Called to suspend telemetry while leaving the connection open.
         *
         * @return true if telemetry was suspended successfully, false otherwise
         */
        boolean suspendTelemetry();

        /**
         * Called to restart telemetry after calling suspendTelemetry.
         *
         * @return true if telemetry was suspended successfully, false otherwise
         */
        boolean resumeTelemetry();

        /**
         * Sends raw data to the flight controller through the current connection
         * without running telemetry.
         *
         * @param dataOut data to send
         * @return number of bytes sent
         */
        @Override
        int rawWriteData(ByteBuffer dataOut);

        /**
         * Reads raw data from the flight controller through the current connection
         * without running telemetry.
         *
         * @param dataIn data read from flight controller
         * @return number of bytes read
         */
        @Override
        int rawReadData(ByteBuffer dataIn);
    }

    static class ServiceHandler extends Handler {
        private final WeakReference<OPTelemetryService> mService;

        ServiceHandler(OPTelemetryService service, Looper looper) {
            super(looper);
            mService = new WeakReference<>(service);
        }

        @Override
        public void handleMessage(Message msg) {
            OPTelemetryService service = mService.get();
            if (service != null) {
                service.handleMessage(msg);
            }
        }
    }

    public class LocalBinder extends Binder {
        public TelemTask getTelemTask() {
            if (mTelemetryTask != null)
                return mTelemetryTask.getTelemTaskIface();
            return null;
        }

        public void openConnection() {
            // TODO: Work out whether or not we want this toast. Causes issues
            // when trying to use the binder on a non-UI thread. Miss out this
            // call and we can run this on any thread.
//			Toast.makeText(getApplicationContext(), "Requested open connection", Toast.LENGTH_SHORT).show();
            Message msg = mServiceHandler.obtainMessage();
            msg.arg1 = MSG_CONNECT;
            mServiceHandler.sendMessage(msg);
        }

        public void stopConnection() {
            Message msg = mServiceHandler.obtainMessage();
            msg.arg1 = MSG_DISCONNECT;
            mServiceHandler.sendMessage(msg);
        }

        public void startLogging() {
            Message msg = mServiceHandler.obtainMessage();
            msg.arg1 = MSG_START_LOG;
            mServiceHandler.sendMessage(msg);
        }

        public void stopLogging() {
            Message msg = mServiceHandler.obtainMessage();
            msg.arg1 = MSG_STOP_LOG;
            mServiceHandler.sendMessage(msg);
        }

        public void toastMessage(String msgText) {
            OPTelemetryService.this.toastMessage(msgText);
        }

        public boolean isConnected() {
            return (mActiveTelemThread != null) && (mTelemetryTask != null) && (mTelemetryTask.getConnected());
        }
    }

}
