/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.views.curve;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.os.Build;
import android.support.v4.view.MotionEventCompat;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;

import org.opng.mobilegcs.R;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: document your custom view class.
 */
public class CurveView extends View {


    private final static String TAG = CurveView.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final List<CurveData> mCurveData = new ArrayList<>();
    private final List<Paint> mCurvePaint = new ArrayList<>();
    private int mGridColor = Color.BLACK;
    private int mLabelColor = Color.RED;
    private int mLabelTextColor = Color.BLACK;
    private int[] mCurveColors;
    private int[] mCurveDecimalPlaces;
    private OnValueChangedListener mOnValueChangedListener;
    private float mMinX;
    private float mMinY;
    private float mMaxX;
    private float mMaxY;
    private RectF mRectContent;
    private CurveData mHitCurve;
    private GridData mGridData;
    private Paint mCirclePaint;
    private Paint mGridPaint;
    private TextPaint mAxisLabelPaint;
    private boolean mIsDraggingPoint;
    private float mLastTouchY;
    private String mLabelXAxis;
    private String mLabelYAxis;
    // The ‘active pointer’ is the one currently moving our grid points.
    private int mActivePointerId = MotionEvent.INVALID_POINTER_ID;
    private Rect mXAxisBounds;
    private Path mYAxisLabelPath;
    public CurveView(Context context) {
        super(context);
        init(null, 0);
    }

    public CurveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public CurveView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Get attributes from application theme to set defaults
        initApplicationDefaults();

        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.CurveView, defStyle, 0);

        mGridPaint = new Paint();
        mCirclePaint = new Paint();
        mAxisLabelPaint = new TextPaint();
        mAxisLabelPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        mMinX = a.getFloat(R.styleable.CurveView_minX, 0.0f);
        mMinY = a.getFloat(R.styleable.CurveView_minY, 0.0f);
        mMaxX = a.getFloat(R.styleable.CurveView_maxX, 1.0f);
        mMaxY = a.getFloat(R.styleable.CurveView_maxY, 1.0f);

        mLabelXAxis = a.getString(R.styleable.CurveView_labelXAxis);
        mLabelYAxis = a.getString(R.styleable.CurveView_labelYAxis);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            mGridColor = a.getColor(
                    R.styleable.CurveView_gridColor, getResources().getColor(
                            android.support.v7.appcompat.R.color.bright_foreground_material_light, null));
            mLabelColor = a.getColor(
                    R.styleable.CurveView_labelColor, getResources().getColor(
                            android.support.v7.appcompat.R.color.accent_material_light, null));
            mLabelTextColor = a.getColor(
                    R.styleable.CurveView_labelTextColor, getResources().getColor(
                            android.support.v7.appcompat.R.color.abc_primary_text_material_light, null));
        } else {
            //noinspection deprecation
            mGridColor = a.getColor(
                    R.styleable.CurveView_gridColor, getResources().getColor(
                            android.support.v7.appcompat.R.color.bright_foreground_material_light));
            //noinspection deprecation
            mLabelColor = a.getColor(
                    R.styleable.CurveView_labelColor, getResources().getColor(
                            android.support.v7.appcompat.R.color.accent_material_light));
            //noinspection deprecation
            mLabelTextColor = a.getColor(
                    R.styleable.CurveView_labelTextColor, getResources().getColor(
                            android.support.v7.appcompat.R.color.abc_primary_text_material_light));
        }
        int curveColorsId = a.getResourceId(R.styleable.CurveView_curveColors, 0);

        // Need to check we're not in edit mode as the gui preview currently can't
        // cope with integer arrays.
        if (curveColorsId > 0 && !isInEditMode()) {
            mCurveColors = getResources().getIntArray(curveColorsId);
        } else {
            if (!isInEditMode()) {
                TypedValue value = new TypedValue();
                getContext().getTheme().resolveAttribute(android.R.attr.textColorPrimary, value, true);
                mCurveColors = new int[]{value.data};
            } else {
                mCurveColors = new int[]{R.color.openpilot_black};
            }
        }

        int curveDecimalPlacesId = a.getResourceId(R.styleable.CurveView_curveDecimalPlaces, 0);

        if (curveDecimalPlacesId > 0 && !isInEditMode()) {
            mCurveDecimalPlaces = getResources().getIntArray(curveDecimalPlacesId);
        } else {
            mCurveDecimalPlaces = new int[]{2};
        }

        // Update TextPaint and text measurements from attributes
        invalidatePaintAndMeasurements();
        initGrid(a.getString(R.styleable.CurveView_grid));
        initPoints(a.getString(R.styleable.CurveView_points));
        updateCurves();

        a.recycle();
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        updateCurves();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.drawRect(mRectContent, mGridPaint);

        mGridData.draw(canvas);

        if (mXAxisBounds != null && mLabelXAxis != null) {
            canvas.drawText(mLabelXAxis, mRectContent.right - mXAxisBounds.width(), mRectContent.bottom + mXAxisBounds.height(), mAxisLabelPaint);
        }
        if (mYAxisLabelPath != null && mLabelYAxis != null) {
            canvas.drawTextOnPath(mLabelYAxis, mYAxisLabelPath, 0, 0, mAxisLabelPaint);
        }

        // Draw the path
        for (CurveData curveData : mCurveData) {
            curveData.draw(canvas);
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = MotionEventCompat.getActionMasked(event);

        switch (action) {
            case MotionEvent.ACTION_DOWN: {
                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                final float x = MotionEventCompat.getX(event, pointerIndex);
                final float y = MotionEventCompat.getY(event, pointerIndex);

                mHitCurve = null;

                for (CurveData curveData : mCurveData) {
                    if (curveData.hitTest(x, y)) {
                        mHitCurve = curveData;
                        break;
                    }
                }
                if (mHitCurve != null) {
                    mIsDraggingPoint = true;

                    // We want to stop parent from interpreting our drag as a scroll gesture.
                    // this code comes from AbsSeekBar (base of SeekBar).
                    ViewParent parent = getParent();
                    if (parent != null) {
                        parent.requestDisallowInterceptTouchEvent(true);
                    }
                }

                // Remember where we started (for dragging). Note we only want y.
                mLastTouchY = y;

                // Save the ID of this pointer (for dragging)
                mActivePointerId = MotionEventCompat.getPointerId(event, 0);
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                // Find the index of the active pointer and fetch its position
                final int pointerIndex =
                        MotionEventCompat.findPointerIndex(event, mActivePointerId);

                final float y = MotionEventCompat.getY(event, pointerIndex);

                // Calculate the distance moved
                final float dy = y - mLastTouchY;

                if (mIsDraggingPoint) {
                    mHitCurve.updateHitPoint(dy);

                    invalidate();
                }

                // Remember this touch position for the next move event
                mLastTouchY = y;
                break;
            }
            case MotionEvent.ACTION_UP:
                mActivePointerId = MotionEvent.INVALID_POINTER_ID;
                if (mIsDraggingPoint) {
                    mIsDraggingPoint = false;
                    mHitCurve.endDragPoint();

                    if (mOnValueChangedListener != null) {
                        mOnValueChangedListener.onValueChanged(this, mHitCurve);
                    }
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                mActivePointerId = MotionEvent.INVALID_POINTER_ID;
                break;
            case MotionEvent.ACTION_POINTER_UP: {

                final int pointerIndex = MotionEventCompat.getActionIndex(event);
                final int pointerId = MotionEventCompat.getPointerId(event, pointerIndex);

                if (pointerId == mActivePointerId) {
                    // This was our active pointer going up. Choose a new
                    // active pointer and adjust accordingly.
                    final int newPointerIndex = pointerIndex == 0 ? 1 : 0;
                    mLastTouchY = MotionEventCompat.getY(event, newPointerIndex);
                    mActivePointerId = MotionEventCompat.getPointerId(event, newPointerIndex);
                }
                break;
            }
        }

        return true;
    }

    public float getMinX() {
        return mMinX;
    }

    public void setMinX(float minX) {
        updateCurves();
        mMinX = minX;
    }

    public float getMinY() {
        return mMinY;
    }

    public void setMinY(float minY) {
        updateCurves();
        mMinY = minY;
    }

    public float getMaxX() {
        return mMaxX;
    }

    public void setMaxX(float maxX) {
        updateCurves();
        mMaxX = maxX;
    }

    public float getMaxY() {
        return mMaxY;
    }

    public void setMaxY(float maxY) {
        updateCurves();
        mMaxY = maxY;
    }

    /**
     * Gets the example color attribute value.
     *
     * @return The example color attribute value.
     */
    public int getLabelColor() {
        return mLabelColor;
    }

    /**
     * Sets the view's label color attribute value.
     *
     * @param labelColor The label color attribute value to use.
     */
    public void setLabelColor(int labelColor) {
        mLabelColor = labelColor;
        invalidatePaintAndMeasurements();
    }

    public int getGridColor() {
        return mGridColor;
    }

    public void setGridColor(int gridColor) {
        mGridColor = gridColor;
        invalidatePaintAndMeasurements();
    }

    public int getLabelTextColor() {
        return mLabelTextColor;
    }

    public void setLabelTextColor(int labelTextColor) {
        mLabelTextColor = labelTextColor;
        invalidatePaintAndMeasurements();
    }

    public int[] getCurveColors() {
        return mCurveColors;
    }

    public void setCurveColors(int[] curveColors) {
        mCurveColors = curveColors;
        invalidatePaintAndMeasurements();
    }

    public int[] getCurveDecimalPlaces() {
        return mCurveDecimalPlaces;
    }

    public void setCurveDecimalPlaces(int[] curveDecimalPlaces) {
        mCurveDecimalPlaces = curveDecimalPlaces;
    }

    public String getLabelXAxis() {
        return mLabelXAxis;
    }

    public void setLabelXAxis(String labelXAxis) {
        mLabelXAxis = labelXAxis;
        updateCurves();
    }

    public String getLabelYAxis() {
        return mLabelYAxis;
    }

    public void setLabelYAxis(String labelYAxis) {
        mLabelYAxis = labelYAxis;
        updateCurves();
    }

    public void setOnValueChangedListener(OnValueChangedListener listener) {
        mOnValueChangedListener = listener;
    }

    public int getCurveDataCount() {
        return mCurveData.size();
    }

    public CurveData getCurveData(int index) {
        return mCurveData.get(index);
    }

    public void setShowingLabels(boolean showLabels) {
        for (CurveData curveData : mCurveData) {
            curveData.setShowingLabels(showLabels);
        }
    }

    private void initApplicationDefaults() {
        if (!isInEditMode()) {
            String packageName = getContext().getPackageName();
            int themeId;
            try {
                PackageInfo info = getContext().getPackageManager().getPackageInfo(packageName, PackageManager.GET_META_DATA);
                themeId = info.applicationInfo.theme;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void initPoints(String points) {
        int count = 0;
        int maxCurvePaintSize = mCurvePaint.size();
        for (String curPoints : points.split(":")) {
            CurveData curveData = new CurveData(this, mCurvePaint.get(count < maxCurvePaintSize ? count : maxCurvePaintSize - 1)
                    , mCirclePaint, mLabelTextColor);
            curveData.initFromString(curPoints);
            mCurveData.add(curveData);
            ++count;
        }
    }

    private void initGrid(String grid) {
        mGridData = new GridData(mGridPaint);
        mGridData.initFromString(grid);
    }

    private void invalidatePaintAndMeasurements() {
        mCurvePaint.clear();

        for (int curCurveColour : mCurveColors) {
            Paint curCurvePaint = new Paint();
            curCurvePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
            curCurvePaint.setStyle(Paint.Style.STROKE);
            curCurvePaint.setStrokeWidth(5);
            curCurvePaint.setColor(curCurveColour);
            mCurvePaint.add(curCurvePaint);
        }
        mCirclePaint.setStyle(Paint.Style.FILL);
        mCirclePaint.setColor(mLabelColor);

        mGridPaint.setStyle(Paint.Style.STROKE);
        mGridPaint.setStrokeWidth(2);
        mGridPaint.setColor(mGridColor);

        mAxisLabelPaint.setColor(mLabelTextColor);
    }

    public void updateCurves() {
        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        // Measure axis label text
        mXAxisBounds = new Rect();
        Rect yAxisBounds = new Rect();

        if (mLabelXAxis != null && !mLabelXAxis.isEmpty()) {
            mAxisLabelPaint.getTextBounds(mLabelXAxis, 0, mLabelXAxis.length() - 1, mXAxisBounds);
        }
        int textPathX = paddingLeft + yAxisBounds.height();
        // To save fiddling about with rotating the canvas, simply draw the y-axis
        // text along a path. Sadly this doesn't render in the gui editor.
        if (mLabelYAxis != null && !mLabelYAxis.isEmpty() && !isInEditMode()) {
            mAxisLabelPaint.getTextBounds(mLabelYAxis, 0, mLabelYAxis.length() - 1, yAxisBounds);
            mYAxisLabelPath = new Path();
            mYAxisLabelPath.moveTo(textPathX, paddingTop + yAxisBounds.width());
            mYAxisLabelPath.lineTo(textPathX, paddingTop);
        } else {
            mYAxisLabelPath = null;
        }

        int contentWidth = getWidth() - paddingLeft - paddingRight - yAxisBounds.height();
        int contentHeight = getHeight() - paddingTop - paddingBottom - mXAxisBounds.height();

        // Map curve bounds to rect defined by content width & height.
        // Note that the content rect is +ve y downward so we need to
        // define the curve rect upside down and then flip the resulting
        // matrix
        mRectContent = new RectF(paddingLeft + yAxisBounds.height(),
                paddingTop,
                paddingLeft + yAxisBounds.height() + contentWidth,
                paddingTop + contentHeight);
        RectF rectCurve = new RectF(mMinX, mMinY, mMaxX, mMaxY);
        Matrix transform = new Matrix();
        transform.setRectToRect(rectCurve, mRectContent, Matrix.ScaleToFit.FILL);

        // Defined our matrix mapping the two rects, now need to flip it about
        // its center point
        transform.postScale(1, -1, 0, paddingTop + (contentHeight / 2));

        // Update the grid
        mGridData.generateGrid(mRectContent);

        // Update the curve
        for (CurveData curveData : mCurveData) {
            curveData.generateCurve(transform);
        }
    }

    public interface OnValueChangedListener {
        void onValueChanged(CurveView sender, CurveData data);
    }
}
