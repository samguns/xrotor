/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.flightcontroller;

import org.opng.mobilegcs.util.Variant;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.uavobjects.FirmwareIAPObj;

abstract class IAPState {
    private static final int LOGLEVEL = 3;
    protected static final boolean VERBOSE = LOGLEVEL > 3;
    static final boolean DEBUG = LOGLEVEL > 2;
    protected static final boolean WARN = LOGLEVEL > 1;
    protected static final boolean ERROR = LOGLEVEL > 0;
    final Uploader mUploader;
    final FirmwareIAPObj mFirmwareIAPObj;

    IAPState(IAPState state) {
        this(state.getUploader());
    }

    IAPState(Uploader uploader) {
        mUploader = uploader;
        mFirmwareIAPObj = (FirmwareIAPObj) uploader.getFirmwareIap();
    }

    private Uploader getUploader() {
        return mUploader;
    }

    void setIapCommand(String command) {
        mFirmwareIAPObj.getField("Command").setValue(new Variant(command));
    }

    void initHandleTransactionComplete() {
        UAVObjectEventHandler listener = new UAVObjectEventHandler();

        // Check to see if we already have a listener set up, remove it if
        // necessary
        if (mUploader.getListener() != null) {
            finishHandleTransactionComplete();
        }
        mUploader.setListener(listener);
        mFirmwareIAPObj.addUAVObjectEventListener(listener);
    }

    void finishHandleTransactionComplete() {
        UAVObjectEventHandler listener = mUploader.getListener();
        mUploader.setListener(null);
        mFirmwareIAPObj.removeUAVObjectEventListener(listener);
    }

    public abstract void process(boolean success);

    public class UAVObjectEventHandler extends UAVObjectEventAdapter {
        @Override
        public void transactionCompleted(UAVObject obj, boolean success) {
            mUploader.gotoBootloader(success);
        }
    }
}
