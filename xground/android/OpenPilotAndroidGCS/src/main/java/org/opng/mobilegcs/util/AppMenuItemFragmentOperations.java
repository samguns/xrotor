/*
 * Eric Henaff Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

/**
 * Implement this interface on Fragments intended to be used with AppMenu.
 */
public interface AppMenuItemFragmentOperations {
    Fragment getFragment() throws IllegalAccessException, InstantiationException;

    void Open(FragmentManager fragmentManager, int containerId);
}
