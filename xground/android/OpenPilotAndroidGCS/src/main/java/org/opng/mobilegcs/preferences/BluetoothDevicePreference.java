/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.preferences;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.support.v7.preference.ListPreference;
import android.util.AttributeSet;

import java.util.ArrayList;
import java.util.Set;


public class BluetoothDevicePreference extends ListPreference {
    private BluetoothAdapter mBtAdapter;
    //private Boolean bluetoothNotSupported = false;

    public BluetoothDevicePreference(Context context) {
        super(context);
    }

    public BluetoothDevicePreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void show() {
//        showDialog(null);
    }

    public void discoverBlueTooth() {
        mBtAdapter = getBtAdapter();
        findPairedDevices();
        discoverDevices();
    }

    public Set<BluetoothDevice> getBondedDevices() {
        return mBtAdapter.getBondedDevices();
    }

    private void findPairedDevices() {
        if (mBtAdapter != null) {
            Set<BluetoothDevice> pairedDevices = mBtAdapter.getBondedDevices();
            ArrayList<CharSequence> entries = new ArrayList<>();
            ArrayList<CharSequence> entryValues = new ArrayList<>();
            for (BluetoothDevice d : pairedDevices) {
                entries.add("paired: " + d.getName());
                entryValues.add(d.getAddress());
            }

            CharSequence[] csEntries = entries.toArray(new CharSequence[entries.size()]);
            CharSequence[] csEntryValues = entryValues.toArray(new CharSequence[entryValues.size()]);

            this.setEntries(csEntries);
            this.setEntryValues(csEntryValues);
        }
    }

    private BluetoothAdapter getBtAdapter() {
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            return null;
        } else {
            return mBluetoothAdapter;
        }
    }

    private void discoverDevices() {
        if (mBtAdapter != null) {
            mBtAdapter.startDiscovery();
        }
    }

}
