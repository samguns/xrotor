/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.views.curve;

import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.text.TextPaint;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class CurveData {

    private final CurveView mParent;
    private final Paint mCurvePaint;
    private final TextPaint mTextPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG | Paint.SUBPIXEL_TEXT_FLAG);
    private final Paint mCirclePaint;
    private final float mCircleRadius = 20;
    private final NumberFormat mFormatter = NumberFormat.getInstance(Locale.getDefault());
    private List<PointData> mPoints;
    private Path mCurve;
    private Matrix mTransform;
    private Matrix mInverseTransform;
    private boolean mCanUseInverseTransform;
    private PointData mCurHitpoint;
    private int mDecimalPlaces = 2;
    private boolean mIsExpo;
    private boolean mShowLabels = true;
    private boolean mReadOnly;
    /**
     * Create a new instance of this class to draw a curve using the given
     * Paint instance
     *
     * @param curvePaint paint used to define curve appearance
     */
    public CurveData(CurveView parent, Paint curvePaint, Paint circlePaint, int labelTextColor) {
        mParent = parent;
        mCurvePaint = curvePaint;
        mTextPaint.setColor(labelTextColor);
        mTextPaint.setTextSize(12);
        mCirclePaint = circlePaint;
        mFormatter.setMaximumFractionDigits(mDecimalPlaces);
    }

    public float getPoint(int index) {
        float retVal = Float.NaN;

        if (index > -1 && index < mPoints.size()) {
            retVal = mPoints.get(index).getDataPoint().y;
        }

        return retVal;
    }

    public void setPoint(int index, float value, boolean updateNow) {
        if (index < mPoints.size()) {
            PointData pointData = mPoints.get(index);
            PointF dataPoint = pointData.getDataPoint();
            dataPoint.set(dataPoint.x, value);
            pointData.updateLabelText();
            pointData.updateViewPointFromDataPoint();

            if (updateNow) {
                updateCurveFromViewPoints();
                mParent.postInvalidate();
            }
        }
    }

    public void setPoint(int index, float x, float y) {
        if (index < mPoints.size()) {
            PointData pointData = mPoints.get(index);
            PointF dataPoint = pointData.getDataPoint();
            dataPoint.set(x, y);
            pointData.updateLabelText();
            pointData.updateViewPointFromDataPoint();
        }
    }

    /**
     * Get the paint object used to define the curve appearance. Use this
     * property to modify the paint object.
     *
     * @return the paint object that defines the curve appearance
     */
    public Paint getCurvePaint() {
        return mCurvePaint;
    }

    public int getDecimalPlaces() {
        return mDecimalPlaces;
    }

    public void setDecimalPlaces(int decimalPlaces) {
        mDecimalPlaces = decimalPlaces;
    }

    public boolean isShowingLabels() {
        return mShowLabels;
    }

    public void setShowingLabels(boolean showLabels) {
        mShowLabels = showLabels;
    }

    public boolean isReadOnly() {
        return mReadOnly;
    }

    public void setReadOnly(boolean readOnly) {
        mReadOnly = readOnly;
    }

    /**
     * Create a new curve using points in the given string. The string is in
     * the form:<p/>
     * "x,y[;x,y]..."<p/>
     * To define more than one curve, separate the curve definitions with a
     * ':'. To define an exponential curve, start the curve definition with
     * 'exp' and define only the start and end points.
     *
     * @param points string defining points used to create curve
     */
    public void initFromString(String points) {
        if (points != null && !points.isEmpty()) {
            final String pointsString;
            boolean pointCountOnly = false;

            if (points.contains("exp")) {
                mIsExpo = true;

                // Strip 'exp' from start of string
                pointsString = points.substring(3);
            } else if (points.contains("#")) {
                // Strip '#'
                pointsString = points.substring(1);
                pointCountOnly = true;
            } else {
                pointsString = points;
            }

            if (mPoints == null) {
                mPoints = new ArrayList<>();
            } else {
                // This method defines a new curve so clear out any old points
                mPoints.clear();
            }

            if (!pointCountOnly) {
                // String is expected to be a ';' separated list of x,y coordinate pairs. If this is
                // a quadratic bezier (string started with 'exp'), we will have two points that define
                // the start and end of the curve.
                // If this is not a bezier, this will be a list of all the points in the curve.
                for (String parsedPair : pointsString.split(";")) {
                    String[] coords = parsedPair.split(",");
                    if (coords.length == 2) {
                        mPoints.add(new PointData(Float.valueOf(coords[0]), Float.valueOf(coords[1])));
                    }
                }
            } else {
                // String is expected to be a single value stating number of points to add to
                // curve. These will all default to zero for y.
                for (int count = 0; count < Integer.valueOf(pointsString); ++count) {
                    mPoints.add(new PointData(count, 0));
                }
            }
        }
    }

    /**
     * Use this method to generate the curve from the stored points. The points
     * will be transformed from the curve points coordinate space to the view
     * coordinate space using the given transform. This method will also save the
     * transformed points so we have a collection of curve points with both the
     * original data and the transformed version.<p/>
     * This method should be called each time the view is resized or the layout
     * changes.
     *
     * @param transform this transform should transform the data bounds of our
     *                  curve points to the view coordinate system
     */
    public void generateCurve(Matrix transform) {
        if (transform != null) {
            mTransform = transform;
            mInverseTransform = new Matrix();
            mCanUseInverseTransform = transform.invert(mInverseTransform);

            if (mCurve == null) {
                mCurve = new Path();
            } else {
                mCurve.reset();
            }

            float[] pointsToTransform = new float[mPoints.size() * 2];
            int counter = 0;

            // This loop will create the curve to draw and also setup an array
            // of floats that will be transformed into the view equivalent of
            // our curve points.
            for (PointData curPoint : mPoints) {
                float x = curPoint.getDataPoint().x;
                float y = curPoint.getDataPoint().y;

                if (counter == 0) {
                    mCurve.moveTo(x, y);
                } else {
                    if (mIsExpo) {
                        final PointF dataPoint = mPoints.get(0).getDataPoint();
                        mCurve.quadTo((Math.abs(x) - Math.abs(dataPoint.x)) / 2, (Math.abs(y) - Math.abs(dataPoint.y)) / 2, x, y);
                    } else {
                        mCurve.lineTo(x, y);
                    }
                }

                // Add point to array of points to transform. This will
                // be used to set the location of the labels.
                int arrayIndex = counter * 2;
                pointsToTransform[arrayIndex] = x;
                pointsToTransform[arrayIndex + 1] = y;

                ++counter;
            }

            // Transform curve points to view points
            transform.mapPoints(pointsToTransform);

            // Save transformed points
            counter = 0;
            for (PointData curPoint : mPoints) {
                float x = pointsToTransform[counter];
                float y = pointsToTransform[counter + 1];
                curPoint.setViewPoint(x, y);

                // Create label text and its draw location
                curPoint.updateLabelText();
                counter += 2;
            }

            // Transform curve to view coordinates
            mCurve.transform(transform);
        }
    }

    /**
     * Draw the curve on the given canvas using the stored paint object.
     *
     * @param canvas canvas to draw curve on
     */
    public void draw(Canvas canvas) {
        canvas.drawPath(mCurve, mCurvePaint);

        if (mShowLabels) {
            for (PointData curData : mPoints) {
                canvas.drawCircle(curData.getViewPoint().x, curData.getViewPoint().y, mCircleRadius, mCirclePaint);
                if (curData.hasText()) {
                    canvas.drawText(curData.getText(), curData.getTextX(), curData.getTextY(), mTextPaint);
                }
            }
        }
    }

    /**
     * Perform a hit test on the curve to see if the given point falls within our circle
     * radius. It does this by simply calculating the distance between each curve point and
     * the given point. If this distance is less than or equal to the circle radius, this
     * counts as a hit and we retain the hit point and return true.
     *
     * @param x x coordinate of hit test point
     * @param y y coordinate of hit test point
     * @return true if we hit one of the curve points, false otherwise
     */
    public boolean hitTest(float x, float y) {
        boolean retVal = false;
        mCurHitpoint = null;

        if (!mReadOnly) {
            for (PointData curData : mPoints) {
                float distX = curData.getViewPoint().x - x;
                float distY = curData.getViewPoint().y - y;

                //noinspection SuspiciousNameCombination
                if (Math.sqrt(Math.pow(distX, 2) + Math.pow(distY, 2)) <= mCircleRadius) {
                    retVal = true;
                    mCurHitpoint = curData;
                    break;
                }
            }
        }

        return retVal;
    }

    /**
     * Move the current hit point by the given distance on the y axis.
     *
     * @param distanceY distance to move point on y axis in screen units
     */
    public void updateHitPoint(float distanceY) {
        if (mCurHitpoint != null) {
            mCurHitpoint.moveViewPoint(distanceY);
            mCurHitpoint.updateDataPointFromViewPoint();
            updateCurveFromViewPoints();
        }
    }

    public void endDragPoint() {
        if (mCurHitpoint != null) {
            mCurHitpoint.updateDataPointFromViewPoint();
        }
    }

    private void updateCurveFromViewPoints() {
        mCurve.reset();
        int counter = 0;

        for (PointData curPoint : mPoints) {
            float x = curPoint.getViewPoint().x;
            float y = curPoint.getViewPoint().y;

            if (counter == 0) {
                mCurve.moveTo(x, y);
            } else {
                mCurve.lineTo(x, y);
            }

            ++counter;
        }
    }

    private class PointData {
        private final PointF mDataPoint;
        private PointF mViewPoint;
        private String mText;
        private float mTextX;
        private float mTextY;
        private boolean mHasText;

        public PointData(float x, float y) {
            mDataPoint = new PointF(x, y);
        }

        public PointF getDataPoint() {
            return mDataPoint;
        }

        public PointF getViewPoint() {
            return mViewPoint;
        }

        public void setViewPoint(float x, float y) {
            mViewPoint = new PointF(x, y);
        }

        public boolean hasText() {
            return mHasText;
        }

        public String getText() {
            return mText;
        }

        public float getTextX() {
            return mTextX;
        }

        public float getTextY() {
            return mTextY;
        }

        public void moveViewPoint(float distanceY) {
            mViewPoint.offset(0, distanceY);
            updateLabelText();
        }

        public void updateDataPointFromViewPoint() {
            if (mCanUseInverseTransform && mInverseTransform != null) {
                float[] point = new float[]{mViewPoint.x, mViewPoint.y};
                mInverseTransform.mapPoints(point);
                mDataPoint.set(point[0], point[1]);
            }
        }

        public void updateViewPointFromDataPoint() {
            if (mTransform != null) {
                float[] point = new float[]{mDataPoint.x, mDataPoint.y};
                mTransform.mapPoints(point);
                mViewPoint.set(point[0], point[1]);
            }
        }

        public void updateLabelText() {
            String text = mFormatter.format(mDataPoint.y);
            Rect bounds = new Rect();
            mTextPaint.getTextBounds(text, 0, text.length() - 1, bounds);
            setPointText(text, mViewPoint.x - (bounds.width() / 2), mViewPoint.y + (bounds.height() / 2));
        }

        private void setPointText(String text, float textX, float textY) {
            if (text != null && !text.isEmpty()) {
                mHasText = true;
                mText = text;
                mTextX = textX;
                mTextY = textY;
            } else {
                mHasText = false;
            }
        }
    }
}
