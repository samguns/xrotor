/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opng.mobilegcs.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StabiResponsivenessFragment extends Fragment implements StabiGuiFragment {

    private View _rootView;

    public StabiResponsivenessFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _rootView = inflater.inflate(R.layout.fragment_stabi_responsiveness, container, false);

        return _rootView;
    }


    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        helper.bindRollResponsivenessWidgets(_rootView.findViewById(R.id.textRollAttitudeResponse),
                _rootView.findViewById(R.id.textRollRateResponse),
                _rootView.findViewById(R.id.textRollMaxRateLimit));
        helper.bindPitchResponsivenessWidgets(_rootView.findViewById(R.id.textPitchAttitudeResponse),
                _rootView.findViewById(R.id.textPitchRateResponse),
                _rootView.findViewById(R.id.textPitchMaxRateLimit));
        helper.bindYawResponsivenessWidgets(_rootView.findViewById(R.id.textYawAttitudeResponse),
                _rootView.findViewById(R.id.textYawRateResponse),
                _rootView.findViewById(R.id.textYawMaxRateLimit));
    }
}
