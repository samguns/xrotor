/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.filemanager;

import android.view.View;

import java.util.List;

/**
 * Implement this interface to define a file selection model to be used by
 * FileRecyclerViewAdapter.
 */
interface FileSelectionModel {
    /**
     * Saves a reference to the owning FileRecyclerViewAdapter so that it can
     * be notified of changes arising from selections changing.
     *
     * @param fileRecyclerViewAdapter adapter to be notified of changes
     */
    void setRecyclerViewAdapter(FileRecyclerViewAdapter fileRecyclerViewAdapter);

    /**
     * Indicates whether or not the item item at the given position is selected.
     *
     * @param position position of item to check
     * @return true if item is selected, false otherwise
     */
    boolean itemIsSelected(int position);

    /**
     * Gets a list of positions of the currently selected items.
     *
     * @return currently selected item positions
     */
    List<Integer> getSelectedItems();

    /**
     * Called whenever an action has occurred that will change the selection. It
     * is assumed that the given view's tag property will contain a reference to its
     * ViewHolder, allowing us to get its current position.
     *
     * @param view view that triggered the change
     */
    void handleSelection(View view);

    /**
     * Called whenever an item is long clicked. It is assumed that the given view's
     * tag property will contain a reference to its ViewHolder, allowing us to get
     * its current position.
     *
     * @param view view that was long clicked
     */
    void handleLongClick(View view);

    /**
     * Used to reset the selection to none.
     */
    void reset();

    /**
     * Add a listener for file selection model events.
     *
     * @param listener listener to add
     */
    void addFileSelectionModelListener(FileSelectionModelListener listener);

    /**
     * Remove a file selection model event listener.
     *
     * @param listener listener to remove
     */
    void removeFileSelectionModelListener(FileSelectionModelListener listener);
}
