/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import org.opng.mobilegcs.telemetry.OPTelemetryService;
import org.opng.uavtalk.UAVObjectManager;

import java.nio.ByteBuffer;
import java.util.concurrent.CountDownLatch;

/**
 * This class is used by {@link ActivityTelemetryConnectionHandler}
 * to give activities access to the telemetry service. It is also used by UAVObjectLoader to
 * provide telemetry service access to fragments.<p/>
 * Once this class is created, it can be bound to the telemetry service by calling one of the
 * bindToTelemetryService overloads. These will start the telemetry service if it is not running
 * and bind to it. The single parameter overload of bindToTelemetryService will bind to the
 * service and connect telemetry, giving access to the {@link UAVObjectManager}
 * via a listener provided
 * to the constructor or via the getObjectManager method. The other overload allows the calling
 * code to disable connecting to telemetry once bound to the service. If this is done, the
 * connect method must be called to connect to telemetry. Note that if you want to ensure that
 * the getObjectManager method will return something, use the CountDownLatch returned by
 * getTelemRunningLatch to synchronize with the telemetry connection process.<p/>
 * The telemetry connection can be started and stopped by calling connect and disconnect. These
 * are intended to be called from buttons or menu items in the GUI. Once we are done with the
 * service we can unbind from it by calling unbindFromTelemetryService.<p/>
 * This class makes use of two callbacks internally to get hold of the UAVObjectManager from
 * the telemetry service:<p/>
 * The ServiceConnection instance _connection gets hold of the services
 * binding interface and will also get the UAVObjectManager if the telemetry is already connected.
 * If telemetry is not connected and we are set to connect on bind, _connection will call the
 * connect method.<p/>
 * The BroadcastReceiver instance _connectedReceiver will get hold of the UAVObjectManager when
 * it receives an INTENT_ACTION_CONNECTED intent action and we don't already have it. The
 * BroadcastReceiver will also disconnect from telemetry when it receives an INTENT_ACTION_DISCONNECTED
 * intent action. The INTENT_ACTION_CONNECTED and INTENT_ACTION_DISCONNECTED intent actions will
 * be sent when the connect and disconnect methods are used respectively.
 */
public class TelemetryServiceHandler implements org.opng.mobilegcs.io.RawSerialIO {


    private static final int LOGLEVEL = 0;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    // Logging setup
    private final String TAG = "TelemetryServiceHandler";
    private final ConnectionListener mConnectionListener;
    /**
     * Defines callbacks for service binding, passed to bindService()
     */
    private final ServiceConnection mServiceConnection;
    /**
     * The binder to access the telemetry task, and thus the object manager
     */
    private OPTelemetryService.LocalBinder mBinder;
    /**
     * UAVObjectManager instance from the telemetry service
     */
    private UAVObjectManager mObjectManager;
    private BroadcastReceiver mConnectedReceiver;
    /**
     * Used to synchronise with starting of the telemetry service
     */
    private CountDownLatch mTelemServiceConnectedLatch = new CountDownLatch(1);
    /**
     * Used to synchronise with establishing a connection with the telemetry service
     */
    private CountDownLatch mTelemRunningLatch = new CountDownLatch(1);
    /**
     * True if we have a telemetry connection
     */
    private boolean mConnected;
    /**
     * True if we are bound to the telemetry service
     */
    private boolean mBound;
    /**
     * If this is true, we will connect after binding to the service. If
     * this is false, the connect method will need to be called to connect
     * telemetry. Setting this false effectively separates binding the
     * service and connecting telemetry.
     */
    private boolean mConnectOnBind = false;

    public TelemetryServiceHandler() {
        this(null);
    }
    /**
     * Create a new instance of this class. The given listener, if provided, will
     * be notified of connection to and disconnection of the telemetry.
     *
     * @param listener listener that will be informed of connection and disconnection
     *                 events
     */
    public TelemetryServiceHandler(ConnectionListener listener) {
        if (DEBUG) Log.d(TAG, "Creating new instance");
        mConnectionListener = listener;
        mServiceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName arg0, IBinder service) {
                // We've bound to LocalService, cast the IBinder and attempt to open a connection
                if (DEBUG)
                    Log.d(TAG, "Service bound");
                mBound = true;
                mBinder = (OPTelemetryService.LocalBinder) service;
                mTelemServiceConnectedLatch.countDown();

                if (mBinder.isConnected()) {
                    if (DEBUG)
                        Log.d(TAG, "Bound to already running telemetry service");
                    OPTelemetryService.TelemTask task;
                    if ((task = mBinder.getTelemTask()) != null) {
                        mObjectManager = task.getObjectManager();
                        mConnected = true;
                        mTelemRunningLatch.countDown();
                        fireConnected();
                    }

                } else if (mConnectOnBind) {
                    if (DEBUG)
                        Log.d(TAG, "Not connected: opening telemetry connection");
                    mBinder.openConnection();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                if (DEBUG)
                    Log.d(TAG, "Service disconnected");
                mBound = false;
                mBinder = null;
                mConnected = false;
                mObjectManager = null;
                mConnected = false;
                mTelemServiceConnectedLatch = new CountDownLatch(1);
            }
        };
    }

    public UAVObjectManager getObjManager() {
        return mObjectManager;
    }

    public CountDownLatch getTelemServiceConnectedLatch() {
        return mTelemServiceConnectedLatch;
    }

    public CountDownLatch getTelemRunningLatch() {
        return mTelemRunningLatch;
    }

    /**
     * Returns whether or not we are connected to telemetry.
     *
     * @return true if we are connected to telemetry, false otherwise
     */
    public boolean isTelemetryConnected() {
        return mConnected;
    }

    /**
     * Returns whether or not we have a 'physical' connection: USB, bluetooth
     * or TCP/IP.
     *
     * @return true if we are communicating with the hardware, false otherwise
     */
    public boolean isTelemetryTaskConnected() {
        return mBinder != null && mBinder.isConnected();
    }

    /**
     * Bind to the telemetry service, starting it if necessary. This overload will
     * also connect to telemetry once successfully bound.
     *
     * @param context context of activity binding the service
     */
    public void bindToTelemetryService(Context context) {
        bindToTelemetryService(context, true);
    }

    /**
     * Bind to the telemetry service, starting it if necessary. This overload has
     * a parameter that indicates whether or not we wish to connect to telemetry
     * once binding is complete. If connectOnBind is false, the connect() method
     * will need to be called to connect to telemetry. This parameter allows us
     * to separate binding the service and connecting telemetry by setting false.
     *
     * @param context       context of activity binding the service
     * @param connectOnBind set to true to connect to telemetry once bound, false
     *                      otherwise.
     */
    public void bindToTelemetryService(Context context, boolean connectOnBind) {
        if (context != null) {
            mConnectOnBind = connectOnBind;
            // Set up the filters
            IntentFilter filter = new IntentFilter();
            filter.addCategory(OPTelemetryService.INTENT_CATEGORY_GCS);
            filter.addAction(OPTelemetryService.INTENT_ACTION_CONNECTED);
            filter.addAction(OPTelemetryService.INTENT_ACTION_DISCONNECTED);
            mConnectedReceiver = createConnectedReceiver();
            context.registerReceiver(mConnectedReceiver, filter);

            // Bind to the telemetry service (which will start it)
            Intent intent = new Intent(context.getApplicationContext(),
                    OPTelemetryService.class);
            context.startService(intent);
            if (DEBUG)
                Log.d(TAG, "Attempting to bind: " + intent);
            context.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    /**
     * Unbind from the telemetry service. This will clean up our connection
     * with the telemetry service and unregister the broadcast receiver. If
     * this is the last connection to the service, it may be terminated.
     *
     * @param context context of activity that bound to the service
     */
    public void unbindFromTelemetryService(Context context) {
        if (mConnectedReceiver != null) {
            if (mBound) {
                if (DEBUG) Log.d(TAG, "Unbind service");
                context.unbindService(mServiceConnection);
            }
            if (DEBUG) Log.d(TAG, "Unregister broadcast receiver");
            context.unregisterReceiver(mConnectedReceiver);
            mConnectedReceiver = null;
        }
    }

    /**
     * Connect to a previously bound telemetry service. This will start telemetry
     * running and connect to the flight controller.
     */
    public void connect() {
        if (mBound) {
            mBinder.openConnection();
        }
    }

    /**
     * Disconnect from a previously bound and connected telemetry service. We will
     * remain bound to the service until unbindFromTelemetryService is called.
     */
    public void disconnect() {
        // We will disconnect even if there is no active telemetry but we do
        // have communication with the hardware.
        if (mBound && (isTelemetryConnected() || isTelemetryTaskConnected())) {
            mBinder.stopConnection();
        }
    }

    /**
     * Start the logging telemetry task
     */
    public void startLogging() {
        if (mBound && mConnected) {
            mBinder.startLogging();
        }
    }

    /**
     * Stop the logging telemetry task
     */
    public void stopLogging() {
        if (mBound && mConnected) {
            mBinder.stopLogging();
        }
    }

    /**
     * Suspend processing of UAVObjects while still allowing data to be sent
     * to the flight controller.
     */
    public void suspendTelemetry() {
        if (mBound) {
            mBinder.getTelemTask().suspendTelemetry();
        }
    }

    /**
     * Resume processing of UAVObjects after calling suspendTelemetry.
     */
    public void resumeTelemetry() {
        if (mBound) {
            mBinder.getTelemTask().resumeTelemetry();
        }
    }

    @Override
    public int rawWriteData(ByteBuffer dataOut) {
        int retVal = -1;

        if (mBound) {
            retVal = mBinder.getTelemTask().rawWriteData(dataOut);
        }

        return retVal;
    }

    @Override
    public int rawReadData(ByteBuffer dataIn) {
        int retVal = -1;

        if (mBound) {
            retVal = mBinder.getTelemTask().rawReadData(dataIn);
        }

        return retVal;
    }

    /**
     * Pop up a toast message
     */
    public void toastMessage(String msgText) {
        if (mBound) {
            mBinder.toastMessage(msgText);
        }
    }

    /**
     * Create a BroadcastReceiver to respond to INTENT_ACTION_CONNECTED
     * by getting hold of the UAVObjectManager and firing the connected event.
     * It will also respond to INTENT_ACTION_DISCONNECTED cleaning up the
     * various object used and firing the disconnect event.
     *
     * @return new BroadcastReceiver to handle telemetry service connect and
     * disconnect events.
     */
    private BroadcastReceiver createConnectedReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (DEBUG)
                    Log.d(TAG, "Received intent");
                if (intent != null) {
                    OPTelemetryService.TelemTask task;
                    final String intentAction = intent.getAction();
                    if (intentAction != null) {
                        if (intentAction.compareTo(OPTelemetryService.INTENT_ACTION_CONNECTED) == 0) {
                            if (mBinder == null)
                                return;
                            if ((task = mBinder.getTelemTask()) == null)
                                return;
                            if (!mConnected) {
                                if (DEBUG)
                                    Log.d(TAG, "Bound to newly started telemetry service");
                                mObjectManager = task.getObjectManager();
                                mConnected = true;
                                mTelemRunningLatch.countDown();
                                fireConnected();
                                Log.d(TAG, "Connected()");
                            }
                        } else if (intentAction.compareTo(OPTelemetryService.INTENT_ACTION_DISCONNECTED) == 0) {
                            mObjectManager = null;
                            mConnected = false;
                            mTelemRunningLatch = new CountDownLatch(1);
                            fireDisconnected();
                            Log.d(TAG, "Disconnected()");
                        }
                    }
                }
            }
        };
    }

    /**
     * If a listener was provided when this instance was created, this method will
     * notify it that we are now connected to telemetry.
     */
    private void fireConnected() {
        if (mConnectionListener != null && mObjectManager != null) {
            if (DEBUG) Log.d(TAG, "Firing connected event");
            mConnectionListener.onConnect(mObjectManager);
        }
    }

    /**
     * If a listener was provided when this instance was created, this method will
     * notify it that we are now disconnected from telemetry.
     */
    private void fireDisconnected() {
        if (mConnectionListener != null) {
            if (DEBUG) Log.d(TAG, "Firing disconnected event");
            mConnectionListener.onDisconnect();
        }
    }

    /**
     * Implement this interface to provide a listener for telemetry connect and disconnect
     * events.
     */
    public interface ConnectionListener {
        void onConnect(UAVObjectManager objectManager);

        void onDisconnect();
    }
}