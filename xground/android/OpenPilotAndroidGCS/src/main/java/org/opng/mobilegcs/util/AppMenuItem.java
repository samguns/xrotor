/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import java.util.ArrayList;

public class AppMenuItem {
    private static final ArrayList<Integer> mManualIds = new ArrayList<>();
    private static int LastId = 0;
    public final String mTitle;
    private final ArrayList<AppMenuItem> mSubMenu = new ArrayList<>();
    private int mId;
    private int mIcon = -1;

    public AppMenuItem(String title) {
        mId = LastId++;
        while (mManualIds.contains(mId))
            mId = LastId++;
        mTitle = title;
    }

    AppMenuItem(int id, String title) {
        this.mId = id;
        mManualIds.add(id);

        mTitle = title;
    }

    public AppMenuItem(String title, int icon) {
        this(title);
        mIcon = icon;
    }

    AppMenuItem(int id, String title, int icon) {
        this(id, title);
        mIcon = icon;
    }

    @Override
    public String toString() {
        return mTitle;
    }

    public ArrayList<AppMenuItem> getSubMenu() {
        return mSubMenu;
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        mIcon = icon;
    }
}
