/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.opng.mobilegcs.preferences.AndroidGCSPrefs;
import org.opng.uavtalk.Telemetry;
import org.opng.uavtalk.TelemetryMonitor;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.UAVTalk;
import org.opng.uavtalk.uavobjects.TelemObjectsInitialize;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public abstract class TelemetryTask implements Runnable {
    public static final String INTENT_ACTION_TRANSPORT_CONNECTED = "org.opng.intent.action.TRANSPORT_CONNECTED";
    private static final int LOGLEVEL = 0;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final boolean WARN = LOGLEVEL > 0;
    //! Handle to the parent service
    final OPTelemetryService mTelemetryService;

	/*
   * This is a self contained runnable that will establish (if possible)
	 * a telemetry connection and provides a listener interface to be
	 * notified of a set of events
	 *
	 * 1. attempt to establish connection
	 * 2. callback when it succeeds (or fails) which notifies listener
	 * 3. once physical connection is established instantiate uavtalk / objectmanager
	 * 4. notify listener they are connected
	 * 5. detect physical link failure and notify listener about that
	 * 6. provide "close link" method
	 *
	 * There are essentially four tasks that need to occur here
	 * 1. Transfer data from the outputStream to the physical link (some protocols do this automatically)
	 * 2. Transfer data from the physical link to the inputStream (again some protocols do this automatically)
	 * 3. Transfer data from the inputStream to UAVTalk (uavTalk.processInputByte)
	 * 4. Transfer data from objects via UAVTalk to output stream (occurs from Telemetry object)
	 */
    final AndroidGCSPrefs mPrefs;
    // Logging settings
    private final String TAG = TelemetryTask.class.getSimpleName();
    private final LoggingHandler mLoggingHandler = new LoggingHandler();
    private final UAVObjectHandler mUavObjectHandler = new UAVObjectHandler();
    private final Context mContext;
    //! Private variables
    Handler mHandler;
    //! The input stream for the telemetry channel
    InputStream mInStream;
    //! The output stream for the telemetry channel
    OutputStream mOutStream;
    //! The object manager that will be used for this telemetry task
    private UAVObjectManager mUAVObjectManager;
    //! The UAVTalk connected to the below streams
    private UAVTalk mUAVTalk;
    //! The telemetry object which takes care of higher level transactions
    private Telemetry mTelemetry;
    //! The telemetry monitor which takes care of high level connects / disconnects
    private TelemetryMonitor mTelemetryMonitor;
    private final TelemetryMonitor.TelemetryMonitorListener mTelemetryMonitorListener =
            new TelemetryMonitor.TelemetryMonitorListener() {

                @Override
                public void connectionStateChanged(boolean connected) {
                    if (mTelemetryMonitor != null) {
                        if (DEBUG)
                            Log.d(TAG, "Mon updated. Connected: " + connected + " objects updated: " + mTelemetryMonitor.getObjectsUpdated());
                        if (connected) {
                            Intent intent = new Intent();
                            intent.setAction(OPTelemetryService.INTENT_ACTION_CONNECTED);
                            mTelemetryService.sendBroadcast(intent, null);
                        }
                    } else if (DEBUG) {
                        Log.d(TAG, "Update called on connection observer after telemetry monitor deleted");
                    }
                }
            };
    //! Thread to process the input stream
    private Thread mInputProcessThread;
    //! Flag to indicate a shut down was requested.  Derived classes should take care to respect this.
    private boolean mShutdown;
    //! Indicate a physical connection is established
    private boolean mConnected;
    private boolean mThreadTerminated;

    TelemetryTask(OPTelemetryService s) {
        mTelemetryService = s;
        mShutdown = false;
        mConnected = false;

        mContext = s.getApplicationContext();
        mPrefs = new AndroidGCSPrefs(mContext);

    }

    /**
     * Attempt a connection.  This method may return before the results are
     * known.
     *
     * @return False if the attempt failed and no connection will be established
     * True if the attempt succeeded but does not guarantee success
     */
    abstract boolean attemptConnection();

    /**
     * Called when a physical channel is opened
     * <p/>
     * When this method is called the derived class must have
     * created a valid inStream and outStream
     */
    void attemptSucceeded() {
        // Create a new object manager and register all objects
        // in the future the particular register method should
        // be dependent on what is connected (e.g. board and
        // version number).
        mUAVObjectManager = new UAVObjectManager();
        TelemObjectsInitialize.register(mUAVObjectManager);

        // Create the required telemetry objects attached to this
        // data stream
        mUAVTalk = new UAVTalk(mInStream, mOutStream, mUAVObjectManager);
        mTelemetry = new Telemetry(mUAVTalk, mUAVObjectManager);
        mTelemetryMonitor = new TelemetryMonitor(mUAVObjectManager, mTelemetry, mTelemetryService);

        // Create an observer to notify system of connection
        mTelemetryMonitor.addListener(mTelemetryMonitorListener);

        // add logging
        mLoggingHandler.connect(mUAVObjectManager);

        if (mPrefs.logOnConnect())
            startLogging();

        // add uavobject handler
        mUavObjectHandler.connect(mUAVObjectManager, mTelemetryService);

        // Create a new thread that processes the input bytes
        startInputProcessing();

        if (DEBUG) {
            Log.d(TAG, "Connection attempt succeeded");
        }
        Intent intent = new Intent(INTENT_ACTION_TRANSPORT_CONNECTED);
        LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
        mConnected = true;
    }

    void attemptedFailed() {
        mConnected = false;
    }

    void disconnect() {
        if (DEBUG) Log.d(TAG, "TelemetryTask signal shutdown");
        // Make the default input processing loop stop
        mShutdown = true;

        // Shut down all the attached
        if (mTelemetryMonitor != null) {
            mTelemetryMonitor.stopMonitor();
            mTelemetryMonitor.removeListener(mTelemetryMonitorListener);
            mTelemetryMonitor = null;
        }
        if (mTelemetry != null) {
            mTelemetry.stopTelemetry();
            mTelemetry = null;
        }

        if (mLoggingHandler != null) {
            mLoggingHandler.stopLogging();
        }

        if (mUavObjectHandler != null)
            mUavObjectHandler.disconnect();

        // Stop the master telemetry thread
        // Check handler is not null: if we attempt to disconnect before
        // the connect process has completed, handler may be null.
        try {
            if (mHandler != null) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        Looper looper = Looper.myLooper();
                        if (looper != null) {
                            looper.quit();
                        }
                    }
                });
            }
        } catch (RuntimeException e1) {
            e1.printStackTrace();
        }

        stopInputProcessing();

        // Make sure the input and output stream is closed
        if (mInStream != null) {
            try {
                mInStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (mOutStream != null) {
            try {
                mOutStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        // TODO: Make sure any threads for input and output are closed
    }

    boolean isTelemetryThreadRunning() {
        return !mThreadTerminated;
    }

    /**
     * Default implementation for processing input stream
     * which creates a new thread that keeps attempting
     * to read from the input stream.
     */
    private void startInputProcessing() {
        if (DEBUG) Log.d(TAG, "Start UAV talk processing thread");
        mInputProcessThread = new Thread(new processUavTalk(), "Process UAV talk");
        mInputProcessThread.start();
    }

    private void stopInputProcessing() {
        if (mInputProcessThread != null) {
            mInputProcessThread.interrupt();
            try {
                mInputProcessThread.join();
                mInputProcessThread = null;
                if (DEBUG) Log.d(TAG, "Input processing thread ended");
            } catch (InterruptedException ignored) {
            }
        }
    }

    public void startLogging() {
        mLoggingHandler.startLogging();
    }

    public void stopLogging() {
        mLoggingHandler.stopLogging();
    }

    @Override
    public void run() {
        try {
            mShutdown = false;
            mConnected = false;
            mThreadTerminated = false;

            if (DEBUG) Log.d(TAG, "TelemetryTask runnable starting");

            Looper.prepare();
            mHandler = new Handler();

            if (DEBUG) Log.d(TAG, "Attempting connection");
            if (!attemptConnection()) {
                if (DEBUG) Log.d(TAG, "Failed to connect");
                mThreadTerminated = true;
                return; // Attempt failed
            }

            if (DEBUG) Log.d(TAG, "TelemetryTask runnable running");

            Looper.loop();

            if (DEBUG) Log.d(TAG, "TelemetryTask runnable finished");

        } catch (Throwable t) {
            Log.e(TAG, "halted due to an error", t);

        }

        mTelemetryService.toastMessage("Telemetry Thread finished");
        mThreadTerminated = true;
    }

    /****
     * General accessors
     ****/

    public boolean getConnected() {
        return mConnected;
    }

    public OPTelemetryService.TelemTask getTelemTaskIface() {
        return new OPTelemetryService.TelemTask() {
            @Override
            public UAVObjectManager getObjectManager() {
                return mUAVObjectManager;
            }

            @Override
            public boolean suspendTelemetry() {
                stopInputProcessing();

                return mInputProcessThread == null;
            }

            @Override
            public boolean resumeTelemetry() {
                resumeTaskTelemetry();

                return mInputProcessThread != null;
            }

            @Override
            public int rawWriteData(ByteBuffer dataOut) {
                int retVal = -1;

                if (mInputProcessThread == null) {
                    retVal = TelemetryTask.this.rawWriteData(dataOut);
                }

                return retVal;
            }

            @Override
            public int rawReadData(ByteBuffer dataIn) {
                int retVal = -1;

                if (mInputProcessThread == null) {
                    retVal = TelemetryTask.this.rawReadData(dataIn);
                }

                return retVal;
            }
        };
    }

    /**
     * Resumes telemetry by re-starting the input processing thread.
     */
    private void resumeTaskTelemetry() {
        startInputProcessing();
    }

    /**
     * Implement this to read raw data from the flight controller using the connection
     * managed by the implementing subclass.
     *
     * @param dataIn data read from flight controller
     * @return number of bytes read or -1 if there's an error
     */
    protected abstract int rawReadData(ByteBuffer dataIn);

    /**
     * Implement this to write raw data to the flight controller using the connection
     * managed by the implementing subclass
     *
     * @param dataOut data to send to flight controller
     * @return number of bytes sent or -1 if there's an error
     */
    protected abstract int rawWriteData(ByteBuffer dataOut);

    //! Runnable to process input stream
    private class processUavTalk implements Runnable {
        @Override
        public void run() {
            if (DEBUG) Log.d(TAG, "Entering UAVTalk processing loop");
            while (!mShutdown && !Thread.currentThread().isInterrupted()) {
                try {
                    mUAVTalk.processInputStream();
                } catch (Exception e) {
                    e.printStackTrace();
                    mTelemetryService.toastMessage("Telemetry input stream interrupted");
                    break;
                }
            }
            if (DEBUG) Log.d(TAG, "UAVTalk processing loop finished");
        }
    }

}
