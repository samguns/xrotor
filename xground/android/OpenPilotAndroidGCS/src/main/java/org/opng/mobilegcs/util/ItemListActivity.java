/*
 * Eric Henaff Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import org.opng.mobilegcs.AppMenu;
import org.opng.mobilegcs.R;

public class ItemListActivity extends ConfigBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        initToolbar(R.id.toolbar);
        setTitle(mMenu.mTitle);

        FragmentManager fragmentManager = getSupportFragmentManager();

        Fragment frag = getSupportFragmentManager().findFragmentByTag(mMenu.mTitle + mMenu.getId());
        if (frag == null) {
            frag = new ItemListFragment();

            Bundle args = new Bundle();
            args.putInt(AppMenu.APP_MENU_ITEM, mMenu.getId());
            frag.setArguments(args);
        }

        fragmentManager.beginTransaction()
                .replace(R.id.container, frag, mMenu.mTitle + mMenu.getId())
                .commit();
    }
}
