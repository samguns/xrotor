/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.opng.mobilegcs.R;

import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

/**
 * This class implements a simple horizontal version of the number picker.
 * Rather than having the buttons above and below the edit control, they are
 * to the left and right.
 */
public class HorizontalNumberPickerView extends LinearLayout {

    private static final String SAVE_SUPER_INSTANCE_STATE = "SuperInstanceState";
    private static final String SAVE_VALUE = "Value";
    private final NumberFormat mNumberFormat = NumberFormat.getInstance(Locale.getDefault());
    private final char mDecimalSeparator = DecimalFormatSymbols.getInstance().getDecimalSeparator();
    private String mDecrementText;
    private String mIncrementText;
    private Drawable mDecrementDrawable;
    private Drawable mIncrementDrawable;
    private int mDecimals;
    private double mValue;
    private double mMinValue;
    private double mMaxValue;
    private double mStepSize;
    private EditText mEditText;
    private OnValueChangeListener mOnValueChangeListener;
    public HorizontalNumberPickerView(Context context) {
        super(context);
        init(null, 0);
    }
    public HorizontalNumberPickerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public HorizontalNumberPickerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    public double getValue() {
        return mValue;
    }

    public void setValue(double value) {
        mValue = value;
        if (mEditText != null && mNumberFormat != null) {
            mEditText.setText(mNumberFormat.format(mValue));
        }
    }

    public int getDecimals() {
        return mDecimals;
    }

    public void setDecimals(int decimals) {
        mDecimals = decimals;
    }

    public double getMinValue() {
        return mMinValue;
    }

    public void setMinValue(double minValue) {
        mMinValue = minValue;
    }

    public double getMaxValue() {
        return mMaxValue;
    }

    public void setMaxValue(double maxValue) {
        mMaxValue = maxValue;
    }

    public double getStepSize() {
        return mStepSize;
    }

    public void setStepSize(double stepSize) {
        mStepSize = stepSize;
    }

    public void setOnValueChangedListener(OnValueChangeListener listener) {
        mOnValueChangeListener = listener;
    }

    /**
     * Gets the decrement button text attribute value.
     *
     * @return The decrement button text attribute value.
     */
    public String getDecrementText() {
        return mDecrementText;
    }

    /**
     * Sets the view's increment button text attribute value.
     *
     * @param decrementText The increment button text attribute value to use.
     */
    public void setDecrementText(String decrementText) {
        mDecrementText = decrementText;
        invalidate();
        requestLayout();
    }

    /**
     * Gets the decrement button text attribute value.
     *
     * @return The decrement button text attribute value.
     */
    public String getIncrementText() {
        return mIncrementText;
    }

    /**
     * Sets the view's increment button text attribute value.
     *
     * @param incrementText The increment button text attribute value to use.
     */
    public void setIncrementText(String incrementText) {
        mIncrementText = incrementText;
        invalidate();
        requestLayout();
    }

    /**
     * Gets the decrement button's drawable attribute value.
     *
     * @return The decrement button's drawable attribute value.
     */
    public Drawable getDecrementDrawable() {
        return mDecrementDrawable;
    }

    /**
     * Sets the view's decrement button drawable attribute value.
     *
     * @param decrementDrawable The decrement button's drawable attribute value.
     */
    public void setDecrementDrawable(Drawable decrementDrawable) {
        mDecrementDrawable = decrementDrawable;
        invalidate();
        requestLayout();
    }

    /**
     * Gets the increment button's drawable attribute value.
     *
     * @return The increment button's drawable attribute value.
     */
    public Drawable getIncrementDrawable() {
        return mIncrementDrawable;
    }

    /**
     * Sets the view's increment button drawable attribute value.
     *
     * @param incrementDrawable The increment button's drawable attribute value.
     */
    public void setIncrementDrawable(Drawable incrementDrawable) {
        mIncrementDrawable = incrementDrawable;
        invalidate();
        requestLayout();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        for (int count = 0; count < getChildCount(); ++count) {
            getChildAt(count).setEnabled(enabled);
        }
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(SAVE_SUPER_INSTANCE_STATE, super.onSaveInstanceState());
        bundle.putDouble(SAVE_VALUE, mValue);
        bundle.putParcelable("TextState", mEditText.onSaveInstanceState());
        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            mValue = bundle.getDouble(SAVE_VALUE);
            state = bundle.getParcelable(SAVE_SUPER_INSTANCE_STATE);
            mEditText.onRestoreInstanceState(bundle.getParcelable("TextState"));
        }
        super.onRestoreInstanceState(state);
    }

    private void init(AttributeSet attrs, int defStyle) {
        // Load attributes
        final TypedArray a = getContext().obtainStyledAttributes(
                attrs, R.styleable.HorizontalNumberPickerView, defStyle, 0);

        if (a.hasValue(R.styleable.HorizontalNumberPickerView_decrementText)) {
            mDecrementText = a.getString(
                    R.styleable.HorizontalNumberPickerView_decrementText);
        }
        if (a.hasValue(R.styleable.HorizontalNumberPickerView_incrementText)) {
            mIncrementText = a.getString(
                    R.styleable.HorizontalNumberPickerView_incrementText);
        }

        if (a.hasValue(R.styleable.HorizontalNumberPickerView_decrementDrawable)) {
            mDecrementDrawable = a.getDrawable(
                    R.styleable.HorizontalNumberPickerView_decrementDrawable);
        }
        else if(Build.VERSION.SDK_INT < 21)
            //noinspection deprecation
            mDecrementDrawable = getContext().getResources().getDrawable(R.drawable.ic_remove_black_36dp);
        else
            mDecrementDrawable = getContext().getDrawable(R.drawable.ic_remove_black_36dp);

        if (mDecrementDrawable != null) {
            mDecrementDrawable.setCallback(this);
        }

        if (a.hasValue(R.styleable.HorizontalNumberPickerView_incrementDrawable)) {
            mIncrementDrawable = a.getDrawable(
                    R.styleable.HorizontalNumberPickerView_incrementDrawable);
        }
        else if(Build.VERSION.SDK_INT < 21)
            //noinspection deprecation
            mIncrementDrawable = getContext().getResources().getDrawable(R.drawable.ic_add_black_36dp);
        else
            mIncrementDrawable = getContext().getDrawable(R.drawable.ic_add_black_36dp);

        if (mIncrementDrawable != null) {
            mIncrementDrawable.setCallback(this);
        }

        mDecimals = a.getInt(R.styleable.HorizontalNumberPickerView_decimals, 3);
        mValue = a.getFloat(R.styleable.HorizontalNumberPickerView_value, 0);
        mMinValue = a.getFloat(R.styleable.HorizontalNumberPickerView_minValue, Float.NaN);
        mMaxValue = a.getFloat(R.styleable.HorizontalNumberPickerView_maxValue, Float.NaN);
        mStepSize = a.getFloat(R.styleable.HorizontalNumberPickerView_stepSize, 1);

        a.recycle();

        // Make sure we are always horizontal
        setOrientation(HORIZONTAL);

        // Set up gui
        Button decrementButton = new Button(getContext(), attrs);
        setupButton(decrementButton, mDecrementDrawable, mDecrementText);
        Button incrementButton = new Button(getContext(), attrs);
        setupButton(incrementButton, mIncrementDrawable, mIncrementText);
        mEditText = new EditText(getContext());
        mEditText.setInputType(InputType.TYPE_CLASS_NUMBER |
                InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        mEditText.setFilters(new InputFilter[]{new DecimalDigitsInputFilter()});
        mEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
        mNumberFormat.setMaximumFractionDigits(mDecimals);

        addButton(decrementButton, Gravity.START | Gravity.CENTER_VERTICAL);
        addTextControl(mEditText);
        addButton(incrementButton, Gravity.END | Gravity.CENTER_VERTICAL);
        decrementButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mValue -= mStepSize;
                if (!Double.isNaN(mMinValue)) {
                    mValue = Math.max(mValue, mMinValue);
                }
                mEditText.setText(mNumberFormat.format(mValue));
            }
        });
        incrementButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mValue += mStepSize;
                if (!Double.isNaN(mMaxValue)) {
                    mValue = Math.min(mValue, mMaxValue);
                }
                mEditText.setText(mNumberFormat.format(mValue));
            }
        });
        mEditText.setSelectAllOnFocus(true);
    }

    private void setupButton(Button button, Drawable drawable, String text) {
        if (Build.VERSION.SDK_INT >= 16) {
            button.setBackground(drawable);
        } else {
            //noinspection deprecation
            button.setBackgroundDrawable(drawable);
        }
        button.setHeight(0);
        button.setMinWidth(0);
        button.setPadding(0, 0, 0, 0);
        button.setText(text);
    }

    private void addButton(Button button, int gravity) {
        LayoutParams params = generateDefaultLayoutParams();
        params.width = 56;
        params.height = 56;
        params.gravity = gravity;
        button.setLayoutParams(params);

        addView(button);
    }

    private void addTextControl(EditText textControl) {
        LayoutParams params = generateDefaultLayoutParams();
        params.width = 0;
        params.height = LayoutParams.WRAP_CONTENT;
        params.weight = 1;

        textControl.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                if (i == EditorInfo.IME_ACTION_DONE) {
                    if (!Double.isNaN(mMaxValue)) {
                        mValue = Math.min(mValue, mMaxValue);
                    }
                    if (!Double.isNaN(mMinValue)) {
                        mValue = Math.max(mValue, mMinValue);
                    }

                    if (!textView.getText().equals(mNumberFormat.format(mValue))) {
                        mEditText.setText(mNumberFormat.format(mValue));

                        return true;
                    }
                }

                return false;
            }
        });

        textControl.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                double previous = mValue;

                boolean parseExceptionThrown = false;
                try {
                    String string = mEditText.getText().toString();

                    // Note that due to android defect 2626, only '.' is accepted by
                    // EditText as a decimal separator. We need to check the separator
                    // and modify if necessary
                    if (mDecimalSeparator != '.' && string.contains(".")) {
                        string = string.replace('.', mDecimalSeparator);
                        mEditText.setText(string);
                    }
                    mValue = mNumberFormat.parse(string).doubleValue();
                } catch (ParseException e) {
                    parseExceptionThrown = true;
                }

                if (!parseExceptionThrown) {
                    // Check limits if they've been set
                    if (!Double.isNaN(mMaxValue)) {
                        mValue = Math.min(mValue, mMaxValue);
                    }
                    if (!Double.isNaN(mMinValue)) {
                        mValue = Math.max(mValue, mMinValue);
                    }
                    notifyValueChanged(previous);
                }
            }
        });

        addView(textControl, params);
        textControl.setText(String.format(Locale.getDefault(), "%f", mValue));
        textControl.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
    }

    private void notifyValueChanged(double previousValue) {
        if (mOnValueChangeListener != null) {
            mOnValueChangeListener.onValueChange(this, previousValue, mValue);
        }
    }

    /**
     * Interface to listen for changes of the current value.
     */
    public interface OnValueChangeListener {

        /**
         * Called upon a change of the current value.
         *
         * @param picker The HorizontalNumberPickerView associated with this listener.
         * @param oldVal The previous value.
         * @param newVal The new value.
         */
        void onValueChange(HorizontalNumberPickerView picker, double oldVal, double newVal);
    }

    private class DecimalDigitsInputFilter implements InputFilter {

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            CharSequence retVal = "";
            String formattedSource = source.subSequence(start, end).toString();
            String destPrefix = dest.subSequence(0, dstart).toString();
            String destSuffix = dest.subSequence(dend, dest.length()).toString();
            String result = destPrefix + formattedSource + destSuffix;

            if (!result.isEmpty()) {
                int pointLocation = result.indexOf(mDecimalSeparator);
                if (pointLocation == -1 || (pointLocation > -1 && result.substring(pointLocation).length() <= (mDecimals + 1))) {
                    // Return unchanged
                    retVal = null;
                }
            }

            return retVal;
        }

    }

}
