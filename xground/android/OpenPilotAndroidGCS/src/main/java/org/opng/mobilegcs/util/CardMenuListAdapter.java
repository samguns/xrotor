/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import org.opng.mobilegcs.R;

import java.util.ArrayList;

public class CardMenuListAdapter extends RecyclerView.Adapter<CardMenuListAdapter.CardMenuViewHolder> {

    private ArrayList<AppMenuItem> mMenuItems;
    private Context mContext;

    public CardMenuListAdapter(AppMenuItem menu) {
        if (menu != null) {
            mMenuItems = menu.getSubMenu();
        }
    }

    private static void setListViewHeightBasedOnItems(ListView listView) {

        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter != null) {

            int numberOfItems = listAdapter.getCount();

            // Get total height of all items.
            View item = listAdapter.getView(0, null, listView);
            item.measure(0, 0);
            int totalItemsHeight = item.getMeasuredHeight() * numberOfItems;

            // Get total height of all item dividers.
            int totalDividersHeight = listView.getDividerHeight() *
                    (numberOfItems - 1);

            // Set list height.
            ViewGroup.LayoutParams params = listView.getLayoutParams();
            params.height = totalItemsHeight + totalDividersHeight;
            listView.setLayoutParams(params);
            listView.requestLayout();

        }
    }

    @Override
    public CardMenuViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mContext = parent.getContext();

        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.fragment_card_menu, parent, false);

        return new CardMenuViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CardMenuViewHolder holder, int position) {
        if (mMenuItems != null) {
            holder.vTitle.setText(mMenuItems.get(position).mTitle);
            holder.vImage.setImageResource(mMenuItems.get(position).getIcon());

            final AppMenuItem item = mMenuItems.get(position);

            holder.vList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectItem(item, position);
                }
            });

            holder.vList.setAdapter(new IconItemArrayAdapter<>(
                    mContext,
                    mMenuItems.get(position).getSubMenu(),
                    R.layout.card_menu_item));

            setListViewHeightBasedOnItems(holder.vList);
        }
    }

    @Override
    public int getItemCount() {
        return mMenuItems != null ? mMenuItems.size() : 0;
    }

    private void selectItem(AppMenuItem item, int position) {
        ActivityAppMenuItem mi = (ActivityAppMenuItem) item.getSubMenu().get(position);
        mi.Open(mContext);
    }

    public static class CardMenuViewHolder extends RecyclerView.ViewHolder {

        public final TextView vTitle;
        public final ImageView vImage;
        public final ListView vList;

        public CardMenuViewHolder(View itemView) {
            super(itemView);

            vTitle = (TextView) itemView.findViewById(R.id.cardTitle);
            vImage = (ImageView) itemView.findViewById(R.id.cardIcon);
            vList = (ListView) itemView.findViewById(R.id.cardMenuList);
        }
    }
}
