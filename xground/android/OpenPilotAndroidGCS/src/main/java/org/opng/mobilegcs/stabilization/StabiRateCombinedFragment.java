/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.ViewSwitcher;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class StabiRateCombinedFragment extends BaseFragment implements StabiGuiFragment {

    private View mRootView;
    private UAVObjectManagerLoaderHandler mUAVObjectManagerLoaderHandler;
    private ViewSwitcher mRateSwitcher;
    private CheckBox mCheckAdvanced;

    public StabiRateCombinedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_stabi_rate_combined, container, false);
        mRateSwitcher = (ViewSwitcher) mRootView.findViewById(R.id.switchRate);
        mCheckAdvanced = (CheckBox) mRootView.findViewById(R.id.checkShowAdvanced);
        setupViewSwitchers(mRateSwitcher, mCheckAdvanced);

        // Set up our loader handler: get all the controls it needs and then create it
        Button apply = (Button) mRootView.findViewById(R.id.buttonCommonFooterApply);
        Button save = (Button) mRootView.findViewById(R.id.buttonCommonFooterSave);
        Button btnDefault = (Button) mRootView.findViewById(R.id.buttonCommonFooterDefault);
        Button restore = (Button) mRootView.findViewById(R.id.buttonCommonFooterRestore);
        CheckBox autoUpdate = (CheckBox) mRootView.findViewById(R.id.checkFcUpdateRealtime);
        Spinner bankSelect = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);

        mUAVObjectManagerLoaderHandler = new UAVObjectManagerLoaderHandler(getActivity(), this, autoUpdate, bankSelect, apply, save,
                btnDefault, restore, R.integer.stabi_rate_button_group);

        getLoaderManager().initLoader(0, null, mUAVObjectManagerLoaderHandler);

        return mRootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onStop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onResume();
        }

        if (mCheckAdvanced != null && mRateSwitcher != null) {
            mRateSwitcher.setDisplayedChild(mCheckAdvanced.isChecked() ? 1 : 0);
        }
    }

    private void setupViewSwitchers(final ViewSwitcher viewSwitcher, final CheckBox checkAdvanced) {
        checkAdvanced.setVisibility(View.VISIBLE);
        checkAdvanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAdvanced.isChecked()) {
                    viewSwitcher.showNext();
                } else {
                    viewSwitcher.showPrevious();
                }
            }
        });
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        if (mRootView != null) {
            FragmentManager fragmentManager = getChildFragmentManager();
            Fragment basicRate = fragmentManager.findFragmentById(R.id.basicRate);
            Fragment advancedRate = fragmentManager.findFragmentById(R.id.advancedRate);
            ((StabiGuiFragment) basicRate).setupGuiHandler(helper);
            ((StabiGuiFragment) advancedRate).setupGuiHandler(helper);
        }
    }
}
