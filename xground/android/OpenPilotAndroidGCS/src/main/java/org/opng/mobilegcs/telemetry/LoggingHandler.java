/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

import android.os.Handler;
import android.util.Log;

import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectEventListener;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.UAVTalk;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created on 10/23/14.
 */
public class LoggingHandler {

    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    // Logging settings
    private final String TAG = LoggingHandler.class.getSimpleName();
    private final List<UAVObject> mListeningList = new ArrayList<>();
    private final FirmwareIAPObjEventListener mFirmwareIAPObjEventListener = new FirmwareIAPObjEventListener();
    private Handler mHandler;
    private UAVObjectManager mObjectManager;
    private Long mTimestamp;
    private FileOutputStream mFileOutputStream;
    private UAVTalk mUavTalk;
    private File mLogFile;
    private boolean mLogging;
    private final UAVObjectEventListener mObjUpdatedListener = new UAVObjectEventAdapter() {
        @Override
        public void objectUpdated(final UAVObject obj) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    logObject(obj);
                }
            });
        }
    };

    public void startLogging() {

        mLogging = true;

//        String logDirectory = DirectoryPath.getFlightLogPath();

        mTimestamp = System.currentTimeMillis();

//        mLogFile = new File(logDirectory, fileName);
        if (DEBUG)
            Log.d(TAG, "Trying for file: " + mLogFile.getAbsolutePath());
        try {
            mFileOutputStream = new FileOutputStream(mLogFile);
            mUavTalk = new UAVTalk(null, mFileOutputStream, mObjectManager);
            mLogging = true;

        } catch (IOException e) {
            Log.e(TAG, "Could not write file " + e.getMessage());
        }

        // Add listener to write the version header when available
        UAVObject firmwareIapObj = mObjectManager.getObject("FirmwareIAPObj");
        if (firmwareIapObj != null) {
            firmwareIapObj.addUAVObjectEventListener(mFirmwareIAPObjEventListener);
        }

        retrieveSettings();

        // Register all existing objects
        List<List<UAVObject>> objects = mObjectManager.getObjects();
        for (int i = 0; i < objects.size(); i++)
            for (int j = 0; j < objects.get(i).size(); j++)
                registerObject(objects.get(i).get(j));


    }

    public void stopLogging() {
        unregisterAllObjects();
        mLogging = false;
    }

    public void connect(UAVObjectManager objManager) {
        // We should be on our activity's thread here, get a handler for it
        this.mHandler = new Handler();
        this.mObjectManager = objManager;

    }

    public void disconnect() {
        unregisterAllObjects();

    }

    private void registerObject(UAVObject obj) {
        synchronized (mListeningList) {
            if (!mListeningList.contains(obj)) {

                obj.addUAVObjectEventListener(mObjUpdatedListener);
                mListeningList.add(obj);
            }
        }
    }


    // ! Unregister all objects from logging
    private void unregisterAllObjects() {
        synchronized (mListeningList) {
            for (int i = 0; i < mListeningList.size(); i++) {
                mListeningList.get(i).removeUAVObjectEventListener(mObjUpdatedListener);
            }
            mListeningList.clear();
        }
    }

    private void logObject(UAVObject obj) {
        if (mLogging) {
            try {
                int elapsed = (int) (System.currentTimeMillis() - mTimestamp);

                mFileOutputStream.write((byte) (elapsed & 0xff));
                mFileOutputStream.write((byte) ((elapsed & 0x0000ff00) >> 8));
                mFileOutputStream.write((byte) ((elapsed & 0x00ff0000) >> 16));
                mFileOutputStream.write((byte) ((elapsed & 0xff000000) >> 24));

                long size = obj.getNumBytes() + 10 + 1;
                mFileOutputStream.write((byte) (size & 0x00000000000000ffL));
                mFileOutputStream.write((byte) (size & 0x000000000000ff00L) >> 8);
                mFileOutputStream.write((byte) (size & 0x0000000000ff0000L) >> 16);
                mFileOutputStream.write((byte) (size & 0x00000000ff000000L) >> 24);
                mFileOutputStream.write((byte) ((size & 0x000000ff00000000L) >> 32));
                mFileOutputStream.write((byte) ((size & 0x0000ff0000000000L) >> 40));
                mFileOutputStream.write((byte) ((size & 0x00ff000000000000L) >> 48));
                mFileOutputStream.write((byte) ((size & 0xff00000000000000L) >> 56));

                mUavTalk.sendObject(obj, false, false);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private void retrieveSettings() {
        //logging will start, get settings
        List<List<UAVDataObject>> objs = mObjectManager.getDataObjects();
        for (int i = 0; i < objs.size(); i++) {
            for (int j = 0; j < objs.get(i).size(); j++) {
                UAVDataObject dataObject = objs.get(i).get(j);
                if (dataObject.isSettings()) {
                    dataObject.requestUpdate();
                }
            }
        }

        if (DEBUG)
            Log.d(TAG, "Logging: retrieve settings objects from the autopilot");


    }

    private class FirmwareIAPObjEventListener extends UAVObjectEventAdapter {
        @Override
        public void objectUpdated(UAVObject obj) {
            if (DEBUG) Log.d(TAG, "Handle FirmwareIap updated event");

            // read all settings after firmwareiap
            retrieveSettings();

            logObject(obj);

            // remove the listener
            obj.removeUAVObjectEventListener(this);

        }
    }


}
