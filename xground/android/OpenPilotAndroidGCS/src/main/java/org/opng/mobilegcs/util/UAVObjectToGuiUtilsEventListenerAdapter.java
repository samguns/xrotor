/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.view.View;

public class UAVObjectToGuiUtilsEventListenerAdapter implements
        UAVObjectToGuiUtilsEventListener {

    @Override
    public void viewContentsChanged(View view, Variant value) {
    }

    @Override
    public void populateViewsRequested() {
    }

    @Override
    public void refreshViewsValuesRequested() {
    }

    @Override
    public void updateObjectsFromViewsRequested() {
    }

    @Override
    public void autoPilotConnected() {
    }

    @Override
    public void autoPilotDisconnected() {
    }

    @Override
    public void defaultRequested(int group) {
    }

    @Override
    public void beforeSave() {

    }

    @Override
    public void afterSave() {

    }

}
