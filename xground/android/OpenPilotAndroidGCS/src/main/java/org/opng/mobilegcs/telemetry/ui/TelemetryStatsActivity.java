/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry.ui;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.ActivityTelemetryConnectionHandler;
import org.opng.mobilegcs.util.TelemetryActivity;

public class TelemetryStatsActivity extends TelemetryActivity implements TelemetryStatsFragment.TelemetryStatsFragmentListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionProvidersToShow(ActivityTelemetryConnectionHandler.SYSTEM_ALARMS);
        setContentView(R.layout.activity_telemetry_stats);

        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBarToolbar);
    }

    @Override
    public boolean onRequestIsGcsStats(int id) {
        return id == R.id.fragmentGcs;
    }
}
