/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class CruiseControlFragment extends BaseFragment implements StabiGuiFragment {
    private UAVObjectManagerLoaderHandler mUAVObjectManagerLoaderHandler;
    private View mRootView;

    public CruiseControlFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_cruise_control, container, false);

        // Set up our loader handler: get all the controls it needs and then create it
        Button apply = (Button) mRootView.findViewById(R.id.buttonCommonFooterApply);
        Button save = (Button) mRootView.findViewById(R.id.buttonCommonFooterSave);
        Button btnDefault = (Button) mRootView.findViewById(R.id.buttonCommonFooterDefault);
        Button restore = (Button) mRootView.findViewById(R.id.buttonCommonFooterRestore);
        CheckBox autoUpdate = (CheckBox) mRootView.findViewById(R.id.checkFcUpdateRealtime);
        Spinner bankSelect = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);

        mUAVObjectManagerLoaderHandler = new UAVObjectManagerLoaderHandler(getActivity(), this, autoUpdate, bankSelect, apply, save,
                btnDefault, restore, R.integer.stabi_cruise_control_button_group);

        getLoaderManager().initLoader(0, null, mUAVObjectManagerLoaderHandler);

        return mRootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onStop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onResume();
        }
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        if (mRootView != null) {
            helper.bindCruiseControlWidgets(mRootView.findViewById(R.id.textCruiseMaxPowerFactor),
                    mRootView.findViewById(R.id.textCruiseMaxAngle),
                    mRootView.findViewById(R.id.textCruiseMinThrust),
                    mRootView.findViewById(R.id.textCruiseInvertedThrustReverse),
                    mRootView.findViewById(R.id.textCruisePwrDelayComp),
                    mRootView.findViewById(R.id.textCruisePowerTrim),
                    mRootView.findViewById(R.id.textCruiseMaxThrust),
                    mRootView.findViewById(R.id.textCruiseInvertedPwr));
        }
    }
}
