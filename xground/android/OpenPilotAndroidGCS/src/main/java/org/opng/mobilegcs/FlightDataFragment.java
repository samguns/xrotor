/*
 * Eric Henaff Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import org.opng.mobilegcs.util.AppMenuItem;
import org.opng.mobilegcs.util.AppMenuItemFragmentOperations;
import org.opng.mobilegcs.util.BaseFragment;
import org.opng.mobilegcs.util.IconItemArrayAdapter;

public class FlightDataFragment extends BaseFragment {

    private static final String STATE_CURRENT_DISPLAY = "StateCurrentDisplay";

    private ListView menuList;
    private View menuListContainer;
    private FloatingActionButton fap;
    private int currentDisplay = 0;

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt(STATE_CURRENT_DISPLAY, currentDisplay);
        super.onSaveInstanceState(outState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_flight_data, container, false);

        // Init Floating button
        fap = (FloatingActionButton) view.findViewById(R.id.fab);
        fap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMenu();
                hideFap();
            }
        });

        // Init menu
        menuListContainer = view.findViewById(R.id.displayMenuContainer);
        menuList = (ListView) view.findViewById(R.id.displayMenu);

        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                menuList.setItemChecked(position, true);
                currentDisplay = position;
                selectItem(mMenu.getSubMenu().get(position));
            }
        });

        menuList.setAdapter(new IconItemArrayAdapter<>(
                getContext(),
                mMenu.getSubMenu(),
                R.layout.menu_item));

        menuListContainer.setVisibility(View.GONE);
        closeMenu();

        view.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (view != menuList && fap.getVisibility() == View.GONE) {
                    closeMenu();
                    showFap();
                }
                return false;
            }
        });

        // Select default menu item
        if (savedInstanceState != null)
            currentDisplay = savedInstanceState.getInt(STATE_CURRENT_DISPLAY, 0);

        //menuList.setSelection(currentDisplay);
        menuList.setItemChecked(currentDisplay, true);
        selectItem(mMenu.getSubMenu().get(currentDisplay));

        return view;
    }

    private void selectItem(AppMenuItem item) {
        closeMenu();
        showFap();

        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment frag;
        // Requires API 19+
        //noinspection TryWithIdenticalCatches
        try {
            frag = ((AppMenuItemFragmentOperations) item).getFragment();

            fragmentManager.beginTransaction()
                    .replace(R.id.container, frag)
                    .commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        }
    }

    private void openMenu() {
        menuListContainer.animate()
                .translationY(0)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationEnd(animation);
                        menuListContainer.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void closeMenu() {
        menuListContainer.animate()
                .translationY(menuList.getHeight())
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        menuListContainer.setVisibility(View.GONE);
                    }
                });
    }

    private void hideFap() {
        fap.animate()
                .scaleX(0)
                .scaleY(0)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        fap.setVisibility(View.GONE);
                    }
                });
    }

    private void showFap() {
        fap.animate()
                .scaleX(1)
                .scaleY(1)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationEnd(animation);
                        fap.setVisibility(View.VISIBLE);
                    }
                });
    }

}
