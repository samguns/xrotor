/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.GCSTelemetryStats;

/**
 * A simple {@link android.app.Fragment} subclass.
 */
public class TelemetryStatsFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {

    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final String ARG_IS_GCS_STATS = "isTx";
    private static boolean WARN = LOGLEVEL > 0;
    private final String TAG = "TelemetryStatsFragment";
    private boolean mIsGcsStats = false;
    private UAVObjectToGuiUtils mUAVObjectToGuiUtils;
    private View mRootView;
    public TelemetryStatsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mIsGcsStats = getArguments().getBoolean(ARG_IS_GCS_STATS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_telemetry_stats, container, false);

        getLoaderManager().initLoader(0, null, this);

        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            TelemetryStatsFragmentListener listener = (TelemetryStatsFragmentListener) context;
            mIsGcsStats = listener.onRequestIsGcsStats(getId());
        } catch (ClassCastException ex) {
            throw new ClassCastException(context.toString() + " must implement TelemetryStatsFragmentListener");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DEBUG) Log.d(TAG, "Resuming");
        enableObjectUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (DEBUG) Log.d(TAG, "Stopping");
        disableObjectUpdates();
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int id, Bundle args) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> loader, UAVObjectManager data) {
        UAVObject telemetryObject = data.getObject(mIsGcsStats ? "GCSTelemetryStats" : "FlightTelemetryStats");
        mUAVObjectToGuiUtils = new UAVObjectToGuiUtils(getActivity(), data, data.getObjectUtilManager());

        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_TXDATARATE),
                mRootView.findViewById(R.id.textViewTxDataRate), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_TXBYTES),
                mRootView.findViewById(R.id.textViewTxBytes), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_TXFAILURES),
                mRootView.findViewById(R.id.textViewTxFailures), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_TXRETRIES),
                mRootView.findViewById(R.id.textViewTxRetries), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_RXDATARATE),
                mRootView.findViewById(R.id.textViewRxDataRate), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_RXBYTES),
                mRootView.findViewById(R.id.textViewRxBytes), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_RXFAILURES),
                mRootView.findViewById(R.id.textViewRxFailures), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_RXSYNCERRORS),
                mRootView.findViewById(R.id.textViewRxSyncFailures), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(telemetryObject, telemetryObject.getField(GCSTelemetryStats.FIELD_RXCRCERRORS),
                mRootView.findViewById(R.id.textViewRxCrcErrors), 0, 1, false, null, 0);
        mUAVObjectToGuiUtils.populateViews();
        mUAVObjectToGuiUtils.enableObjUpdates();
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> loader) {

    }

    private void enableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.enableObjUpdates();
        }
    }

    private void disableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.disableObjUpdates();
        }
    }

    public interface TelemetryStatsFragmentListener {
        boolean onRequestIsGcsStats(int id);
    }
}
