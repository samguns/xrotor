/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.firmware.gui;

import android.content.Context;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.firmware.UploaderEventsListener;
import org.opng.mobilegcs.firmware.flightcontroller.DeviceInfo;
import org.opng.mobilegcs.firmware.flightcontroller.Uploader;
import org.opng.mobilegcs.io.IAP;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Defines a fragment that can be used to upload firmware to a connected device.
 * By default, firmware embedded in the app will be uploaded but this fragment
 * can also upload firmware chosen by the user.
 */
public class UploaderActivityFragment extends Fragment {
    private final Handler mHandler = new Handler();
    // --Commented out by Inspection (31/12/15 00:46):private static final String TAG = UploaderActivityFragment.class.getSimpleName();
    private Uploader mUploader;
    private boolean mBootSucceeded;
    private FloatingActionButton mFlashActionButton;
    private ProgressBar mFlashProgressBar;
    private DevicesFragment mDevicesFragment;
    private TextView mDebugMessages;
    private OnUploaderActivityFragmentInteractionListener mListener;
    private TextView mTextViewFirmware;

    public UploaderActivityFragment() {
    }

    public boolean isTelemetryConnected() {
        return mUploader != null && mUploader.isTelemetryConnected();
    }

    public void halt() {
        if (mUploader != null) {
            mUploader.systemHalt();
        }
    }

    public void reset() {
        if (mUploader != null) {
            mUploader.systemReset();
        }
    }

    public void reboot() {
        if (mUploader != null) {
            mUploader.systemBoot();
        }
    }

    public void rebootDefault() {
        if (mUploader != null) {
            mUploader.systemSafeBoot();
        }
    }

    public void rebootErase() {
        if (mUploader != null) {
            // TODO: pop up warning dialog as this will erase user settings
            mUploader.systemEraseBoot();
        }
    }

    public boolean connect() {
        boolean retVal = false;
        if (!mBootSucceeded) {
            mUploader.connectToBootloaderWithUsb();
            retVal = true;
        } else {
            uploadGuiCleanup();
        }

        return retVal;
    }

    public void setFirmwareFilename(String firmwareFilename) {
        mTextViewFirmware.setText(firmwareFilename);
        mDevicesFragment.getSelectedDevice().setFirmwareFilename(firmwareFilename);
    }

    public DeviceInfo getSelectedDevice() {
        return mDevicesFragment != null ? mDevicesFragment.getSelectedDevice() : null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View retVal = inflater.inflate(R.layout.fragment_uploader, container, false);

        mFlashActionButton = (FloatingActionButton) retVal.findViewById(R.id.buttonFlashFirmware);
        mFlashProgressBar = (ProgressBar) retVal.findViewById(R.id.progressFlash);
        mDebugMessages = (TextView) retVal.findViewById(R.id.textUploaderDebugMessages);
        mTextViewFirmware = (TextView) retVal.findViewById(R.id.textViewFimwareFile);

        return retVal;
    }

    @Override
    public void onAttach(final Context context) {
        super.onAttach(context);
        try {
            mListener = (OnUploaderActivityFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mUploader = new Uploader(context);
        mUploader.addUploaderEventListener(new UploaderEventsListener() {
            @Override
            public void bootProgress(final Uploader.ProgressStep progressStep, final int progress) {
                switch (progressStep) {
                    case BOOTING:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_boot_start) + "\n");
                            }
                        });
                        break;
                    case BOOTING_AND_ERASING:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_boot_and_erase_start) + "\n");
                            }
                        });
                        break;
                    case FAILURE:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_boot_fail) + "\n");
                            }
                        });
                        break;
                    case JUMP_TO_BL:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(String.format(context.getString(R.string.message_uploader_starting_bootloader), progress) + "\n");
                            }
                        });
                        if (progress == 1) {
                            mFlashProgressBar.setMax(5);
                            mFlashProgressBar.setVisibility(View.VISIBLE);
                        } else {
                            mFlashProgressBar.setProgress(progress);
                        }
                        break;
                    case LOADING_FW:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_loading_firmware) + "\n");
                            }
                        });
                        break;
                    case UPLOADING_DESC:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_flash_description) + "\n");
                            }
                        });
                        break;
                    case SUCCESS:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_boot_success) + "\n");
                            }
                        });
                        break;
                    case UPLOADING_FW:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_flash_firmware) + "\n");
                            }
                        });
                        break;
                    case WAITING_CONNECT:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_wait_for_connection) + "\n");
                            }
                        });
                        break;
                    case WAITING_DISCONNECT:
                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                mDebugMessages.append(context.getString(R.string.message_uploader_wait_for_disconnect) + "\n");
                            }
                        });
                        break;
                }
            }

            @Override
            public void bootSucceeded() {
                mFlashProgressBar.setVisibility(View.INVISIBLE);
                mBootSucceeded = true;
                if (!mUploader.isResetOnly()) {
                    initDevicesFragment(mUploader.getDevices());
                }

                mListener.onUploaderReady();
            }

            @Override
            public void bootFailed() {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mFlashProgressBar.setVisibility(View.INVISIBLE);
                    }
                });
            }

            @Override
            public void updateProgress(int progress) {
                if (mFlashProgressBar != null) {
                    mFlashProgressBar.setIndeterminate(false);
                    mFlashProgressBar.setProgress(progress);
                }
            }

            @Override
            public void downloadFinished(IAP.Status status) {

            }

            @Override
            public void uploadFinished(IAP.Status status) {
                mFlashProgressBar.setVisibility(View.INVISIBLE);

                if (status == IAP.Status.LAST_OPERATION_SUCCESS) {
                    mFlashActionButton.setImageResource(R.drawable.ic_done_black_36dp);
                    mFlashActionButton.setEnabled(false);
                } else {
                    mFlashActionButton.setImageResource(R.drawable.ic_clear_black_36dp);
                }

                mDebugMessages.append(String.format(context.getString(R.string.message_upload_activity_complete), IAP.statusToString(status)));
            }

            @Override
            public void updateOperationProgress(final String status) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mDebugMessages.append(status + "\n");
                    }
                });
            }

            @Override
            public void debugMessage(String message) {
                mDebugMessages.append(message);
            }

            @Override
            public void runningFlightCode() {
                uploadGuiCleanup();
            }
        });
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mUploader != null) {
            mUploader.cleanup();
        }

        mListener = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mUploader.disconnectUsb();
    }

    private void uploadGuiCleanup() {
        mListener.onUploaderFinished();
        mTextViewFirmware.setText(getContext().getString(R.string.text_default_firmware));
        mFlashActionButton.setVisibility(View.INVISIBLE);
        mUploader.disconnectUsb();
        mBootSucceeded = false;
        getActivity().getSupportFragmentManager()
                .beginTransaction().remove(mDevicesFragment).commit();
        mDebugMessages.setText("");
        mDevicesFragment = null;
    }

    private void initDevicesFragment(List<IAP.Device> devices) {
        if (!devices.isEmpty()) {
            mDevicesFragment = DevicesFragment.newInstance(createDeviceInfoList(devices));

            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.devicesList, mDevicesFragment).commit();

            mFlashActionButton.setImageResource(R.drawable.ic_flash_black_36dp);
            mFlashActionButton.setEnabled(true);
            mFlashActionButton.setVisibility(View.VISIBLE);
            mFlashActionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDevicesFragment != null) {
                        DeviceInfo deviceInfo = mDevicesFragment.getSelectedDevice();

                        if (deviceInfo != null) {
                            mFlashProgressBar.setIndeterminate(true);
                            mFlashProgressBar.setMax(100);
                            mFlashProgressBar.setProgress(0);
                            mFlashProgressBar.setVisibility(View.VISIBLE);

                            String firmwareFilename = deviceInfo.getFirmwareFilename();

                            // TODO: turn on firmware verification once upload and CRC check are stable
                            if (firmwareFilename != null && !firmwareFilename.isEmpty() && new File(firmwareFilename).exists()) {
                                mUploader.uploadFirmwareFromFilename(firmwareFilename, deviceInfo.getDeviceIndex(), false);
                            } else {
                                mUploader.uploadFirmwareFromResourceId(deviceInfo.getFirmwareResId(), deviceInfo.getDeviceIndex(), false);
                            }
                        }
                    }
                }
            });

            mDevicesFragment.setItemClickedCallback(new DevicesFragment.ItemClickedCallback() {
                @Override
                public void itemClicked(DeviceInfo clickedDeviceInfo) {
                    String firmwareFilename = clickedDeviceInfo.getFirmwareFilename();
                    if (firmwareFilename != null && !firmwareFilename.isEmpty()) {
                        mTextViewFirmware.setText(firmwareFilename);
                    } else {
                        mTextViewFirmware.setText(getContext().getString(R.string.text_default_firmware));
                    }
                }
            });
        }
    }

    private ArrayList<DeviceInfo> createDeviceInfoList(List<IAP.Device> devices) {
        ArrayList<DeviceInfo> retVal = new ArrayList<>();

        int curIndex = 0;
        for (IAP.Device curDevice : devices) {
            final DeviceInfo deviceInfo = new DeviceInfo(curDevice, curIndex++);
            deviceInfo.setSerialNumber(mUploader.getSerialNumber());
            retVal.add(deviceInfo);
        }

        return retVal;
    }

    /**
     * Owning activity must implement this interface. This will allow this fragment
     * to notify its parent activity of any changes in the uploader's readiness.
     */
    public interface OnUploaderActivityFragmentInteractionListener {
        /**
         * Called to indicate that the uploader is ready to be used to upload
         * firmware. This will be called when the device successfully reboots
         * to the bootloader.
         */
        void onUploaderReady();

        /**
         * Called to indicate that the uploader is no longer ready to be used.
         * This will be called when the device is disconnected.
         */
        void onUploaderFinished();
    }
}
