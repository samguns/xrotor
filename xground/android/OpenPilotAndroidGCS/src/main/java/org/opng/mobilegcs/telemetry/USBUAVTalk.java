/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

// Code based on notes from http://torvafirmus-android.blogspot.com/2011/09/implementing-usb-hid-interface-in.html
// Taken 2012-08-10

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.util.Log;

import org.opng.mobilegcs.drivers.cdc.CdcSerialDriver;
import org.opng.mobilegcs.io.UsbConnectionHandler;

import java.io.IOException;
import java.nio.ByteBuffer;

public class USBUAVTalk extends TelemetryTask {

    private static final String TAG = USBUAVTalk.class.getSimpleName();
    private static final int LOGLEVEL = 3;
    public static final boolean WARN = LOGLEVEL > 1;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean ERROR = LOGLEVEL > 0;
    private static final int IO_TIMEOUT = 200;
    private CdcSerialDriver.CdcSerialPort mPort;
    private CdcSerialDriver mCdcSerialDriver;
    private final UsbConnectionHandler mUsbConnectionHandler = new UsbConnectionHandler(new UsbConnectionHandler.UsbConnectionHandlerCallback() {
        @Override
        public boolean validated() {
            boolean retVal = false;

            // See if we need to reconnect by starting a new telemetry thread
            // to run this task. If we are already running in a thread, request
            // permission connect to the device, otherwise exit here and wait for
            // the connection process to come back round.
            if (isTelemetryThreadRunning()) {
                if (DEBUG)
                    Log.d(TAG, "Telemetry thread currently running: requesting USB connect permission");
                retVal = true;
            } else {
                if (DEBUG)
                    Log.d(TAG, "Telemetry thread currently not running: requesting reconnect");
                mTelemetryService.requestReconnect();
            }

            return retVal;
        }

        @Override
        public boolean connectToDevice(UsbDevice device, UsbManager usbManager, boolean allowedDevice) {
            boolean retVal = false;
            if (allowedDevice) {
                if (DEBUG) Log.d(TAG, "connectToDeviceInterface: CDC Serial");
                retVal = connectToCDCSerialDevice(device, usbManager);
            } else {
                mTelemetryService.toastMessage("Unable to locate USB Device, ensure Serial configuration.");
            }
            return retVal;
        }

        @Override
        public void disconnectDevice() {
            disconnect();
        }
    });

    public USBUAVTalk(OPTelemetryService service) {
        super(service);
    }

    @Override
    void disconnect() {
        if (DEBUG) Log.d(TAG, "USBUAVTalk shutting down");
        mUsbConnectionHandler.disconnect(mTelemetryService);

        super.disconnect();

        if (mCdcSerialDriver != null) {
            mCdcSerialDriver.shutdown();
            mCdcSerialDriver = null;
        }
    }

    @Override
    protected int rawReadData(ByteBuffer dataIn) {
        int retVal = -1;

        if (mPort != null) {
            try {
                retVal = mPort.read(dataIn, IO_TIMEOUT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return retVal;
    }

    @Override
    protected int rawWriteData(ByteBuffer dataOut) {
        int retVal = 1;
        if (mPort != null) {
            if (VERBOSE) Log.d(TAG, "rawWriteData() Writing to device()");
            try {
                mPort.write(dataOut, IO_TIMEOUT);
            } catch (IOException e) {
                e.printStackTrace();
                retVal = -1;
            }
        }
        return retVal;
    }

    @Override
    boolean attemptConnection() {
        return mUsbConnectionHandler.initUsb(mTelemetryService);
    }

    private boolean connectToCDCSerialDevice(UsbDevice connectDevice, UsbManager usbManager) {
        boolean retVal = true;

        mCdcSerialDriver = new CdcSerialDriver(usbManager, connectDevice);

        int comBaudSpeed = mPrefs.getComBaudSpeed();
        mTelemetryService.toastMessage("Connect request at " + comBaudSpeed);

        try {
            mPort = mCdcSerialDriver.getPorts().get(0);
            mPort.open();
            mCdcSerialDriver.setPortParameters(comBaudSpeed, CdcSerialDriver.DATA_BITS_8, CdcSerialDriver.STOP_BITS_1, CdcSerialDriver.PARITY_NONE);
            mCdcSerialDriver.setDtr(true);
            mInStream = mPort.getInputStream();
            mOutStream = mPort.getOutputStream();
            mCdcSerialDriver.startAsyncComms();
        } catch (IOException e1) {
            if (mPort != null) {
                mTelemetryService.toastMessage("USB Connection Failed");
                mPort.close();
            }
            e1.printStackTrace();
            retVal = false;
        }

        if (mPort != null) {
            // This method is running on the main telemetry service thread but we want to
            // call attemptSucceeded on the USBUAVTalk telemetry task thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    attemptSucceeded();
                }
            });

            mTelemetryService.toastMessage("CDC Device Opened");
        }

        return retVal;
    }

    //    @Override
//    protected void startInputProcessing() {
//        if (mStoppedInputProcessing) {
//            inStream = mPort.getInputStream();
//            outStream = mPort.getOutputStream();
//            mCdcSerialDriver.startAsyncComms();
//        }
//        super.startInputProcessing();
//    }
}
