/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.preferences;


import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.support.v7.preference.EditTextPreference;
import android.support.v7.preference.ListPreference;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceCategory;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;

import org.opng.mobilegcs.BuildConfig;
import org.opng.mobilegcs.R;
import org.opng.mobilegcs.telemetry.UAVTalkConnectionTypes;

import java.util.Set;

public class GCSPreferencesFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = GCSPreferencesFragment.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;

    public static boolean WARN = LOGLEVEL > 0;
    private PreferenceCategory mConnCategory;
    private BluetoothDevicePreference mBtDevicesList;
    private EditTextPreference mIpAddressEditText;
    private EditTextPreference mPortEditText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Activity activity = getActivity();

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.preferences);
        mConnCategory = (PreferenceCategory) findPreference("pref_connection_settings");
        mBtDevicesList = (BluetoothDevicePreference) findPreference("pref_bt_devices");
        mIpAddressEditText = (EditTextPreference) findPreference("pref_tcp_ip_address");
        mPortEditText = (EditTextPreference) findPreference("pref_tcp_port");


        disableAllControls();
        handleControls();

        //Values generated from buid.gradle file

        try {
            Preference versionPref = findPreference("pref_version");
            if (versionPref != null) {
                String versionCode = BuildConfig.BUILD_NAME;
                String buildType = BuildConfig.BUILD_TYPE;

                versionPref.setSummary(versionCode + "-" + buildType);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Preference versionPref = findPreference("pref_firmware_version");
            if (versionPref != null) {
                versionPref.setSelectable(true);
                String versionName = PreferenceManager.getDefaultSharedPreferences(getActivity()).
                        getString("pref_firmware_version", activity.getString(R.string.prefs_fragment_no_device));
                versionPref.setSummary(versionName);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Preference versionPref = findPreference("pref_uavo_version");
            if (versionPref != null) {

                String firmwareVersion = BuildConfig.UAVO_HASH;
                versionPref.setSummary(firmwareVersion);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        findPreference("pref_storage").setSummary(DirectoryPath.getAppPath());
//        findPreference("pref_log_storage").setSummary(DirectoryPath.getFlightLogPath());
    }

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

    }

    private void handleControls() {
        int connTypeInt = Integer.parseInt(((ListPreference) findPreference("pref_connection_type")).getValue());
        switch (connTypeInt) {
            case UAVTalkConnectionTypes.UAVTALK_CONNECTION_BLUETOOTH:
                enableBluetooth();
                mBtDevicesList.discoverBlueTooth();
                mConnCategory.removePreference(mIpAddressEditText);
                mConnCategory.removePreference(mPortEditText);
                mBtDevicesList.setEnabled(true);
                break;
            case UAVTalkConnectionTypes.UAVTALK_CONNECTION_TCP:
                mConnCategory.removePreference(mBtDevicesList);
                if (mIpAddressEditText != null) mIpAddressEditText.setEnabled(true);
                if (mPortEditText != null) mPortEditText.setEnabled(true);
                break;
            case UAVTalkConnectionTypes.UAVTALK_CONNECTION_USB:
                mConnCategory.removePreference(mIpAddressEditText);
                mConnCategory.removePreference(mPortEditText);
                mConnCategory.removePreference(mBtDevicesList);
                break;
        }


    }

    private void disableAllControls() {
        mBtDevicesList.setEnabled(false);
        if (mIpAddressEditText != null) mIpAddressEditText.setEnabled(false);
        if (mPortEditText != null) mPortEditText.setEnabled(false);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        SharedPreferences.Editor e = sharedPreferences.edit();
        if (key.equals("pref_bt_devices")) {
            String keys = sharedPreferences.getString(key, "");
            String bt_device_name = "None";
            Set<BluetoothDevice> pairedDevices = mBtDevicesList.getBondedDevices();
            for (BluetoothDevice device : pairedDevices) {

                if (device.getAddress().equals(keys)) {
                    bt_device_name = device.getName();
                    break;
                }

            }

            mBtDevicesList = (BluetoothDevicePreference) findPreference("pref_bt_devices");
            mBtDevicesList.setSummary(bt_device_name);

        } else if (key.equals("pref_connection_type")) {

            disableAllControls();
            String keys = sharedPreferences.getString(key, "");

            int connectionType = Integer.parseInt(keys);
            if (DEBUG) Log.d(TAG, "selected : " + connectionType);
            switch (connectionType) {
                case 0:
                    // bluetooth
                    enableBluetooth();
                    mBtDevicesList.discoverBlueTooth();

                    e.putInt("connection_type", UAVTalkConnectionTypes.UAVTALK_CONNECTION_BLUETOOTH);
                    mConnCategory.removePreference(mIpAddressEditText);
                    mConnCategory.removePreference(mPortEditText);
                    mConnCategory.addPreference(mBtDevicesList);
                    mBtDevicesList.setEnabled(true);
                    mBtDevicesList.show();
                    break;
                case 1:
                    // tcp
                    e.putInt("connection_type", UAVTalkConnectionTypes.UAVTALK_CONNECTION_TCP);
                    mConnCategory.removePreference(mBtDevicesList);
                    mConnCategory.addPreference(mIpAddressEditText);
                    mConnCategory.addPreference(mPortEditText);
                    mIpAddressEditText.setEnabled(true);
                    mPortEditText.setEnabled(true);
                    break;
                case 2:
                    //hid
                    mConnCategory.removePreference(mIpAddressEditText);
                    mConnCategory.removePreference(mPortEditText);
                    mConnCategory.removePreference(mBtDevicesList);
                    e.putInt("connection_type", UAVTalkConnectionTypes.UAVTALK_CONNECTION_USB);

                    break;
            }
            e.apply();

        }
    }

    @Override
    public void onResume() {
        super.onResume();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        super.onPause();
        // Set up a listener whenever a key changes
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onDestroy() {
        super.onPause();
    }

    private void enableBluetooth() {

        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        startActivityForResult(enableBtIntent, 100);

    }


}
