/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.flightcontroller;

import android.util.Log;

import org.opng.mobilegcs.util.Variant;

class IAPStateReady extends IAPState {
    private static final String TAG = IAPStateReady.class.getSimpleName();

    public IAPStateReady(IAPState state) {
        super(state);
    }

    public IAPStateReady(Uploader uploader) {
        super(uploader);
    }

    @Override
    public void process(boolean success) {
        // Setup IAP command
        setIapCommand("1122");
        mFirmwareIAPObj.getField("BoardRevision").setValue(new Variant(0.0));
        mFirmwareIAPObj.getField("BoardType").setValue(new Variant(0.0));

        // Set up handler for transaction complete
        initHandleTransactionComplete();

        // Set next state
        mUploader.setCurrentIAPState(new IAPStateStepOne(this));

        // Reset the reset only flag to false
        mUploader.setIsResetOnly(false);

        // Update IAP object in flight controller
        if (DEBUG) Log.d(TAG, "Sending IAP command 1122");
        mFirmwareIAPObj.updated();
        mUploader.fireBootProgress(Uploader.ProgressStep.JUMP_TO_BL, 1);
    }
}
