/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.txpid;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Switch;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.uavtalk.UAVObjectManager;

/**
 * A simple {@link Fragment} subclass.
 */
public class TxPIDFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {

    private View mRootView;
    private UAVObjectToGuiUtils mUAVObjectToGuiUtils;
    private Button mButtonApply;
    private Button mButtonSave;
    private Switch mSwitchEnable;

    public TxPIDFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_tx_pid, container, false);

        getLoaderManager().initLoader(0, null, this);

        mButtonApply = (Button) mRootView.findViewById(R.id.buttonApply);
        mButtonSave = (Button) mRootView.findViewById(R.id.buttonSave);
        mSwitchEnable = (Switch) mRootView.findViewById(R.id.switchEnableTxPID);

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        enableObjectUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        disableObjectUpdates();
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int i, Bundle bundle) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> uavObjectManagerLoader, UAVObjectManager uavObjectManager) {

        if (uavObjectManager != null) {
            // Bind widgets to uavobjects
            mUAVObjectToGuiUtils = new UAVObjectToGuiUtils(getActivity(), uavObjectManager, uavObjectManager.getObjectUtilManager());
            mUAVObjectToGuiUtils.addApplySaveButtons(mButtonApply, mButtonSave);

            // Create TxPID gui helper and set up controls
            TxPidGuiHelper _TxPidGuiHelper = new TxPidGuiHelper(uavObjectManager, mUAVObjectToGuiUtils, mSwitchEnable);

            _TxPidGuiHelper.bindBankNumberWidget(mRootView.findViewById(R.id.spinnerPidBank));

            _TxPidGuiHelper.bindPidWidgets(mRootView.findViewById(R.id.spinnerPidOption1),
                    mRootView.findViewById(R.id.spinnerCtrlSource1),
                    mRootView.findViewById(R.id.editTextMin1),
                    mRootView.findViewById(R.id.editTextMax1), 0);
            _TxPidGuiHelper.bindPidWidgets(mRootView.findViewById(R.id.spinnerPidOption2),
                    mRootView.findViewById(R.id.spinnerCtrlSource2),
                    mRootView.findViewById(R.id.editTextMin2),
                    mRootView.findViewById(R.id.editTextMax2), 1);
            _TxPidGuiHelper.bindPidWidgets(mRootView.findViewById(R.id.spinnerPidOption3),
                    mRootView.findViewById(R.id.spinnerCtrlSource3),
                    mRootView.findViewById(R.id.editTextMin3),
                    mRootView.findViewById(R.id.editTextMax3), 2);

            _TxPidGuiHelper.bindThrottleRangeWidgets(mRootView.findViewById(R.id.editTextThrottleMin),
                    mRootView.findViewById(R.id.editTextThrottleMax));

            _TxPidGuiHelper.bindUpdateModeWidget(mRootView.findViewById(R.id.spinnerUpdateMode));

            // Update widgets and objects
            mUAVObjectToGuiUtils.populateViews();
            mUAVObjectToGuiUtils.enableObjUpdates();

            _TxPidGuiHelper.refreshValues();
        }
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> uavObjectManagerLoader) {

    }

    private void enableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.enableObjUpdates();
        }
    }

    private void disableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.disableObjUpdates();
        }
    }
}
