/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.optune;

import android.os.Parcel;
import android.util.Log;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.wizard.WizardState;
import org.opng.uavtalk.uavobjects.TxPIDSettings;

class AcquireRollUovState extends WizardState implements OPTuneFragment.OPTuneWizardState {
    public static final Creator<AcquireRollUovState> CREATOR = new Creator<AcquireRollUovState>() {
        @Override
        public AcquireRollUovState createFromParcel(Parcel source) {
            return new AcquireRollUovState(source);
        }

        @Override
        public AcquireRollUovState[] newArray(int size) {
            return new AcquireRollUovState[size];
        }
    };
    private final static String TAG = AcquireRollUovState.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private OPTuneFragment mOwner;

    AcquireRollUovState(WizardState previousState, OPTuneFragment owner) {
        super(previousState);
        mOwner = owner;
    }

    private AcquireRollUovState(Parcel source) {
        super(source);
    }

    @Override
    public WizardState next() {
        if (DEBUG) Log.d(TAG, "AcquireRollUovState: next");

        // We want to restore the starting roll rate Kp and store our
        // UOV for roll as we're about to either process pitch or calculate
        // our tuned PIDs
        mOwner.saveUovAndRestoreKp(OPTuneFragment.Axis.ROLL);

        return new AcquirePitchUovState(this, mOwner);
    }

    @Override
    public void process() {
        if (DEBUG) Log.d(TAG, "AcquireRollUovState: process");

        mOwner.setInstructions(String.format(mOwner.getString(R.string.text_optune_wizard_acquire_uov), "Roll"));

        // We are adjusting roll rate Kp
        mOwner.getTxPidGuiHelper().setPIDOption(TxPIDSettings.PIDS_ROLL_RATE_KP, 0);
        mOwner.getRadioGroupAxes().check(R.id.radioRoll);

        // Zero Ki & Kd. Note that this method will only do this once
        // when we start to process the first axis
        mOwner.zeroKiKd();
        mOwner.getTxPidGuiHelper().saveChangesToRam();
    }

    @Override
    public void setOwner(OPTuneFragment owner) {
        mOwner = owner;
        ((OPTuneFragment.OPTuneWizardState) previous()).setOwner(owner);
    }
}
