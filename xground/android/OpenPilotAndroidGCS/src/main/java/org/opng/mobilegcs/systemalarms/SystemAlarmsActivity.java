/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.systemalarms;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.ActivityTelemetryConnectionHandler;
import org.opng.mobilegcs.util.TelemetryActivity;

public class SystemAlarmsActivity extends TelemetryActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionProvidersToShow(ActivityTelemetryConnectionHandler.TELEMETRY_STATUS);
        setContentView(R.layout.activity_system_alarms);

        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBarToolbar);

        // Show the Up button in the toolbar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
        }
    }
}
