/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.drivers.cdc;

import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbEndpoint;
import android.hardware.usb.UsbInterface;
import android.hardware.usb.UsbManager;
import android.hardware.usb.UsbRequest;
import android.os.Build;
import android.util.Log;

import com.google.common.collect.ImmutableList;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import okio.Buffer;

/**
 * Defines a simple CDC driver for USB. Currently supports devices with one or two
 * virtual COM ports defined. The ports support synchronous reads and writes with
 * a {@link CdcSerialDriver.CdcSerialPort#read(ByteBuffer, int)} method and a
 * {@link CdcSerialDriver.CdcSerialPort#write(ByteBuffer, int)} method. Asynchronous
 * communication is supported via an InputStream and an OutputStream that can be
 * read from and written to as required. These are accessed via {@link CdcSerialPort#getInputStream()}
 * and {@link CdcSerialPort#getOutputStream()} respectively.<p>
 * Before using the ports, the driver needs to be configured by calling {@link CdcSerialDriver#setPortParameters(int, int, int, int)}.
 * The ports are then accessed with {@link CdcSerialDriver#getPorts()}. Open a port
 * before use with the {@link CdcSerialPort#open()} method. If you wish
 * to use synchronous reads and writes, simply use the read and write methods on the
 * port. If you wish to use the ports asynchronously, retrieve the input and output
 * streams and call {@link CdcSerialDriver#startAsyncComms()}. Call
 * {@link CdcSerialDriver#stopAsyncComms(boolean)} if you wish to move to using the synchronous methods.<p>
 * When a port is finished with, call {@link CdcSerialPort#close()}. When the driver
 * is finished with, call {@link CdcSerialDriver#shutdown()}. This will close any
 * ports that are still open before closing the USB connection.
 * The read/write thread code is based on Google's AdbTest sample.
 */
public class CdcSerialDriver {
    public final static int STOP_BITS_1 = 0;
    public final static int STOP_BITS_1_5 = 1;
    public final static int STOP_BITS_2 = 2;
    public final static int PARITY_NONE = 0;
    public final static int PARITY_ODD = 1;
    public final static int PARITY_EVEN = 2;
    public final static int PARITY_MARK = 3;
    public final static int PARITY_SPACE = 4;
    public final static int DATA_BITS_5 = 5;
    public final static int DATA_BITS_6 = 6;
    public final static int DATA_BITS_7 = 7;
    public final static int DATA_BITS_8 = 8;
    private final static String DRIVER_TAG = CdcSerialDriver.class.getSimpleName();
    private final static String PORT_TAG = CdcSerialPort.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean WARN = LOGLEVEL > 1;
    private static final boolean ERROR = LOGLEVEL > 0;
    private static final int USB_RECIP_INTERFACE = 0x01;
    private static final int USB_RT_ACM = UsbConstants.USB_TYPE_CLASS | USB_RECIP_INTERFACE;
    private static final int SET_LINE_CODING = 0x20;  // USB CDC 1.1 section 6.2
    private static final int SET_CONTROL_LINE_STATE = 0x22;
    private static final int BUFFER_SIZE = 4096;
    private final UsbManager mUsbManager;
    private final UsbDevice mDevice;
    private final List<CdcSerialPort> mPorts;
    private final ReadRunnable mReadThread = new ReadRunnable();
    private final WriteRunnable mWriteThread = new WriteRunnable();
    private final ExecutorService mReadWriteRunner = Executors.newFixedThreadPool(2);
    // pool of requests for the OUT endpoints
    private final LinkedList<UsbRequest> mOutRequestPool = new LinkedList<>();
    // pool of requests for the IN endpoints
    private final LinkedList<UsbRequest> mInRequestPool = new LinkedList<>();
    private boolean mDtr;
    private UsbDeviceConnection mConnection;
    private Future<?> mReadFuture;
    private Future<?> mWriteFuture;
    private CountDownLatch mReadShutdownLatch;
    private CountDownLatch mWriteShutdownLatch;

    /**
     * Create a new instance of this class to access the virtual COM
     * ports defined by the given device. The device will be managed
     * with the given UsbManager
     *
     * @param usbManager used to manage the given device
     * @param device     device we are communicating with
     */
    public CdcSerialDriver(UsbManager usbManager, UsbDevice device) {
        mUsbManager = usbManager;
        mDevice = device;
        if (device.getInterfaceCount() == 2) {
            mPorts = ImmutableList.<CdcSerialPort>builder()
                    .add(new CdcSerialPort(0))
                    .build();
        } else if (device.getInterfaceCount() == 4) {
            mPorts = ImmutableList.<CdcSerialPort>builder()
                    .add(new CdcSerialPort(0))
                    .add(new CdcSerialPort(1))
                    .build();

        } else {
            mPorts = null;
        }
    }

    /**
     * Get the list of ports defined by this driver instance.
     *
     * @return list of ports
     */
    public List<CdcSerialPort> getPorts() {
        return mPorts;
    }

    /**
     * Get whether or not the DTR line is set.
     *
     * @return true if DTR is set, false otherwise
     */
    public boolean getDtr() {
        return mDtr;
    }

    /**
     * Set or unset the DTR line.
     *
     * @param value true to set DTR on
     */
    public void setDtr(boolean value) {
        mDtr = value;
        sendAcmControlMessage(SET_CONTROL_LINE_STATE, value ? 0x1 : 0, null);
    }

    private boolean isAsyncActive() {
        // Check to see if we have a pending shutdown
        awaitReadTerminate();
        awaitWriteTerminate();

        return mReadFuture != null && mWriteFuture != null;
    }

    /**
     * Used to start sending and receiving data on the input and output streams
     * of each port.
     */
    public void startAsyncComms() {
        if (!isAsyncActive()) {
            mReadFuture = mReadWriteRunner.submit(mReadThread);
            mWriteFuture = mReadWriteRunner.submit(mWriteThread);
        }
    }

    /**
     * Stop sending and receiving data.
     */
    public void stopAsyncComms(boolean awaitTerminate) {
        if (isAsyncActive()) {
            // Create latch read/write threads will use to signal that they have
            // shut down
            mReadShutdownLatch = new CountDownLatch(1);
            mWriteShutdownLatch = new CountDownLatch(1);

            // Clean up our requests
            for (UsbRequest curRequest : mInRequestPool) {
                curRequest.close();
            }
            for (UsbRequest curRequest : mOutRequestPool) {
                curRequest.close();
            }

            // Shut down the executor service
            mReadWriteRunner.shutdown();
            try {
                mReadWriteRunner.awaitTermination(2, TimeUnit.SECONDS);
                if (DEBUG) Log.d(DRIVER_TAG, "ReadWriteRunner terminated");
            } catch (InterruptedException e) {
                e.printStackTrace();
                mReadWriteRunner.shutdownNow();
            }

            // Cancel read/write threads
            boolean cancelled = mReadFuture.cancel(true);
            if (DEBUG)
                Log.d(DRIVER_TAG, "Successfully sent cancel to ReadThread: " + cancelled);

            cancelled = mWriteFuture.cancel(true);
            if (DEBUG)
                Log.d(DRIVER_TAG, "Successfully sent cancel to WriteThread: " + cancelled);

            if (awaitTerminate) {
                awaitReadTerminate();
                awaitWriteTerminate();
            }
            mReadFuture = null;
        }
    }

    /**
     * Set the virtual COM port parameters. These are common to all ports exposed
     * by the driver.
     *
     * @param baudRate transmit and receive baud rate
     * @param dataBits number of data bits; use one of the DATA_BITS_X constants
     * @param stopBits number of stop bits; use one of the STOP_BITS_X constants
     * @param parity   data parity; use one of the PARITY_XXX constants
     */
    public void setPortParameters(int baudRate, int dataBits, int stopBits, int parity) {
        byte[] msg = {
                (byte) (baudRate & 0xff),
                (byte) ((baudRate >> 8) & 0xff),
                (byte) ((baudRate >> 16) & 0xff),
                (byte) ((baudRate >> 24) & 0xff),
                (byte) stopBits,
                (byte) parity,
                (byte) dataBits};
        sendAcmControlMessage(SET_LINE_CODING, 0, msg);
    }

    /**
     * Called when the driver is finished with. This will stop the async send/receive
     * process and close any ports that are still open.
     */
    public void shutdown() {
        stopAsyncComms(true);

        // Close any ports that might've been left open
        for (CdcSerialPort curPort : mPorts) {
            curPort.close();
        }

        // Close the device connection
        if (mConnection != null) {
            mConnection.close();
        }

        mConnection = null;
    }

    /**
     * Get an OUT request from our pool
     *
     * @param endpointOut write endpoint for the request
     * @return new UsbRequest
     */
    private UsbRequest getOutRequest(UsbEndpoint endpointOut) {
        synchronized (mOutRequestPool) {
            if (mOutRequestPool.isEmpty()) {
                UsbRequest request = new UsbRequest();
                request.initialize(mConnection, endpointOut);
                return request;
            } else {
                return mOutRequestPool.removeFirst();
            }
        }
    }

    /**
     * Return an OUT request to the pool
     *
     * @param request request from a call to getOutRequest
     */
    private void releaseOutRequest(UsbRequest request) {
        synchronized (mOutRequestPool) {
            mOutRequestPool.add(request);
        }
    }

    /**
     * Get an IN request from our pool
     *
     * @param endpointIn read endpoint for the request
     * @return new UsbRequest
     */
    private UsbRequest getInRequest(UsbEndpoint endpointIn) {
        synchronized (mInRequestPool) {
            if (mInRequestPool.isEmpty()) {
                UsbRequest request = new UsbRequest();
                request.initialize(mConnection, endpointIn);
                return request;
            } else {
                return mInRequestPool.removeFirst();
            }
        }
    }

    /**
     * Return an IN request to the pool
     *
     * @param request request from a call to getInRequest
     */
    private void releaseInRequest(UsbRequest request) {
        synchronized (mInRequestPool) {
            mInRequestPool.add(request);
        }
    }

    private void awaitReadTerminate(){
        if (mReadShutdownLatch != null) {
            // Wait for the read/write thread to signal that it has stopped.
            try {
                if (DEBUG) Log.d(DRIVER_TAG, "ReadThread waiting for termination");
                boolean result = mReadShutdownLatch.await(5, TimeUnit.SECONDS);
                if (DEBUG) Log.d(DRIVER_TAG, "ReadThread exited successfully: " + result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mReadShutdownLatch = null;
        }
    }

    private void awaitWriteTerminate(){
        if (mWriteShutdownLatch != null) {
            // Wait for the read/write thread to signal that it has stopped.
            try {
                if (DEBUG) Log.d(DRIVER_TAG, "WriteThread waiting for termination");
                boolean result = mWriteShutdownLatch.await(5, TimeUnit.SECONDS);
                if (DEBUG) Log.d(DRIVER_TAG, "WriteThread exited successfully: " + result);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            mWriteShutdownLatch = null;
        }
    }

    /**
     * Send a message on the control endpoint.
     *
     * @param request request id for this transaction
     * @param value   value field for this transaction
     * @param buf     data for this transaction
     */
    private void sendAcmControlMessage(int request, int value, byte[] buf) {
        mConnection.controlTransfer(
                USB_RT_ACM, request, value, 0, buf, buf != null ? buf.length : 0, 5000);
    }

    /**
     * Represents a virtual COM port provided by a CdcSerialDriver instance.
     */
    public class CdcSerialPort {
        private final int mPortNumber;
        // Buffer to read from USB device
        private final Buffer mInputBuffer = new Buffer();
        private final ByteBuffer mReadBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        // Buffer to write to USB device
        private final Buffer mOutputBuffer = new Buffer();
        private UsbInterface mControlInterface;
        private UsbEndpoint mControlEndpoint;
        private UsbInterface mDataInterface;
        private UsbEndpoint mReadEndpoint;
        private UsbEndpoint mWriteEndpoint;
        private boolean mOpened;
        private ByteBuffer mWriteBuffer;

        public CdcSerialPort(int portNumber) {
            mPortNumber = portNumber;
        }

        public boolean isOpened() {
            return mOpened;
        }

        public boolean hadData() {
            return mOutputBuffer.size() > 0;
        }

        public InputStream getInputStream() {
            return mInputBuffer.inputStream();
        }

        public OutputStream getOutputStream() {
            return mOutputBuffer.outputStream();
        }

        UsbEndpoint getReadEndpoint() {
            return mReadEndpoint;
        }

        UsbEndpoint getWriteEndpoint() {
            return mWriteEndpoint;
        }

        public String getSerial() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                return mDevice.getSerialNumber();
            } else {
                return mConnection.getSerial();
            }
        }

        public void open() throws IOException {
            if (mConnection == null) {
                // First port to be opened will open the device connection,
                // subsequent ports will just reuse the connection. It is,
                // after all, the same physical USB port.
                mConnection = mUsbManager.openDevice(mDevice);

                if (mConnection == null) {
                    throw new IOException("Failed to open device: " + mDevice);
                }
            }

            // Need to take account of port number when getting interfaces: if we
            // have two ports (revo, revonano) we have four interfaces to choose
            // from
            int interfaceSelector = mPortNumber * 2;

            mOpened = false;
            try {
                Log.d(PORT_TAG, "claiming interfaces, count=" + mDevice.getInterfaceCount());
                mControlInterface = mDevice.getInterface(interfaceSelector);
                Log.d(PORT_TAG, "Control iface=" + mControlInterface);
                // class should be USB_CLASS_COMM

                if (!mConnection.claimInterface(mControlInterface, true)) {
                    throw new IOException("Could not claim control interface.");
                }
                mControlEndpoint = mControlInterface.getEndpoint(0);
                Log.d(PORT_TAG, "Control endpoint direction: " + mControlEndpoint.getDirection());

                Log.d(PORT_TAG, "Claiming data interface.");
                mDataInterface = mDevice.getInterface(1 + interfaceSelector);
                Log.d(PORT_TAG, "data iface=" + mDataInterface);
                // class should be USB_CLASS_CDC_DATA

                if (!mConnection.claimInterface(mDataInterface, true)) {
                    throw new IOException("Could not claim data interface.");
                }
                mReadEndpoint = mDataInterface.getEndpoint(0);
                Log.d(PORT_TAG, "Read endpoint direction: " + mReadEndpoint.getDirection());
                mWriteEndpoint = mDataInterface.getEndpoint(1);
                Log.d(PORT_TAG, "Write endpoint direction: " + mWriteEndpoint.getDirection());
                mOpened = true;
            } finally {
                if (!mOpened) {
                    mConnection = null;
                }
            }
        }

        public int read(ByteBuffer dataIn, int timeoutMs) throws IOException {
            if (!mOpened || mConnection == null || mReadEndpoint == null) {
                throw new IOException("Port is not open");
            }
            if (isAsyncActive()) {
                if(DEBUG)Log.d(PORT_TAG, "Exiting async mode for sync read");
                stopAsyncComms(true);
            }
            byte[] data = dataIn.array();
            return mConnection.bulkTransfer(mReadEndpoint, data, data.length, timeoutMs);
        }

        public int write(ByteBuffer dataOut, int timeoutMs) throws IOException {
            if (!mOpened || mConnection == null || mWriteEndpoint == null) {
                throw new IOException("Port is not open");
            }
            if (isAsyncActive()) {
                if(DEBUG)Log.d(PORT_TAG, "Exiting async mode for sync write");
                stopAsyncComms(true);
            }
            byte[] data = dataOut.array();
            return mConnection.bulkTransfer(mWriteEndpoint, data, data.length, timeoutMs);
        }

        public void close() {
            mOpened = false;
            if (mConnection != null) {
                if (DEBUG) Log.d(PORT_TAG, "Releasing USB interfaces");
                if (mControlInterface != null) {
                    mConnection.releaseInterface(mControlInterface);
                }

                if (mDataInterface != null) {
                    mConnection.releaseInterface(mDataInterface);
                }
            }
        }

        /**
         * Use by ReadWriteThread to read up to BUFFER_SIZE bytes from the USB port. When the
         * given request completes, ReadWriteThread will push the read bytes into mInputBuffer
         *
         * @param inRequest UsbRequest retrieved by calling getInRequest with mReadEndpoint.
         */
        void read(UsbRequest inRequest) {
            inRequest.setClientData(this);
            inRequest.queue(mReadBuffer, BUFFER_SIZE);
        }

        void transferToInputBuffer() {
            int byteCount = mReadBuffer.position();

            if (byteCount > 0) {
                byte[] readBuffer = new byte[byteCount];
                mReadBuffer.rewind();
                mReadBuffer.get(readBuffer, 0, byteCount);
                mInputBuffer.write(readBuffer);
                mReadBuffer.clear();
            }
        }

        /**
         * Used by ReadWriteThread to send the next BUFFER_SIZE bytes from mOutputBuffer
         * to the USB port. If the request fails to queue, it will be returned to the
         * request pool.
         *
         * @param outRequest UsbRequest retrieved by calling getOutRequest with mWriteEndpoint
         */
        void write(UsbRequest outRequest) {
            outRequest.setClientData(this);

            try {
                final byte[] array = mOutputBuffer.readByteArray();
                mWriteBuffer = ByteBuffer.wrap(array);
                if (!outRequest.queue(mWriteBuffer, array.length)) {
                    if (DEBUG) Log.d(PORT_TAG, "Write queue request failed");
                    releaseOutRequest(outRequest);
                }
            } catch (AssertionError e) {
                e.printStackTrace();
            }
        }

        boolean isReadRequest(UsbRequest request) {
            return request.getEndpoint().equals(mReadEndpoint);
        }
    }

    private class WriteRunnable implements Runnable {

        @Override
        public void run() {
            if(DEBUG)Log.d(DRIVER_TAG, "Starting write thread");

            while(true){
                if (Thread.interrupted()) {
                    if (DEBUG) Log.d(DRIVER_TAG, "WriteThread interrupted at start of loop");
                    break;
                }

                // Queue write to each port
                if (mPorts != null) {
                    for (CdcSerialPort curPortForWrite : mPorts) {
                        if (curPortForWrite.isOpened() && curPortForWrite.hadData()) {
                            if (VERBOSE)
                                Log.d(DRIVER_TAG, "WriteThread writing to port: " + curPortForWrite);
                            curPortForWrite.write(getOutRequest(curPortForWrite.getWriteEndpoint()));
                        }
                    }
                }
            }
            if (DEBUG) Log.d(DRIVER_TAG, "WriteThread exiting");

            if (mWriteShutdownLatch != null) {
                // Signal to waiting shutdown code that we have actually shut down
                mWriteShutdownLatch.countDown();
                if (DEBUG)
                    Log.d(DRIVER_TAG, "Await shutdown latch, count: " + mWriteShutdownLatch.getCount());
            }
        }
    }

    private class ReadRunnable implements Runnable {

        @Override
        public void run() {
            if(DEBUG)Log.d(DRIVER_TAG, "Starting read thread");

            // Queue read request on each port
            if (mPorts != null) {
                for (CdcSerialPort curPort : mPorts) {
                    if (curPort.isOpened()) {
                        curPort.read(getInRequest(curPort.getReadEndpoint()));
                    }
                }
            }

            while (true) {
                if (Thread.interrupted()) {
                    if (DEBUG) Log.d(DRIVER_TAG, "ReadThread interrupted at start of loop");
                    break;
                }

                // Wait for read request completion. Note that this could be for
                // any port.
                UsbRequest request = mConnection.requestWait();
                if (request == null) {
                    if (DEBUG) Log.d(DRIVER_TAG, "requestWait() failed");
                    break;
                }

                if (Thread.interrupted()) {
                    if (DEBUG)
                        Log.d(DRIVER_TAG, "ReadThread interrupted after getting request");
                    break;
                }

                // Get port associated with current returned request
                CdcSerialPort curPort = (CdcSerialPort) request.getClientData();
                request.setClientData(null);

                boolean curPortIsReadRequest = curPort.isReadRequest(request);
                if (curPortIsReadRequest) {
                    if (VERBOSE) Log.d(DRIVER_TAG, "ReadThread reading port: " + curPort);

                    // Transfer read data to input buffer to be read by client code
                    curPort.transferToInputBuffer();
                }

                if (Thread.interrupted()) {
                    if (DEBUG)
                        Log.d(DRIVER_TAG, "ReadThread interrupted after servicing read request");
                    break;
                }

                // Queue read request on current port if we've just handled its
                // last read request
                if (curPortIsReadRequest) {
                    if (VERBOSE)
                        Log.d(DRIVER_TAG, "ReadThread queuing next read for port: " + curPort);
                    curPort.read(getInRequest(curPort.getReadEndpoint()));
                }

                if (Thread.interrupted()) {
                    if (DEBUG)
                        Log.d(DRIVER_TAG, "ReadThread interrupted after queuing next read");
                    break;
                }

                // Done with request
                if (curPortIsReadRequest) {
                    if (VERBOSE)
                        Log.d(DRIVER_TAG, "ReadThread releaseInRequest() for port: " + curPort);
                    releaseInRequest(request);
                } else {
                    if (VERBOSE)
                        Log.d(DRIVER_TAG, "ReadThread releaseOutRequest() for port: " + curPort);
                    releaseOutRequest(request);
                }
            }
            if (DEBUG) Log.d(DRIVER_TAG, "ReadThread exiting");

            if (mReadShutdownLatch != null) {
                // Signal to waiting shutdown code that we have actually shut down
                mReadShutdownLatch.countDown();
                if (DEBUG)
                    Log.d(DRIVER_TAG, "Await shutdown latch, count: " + mReadShutdownLatch.getCount());
            }

        }
    }
}
