/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.tasks.bamboo;

import org.opng.mobilegcs.firmware.downloader.bamboo.Plan;

import java.io.IOException;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLHandshakeException;

public class FetchPlanDataTask extends BaseTask<Plan, Void, Boolean> {
    private final FetchCallback mFetchCallback;
    private final String mAuthString;
    private boolean mAuthFailed;

    public FetchPlanDataTask(FetchCallback fetchCallback, String authString) {
        mFetchCallback = fetchCallback;
        mAuthString = authString;
    }

    @Override
    protected Boolean doInBackground(Plan... plans) {
        Boolean retVal = plans.length > 0 && plans[0] != null;
        Plan plan = plans[0];
        HttpURLConnection conn = null;
        InputStream is = null;
        CookieManager cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);

        if (retVal) {
            try {
                URL url = new URL(String.format("%s/rest/api/latest/plan/%s-%s.json?expand=branches",
                        plan.getRestUrl(), plan.getProjectKey(), plan.getPlanKey()));
                conn = (HttpURLConnection) url.openConnection();

                if(initKeyStoreHandling(plan.getKeyStore())){
                    ((HttpsURLConnection)conn).setSSLSocketFactory(mSslContext.getSocketFactory());
                }
                if (mAuthString != null) {
                    conn.setRequestProperty("Authorization", "Basic " + mAuthString);
                }
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                plan.fromJson(is);

            } catch (SSLHandshakeException e) {
                mThrownException = e;
            }catch (IOException e) {
                e.printStackTrace();
                retVal = false;
                try {
                    if (conn != null) {
                        mAuthFailed = conn.getResponseCode() == 401;
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (conn != null) {
                    conn.disconnect();
                }
            }
        }
        return retVal;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mFetchCallback != null) {
            if (mAuthFailed) {
                mFetchCallback.needPassword();
            } else if (mThrownException != null) {
                mFetchCallback.handleException(mThrownException);
            } else {
                mFetchCallback.fetchComplete(result);
            }
        }
    }

}
