/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.io;

/**
 * Defines various events that can be fired by an IAP instance.
 */
public interface IAPEventListener {
    /**
     * Gives a percentage complete figure for the firmware upload
     *
     * @param progress current progress as a percentage
     */
    void progressUpdated(int progress);

    /**
     * Fired when firmware download from device is complete
     *
     * @param status end status of the operation
     */
    void downloadFinished(IAP.Status status);

    /**
     * Fired when firmware upload to the device is complete.
     * If a description has been extracted from the uploaded firmware, it will
     * be passed to the implementer of this interface via this method.
     *
     * @param status      end status of the operation
     * @param description byte array containing firmware description
     */
    void uploadFinished(IAP.Status status, byte[] description);

    /**
     * Provides textual descriptions of progress as firmware is updated.
     *
     * @param status current progress message
     */
    void operationProgress(String status);
}
