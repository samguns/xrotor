/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.objectbrowser;

import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.opng.mobilegcs.ObjectEditView;
import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.mobilegcs.util.UAVObjectUtilManager;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;

import java.util.List;

/**
 * A fragment representing a single UAVObject detail screen. This fragment is
 * either contained in a {@link UAVObjectListActivity} in two-pane mode (on
 * tablets) or a {@link UAVObjectDetailActivity} on handsets.
 */
public class UAVObjectDetailFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final String TAG = "UAVObjectDetailFragment";
    private String[] mObjectInfo;
    private UAVObjectToGuiUtils mUAVObjectToGuiUtils;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UAVObjectDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            // TODO: Currently the id will be in form <object name>|<object id>|<object instance id>
            String string = getArguments().getString(
                    ARG_ITEM_ID);
            if (string != null) {
                mObjectInfo = string.split(":");
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_uavobject_detail,
                container, false);

        getLoaderManager().initLoader(0, null, this);

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (DEBUG) Log.d(TAG, "Pausing");
        disableObjectUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (DEBUG) Log.d(TAG, "Resuming");
        enableObjectUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (DEBUG) Log.d(TAG, "Stopping");
        disableObjectUpdates();
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int arg0, Bundle arg1) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> arg0, UAVObjectManager arg1) {
        UAVObject object = arg1.getObject(mObjectInfo[0], Long.valueOf(mObjectInfo[1]), Long.valueOf(mObjectInfo[2]));
        final View view = getView();
        ObjectEditView editView = null;
        if (view != null) {
            editView = (ObjectEditView) view.findViewById(R.id.uavobject_detail);
        }

        UAVObjectUtilManager utilMngr = arg1.getObjectUtilManager();
        mUAVObjectToGuiUtils = new UAVObjectToGuiUtils(getActivity(), arg1, utilMngr);

        Button saveButton = null;
        Button applyButton = null;
        if (view != null) {
            saveButton = (Button) view.findViewById(R.id.btn_save);
            applyButton = (Button) view.findViewById(R.id.btn_upload);
        }

        if (((UAVDataObject) object).isSettings()) {
            mUAVObjectToGuiUtils.addApplySaveButtons(applyButton, saveButton);
        } else {
            if (saveButton != null) {
                saveButton.setVisibility(View.GONE);
            }
            if (applyButton != null) {
                applyButton.setVisibility(View.GONE);
            }
            if (editView != null) {
                editView.setReadOnly();
            }
        }

        List<UAVObjectField> fields = object.getFields();
        if (editView != null) {
            for (UAVObjectField field : fields) {
                List<View> viewsAdded = editView.addField(field);

                for (int count = 0; count < viewsAdded.size(); ++count) {
                    mUAVObjectToGuiUtils.addUAVObjectToWidgetRelation(object, field, viewsAdded.get(count), count, 1, false, null, 0);
                }
            }
        }
        object.requestUpdate();
        mUAVObjectToGuiUtils.populateViews();
        mUAVObjectToGuiUtils.enableObjUpdates();
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> arg0) {
    }

    private void enableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.enableObjUpdates();
        }
    }

    private void disableObjectUpdates() {
        if (mUAVObjectToGuiUtils != null) {
            mUAVObjectToGuiUtils.disableObjUpdates();
        }
    }
}
