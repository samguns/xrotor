/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.view.View;

import java.util.EventListener;

public interface UAVObjectToGuiUtilsEventListener extends EventListener {
    //fired when a view's contents changes
    void viewContentsChanged(View view, Variant value);

    //fired when the framework requests that the view's values be populated, use for custom behaviour
    void populateViewsRequested();

    //fired when the framework requests that the view's values be refreshed, use for custom behaviour
    void refreshViewsValuesRequested();

    //fired when the framework requests that the UAVObject values be updated from the view's value, use for custom behaviour
    void updateObjectsFromViewsRequested();

    //fired when the autopilot connects
    void autoPilotConnected();

    //fired when the autopilot disconnects
    void autoPilotDisconnected();

    void defaultRequested(int group);

    //fired when we are about to process a save operation
    void beforeSave();

    //fired when saving is complete, regardless of success or failure
    void afterSave();
}
