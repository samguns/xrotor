/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.logging;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.ListView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.logging.adapter.LoggingAdapter;

import java.io.File;
import java.util.ArrayList;


/**
 * Fragment to manage SD Card log files
 * <p/>
 * list view ref http://www.vogella.com/tutorials/AndroidListView/article.html
 */
public class SDLoggingFragment extends Fragment {

    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final String TAG = SDLoggingFragment.class.getCanonicalName();
    private ListView mFilesList;
    private LoggingAdapter listAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_sdlog_manage, container, false);

        mFilesList = (ListView) rootView.findViewById(R.id.sdlog_file_list);

        // find buttons and disable by default, if there is data we will enable them
        Button btnDelete = (Button) rootView.findViewById(R.id.button_delete);
        btnDelete.setEnabled(false);
        Button btnShare = (Button) rootView.findViewById(R.id.button_share);
        btnShare.setEnabled(false);

        //delete temp folder
//        DirectoryPath.cleanAppTempPath();

        ViewGroup header = (ViewGroup) inflater.inflate(R.layout.listview_log_header, mFilesList, false);
        mFilesList.addHeaderView(header, null, false);
        mFilesList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

        //TODO add logic for select all checkbox
        CheckedTextView headerCheckItem = (CheckedTextView) header.findViewById(R.id.checkedTextView2);
        //headerCheckItem.setVisibility(View.INVISIBLE);
        headerCheckItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CheckedTextView checkedTextView = (CheckedTextView) v;

                if (listAdapter.getData().size() < 1)
                    return;

                if (checkedTextView.isChecked()) {
                    checkedTextView.setChecked(false);
                    toggleAllOfforOn(false);
                } else {
                    checkedTextView.setChecked(true);
                    toggleAllOfforOn(true);
                }

            }
        });

        // gets file list and loads the listview
        ArrayList<FileListObject> fileList = setupFileManager();

        listAdapter = new LoggingAdapter(getActivity(), R.layout.listview_log_row, fileList);
        mFilesList.setAdapter(listAdapter);

        mFilesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView markedItem = (CheckedTextView) view.findViewById(R.id.checkedTextView1);

                if (markedItem.isChecked()) {
                    markedItem.setChecked(false);
                } else {
                    markedItem.setChecked(true);
                }

                view.refreshDrawableState();
            }
        });

        // listener for the share button
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareFiles(v);
            }
        });

        // listener for the delete button
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                SparseBooleanArray selected = mFilesList.getCheckedItemPositions();
                if (selected.size() < 1)
                    return;

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage("Are you sure?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // delete files from fs and list
                                handleDeleteFiles();

                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });


        if (listAdapter.getData().size() > 0) {
            btnDelete.setEnabled(true);
            btnShare.setEnabled(true);
        }
        return rootView;
    }

    private void handleDeleteFiles() {
        SparseBooleanArray selected = mFilesList.getCheckedItemPositions();
        for (int i = (selected.size() - 1); i >= 0; i--) {
            if (selected.valueAt(i)) {

                FileListObject selecteditem = (FileListObject) mFilesList.getAdapter().getItem(
                        selected.keyAt(i));

                deleteFile(selecteditem.getFileName());
                listAdapter.remove(selecteditem);

            }
        }
        // redraw listview
        listAdapter.notifyDataSetChanged();
        mFilesList.clearChoices();
    }


    private void shareFiles(View v) {
        SparseBooleanArray checkedItems = mFilesList.getCheckedItemPositions();

        if (checkedItems == null || checkedItems.size() < 1) {
            return;
        }

        ArrayList<FileListObject> items = listAdapter.getData();

        ArrayList<String> filesToShare = new ArrayList<>();

        for (int i = 0; i < checkedItems.size(); i++) {
            // Item position in adapter
            int position = checkedItems.keyAt(i);

            FileListObject flo = items.get(position - 1);
//            filesToShare.add(DirectoryPath.getFlightLogPath() + "/" + flo.getFileName());
        }


        Intent shareIntent = new Intent();

        // if we are sending one or more files it may be better to zip them up
        File filesToSend;
        if (checkedItems.size() > 1) {
            filesToSend = zipFiles(filesToShare);
            if (filesToSend != null) {
                shareIntent.putExtra(Intent.EXTRA_TEXT, filesToSend.getName());
            }
        } else {
            filesToSend = new File(filesToShare.get(0));
            shareIntent.putExtra(Intent.EXTRA_TEXT, filesToSend.getName());
        }

        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.setType("application/octet-stream");
        shareIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"noreply@opng", ""});
        if (filesToSend != null) {
            shareIntent.putExtra(Intent.EXTRA_SUBJECT, filesToSend.getName());
        }
        if (filesToSend != null) {
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + filesToSend.getAbsolutePath()));
        }
        startActivity(Intent.createChooser(shareIntent, "Choose app to send file:"));

    }

    private void deleteFile(String fileName) {
//        File f = new File(DirectoryPath.getFlightLogPath() + "/" + fileName);
//        f.delete();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private ArrayList<FileListObject> setupFileManager() {

//        File[] files = Utils.getOPLogs();
//        Arrays.sort(files, Collections.reverseOrder());
//
//        for (File file : files) {
//            FileListObject fileListObject = new FileListObject();
//            fileListObject.setFileName(file.getName());
//
//            long fileSize = file.length();
//            fileListObject.setFileSize(Utils.getReadableFileSize(fileSize));
//
//            fileList.add(fileListObject);
//        }

        return new ArrayList<>();

    }

    /**
     * takes a list of files and zip them
     *
     * @param files files to zip
     * @return zip archive
     */
    private File zipFiles(ArrayList<String> files) {

//        File zipFile = null;
//        try {
//            String dateString = Utils.getDateString("yyMMddhhmmSS");
//            zipFile = File.createTempFile("OPLogs-" + dateString, ".zip");
//
//            FileOutputStream fos = new FileOutputStream(zipFile);
//            ZipOutputStream zos = new ZipOutputStream(fos);
//
//            // create byte buffer
//            byte[] buffer = new byte[1024];
//
//            for(String filename : files){
//                File logFile = new File(filename);
//                FileInputStream fis = new FileInputStream(logFile);
//
//                // begin writing a new ZIP entry, positions the stream to the start
//                // of the entry data
//                zos.putNextEntry(new ZipEntry(logFile.getName()));
//
//                int length;
//
//                while ((length = fis.read(buffer)) > 0) {
//                    zos.write(buffer, 0, length);
//                }
//
//                zos.closeEntry();
//
//                // close the InputStream
//                fis.close();
//
//            }
//
//            zos.close();
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        // move the file to location that is accessible
//        File newLoc = new File(DirectoryPath.getAppTempPath() + zipFile.getName());
//
//        try {
//            Files.move(zipFile,newLoc);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return newLoc;
        return null;
    }

    private void toggleAllOfforOn(boolean checked) {
        for (int i = 1; i < mFilesList.getChildCount(); i++) {
            // need both of these
            mFilesList.setItemChecked(i, checked);
            FileListObject flo = (FileListObject) mFilesList.getItemAtPosition(i);
            flo.setSelected(checked);
        }


        mFilesList.invalidateViews();
    }

}
