/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.optune;

import android.os.Parcel;
import android.util.Log;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.wizard.WizardState;

class CalculateState extends WizardState implements OPTuneFragment.OPTuneWizardState {
    public static final Creator<CalculateState> CREATOR = new Creator<CalculateState>() {
        @Override
        public CalculateState createFromParcel(Parcel source) {
            return new CalculateState(source);
        }

        @Override
        public CalculateState[] newArray(int size) {
            return new CalculateState[size];
        }
    };
    private final static String TAG = CalculateState.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private OPTuneFragment mOwner;

    CalculateState(WizardState previousState, OPTuneFragment owner) {
        super(previousState);
        mOwner = owner;
    }

    private CalculateState(Parcel source) {
        super(source);
    }

    @Override
    public WizardState next() {
        if (DEBUG) Log.d(TAG, "CalculateState: next");
        Calculator calculator = new Calculator(mOwner.getOPTuneFactors());

        //noinspection PointlessBooleanExpression
        if (!OPTuneFragment.PITCH_AND_ROLL_ONLY) {
            calculator.calculate(mOwner.getSavedState().getPitchUov(), mOwner.getSavedState().getRollUov(), mOwner.getSavedState().getYawUov());
        } else {
            calculator.calculate(mOwner.getSavedState().getPitchUov(), mOwner.getSavedState().getRollUov());
        }

        // Display them in the GUI, the user can use save or upload as
        // appropriate.
        // This will simply display them in the gui
        mOwner.getStabiGuiHelper().displayPitchRatePID(calculator.getPitchKp(), calculator.getPitchKi(), calculator.getPitchKd());
        mOwner.getStabiGuiHelper().displayRollRatePID(calculator.getRollKp(), calculator.getRollKi(), calculator.getRollKd());
        mOwner.getStabiGuiHelper().displayYawRatePID(calculator.getYawKp(), calculator.getYawKi(), calculator.getYawKd());

        return new BeginOpTuneState(mOwner);
    }

    @Override
    public boolean isEndState() {
        return true;
    }

    @Override
    public void process() {
        if (DEBUG) Log.d(TAG, "CalculateState: process");

        mOwner.setInstructions(mOwner.getString(R.string.txt_optune_wizard_calculate));

        // Finished now so restore original settings
        mOwner.restoreKiKd();
    }

    @Override
    public void setOwner(OPTuneFragment owner) {
        mOwner = owner;
        ((OPTuneFragment.OPTuneWizardState) previous()).setOwner(owner);
    }
}
