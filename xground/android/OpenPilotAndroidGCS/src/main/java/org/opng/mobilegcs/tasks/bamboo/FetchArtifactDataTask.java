/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.tasks.bamboo;

import com.google.common.io.ByteStreams;

import org.opng.mobilegcs.firmware.downloader.bamboo.Artifact;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.ByteBuffer;

import javax.net.ssl.HttpsURLConnection;

public class FetchArtifactDataTask extends BaseTask<Artifact, Void, Boolean> {
    private final FetchCallback mFetchCallback;
    private Artifact mArtifact;

    public FetchArtifactDataTask(FetchCallback fetchCallback) {
        mFetchCallback = fetchCallback;
    }

    @Override
    protected Boolean doInBackground(Artifact... artifacts) {
        Boolean retVal = artifacts.length > 0 && artifacts[0] != null;
        mArtifact = artifacts[0];
        HttpURLConnection conn = null;
        InputStream is = null;

        if (retVal) {
            try {
                URL url = new URL(mArtifact.getArtifactUrl());
                conn = (HttpURLConnection) url.openConnection();

                if(initKeyStoreHandling(mArtifact.getPlan().getKeyStore())){
                    ((HttpsURLConnection)conn).setSSLSocketFactory(mSslContext.getSocketFactory());
                }
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                mArtifact.setBytes(ByteBuffer.wrap(ByteStreams.toByteArray(is)));

            } catch (IOException e) {
                e.printStackTrace();
                retVal = false;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (conn != null) {
                    conn.disconnect();
                }
            }
        }
        return retVal;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mFetchCallback != null) {
            mFetchCallback.fetchComplete(result, mArtifact);
        }
    }

    public interface FetchCallback {
        void fetchComplete(Boolean result, Artifact artifact);
    }
}
