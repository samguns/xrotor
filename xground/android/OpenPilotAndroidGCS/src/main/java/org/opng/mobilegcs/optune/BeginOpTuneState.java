/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.optune;

import android.os.Parcel;
import android.util.Log;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.wizard.WizardState;

class BeginOpTuneState extends WizardState implements OPTuneFragment.OPTuneWizardState {
    public static final Creator<BeginOpTuneState> CREATOR = new Creator<BeginOpTuneState>() {
        @Override
        public BeginOpTuneState createFromParcel(Parcel source) {
            return new BeginOpTuneState(source);
        }

        @Override
        public BeginOpTuneState[] newArray(int size) {
            return new BeginOpTuneState[size];
        }
    };
    private final static String TAG = BeginOpTuneState.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private OPTuneFragment mOwner;

    BeginOpTuneState(OPTuneFragment owner) {
        mOwner = owner;
        mOwner.setInstructions(mOwner.getString(R.string.text_optune_wizard_begin));
    }

    private BeginOpTuneState(Parcel source) {
        super(source);
    }

    @Override
    public WizardState next() {
        if (DEBUG) Log.d(TAG, "BeginOpTuneState: next");

        mOwner.resetUovDisplay();

        return new AcquireRollUovState(this, mOwner);
    }

    @Override
    public WizardState previous() {
        return this;
    }

    @Override
    public boolean isStartState() {
        return true;
    }

    @Override
    public void process() {
        mOwner.setInstructions(mOwner.getString(R.string.text_optune_wizard_begin));
    }

    @Override
    public void setOwner(OPTuneFragment owner) {
        mOwner = owner;
    }
}
