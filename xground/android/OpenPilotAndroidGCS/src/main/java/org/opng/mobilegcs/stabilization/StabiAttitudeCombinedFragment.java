/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.ViewSwitcher;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class StabiAttitudeCombinedFragment extends BaseFragment implements StabiGuiFragment {
    private View mRootView;
    private UAVObjectManagerLoaderHandler mUAVObjectManagerLoaderHandler;
    private ViewSwitcher mAttitudeSwitcher;
    private CheckBox mCheckAdvanced;

    public StabiAttitudeCombinedFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_stabi_attitude_combined, container, false);
        mAttitudeSwitcher = (ViewSwitcher) mRootView.findViewById(R.id.switchAttitude);
        mCheckAdvanced = (CheckBox) mRootView.findViewById(R.id.checkShowAdvanced);
        setupViewSwitchers(mAttitudeSwitcher, mCheckAdvanced);

        // Set up our loader handler: get all the controls it needs and then create it
        Button apply = (Button) mRootView.findViewById(R.id.buttonCommonFooterApply);
        Button save = (Button) mRootView.findViewById(R.id.buttonCommonFooterSave);
        Button btnDefault = (Button) mRootView.findViewById(R.id.buttonCommonFooterDefault);
        Button restore = (Button) mRootView.findViewById(R.id.buttonCommonFooterRestore);
        CheckBox autoUpdate = (CheckBox) mRootView.findViewById(R.id.checkFcUpdateRealtime);
        Spinner bankSelect = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);

        mUAVObjectManagerLoaderHandler = new UAVObjectManagerLoaderHandler(getActivity(), this, autoUpdate, bankSelect, apply, save,
                btnDefault, restore, R.integer.stabi_attitude_button_group);

        getLoaderManager().initLoader(0, null, mUAVObjectManagerLoaderHandler);

        return mRootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onStop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onResume();
        }

        if (mCheckAdvanced != null && mAttitudeSwitcher != null) {
            mAttitudeSwitcher.setDisplayedChild(mCheckAdvanced.isChecked() ? 1 : 0);
        }
    }

    private void setupViewSwitchers(final ViewSwitcher viewSwitcher, final CheckBox checkAdvanced) {
        checkAdvanced.setVisibility(View.VISIBLE);
        checkAdvanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAdvanced.isChecked()) {
                    viewSwitcher.showNext();
                } else {
                    viewSwitcher.showPrevious();
                }
            }
        });
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        if (mRootView != null) {
            FragmentManager fragmentManager = getChildFragmentManager();
            Fragment basicAttitude = fragmentManager.findFragmentById(R.id.basicAttitude);
            Fragment advancedAttitude = fragmentManager.findFragmentById(R.id.advancedAttitude);
            ((StabiGuiFragment) basicAttitude).setupGuiHandler(helper);
            ((StabiGuiFragment) advancedAttitude).setupGuiHandler(helper);
        }
    }
}
