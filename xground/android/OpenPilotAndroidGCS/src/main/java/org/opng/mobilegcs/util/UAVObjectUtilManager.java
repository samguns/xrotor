/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.util.Log;

import org.opng.mobilegcs.BuildConfig;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.FirmwareIAPObj;
import org.opng.uavtalk.uavobjects.ObjectPersistence;

import java.util.EventListener;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class UAVObjectUtilManager {

    private final static String TAG = SmartSave.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final UAVObjectManager mUAVObjectManager;
    private final Set<UAVObjectUtilManagerEventListener> mUAVObjectUtilManagerEventListeners = new CopyOnWriteArraySet<>();
    private final Queue<UAVObject> mQueue = new ConcurrentLinkedQueue<>();
    private SaveState mSaveState;
    private ScheduledThreadPoolExecutor mFailureTimer;
    private ScheduledFuture<?> mFailureTimerTask;
    private UAVObject mObjectPersistence;
    private final UAVObjectEventAdapter mObjectTransactionListener = new UAVObjectEventAdapter() {
        @Override
        public void transactionCompleted(UAVObject obj, boolean success) {
            objectPersistenceTransactionCompleted(obj, success);
        }
    };
    // Need two separate handlers for updated and transaction complete as there is a case
    // where we need to remove the transaction complete listener but leave the updated listener
    private final UAVObjectEventAdapter mObjectUpdatedListener = new UAVObjectEventAdapter() {

        @Override
        public void objectUpdated(UAVObject obj) {
            objectPersistenceUpdated(obj);
        }

    };
    public UAVObjectUtilManager(UAVObjectManager objManager) {
        mUAVObjectManager = objManager;
    }

    private UAVObjectManager getObjectManager() {
        assert (mUAVObjectManager != null);

        return mUAVObjectManager;
    }

    public void addEventListener(UAVObjectUtilManagerEventListener listener) {
        mUAVObjectUtilManagerEventListeners.add(listener);
    }

    public void removeEventListener(UAVObjectUtilManagerEventListener listener) {
        mUAVObjectUtilManagerEventListeners.remove(listener);
    }

    public void saveObjectToSD(UAVDataObject obj) {
        mQueue.offer(obj);
        if (DEBUG)
            Log.d(TAG, "Enqueue object: " + obj.getName());

        // If queue length is one, then start sending (call sendNextObject)
        // Otherwise, do nothing, it's sending anyway
        if (mQueue.size() == 1)
            saveNextObject();
    }

    public int getBoardModel() {
        UAVObject firmwareIAPObject = mUAVObjectManager.getObject("FirmwareIAPObj");
        Integer boardType = firmwareIAPObject.getField(FirmwareIAPObj.FIELD_BOARDTYPE)
                .getValue().toInt();
        Integer boardRev = firmwareIAPObject
                .getField(FirmwareIAPObj.FIELD_BOARDREVISION).getValue().toInt();
        if (DEBUG)
            Log.d(TAG, "Board type=" + boardType);
        if (DEBUG)
            Log.d(TAG, "Board revision=" + boardRev);
        int ret = boardType << 8;
        ret = ret + boardRev;
        if (DEBUG)
            Log.d(TAG, "Board info=" + ret);
        return ret;
    }

    private void saveNextObject() {
        if (mQueue.isEmpty()) {
            return;
        }

        if (BuildConfig.DEBUG && mSaveState != SaveState.IDLE) {
            throw new AssertionError();
        }

        // Get next object from the queue
        UAVObject obj = mQueue.poll();
        if (DEBUG)
            Log.d(TAG, "Send save object request to board " + (obj != null ? obj.getName() : "Unknown"));

        mObjectPersistence = getObjectManager().getObject("ObjectPersistence");

        mObjectPersistence.addUAVObjectEventListener(mObjectUpdatedListener);
        mObjectPersistence.addUAVObjectEventListener(mObjectTransactionListener);
        mSaveState = SaveState.AWAITING_ACK;
        if (obj != null) {
            Long objId = obj.getObjID();
            if (DEBUG)
                Log.d(TAG, "Saving object ID: " + objId);
            mObjectPersistence.getField(ObjectPersistence.FIELD_OBJECTID).setValue(new Variant(objId));
            mObjectPersistence.getField(ObjectPersistence.FIELD_INSTANCEID).setValue(new Variant(obj.getInstID()));
            mObjectPersistence.getField(ObjectPersistence.FIELD_OPERATION).setValue(new Variant(ObjectPersistence.OPERATION_SAVE));
            mObjectPersistence.getField(ObjectPersistence.FIELD_SELECTION).setValue(new Variant(ObjectPersistence.SELECTION_SINGLEOBJECT));
            mObjectPersistence.updated();
        }
        // Now: we are going to get two "objectUpdated" messages (one coming
        // from GCS, one coming from Flight, which
        // will confirm the object was properly received by both sides) and then
        // one "transactionCompleted" indicating
        // that the Flight side did not only receive the object but it did
        // receive it without error. Last we will get
        // a last "objectUpdated" message coming from flight side, where we'll
        // get the results of the objectPersistence
        // operation we asked for (saved, other).
    }

    private void objectPersistenceUpdated(UAVObject obj) {
        if (DEBUG) Log.d(TAG, "objectPersistenceUpdated ");
        if (BuildConfig.DEBUG && obj == null) {
            throw new AssertionError();
        }
        if (BuildConfig.DEBUG && (obj.getObjID() != mObjectPersistence.getObjID())) {
            throw new AssertionError();
        }

        String operation = mObjectPersistence.getField(ObjectPersistence.FIELD_OPERATION).getValue().toString();
        if (mSaveState == SaveState.AWAITING_COMPLETED
                && operation.compareTo(ObjectPersistence.OPERATION_ERROR) == 0) {
            stopFailureTimer();
            objectPersistenceOperationFailed();
        } else if (mSaveState == SaveState.AWAITING_COMPLETED
                && operation.compareTo(ObjectPersistence.OPERATION_COMPLETED) == 0) {
            stopFailureTimer();
            // Check right object saved
            UAVObject savingObj = mQueue.peek();
            Long persistedObjectID = mObjectPersistence.getField(ObjectPersistence.FIELD_OBJECTID).getValue().toLong();
            if (savingObj != null && persistedObjectID != savingObj.getObjID()) {
                objectPersistenceOperationFailed();
                return;
            }

            mObjectPersistence.removeUAVObjectEventListener(mObjectUpdatedListener);
            mObjectPersistence.removeUAVObjectEventListener(mObjectTransactionListener);
            mQueue.poll(); // We can now remove the object, it's done.
            mSaveState = SaveState.IDLE;

            fireSaveCompleted(persistedObjectID, true);
            saveNextObject();
        }
    }

    private void objectPersistenceTransactionCompleted(UAVObject obj,
                                                       boolean success) {
        if (DEBUG)
            Log.d(TAG, "objectPersistenceTransactionCompleted " + (success ? "" : "un") + "successfully");
        if (success) {
            if (BuildConfig.DEBUG && obj.getName().compareTo("ObjectPersistence") != 0) {
                throw new AssertionError();
            }
            if (BuildConfig.DEBUG && mSaveState != SaveState.AWAITING_ACK) {
                throw new AssertionError();
            }
            // Two things can happen:
            // Either the Object Save Request did actually go through, and then
            // we should get in
            // "AWAITING_COMPLETED" mode, or the Object Save Request did _not_
            // go through, for example
            // because the object does not exist and then we will never get a
            // subsequent update.
            // For this reason, we will arm a 2 second timer to make provision
            // for this and not block
            // the queue:
            mSaveState = SaveState.AWAITING_COMPLETED;
            obj.removeUAVObjectEventListener(mObjectTransactionListener);
            stopFailureTimer();
            mFailureTimer = new ScheduledThreadPoolExecutor(1);
            mFailureTimerTask = mFailureTimer.schedule(new Runnable() {

                @Override
                public void run() {
                    if (DEBUG)
                        Log.d(TAG, "objectPersistenceTransactionCompleted: failure timer triggered, operation failed.");
                    objectPersistenceOperationFailed();
                }
            }, 2000, TimeUnit.MILLISECONDS);
        } else {
            // Can be caused by timeout errors on sending. Forget it and send
            // next.
            if (DEBUG)
                Log.d(TAG, "objectPersistenceTransactionCompleted (error)");
            mObjectPersistence.removeUAVObjectEventListener(mObjectUpdatedListener);
            mObjectPersistence.removeUAVObjectEventListener(mObjectTransactionListener);
            mQueue.poll(); // We can now remove the object, it failed anyway.
            mSaveState = SaveState.IDLE;
            fireSaveCompleted(mObjectPersistence.getField(ObjectPersistence.FIELD_OBJECTID).getValue().toLong(),
                    false);
            saveNextObject();
        }
    }

    private void stopFailureTimer() {
        if (mFailureTimer != null) {
            mFailureTimer.shutdownNow();
            mFailureTimer = null;
        }
        if (mFailureTimerTask != null) {
            mFailureTimerTask.cancel(true);
            mFailureTimerTask = null;
        }
    }

    private void objectPersistenceOperationFailed() {
        if (DEBUG) Log.d(TAG, "objectPersistenceOperationFailed");
        if (mSaveState == SaveState.AWAITING_COMPLETED) {
            //TODO: some warning that this operation failed somehow
            // We have to disconnect the object persistence 'updated' signal
            // and ask to save the next object:

            assert (mObjectPersistence != null);

            UAVObject obj = mQueue.poll(); // We can now remove the object, it failed anyway.

            if (obj != null) {

                mObjectPersistence.removeUAVObjectEventListener(mObjectUpdatedListener);
                mObjectPersistence.removeUAVObjectEventListener(mObjectTransactionListener);

                mSaveState = SaveState.IDLE;
                fireSaveCompleted(obj.getObjID(), false);

                saveNextObject();
            }
        }
    }

    private void fireSaveCompleted(long objectId, boolean status) {
        if (DEBUG) Log.d(TAG, "fireSaveCompleted " + status);
        for (UAVObjectUtilManagerEventListener curListener : mUAVObjectUtilManagerEventListeners) {
            curListener.saveCompleted(objectId, status);
        }
    }

    private enum SaveState {
        IDLE, AWAITING_ACK, AWAITING_COMPLETED
    }

    public interface UAVObjectUtilManagerEventListener extends EventListener {
        void saveCompleted(long objectID, boolean status);
    }

//    private boolean descriptionToStructure(byte[] desc, deviceDescriptorStruct & struc){
//        if (desc.("OpFw")) {
//        /*
//         * This looks like a binary with a description at the end:
//         *   4 bytes: header: "OpFw".
//         *   4 bytes: GIT commit tag (short version of SHA1).
//         *   4 bytes: Unix timestamp of compile time.
//         *   2 bytes: target platform. Should follow same rule as BOARD_TYPE and BOARD_REVISION in board define files.
//         *  26 bytes: commit tag if it is there, otherwise branch name. '-dirty' may be added if needed. Zero-padded.
//         *  20 bytes: SHA1 sum of the firmware.
//         *  20 bytes: SHA1 sum of the uavo definitions.
//         *  20 bytes: free for now.
//         */
//        int gitCommitHash = desc[7] & 0xFF;
//        for (int i = 1; i < 4; i++) {
//            gitCommitHash  = gitCommitHash << 8;
//            gitCommitHash += desc[7 - i] & 0xFF;
//        }
//
//        //struc.gitHash = QString("%1").arg(gitCommitHash, 8, 16, QChar('0'));
////            quint32 gitDate = desc.at(11) & 0xFF;
////            for (int i = 1; i < 4; i++) {
////                gitDate  = gitDate << 8;
////                gitDate += desc.at(11 - i) & 0xFF;
////            }
////            struc.gitDate = QDateTime::fromTime_t(gitDate).toUTC().toString("yyyyMMdd HH:mm");
////
////            QString gitTag = QString(desc.mid(14, 26));
////            struc.gitTag  = gitTag;
////
////            // TODO: check platform compatibility
//         byte[] targetPlatform = desc.
////            QByteArray targetPlatform = desc.mid(12, 2);
////            struc.boardType     = (int)targetPlatform.at(0);
////            struc.boardRevision = (int)targetPlatform.at(1);
////            struc.fwHash.clear();
////            struc.fwHash   = desc.mid(40, 20);
////            struc.uavoHash.clear();
////            struc.uavoHash = desc.mid(60, 20);
//    }
}
