/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.downloader.bamboo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.JsonReader;

import org.opng.mobilegcs.tasks.bamboo.FetchPlanDataTask;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

public class Plan {
    private final String mRestUrl;
    private final String mProjectKey;
    private final String mPlanKey;
    private final List<Branch> mBranchList = new ArrayList<>();
    private final KeyStore mKeyStore;
    private final Context mContext;
    private Exception mException;

    public Plan(String restUrl, String projectKey, String planKey, KeyStore keyStore, Context context) {
        mRestUrl = restUrl;
        mProjectKey = projectKey;
        mPlanKey = planKey;
        mKeyStore = keyStore;
        mContext = context;
    }

    public String getRestUrl() {
        return mRestUrl;
    }

    public String getProjectKey() {
        return mProjectKey;
    }

    public String getPlanKey() {
        return mPlanKey;
    }

    public KeyStore getKeyStore(){
        return mKeyStore;
    }

    public List<Branch> getBranchList() {
        return mBranchList;
    }

    public Exception getException() {
        return mException;
    }

    public void fetch(FetchPlanDataTask.FetchCallback fetchCallback, String authString) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            FetchPlanDataTask task = new FetchPlanDataTask(fetchCallback, authString);
            task.execute(this);
        }
    }

    public void fromJson(InputStream is) throws UnsupportedEncodingException {
        readJson(new InputStreamReader(is, "UTF-8"));
    }

    private void readJson(InputStreamReader inputStreamReader) {
        JsonReader jsonReader = null;
        try {
            jsonReader = new JsonReader(inputStreamReader);
            jsonReader.beginObject();

            while (jsonReader.hasNext()) {
                String name = jsonReader.nextName();

                if (name.equals("branches")) {
                    readBranches(jsonReader);
                } else {
                    jsonReader.skipValue();
                }
            }

            jsonReader.endObject();
        } catch (IOException | NumberFormatException e) {
            mException = e;
        } finally {
            if (jsonReader != null) {
                try {
                    jsonReader.close();
                } catch (IOException e) {
                    mException = e;
                }
            }
        }

    }

    private void readBranches(JsonReader jsonReader) throws IOException {
        mBranchList.clear();
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            if (name.equals("branch")) {
                jsonReader.beginArray();
                while (jsonReader.hasNext()) {
                    readBranch(jsonReader);
                }
                jsonReader.endArray();
            } else {
                jsonReader.skipValue();
            }
        }
        jsonReader.endObject();
    }

    private void readBranch(JsonReader jsonReader) throws IOException {
        String branchKey = "";
        String branchName = "";
        jsonReader.beginObject();

        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();

            switch (name) {
                case "key":
                    branchKey = jsonReader.nextString();
                    break;
                case "name":
                    branchName = jsonReader.nextString();
                    break;
                default:
                    jsonReader.skipValue();
                    break;
            }
        }

        jsonReader.endObject();

        if (branchKey.length() > 0 && branchName.length() > 0) {
            mBranchList.add(new Branch(this, branchKey, branchName, mContext));
        }
    }
}
