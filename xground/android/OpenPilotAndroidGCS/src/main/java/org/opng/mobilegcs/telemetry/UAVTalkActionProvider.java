/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

import android.os.Bundle;

import org.opng.uavtalk.UAVObjectManager;

/**
 * This interface should be implemented by ActionProvider derived classes that are
 * intended to be used with {@link org.opng.mobilegcs.util.TelemetryActivity}
 * derived classes or Activities that use {@link org.opng.mobilegcs.util.ActivityTelemetryConnectionHandler}.
 */
public interface UAVTalkActionProvider {
    /**
     * This method is called when telemetry is connected and allows the implementing
     * ActionProvider to access its associated {@link org.opng.uavtalk.UAVObject}
     * via the objectManager parameter.
     *
     * @param objectManager gives access to the UAVObjects
     */
    void connected(UAVObjectManager objectManager);

    /**
     * Called when the owning Activity is paused or stopped. The telemetryIsRunning
     * parameter lets the implementing ActionProvider know whether or not telemetry
     * is running as its pause behaviour may differ based on this.
     * <p/>
     * Generally this will be used to stop updating the ActionProvider based on changes
     * to its associated UAVObjects while the Activity is hidden.
     *
     * @param telemetryIsRunning true if telemetry was running at the point of this
     *                           call, false otherwise
     */
    void pause(boolean telemetryIsRunning);

    /**
     * Called when a previously paused or stopped Activity is restarted.
     * <p/>
     * Generally this will be used to start updating the ActionProvider based on changes
     * to its associated UAVObjects.
     */
    void resume();

    /**
     * Called when telemetry is disconnected.
     */
    void disconnected();

    /**
     * Called by the owning Activity when it needs its state to be saved.
     *
     * @param outState bundle to save the state in, provided by the Activity
     */
    void saveState(Bundle outState);

    /**
     * Called by the owning Activity when it needs to restore a previously saved
     * state.
     *
     * @param savedInstanceState bundle containing previously saved state
     */
    void restoreState(Bundle savedInstanceState);

    /**
     * Called whenever we need to reset the state of this ActionProvider.
     */
    void setInitialValues();
}
