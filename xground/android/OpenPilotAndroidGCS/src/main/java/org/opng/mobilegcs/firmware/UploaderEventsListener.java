/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware;

import org.opng.mobilegcs.firmware.flightcontroller.Uploader;
import org.opng.mobilegcs.io.IAP;

public interface UploaderEventsListener {
    /**
     * Indicates progress of the device operation.
     *
     * @param progressStep current operation
     * @param progress     relative progress
     */
    void bootProgress(Uploader.ProgressStep progressStep, int progress);

    /**
     * Fired if the device successfully boots into IAP mode
     */
    void bootSucceeded();

    /**
     * Fired if the device fails to boot into IAP mode
     */
    void bootFailed();

    /**
     * Indicates percentage progress of the firmware upload to the device
     *
     * @param progress percentage progress
     */
    void updateProgress(int progress);

    /**
     * Indicates that a download from the device is complete
     *
     * @param status ending status of the operation
     */
    void downloadFinished(IAP.Status status);

    /**
     * Indicates that an upload to the device is complete.
     *
     * @param status ending status of the operation
     */
    void uploadFinished(IAP.Status status);

    /**
     * Provides textual descriptions of progress as firmware is updated.
     *
     * @param status description of current step
     */
    void updateOperationProgress(String status);

    /**
     * Provides debug messages from the attached device
     *
     * @param message debug message
     */
    void debugMessage(String message);

    /**
     * Indicates that the device successfully reset to run
     * the flight code.
     */
    void runningFlightCode();
}
