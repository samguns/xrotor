/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.ViewSwitcher;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.BaseFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class StabiResponsivenessCombinedFragment extends BaseFragment implements StabiGuiFragment {

    private View mRootView;
    private UAVObjectManagerLoaderHandler mUAVObjectManagerLoaderHandler;
    private CheckBox mCheckAdvanced;
    private ViewSwitcher mResponsivenessSwitcher;

    public StabiResponsivenessCombinedFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_stabi_responsiveness_combined, container, false);

        // Set up the view switcher for basic/advanced switching
        mResponsivenessSwitcher = (ViewSwitcher) mRootView.findViewById(R.id.switchResponsiveness);
        mCheckAdvanced = (CheckBox) mRootView.findViewById(R.id.checkShowAdvanced);
        setupViewSwitchers(mResponsivenessSwitcher, mCheckAdvanced);

        // Set up our loader handler: get all the controls it needs and then create it
        Button apply = (Button) mRootView.findViewById(R.id.buttonCommonFooterApply);
        Button save = (Button) mRootView.findViewById(R.id.buttonCommonFooterSave);
        Button btnDefault = (Button) mRootView.findViewById(R.id.buttonCommonFooterDefault);
        Button restore = (Button) mRootView.findViewById(R.id.buttonCommonFooterRestore);
        CheckBox autoUpdate = (CheckBox) mRootView.findViewById(R.id.checkFcUpdateRealtime);
        Spinner bankSelect = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);

        mUAVObjectManagerLoaderHandler = new UAVObjectManagerLoaderHandler(getActivity(), this, autoUpdate, bankSelect, apply, save,
                btnDefault, restore, R.integer.stabi_responsivness_button_group);

        getLoaderManager().initLoader(0, null, mUAVObjectManagerLoaderHandler);

        return mRootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onStop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onResume();
        }

        if (mCheckAdvanced != null && mResponsivenessSwitcher != null) {
            mResponsivenessSwitcher.setDisplayedChild(mCheckAdvanced.isChecked() ? 1 : 0);
        }
    }

    private void setupViewSwitchers(final ViewSwitcher viewSwitcher, final CheckBox checkAdvanced) {
        checkAdvanced.setVisibility(View.VISIBLE);
        checkAdvanced.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkAdvanced.isChecked()) {
                    viewSwitcher.showNext();
                } else {
                    viewSwitcher.showPrevious();
                }
            }
        });
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        if (mRootView != null) {
            // Our view is created, safe to setup the gui to uavobject
            // bindings
            FragmentManager fragmentManager = getChildFragmentManager();
            Fragment basicResponsiveness = fragmentManager.findFragmentById(R.id.basicResponsiveness);
            Fragment advancedResponsiveness = fragmentManager.findFragmentById(R.id.advancedResponsiveness);
            ((StabiGuiFragment) basicResponsiveness).setupGuiHandler(helper);
            ((StabiGuiFragment) advancedResponsiveness).setupGuiHandler(helper);
        }
    }
}
