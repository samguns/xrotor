/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.optune;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.tasks.network.FetchOpTuneVarsTask;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

class OPTuneFactors {
    private static final String OPTUNE_VARS_URL = "https://forums.openpilot.org/optunevars.php";
    /**
     * Suggested Kp is multiplied by this to get suggested Kp for Yaw
     */
    @SuppressWarnings("FieldCanBeLocal")
    private final double mYawFactor = 1.8f;
    /**
     * Adjustment applied to proportional factor to take account of
     * ESC type
     */
    private double mEscTypeAdjustment = 1f;
    /**
     * UOV is multiplied by this to get suggested Kp
     */
    private double mProportionalFactor = 0.65f * mEscTypeAdjustment;
    /**
     * Suggested Kp is multiplied by this to get suggested Ki
     */
    private double mIntegralFactor = 2.5f;
    /**
     * Suggested Kp is multiplied by this to get suggested Kd
     */
    private double mDerivativeFactor = 0.013f;
    /**
     * Suggested Yaw Kp is multiplied by this to get suggested yaw Ki
     */
    private double mYawProportionalFactor = 1.3f;
    /**
     * Suggested Yaw Kp is multiplied by this to get suggested yaw Ki
     */
    private double mYawIntegralFactor = 2.5f;
    /**
     * Suggested Yaw Kp is multiplied by this to get suggested yaw Ki
     */
    private double mYawDerivativeFactor = 0.013f;
    private String[] mEscTypes;
    private String[] mTuningStyles;
    private String[] mYawStyles;
    private FetchOpTuneVarsTask mFetchOpTuneVarsTask;
    private OpTuneVars mOptuneVars;
    private int mSelectedYawStyle;
    private int mSelectedTuningStyle;
    private int mSelectedEscType;
    private Context mContext;

    public double getProportionalFactor() {
        return mProportionalFactor;
    }

    public double getIntegralFactor() {
        return mIntegralFactor;
    }

    public double getDerivativeFactor() {
        return mDerivativeFactor;
    }

    public double getYawFactor() {
        return mYawFactor;
    }

    public double getYawProportionalFactor() {
        return mYawProportionalFactor;
    }

    public double getYawIntegralFactor() {
        return mYawIntegralFactor;
    }

    public double getYawDerivativeFactor() {
        return mYawDerivativeFactor;
    }

    public String[] getEscTypes() {
        return mEscTypes;
    }

    public String[] getTuningStyles() {
        return mTuningStyles;
    }

    public String[] getYawStyles() {
        return mYawStyles;
    }

    public void initialize(Context context) {
        mEscTypes = context.getResources().getStringArray(R.array.optuneEscTypes);
        mTuningStyles = context.getResources().getStringArray(R.array.optuneTuningStyles);
        mYawStyles = context.getResources().getStringArray(R.array.optuneYawStyles);
        mContext = context;

        fetchOPTuneVars();
    }

    public void determineSelectedFactors() {
        if (mFetchOpTuneVarsTask != null && mOptuneVars == null) {
            try {
                mOptuneVars = mFetchOpTuneVarsTask.get(5, TimeUnit.SECONDS);
            } catch (InterruptedException | TimeoutException | ExecutionException e) {
                // Read from local resource instead
                mOptuneVars = new OpTuneVars(mContext.getString(R.string.optune_vars_json));
            }
        }

        if (mOptuneVars != null) {
            mEscTypeAdjustment = mOptuneVars.getEscFactor(mSelectedEscType);
            mProportionalFactor = mOptuneVars.getProportionalFactor(mSelectedTuningStyle);
            mIntegralFactor = mOptuneVars.getIntegralFactor(mSelectedTuningStyle);
            mDerivativeFactor = mOptuneVars.getDerivativeFactor(mSelectedTuningStyle);
            mYawProportionalFactor = mOptuneVars.getYawProportionalFactor(mSelectedYawStyle);
            mYawIntegralFactor = mOptuneVars.getYawIntegralFactor(mSelectedYawStyle);
            mYawDerivativeFactor = mOptuneVars.getYawDerivativeFactor(mSelectedYawStyle);
        }
    }

    private void fetchOPTuneVars() {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            mFetchOpTuneVarsTask = new FetchOpTuneVarsTask(mContext);
            mFetchOpTuneVarsTask.execute(OPTUNE_VARS_URL);
        }
    }

    public void setSelectedYawStyle(int selectedYawStyle) {
        mSelectedYawStyle = selectedYawStyle;
    }

    public void setSelectedTuningStyle(int selectedTuningStyle) {
        mSelectedTuningStyle = selectedTuningStyle;
    }

    public void setSelectedEscType(int selectedEscType) {
        mSelectedEscType = selectedEscType;
    }
}