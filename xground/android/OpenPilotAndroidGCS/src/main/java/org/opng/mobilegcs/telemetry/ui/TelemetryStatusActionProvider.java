/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.telemetry.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ActionProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.telemetry.UAVTalkActionProvider;
import org.opng.mobilegcs.util.Variant;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectEventListener;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.GCSTelemetryStats;

import java.text.NumberFormat;

/**
 * This class is used to display transmit and receive data rates using a bar graph
 * on an activity's action bar. An instance of this class can be added to the action
 * bar by passing ActivityTelemetryConnectionHandler.TELEMETRY_STATUS to the
 * setActionProvidersToShow method in {@link org.opng.mobilegcs.util.TelemetryActivity}
 * derived classes or by calling addActionProviderWidgets directly on
 * {@link org.opng.mobilegcs.util.ActivityTelemetryConnectionHandler}.
 */
public class TelemetryStatusActionProvider extends ActionProvider implements UAVTalkActionProvider {

    private static final int LOGLEVEL = 0;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final String TELEMETRY_WIDGET_TX_LEVEL = "TelemetryWidgetTxLevel";
    private static final String TELEMETRY_WIDGET_RX_LEVEL = "TelemetryWidgetRxLevel";
    private static final NumberFormat mFormatter = NumberFormat.getInstance();
    private static boolean WARN = LOGLEVEL > 0;

    static {
        mFormatter.setMaximumFractionDigits(0);
    }

    // Logging setup, TAG somewhat abbreviated to fit into limit for Log tag
    private final String TAG = "TelemStatusActnProvider";
    private final Context mContext;
    private final Handler mHandler;
    private ImageView mTxScale;
    private ImageView mRxScale;
    private TextView mTxRate;
    private TextView mRxRate;
    private GCSTelemetryStats mGCSTelemetryStats;
    private boolean mConnected = false;
    private double mRxInitValue = 0.0;
    private double mTxInitValue = 0.0;
    private double mRxLevel = 0.0;
    private double mTxLevel = 0.0;
    private final UAVObjectEventListener mGcsStatsListener = new UAVObjectEventAdapter() {
        @Override
        public void objectUpdated(final UAVObject obj) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    Variant txDataRate = obj.getField(GCSTelemetryStats.FIELD_TXDATARATE).getValue();
                    Variant rxDataRate = obj.getField(GCSTelemetryStats.FIELD_RXDATARATE).getValue();
                    setValues(txDataRate, rxDataRate);
                    mRxLevel = rxDataRate.toDouble();
                    mTxLevel = txDataRate.toDouble();
                }
            });
        }
    };

    public TelemetryStatusActionProvider(Context context) {
        super(context);
        mContext = context;

        // We should be on our activity's thread here, get a handler for it
        mHandler = new Handler();
    }

    @Override
    public View onCreateActionView() {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View retVal = inflater.inflate(R.layout.actionbar_telemetry, null);

        mTxScale = (ImageView) retVal.findViewById(R.id.imageViewTx);
        mRxScale = (ImageView) retVal.findViewById(R.id.imageViewRx);
        mTxRate = (TextView) retVal.findViewById(R.id.textViewTxRate);
        mRxRate = (TextView) retVal.findViewById(R.id.textViewRxRate);

        retVal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mConnected) {
                    Intent runTelemActivity = new Intent(mContext, TelemetryStatsActivity.class);
                    mContext.startActivity(runTelemActivity);
                }
            }
        });

        return retVal;
    }

    @Override
    public void connected(UAVObjectManager objectManager) {
        if (DEBUG) Log.d(TAG, "Connected");
        mConnected = true;
        if (objectManager != null) {
            if (DEBUG) Log.d(TAG, "Begin initialise stats object handlers");
            initGcsStats(objectManager);
            if (DEBUG) Log.d(TAG, "End initialise stats object handlers");
        }
    }

    @Override
    public void pause(boolean telemetryIsRunning) {
        if (DEBUG) Log.d(TAG, "Pause telemetry stats updates");

        // This will remove the object updated listener, stopping updating
        // of the display
        cleanupGcsStats(telemetryIsRunning);
    }

    @Override
    public void resume() {
        if (DEBUG) Log.d(TAG, "Resume telemetry stats updates");

        // This will re-add the object updated listener, resuming updating
        // of the display
        initGcsStats(null);
    }

    @Override
    public void disconnected() {
        if (DEBUG) Log.d(TAG, "Disconnected");
        mConnected = false;
        if (DEBUG) Log.d(TAG, "Begin cleanup stats object handlers");
        cleanupGcsStats(false);
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                mTxScale.setImageLevel(0);
                mRxScale.setImageLevel(0);
                mTxRate.setText("0");
                mRxRate.setText("0");
            }
        });
        if (DEBUG) Log.d(TAG, "End cleanup stats object handlers");
    }

    @Override
    public void saveState(Bundle outState) {
        outState.putDouble(TELEMETRY_WIDGET_RX_LEVEL, mRxLevel);
        outState.putDouble(TELEMETRY_WIDGET_TX_LEVEL, mTxLevel);
    }

    @Override
    public void restoreState(Bundle savedInstanceState) {
        mRxInitValue = savedInstanceState.getDouble(TELEMETRY_WIDGET_RX_LEVEL);
        mTxInitValue = savedInstanceState.getDouble(TELEMETRY_WIDGET_TX_LEVEL);
        setInitialValues();
        if (DEBUG)
            Log.d(TAG, "Tx init. level: " + mTxInitValue + ", Rx init. level: " + mRxInitValue);
    }

    @Override
    public void setInitialValues() {
        mRxLevel = mRxInitValue;
        mTxLevel = mTxInitValue;
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                setValues(new Variant(mTxInitValue), new Variant(mRxInitValue));
            }
        });
    }

    private void initGcsStats(UAVObjectManager objectManager) {
        if (objectManager != null) {
            mGCSTelemetryStats = (GCSTelemetryStats) objectManager.getObject("GCSTelemetryStats");
        }

        if (mGCSTelemetryStats != null) {
            if (DEBUG) Log.d(TAG, "Adding object updated listener");
            mGCSTelemetryStats.addUAVObjectEventListener(mGcsStatsListener);
        }
    }

    private void cleanupGcsStats(boolean telemetryIsRunning) {
        if (mGCSTelemetryStats != null) {
            if (DEBUG) Log.d(TAG, "Removing object updated listener");
            mGCSTelemetryStats.removeUAVObjectEventListener(mGcsStatsListener);
        }

        // If telemetry is running, we don't want to reset to zero. Display
        // will remain at last value updated.
        if (!telemetryIsRunning) {
            mRxLevel = 0.0;
            mTxLevel = 0.0;
        }
    }

    private void setValues(Variant txDataRate, Variant rxDataRate) {
        mRxScale.setImageLevel(rxDataRate.toInt());
        mRxRate.setText(rxDataRate.toString(mFormatter));
        mTxScale.setImageLevel(txDataRate.toInt());
        mTxRate.setText(txDataRate.toString(mFormatter));
    }
}
