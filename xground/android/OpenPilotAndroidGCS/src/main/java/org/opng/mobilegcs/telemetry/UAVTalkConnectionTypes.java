/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

public class UAVTalkConnectionTypes {

    //public static final int UAVTALK_CONNECTION_NONE = 0;
    /**
     * Bluetooth uavtalk connection.
     */
    public static final int UAVTALK_CONNECTION_BLUETOOTH = 0;

    /**
     * USP uavtalk connection.
     */
    public static final int UAVTALK_CONNECTION_USB = 2;

    /**
     * UDP uavtalk connection.
     */
    //public static final int UAVTALK_CONNECTION_UDP = 3;

    /**
     * TCP uavtalk connection.
     */
    public static final int UAVTALK_CONNECTION_TCP = 1;

    //Not instantiable
    private UAVTalkConnectionTypes() {
    }

}
