/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.logging.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Checkable;
import android.widget.CheckedTextView;
import android.widget.TextView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.logging.FileListObject;

import java.util.ArrayList;

/**
 * Logging Adapter
 */
public class LoggingAdapter extends ArrayAdapter<FileListObject> implements Checkable {

    private final Context context;
    private final int layoutResourceId;
    private ArrayList<FileListObject> data = new ArrayList<>();

    private boolean isChecked;


    public LoggingAdapter(Context context, int layoutResourceId, ArrayList<FileListObject> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;

        //selected = new boolean[data.size()];
    }

    @Override
    public boolean isChecked() {
        return isChecked;
    }

    @Override
    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    @Override
    public void toggle() {
        setChecked(!isChecked);
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            convertView = inflater.inflate(layoutResourceId, parent, false);

            holder = new ViewHolder();
            holder.txtFilename = (TextView) convertView.findViewById(R.id.txtRowFileName);
            holder.txtFilesize = (TextView) convertView.findViewById(R.id.txtRowFileSize);
            holder.checkBox = ((CheckedTextView) convertView.findViewById(R.id.checkedTextView1));

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String fileName = data.get(position).getFileName();
        String fileSize = data.get(position).getFileSize();
        boolean checked = data.get(position).isSelected();

        holder.txtFilename.setText(fileName);
        holder.txtFilesize.setText(fileSize);
        holder.checkBox.setChecked(checked);
        return convertView;
    }

    public ArrayList<FileListObject> getData() {
        return data;
    }

    static class ViewHolder {
        TextView txtFilename;
        TextView txtFilesize;
        CheckedTextView checkBox;
    }


}
