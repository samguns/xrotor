/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.objectbrowser;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectManager;

import java.util.List;

/**
 * A list fragment representing a list of UAVObjects. This fragment also
 * supports tablet devices by allowing list items to be given an 'activated'
 * state upon selection. This helps indicate which item is currently being
 * viewed in a {@link UAVObjectDetailFragment}.
 * <p/>
 * Activities containing this fragment MUST implement the {@link Callbacks}
 * interface.
 */
public class UAVObjectListFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {
    /**
     * The serialization (saved instance state) Bundle key representing the
     * activated item position. Only used on tablets.
     */
    private static final String STATE_ACTIVATED_POSITION = "activated_position";
    /**
     * A dummy implementation of the {@link Callbacks} interface that does
     * nothing. Used only when this fragment is not attached to an activity.
     */
    private static final Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onItemSelected(String id) {
        }
    };
    private UAVObjectExpandableListAdapter mUAVObjectExpandableListAdapter;
    /**
     * The fragment's current callback object, which is notified of list item
     * clicks.
     */
    private Callbacks mCallbacks = sDummyCallbacks;

//	private ObjectManagerFragmentHelper _objectManagerHelper = new ObjectManagerFragmentHelper();
    /**
     * The current activated item position. Only used on tablets.
     */
    private int mActivatedPosition = ExpandableListView.INVALID_POSITION;

//	final Handler uavobjHandler = new Handler();
//	final Runnable updateText = new Runnable() {
//		@Override
//		public void run() {
//			updateObject();
//		}
//	};
//
//	private final Observer updatedObserver = new Observer() {
//		@Override
//		public void update(Observable observable, Object data) {
//			uavobjHandler.post(updateText);
//		}
//	};
    private ExpandableListView mListView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UAVObjectListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Create an empty adapter we will use to display the loaded data.
        mUAVObjectExpandableListAdapter = new UAVObjectExpandableListAdapter(getActivity(), android.R.layout.simple_expandable_list_item_1,
                android.R.id.text1, android.R.layout.simple_expandable_list_item_1, android.R.id.text1,
                android.R.layout.simple_expandable_list_item_1, android.R.id.text1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View retVal = inflater.inflate(R.layout.fragment_uavobject_list, container, false);
        mListView = (ExpandableListView) retVal.findViewById(R.id.uavo_list);
        mListView.setAdapter(mUAVObjectExpandableListAdapter);
        mListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View view, int groupPosition, int childPosition, long id) {
                onListItemClick(parent, groupPosition, childPosition);
                return true;
            }
        });
        return retVal;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Prepare the loader.  Either re-connect with an existing one,
        // or start a new one.
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Restore the previously serialized activated item position.
        if (savedInstanceState != null
                && savedInstanceState.containsKey(STATE_ACTIVATED_POSITION)) {
            setActivatedPosition(savedInstanceState
                    .getInt(STATE_ACTIVATED_POSITION));
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Activities containing this fragment must implement its callbacks.
        if (!(context instanceof Callbacks)) {
            throw new IllegalStateException(
                    "Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) context;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        // Reset the active callbacks interface to the dummy implementation.
        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mActivatedPosition != ExpandableListView.INVALID_POSITION) {
            // Serialize and persist the activated item position.
            outState.putInt(STATE_ACTIVATED_POSITION, mActivatedPosition);
        }
    }

    /**
     * Turns on activate-on-click mode. When this mode is on, list items will be
     * given the 'activated' state when touched.
     */
    public void setActivateOnItemClick(boolean activateOnItemClick) {
        // When setting CHOICE_MODE_SINGLE, ListView will automatically
        // give items the 'activated' state when touched.
        mListView.setChoiceMode(
                activateOnItemClick ? ExpandableListView.CHOICE_MODE_SINGLE
                        : ExpandableListView.CHOICE_MODE_NONE
        );
    }

    private void onListItemClick(ExpandableListView parent, int groupPosition, int childPosition) {
        // Get selected item's associated UAVObject
        UAVObject object = (UAVObject) parent.getExpandableListAdapter().getChild(groupPosition, childPosition);

        if (object != null) {
            // Notify the active callbacks interface (the activity, if the
            // fragment is attached to one) that an item has been selected.
            mCallbacks.onItemSelected(object.getName() + ":" +
                    object.getObjID() + ":" +
                    object.getInstID());
        }
    }

    private void setActivatedPosition(int position) {
        if (position == ExpandableListView.INVALID_POSITION) {
            mListView.setItemChecked(mActivatedPosition, false);
        } else {
            mListView.setItemChecked(position, true);
        }

        mActivatedPosition = position;
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int arg0, Bundle arg1) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> arg0, UAVObjectManager data) {
        if (data != null && mUAVObjectExpandableListAdapter.isEmpty()) {
            for (List<UAVObject> curObjectList : data.getObjects()) {
                for (UAVObject curObject : curObjectList) {
                    if (!curObject.isMetadata()) {
                        mUAVObjectExpandableListAdapter.addUAVObject(curObject);
                    }
                }
            }
        }
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> arg0) {
        mUAVObjectExpandableListAdapter.clear();
    }

    /**
     * A callback interface that all activities containing this fragment must
     * implement. This mechanism allows activities to be notified of item
     * selections.
     */
    public interface Callbacks {
        /**
         * Callback for when an item has been selected.
         */
        void onItemSelected(String id);
    }
}
