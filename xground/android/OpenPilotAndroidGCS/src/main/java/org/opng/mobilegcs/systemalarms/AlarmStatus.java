/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.systemalarms;

import org.opng.uavtalk.uavobjects.SystemAlarms;

public enum AlarmStatus {
    UNINITIALISED(SystemAlarms.ALARM_UNINITIALISED, 0),
    OK(SystemAlarms.ALARM_OK, 1),
    WARNING(SystemAlarms.ALARM_WARNING, 2),
    ERROR(SystemAlarms.ALARM_ERROR, 3),
    CRITICAL(SystemAlarms.ALARM_CRITICAL, 4);

    private final String mValue;
    private final int mPriority;

    AlarmStatus(String value, int priority) {
        mValue = value;
        mPriority = priority;
    }

    public static AlarmStatus fromString(String value) {
        AlarmStatus retVal = UNINITIALISED;
        switch (value) {
            case SystemAlarms.ALARM_OK:
                retVal = OK;
                break;
            case SystemAlarms.ALARM_WARNING:
                retVal = WARNING;
                break;
            case SystemAlarms.ALARM_ERROR:
                retVal = ERROR;
                break;
            case SystemAlarms.ALARM_CRITICAL:
                retVal = CRITICAL;
                break;
        }

        return retVal;
    }

    public boolean equals(AlarmStatus other) {
        return mValue.equals(other.mValue);
    }

    public String toString() {
        return mValue;
    }

    public short compare(AlarmStatus other) {
        short retVal = 0;

        if (mPriority > other.mPriority) {
            retVal = 1;
        } else if (mPriority < other.mPriority) {
            retVal = -1;
        }

        return retVal;
    }
}
