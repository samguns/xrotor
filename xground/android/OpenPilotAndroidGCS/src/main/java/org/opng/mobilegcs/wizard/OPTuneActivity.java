/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.wizard;

import android.os.Bundle;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.BaseActivity;

public class OPTuneActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_optune);
        initToolbar(R.id.toolbar);
    }
}
