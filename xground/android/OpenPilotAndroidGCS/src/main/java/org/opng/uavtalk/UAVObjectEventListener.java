/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

import java.util.EventListener;

public interface UAVObjectEventListener extends EventListener {
    void objectUpdated(UAVObject obj);

    void objectUpdatedAuto(UAVObject obj);

    void objectUpdatedManual(UAVObject obj, boolean all);

    void objectUpdatedPeriodic(UAVObject obj);

    void objectUnpacked(UAVObject obj);

    void updateRequested(UAVObject obj, boolean all);

    void transactionCompleted(UAVObject obj, boolean success);

    void newInstance(UAVObject obj);

}
