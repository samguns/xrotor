/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.logging;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.ActivityTelemetryConnectionHandler;
import org.opng.mobilegcs.util.TelemetryActivity;

public class LoggingActivity extends TelemetryActivity {

    private View mSdLogManagerView;
    private View mFCLogManagerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logging);

        setActionProvidersToShow(ActivityTelemetryConnectionHandler.TELEMETRY_STATUS |
                ActivityTelemetryConnectionHandler.SYSTEM_ALARMS);

        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBarToolbar);

        // Show the Up button in the toolbar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        FragmentManager fragmentManager = getSupportFragmentManager();

        // Add the sdlogmanager fragment
        Fragment sdLogfragment = fragmentManager.findFragmentById(R.id.sdlog_manager);
        if (sdLogfragment == null) {
            sdLogfragment = new SDLoggingFragment();
            fragmentManager.beginTransaction().add(R.id.sdlog_manager, sdLogfragment)
                    .commit();
        }

        // Add the fclogmanager fragment
        Fragment sdFCfragment = fragmentManager.findFragmentById(R.id.fclog_manager);
        if (sdFCfragment == null) {
            sdFCfragment = new FlightControlLogging();
            fragmentManager.beginTransaction().add(R.id.fclog_manager, sdFCfragment)
                    .commit();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        Intent myIntent = new Intent(getApplicationContext(), WelcomeActivity.class);
//        startActivityForResult(myIntent, 0);
        return true;
    }


}
