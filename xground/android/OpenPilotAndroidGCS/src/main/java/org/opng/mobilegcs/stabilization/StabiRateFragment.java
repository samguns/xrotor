/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opng.mobilegcs.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StabiRateFragment extends Fragment implements StabiGuiFragment {


    private View mRootView;

    public StabiRateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_stabi_rate, container, false);

        return mRootView;
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper){
        helper.bindPitchRatePidWidgets(mRootView.findViewById(R.id.textPitchRateKp),
                1, mRootView.findViewById(R.id.textPitchRateKi),
                1, mRootView.findViewById(R.id.textPitchRateKd), 1);
        helper.bindRollRatePidWidgets(mRootView.findViewById(R.id.textRollRateKp),
                1, mRootView.findViewById(R.id.textRollRateKi),
                1, mRootView.findViewById(R.id.textRollRateKd), 1);
        helper.bindYawRatePidWidgets(mRootView.findViewById(R.id.textYawRateKp),
                1, mRootView.findViewById(R.id.textYawRateKi),
                1, mRootView.findViewById(R.id.textYawRateKd), 1);
    }
}
