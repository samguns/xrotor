/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectUtilManager.UAVObjectUtilManagerEventListener;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObject.AccessMode;
import org.opng.uavtalk.UAVObjectEventAdapter;

import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SmartSave {
    private static final int UPDATE_SAVE_TIMEOUT = 3000;
    private final static String TAG = SmartSave.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final UAVObjectUtilManager mUAVObjectUtilManager;
    /**
     * Using a Set here to save having to do a contains check when
     * we add objects to this list. Guarantees that we only have one
     * instance of a given UAVObject in the list.
     */
    private final Set<UAVObject> mDisableSaveList = new HashSet<>();
    /**
     * Thread pool with two threads, one to run the entire upload/save process and
     * one to run the individual upload and save of a single object
     */
    private final ExecutorService mSmartSaveExecutor = Executors.newFixedThreadPool(2);
    private final List<UAVDataObject> mUAVDataObjects = new ArrayList<>();
    private final Map<Button, ButtonTypeEnum> mButtonList = new HashMap<>();
    private final Set<SmartSaveEventListener> mSmartSaveEventListeners = new CopyOnWriteArraySet<>();
    private final ButtonHandler mButtonHandler = new ButtonHandler();
    private long mCurrentObjectID;
    private UAVDataObject mCurrentObject;
    private boolean mUploadResult;
    private boolean mSaveResult;
    private Handler mHandler;
    /**
     * Countdown latch used to notify successful completion of an upload or save. A
     * timeout while waiting for this latch indicates a failure to upload or save
     */
    private CountDownLatch mTransactionTimeoutLatch;
    private final UAVObjectEventAdapter mTransactionCompleteListener = new UAVObjectEventAdapter() {

        @Override
        public void transactionCompleted(UAVObject obj, boolean success) {
            transactionFinished(obj, success);
        }
    };
    private final UAVObjectUtilManagerEventListener mUAVObjectUtilManagerEventListener = new UAVObjectUtilManagerEventListener() {

        @Override
        public void saveCompleted(long objectID, boolean status) {
            savingFinished(objectID, status);
        }
    };
    // ! Create a smart save button attached to the object manager and an apply
    // and save button
    public SmartSave(UAVObjectUtilManager objUtilMngr, UAVObject obj,
                     Button saveButton, Button applyButton) {
        this(objUtilMngr);
        addButtons(saveButton, applyButton);

        assert (obj != null);
    }

    public SmartSave(UAVObjectUtilManager objUtilMngr) {
        assert (objUtilMngr != null);
        mUAVObjectUtilManager = objUtilMngr;

        // This assumes that this instance was created on the same thread as the buttons
        // we will be attached to. In this case, this handler will allow us to manipulate
        // the buttons on the correct thread.
        mHandler = new Handler();
    }

    public void addEventListener(SmartSaveEventListener listener) {
        mSmartSaveEventListeners.add(listener);
    }

    public void removeEventListener(SmartSaveEventListener listener) {
        mSmartSaveEventListeners.remove(listener);
    }

    public void addButtons(Button save, Button apply) {
        addApplyButton(apply);
        addSaveButton(save);
    }

    public void addApplyButton(Button apply) {
        mButtonList.put(apply, ButtonTypeEnum.APPLY_BUTTON);
        apply.setOnClickListener(mButtonHandler);
    }

    public void addSaveButton(Button save) {
        mButtonList.put(save, ButtonTypeEnum.SAVE_BUTTON);
        save.setOnClickListener(mButtonHandler);
    }

    public void addObject(UAVDataObject obj) {
        assert (obj != null);
        if (!mUAVDataObjects.contains(obj)) {
            mUAVDataObjects.add(obj);
        }
    }

    /**
     * Use this method to enable saves for the given object when saves
     * have previously been disabled with disableSavesForObject().
     *
     * @param object UAVObject to re-enable saves for
     */
    public void enableSavesForObject(UAVObject object) {
        mDisableSaveList.remove(object);
    }

    /**
     * There may be times when we want to disable saving for a given UAVObject.
     * For example, we may have a GUI used to set up an optional firmware module
     * where the widget used to enable/disable the module is bound to, in this
     * case, the HwSettings UAVObject. When we enable/disable the module and
     * save settings to the flight controller, we don't want to attempt a save
     * of settings for a disabled module as this will appear to cause an error
     * on the upload or save button.<p/>
     * To avoid this, call this method to disable saves on the given object when
     * attempting to save it would result in a NACK.<p/>
     * Remember to call enableSavesForObject to re-enable saves as appropriate.
     *
     * @param object UAVObject to disable saves for
     */
    public void disableSavesForObject(UAVObject object) {
        mDisableSaveList.add(object);
    }

    public void enableControls(boolean value) {
        for (Button button : mButtonList.keySet()) {
            button.setEnabled(value);
        }
    }

    public void resetIcons() {
        for (Button button : mButtonList.keySet()) {
            button.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
        }
    }

    public void apply() {
        processOperation(null, false);
    }

    public void apply(UAVDataObject object) {
        if (saveObject(object, false)) {
            fireSaveSuccessful();
        }
    }

    public void save() {
        processOperation(null, true);
    }

    public void save(UAVDataObject object) {
        if (saveObject(object, true)) {
            fireSaveSuccessful();
        }
    }

    private void savingFinished(long objectID, boolean status) {
        if (objectID == mCurrentObjectID) {
            mSaveResult = status;
            if (DEBUG)
                Log.d(TAG, "Saving completed for object ID: " + objectID + ". Result: " + status);
            if (mTransactionTimeoutLatch != null) {
                mTransactionTimeoutLatch.countDown();
            }
        } else {
            if (DEBUG)
                Log.d(TAG, "Saving completed for object ID: " + objectID + " Expected " + mCurrentObjectID + ". Result: " + status);
        }
    }

    private void transactionFinished(UAVObject obj, boolean status) {
        if (mCurrentObject == obj) {
            mUploadResult = status;
            if (DEBUG)
                Log.d(TAG, "Transaction completed for " + obj.getName() + ". Result: " + status);
            if (mTransactionTimeoutLatch != null) {
                mTransactionTimeoutLatch.countDown();
            }
        }
    }

    private void processOperation(final Button button, boolean save) {
        firePreProcessOperations();
        if (button != null) {
            mHandler.post(new Runnable() {

                @Override
                public void run() {
                    button.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_system_run, 0, 0, 0);
                    button.setEnabled(false);
                }
            });
        } else if (DEBUG) Log.d(TAG, "Button is null");

        boolean error = false;

        if (DEBUG) Log.d(TAG, "Processing " + mUAVDataObjects.size() + " object updates");

        for (final UAVDataObject obj : mUAVDataObjects) {
            if (!saveObject(obj, save)) {
                error = true;
            }
        }
        mHandler.post(new Runnable() {

            @Override
            public void run() {
                if (button != null) {
                    button.setEnabled(true);
                }
            }
        });
        if (!error) {
            if (button != null) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        button.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_dialog_apply, 0, 0, 0);
                    }
                });
            }
            fireSaveSuccessful();
        } else {
            if (button != null) {
                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        button.setCompoundDrawablesWithIntrinsicBounds(
                                R.drawable.ic_process_stop, 0, 0, 0);
                    }
                });
            }
        }
        fireEndOp();
    }

    private boolean saveObject(final UAVDataObject obj, boolean save) {
        boolean succeeded = true;
        UAVObject.Metadata mdata = obj.getMetadata();
        if (mdata.GetGcsAccess() == AccessMode.ACCESS_READONLY || mDisableSaveList.contains(obj))
            return false;
        mUploadResult = false;
        mCurrentObject = obj;

        // Try three times to upload
        for (int i = 0; i < 3; ++i) {
            if (DEBUG)
                Log.d(TAG,
                        "Upload try number " + i + " Object "
                                + obj.getName());

            // Look for transaction complete, handling this will countdown our countdown latch.
            obj.addUAVObjectEventListener(mTransactionCompleteListener);
            mTransactionTimeoutLatch = new CountDownLatch(1);

            // Submit our object update to the thread pool
            mSmartSaveExecutor.submit(new Runnable() {

                @Override
                public void run() {
                    obj.updated();
                }
            });

            // Wait for our latch to countdown to zero (initialized to one)
            try {
                if (mTransactionTimeoutLatch.await(UPDATE_SAVE_TIMEOUT, TimeUnit.MILLISECONDS)) {
                    if (DEBUG)
                        Log.d(TAG, "Upload did not timeout " + i + " Object "
                                + obj.getName());
                } else {
                    if (DEBUG)
                        Log.d(TAG,
                                "Upload TIMEOUT " + i + " Object "
                                        + obj.getName());
                }
            } catch (InterruptedException e) {
                if (DEBUG)
                    Log.d(TAG, "Upload thread interrupted " + i + " Object "
                            + obj.getName());
            }

            // Stop listening for transaction complete
            obj.removeUAVObjectEventListener(mTransactionCompleteListener);

            // If the transaction was successful, _upResult will be true, we
            // can stop trying.
            if (DEBUG) Log.d(TAG, "Upload " + (mUploadResult ? "" : "un") + "successful");
            if (mUploadResult)
                break;
        }
        if (!mUploadResult) {

            // Run out of retries, skip to next object
            if (DEBUG)
                Log.d(TAG, "Object upload error:" + obj.getName());
            return true;
        }
        mSaveResult = false;
        mCurrentObjectID = obj.getObjID();

        // Do we want to save as well as up load?
        if (save && (obj.isSettings())) {

            // Again we get three tries to save
            for (int i = 0; i < 3; ++i) {
                if (DEBUG)
                    Log.d(TAG,
                            "Save try number " + i + " Object "
                                    + obj.getName());

                // Listen for the UAVObject utility manager signalling save complete, this will count down
                // our latch again.
                mUAVObjectUtilManager.addEventListener(mUAVObjectUtilManagerEventListener);

                // Reset our countdown latch
                mTransactionTimeoutLatch = new CountDownLatch(1);

                // Submit our save to the thread pool
                mSmartSaveExecutor.submit(new Runnable() {

                    @Override
                    public void run() {
                        mUAVObjectUtilManager.saveObjectToSD(obj);
                    }
                });

                // Wait for our latch to countdown to zero (initialized to one)
                try {
                    if (mTransactionTimeoutLatch.await(UPDATE_SAVE_TIMEOUT, TimeUnit.MILLISECONDS)) {
                        if (DEBUG)
                            Log.d(TAG, "Saving did not timeout" + i
                                    + " Object " + obj.getName());
                    } else if (DEBUG)
                        Log.d(TAG,
                                "Saving TIMEOUT " + i + " Object "
                                        + obj.getName());
                } catch (InterruptedException e) {
                    Log.d(TAG,
                            "Saving thread interrupted " + i + " Object "
                                    + obj.getName());
                }

                // Stop listening to prevent triggering outside of timeout
                mUAVObjectUtilManager.removeEventListener(mUAVObjectUtilManagerEventListener);

                // If we successfully save, stop trying
                if (mSaveResult)
                    break;
            }
            if (!mSaveResult) {

                // Run out of retries, give up and go to next object
                if (DEBUG)
                    Log.d(TAG, "failed to save: " + obj.getName());
                succeeded = false;
            }
        }
        return succeeded;
    }

    private void firePreProcessOperations() {
        for (SmartSaveEventListener curListener : mSmartSaveEventListeners) {
            curListener.preProcessOperations();
        }
    }

    private void fireSaveSuccessful() {
        for (SmartSaveEventListener curListener : mSmartSaveEventListeners) {
            curListener.saveSuccessful();
        }
    }

    private void fireBeginOp() {
        for (SmartSaveEventListener curListener : mSmartSaveEventListeners) {
            curListener.beginOp();
        }
    }

    private void fireEndOp() {
        for (SmartSaveEventListener curListener : mSmartSaveEventListeners) {
            curListener.endOp();
        }
    }

    enum ButtonTypeEnum {
        SAVE_BUTTON, APPLY_BUTTON
    }

    private class ButtonHandler implements OnClickListener {

        @Override
        public void onClick(View v) {
            fireBeginOp();

            if (v instanceof Button) {
                final Button button = (Button) v;
                mSmartSaveExecutor.submit(new Runnable() {

                    @Override
                    public void run() {
                        processOperation(button, mButtonList.get(button) == ButtonTypeEnum.SAVE_BUTTON);
                    }
                });
            }
        }
    }

}
