/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.io;

import static org.opng.mobilegcs.io.IAP.Status;

public class IAPEventListenerAdapter implements IAPEventListener {
    @Override
    public void progressUpdated(int progress) {
    }

    @Override
    public void downloadFinished(Status status) {
    }

    @Override
    public void uploadFinished(Status status, byte[] description) {
    }

    @Override
    public void operationProgress(String status) {
    }
}
