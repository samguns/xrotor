/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

import android.util.Log;

import org.opng.mobilegcs.telemetry.OPTelemetryService;
import org.opng.mobilegcs.util.Variant;
import org.opng.uavtalk.uavobjects.UAVObjectsInitialize;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class TelemetryMonitor {
    private static final String TAG = "TelemetryMonitor";
    private static final int LOGLEVEL = 2;
    public static final boolean WARN = LOGLEVEL > 1;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean ERROR = LOGLEVEL > 0;
    private static final int STATS_UPDATE_PERIOD_MS = 4000;
    private static final int STATS_CONNECT_PERIOD_MS = 2000;
    private static final int CONNECTION_TIMEOUT_MS = 8000;
    private final Set<TelemetryMonitorListener> mTelemetryMonitorListeners = new CopyOnWriteArraySet<>();
    private final UAVObject mFirmwareIapObj;
    //! Internal buffer
    private final ConcurrentLinkedQueue<UAVObject> mUAVObjects = new ConcurrentLinkedQueue<>();
    private UAVObjectManager mUAVObjectManager = null;
    private Telemetry mTelemetry = null;
    // private UAVObject objPending;
    private UAVObject mGcsStatsObj;
    private UAVObject mFlightStatsObj;
    private ScheduledThreadPoolExecutor mPeriodicTask;
    private long mLastUpdateTime;
    private OPTelemetryService mOPTelemetryService;
    private boolean mConnected = false;
    private boolean mObjectsUpdated = false;
    private UAVObject mObjPending = null;
    private long mLastStatsTime;
    private FirmwareIAPUpdatedHandler mFirmwareIAPUpdatedHandler;
    private final UAVObjectEventAdapter mTransactionListener = new UAVObjectEventAdapter() {

        @Override
        public void transactionCompleted(UAVObject obj, boolean success) {
            TelemetryMonitor.this.transactionCompleted(obj, success);
        }

    };

    public TelemetryMonitor(UAVObjectManager objMngr, Telemetry tel,
                            OPTelemetryService s) {
        this(objMngr, tel);
        mOPTelemetryService = s;
    }

    private TelemetryMonitor(UAVObjectManager objMngr, Telemetry tel) {
        this.mUAVObjectManager = objMngr;
        this.mTelemetry = tel;

        // Get stats objects
        mGcsStatsObj = objMngr.getObject("GCSTelemetryStats");
        mFlightStatsObj = objMngr.getObject("FlightTelemetryStats");
        mFirmwareIapObj = objMngr.getObject("FirmwareIAPObj");

        UAVObjectsInitialize.register(objMngr);

        mFlightStatsObj.addUAVObjectEventListener(new UAVObjectEventAdapter() {

            @Override
            public void objectUpdated(UAVObject obj) {
                try {
                    flightStatsUpdated();
                } catch (Exception e) {
                    // The UAVTalk stream was broken, disconnect this signal
                    // TODO: Should this actually be disconnected. Do we create
                    // a new TelemetryMonitor for this
                    // or fix the stream?
                    mFlightStatsObj.removeUAVObjectEventListener(this);
                }
            }

        });

        // Start update timer
        setPeriod(STATS_CONNECT_PERIOD_MS);
    }

    public boolean getConnected() {
        return mConnected;
    }

    public boolean getObjectsUpdated() {
        return mObjectsUpdated;
    }

    /**
     * Initiate object retrieval, initialize queue with objects to be retrieved.
     */
    private synchronized void startRetrievingObjects() {
        if (DEBUG)
            Log.d(TAG, "Start retrieving objects");

        // Clear object queue
        mUAVObjects.clear();
        // Get all objects, add metaobjects, settings and data objects with
        // OnChange update mode to the queue
        List<List<UAVObject>> objs = mUAVObjectManager.getObjects();

        for (List<UAVObject> instList : objs) {
            UAVObject obj = instList.get(0);
            UAVObject.Metadata mdata = obj.getMetadata();
            if (obj.isMetadata()) {
                mUAVObjects.add(obj);

            } else /* Data object */ {
                UAVDataObject dobj = (UAVDataObject) obj;
                if (dobj.isSettings()) {
                    mUAVObjects.add(obj);
                } else {
                    if (mdata.GetFlightTelemetryUpdateMode() == UAVObject.UpdateMode.UPDATEMODE_ONCHANGE) {
                        mUAVObjects.add(obj);
                    }
                }
            }
        }
        // Start retrieving
        Log.d(TAG,
                "Starting to retrieve meta and settings objects from the autopilot ("
                        + mUAVObjects.size() + " objects)");
        retrieveNextObject();
    }

    /**
     * Cancel the object retrieval
     */
    private void stopRetrievingObjects() {
        Log.d(TAG, "Stop retrieving objects");
        mUAVObjects.clear();
    }

    /**
     * Retrieve the next object in the queue
     */
    private synchronized void retrieveNextObject() {
        // If queue is empty return
        if (mUAVObjects.isEmpty()) {
            if (DEBUG) Log.d(TAG, "Object queue empty");
            // Implement fix from OP1080
            if (mUAVObjectManager.getObjectUtilManager().getBoardModel() != 0) {
                if (mOPTelemetryService != null)
                    mOPTelemetryService.toastMessage("Connected");
                if (DEBUG) Log.d(TAG, "All objects retrieved: Connected Successfully");
                mObjectsUpdated = true;
                fireConnectionStateChanged(true);
            } else {
                if (mFirmwareIAPUpdatedHandler == null) {
                    mFirmwareIAPUpdatedHandler = new FirmwareIAPUpdatedHandler();
                }
                mFirmwareIapObj.addUAVObjectEventListener(mFirmwareIAPUpdatedHandler);
            }
            return;
        }
        // Get next object from the queue
        UAVObject obj = mUAVObjects.poll();


        if (obj == null) {
            throw new Error("Got null object from transaction queue");
        }

        //if (DEBUG)
        Log.d(TAG, "Retrieving object: " + obj.getName());

        // TODO: Does this need to stay here permanently? This appears to be
        // used for setup mainly
        obj.addUAVObjectEventListener(mTransactionListener);

        // Request update
        obj.fireUpdateRequested();
        mObjPending = obj;
    }

    /**
     * Called by the retrieved object when a transaction is completed.
     */
    private synchronized void transactionCompleted(UAVObject obj, boolean success) {
        if (mObjPending == obj) {
            if (DEBUG)
                Log.d(TAG, "transactionCompleted.  Status: " + success);

            // Remove the listener for the event that just finished
            obj.removeUAVObjectEventListener(mTransactionListener);
            mObjPending = null;

            if (!success) {
                // Right now success = false means received a NAK so don't
                // re-attempt
                if (ERROR) Log.e(TAG, "Transaction failed.");
            }

            // Process next object if telemetry is still available
            if (mGcsStatsObj.getField("Status").getValue().toString().compareTo("Connected") == 0) {
                retrieveNextObject();
            } else {
                stopRetrievingObjects();
            }
        }
    }

    /**
     * Called each time the flight stats object is updated by the autopilot.
     */
    private synchronized void flightStatsUpdated() {
        // Force update if not yet connected
        mGcsStatsObj = mUAVObjectManager.getObject("GCSTelemetryStats");
        mFlightStatsObj = mUAVObjectManager.getObject("FlightTelemetryStats");
        if (DEBUG)
            Log.d(TAG, "GCS Status: "
                    + mGcsStatsObj.getField("Status").getValue());
        if (DEBUG)
            Log.d(TAG, "Flight Status: "
                    + mFlightStatsObj.getField("Status").getValue());
        if (mGcsStatsObj.getField("Status").getValue().toString()
                .compareTo("Connected") != 0
                || mFlightStatsObj.getField("Status").getValue().toString()
                .compareTo("Connected") != 0) {
            if (DEBUG) Log.d(TAG, "Autopilot connected process stats update: start");
            processStatsUpdates();
            if (DEBUG) Log.d(TAG, "Autopilot connected process stats update: end");
        }
    }

    private synchronized void firmwareIAPUpdated() {
        if (mUAVObjectManager.getObjectUtilManager().getBoardModel() != 0) {
            // Done with this handler for now
            mFirmwareIapObj.removeUAVObjectEventListener(mFirmwareIAPUpdatedHandler);
            if (mOPTelemetryService != null)
                mOPTelemetryService.toastMessage("Connected");
            if (DEBUG) Log.d(TAG, "All objects retrieved: Connected Successfully");
            mObjectsUpdated = true;
            fireConnectionStateChanged(true);
        }
    }

    /**
     * Called periodically to update the statistics and connection status.
     */
    private synchronized void processStatsUpdates() {
        // Get telemetry stats
        if (DEBUG)
            Log.d(TAG, "processStatsUpdates()");
        Telemetry.TelemetryStats telStats = mTelemetry.getStats();

        if (DEBUG)
            Log.d(TAG, "processStatsUpdates() - stats reset");
        mTelemetry.resetStats();

        // Need to compute time because this update is not regular enough
        float dT = (System.currentTimeMillis() - mLastStatsTime) / 1000.0f;
        mLastStatsTime = System.currentTimeMillis();

        // Update stats object
        mGcsStatsObj.getField("TxDataRate").setDouble(telStats.txBytes / dT);
        UAVObjectField field = mGcsStatsObj.getField("TxFailures");
        field.setDouble(field.getDouble() + telStats.txErrors);
        field = mGcsStatsObj.getField("TxRetries");
        field.setDouble(field.getDouble() + telStats.txRetries);
        field = mGcsStatsObj.getField("TxBytes");
        field.setDouble(field.getDouble() + telStats.txBytes);

        mGcsStatsObj.getField("RxDataRate").setDouble(telStats.rxBytes / dT);
        field = mGcsStatsObj.getField("RxFailures");
        field.setDouble(field.getDouble() + telStats.rxErrors);
        field = mGcsStatsObj.getField("RxBytes");
        field.setDouble(field.getDouble() + telStats.rxBytes);
        field = mGcsStatsObj.getField("RxSyncErrors");
        field.setDouble(field.getDouble() + telStats.rxSyncErrors);
        field = mGcsStatsObj.getField("RxCrcErrors");
        field.setDouble(field.getDouble() + telStats.rxCrcErrors);

        // Check for a connection timeout
        boolean connectionTimeout;
        if (telStats.rxObjects > 0) {
            mLastUpdateTime = System.currentTimeMillis();

        }
        connectionTimeout = (System.currentTimeMillis() - mLastUpdateTime) > CONNECTION_TIMEOUT_MS;

        // Update connection state
        mGcsStatsObj = mUAVObjectManager.getObject("GCSTelemetryStats");
        mFlightStatsObj = mUAVObjectManager.getObject("FlightTelemetryStats");
        if (mGcsStatsObj == null) {
            Log.d(TAG, "No GCS stats yet");
            return;
        }
        UAVObjectField statusField = mGcsStatsObj.getField("Status");
        String oldStatus = statusField.getValue().toString();

        if (DEBUG)
            Log.d(TAG, "GCS: " + statusField.getValue() + " Flight: "
                    + mFlightStatsObj.getField("Status").getValue());

        if (oldStatus.compareTo("Disconnected") == 0) {
            // Request connection
            statusField.setValue(new Variant("HandshakeReq"));
        } else if (oldStatus.compareTo("HandshakeReq") == 0) {
            // Check for connection acknowledge
            if (mFlightStatsObj.getField("Status").getValue().toString()
                    .compareTo("HandshakeAck") == 0) {
                statusField.setValue(new Variant("Connected"));
                if (DEBUG)
                    Log.d(TAG, "Connected" + statusField.toString());
            }
        } else if (oldStatus.compareTo("Connected") == 0) {
            // Check if the connection is still active and the the autopilot is
            // still connected
            if (mFlightStatsObj.getField("Status").getValue().toString()
                    .compareTo("Disconnected") == 0 || connectionTimeout) {
                statusField.setValue(new Variant("Disconnected"));
            }
        }

        // Force telemetry update if not yet connected
        boolean gcsStatusChanged = !oldStatus.equals(statusField.getValue().toString());

        boolean gcsConnected = statusField.getValue().toString().equals("Connected");
        boolean gcsDisconnected = statusField.getValue().toString().equals("Disconnected");
        boolean flightConnected = mFlightStatsObj.getField("Status").toString().equals(
                "Connected");

        if (!gcsConnected || !flightConnected) {
            if (DEBUG)
                Log.d(TAG, "Sending gcs status");
            mGcsStatsObj.updated();
        }

        // Act on new connections or disconnections
        if (gcsConnected && gcsStatusChanged) {
            if (DEBUG)
                Log.d(TAG, "Connection with the autopilot established");
            setPeriod(STATS_UPDATE_PERIOD_MS);
            mConnected = true;
            mObjectsUpdated = false;
            fireConnectionStateChanged(true);
            startRetrievingObjects();
        }
        if (gcsDisconnected && gcsStatusChanged) {
            if (DEBUG)
                Log.d(TAG, "Trying to connect to the autopilot");
            setPeriod(STATS_CONNECT_PERIOD_MS);
            mConnected = false;
            mObjectsUpdated = false;
            fireConnectionStateChanged(false);
        }
    }

    private void setPeriod(int ms) {
        if (mPeriodicTask != null) {
            mPeriodicTask.shutdown();
        }

        mPeriodicTask = new ScheduledThreadPoolExecutor(4);
        mPeriodicTask.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                if (DEBUG) Log.d(TAG, "Scheduled process stats update: start");
                processStatsUpdates();
                if (DEBUG) Log.d(TAG, "Scheduled process stats update: end");
            }
        }, ms, ms, TimeUnit.MILLISECONDS);
    }

    public void stopMonitor() {
        if (mPeriodicTask != null) {
            mPeriodicTask.shutdownNow();
            mPeriodicTask = null;
        }
    }

    public void addListener(TelemetryMonitorListener listener) {
        mTelemetryMonitorListeners.add(listener);
    }

    public void removeListener(TelemetryMonitorListener listener) {
        mTelemetryMonitorListeners.remove(listener);
    }

    private void fireConnectionStateChanged(boolean connected) {
        for (TelemetryMonitorListener curListener : mTelemetryMonitorListeners) {
            curListener.connectionStateChanged(connected);
        }
    }

    public interface TelemetryMonitorListener {
        void connectionStateChanged(boolean connected);
    }

    private final class FirmwareIAPUpdatedHandler extends UAVObjectEventAdapter {
        @Override
        public void objectUpdated(UAVObject obj) {
            firmwareIAPUpdated();
        }
    }

}
