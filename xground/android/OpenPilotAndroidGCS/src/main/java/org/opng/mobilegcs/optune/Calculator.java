/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.optune;

import android.util.Log;

class Calculator {
    private final static String TAG = Calculator.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final OPTuneFactors mOPTuneFactors;
    private double mPitchKp;
    private double mPitchKi;
    private double mPitchKd;
    private double mRollKp;
    private double mRollKi;
    private double mRollKd;
    private double mYawKp;
    private double mYawKi;
    private double mYawKd;

    public Calculator(OPTuneFactors opTuneFactors) {
        mOPTuneFactors = opTuneFactors;
    }

    public double getPitchKp() {
        return mPitchKp;
    }

    public double getPitchKi() {
        return mPitchKi;
    }

    public double getPitchKd() {
        return mPitchKd;
    }

    public double getRollKp() {
        return mRollKp;
    }

    public double getRollKi() {
        return mRollKi;
    }

    public double getRollKd() {
        return mRollKd;
    }

    public double getYawKp() {
        return mYawKp;
    }

    public double getYawKi() {
        return mYawKi;
    }

    public double getYawKd() {
        return mYawKd;
    }

    public void calculate(float pitchUov, float rollUov) {
        mOPTuneFactors.determineSelectedFactors();

        // Calculate roll P, I & D
        mRollKp = rollUov * mOPTuneFactors.getProportionalFactor();
        mRollKi = mRollKp * mOPTuneFactors.getIntegralFactor();
        mRollKd = mRollKp * mOPTuneFactors.getDerivativeFactor();

        // Calculate pitch P, I & D
        mPitchKp = pitchUov * mOPTuneFactors.getProportionalFactor();
        mPitchKi = mPitchKp * mOPTuneFactors.getIntegralFactor();
        mPitchKd = mPitchKp * mOPTuneFactors.getDerivativeFactor();

        // Calculate yaw P, I & D
        mYawKp = ((mRollKp + mPitchKp) / 2) * mOPTuneFactors.getYawProportionalFactor();
        mYawKi = mYawKp * mOPTuneFactors.getYawIntegralFactor();
        mYawKd = mYawKp * mOPTuneFactors.getYawDerivativeFactor();

        dumpValues();
    }

    public void calculate(float pitchUov, float rollUov, float yawUov) {

        // Calculate roll P, I & D
        mRollKp = rollUov * mOPTuneFactors.getProportionalFactor();
        mRollKi = mRollKp * mOPTuneFactors.getIntegralFactor();
        mRollKd = mRollKp * mOPTuneFactors.getDerivativeFactor();

        // Calculate pitch P, I & D
        mPitchKp = pitchUov * mOPTuneFactors.getProportionalFactor();
        mPitchKi = mPitchKp * mOPTuneFactors.getIntegralFactor();
        mPitchKd = mPitchKp * mOPTuneFactors.getDerivativeFactor();

        // Calculate yaw P, I & D
        mYawKp = yawUov * mOPTuneFactors.getYawProportionalFactor();
        mYawKi = mYawKp * mOPTuneFactors.getYawIntegralFactor();
        mYawKd = mYawKp * mOPTuneFactors.getYawDerivativeFactor();

        dumpValues();
    }

    private void dumpValues() {
        if (DEBUG) {
            Log.d(TAG, String.format("Roll PID: %f, %f, %f", mRollKp, mRollKi, mRollKd));
            Log.d(TAG, String.format("Pitch PID: %f, %f, %f", mPitchKp, mPitchKi, mPitchKd));
            Log.d(TAG, String.format("Yaw PID: %f, %f, %f", mYawKp, mYawKi, mYawKd));
        }
    }
}
