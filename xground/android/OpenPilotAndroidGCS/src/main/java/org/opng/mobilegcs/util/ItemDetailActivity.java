/*
 * Eric Henaff Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;

import org.opng.mobilegcs.R;

public class ItemDetailActivity extends ConfigBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_detail);
        setTitle(mMenu.mTitle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        ((BaseFragmentAppMenuItem) mMenu).Open(fragmentManager, R.id.container);
    }
}
