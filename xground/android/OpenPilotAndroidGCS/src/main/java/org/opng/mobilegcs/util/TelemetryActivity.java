/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.WindowManager;

import org.opng.mobilegcs.preferences.AndroidGCSPrefs;
import org.opng.uavtalk.UAVObjectManager;

/**
 * Extend this class to create an Activity that has various predefined widgets in its
 * Action Bar. The doConnect and doDisconnect methods can be called from menu items
 * to connect and disconnect telemetry.
 * <p/>
 * This class is basically a wrapper around ActivityTelemetryConnectionHandler that
 * simplifies its use for Activity classes that don't need to extend another Activity
 * derived class. If you need to extend a different Activity class, use
 * ActivityTelemetryConnectionHandler directly. This class handles various Activity
 * lifecycle callbacks to control the lifetime and state of the telemetry widget.
 */
@SuppressLint("Registered")
public class TelemetryActivity extends AppCompatActivity {

    private final ActivityTelemetryConnectionHandler mTelemetryConnectionHandler = new ActivityTelemetryConnectionHandler();
    private int mActionProvidersToShow;

    /**
     * Handle to the app preferences.
     */
    private AndroidGCSPrefs mAppPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mTelemetryConnectionHandler.restoreState(savedInstanceState);
        } else {
            // New instance of this class, add connection listener
            mTelemetryConnectionHandler.setUserTelemetryServiceHandlerListener(new TelemetryServiceHandler.ConnectionListener() {
                @Override
                public void onConnect(UAVObjectManager objectManager) {
                    onTelemetryConnected();
                }

                @Override
                public void onDisconnect() {
                    onTelemetryDisconnected();
                }
            });
        }

        mAppPrefs = new AndroidGCSPrefs(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mTelemetryConnectionHandler.onActivityResume(this);

        // Set keep screen on if set in prefs.
        if (mAppPrefs.keepScreenOn()) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        } else {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mTelemetryConnectionHandler.onActivityStop(this);

        // Reset keep screen on if set in prefs.
        if (mAppPrefs.keepScreenOn()) {
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mTelemetryConnectionHandler.onActivityDestroy(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mTelemetryConnectionHandler.onActivitySaveInstanceState(outState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mTelemetryConnectionHandler.addActionProviderWidgets(menu, this, mActionProvidersToShow);
        return true;
    }

    protected boolean isConnected() {
        return mTelemetryConnectionHandler.isTelemetryTaskConnected(this) ||
                mTelemetryConnectionHandler.isTelemetryConnected(this);
    }

    /**
     * This method is used to start the telemetry service if required and connect to
     * the flight controller.
     * <p/>
     * This method is mainly intended to be associated with a menu item.
     */
    protected void doConnect() {
        mTelemetryConnectionHandler.doConnect(this);
    }

    /**
     * This method is used to disconnect from the flight controller.
     * <p/>
     * This method is mainly intended to be associated with a menu item.
     */
    protected void doDisconnect() {
        mTelemetryConnectionHandler.doDisconnect(this);
    }

    protected void doStartLogging() {
        mTelemetryConnectionHandler.startLogging(this);
    }

    protected void doStopLogging() {
        mTelemetryConnectionHandler.stopLogging(this);
    }

    protected void setActionProvidersToShow(int actionProvidersToShow) {
        mActionProvidersToShow = actionProvidersToShow;
    }

    /**
     * Override to be notified of telemetry connection events.
     */
    protected void onTelemetryConnected() {
    }

    /**
     * Override to be notified of telemetry disconnection events.
     */
    protected void onTelemetryDisconnected() {
    }
}
