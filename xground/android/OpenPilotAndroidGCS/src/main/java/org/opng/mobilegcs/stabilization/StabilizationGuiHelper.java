/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.stabilization;

import android.util.Log;
import android.view.View;

import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.mobilegcs.util.UAVObjectToGuiUtilsEventListener;
import org.opng.mobilegcs.util.Variant;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.AltitudeHoldSettings;
import org.opng.uavtalk.uavobjects.AttitudeSettings;
import org.opng.uavtalk.uavobjects.StabilizationSettings;
import org.opng.uavtalk.uavobjects.StabilizationSettingsBank1;

import java.util.ArrayList;
import java.util.List;

public class StabilizationGuiHelper {
    private final static String TAG = StabilizationGuiHelper.class.getSimpleName();
    private static final int LOGLEVEL = 0;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final int BOARD_NUM_REVO = 0x0900;
    private static boolean WARN = LOGLEVEL > 0;
    private final UAVObjectToGuiUtils _objectToGui;
    private final List<UAVObject> _stabiSettingsBanks = new ArrayList<>();
    private int _bankNumber = 0; // Bank 1
    private List<Integer> mButtonGroup = null;
    private int _idxRateP;
    private int _idxRateI;
    private int _idxRateD;
    private int _idxAttP;
    private int _idxAttI;
    private View _rollKpWidget;
    private View _rollKiWidget;
    private View _rollKdWidget;
    private View _pitchKpWidget;
    private View _pitchKiWidget;
    private View _pitchKdWidget;
    private View _yawKpWidget;
    private View _yawKiWidget;
    private View _yawKdWidget;
    private UAVObject _stabiSettings;
    private UAVObject _attitudeSettings;
    private UAVObject _altitudeHoldSettings;

    public StabilizationGuiHelper(UAVObjectManager uavObjectManager, UAVObjectToGuiUtils objectToGuiUtils, int buttonGroup) {
        _objectToGui = objectToGuiUtils;
        mButtonGroup = new ArrayList<>();
        mButtonGroup.add(buttonGroup);

        init(uavObjectManager);
    }

    public StabilizationGuiHelper(UAVObjectManager uavObjectManager, UAVObjectToGuiUtils objectToGuiUtils) {
        this(uavObjectManager, objectToGuiUtils, 0);
    }

    public void setBankNumber(int bankNumber) {
        if (bankNumber < 1 || bankNumber > 3) {
            throw new IndexOutOfBoundsException("Bank must be between 1 and 3 inclusive");
        }

        // _bankNumber is zero based but for GUI simplicity, bankNumber passed in
        // is one based.
        _bankNumber = bankNumber - 1;

        configureBankSaving();
        if (DEBUG) Log.d(TAG, "Refreshing stabilization bank " + bankNumber);
        _stabiSettingsBanks.get(_bankNumber).requestUpdate();
    }

    public float getPitchRateKp() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_PITCHRATEPID).getValue(_idxRateP).toFloat();
    }

    public float getPitchRateKi() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_PITCHRATEPID).getValue(_idxRateI).toFloat();
    }

    public float getPitchRateKd() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_PITCHRATEPID).getValue(_idxRateD).toFloat();
    }

    public float getRollRateKp() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_ROLLRATEPID).getValue(_idxRateP).toFloat();
    }

    public float getRollRateKi() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_ROLLRATEPID).getValue(_idxRateI).toFloat();
    }

    public float getRollRateKd() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_ROLLRATEPID).getValue(_idxRateD).toFloat();
    }

    public float getYawRateKp() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_YAWRATEPID).getValue(_idxRateP).toFloat();
    }

    public float getYawRateKi() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_YAWRATEPID).getValue(_idxRateI).toFloat();
    }

    public float getYawRateKd() {
        return _stabiSettingsBanks.get(_bankNumber).getField(StabilizationSettingsBank1.FIELD_YAWRATEPID).getValue(_idxRateD).toFloat();
    }

    public void setRollRatePID(float p, float i, float d){
        setPid(StabilizationSettingsBank1.FIELD_ROLLRATEPID, p, i, d);
    }

    public void setPitchRatePID(float p, float i, float d){
        setPid(StabilizationSettingsBank1.FIELD_PITCHRATEPID, p, i, d);
    }

    public void setYawRatePID(float p, float i, float d){
        setPid(StabilizationSettingsBank1.FIELD_YAWRATEPID, p, i, d);
    }

    public void refresh() {
        if (_stabiSettings != null) {
            _stabiSettings.requestUpdate();
        }
        if (_attitudeSettings != null) {
            _attitudeSettings.requestUpdate();
        }
        if (_altitudeHoldSettings != null) {
            _altitudeHoldSettings.requestUpdate();
        }

        UAVObject stabiSettingsBank = _stabiSettingsBanks.get(_bankNumber);
        if (stabiSettingsBank != null) {
            stabiSettingsBank.requestUpdate();
        }
    }

    public void addObjectToGuiEventListener(UAVObjectToGuiUtilsEventListener listener) {
        if (_objectToGui != null) {
            _objectToGui.addEventListener(listener);
        }
    }

    public void bindRollRatePidWidgets(View viewP, double scaleP, View viewI, double scaleI, View viewD, double scaleD) {
        bindPidWidgets(viewP, scaleP, viewI, scaleI, viewD, scaleD, StabilizationSettingsBank1.FIELD_ROLLRATEPID);
        _rollKpWidget = viewP;
        _rollKiWidget = viewI;
        _rollKdWidget = viewD;
    }

    public void bindPitchRatePidWidgets(View viewP, double scaleP, View viewI, double scaleI, View viewD, double scaleD) {
        bindPidWidgets(viewP, scaleP, viewI, scaleI, viewD, scaleD, StabilizationSettingsBank1.FIELD_PITCHRATEPID);
        _pitchKpWidget = viewP;
        _pitchKiWidget = viewI;
        _pitchKdWidget = viewD;
    }

    public void bindYawRatePidWidgets(View viewP, double scaleP, View viewI, double scaleI, View viewD, double scaleD) {
        bindPidWidgets(viewP, scaleP, viewI, scaleI, viewD, scaleD, StabilizationSettingsBank1.FIELD_YAWRATEPID);
        _yawKpWidget = viewP;
        _yawKiWidget = viewI;
        _yawKdWidget = viewD;
    }

    public void bindRollAttitudePiWidgets(View viewP, double scaleP, View viewI, double scaleI){
        bindPiWidgets(viewP, scaleP, viewI, scaleI, StabilizationSettingsBank1.FIELD_ROLLPI);
    }

    public void bindPitchAttitudePiWidgets(View viewP, double scaleP, View viewI, double scaleI){
        bindPiWidgets(viewP, scaleP, viewI, scaleI, StabilizationSettingsBank1.FIELD_PITCHPI);
    }

    public void bindYawAttitudePiWidgets(View viewP, double scaleP, View viewI, double scaleI){
        bindPiWidgets(viewP, scaleP, viewI, scaleI, StabilizationSettingsBank1.FIELD_YAWPI);
    }

    public void bindRollResponsivenessWidgets(View viewAttitude, View viewRate, View viewMaxRate){
        bindResponsivenessWidgets(viewAttitude, viewRate, viewMaxRate, StabilizationSettingsBank1.FIELD_ROLLMAX, StabilizationSettingsBank1.MANUALRATE_ROLL);
    }

    public void bindPitchResponsivenessWidgets(View viewAttitude, View viewRate, View viewMaxRate){
        bindResponsivenessWidgets(viewAttitude, viewRate, viewMaxRate, StabilizationSettingsBank1.FIELD_PITCHMAX, StabilizationSettingsBank1.MANUALRATE_PITCH);
    }

    public void bindYawResponsivenessWidgets(View viewAttitude, View viewRate, View viewMaxRate){
        bindResponsivenessWidgets(viewAttitude, viewRate, viewMaxRate, StabilizationSettingsBank1.FIELD_YAWMAX, StabilizationSettingsBank1.MANUALRATE_YAW);
    }

    public void bindWeakLevelingWidgets(View viewGain, View viewRate) {
        if (viewGain != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_WEAKLEVELINGKP),
                    viewGain, 0, 1, false, mButtonGroup, 0);
        }
        if (viewRate != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_MAXWEAKLEVELINGRATE),
                    viewRate, 0, 1, false, mButtonGroup, 0);
        }
    }

    public void bindAxisLockWidgets(View viewMaxAxisLock, View viewMaxAxisLockRate) {
        if (viewMaxAxisLock != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_MAXAXISLOCK),
                    viewMaxAxisLock, 0, 1, false, mButtonGroup, 0);
        }
        if (viewMaxAxisLockRate != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_MAXAXISLOCKRATE),
                    viewMaxAxisLockRate, 0, 1, false, mButtonGroup, 0);
        }
    }

    public void bindRattitudeWidgets(View viewModeTransition) {
        if (viewModeTransition != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_RATTITUDEMODETRANSITION),
                    viewModeTransition, 0, 1, false, mButtonGroup, 0);
        }
    }

    public void bindCruiseControlWidgets(View viewMaxPwrFactor, View viewMaxAngle, View viewMinThrust, View viewInvtdThrustRev,
                                         View viewPwrDelayComp, View viewPwrTrim, View viewMaxThrust, View viewInvtdPwr) {
        if (viewMaxPwrFactor != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLMAXPOWERFACTOR),
                    viewMaxPwrFactor, 0, 1, false, mButtonGroup, 0);
        }
        if (viewMaxAngle != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLMAXANGLE),
                    viewMaxAngle, 0, 1, false, mButtonGroup, 0);
        }
        if (viewMinThrust != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLMINTHRUST),
                    viewMinThrust, 0, 1, false, mButtonGroup, 0);
        }
        if (viewInvtdThrustRev != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLINVERTEDTHRUSTREVERSING),
                    viewInvtdThrustRev, 0, 1, false, mButtonGroup, 0);
        }
        if (viewPwrDelayComp != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLPOWERDELAYCOMP),
                    viewPwrDelayComp, 0, 1, false, mButtonGroup, 0);
        }
        if (viewPwrTrim != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLPOWERTRIM),
                    viewPwrTrim, 0, 1, false, mButtonGroup, 0);
        }
        if (viewMaxThrust != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLMAXTHRUST),
                    viewMaxThrust, 0, 1, false, mButtonGroup, 0);
        }
        if (viewInvtdPwr != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_CRUISECONTROLINVERTEDPOWEROUTPUT),
                    viewInvtdPwr, 0, 1, false, mButtonGroup, 0);
        }
    }

    public void bindSensorTuningWidgets(View viewGyroTau, View viewAccelP, View viewAccelI) {
        if (viewGyroTau != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_GYROTAU),
                    viewGyroTau, 0, 1, false, mButtonGroup, 0);
        }
        if (viewAccelP != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_attitudeSettings, _attitudeSettings.getField(AttitudeSettings.FIELD_ACCELKP),
                    viewAccelP, 0, 1, false, mButtonGroup, 0);
        }
        if (viewAccelI != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_attitudeSettings, _attitudeSettings.getField(AttitudeSettings.FIELD_ACCELKI),
                    viewAccelI, 0, 1, false, mButtonGroup, 0);
        }

    }

    public void bindAltHoldTuningWidgets(View viewAltitudeKp, double scaleAltKp, View viewVelocityKp,
                                         double scaleVelKp, View viewVelocityKi, double scaleVelKi) {
        if ((_objectToGui.getCurrentBoard() & 0xff00) == BOARD_NUM_REVO) {
            // Only bind these widgets if we are connected to a revo
            UAVObjectField velocityPIField = _altitudeHoldSettings.getField(AltitudeHoldSettings.FIELD_VERTICALVELPID);
            if (viewAltitudeKp != null) {
                UAVObjectField altitudePIField = _altitudeHoldSettings.getField("AltitudePI");
                _objectToGui.addUAVObjectToWidgetRelation(_altitudeHoldSettings, altitudePIField,
                        viewAltitudeKp, altitudePIField.getElementNames().indexOf("Kp"), scaleAltKp, false, mButtonGroup, 0);
            }
            if (viewVelocityKp != null) {
                _objectToGui.addUAVObjectToWidgetRelation(_altitudeHoldSettings, velocityPIField,
                        viewVelocityKp, velocityPIField.getElementNames().indexOf(AltitudeHoldSettings.VERTICALVELPID_KP), scaleVelKp, false, mButtonGroup, 0);
            }
            if (viewVelocityKi != null) {
                _objectToGui.addUAVObjectToWidgetRelation(_altitudeHoldSettings, velocityPIField,
                        viewVelocityKi, velocityPIField.getElementNames().indexOf(AltitudeHoldSettings.VERTICALVELPID_KI), scaleVelKi, false, mButtonGroup, 0);
            }
        } else {
            // Disable widgets
            if (viewAltitudeKp != null) {
                viewAltitudeKp.setEnabled(false);
            }
            if (viewVelocityKp != null) {
                viewVelocityKp.setEnabled(false);
            }
            if (viewVelocityKi != null) {
                viewVelocityKi.setEnabled(false);
            }

        }
    }

    public void bindVarioAltitudeTuningWidgets(View viewExpo, View viewMaxVelocity) {
        if ((_objectToGui.getCurrentBoard() & 0xff00) == BOARD_NUM_REVO) {
            if (viewExpo != null) {
                _objectToGui.addUAVObjectToWidgetRelation(_altitudeHoldSettings, _altitudeHoldSettings.getField(AltitudeHoldSettings.FIELD_THRUSTEXP),
                        viewExpo, 0, 1, false, mButtonGroup, 0);
            }
            if (viewMaxVelocity != null) {
                _objectToGui.addUAVObjectToWidgetRelation(_altitudeHoldSettings, _altitudeHoldSettings.getField(AltitudeHoldSettings.FIELD_THRUSTRATE),
                        viewMaxVelocity, 0, 1, false, mButtonGroup, 0);
            }
        } else {
            // Disable widgets
            if (viewExpo != null) {
                viewExpo.setEnabled(false);
            }
            if (viewMaxVelocity != null) {
                viewMaxVelocity.setEnabled(false);
            }
        }
    }

    public void bindAcroPlusWidgets(View viewFactor, double scale) {
        for(UAVObject stabiSettingsBank: _stabiSettingsBanks){
            if(viewFactor != null){
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_ACROINSANITYFACTOR),
                        viewFactor, 0, scale, false, mButtonGroup, 0);
            }
        }
    }

    public void bindTpsWidgets(View viewEnable, View viewSource, View viewTarget, View viewAxis, View viewCurve) {
        for(UAVObject stabiSettingsBank: _stabiSettingsBanks){
            if(viewEnable != null){
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_ENABLETHRUSTPIDSCALING),
                        viewEnable, 0, 1, false, mButtonGroup, 0, new String[]{StabilizationSettingsBank1.ENABLETHRUSTPIDSCALING_TRUE, StabilizationSettingsBank1.ENABLETHRUSTPIDSCALING_FALSE});
            }
            if(viewSource != null){
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_THRUSTPIDSCALESOURCE),
                        viewSource, 0, 1, false, mButtonGroup, 0);
            }
            if(viewTarget != null){
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_THRUSTPIDSCALETARGET),
                        viewTarget, 0, 1, false, mButtonGroup, 0);
            }
            if(viewAxis != null){
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_THRUSTPIDSCALEAXES),
                        viewAxis, 0, 1, false, mButtonGroup, 0);
            }
            if (viewCurve != null) {
                // Setting index to -1 here to indicate that the given view is expected to
                // be showing the value for all elements of the field. Currently this should
                // be a CurveView with one point per element in the given field.
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_THRUSTPIDSCALECURVE),
                        viewCurve, -1, 1, false, mButtonGroup, 0);
            }
        }
    }

    public void bindExpoWidgets(View viewRoll, View viewPitch, View viewYaw) {
        UAVObjectField field = _stabiSettingsBanks.get(0).getField(StabilizationSettingsBank1.FIELD_STICKEXPO);
        final List<String> elementNames = field.getElementNames();
        for(UAVObject stabiSettingsBank: _stabiSettingsBanks){
            field = stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_STICKEXPO);
            if(viewRoll != null){
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewRoll, elementNames.indexOf(StabilizationSettingsBank1.STICKEXPO_ROLL), 1, false, mButtonGroup, 0);
            }
            if (viewPitch != null) {
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewPitch, elementNames.indexOf(StabilizationSettingsBank1.STICKEXPO_PITCH), 1, false, mButtonGroup, 0);
            }
            if (viewYaw != null) {
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewYaw, elementNames.indexOf(StabilizationSettingsBank1.STICKEXPO_YAW), 1, false, mButtonGroup, 0);
            }
        }
    }

    public void bindZeroIntegralWidgets(View viewZeroIntegral) {
        if (viewZeroIntegral != null) {
            _objectToGui.addUAVObjectToWidgetRelation(_stabiSettings, _stabiSettings.getField(StabilizationSettings.FIELD_LOWTHROTTLEZEROINTEGRAL),
                    viewZeroIntegral, 0, 1, false, mButtonGroup, 0, new String[]{StabilizationSettings.LOWTHROTTLEZEROINTEGRAL_TRUE, StabilizationSettings.LOWTHROTTLEZEROINTEGRAL_FALSE});
        }
    }

    public void bindPiroComp(View viewPiroComp) {
        final UAVObjectField field = _stabiSettingsBanks.get(0).getField(StabilizationSettingsBank1.FIELD_ENABLEPIROCOMP);
        for(UAVObject stabiSettingsBank: _stabiSettingsBanks){
            if(viewPiroComp != null){
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewPiroComp, 0, 1, false, mButtonGroup, 0, new String[]{StabilizationSettingsBank1.ENABLEPIROCOMP_TRUE, StabilizationSettingsBank1.ENABLEPIROCOMP_FALSE});
            }
        }

    }

    public void displayPitchRatePID(double pitchKp, double pitchKi, double pitchKd) {
        _objectToGui.displayValueInWidget(_pitchKpWidget, new Variant(pitchKp));
        _objectToGui.displayValueInWidget(_pitchKiWidget, new Variant(pitchKi));
        _objectToGui.displayValueInWidget(_pitchKdWidget, new Variant(pitchKd));
    }

    public void displayRollRatePID(double rollKp, double rollKi, double rollKd) {
        _objectToGui.displayValueInWidget(_rollKpWidget, new Variant(rollKp));
        _objectToGui.displayValueInWidget(_rollKiWidget, new Variant(rollKi));
        _objectToGui.displayValueInWidget(_rollKdWidget, new Variant(rollKd));
    }

    public void displayYawRatePID(double yawKp, double yawKi, double yawKd) {
        _objectToGui.displayValueInWidget(_yawKpWidget, new Variant(yawKp));
        _objectToGui.displayValueInWidget(_yawKiWidget, new Variant(yawKi));
        _objectToGui.displayValueInWidget(_yawKdWidget, new Variant(yawKd));
    }

    public void saveChangesToRam() {
        UAVObject stabiSettings = _stabiSettingsBanks.get(_bankNumber);
        if (stabiSettings != null) {
            _objectToGui.saveObjectToRam((UAVDataObject) stabiSettings);
        }
    }

    public void populateWidgets() {
        if (_objectToGui != null) {
            _objectToGui.populateViews();
        }
    }

    private void init(UAVObjectManager uavObjectManager) {
        // Set up list of uav objects for settings banks
        _stabiSettingsBanks.add(uavObjectManager.getObject("StabilizationSettingsBank1"));
        _stabiSettingsBanks.add(uavObjectManager.getObject("StabilizationSettingsBank2"));
        _stabiSettingsBanks.add(uavObjectManager.getObject("StabilizationSettingsBank3"));

        // Get stabilization settings object
        _stabiSettings = uavObjectManager.getObject("StabilizationSettings");

        // Get attitude settings object
        _attitudeSettings = uavObjectManager.getObject("AttitudeSettings");

        // Get altitude hold settings object
        _altitudeHoldSettings = uavObjectManager.getObject("AltitudeHoldSettings");

        // Using roll rate PID field to get indices of p, i & d. Assume that these
        // elements are at the same indices for all PID fields.
        List<String> pidElementNames = _stabiSettingsBanks.get(0).getField(StabilizationSettingsBank1.FIELD_ROLLRATEPID).getElementNames();
        _idxRateP = pidElementNames.indexOf(StabilizationSettingsBank1.ROLLRATEPID_KP);
        _idxRateI = pidElementNames.indexOf(StabilizationSettingsBank1.ROLLRATEPID_KI);
        _idxRateD = pidElementNames.indexOf(StabilizationSettingsBank1.ROLLRATEPID_KD);

        // Using roll attitude PI field to get indices of p and i. Assume that these
        // elements are at the same indices for all PI fields.
        List<String> piElementNames = _stabiSettingsBanks.get(0).getField(StabilizationSettingsBank1.FIELD_ROLLPI).getElementNames();
        _idxAttP = piElementNames.indexOf(StabilizationSettingsBank1.ROLLPI_KP);
        _idxAttI = piElementNames.indexOf(StabilizationSettingsBank1.ROLLPI_KI);

        // Disable saving for all stabilisation banks bar the one we are currently
        // updating.
        configureBankSaving();
        if (DEBUG) Log.d(TAG, "Refreshing stabilization bank " + (_bankNumber + 1));
        _stabiSettingsBanks.get(_bankNumber).requestUpdate();
    }

    private void setPid(String field, float p, float i, float d) {
        UAVObjectField pidField = _stabiSettingsBanks.get(_bankNumber).getField(field);
        if (p > -1f) {
            if (DEBUG)
                Log.d(TAG, "Setting Kp for " + field + ":" + (_bankNumber + 1) + " (" + p + ")");
            pidField.setValue(new Variant(p), _idxRateP);
        }
        if (i > -1f) {
            if (DEBUG)
                Log.d(TAG, "Setting Ki for " + field + ":" + (_bankNumber + 1) + " (" + i + ")");
            pidField.setValue(new Variant(i), _idxRateI);
        }
        if (d > -1f) {
            if (DEBUG)
                Log.d(TAG, "Setting Kd for " + field + ":" + (_bankNumber + 1) + " (" + d + ")");
            pidField.setValue(new Variant(d), _idxRateD);
        }
    }

    private void bindPidWidgets(View viewP, double scaleP, View viewI, double scaleI, View viewD, double scaleD, String pidFieldName) {
        // Bind the widgets to all the settings banks. We will only ever be manipulating
        // one bank at a time so there should be no clash...
        for (UAVObject stabiSettingsBank : _stabiSettingsBanks) {
            UAVObjectField field = stabiSettingsBank.getField(pidFieldName);

            if (viewP != null) {
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewP, _idxRateP, scaleP, false, mButtonGroup, 0);
            }
            if (viewI != null) {
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewI, _idxRateI, scaleI, false, mButtonGroup, 0);
            }
            if (viewD != null) {
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewD, _idxRateD, scaleD, false, mButtonGroup, 0);
            }
        }
    }

    private void bindPiWidgets(View viewP, double scaleP, View viewI, double scaleI, String piFieldName) {
        for (UAVObject stabiSettingsBank : _stabiSettingsBanks) {
            UAVObjectField field = stabiSettingsBank.getField(piFieldName);

            if (viewP != null) {
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewP, _idxAttP, scaleP, false, mButtonGroup, 0);
            }

            if (viewI != null) {
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, field,
                        viewI, _idxAttI, scaleI, false, mButtonGroup, 0);
            }
        }
    }

    private void bindResponsivenessWidgets(View viewAttitude, View viewRate, View viewMaxRate, String attiRespField, String elementName) {
        for (UAVObject stabiSettingsBank : _stabiSettingsBanks) {

            // Get element indices for ManualRate and MaximumRate fields. Assume that these
            // are the same for both.
            List<String> rateResponsivenessElementNames = _stabiSettingsBanks.get(0).getField(StabilizationSettingsBank1.FIELD_MANUALRATE).getElementNames();

            // Get index for axis we are setting here
            int index = rateResponsivenessElementNames.indexOf(elementName);

            if (viewAttitude != null) {
                UAVObjectField axisMax = stabiSettingsBank.getField(attiRespField);
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, axisMax,
                        viewAttitude, 0, 1, false, mButtonGroup, 0);
            }

            if(viewRate != null){
                UAVObjectField manualRate = stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_MANUALRATE);
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, manualRate,
                        viewRate, index, 1, false, mButtonGroup, 0);
            }

            if(viewMaxRate != null){
                UAVObjectField maxRate = stabiSettingsBank.getField(StabilizationSettingsBank1.FIELD_MAXIMUMRATE);
                _objectToGui.addUAVObjectToWidgetRelation(stabiSettingsBank, maxRate,
                        viewMaxRate, index, 1, false, mButtonGroup, 0);
            }
        }
    }

    private void configureBankSaving() {
        int curBank = 0;
        for (UAVObject curStabiObject : _stabiSettingsBanks) {
            if (curBank == _bankNumber) {
                if (DEBUG) Log.d(TAG, "Enable updates for " + curStabiObject.getName());
                _objectToGui.enableObjUpdates(curStabiObject);
                _objectToGui.enableSavesForObject(curStabiObject);
            } else {
                if (DEBUG) Log.d(TAG, "Disable updates for " + curStabiObject.getName());
                _objectToGui.disableObjUpdates(curStabiObject);
                _objectToGui.disableSavesForObject(curStabiObject);
            }
            ++curBank;
        }
    }
}
