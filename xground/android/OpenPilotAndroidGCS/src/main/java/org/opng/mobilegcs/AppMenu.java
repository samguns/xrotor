/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs;

import android.content.Context;
import android.util.SparseArray;

import org.opng.mobilegcs.firmware.gui.UploaderActivity;
import org.opng.mobilegcs.flight.StickExpoActivity;
import org.opng.mobilegcs.flight.ThrustPIDActivity;
import org.opng.mobilegcs.flight.TxPIDActivity;
import org.opng.mobilegcs.flightdata.AlarmsHudMapFragment;
import org.opng.mobilegcs.flightdata.HudAlarmsFragment;
import org.opng.mobilegcs.flightdata.MapFragment;
import org.opng.mobilegcs.flightmodes.FlightSelectionActivity;
import org.opng.mobilegcs.objectbrowser.UAVObjectListActivity;
import org.opng.mobilegcs.preferences.GCSPreferencesFragment;
import org.opng.mobilegcs.stabilization.AcroPlusFragment;
import org.opng.mobilegcs.stabilization.CruiseControlFragment;
import org.opng.mobilegcs.stabilization.RattitudeFragment;
import org.opng.mobilegcs.stabilization.StabiAttitudeCombinedFragment;
import org.opng.mobilegcs.stabilization.StabiAxisLockFragment;
import org.opng.mobilegcs.stabilization.StabiRateCombinedFragment;
import org.opng.mobilegcs.stabilization.StabiResponsivenessCombinedFragment;
import org.opng.mobilegcs.stabilization.StabilizationMiscFragment;
import org.opng.mobilegcs.stabilization.VarioAltitudeFragment;
import org.opng.mobilegcs.util.ActivityAppMenuItem;
import org.opng.mobilegcs.util.AppMenuItem;
import org.opng.mobilegcs.util.BaseFragmentAppMenuItem;
import org.opng.mobilegcs.util.FragmentAppMenuItem;
import org.opng.mobilegcs.util.ItemListActivity;
import org.opng.mobilegcs.util.ItemListFragment;
import org.opng.mobilegcs.wizard.OPTuneActivity;

public class AppMenu {

    public static final String APP_MENU_ITEM = "APP_MENU_ITEM";
    private static final SparseArray<AppMenuItem> itemDico = new SparseArray<>();
    private static AppMenuItem root;
    private static Context context;

    public static AppMenuItem getMenu(Context ctx) {
        context = ctx;
        if (root == null)
            buildMenu();

        return root;
    }

    private static void addToDico(AppMenuItem item) {
        itemDico.put(item.getId(), item);

        for (AppMenuItem it : item.getSubMenu()) {
            addToDico(it);
        }
    }

    public static AppMenuItem getMenuItem(int menuId) {
        return itemDico.get(menuId);
    }

    private static void buildMenu() {
        root = new AppMenuItem(context.getString(R.string.app_name));

        root.getSubMenu().add(new BaseFragmentAppMenuItem<>(R.id.nav_flight_data, context.getString(R.string.main_flight_settings_label), R.drawable.ic_navigation_black_24dp, FlightDataFragment.class));
//        root.SubMenu.add(new FragmentAppMenuItem(R.id.nav_planner, context.getString(R.string.welcome_planner_label), R.drawable.ic_map_black_24dp, PlannerFragment.class));
        root.getSubMenu().add(new BaseFragmentAppMenuItem<>(R.id.nav_config, context.getString(R.string.main_config_label), R.drawable.ic_tune_black_24dp, ConfigFragment.class));
        root.getSubMenu().add(new BaseFragmentAppMenuItem<>(R.id.nav_log, context.getString(R.string.main_logging_label), R.drawable.ic_sd_storage_black_24dp, LogFragment.class));
        root.getSubMenu().add(new BaseFragmentAppMenuItem<>(R.id.nav_system, context.getString(R.string.main_system_label), R.drawable.ic_widgets_black_24dp, ItemListFragment.class));
        root.getSubMenu().add(new FragmentAppMenuItem<>(R.id.nav_settings, context.getString(R.string.main_settings_label), R.drawable.ic_settings_black_24dp, GCSPreferencesFragment.class));
        root.getSubMenu().add(new BaseFragmentAppMenuItem<>(R.id.nav_about, context.getString(R.string.main_about_label), R.drawable.ic_help_black_24dp, AboutFragment.class));

        initFlightDataMenu(root.getSubMenu().get(0));
        initConfigMenu(root.getSubMenu().get(1));
        initSystemMenu(root.getSubMenu().get(3));

        addToDico(root);
    }

    private static void initFlightDataMenu(AppMenuItem menu) {
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_alarms_hud), R.drawable.ic_drawer, HudAlarmsFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_map), R.drawable.ic_drawer, MapFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_alarms_hud_map), R.drawable.ic_drawer, AlarmsHudMapFragment.class));
    }

    private static void initConfigMenu(AppMenuItem menu) {
        AppMenuItem m = new AppMenuItem(context.getString(R.string.title_wizards), R.drawable.ic_drawer);

//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.vehicle), R.drawable.ic_drawer, VehiculeWizardActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.radio), R.drawable.ic_drawer, RadioWizardActivity.class));
        m.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_optune), R.drawable.ic_drawer, OPTuneActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.calibration), R.drawable.ic_drawer, CalibrationWizardActivity.class));

        menu.getSubMenu().add(m);

//        m = new AppMenuItem(context.getString(R.string.hardware), R.drawable.ic_drawer);
//
//        m.SubMenu.add(new ActivityAppMenuItem<android.app.Activity>(context.getString(R.string.board_settings), R.drawable.ic_memory_black_24dp, BoardActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.vehicle_mixer), R.drawable.ic_drawer, MixerActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.motor_esc), R.drawable.ic_drawer, MotorActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.radio), R.drawable.ic_drawer, RadioActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.op_link), R.drawable.ic_drawer, OpLinkActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem<>(context.getString(R.string.sensors), R.drawable.ic_drawer, ItemListActivity.class));
//        initSensorsnMenu(m.SubMenu.get(5));
//
//        menu.SubMenu.add(m);

        m = new AppMenuItem(context.getString(R.string.main_flight_settings_label), R.drawable.ic_drawer);

        m.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_flight_selection), R.drawable.ic_drawer, FlightSelectionActivity.class));
        m.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_stabilization), R.drawable.ic_drawer, ItemListActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem<>(context.getString(R.string.gps_modes), R.drawable.ic_drawer, ItemListActivity.class));
        m.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_thrust_control), R.drawable.ic_drawer, ItemListActivity.class));
        m.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_txpid), R.drawable.ic_drawer, TxPIDActivity.class));
        m.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_stabi_tps), R.drawable.ic_drawer, ThrustPIDActivity.class));
        m.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_stabi_expo), R.drawable.ic_drawer, StickExpoActivity.class));

        initStabilizeMenu(m.getSubMenu().get(1));
//        initGPSMenu(m.SubMenu.get(2));
        initThrustMenu(m.getSubMenu().get(2));

        menu.getSubMenu().add(m);

//        m = new AppMenuItem(context.getString(R.string.camera), R.drawable.ic_drawer);

//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.gimbal_settings), R.drawable.ic_drawer, GimbalActivity.class));
//        m.SubMenu.add(new ActivityAppMenuItem(context.getString(R.string.Minim_OSD), R.drawable.ic_drawer, OSDActivity.class));

//        menu.SubMenu.add(m);
    }

    private static void initStabilizeMenu(AppMenuItem menu) {
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_stabi_resp), StabiResponsivenessCombinedFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_stabi_attitude), StabiAttitudeCombinedFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_stabi_rate), StabiRateCombinedFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_stabi_rattitude), RattitudeFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_acro_plus), AcroPlusFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_stabi_axis_lock), StabiAxisLockFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_stabi_misc), StabilizationMiscFragment.class));
    }

//    private static void initGPSMenu(AppMenuItem menu) {
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.pos_hold), PosHoldFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.gps_assist), GpsAssistFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.rtb), RTBFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.take_off), TakeOffFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.land), LandFragment.class));
//    }

    private static void initThrustMenu(AppMenuItem menu) {
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_cruise_control), CruiseControlFragment.class));
        menu.getSubMenu().add(new BaseFragmentAppMenuItem<>(context.getString(R.string.title_vario_alt_tuning), VarioAltitudeFragment.class));
    }

//    private static void initSensorsnMenu(AppMenuItem menu) {
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.temperature), TemperatureFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.accelerometer), AccelerometerFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.gyroscope), GyroscopeFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.magnetometer), MagnetometerFragment.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.board_level), BoardLevelFragment.class));
//    }

    private static void initSystemMenu(AppMenuItem menu) {
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.gps_data), GPSFragment.class));
        menu.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_uavobject_list), UAVObjectListActivity.class));
//        menu.SubMenu.add(new BaseFragmentAppMenuItem(context.getString(R.string.scopes), ScopeFragment.class));
        menu.getSubMenu().add(new ActivityAppMenuItem<>(context.getString(R.string.title_uploader), UploaderActivity.class));
    }
}
