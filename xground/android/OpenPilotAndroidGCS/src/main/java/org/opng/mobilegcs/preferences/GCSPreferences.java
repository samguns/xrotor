/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.preferences;


interface GCSPreferences {

    String getConnectionType();

    boolean keepScreenOn();

    String getBluetoothDeviceAddress();

    String getMapType();

    boolean isAlarmsTTSEnabled();

    boolean isArmedStateTTSEnabled();

    boolean logOnConnect();

    int getComBaudSpeed();

    String getIpAddress();

    int getTcpPort();
}
