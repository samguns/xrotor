/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.util;

import android.content.Context;
//import android.content.AsyncTaskLoader;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import org.opng.uavtalk.UAVObjectManager;

import java.util.concurrent.TimeUnit;

public class UAVObjectLoader extends AsyncTaskLoader<UAVObjectManager> {
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final TelemetryServiceHandler mTelemetryServiceHandler = new TelemetryServiceHandler();
    // Logging setup
    private final String TAG = "UAVObjectLoader";

    public UAVObjectLoader(Context context) {
        super(context);
    }

    public static boolean isDEBUG() {
        return DEBUG;
    }

    @Override
    public UAVObjectManager loadInBackground() {
        UAVObjectManager retVal = null;
        boolean okSoFar;

//        if(!_bound){
        if (DEBUG) Log.d(TAG, "Waiting for telemetry to start");
        try {
            okSoFar = mTelemetryServiceHandler.getTelemServiceConnectedLatch().await(60, TimeUnit.SECONDS);
            if (DEBUG) Log.d(TAG, "Telemetry started");
        } catch (InterruptedException e) {
            okSoFar = false;
            e.printStackTrace();
        }
//        }

        if (okSoFar) {
            if (DEBUG) Log.d(TAG, "Waiting for telemetry connection");

            try {
                okSoFar = mTelemetryServiceHandler.getTelemRunningLatch().await(60, TimeUnit.SECONDS);
                if (DEBUG) Log.d(TAG, "Telemetry connected");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (okSoFar) {
            retVal = mTelemetryServiceHandler.getObjManager();
        }
        return retVal;
    }

    @Override
    protected void onStartLoading() {
        super.onStartLoading();

        final UAVObjectManager objManager = mTelemetryServiceHandler.getObjManager();
        if (objManager != null) {
            if (DEBUG) Log.d(TAG, "onStartLoading(): delivering existing object manager");
            deliverResult(objManager);
        } else {
            if (DEBUG) Log.d(TAG, "onStartLoading(): get new object manager");
            mTelemetryServiceHandler.bindToTelemetryService(getContext());
        }

        if (takeContentChanged() || objManager == null) {
            // If the data has changed since the last time it was loaded
            // or is not currently available, start a load.
            forceLoad();
        }
    }
}
