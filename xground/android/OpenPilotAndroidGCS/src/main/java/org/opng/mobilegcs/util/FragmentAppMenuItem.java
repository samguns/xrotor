/*
 * Eric Henaff Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class FragmentAppMenuItem<T extends Fragment> extends AppMenuItem implements AppMenuItemFragmentOperations {

    private final Class<T> mFragmentClass;

    public FragmentAppMenuItem(int id, String title, int icon, Class<T> fragment) {
        super(id, title, icon);
        mFragmentClass = fragment;
    }

    public FragmentAppMenuItem(String title, int icon, Class<T> fragment) {
        super(title, icon);
        mFragmentClass = fragment;
    }

    public FragmentAppMenuItem(int id, String title, Class<T> fragment) {
        super(id, title);
        mFragmentClass = fragment;
    }

    public FragmentAppMenuItem(String title, Class<T> fragment) {
        super(title);
        mFragmentClass = fragment;
    }

    @Override
    public Fragment getFragment() throws IllegalAccessException, InstantiationException {

        return mFragmentClass.newInstance();
    }

    @Override
    public void Open(FragmentManager fragmentManager, int containerId) {
        // Requires API 19+
        //noinspection TryWithIdenticalCatches
        try {
            Fragment frag = fragmentManager.findFragmentByTag(mTitle + getId());

            if (frag == null)
                frag = getFragment();

            fragmentManager.beginTransaction()
                    .replace(containerId, frag, mTitle + getId())
                    .commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}
