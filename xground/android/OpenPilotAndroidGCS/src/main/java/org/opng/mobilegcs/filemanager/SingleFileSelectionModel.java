/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.filemanager;

import android.view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * FileSelectionModel implementation that allows only a single item to be
 * selected. Selecting a new item will deselect the previous one.
 */
public class SingleFileSelectionModel implements FileSelectionModel {
    private final List<Integer> mSelectedItems = new ArrayList<>();
    private final List<Integer> mSelectedItemsAccessor = Collections.unmodifiableList(mSelectedItems);
    private final List<FileSelectionModelListener> mListenerList = new CopyOnWriteArrayList<>();
    private FileRecyclerViewAdapter mFileRecyclerViewAdapter;

    @Override
    public void setRecyclerViewAdapter(FileRecyclerViewAdapter fileRecyclerViewAdapter) {
        mFileRecyclerViewAdapter = fileRecyclerViewAdapter;
    }

    @Override
    public boolean itemIsSelected(int position) {
        return mSelectedItems.contains(position);
    }

    @Override
    public List<Integer> getSelectedItems() {
        return mSelectedItemsAccessor;
    }

    @Override
    public void handleSelection(View view) {
        int oldSelection = mSelectedItems.isEmpty() ? -1 : mSelectedItems.get(0);
        int newSelection = ((FileRecyclerViewAdapter.FileViewHolder) view.getTag()).getItemPosition();

        if (mSelectedItems.contains(newSelection)) {
            // This will essentially deselect our single selected item
            mSelectedItems.clear();
        } else if (mSelectedItems.isEmpty()) {
            // New selected item with no previous selection, add to list
            mSelectedItems.add(newSelection);
        } else {
            // New selected item, replace the old one
            mSelectedItems.set(0, newSelection);

        }
        if (oldSelection > -1) {
            mFileRecyclerViewAdapter.notifyItemChanged(oldSelection);
        }
        mFileRecyclerViewAdapter.notifyItemChanged(newSelection);
        fireSelectionChanged();
    }

    @Override
    public void handleLongClick(View view) {
        int itemPosition = ((FileRecyclerViewAdapter.FileViewHolder) view.getTag()).getItemPosition();
        fireItemLongClicked(itemPosition);
    }

    @Override
    public void reset() {
        mSelectedItems.clear();
    }

    @Override
    public void addFileSelectionModelListener(FileSelectionModelListener listener) {
        mListenerList.add(listener);
    }

    @Override
    public void removeFileSelectionModelListener(FileSelectionModelListener listener) {
        mListenerList.remove(listener);
    }

    private void fireSelectionChanged() {
        for (FileSelectionModelListener curListener : mListenerList) {
            curListener.selectionChanged(getSelectedItems());
        }
    }

    private void fireItemLongClicked(int itemPosition) {
        for (FileSelectionModelListener curListener : mListenerList) {
            curListener.itemLongClicked(itemPosition);
        }
    }
}
