/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

import org.opng.mobilegcs.util.Crc;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

public abstract class UAVObject implements Cloneable {

    protected final static int UAVOBJ_ACCESS_SHIFT = 0;
    protected final static int UAVOBJ_GCS_ACCESS_SHIFT = 1;
    protected final static int UAVOBJ_TELEMETRY_ACKED_SHIFT = 2;
    protected final static int UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT = 3;
    protected final static int UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT = 4;
    protected final static int UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT = 6;
    protected final static int UAVOBJ_LOGGING_UPDATE_MODE_SHIFT = 8;
    private final static int UAVOBJ_UPDATE_MODE_MASK = 0x3;
    private final Set<UAVObjectEventListener> mEventListeners = new CopyOnWriteArraySet<>();
    private ByteBuffer mData;
    /**
     * Private data for the object, common to all
     */
    private long mObjID;
    private long mInstID;
    private boolean mIsSingleInst;
    private String mName;
    private String mDescription;
    private String mCategory;
    private int mNumBytes;
    private List<UAVObjectField> mObjectFields;
    UAVObject(long objID, Boolean isSingleInst, String name) {
        mObjID = objID;
        mInstID = 0;
        mIsSingleInst = isSingleInst;
        mName = name;
    }

    public abstract boolean isMetadata();

    synchronized void initialize(long instID) {
        mInstID = instID;
    }

    /**
     * Initialize objects' data fields
     *
     * @param fields   List of fields held by the object
     * @param data     Pointer to that actual object data, this is needed by the
     *                 fields to access the data
     * @param numBytes Number of bytes in the object (total, including all fields)
     */
    protected synchronized void initializeFields(List<UAVObjectField> fields, ByteBuffer data,
                                                 int numBytes) {
        mNumBytes = numBytes;
        mObjectFields = fields;
        mData = data;
        // Initialize fields
        int offset = 0;
        for (UAVObjectField curField : fields) {
            curField.initialize(data, offset, this);
            offset += curField.getNumBytes();
        }
    }

    /**
     * Get the object ID
     */
    public long getObjID() {
        return mObjID;
    }

    /**
     * Get the instance ID
     */
    public long getInstID() {
        return mInstID;
    }

    /**
     * Returns true if this is a single instance object
     */
    public boolean isSingleInstance() {
        return mIsSingleInst;
    }

    /**
     * Get the name of the object
     */
    public String getName() {
        return mName;
    }

    /**
     * Get the description of the object
     *
     * @return The description of the object
     */
    public String getDescription() {
        return mDescription;
    }

    /**
     * Set the description of the object
     *
     * @param description description of the object
     */
    protected void setDescription(String description) {
        this.mDescription = description;
    }

    /**
     * Get the category of the object
     */
    public String getCategory() {
        return mCategory;
    }

    /**
     * Set the category of the object
     */
    public void setCategory(String category) {
        mCategory = category;
    }

    /**
     * Get the total number of bytes of the object's data
     */
    public int getNumBytes() {
        return mNumBytes;
    }

    // /**
    // * Request that this object is updated with the latest values from the
    // autopilot
    // */
    public void requestUpdate() {
        fireUpdateRequested();
    }

    /**
     * Request that all instances of this object are updated with the latest values from the autopilot
     * Must be called on instance zero
     */
    public void requestUpdateAll() {
        fireUpdateRequestedAll();
    }

    /**
     * Signal that the object has been updated
     */
    public void updated() {
        fireUpdated(true);
    }

    /**
     * Signal that all instance of the object have been updated
     * Must be called on instance zero
     */
    public void updatedAll() {
        fireUpdatedAll(true);
    }

    /**
     * Get the object's fields
     */
    public synchronized List<UAVObjectField> getFields() {
        return mObjectFields;
    }

    /**
     * Get a specific field
     *
     * @return The field or NULL if not found
     */
    public UAVObjectField getField(String name) {
        // Look for field
        for (UAVObjectField field : mObjectFields) {
            if (field.getName().equals(name))
                return field;
        }
        //throw new Exception("Field not found");
        return null;
    }

    /**
     * Pack the object data into a byte array. Note: The array must already have enough space allocated for the object
     *
     * @param dataOut ByteBuffer to receive the data.
     * @return The number of bytes copied
     * @throws Exception
     */
    public synchronized int pack(ByteBuffer dataOut) throws Exception {
        if (dataOut.remaining() < getNumBytes())
            throw new Exception("Not enough bytes in ByteBuffer to pack object");
        int numBytes = 0;

        for (UAVObjectField field : mObjectFields) {
            numBytes += field.pack(dataOut);
        }
        return numBytes;
    }

    /**
     * Unpack the object data from a byte array
     *
     * @param dataIn The ByteBuffer to pull data from
     */
    public synchronized void unpack(ByteBuffer dataIn) {
        if (dataIn == null)
            return;

        for (UAVObjectField field : mObjectFields) {
            field.unpack(dataIn);
        }

        // Trigger all the listeners for the unpack event
        fireUnpacked();
        fireUpdated(false);
    }

    /**
     * Update a CRC with the object data
     *
     * @return The updated CRC
     */
    public synchronized int updateCRC(int crc) {
        return Crc.updateCRC(crc, mData.array(), mNumBytes);
    }

    public void addUAVObjectEventListener(UAVObjectEventListener listener) {
        mEventListeners.add(listener);
    }

    public void removeUAVObjectEventListener(UAVObjectEventListener listener) {
        mEventListeners.remove(listener);
    }

    synchronized void fireTransactionCompleted(boolean status) {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.transactionCompleted(this, status);
        }
    }


    // /**
    // * Save the object data to the file.
    // * The file will be created in the current directory
    // * and its name will be the same as the object with
    // * the .uavobj extension.
    // * @returns True on success, false on failure
    // */
    // bool UAVObject::save()
    // {
    // QMutexLocker locker(mutex);
    //
    // // Open file
    // QFile file(name + ".uavobj");
    // if (!file.open(QFile::WriteOnly))
    // {
    // return false;
    // }
    //
    // // Write object
    // if ( !save(file) )
    // {
    // return false;
    // }
    //
    // // Close file
    // file.close();
    // return true;
    // }
    //
    // /**
    // * Save the object data to the file.
    // * The file is expected to be already open for writting.
    // * The data will be appended and the file will not be closed.
    // * @returns True on success, false on failure
    // */
    // bool UAVObject::save(QFile& file)
    // {
    // QMutexLocker locker(mutex);
    // quint8 buffer[numBytes];
    // quint8 tmpId[4];
    //
    // // Write the object ID
    // qToLittleEndian<quint32>(objID, tmpId);
    // if ( file.write((const char*)tmpId, 4) == -1 )
    // {
    // return false;
    // }
    //
    // // Write the instance ID
    // if (!isSingleInst)
    // {
    // qToLittleEndian<quint16>(instID, tmpId);
    // if ( file.write((const char*)tmpId, 2) == -1 )
    // {
    // return false;
    // }
    // }
    //
    // // Write the data
    // pack(buffer);
    // if ( file.write((const char*)buffer, numBytes) == -1 )
    // {
    // return false;
    // }
    //
    // // Done
    // return true;
    // }
    //
    // /**
    // * Load the object data from a file.
    // * The file will be openned in the current directory
    // * and its name will be the same as the object with
    // * the .uavobj extension.
    // * @returns True on success, false on failure
    // */
    // bool UAVObject::load()
    // {
    // QMutexLocker locker(mutex);
    //
    // // Open file
    // QFile file(name + ".uavobj");
    // if (!file.open(QFile::ReadOnly))
    // {
    // return false;
    // }
    //
    // // Load object
    // if ( !load(file) )
    // {
    // return false;
    // }
    //
    // // Close file
    // file.close();
    // return true;
    // }
    //
    // /**
    // * Load the object data from file.
    // * The file is expected to be already open for reading.
    // * The data will be read and the file will not be closed.
    // * @returns True on success, false on failure
    // */
    // bool UAVObject::load(QFile& file)
    // {
    // QMutexLocker locker(mutex);
    // quint8 buffer[numBytes];
    // quint8 tmpId[4];
    //
    // // Read the object ID
    // if ( file.read((char*)tmpId, 4) != 4 )
    // {
    // return false;
    // }
    //
    // // Check that the IDs match
    // if (qFromLittleEndian<quint32>(tmpId) != objID)
    // {
    // return false;
    // }
    //
    // // Read the instance ID
    // if ( file.read((char*)tmpId, 2) != 2 )
    // {
    // return false;
    // }
    //
    // // Check that the IDs match
    // if (qFromLittleEndian<quint16>(tmpId) != instID)
    // {
    // return false;
    // }
    //
    // // Read and unpack the data
    // if ( file.read((char*)buffer, numBytes) != numBytes )
    // {
    // return false;
    // }
    // unpack(buffer);
    //
    // // Done
    // return true;
    // }

    private synchronized void fireUpdated(boolean manually) {
        for (UAVObjectEventListener curListener : mEventListeners) {
            if (manually) {
                curListener.objectUpdatedManual(this, false);
            }

            curListener.objectUpdated(this);
        }

//        Log.d("UAVObject", _name + " " + Arrays.toString(_data.array()));
    }

    private synchronized void fireUpdatedAll(boolean manually) {
        for (UAVObjectEventListener curListener : mEventListeners) {
            if (manually) {
                curListener.objectUpdatedManual(this, true);
            }

            curListener.objectUpdated(this);
        }

//        Log.d("UAVObject", _name + " " + Arrays.toString(_data.array()));
    }

    private synchronized void fireUnpacked() {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.objectUnpacked(this);
        }
    }

    synchronized void fireUpdatedAuto() {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.objectUpdatedAuto(this);
        }
    }

    synchronized void fireUpdatedManual() {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.objectUpdatedManual(this, false);
        }
    }

    synchronized void fireUpdatedManualAll() {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.objectUpdatedManual(this, true);
        }
    }

    synchronized void fireUpdateRequested() {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.updateRequested(this, false);
        }
    }

    private synchronized void fireUpdateRequestedAll() {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.updateRequested(this, true);
        }
    }

    private synchronized void fireNewInstance(UAVObject obj) {
        for (UAVObjectEventListener curListener : mEventListeners) {
            curListener.newInstance(obj);
        }
    }

    /**
     * Return a string with the object information
     */
    @Override
    public String toString() {
        return mName;
    }

    /**
     * Return a string with the object information (only the header)
     */
    public String toStringBrief() {
        return String.format(
                "%s (ID: %d, InstID: %d, NumBytes: %d, SInst: %b)\n", mName,
                mObjID, mInstID, mNumBytes, mIsSingleInst);
    }

    /**
     * Return a string with the object information (only the data)
     */
    public String toStringData() {
        StringBuilder s = new StringBuilder();
        for (UAVObjectField field : mObjectFields) {
            s.append(field.toString());
        }
        return s.toString();
    }

    /**
     * Emit the transactionCompleted event (used by the UAVTalk plugin)
     */
    public void emitTransactionCompleted(boolean success) {
        fireTransactionCompleted(success);
    }

    /**
     * Java specific functions
     */
    @Override
    public synchronized UAVObject clone() throws CloneNotSupportedException {
        UAVObject newObj = (UAVObject) super.clone();
        newObj.mObjID = mObjID;
        newObj.mInstID = 0;
        newObj.mIsSingleInst = mIsSingleInst;
        newObj.mName = mName;
        List<UAVObjectField> newFields = new ArrayList<>();
        for (UAVObjectField curField : mObjectFields) {
            UAVObjectField newField = curField.clone();
            newFields.add(newField);
        }
        newObj.initializeFields(newFields, ByteBuffer.allocate(mNumBytes), mNumBytes);
        return newObj;
    }

    public abstract Metadata getMetadata();

    /**
     * Abstract functions
     */
    public abstract void setMetadata(Metadata mdata);

    protected abstract Metadata getDefaultMetadata();

    /**
     * Object update mode
     */
    public enum UpdateMode {
        UPDATEMODE_MANUAL(0), /**
         * Manually update object, by calling the updated() function
         */
        UPDATEMODE_PERIODIC(1), /**
         * Automatically update object at periodic intervals
         */
        UPDATEMODE_ONCHANGE(2), /**
         * Only update object when its data changes
         */
        UPDATEMODE_THROTTLED(3);
        private final int _value;

        /**
         * Object is updated on change, but not more often than the interval time
         */

        UpdateMode(int value) {
            _value = value;
        }

        public static UpdateMode fromInt(int value) {
            UpdateMode retVal = null;

            switch (value) {
                case 0:
                    retVal = UPDATEMODE_MANUAL;
                    break;
                case 1:
                    retVal = UPDATEMODE_PERIODIC;
                    break;
                case 2:
                    retVal = UPDATEMODE_ONCHANGE;
                    break;
                case 3:
                    retVal = UPDATEMODE_THROTTLED;
                    break;
            }

            return retVal;
        }

        public int getValue() {
            return _value;
        }
    }

    /**
     * Access mode
     */
    public enum AccessMode {
        ACCESS_READWRITE(0), ACCESS_READONLY(1);

        private final int _value;

        AccessMode(int value) {
            _value = value;
        }

        public static AccessMode fromInt(int value) {
            AccessMode retVal = null;
            switch (value) {
                case 0:
                    retVal = ACCESS_READWRITE;
                    break;
                case 1:
                    retVal = ACCESS_READONLY;
                    break;
            }

            return retVal;
        }

        public int getValue() {
            return _value;
        }
    }

    public final static class Metadata {
        /**
         * Object metadata, each object has a meta object that holds its metadata. The metadata define
         * properties for each object and can be used by multiple modules (e.g. telemetry and logger)
         *
         * The object metadata flags are packed into a single 16 bit integer.
         * The bits in the flag field are defined as:
         *
         *   Bit(s)  Name                       Meaning
         *   ------  ----                       -------
         *      0    access                     Defines the access level for the local transactions (readonly=0 and readwrite=1)
         *      1    gcsAccess                  Defines the access level for the local GCS transactions (readonly=0 and readwrite=1), not used in the flight s/w
         *      2    telemetryAcked             Defines if an ack is required for the transactions of this object (1:acked, 0:not acked)
         *      3    gcsTelemetryAcked          Defines if an ack is required for the transactions of this object (1:acked, 0:not acked)
         *    4-5    telemetryUpdateMode        Update mode used by the telemetry module (UAVObjUpdateMode)
         *    6-7    gcsTelemetryUpdateMode     Update mode used by the GCS (UAVObjUpdateMode)
         *    8-9    loggingUpdateMode          Update mode used by the logging module (UAVObjUpdateMode)
         */

        /**
         * Defines flags for update and logging modes and whether an update should be ACK'd (bits defined above)
         */
        public int flags =
                AccessMode.ACCESS_READWRITE.getValue() |
                        AccessMode.ACCESS_READWRITE.getValue() << UAVOBJ_GCS_ACCESS_SHIFT |
                        1 << UAVOBJ_TELEMETRY_ACKED_SHIFT |
                        1 << UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT |
                        UpdateMode.UPDATEMODE_ONCHANGE.getValue() << UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT |
                        UpdateMode.UPDATEMODE_ONCHANGE.getValue() << UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT |
                        UpdateMode.UPDATEMODE_ONCHANGE.getValue() << UAVOBJ_LOGGING_UPDATE_MODE_SHIFT;

        /**
         * Update period used by the telemetry module (only if telemetry mode is PERIODIC)
         */
        public int flightTelemetryUpdatePeriod;

        /**
         * Update period used by the GCS (only if telemetry mode is PERIODIC)
         */
        public int gcsTelemetryUpdatePeriod;

        /**
         * Update period used by the GCS (only if telemetry mode is PERIODIC)
         */
        public int loggingUpdatePeriod;
        /**
         * Update period used by the logging module (only if logging mode is
         * PERIODIC)
         */

        public static short AccessModeNum(AccessMode mode) {
            return (short) mode.getValue();
        }

        public static short UpdateModeNum(UpdateMode mode) {
            return (short) mode.getValue();
        }

        /**
         * Helper method for metadata accessors
         *
         * @param shift The offset of these bits
         * @param value The new value
         * @param mask  The mask of these bits
         */
        private void SET_BITS(int shift, int value, int mask) {
            this.flags = (this.flags & ~(mask << shift)) | (value << shift);
        }

        /**
         * Get the UAVObject metadata access member
         * \return the access type
         */
        public AccessMode GetFlightAccess() {
            return AccessMode.fromInt((this.flags) & 1);
        }

        /**
         * Set the UAVObject metadata access member
         * \param[in] mode The access mode
         */
        public void SetFlightAccess(AccessMode mode) {
            // Need to convert java enums which have no numeric value to bits
            SET_BITS(UAVOBJ_ACCESS_SHIFT, mode.getValue(), 1);
        }

        /**
         * Get the UAVObject metadata GCS access member
         * \return the GCS access type
         */
        public AccessMode GetGcsAccess() {
            return AccessMode.fromInt((this.flags >> UAVOBJ_GCS_ACCESS_SHIFT) & 1);
        }

        /**
         * Set the UAVObject metadata GCS access member
         * \param[in] mode The access mode
         */
        public void SetGcsAccess(AccessMode mode) {
            // Need to convert java enums which have no numeric value to bits
            SET_BITS(UAVOBJ_GCS_ACCESS_SHIFT, mode.getValue(), 1);
        }

        /**
         * Get the UAVObject metadata telemetry acked member
         * \return the telemetry acked boolean
         */
        public boolean GetFlightTelemetryAcked() {
            return (((this.flags >> UAVOBJ_TELEMETRY_ACKED_SHIFT) & 1) == 1);
        }

        /**
         * Set the UAVObject metadata telemetry acked member
         * \param[in] val The telemetry acked boolean
         */
        public void SetFlightTelemetryAcked(boolean val) {
            SET_BITS(UAVOBJ_TELEMETRY_ACKED_SHIFT, val ? 1 : 0, 1);
        }

//		/**
//		 * Maps from the bitfield number to the symbolic java enumeration
//		 * @param num The value in the bitfield after shifting
//		 * @return The update mode
//		 */
//		public static AccessMode AccessModeEnum(int num) {
//			switch(num) {
//			case 0:
//				return AccessMode.ACCESS_READONLY;
//			case 1:
//				return AccessMode.ACCESS_READWRITE;
//			}
//			return AccessMode.ACCESS_READONLY;
//		}
//
//		/**
//		 * Maps from the java symbolic enumeration of update mode to the bitfield value
//		 * @param e The update mode
//		 * @return The numeric value to use on the wire
//		 */
//		public static int AccessModeNum(AccessMode e) {
//			switch(e) {
//			case ACCESS_READONLY:
//				return 0;
//			case ACCESS_READWRITE:
//				return 1;
//			}
//			return 0;
//		}
//
//		/**
//		 * Maps from the bitfield number to the symbolic java enumeration
//		 * @param num The value in the bitfield after shifting
//		 * @return The update mode
//		 */
//		public static UpdateMode UpdateModeEnum(int num) {
//			switch(num) {
//			case 0:
//				return UpdateMode.UPDATEMODE_MANUAL;
//			case 1:
//				return UpdateMode.UPDATEMODE_PERIODIC;
//			case 2:
//				return UpdateMode.UPDATEMODE_ONCHANGE;
//			default:
//				return UpdateMode.UPDATEMODE_THROTTLED;
//			}
//		}
//
//		/**
//		 * Maps from the java symbolic enumeration of update mode to the bitfield value
//		 * @param e The update mode
//		 * @return The numeric value to use on the wire
//		 */
//		public static int UpdateModeNum(UpdateMode e) {
//			switch(e) {
//			case UPDATEMODE_MANUAL:
//				return 0;
//			case UPDATEMODE_PERIODIC:
//				return 1;
//			case UPDATEMODE_ONCHANGE:
//				return 2;
//			case UPDATEMODE_THROTTLED:
//				return 3;
//			}
//			return 0;
//		}

        /**
         * Get the UAVObject metadata GCS telemetry acked member
         * \return the telemetry acked boolean
         */
        public boolean GetGcsTelemetryAcked() {
            return ((this.flags >> UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT) & 1) == 1;
        }

        /**
         * Set the UAVObject metadata GCS telemetry acked member
         * \param[in] val The GCS telemetry acked boolean
         */
        public void SetGcsTelemetryAcked(boolean val) {
            SET_BITS(UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT, val ? 1 : 0, 1);
        }

        /**
         * Get the UAVObject metadata telemetry update mode
         * \return the telemetry update mode
         */
        public UpdateMode GetFlightTelemetryUpdateMode() {
            return UpdateMode.fromInt((this.flags >> UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT) & UAVOBJ_UPDATE_MODE_MASK);
        }

        /**
         * Set the UAVObject metadata telemetry update mode member
         * \param[in] metadata The metadata object
         * \param[in] val The telemetry update mode
         */
        public void SetFlightTelemetryUpdateMode(UpdateMode val) {
            SET_BITS(UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT, val.getValue(), UAVOBJ_UPDATE_MODE_MASK);
        }

        /**
         * Get the UAVObject metadata GCS telemetry update mode
         * \param[in] metadata The metadata object
         * \return the GCS telemetry update mode
         */
        public UpdateMode GetGcsTelemetryUpdateMode() {
            return UpdateMode.fromInt((this.flags >> UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT) & UAVOBJ_UPDATE_MODE_MASK);
        }

        /**
         * Set the UAVObject metadata GCS telemetry update mode member
         * \param[in] metadata The metadata object
         * \param[in] val The GCS telemetry update mode
         */
        public void SetGcsTelemetryUpdateMode(UpdateMode val) {
            SET_BITS(UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT, val.getValue(), UAVOBJ_UPDATE_MODE_MASK);
        }
    }
}
