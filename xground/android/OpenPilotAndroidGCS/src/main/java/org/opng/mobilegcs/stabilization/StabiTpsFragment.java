/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;

import org.opng.mobilegcs.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class StabiTpsFragment extends Fragment implements StabiGuiFragment {
    private UAVObjectManagerLoaderHandler mUAVObjectManagerLoaderHandler;
    private View mRootView;

    public StabiTpsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_stabi_tps, container, false);

        // Set up our loader handler: get all the controls it needs and then create it
        Button apply = (Button) mRootView.findViewById(R.id.buttonCommonFooterApply);
        Button save = (Button) mRootView.findViewById(R.id.buttonCommonFooterSave);
        Button btnDefault = (Button) mRootView.findViewById(R.id.buttonCommonFooterDefault);
        Button restore = (Button) mRootView.findViewById(R.id.buttonCommonFooterRestore);
        CheckBox autoUpdate = (CheckBox) mRootView.findViewById(R.id.checkFcUpdateRealtime);
        Spinner bankSelect = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);

        mUAVObjectManagerLoaderHandler = new UAVObjectManagerLoaderHandler(getActivity(), this, autoUpdate, bankSelect, apply, save,
                btnDefault, restore, R.integer.stabi_tps_button_group);

        getLoaderManager().initLoader(0, null, mUAVObjectManagerLoaderHandler);

        return mRootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onStop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onResume();
        }
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        if (mRootView != null) {
            // Our view is created, safe to setup the gui to uavobject
            // bindings
            helper.bindTpsWidgets(mRootView.findViewById(R.id.switchEnableTPS),
                    mRootView.findViewById(R.id.spinnerThrustPidSource),
                    mRootView.findViewById(R.id.spinnerThrustPidTarget),
                    mRootView.findViewById(R.id.spinnerThrustPidAxis),
                    mRootView.findViewById(R.id.curveTps));
        }
    }
}
