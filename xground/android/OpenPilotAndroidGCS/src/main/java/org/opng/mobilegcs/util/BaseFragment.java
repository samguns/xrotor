/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import org.opng.mobilegcs.AppMenu;

public class BaseFragment extends Fragment {

    protected AppMenuItem mMenu;

    public BaseFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int id = -1;

        if (savedInstanceState != null)
            id = savedInstanceState.getInt(AppMenu.APP_MENU_ITEM, -1);

        if (id == -1) {
            Bundle args = getArguments();

            if (args != null) {
                id = args.getInt(AppMenu.APP_MENU_ITEM, -1);
            }
        }

        if (id != -1)
            mMenu = AppMenu.getMenuItem(id);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mMenu != null)
            outState.putInt(AppMenu.APP_MENU_ITEM, mMenu.getId());
        super.onSaveInstanceState(outState);
    }
}
