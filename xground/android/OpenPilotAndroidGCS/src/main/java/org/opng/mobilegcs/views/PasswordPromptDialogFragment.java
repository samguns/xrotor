/*
 * David Willis Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.views;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import org.opng.mobilegcs.R;

public class PasswordPromptDialogFragment extends DialogFragment {
    private PasswordPromptListener mListener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        // Null is valid here for dialog framgments so we suppress this warning
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.password_prompt_dialog, null);
        final EditText editUsername = (EditText) view.findViewById(R.id.editTextUser);
        final EditText editPassword = (EditText) view.findViewById(R.id.editTextPassword);
        builder.setView(view)
                .setPositiveButton(R.string.label_login, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onLoginClicked(
                                Base64.encodeToString(
                                        (editUsername.getText() + ":" + editPassword.getText())
                                                .getBytes(), Base64.NO_WRAP));
                    }
                })
                .setNegativeButton(R.string.label_login_cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mListener.onCancelLoginClicked();
                    }
                });
        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mListener = (PasswordPromptListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement PasswordPromptListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface PasswordPromptListener {
        /**
         * Implemented by hosting activity to retrieve the entered username and password
         *
         * @param authenticationString base64 encoded combination of username and password
         */
        void onLoginClicked(String authenticationString);

        /**
         * Implemented by hosting activity to handle the user cancelling the password prompt.
         */
        void onCancelLoginClicked();
    }
}
