/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.optune;

import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.stabilization.StabilizationGuiHelper;
import org.opng.mobilegcs.txpid.TxPidGuiHelper;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.mobilegcs.wizard.WizardStateMachine;
import org.opng.uavtalk.UAVObjectManager;

import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class OPTuneFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {
    final static boolean PITCH_AND_ROLL_ONLY = true;
    private final static String TAG = OPTuneFragment.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final String SAVED_STATE = "SavedState";
    private static boolean WARN = LOGLEVEL > 0;
    private final Handler mHandler = new Handler();
    private final OPTuneFactors mOPTuneFactors = new OPTuneFactors();
    private UAVObjectToGuiUtils mObjectToGuiUtils;
    private TxPidGuiHelper mTxPidGuiHelper;
    private StabilizationGuiHelper mStabiGuiHelper;
    private Button mButtonApply;
    private Button mButtonSave;
    private Switch mSwitchEnable;
    private SavedState mSavedState = new SavedState();
    private RadioGroup mRadioGroupAxes;
    private TextView mTxtPitchUov;
    private TextView mTxtRollUov;
    private TextView mTxtYawUov;
    private TextView mTxtInstructions;
    private Button mButtonNext;
    private Button mButtonPrevious;
    private Spinner mSpinnerEscType;
    private Spinner mSpinnerTuningStyle;
    private Spinner mSpinnerYawStyle;
    private WizardStateMachine mStateMachine;
    private boolean mStateMachineInitFromSaved;
    private View mRootView;

    public OPTuneFragment() {
        // Required empty public constructor
    }

    TxPidGuiHelper getTxPidGuiHelper() {
        return mTxPidGuiHelper;
    }

    RadioGroup getRadioGroupAxes() {
        return mRadioGroupAxes;
    }

    OPTuneFactors getOPTuneFactors() {
        return mOPTuneFactors;
    }

    SavedState getSavedState() {
        return mSavedState;
    }

    public StabilizationGuiHelper getStabiGuiHelper() {
        return mStabiGuiHelper;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_optune, container, false);

        mTxtInstructions = (TextView) mRootView.findViewById(R.id.txtOpTuneInstructions);

        mButtonPrevious = (Button) mRootView.findViewById(R.id.buttonPrevious);
        mButtonNext = (Button) mRootView.findViewById(R.id.buttonNext);

        if (savedInstanceState == null) {
            mStateMachine = new WizardStateMachine(new BeginOpTuneState(this),
                    mButtonPrevious, mButtonNext, mHandler);
            mSavedState.setStateMachine(mStateMachine);
            mStateMachineInitFromSaved = false;
        } else if (isAdded()) {
            mSavedState = savedInstanceState.getParcelable(SAVED_STATE);

            Spinner stabiBank = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);
            if (stabiBank != null) {
                stabiBank.setSelection(mSavedState.getStabiBank() - 1);
            }
            mStateMachine = mSavedState.getStateMachine();
            ((OPTuneWizardState) mStateMachine.getCurrentState()).setOwner(this);
            mStateMachineInitFromSaved = true;
        }
        mSpinnerEscType = (Spinner) mRootView.findViewById(R.id.spinnerEscType);
        mSpinnerTuningStyle = (Spinner) mRootView.findViewById(R.id.spinnerTuningStyle);
        mSpinnerYawStyle = (Spinner) mRootView.findViewById(R.id.spinnerYawStyle);

        initTuningStyleGui();

        mButtonApply = (Button) mRootView.findViewById(R.id.buttonOpTuneApply);
        mButtonSave = (Button) mRootView.findViewById(R.id.buttonOpTuneSave);
        mSwitchEnable = (Switch) mRootView.findViewById(R.id.switchOpTuneEnableTxPID);

        mTxtPitchUov = (TextView) mRootView.findViewById(R.id.textOpTunePitchUov);
        mTxtRollUov = (TextView) mRootView.findViewById(R.id.textOpTuneRollUov);
        mTxtYawUov = (TextView) mRootView.findViewById(R.id.textOpTuneYawUov);

        //noinspection ConstantConditions
        mRootView.findViewById(R.id.radioYaw).setVisibility(PITCH_AND_ROLL_ONLY ? View.GONE : View.VISIBLE);

        if (savedInstanceState != null) {
            restoreUOVsToGui();
        }

        mRadioGroupAxes = (RadioGroup) mRootView.findViewById(R.id.axisOpTuneRadioGroup);

        getLoaderManager().initLoader(0, null, this);

        return mRootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        enableObjectUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        disableObjectUpdates();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(SAVED_STATE, mSavedState);
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int i, Bundle bundle) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> uavObjectManagerLoader, UAVObjectManager uavObjectManager) {

        if (uavObjectManager != null) {
            // Bind widgets to uavobjects
            mObjectToGuiUtils = new UAVObjectToGuiUtils(getActivity(), uavObjectManager, uavObjectManager.getObjectUtilManager());
            mObjectToGuiUtils.addApplySaveButtons(mButtonApply, mButtonSave);

            // Create TxPID gui helper and set up controls for a single TxPID instance
            mTxPidGuiHelper = new TxPidGuiHelper(uavObjectManager, mObjectToGuiUtils, mSwitchEnable);

            mTxPidGuiHelper.bindPidWidgets(mRootView.findViewById(R.id.textPidOption),
                    mRootView.findViewById(R.id.spinnerOpTuneCtrlSource),
                    mRootView.findViewById(R.id.editTextOpTuneMin),
                    mRootView.findViewById(R.id.editTextOpTuneMax), 0);

            mTxPidGuiHelper.addEventListener(new TxPidGuiHelper.TxPidGuiHelperEvent() {
                @Override
                public void hardwareSettingsChanged() {
                    checkAndSetNextButtonEnabledState();
                }
            });

            // Create stabilization gui helper and set up controls to display PID settings
            mStabiGuiHelper = new StabilizationGuiHelper(uavObjectManager, mObjectToGuiUtils);

            Spinner stabiBank = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);
            if (stabiBank != null) {
                stabiBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        int bankNumber = i + 1;
                        mStabiGuiHelper.setBankNumber(bankNumber);
                        mSavedState.setStabiBank(bankNumber);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                stabiBank.setSelection(mSavedState.getStabiBank() - 1);
            }

            // Initialise wizard state machine from saved state here as we need to
            // ensure that mTxPidGuiHelper has been created.
            if (mStateMachineInitFromSaved) {
                mStateMachine.initAfterRestoreFromParcel(mButtonPrevious,
                        mButtonNext, mHandler);
            }

            mStabiGuiHelper.bindRollRatePidWidgets(mRootView.findViewById(R.id.textRollKp),
                    1, mRootView.findViewById(R.id.textRollKi),
                    1, mRootView.findViewById(R.id.textRollKd),
                    1);

            mStabiGuiHelper.bindPitchRatePidWidgets(mRootView.findViewById(R.id.textPitchKp),
                    1, mRootView.findViewById(R.id.textPitchKi),
                    1, mRootView.findViewById(R.id.textPitchKd),
                    1);

            mStabiGuiHelper.bindYawRatePidWidgets(mRootView.findViewById(R.id.textYawKp),
                    1, mRootView.findViewById(R.id.textYawKi),
                    1, mRootView.findViewById(R.id.textYawKd),
                    1);

            // Update widgets and objects
            mObjectToGuiUtils.populateViews();
            mObjectToGuiUtils.enableObjUpdates();

            mTxPidGuiHelper.refreshValues();
        }
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> uavObjectManagerLoader) {

    }

    private void initTuningStyleGui() {
        mOPTuneFactors.initialize(getActivity());
        initTuningStyleGuiDropdown(mSpinnerEscType,
                mOPTuneFactors.getEscTypes(), new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        mOPTuneFactors.setSelectedEscType(i);
                        checkAndSetNextButtonEnabledState();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        initTuningStyleGuiDropdown(mSpinnerTuningStyle,
                mOPTuneFactors.getTuningStyles(), new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        mOPTuneFactors.setSelectedTuningStyle(i);
                        checkAndSetNextButtonEnabledState();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
        initTuningStyleGuiDropdown(mSpinnerYawStyle,
                mOPTuneFactors.getYawStyles(), new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        mOPTuneFactors.setSelectedYawStyle(i);
                        checkAndSetNextButtonEnabledState();
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
    }

    private void initTuningStyleGuiDropdown(Spinner spinner, String[] values, AdapterView.OnItemSelectedListener listener) {
        ArrayAdapter<String> escType = new ArrayAdapter<>(getActivity(),
                android.R.layout.simple_spinner_item,
                values);
        spinner.setAdapter(escType);
        spinner.setOnItemSelectedListener(listener);
    }

    private void checkAndSetNextButtonEnabledState() {
        if (mButtonNext != null) {
            final boolean enabled = mSpinnerEscType.getSelectedItemPosition() > 0 &&
                    mSpinnerTuningStyle.getSelectedItemPosition() > 0 &&
                    mSpinnerYawStyle.getSelectedItemPosition() > 0 &&
                    mSwitchEnable.isChecked();
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mButtonNext.setEnabled(enabled);
                }
            });
        }
    }

    private void restoreUOVsToGui() {
        float uov = mSavedState.getPitchUov();

        if (uov > 0) {
            mTxtPitchUov.setText(String.format(Locale.getDefault(), "%1.4f", uov));
        }

        uov = mSavedState.getRollUov();

        if (uov > 0) {
            mTxtRollUov.setText(String.format(Locale.getDefault(), "%1.4f", uov));
        }

        uov = mSavedState.getYawUov();
        if (uov > 0) {
            mTxtYawUov.setText(String.format(Locale.getDefault(), "%1.4f", uov));
        }
    }

    void zeroKiKd() {
        if (!mSavedState.getKiKdZeroed()) {
            mSavedState.setKiKdZeroed(true);
            // Save current pitch & roll P, I & D values so we can restore them when
            // we're done tuning.
            mStabiGuiHelper.refresh();
            mSavedState.setPitchKp(mStabiGuiHelper.getPitchRateKp());
            mSavedState.setPitchKd(mStabiGuiHelper.getPitchRateKd());
            mSavedState.setPitchKi(mStabiGuiHelper.getPitchRateKi());
            mSavedState.setRollKp(mStabiGuiHelper.getRollRateKp());
            mSavedState.setRollKd(mStabiGuiHelper.getRollRateKd());
            mSavedState.setRollKi(mStabiGuiHelper.getRollRateKi());
            mSavedState.setYawKp(mStabiGuiHelper.getYawRateKp());
            mSavedState.setYawKd(mStabiGuiHelper.getYawRateKd());
            mSavedState.setYawKi(mStabiGuiHelper.getYawRateKi());

            // Zero pitch Ki & Kd, pass -1 for Kp to leave it as
            // its current value
            mStabiGuiHelper.setPitchRatePID(-1f, 0.0f, 0.0f);
            mStabiGuiHelper.setRollRatePID(-1f, 0.0f, 0.0f);
            //noinspection PointlessBooleanExpression
            if (!PITCH_AND_ROLL_ONLY) {
                mStabiGuiHelper.setYawRatePID(-1f, 0.0f, 0.0f);
            }
            if (DEBUG) Log.d(TAG, "Zeroing PIDs");
            mStabiGuiHelper.saveChangesToRam();

            // Force update of gui with zeroed PIDs
            mStabiGuiHelper.refresh();
        }
    }

    void restoreKiKd() {
        mStabiGuiHelper.setRollRatePID(-1f, mSavedState.getRollKi(), mSavedState.getRollKd());
        mStabiGuiHelper.setPitchRatePID(-1f, mSavedState.getPitchKi(), mSavedState.getPitchKd());
        //noinspection PointlessBooleanExpression
        if (!PITCH_AND_ROLL_ONLY) {
            mStabiGuiHelper.setYawRatePID(-1f, mSavedState.getYawKi(), mSavedState.getYawKd());
        }
        mStabiGuiHelper.saveChangesToRam();
    }

    void saveUovAndRestoreKp(Axis currentAxis) {
        switch (currentAxis) {
            case PITCH:
                // Save our pitch UOV value
                float pitchRateKp = mStabiGuiHelper.getPitchRateKp();
                mSavedState.setPitchUov(pitchRateKp);
                mTxtPitchUov.setText(String.format(Locale.getDefault(), "%1.4f", pitchRateKp));

                // Restore the starting pitch rate Kp value, leave Ki & Kd at zero.
                mStabiGuiHelper.setPitchRatePID(mSavedState.getPitchKp(), 0.0f, 0.0f);
                break;
            case ROLL:
                // Save our roll UOV value
                float rollRateKp = mStabiGuiHelper.getRollRateKp();
                mSavedState.setRollUov(rollRateKp);
                mTxtRollUov.setText(String.format(Locale.getDefault(), "%1.4f", rollRateKp));

                // Restore the starting roll rate Kp value, leave Ki & Kd at zero.
                mStabiGuiHelper.setRollRatePID(mSavedState.getRollKp(), 0.0f, 0.0f);
                break;
            case YAW:
                // Save our yaw UOV value
                float yawRateKp = mStabiGuiHelper.getYawRateKp();
                mSavedState.setYawUov(yawRateKp);
                mTxtYawUov.setText(String.format(Locale.getDefault(), "%1.4f", yawRateKp));

                // Restore the starting yaw rate Kp value, leave Ki & Kd at zero.
                mStabiGuiHelper.setYawRatePID(mSavedState.getYawKp(), 0.0f, 0.0f);
                break;
        }
        mStabiGuiHelper.saveChangesToRam();
    }

    void resetUovDisplay() {
        mTxtPitchUov.setText("");
        mTxtRollUov.setText("");
        mTxtYawUov.setText("");
        mSavedState.setKiKdZeroed(false);
    }

    private void enableObjectUpdates() {
        if (mObjectToGuiUtils != null) {
            mObjectToGuiUtils.enableObjUpdates();
        }
    }

    private void disableObjectUpdates() {
        if (mObjectToGuiUtils != null) {
            mObjectToGuiUtils.disableObjUpdates();
        }
    }

    void setInstructions(String instructions) {
        mTxtInstructions.setText(instructions);
    }

    enum Axis {
        PITCH,
        ROLL,
        YAW
    }

    /**
     * This interface is used to set the current owning fragment in a given wizard
     * state so that the state always has access to the most up to date fragment.
     */
    interface OPTuneWizardState {
        void setOwner(OPTuneFragment owner);
    }

    class SavedState implements Parcelable {
        public final Creator<SavedState> CREATOR = new Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel parcel) {
                return new SavedState(parcel);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
        private float _pitchKp;
        private float _pitchKi;
        private float _pitchKd;
        private float _rollKp;
        private float _rollKi;
        private float _rollKd;
        private float _yawKp;
        private float _yawKi;
        private float _yawKd;
        private float _pitchUov;
        private float _rollUov;
        private float _yawUov;
        private boolean _kiKdZeroed;
        private WizardStateMachine _stateMachine;
        private int _stabiBank = 1; // First bank, one based

        private SavedState(Parcel in) {
            _pitchKp = in.readFloat();
            _pitchKi = in.readFloat();
            _pitchKd = in.readFloat();
            _pitchUov = in.readFloat();
            _rollKp = in.readFloat();
            _rollKi = in.readFloat();
            _rollKd = in.readFloat();
            _rollUov = in.readFloat();
            _yawKp = in.readFloat();
            _yawKi = in.readFloat();
            _yawKd = in.readFloat();
            _yawUov = in.readFloat();
            _kiKdZeroed = in.readInt() == 1;
            _stabiBank = in.readInt();
            _stateMachine = in.readParcelable(getClass().getClassLoader());
        }

        public SavedState() {

        }

        public float getPitchKp() {
            return _pitchKp;
        }

        public void setPitchKp(float pitchKp) {
            _pitchKp = pitchKp;
        }

        public float getPitchKi() {
            return _pitchKi;
        }

        public void setPitchKi(float pitchKi) {
            _pitchKi = pitchKi;
        }

        public float getPitchKd() {
            return _pitchKd;
        }

        public void setPitchKd(float pitchKd) {
            _pitchKd = pitchKd;
        }

        public float getRollKp() {
            return _rollKp;
        }

        public void setRollKp(float rollKp) {
            _rollKp = rollKp;
        }

        public float getRollKi() {
            return _rollKi;
        }

        public void setRollKi(float rollKi) {
            _rollKi = rollKi;
        }

        public float getRollKd() {
            return _rollKd;
        }

        public void setRollKd(float rollKd) {
            _rollKd = rollKd;
        }

        public float getYawKp() {
            return _yawKp;
        }

        public void setYawKp(float yawKp) {
            _yawKp = yawKp;
        }

        public float getYawKi() {
            return _yawKi;
        }

        public void setYawKi(float yawKi) {
            _yawKi = yawKi;
        }

        public float getYawKd() {
            return _yawKd;
        }

        public void setYawKd(float yawKd) {
            _yawKd = yawKd;
        }

        public float getPitchUov() {
            return _pitchUov;
        }

        public void setPitchUov(float pitchRateKp) {
            _pitchUov = pitchRateKp;
        }

        public float getRollUov() {
            return _rollUov;
        }

        public void setRollUov(float rollRateKp) {
            _rollUov = rollRateKp;
        }

        public float getYawUov() {
            return _yawUov;
        }

        public void setYawUov(float yawUov) {
            _yawUov = yawUov;
        }

        public boolean getKiKdZeroed() {
            return _kiKdZeroed;
        }

        public void setKiKdZeroed(boolean kiKdZeroed) {
            _kiKdZeroed = kiKdZeroed;
        }

        public int getStabiBank() {
            return _stabiBank;
        }

        public void setStabiBank(int stabiBank) {
            _stabiBank = stabiBank;
        }

        public WizardStateMachine getStateMachine() {
            return _stateMachine;
        }

        public void setStateMachine(WizardStateMachine stateMachine) {
            _stateMachine = stateMachine;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel out, int i) {
            out.writeFloat(_pitchKp);
            out.writeFloat(_pitchKi);
            out.writeFloat(_pitchKd);
            out.writeFloat(_pitchUov);
            out.writeFloat(_rollKp);
            out.writeFloat(_rollKi);
            out.writeFloat(_rollKd);
            out.writeFloat(_rollUov);
            out.writeFloat(_yawKp);
            out.writeFloat(_yawKi);
            out.writeFloat(_yawKd);
            out.writeFloat(_yawUov);
            out.writeInt(_kiKdZeroed ? 1 : 0);
            out.writeInt(_stabiBank);
            out.writeParcelable(_stateMachine, i);
        }
    }

}
