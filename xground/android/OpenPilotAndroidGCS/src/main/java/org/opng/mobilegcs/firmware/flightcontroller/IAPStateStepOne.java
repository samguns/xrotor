/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.flightcontroller;

import android.os.AsyncTask;
import android.util.Log;

class IAPStateStepOne extends IAPState {
    private static final String TAG = IAPStateStepOne.class.getSimpleName();

    IAPStateStepOne(IAPState state) {
        super(state);
    }

    @Override
    public void process(boolean success) {
        if (!success) {
            if (DEBUG) Log.d(TAG, "Previous IAP step failed");
            mUploader.setCurrentIAPState(new IAPStateReady(this));
            finishHandleTransactionComplete();
            mUploader.fireBootProgress(Uploader.ProgressStep.FAILURE, 0);
            mUploader.fireBootFailed();
        } else {
            ProcessTask task = new ProcessTask();
            task.execute((Void[]) null);
        }
    }

    private class ProcessTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                if (DEBUG) Log.d(TAG, "Waiting for last IAP command to complete");
                Thread.sleep(600);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            setIapCommand("2233");
            mUploader.setCurrentIAPState(new IAPStateStepTwo(IAPStateStepOne.this));
            if (DEBUG) Log.d(TAG, "Sending IAP command 2233");
            mFirmwareIAPObj.updated();
            mUploader.fireBootProgress(Uploader.ProgressStep.JUMP_TO_BL, 2);

            return null;
        }
    }
}
