/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

import android.util.SparseArray;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.opng.mobilegcs.util.Variant;

public class UAVObjectField {

    private final static String TAG = UAVObjectField.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private final SparseArray<LimitStruct> mElementLimits = new SparseArray<>();
    private String mName;
    private String mUnits;
    private FieldType mType;
    private List<String> mElementNames;
    private List<String> mOptions;
    private int mNumElements;
    private int mNumBytesPerElement;
    private int mOffset;
    private UAVObject mUAVObject;
    private ByteBuffer mData;
    private UAVObjectField(String name, String units, FieldType type,
                           int numElements, List<String> options, String limits) {
        List<String> elementNames = new ArrayList<>();
        // Set element names
        for (int n = 0; n < numElements; ++n) {
            elementNames.add(String.valueOf(n));
        }
        // Initialize
        constructorInitialize(name, units, type, elementNames, options, limits);
    }

    public UAVObjectField(String name, String units, FieldType type,
                           List<String> elementNames, List<String> options, String limits) {
        constructorInitialize(name, units, type, elementNames, options, limits);
    }

    public UAVObjectField(String name, String units, FieldType type,
                          int numElements, List<String> options) {
        this(name, units, type, numElements, options, "");
    }

    public UAVObjectField(String name, String units, FieldType type,
                          List<String> elementNames, List<String> options) {
        this(name, units, type, elementNames, options, "");
    }

    public void initialize(ByteBuffer data, int offset, UAVObject obj) {
        this.mUAVObject = obj;
        this.mOffset = offset;
        this.mData = data;
        clear();
    }

    public UAVObject getObject() {
        return mUAVObject;
    }

    public FieldType getType() {
        return mType;
    }

    public String getName() {
        return mName;
    }

    public String getUnits() {
        return mUnits;
    }

    public int getNumElements() {
        return mNumElements;
    }

    public List<String> getElementNames() {
        return mElementNames;
    }

    public List<String> getOptions() {
        return mOptions;
    }

    /**
     * This function copies this field from the internal storage of the parent
     * object to a new ByteBuffer for UAVTalk. It also converts from the java
     * standard (big endian) to the arm/uavtalk standard (little endian)
     *
     * @param dataOut container for this field's data
     * @return the number of bytes added
     **/
    public synchronized int pack(ByteBuffer dataOut) {
        // Pack each element in output buffer
        dataOut.order(ByteOrder.LITTLE_ENDIAN);
        switch (mType) {
            case INT8:
            case UINT8:
            case ENUM:
                for (int index = 0; index < mNumElements; ++index) {
                    dataOut.put(mData.get(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case INT16:
            case UINT16:
                for (int index = 0; index < mNumElements; ++index) {
                    dataOut.putShort(mData.getShort(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case INT32:
            case UINT32:
                for (int index = 0; index < mNumElements; ++index) {
                    dataOut.putInt(mData.getInt(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case FLOAT32:
                for (int index = 0; index < mNumElements; ++index) {
                    dataOut.putFloat(mData.getFloat(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case STRING:
                dataOut.put(mData.get());
                break;
            case BITFIELD: {
                for (int index = 0; index < 1 + (mNumElements - 1) / 8; ++index) {
                    dataOut.put(mData.get(mOffset + (index * mNumBytesPerElement)));
                }
            }
            break;
        }

        // Done
        return getNumBytes();
    }

    public synchronized void unpack(ByteBuffer dataIn) {
        // Unpack each element from input buffer
        mData.mark();
        mData.position(mOffset);
        switch (mType) {
            case INT8:
            case UINT8:
            case ENUM:
                for (int index = 0; index < mNumElements; ++index) {
                    mData.put(dataIn.get(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case INT16:
            case UINT16:
                for (int index = 0; index < mNumElements; ++index) {
                    mData.putShort(dataIn.getShort(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case INT32:
            case UINT32:
                for (int index = 0; index < mNumElements; ++index) {
                    mData.putInt(dataIn.getInt(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case FLOAT32:
                for (int index = 0; index < mNumElements; ++index) {
                    mData.putFloat(dataIn.getFloat(mOffset + (index * mNumBytesPerElement)));
                }
                break;
            case STRING:
                mData.put(dataIn.get());
                break;
            case BITFIELD: {
                for (int index = 0; index < 1 + (mNumElements - 1) / 8; ++index) {
                    mData.put(dataIn.get(mOffset + (index * mNumBytesPerElement)));
                }
            }
            break;
        }

        mData.reset();
    }

    public Variant getValue() {
        return getValue(0);
    }

    public void setValue(Variant data) {
        setValue(data, 0);
    }

    public synchronized Variant getValue(int index) {
        Variant retVal = null;

        // Check that index is not out of bounds
        if (index >= mNumElements) {
            return null;
        }

        int location = index * mNumBytesPerElement;

        switch (mType) {
            case INT8:
                retVal = new Variant(mData.get(mOffset + location));
                break;
            case UINT8:
                retVal = new Variant(mData.get(mOffset + location), true);
                break;
            case ENUM:
                byte value = mData.get(mOffset + location);

                if (value < 0 || value > mOptions.size()) {
                    value = 0;
                }
                retVal = new Variant(mOptions.get(value));
                break;
            case INT16:
                retVal = new Variant(mData.getShort(mOffset + location));
                break;
            case UINT16:
                retVal = new Variant(mData.getShort(mOffset + location), true);
                break;
            case INT32:
                retVal = new Variant(mData.getInt(mOffset + location));
                break;
            case UINT32:
                retVal = new Variant(mData.getInt(mOffset + location), true);
                break;
            case FLOAT32:
                retVal = new Variant(mData.getFloat(mOffset + location));
                break;
            case STRING:
                byte[] stringBytes = new byte[mOffset + mNumElements - 1];
                mData.position(mOffset);
                mData.get(stringBytes);
                retVal = new Variant(new String(stringBytes));
                break;
            case BITFIELD:
                retVal = new Variant((mData.get(mOffset + (index / 8 * mNumBytesPerElement)) >> (index % 8)) & 1);
                break;
        }

        return retVal;
    }

    public void setValue(Object data) {
        setValue(new Variant(data));
    }

    public void setValue(Object data, int index) {
        setValue(new Variant(data), index);
    }

    public synchronized void setValue(Variant data, int index) {
        // Check that index is not out of bounds
        // if ( index >= numElements );
        // throw new Exception("Index out of bounds");

        int location = index * mNumBytesPerElement;

        // Get metadata
        UAVObject.Metadata mdata = mUAVObject.getMetadata();
        // Update value if the access mode permits
        if (mdata.GetGcsAccess() == UAVObject.AccessMode.ACCESS_READWRITE) {
            switch (mType) {
                case INT8:
                    mData.put(mOffset + location, data.toByte());
                    break;
                case INT16:
                    mData.putShort(mOffset + location, data.toShort());
                    break;
                case INT32:
                    mData.putInt(mOffset + location, data.toInt());
                    break;
                case UINT8:
                    mData.put(mOffset + location, data.toUByte().byteValue());
                    break;
                case UINT16:
                    mData.putShort(mOffset + location, data.toUShort().shortValue());
                    break;
                case UINT32:
                    mData.putInt(mOffset + location, data.toUInt().intValue());
                    break;
                case FLOAT32:
                    mData.putFloat(mOffset + location, data.toFloat());
                    break;
                case ENUM:
                    byte val;
                    try {
                        // Test if numeric constant passed in
                        val = data.toByte();
                    } catch (Exception e) {
                        val = (byte) mOptions.indexOf(data.toString());
                    }
                    // if(val < 0) throw new
                    // Exception("Enumerated value not found");
                    mData.put(mOffset + index, val);
                    break;
                case BITFIELD:
                    int bitfieldLocation = mOffset + (index / 8 * mNumBytesPerElement);
                    byte tempValue = mData.get(bitfieldLocation);
                    tempValue = (byte) ((tempValue & ~(1 << (index % 8))) | ((data.toUInt() != 0 ? 1 : 0) << (index % 8)));
                    mData.put(bitfieldLocation, tempValue);
                    break;
                case STRING:
                    mData.position(mOffset);
                    mData.put(data.toString().getBytes());
            }
        }
    }

    public double getDouble() {
        return getDouble(0);
    }

    public void setDouble(double value) {
        setDouble(value, 0);
    }

    public double getDouble(int index) {
        switch (mType) {
            case ENUM:
                return mData.get(mOffset + index);
            default:
                break;
        }
        return getValue(index).toDouble();
    }

    private void setDouble(double value, int index) {
        setValue(new Variant(value), index);
    }

    public int getNumBytes() {
        int retVal;
        switch (mType) {
            case BITFIELD:
                retVal = mNumBytesPerElement * (1 + (mNumElements - 1) / 8);

                break;
            default:
                retVal = mNumBytesPerElement * mNumElements;

                break;
        }

        return retVal;
    }

    @Override
    public String toString() {
        StringBuilder sout = new StringBuilder();
        sout.append(mName).append(": ");
        for (int i = 0; i < mNumElements; i++) {
            sout.append(getValue(i).toString());
            if (i != mNumElements - 1)
                sout.append(", ");
            else
                sout.append(" ");
        }
        if (mUnits.length() > 0)
            sout.append(" (").append(mUnits).append(")\n");
        else
            sout.append("\n");
        return sout.toString();
    }

    private synchronized void clear() {
        byte[] zeroBytes;
        mData.mark();
        mData.position(mOffset);

        switch (mType) {
            case BITFIELD:
                zeroBytes = new byte[mNumBytesPerElement * (1 + (mNumElements - 1) / 8)];
                break;
            default:
                zeroBytes = new byte[mNumBytesPerElement * mNumElements];
                break;
        }

        mData.put(zeroBytes);
        mData.reset();
    }

    private synchronized void constructorInitialize(String name, String units,
                                                    FieldType type, List<String> elementNames, List<String> options,
                                                    String limits) {
        // Copy params
        this.mName = name;
        this.mUnits = units;
        this.mType = type;
        this.mOptions = options;
        this.mNumElements = elementNames.size();
        this.mOffset = 0;
        this.mData = null;
        this.mUAVObject = null;
        this.mElementNames = elementNames;
        switch (type) {
            case INT8:
                mNumBytesPerElement = 1;
                break;
            case INT16:
                mNumBytesPerElement = 2;
                break;
            case INT32:
                mNumBytesPerElement = 4;
                break;
            case UINT8:
                mNumBytesPerElement = 1;
                break;
            case UINT16:
                mNumBytesPerElement = 2;
                break;
            case UINT32:
                mNumBytesPerElement = 4;
                break;
            case FLOAT32:
                mNumBytesPerElement = 4;
                break;
            case ENUM:
                mNumBytesPerElement = 1;
                break;
            case BITFIELD:
                mNumBytesPerElement = 1;
                mOptions.add("0");
                mOptions.add("1");
                break;
            case STRING:
                mNumBytesPerElement = 2;
                break;
            default:
                mNumBytesPerElement = 0;
        }
        limitsInitialize(limits);
    }

    @Override
    public UAVObjectField clone() throws CloneNotSupportedException {
        UAVObjectField newField = (UAVObjectField) super.clone();
        newField.mName = mName;
        newField.mUnits = mUnits;
        newField.mType = mType;
        newField.mElementNames = new ArrayList<>(mElementNames);
        newField.mOptions = new ArrayList<>(mOptions);
        newField.mNumElements = mNumElements;
        newField.mNumBytesPerElement = mNumBytesPerElement;
        ByteBuffer buffer = ByteBuffer.allocate(getNumBytes());
        pack(buffer);
        newField.initialize(buffer, mOffset, mUAVObject);
        return newField;
    }

    public boolean isWithinLimits(Variant var, int index, int board) {
        boolean retVal = true;
        if (mElementLimits.get(index) == null) {
            return true;
        }

        LimitStruct struc = mElementLimits.get(index);
        if ((struc.board != board) && board != 0 && struc.board != 0)
                return true;

        switch (struc.type) {
        case EQUAL:
            // Defaults to false here
            retVal = false;
            switch (mType) {
            case INT8:
            case INT16:
            case INT32:
                for (Variant vars : struc.values) {
                    if (var.toInt().equals(vars.toInt()))
                        retVal = true;
                }
                break;
            case UINT8:
            case UINT16:
            case UINT32:
            case BITFIELD:
                for (Variant vars : struc.values) {
                    if (var.toUInt().equals(vars.toUInt()))
                        retVal = true;
                }
                break;
            case ENUM:
            case STRING:
                for (Variant vars : struc.values) {
                    if (var.toString().equals(vars.toString()))
                        retVal = true;
                }
                break;
            case FLOAT32:
                for (Variant vars : struc.values) {
                    if (var.toFloat().equals(vars.toFloat()))
                        retVal = true;
                }
                break;
            default:
                return true;
            }
            break;
        case NOT_EQUAL:
            switch (mType) {
            case INT8:
            case INT16:
            case INT32:
                for (Variant vars : struc.values) {
                    if (var.toInt().equals(vars.toInt()))
                        retVal = false;
                }
                break;
            case UINT8:
            case UINT16:
            case UINT32:
            case BITFIELD:
                for (Variant vars : struc.values) {
                    if (var.toUInt().equals(vars.toUInt()))
                        retVal = false;
                }
                break;
            case ENUM:
            case STRING:
                for (Variant vars : struc.values) {
                    if (var.toString().equals(vars.toString()))
                        retVal = false;
                }
                break;
            case FLOAT32:
                for (Variant vars : struc.values) {
                    if (var.toFloat().equals(vars.toFloat()))
                        retVal = false;
                }
                break;
            default:
                return true;
            }
            break;
        case BETWEEN:
            if (struc.values.size() < 2) {
                return true;
            }
            switch (mType) {
            case INT8:
            case INT16:
            case INT32:
                if (!(var.toInt() >= struc.values.get(0).toInt() && var
                        .toInt() <= struc.values.get(1).toInt())) {
                    retVal = false;
                }
                break;
            case UINT8:
            case UINT16:
            case UINT32:
            case BITFIELD:
                if (!(var.toUInt() >= struc.values.get(0).toUInt() && var
                        .toUInt() <= struc.values.get(1).toUInt())) {
                    retVal = false;
                }
                break;
            case ENUM:
                if (!(mOptions.indexOf(var.toString()) >= mOptions
                        .indexOf(struc.values.get(0).toString()) && mOptions
                        .indexOf(var.toString()) <= mOptions
                        .indexOf(struc.values.get(1).toString()))) {
                    retVal = false;
                }
                break;
            case STRING:
                break;
            case FLOAT32:
                if (!(var.toFloat() >= struc.values.get(0).toFloat() && var
                        .toFloat() <= struc.values.get(1).toFloat())) {
                    retVal = false;
                }
                break;
            default:
                break;
            }
            break;
        case BIGGER:
            if (struc.values.size() < 1) {
                return true;
            }
            switch (mType) {
            case INT8:
            case INT16:
            case INT32:
                if (!(var.toInt() >= struc.values.get(0).toInt())) {
                    retVal = false;
                }
                break;
            case UINT8:
            case UINT16:
            case UINT32:
            case BITFIELD:
                if (!(var.toUInt() >= struc.values.get(0).toUInt())) {
                    retVal = false;
                }
                break;
            case ENUM:
                if (!(mOptions.indexOf(var.toString()) >= mOptions
                        .indexOf(struc.values.get(0).toString()))) {
                    retVal = false;
                }
                break;
            case STRING:
                break;
            case FLOAT32:
                if (!(var.toFloat() >= struc.values.get(0).toFloat())) {
                    retVal = false;
                }
                break;
            default:
                break;
            }
            break;
        case SMALLER:
            switch (mType) {
            case INT8:
            case INT16:
            case INT32:
                if (!(var.toInt() <= struc.values.get(0).toInt())) {
                    retVal = false;
                }
                break;
            case UINT8:
            case UINT16:
            case UINT32:
            case BITFIELD:
                if (!(var.toUInt() <= struc.values.get(0).toUInt())) {
                    retVal = false;
                }
                break;
            case ENUM:
                if (!(mOptions.indexOf(var.toString()) <= mOptions
                        .indexOf(struc.values.get(0).toString()))) {
                    retVal = false;
                }
                break;
            case STRING:
                break;
            case FLOAT32:
                if (!(var.toFloat() <= struc.values.get(0).toFloat())) {
                    retVal = false;
                }
                break;
            default:
                break;
            }
        }

        return retVal;
    }
    private void limitsInitialize(String limits)
{
    /// format
    /// (TY)->type (EQ-equal;NE-not equal;BE-between;BI-bigger;SM-smaller)
    /// (VALX)->value
    /// %TY:VAL1:VAL2:VAL3;%TY:VAL1:VAL2:VAL3
    /// example: first element bigger than 3 and second element inside [2.3,5]
    /// "%BI:3;%BE:2.3:5"
    if(limits.isEmpty())
        return;

    int index=0;
    List<String> ruleList= Arrays.asList(limits.split(";"));

    for(String rule: ruleList)
    {
        String str=rule.trim();
        if(str.isEmpty())
            continue;

        List<String> valuesPerElement= new LinkedList<>(Arrays.asList(str.split(":")));
        LimitStruct lstruc = new LimitStruct();
        lstruc.values = new LinkedList<>();

        boolean startFlag=valuesPerElement.get(0).startsWith("%");
        boolean maxIndexFlag= index < mNumElements;
        boolean elemNumberSizeFlag=valuesPerElement.get(0).length()==3;
        boolean aux = false;
        Integer board = -1;

        if (valuesPerElement.get(0).length() >= 4) {
            try {
                String substring = valuesPerElement.get(0).substring(1, 4);
                board = Integer.parseInt(substring, 16);
                aux = true;
            } catch (NumberFormatException ignored) {
            }
        }
        boolean b4=((valuesPerElement.get(0).length())==7 && aux);
        if(startFlag && maxIndexFlag && (elemNumberSizeFlag || b4))
        {
            if(b4)
                lstruc.board= board;
            else
                lstruc.board=0;
            if(valuesPerElement.get(0).endsWith("EQ"))
                lstruc.type= LimitType.EQUAL;
            else if(valuesPerElement.get(0).endsWith("NE"))
                lstruc.type= LimitType.NOT_EQUAL;
            else if(valuesPerElement.get(0).endsWith("BE"))
                lstruc.type= LimitType.BETWEEN;
            else if(valuesPerElement.get(0).endsWith("BI"))
                lstruc.type= LimitType.BIGGER;
            else if(valuesPerElement.get(0).endsWith("SM"))
                lstruc.type= LimitType.SMALLER;
            //else
//                    if(DEBUG)Log.d(TAG, "limits parsing failed (invalid property) on UAVObjectField" + _name);
            valuesPerElement.remove(0);
            for(String _value: valuesPerElement)
            {
                String value=_value.trim();
                switch (mType)
                {
                case UINT8:
                case UINT16:
                case UINT32:
                case BITFIELD:
                    lstruc.values.add(new Variant(Integer.valueOf(value), true));
                    break;
                case INT8:
                case INT16:
                case INT32:
                    lstruc.values.add(new Variant(Long.valueOf(value)));
                    break;
                case FLOAT32:
                    lstruc.values.add(new Variant(Float.valueOf(value)));
                    break;
                case ENUM:
                    lstruc.values.add(new Variant(value));
                    break;
                case STRING:
                    lstruc.values.add(new Variant(value));
                    break;
                default:
                    lstruc.values.add(Variant.emptyVariant());
                }
            }
            mElementLimits.put(index, lstruc);
            ++index;
        }
    }
}

    public Variant getMaxLimit(int index, int board) {
        Variant retVal = Variant.emptyVariant();
        
        if (mElementLimits.get(index) == null) {
            return retVal;
        }
        LimitStruct struc = mElementLimits.get(index);

        if ((struc.board != board) && board != 0 && struc.board != 0)
            return retVal;

        switch (struc.type) {
        case BETWEEN:
            retVal = struc.values.get(1);
            break;
        case SMALLER:
            retVal = struc.values.get(0);
            break;
        case EQUAL:
        case NOT_EQUAL:
        case BIGGER:
        default:
            break;
        }

        return retVal;
    }

    public Variant getMinLimit(int index, int board) {
        Variant retVal = Variant.emptyVariant();
        
        if (mElementLimits.get(index) == null) {
            return retVal;
        }
        LimitStruct struc = mElementLimits.get(index);
        if ((struc.board != board) && board != 0 && struc.board != 0)
            return retVal;

        switch (struc.type) {
        case BETWEEN:
            retVal = struc.values.get(0);
            break;
        case BIGGER:
            retVal = struc.values.get(0);
            break;
        case EQUAL:
        case NOT_EQUAL:
        case SMALLER:
        default:
            break;
        }
        return retVal;
    }

    public enum FieldType {
        INT8, INT16, INT32, UINT8, UINT16, UINT32, FLOAT32, ENUM, BITFIELD, STRING
    }

    public enum LimitType {
        EQUAL, NOT_EQUAL, BETWEEN, BIGGER, SMALLER
    }

    public class LimitStruct {
        LimitType type;
        List<Variant> values;
        int board;
    }
}
