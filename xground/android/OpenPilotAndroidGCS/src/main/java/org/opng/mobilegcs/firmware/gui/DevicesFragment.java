/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.firmware.gui;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.firmware.flightcontroller.DeviceInfo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class DevicesFragment extends Fragment {

    private static final String ARG_DEVICE_INFOS = "DeviceInfos";
    private List<DeviceInfo> mDeviceInfoList;
    private DevicesRecyclerViewAdapter mDevicesRecyclerViewAdapter;

    public DevicesFragment() {
        // Required empty public constructor
    }

    public static DevicesFragment newInstance(ArrayList<DeviceInfo> deviceInfoList) {
        DevicesFragment devicesFragment = new DevicesFragment();

        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_DEVICE_INFOS, deviceInfoList);
        devicesFragment.setArguments(args);

        return devicesFragment;
    }

    public void setItemClickedCallback(ItemClickedCallback callback) {
        if (mDevicesRecyclerViewAdapter != null) {
            mDevicesRecyclerViewAdapter.setItemClickedCallback(callback);
        }
    }

    public DeviceInfo getSelectedDevice() {
        return mDevicesRecyclerViewAdapter != null ? mDevicesRecyclerViewAdapter.getSelectedDevice() : null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mDeviceInfoList = getArguments().getParcelableArrayList(ARG_DEVICE_INFOS);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_devices, container, false);
        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.device_list);
        initRecyclerView(recyclerView);

        return rootView;
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(recyclerView.getContext());
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        recyclerView.setLayoutManager(layoutManager);
        mDevicesRecyclerViewAdapter = new DevicesRecyclerViewAdapter(mDeviceInfoList);
        recyclerView.setAdapter(mDevicesRecyclerViewAdapter);
    }

    interface ItemClickedCallback {
        void itemClicked(DeviceInfo clickedDeviceInfo);
    }

    private static class DevicesRecyclerViewAdapter extends RecyclerView.Adapter<DevicesRecyclerViewAdapter.ViewHolder> {
        private final List<DeviceInfo> mDeviceInfoList;
        private int mSelectedPosition;
        private ItemClickedCallback mCallback;

        public DevicesRecyclerViewAdapter(List<DeviceInfo> deviceInfoList) {
            mDeviceInfoList = deviceInfoList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_device, parent, false);

            view.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {
                    int oldSelectedPosition = mSelectedPosition;
                    mSelectedPosition = ((ViewHolder) view.getTag()).getItemPosition();
                    if (mCallback != null) {
                        mCallback.itemClicked(mDeviceInfoList.get(mSelectedPosition));
                    }

                    // This should cause the update of the previous selected item
                    // and the new selected item.
                    notifyItemChanged(oldSelectedPosition);
                    notifyItemChanged(mSelectedPosition);
                }
            });

            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (mDeviceInfoList != null) {
                holder.bind(mDeviceInfoList.get(position));

                if (position == mSelectedPosition) {
                    ((CardView) holder.itemView).setCardElevation(R.dimen.fragment_card_elevation_selected);
                } else {
                    ((CardView) holder.itemView).setCardElevation(R.dimen.fragment_card_elevation);
                }
                holder.itemView.setTag(holder);
            }
        }

        @Override
        public int getItemCount() {
            return mDeviceInfoList != null ? mDeviceInfoList.size() : 0;
        }

        public DeviceInfo getSelectedDevice() {
            return mDeviceInfoList.get(mSelectedPosition);
        }

        public void setItemClickedCallback(ItemClickedCallback callback) {
            mCallback = callback;
        }

        public static class ViewHolder extends RecyclerView.ViewHolder {
            private final TextView mDeviceId;
            private final TextView mHardwareRev;
            private final TextView mSerialNumber;
            private final TextView mBLVersion;
            private final TextView mCodeSize;
            private final TextView mCrc;
            private final ImageView mDeviceImage;

            public ViewHolder(View itemView) {
                super(itemView);

                mDeviceId = (TextView) itemView.findViewById(R.id.textDeviceID);
                mHardwareRev = (TextView) itemView.findViewById(R.id.textDeviceHardwareVersion);
                mSerialNumber = (TextView) itemView.findViewById(R.id.textDeviceSerial);
                mBLVersion = (TextView) itemView.findViewById(R.id.textDeviceBLVersion);
                mCodeSize = (TextView) itemView.findViewById(R.id.textDeviceFWCodeSize);
                mCrc = (TextView) itemView.findViewById(R.id.textDeviceFWCRC);
                mDeviceImage = (ImageView) itemView.findViewById(R.id.imageViewDevice);
            }

            public int getItemPosition() {
                return getAdapterPosition();
            }

            public void bind(DeviceInfo deviceInfo) {
                mDeviceId.setText(String.format("%#4x", deviceInfo.getDeviceId()));
                mHardwareRev.setText(String.format(Locale.getDefault(), "%d", deviceInfo.getHardwareRev()));
                mSerialNumber.setText(deviceInfo.getSerialNumber());
                mBLVersion.setText(deviceInfo.getBLVersion());
                mCodeSize.setText(String.format(Locale.getDefault(), "%d", deviceInfo.getFWCodeSize()));
                mCrc.setText(String.format("%#x", deviceInfo.getFWCrc()));
                mDeviceImage.setImageResource(deviceInfo.getImageResId());
            }
        }
    }
}
