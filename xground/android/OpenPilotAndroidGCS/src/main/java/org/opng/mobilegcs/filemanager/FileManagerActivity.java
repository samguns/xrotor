/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.filemanager;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.opng.mobilegcs.R;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileManagerActivity extends AppCompatActivity implements FileManagerFragment.OnFileManagerInteractionListener {
    /**
     * Gives access to the selected files from the FILE_MANAGER_RESULT_DATA bundle.
     */
    public static final String FILE_MANAGER_BUNDLE_SELECTED_FILES_KEY = "SelectedFiles";

    /**
     * Used to get the bundle containing the result of the file manager operation by
     * calling Intent.getBundleExtra.
     */
    public static final String FILE_MANAGER_RESULT_DATA = "org.opng.mobilegcs.ResultData";

    /**
     * Key for bundle containing configuration options for the file manager. Use this when
     * creating an intent to launch this activity.
     */
    public static final String FILE_MANAGER_OPEN = "org.opng.mobilegcs.Open";

    /**
     * Key in FILE_MANAGER_OPEN bundle for boolean value setting whether or not we are only
     * browsing directories
     */
    public static final String FILE_MANAGER_DIR_ONLY = "paramDirOnly";

    /**
     * Key in FILE_MANAGER_OPEN bundle for boolean value setting whether or not we are
     * using the file manager to save.
     */
    public static final String FILE_MANAGER_IS_FOR_SAVE = "paramIsForSave";

    /**
     * Key in FILE_MANAGER_OPEN bundle for string value setting the initial directory.
     */
    public static final String FILE_MANAGER_START_DIR = "paramStartDir";

    /**
     * Key in FILE_MANAGER_OPEN bundle for string value describing file filters.
     */
    public static final String FILE_MANAGER_FILE_FILTER = "paramFileFilter";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_manager);

        Bundle bundle = getIntent().getBundleExtra(FILE_MANAGER_OPEN);
        boolean isDirectoryOnly = bundle.getBoolean(FILE_MANAGER_DIR_ONLY, false);
        boolean isForSave = bundle.getBoolean(FILE_MANAGER_IS_FOR_SAVE, false);
        String startDir = bundle.getString(FILE_MANAGER_START_DIR, getFilesDir().getAbsolutePath());
        String fileFilter = bundle.getString(FILE_MANAGER_FILE_FILTER, "");

        FileManagerFragment fileManagerFragment = FileManagerFragment.newInstance(isDirectoryOnly, isForSave, startDir, fileFilter);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragmentFileManager, fileManagerFragment).commit();
    }

    @Override
    public void onUserSelectedFile(List<File> files) {
        Bundle bundle = new Bundle();
        ArrayList<String> paths = new ArrayList<>();
        for (File curFile : files) {
            paths.add(curFile.getAbsolutePath());
        }

        bundle.putStringArrayList(FILE_MANAGER_BUNDLE_SELECTED_FILES_KEY, paths);
        Intent intent = new Intent("org.opng.mobilegcs.filemanager.FileSelected");
        intent.putExtra(FILE_MANAGER_RESULT_DATA, bundle);
        setResult(Activity.RESULT_OK, intent);
        finish();
    }

    @Override
    public void onUserCancelled() {
        setResult(Activity.RESULT_CANCELED);
        finish();
    }
}
