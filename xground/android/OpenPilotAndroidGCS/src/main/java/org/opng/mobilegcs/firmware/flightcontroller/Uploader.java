/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.flightcontroller;

import android.content.Context;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.drivers.cdc.CdcSerialDriver;
import org.opng.mobilegcs.firmware.UploaderEventsListener;
import org.opng.mobilegcs.io.IAP;
import org.opng.mobilegcs.io.IAPEventListener;
import org.opng.mobilegcs.io.RawSerialIO;
import org.opng.mobilegcs.io.UsbConnectionHandler;
import org.opng.mobilegcs.preferences.AndroidGCSPrefs;
import org.opng.mobilegcs.util.TelemetryServiceHandler;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObjectManager;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Uploader {

    private static final String TAG = Uploader.class.getSimpleName();
    private static final int LOGLEVEL = 3;
    public static final boolean WARN = LOGLEVEL > 1;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean ERROR = LOGLEVEL > 0;
    private static final int DEBUG_POLL_INTERVAL = 250;
    private final AndroidGCSPrefs mAndroidGCSPrefs;
    private final Context mContext;
    private final List<UploaderEventsListener> mUploaderEventsListeners = new CopyOnWriteArrayList<>();
    private final TelemetryServiceHandler mTelemetryServiceHandler;
    private final Handler mHandler;
    private UAVObjectManager mUavObjectManager;
    private CdcSerialDriver.CdcSerialPort mSerialPort;
    private CdcSerialDriver.CdcSerialPort mDebugPort;
    private IAP mIAP;
    private IAPState mCurrentIAPState;
    private boolean mIsResetOnly;
    private boolean mUsingOwnConnection;
    private IAPState.UAVObjectEventHandler mListener;
    private UAVDataObject mFlight;
    private UAVDataObject mFirmwareIap;
    private ScheduledExecutorService mDebugPortReader;
    private final UsbConnectionHandler mUsbConnectionHandler = new UsbConnectionHandler(new UsbConnectionHandler.UsbConnectionHandlerCallback() {
        @Override
        public boolean validated() {
            return true;
        }

        @Override
        public boolean connectToDevice(UsbDevice device, UsbManager usbManager, boolean allowedDevice) {
            boolean retVal = false;
            if (allowedDevice) {
                retVal = iapInit(device, usbManager);
            }
            return retVal;
        }

        @Override
        public void disconnectDevice() {
            disconnect();
        }
    });
    private byte[] mDescription;

    public Uploader(Context context) {
        mHandler = new Handler(Looper.getMainLooper());
        mAndroidGCSPrefs = new AndroidGCSPrefs(context);
        mContext = context;
        mTelemetryServiceHandler = new TelemetryServiceHandler(new TelemetryServiceHandler.ConnectionListener() {
            @Override
            public void onConnect(UAVObjectManager objectManager) {
                onTelemetryConnect(objectManager);
            }

            @Override
            public void onDisconnect() {
                onTelemetryDisconnect();
            }
        });

        // Bind to telemetry service but don't connect if it's not already connected
        mTelemetryServiceHandler.bindToTelemetryService(context, false);
    }

    void setCurrentIAPState(IAPState currentIAPState) {
        mCurrentIAPState = currentIAPState;
    }

    public List<IAP.Device> getDevices() {
        return mIAP != null ? mIAP.getDevices() : new ArrayList<IAP.Device>();
    }

    public String getSerialNumber() {
        return mSerialPort != null ? mSerialPort.getSerial() : "No Device";
    }

    public Context getContext() {
        return mContext;
    }

    public boolean isTelemetryConnected() {
        return mTelemetryServiceHandler.isTelemetryConnected();
    }

    public void connectTelemetry(){
        mTelemetryServiceHandler.connect();
    }

    /**
     * Used to connect with a USB device if we are not connected via the
     * telemetry service. Use this method if board is in a state that prevents
     * it from running telemetry. This will connect directly to the bootloader
     * and can only be used via USB.
     */
    public void connectToBootloaderWithUsb() {
        mUsbConnectionHandler.initUsb(mContext);
        mUsingOwnConnection = true;
    }

    /**
     * Put the device with the given ID's bootloader into IAP mode.
     *
     * @param device device whose bootloader is to be started in IAP mode
     * @return true if we entered IAP mode, false otherwise
     */
    public boolean enterIap(int device) {
        return mIAP != null && mIAP.enterIAP(device);
    }

    /**
     * Upload firmware to a board booted into IAP mode. Firmware is read from the given
     * filename.
     *
     * @param filename path to firmware to upload
     * @param device   device to upload firmware to
     * @param verify   true to verify uploaded firmware
     * @return true if firmware was uploaded successfully, false otherwise
     */
    public boolean uploadFirmwareFromFilename(String filename, int device, boolean verify) {
        return mIAP != null && mIAP.uploadFirmware(filename, device, verify);
    }

    /**
     * Upload firmware to a board booted into IAP mode. Firmware is retrieved via the given
     * resource ID.
     *
     * @param resourceId id of a raw resource containing the firmware to upload
     * @param device     id of device to upload to
     * @param verify     true to verify uploaded firmware
     * @return true if firmware was uploaded successfully, false otherwise
     */
    public boolean uploadFirmwareFromResourceId(int resourceId, int device, boolean verify) {
        boolean retVal;
        mDescription = null;

        if (mIAP != null) {
            ByteArrayOutputStream outStream = null;
            BufferedInputStream inputStream = null;
            try {
                byte[] buffer = new byte[1024];
                inputStream = new BufferedInputStream(mContext.getResources().openRawResource(resourceId));
                outStream = new ByteArrayOutputStream();
                int bytesRead = inputStream.read(buffer);
                while (bytesRead > -1) {
                    outStream.write(buffer, 0, bytesRead);
                    bytesRead = inputStream.read(buffer);
                }

                byte[] outStreamBytes = outStream.toByteArray();
                int fileSize = outStreamBytes.length;
                mDescription = Arrays.copyOfRange(outStreamBytes, fileSize - 100, fileSize);

                // Pad file to four byte boundary
                if (fileSize % 4 != 0) {
                    int pad = fileSize / 4;
                    ++pad;
                    pad = pad * 4;
                    pad = pad - fileSize;
                    byte[] paddingBuffer = new byte[pad];
                    Arrays.fill(paddingBuffer, (byte) 255);
                    outStream.write(paddingBuffer);
                }
            } catch (IOException e) {
                e.printStackTrace();
                if (DEBUG) Log.d(TAG, "Can't open file");
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (outStream != null) {
                    try {
                        outStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            fireUpdateOperationProgress(mContext.getString(R.string.message_uploader_flash_firmware));
            retVal = mIAP.uploadFirmware(ByteBuffer.wrap(outStream.toByteArray()), device, verify);

        } else {
            retVal = false;
        }

        return retVal;
    }

    public boolean resetDevice() {
        return mIAP != null && mIAP.resetDevice();
    }

    /**
     * Call this to disconnect USB from a device attached via a call to
     * connectToBootloaderWithUsb.
     */
    public void disconnectUsb() {
        disconnect();
    }

    /**
     * Used to restart the flight controller in IAP mode. If successful, the
     * device flashing GUI will be displayed. This requires an active
     * telemetry connection.
     */
    public void systemHalt() {
        if (isDisarmed()) {
            mCurrentIAPState = new IAPStateReady(this);
            gotoBootloader(true);
        } else {
            mTelemetryServiceHandler.toastMessage(mContext.getString(R.string.message_no_halt_when_armed));
        }
    }

    public void systemReset() {
        if (isDisarmed()) {
            mCurrentIAPState = new IAPStateReady(this);
            mIsResetOnly = true;
            if (mIAP != null) {
                mIAP = null;
            }

            gotoBootloader(true);
        } else {
            mTelemetryServiceHandler.toastMessage(mContext.getString(R.string.message_no_reset_when_armed));
        }
    }

    /**
     * Reboot the flight controller with user settings. This requires an active
     * telemetry connection via any of the supported interfaces.
     */
    public void systemBoot() {
        commonSystemBoot(false, false);
    }

    /**
     * Reboot the flight controller with default settings. This requires an active
     * telemetry connection via any of the supported interfaces.
     */
    public void systemSafeBoot() {
        commonSystemBoot(true, false);
    }

    /**
     * Make sure this is only called after use confirmation as it will erase
     * the board. This requires an active telemetry connection via any of the
     * supported interfaces.
     */
    public void systemEraseBoot() {
        commonSystemBoot(true, true);
    }

    public void addUploaderEventListener(UploaderEventsListener listener) {
        mUploaderEventsListeners.add(listener);
    }

    public void removeUploaderEventListener(UploaderEventsListener listener) {
        mUploaderEventsListeners.remove(listener);
    }

    public void cleanup() {
        if (mUsingOwnConnection) {
            disconnect();
            mUsingOwnConnection = false;
        }
        mTelemetryServiceHandler.unbindFromTelemetryService(mContext);
    }

    /**
     * Used to connect to the flight controller via USB in the absence of an
     * existing connection.
     *
     * @param device     USB device to connect via
     * @param usbManager USB manager used to open connection
     * @return true if the connection was opened, false otherwise
     */
    private boolean connectToCDCSerialDevice(UsbDevice device, UsbManager usbManager) {
        boolean retVal = true;

        // TODO: Handle two CDC interfaces. See opng/flight/pios/common/pios_usb_desc_cdc_only.c for
        // USB descriptors.

        CdcSerialDriver cdcSerialDriver = new CdcSerialDriver(usbManager, device);

        // We don't hang onto the device connection here as it will be managed by
        // the serial driver.
        UsbDeviceConnection usbDeviceConnection = usbManager.openDevice(device);
        if (usbDeviceConnection == null) {
            return false;
        }

        int comBaudSpeed = mAndroidGCSPrefs.getComBaudSpeed();

        try {
            final List<CdcSerialDriver.CdcSerialPort> ports = cdcSerialDriver.getPorts();
            mSerialPort = ports.get(0);
            mSerialPort.open();
            cdcSerialDriver.setPortParameters(comBaudSpeed, CdcSerialDriver.DATA_BITS_8, CdcSerialDriver.STOP_BITS_1, CdcSerialDriver.PARITY_NONE);
            cdcSerialDriver.setDtr(true);

//            if(ports.size() > 1){
//                mDebugPort = ports.get(1);
//                mDebugPort.open(usbDeviceConnection);
//            }

        } catch (IOException e1) {
            if (mSerialPort != null) {
                mSerialPort.close();
            }
            e1.printStackTrace();
            retVal = false;
        }

        return retVal;
    }

    private void initDebugPortReader() {
        mDebugPortReader = Executors.newScheduledThreadPool(1);
        mDebugPortReader.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                readDebugPort();
            }
        }, DEBUG_POLL_INTERVAL, DEBUG_POLL_INTERVAL, TimeUnit.MILLISECONDS);
    }

    private void readDebugPort() {
//        byte[] readBuffer = new byte[64];
//        try {
//            mDebugPort.read(readBuffer, 250);
//            fireDebugMessage(new String(readBuffer));
//        } catch (IOException e) {
//            fireDebugMessage(e.getMessage());
//        }
    }

    private void disconnect() {
        if (DEBUG) Log.d(TAG, "Uploader disconnecting");
        mUsbConnectionHandler.disconnect(mContext);

        if (mSerialPort != null) {
            mSerialPort.close();
            mSerialPort = null;
        }

        if (mDebugPort != null) {
            mDebugPortReader.shutdownNow();
            mDebugPortReader = null;

            if (mDebugPort != null) {
                mDebugPort.close();
            }
            mDebugPort = null;

        }
    }

    /**
     * This overload will create a new IAP instance that uses an existing telemetry connection.
     *
     * @return true if the bootloader enters IAP mode, false otherwise
     */
    boolean iapInit() {
        boolean retVal = true;

        if (mIAP == null) {
            mIAP = new IAP(mTelemetryServiceHandler, mContext);
            mIAP.addIAPEventListener(new UploaderIAPEventListener());
        }

        mIAP.endOperation();
        Log.d(TAG, "Finding devices");
        Log.d(TAG, "Entering IAP mode");
        if (!mIAP.findDevices() || !mIAP.enterIAP(0) || mIAP.statusRequest() != IAP.Status.IAP_IDLE) {
//            fireBootProgress(ProgressStep.FAILURE, 0);
            mIAP = null;
            retVal = false;
        }

        return retVal;
    }

    /**
     * This overload creates a new IAP instance that requires a USB connection to
     * be opened with a call to connectToBootloaderWithUsb.
     *
     * @param device     usb device for connected hardware
     * @param usbManager usb manager used to open device
     * @return true if the bootloader enters IAP mode, false otherwise
     */
    private boolean iapInit(UsbDevice device, UsbManager usbManager) {
        boolean retVal = device != null && usbManager != null;
        if (retVal) {
            if (DEBUG) Log.d(TAG, "connectToDeviceInterface: CDC Serial");
            retVal = connectToCDCSerialDevice(device, usbManager);
            if (retVal) {
                if (mIAP == null) {
                    mIAP = new IAP(new RawSerialIO() {

                        @Override
                        public int rawWriteData(ByteBuffer dataOut) {
                            int retVal = -1;
                            try {
                                if (mSerialPort != null) {
                                    retVal = mSerialPort.write(dataOut, 200);
                                }
                            } catch (IOException e) {
                                Log.d(TAG, e.getMessage());
                            }

                            return retVal;
                        }

                        @Override
                        public int rawReadData(ByteBuffer dataIn) {
                            int retVal = -1;

                            try {
                                if (mSerialPort != null) {
                                    retVal = mSerialPort.read(dataIn, 200);
                                }
                            } catch (IOException e) {
                                Log.d(TAG, e.getMessage());
                            }

                            return retVal;
                        }
                    }, mContext);
                    mIAP.addIAPEventListener(new UploaderIAPEventListener());
                }

                Log.d(TAG, IAP.statusToString(mIAP.statusRequest()));
                mIAP.endOperation();
                Log.d(TAG, "Finding devices");
                mIAP.findDevices();
                Log.d(TAG, "Entering IAP mode");

                if (!mIAP.enterIAP(0) || mIAP.statusRequest() != IAP.Status.IAP_IDLE) {
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            mTelemetryServiceHandler.toastMessage(mContext.getString(R.string.message_uploader_boot_fail));
                        }
                    });
                    mIAP = null;
                    retVal = false;
                    fireBootFailed();
                } else {
                    Log.d(TAG, "Boot succeeded: " + IAP.statusToString(mIAP.statusRequest()));
                    fireBootSucceeded();
//                    initDebugPortReader();
                }
            }
        }
        return retVal;
    }

    private void onTelemetryConnect(UAVObjectManager objectManager) {
        mUavObjectManager = objectManager;
    }

    private void onTelemetryDisconnect() {
        mUavObjectManager = null;
    }

    private UAVDataObject getFlight() {
        if (mFlight == null && mUavObjectManager != null) {
            mFlight = (UAVDataObject) mUavObjectManager.getObject("FlightStatus");
        }

        return mFlight;
    }

    UAVDataObject getFirmwareIap() {
        if (mFirmwareIap == null && mUavObjectManager != null) {
            mFirmwareIap = (UAVDataObject) mUavObjectManager.getObject("FirmwareIAPObj");
        }

        return mFirmwareIap;
    }

    private boolean isDisarmed() {
        final UAVDataObject flight = getFlight();
        return flight != null && flight.getField("Armed").getValue().toString().equals("Disarmed");
    }

    void gotoBootloader(boolean success) {
        mCurrentIAPState.process(success);
    }

    private void commonSystemBoot(boolean safeBoot, boolean erase) {
        if (mTelemetryServiceHandler.isTelemetryConnected()) {
            mTelemetryServiceHandler.suspendTelemetry();

            boolean failedToBoot = false;

            // Check to see if we're already in IAP mode
            if (mIAP == null || mIAP.getDevices().isEmpty()) {
                // Not already in IAP mode so restart
                if (iapInit()) {
                    fireBootProgress(ProgressStep.BOOTING, 0);
                } else {
                    failedToBoot = true;
                    fireBootFailed();
                }
            }
            if (!failedToBoot) {
                mIAP.jumpToApp(safeBoot, erase);
                mCurrentIAPState = new IAPStateReady(this);
                mIAP = null;
                fireBootProgress(ProgressStep.SUCCESS, 0);
                fireRunningFlightCode();
                resumeTelemetry();
            }
        }
    }

    void suspendTelemetry() {
        mTelemetryServiceHandler.suspendTelemetry();
    }

    private void resumeTelemetry() {
        mTelemetryServiceHandler.resumeTelemetry();
    }

    void fireBootProgress(ProgressStep progressStep, int progress) {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.bootProgress(progressStep, progress);
        }
    }

    void fireBootSucceeded() {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.bootSucceeded();
        }
    }

    void fireBootFailed() {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.bootFailed();
        }
    }

    private void fireUpdateProgress(int progress) {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.updateProgress(progress);
        }
    }

    private void fireDownloadFinished(IAP.Status status) {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.downloadFinished(status);
        }
    }

    private void fireUploadFinished(IAP.Status status) {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.uploadFinished(status);
        }
    }

    private void fireUpdateOperationProgress(String status) {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.updateOperationProgress(status);
        }
    }

    void fireDebugMessage(String message) {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.debugMessage(message);
        }
    }

    private void fireRunningFlightCode() {
        for (UploaderEventsListener curListener : mUploaderEventsListeners) {
            curListener.runningFlightCode();
        }
    }

    public boolean isResetOnly() {
        return mIsResetOnly;
    }

    public void setIsResetOnly(boolean isResetOnly) {
        mIsResetOnly = isResetOnly;
    }

    /**
     * Gives the IAP state machine access to the firmware IAP object's transaction
     * events listener.
     *
     * @return the transaction events listener
     */
    IAPState.UAVObjectEventHandler getListener() {
        return mListener;
    }

    /**
     * This is used by the IAP state machine to retain the listener used to
     * listen to transaction completed events on the firmware IAP object.
     *
     * @param listener transaction event listener to retain
     */
    void setListener(IAPState.UAVObjectEventHandler listener) {
        mListener = listener;
    }

    public enum ProgressStep {
        WAITING_DISCONNECT,
        WAITING_CONNECT,
        JUMP_TO_BL,
        LOADING_FW,
        UPLOADING_FW,
        UPLOADING_DESC,
        BOOTING,
        BOOTING_AND_ERASING,
        SUCCESS,
        FAILURE
    }

    /**
     * Listener for IAP events. Simply re-fires the corresponding Uploader
     * events.
     */
    private class UploaderIAPEventListener implements IAPEventListener {
        @Override
        public void progressUpdated(int progress) {
            fireUpdateProgress(progress);
        }

        @Override
        public void downloadFinished(IAP.Status status) {
            if (mIAP != null) {
                mIAP.resetDevice();
            }
            fireDownloadFinished(status);
        }

        @Override
        public void uploadFinished(IAP.Status status, byte[] description) {
            IAP.Status returnedStatus = status;
            if (returnedStatus == IAP.Status.LAST_OPERATION_SUCCESS) {
                fireUpdateOperationProgress(mContext.getString(R.string.message_uploader_flash_description));

                // If we uploaded firmware from an embedded resource, we will have our
                // description in mDescription. As uploading from an external file uses
                // a different code path, the description will be provided by the description
                // parameter.
                if (mDescription != null) {
                    returnedStatus = mIAP.uploadDescription(ByteBuffer.wrap(mDescription));
                } else if (description != null) {
                    returnedStatus = mIAP.uploadDescription(ByteBuffer.wrap(description));
                }
            }
            fireUploadFinished(returnedStatus);
        }

        @Override
        public void operationProgress(String status) {
            fireUpdateOperationProgress(status);
        }
    }
}
