/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

public class UAVObjectEventAdapter implements UAVObjectEventListener {

    @Override
    public void objectUpdated(UAVObject obj) {
    }

    @Override
    public void objectUpdatedAuto(UAVObject obj) {
    }

    @Override
    public void objectUpdatedManual(UAVObject obj, boolean all) {
    }

    @Override
    public void objectUpdatedPeriodic(UAVObject obj) {
    }

    @Override
    public void objectUnpacked(UAVObject obj) {
    }

    @Override
    public void updateRequested(UAVObject obj, boolean all) {
    }

    @Override
    public void transactionCompleted(UAVObject obj, boolean success) {
    }

    @Override
    public void newInstance(UAVObject obj) {
    }

}
