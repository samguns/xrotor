/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.firmware.downloader.bamboo.gui;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.preference.PreferenceManager;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.firmware.downloader.bamboo.Artifact;
import org.opng.mobilegcs.firmware.downloader.bamboo.Branch;
import org.opng.mobilegcs.firmware.downloader.bamboo.Plan;
import org.opng.mobilegcs.tasks.bamboo.FetchArtifactDataTask;
import org.opng.mobilegcs.tasks.bamboo.FetchBranchDataFromPlanTask;
import org.opng.mobilegcs.tasks.bamboo.FetchPlanDataTask;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link BambooDownloadFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class BambooDownloadFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private EditText mEditFileSavePath;
    private Spinner mBranchesSpinner;
    private ListView mFileListView;
    private ProgressBar mProgressBar;
    private Branch mNextBranch;
    private ArrayAdapter<Artifact> mEmptyBranchArrayAdapter;

    public BambooDownloadFragment() {
        // Required empty public constructor
    }

    public String getFileSavePath() {
        return mEditFileSavePath != null ? mEditFileSavePath.getText().toString() : "";
    }

    public void setFileSavePath(String path) {
        if (mEditFileSavePath != null) {
            mEditFileSavePath.setText(path);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_bamboo_download, container, false);

        Button browseButton = (Button) rootView.findViewById(R.id.buttonBrowse);
        browseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null) {
                    // Need to call out to the hosting activity as we need it to
                    // launch the file browser activity and get its result
                    mListener.onBrowseClicked();
                }
            }
        });

        Button downloadButton = (Button) rootView.findViewById(R.id.buttonDownload);
        downloadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadSelectedFiles();
            }
        });

        mFileListView = (ListView) rootView.findViewById(R.id.listViewFiles);
        mFileListView.setItemsCanFocus(false);
        mFileListView.setChoiceMode(AbsListView.CHOICE_MODE_MULTIPLE);
        mProgressBar = (ProgressBar) rootView.findViewById(R.id.progressBar);
        initBranchesSpinner(rootView);
        initFileSave(rootView);

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }

        mEmptyBranchArrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_list_item_1, new ArrayList<Artifact>());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initFileSave(View view) {
        mEditFileSavePath = (EditText) view.findViewById(R.id.editTextSavePath);

        // Create firmware directory if necessary and use it as our starting
        // directory in the file manager.
        File startPath = new File(getActivity().getFilesDir(), "firmware");

        boolean startPathExists = true;
        if (!startPath.exists()) {
            startPathExists = startPath.mkdir();
        }

        mEditFileSavePath.setText(startPathExists ? startPath.getAbsolutePath() : getContext().getString(R.string.text_unknown_path));
    }

    private void initBranchesSpinner(View view) {
        mBranchesSpinner = (Spinner) view.findViewById(R.id.spinnerBranches);
        mBranchesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // Populate file list with artifacts from selected branch
                updateFileListView((Branch) mBranchesSpinner.getItemAtPosition(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                // Nothing selected, clear file list
                updateFileListView(null);
            }
        });

        // Initial attempt to populate the branches list. If this fails with an authentication
        // error, mListener.promptForPassword will be called to get the hosting activity to
        // prompt the user for a password.
        fetchBranchesForPlan(null);
    }

    /**
     * Called by the hosting activity to attempt to load up the branches list from the plan
     * by passing the given authentication string to Plan.fetch.
     *
     * @param authString base64 encoded combination of username and password
     */
    void fetchBranchesForPlan(String authString) {
        final Context context = getContext();
        final SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        final String bambooServerUrl = prefs.getString(context.getString(R.string.pref_bamboo_server_key), "");
        final Plan plan = new Plan(bambooServerUrl,
                prefs.getString(context.getString(R.string.pref_bamboo_project_key), ""),
                prefs.getString(context.getString(R.string.pref_bamboo_plan_key), ""), getKeyStoreByUrl(bambooServerUrl), context);

        if (mNextBranch == null) {
            // Create Branch instance for next if required. It appears that unless
            // this branch appears in the top 25 branches by default, it won't be
            // found by the code below therefore we need to manually create an
            // instance.
            mNextBranch = new Branch(plan, context);
        }

        // Fetch the plan's data from the server. The callback will take the
        // list of branches and populate the branches spinner. If we need a
        // username and password, we will call back to the hosting activity
        // so that it can display the appropriate dialog.
        plan.fetch(new FetchPlanDataTask.FetchCallback() {
            @Override
            public void fetchComplete(Boolean result) {
                if (result) {
                    List<Branch> branchList = plan.getBranchList();
                    ArrayAdapter<Branch> arrayAdapter =
                            new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, branchList);
                    arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    mBranchesSpinner.setAdapter(arrayAdapter);

                    if (mNextBranch != null) {
                        branchList.add(0, mNextBranch);
                    }
                }
            }

            @Override
            public void needPassword() {
                mListener.promptForPassword();
            }

            @Override
            public void handleException(Exception exception) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setMessage(exception.getMessage());
                builder.setPositiveButton(context.getString(R.string.label_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog sslExceptDialog = builder.create();
                sslExceptDialog.show();
            }
        }, authString);
    }

    /**
     * Used to create a KeyStore containing a certificate for the given URL, if
     * one exists. The certificate, if any is searched for in the assets folder.
     * @param bambooServerUrl url to find certificate for
     * @return KeyStore containing desired certificate or null if certificate isn't
     * found
     */
    private KeyStore getKeyStoreByUrl(String bambooServerUrl) {
        KeyStore retVal = null;
        String assetName = "";
        InputStream certInput = null;

        switch (bambooServerUrl){
            case "https://bamboo.beck4.com:8443":
                assetName = "bamboo.beck4.com.8443.crt";
                break;
        }

        if (assetName.length() > 0) {
            try {
                certInput = getActivity().getAssets().open(assetName);
                Certificate certificate = CertificateFactory.getInstance("X.509").generateCertificate(certInput);
                retVal = KeyStore.getInstance(KeyStore.getDefaultType());
                retVal.load(null, null);
                retVal.setCertificateEntry(assetName, certificate);
            } catch (IOException | KeyStoreException | CertificateException | NoSuchAlgorithmException e) {
                e.printStackTrace();
            } finally {
                if(certInput != null){
                    try {
                        certInput.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        return retVal;
    }

    private void updateFileListView(final Branch selectedBranch) {
        if (mFileListView != null) {
            // Clear file list by setting an empty list
            mFileListView.setAdapter(mEmptyBranchArrayAdapter);

            mProgressBar.setVisibility(View.INVISIBLE);
            if (selectedBranch != null) {
                final Context context = getContext();
                final ProgressDialog progressDialog = ProgressDialog.show(context,
                        context.getString(R.string.title_action_bamboo_download),
                        context.getString(R.string.message_getting_files_list), true);

                // Filter to just firmware for now
                selectedBranch.setType(EnumSet.of(Branch.Type.FIRMWARE));

                // Populate branch with data from server
                selectedBranch.fetch(new FetchBranchDataFromPlanTask.FetchCallback() {
                    @Override
                    public void fetchComplete(Boolean result) {
                        if (result) {
                            ArrayAdapter<Artifact> arrayAdapter =
                                    new ArrayAdapter<>(getActivity(),
                                            android.R.layout.simple_list_item_multiple_choice,
                                            selectedBranch.getArtifactList());
                            mFileListView.setAdapter(arrayAdapter);
                        }
                        progressDialog.dismiss();
                    }
                });
            }
        }
    }

    private void downloadSelectedFiles() {

        SparseBooleanArray selectedItems = mFileListView.getCheckedItemPositions();
        List<Artifact> selectedArtifacts = new ArrayList<>();

        for (int count = 0; count < mFileListView.getAdapter().getCount(); ++count) {
            if (selectedItems.get(count)) {
                selectedArtifacts.add((Artifact) mFileListView.getAdapter().getItem(count));
            }
        }

        mProgressBar.setMax(selectedArtifacts.size());
        mProgressBar.setVisibility(View.VISIBLE);

        for (Artifact curArtifact : selectedArtifacts) {
            curArtifact.fetch(new FetchArtifactDataTask.FetchCallback() {
                @Override
                public void fetchComplete(Boolean result, Artifact artifact) {
                    try {
                        if (result) {
                            artifact.writeToFile(new File(mEditFileSavePath.getText().toString()));
                        }
                        mProgressBar.incrementProgressBy(1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        /**
         * Implement in hosting activity to display a save location for downloading
         * firmware when requested by this fragment.
         */
        void onBrowseClicked();

        /**
         * Implement in hosting activity to display a username/password dialog for
         * bamboo when requested by this fragment.
         */
        void promptForPassword();
    }

}
