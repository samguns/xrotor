/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.tasks.bamboo;

import android.support.annotation.NonNull;

import org.opng.mobilegcs.firmware.downloader.bamboo.Branch;
import org.opng.mobilegcs.firmware.downloader.bamboo.Plan;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public abstract class FetchBranchDataTask extends BaseTask<Branch, Void, Boolean> {
    private final FetchCallback mFetchCallback;

    FetchBranchDataTask(FetchCallback fetchCallback) {
        mFetchCallback = fetchCallback;
    }

    @Override
    protected Boolean doInBackground(Branch... branches) {
        Boolean retVal = branches.length > 0 && branches[0] != null;
        Branch branch = branches[0];
        HttpURLConnection conn = null;
        InputStream is = null;

        if (retVal) {
            try {
                Plan plan = branch.getPlan();
                URL url = constructURL(branch, plan);
                conn = (HttpURLConnection) url.openConnection();

                if(initKeyStoreHandling(plan.getKeyStore())){
                    ((HttpsURLConnection)conn).setSSLSocketFactory(mSslContext.getSocketFactory());
                }
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                branch.fromJson(is);

            } catch (IOException e) {
                e.printStackTrace();
                retVal = false;
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                if (conn != null) {
                    conn.disconnect();
                }
            }
        }
        return retVal;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        if (mFetchCallback != null) {
            mFetchCallback.fetchComplete(result);
        }
    }

    @NonNull
    protected abstract URL constructURL(Branch branch, Plan plan) throws MalformedURLException;

    public interface FetchCallback {
        void fetchComplete(Boolean result);
    }
}
