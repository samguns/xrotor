/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.filemanager;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.opng.mobilegcs.R;

import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;

public class FileRecyclerViewAdapter extends RecyclerView.Adapter<FileRecyclerViewAdapter.FileViewHolder> {
    private final List<File> mFileList;
    private final FileSelectionModel mSelectionModel;
    private final DateFormat mDateFormat = DateFormat.getDateTimeInstance();

    public FileRecyclerViewAdapter(List<File> fileList, FileSelectionModel selectionModel) {
        mFileList = fileList;
        mSelectionModel = selectionModel;
        mSelectionModel.setRecyclerViewAdapter(this);
    }

    @Override
    public FileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.file_detail_item, parent, false);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mSelectionModel.handleSelection(view);
            }
        });

        itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mSelectionModel.handleLongClick(view);
                return true;
            }
        });

        return new FileViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FileViewHolder holder, int position) {
        File curFile = mFileList.get(position);
        holder.setFile(curFile, mSelectionModel.itemIsSelected(position));
        holder.itemView.setTag(holder);
    }

    @Override
    public int getItemCount() {
        return mFileList != null ? mFileList.size() : 0;
    }

    public class FileViewHolder extends RecyclerView.ViewHolder {
        private final ImageView mImageViewFileType;
        private final TextView mTextViewFileName;
        private final TextView mTextViewFileCreateDate;

        public FileViewHolder(View itemView) {
            super(itemView);

            mImageViewFileType = (ImageView) itemView.findViewById(R.id.imageViewType);
            mTextViewFileName = (TextView) itemView.findViewById(R.id.textFileName);
            mTextViewFileCreateDate = (TextView) itemView.findViewById(R.id.textFileCreateDate);
        }

        public void setFile(File file, boolean isSelected) {
            TypedValue typedValue = new TypedValue();
            Resources.Theme theme = itemView.getContext().getTheme();
            theme.resolveAttribute(isSelected ? R.attr.colorControlHighlight : R.attr.colorControlNormal, typedValue, true);
            int color = typedValue.data;
            itemView.setBackgroundColor(color);

            if (file.isDirectory()) {
                mImageViewFileType.setImageResource(R.drawable.ic_folder_black_24dp);
            } else {
                mImageViewFileType.setImageDrawable(null);
            }
            mTextViewFileName.setText(file.getName());
            mTextViewFileCreateDate.setText(mDateFormat.format(new Date(file.lastModified())));
        }

        public int getItemPosition() {
            return getAdapterPosition();
        }
    }
}
