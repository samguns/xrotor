/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.tasks.bamboo;

import android.support.annotation.NonNull;

import org.opng.mobilegcs.firmware.downloader.bamboo.Branch;
import org.opng.mobilegcs.firmware.downloader.bamboo.Plan;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Use this task to populate the given branch (passed to execute) with data from the plan
 * it references. This is intended to be used with Branch instances created from a Plan and
 * a branch name. Use this when opening a specific Branch by name rather than from a list
 * read from a Plan.
 */
public class FetchBranchDataByNameTask extends FetchBranchDataTask {

    public FetchBranchDataByNameTask(FetchCallback fetchCallback) {
        super(fetchCallback);
    }

    @NonNull
    @Override
    protected URL constructURL(Branch branch, Plan plan) throws MalformedURLException {
        return new URL(String.format("%s/rest/api/latest/result/%s-%s/branch/%s.json?expand=results.result.artifacts&start-index=0&max-results=1",
                plan.getRestUrl(), plan.getProjectKey(), plan.getPlanKey(), branch.getName()));
    }
}
