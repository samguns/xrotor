/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.optune;

import android.os.Parcel;
import android.util.Log;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.wizard.WizardState;
import org.opng.uavtalk.uavobjects.TxPIDSettings;

class AcquireYawUovState extends WizardState implements OPTuneFragment.OPTuneWizardState {
    public static final Creator<AcquireYawUovState> CREATOR = new Creator<AcquireYawUovState>() {
        @Override
        public AcquireYawUovState createFromParcel(Parcel source) {
            return new AcquireYawUovState(source);
        }

        @Override
        public AcquireYawUovState[] newArray(int size) {
            return new AcquireYawUovState[size];
        }
    };
    private final static String TAG = AcquireYawUovState.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private OPTuneFragment mOwner;

    AcquireYawUovState(WizardState previousState, OPTuneFragment owner) {
        super(previousState);
        mOwner = owner;
    }

    private AcquireYawUovState(Parcel source) {
        super(source);
    }

    @Override
    public WizardState next() {
        if (DEBUG) Log.d(TAG, "AcquireYawUovState: next");

        // We want to restore the starting pitch rate Kp and store our
        // UOV for pitch as we're about to either process roll or calculate
        // our tuned PIDs
        mOwner.saveUovAndRestoreKp(OPTuneFragment.Axis.YAW);

        return new CalculateState(this, mOwner);
    }

    @Override
    public void process() {
        if (DEBUG) Log.d(TAG, "AcquireYawUovState: process");

        mOwner.setInstructions(String.format(mOwner.getString(R.string.text_optune_wizard_acquire_uov), "Yaw"));

        // We are adjusting pitch rate Kp
        mOwner.getTxPidGuiHelper().setPIDOption(TxPIDSettings.PIDS_YAW_RATE_KP, 0);
        mOwner.getRadioGroupAxes().check(R.id.radioYaw);

        // Zero Ki & Kd. Note that this method will only do this once
        // when we start to process the first axis
        mOwner.zeroKiKd();
        mOwner.getTxPidGuiHelper().saveChangesToRam();
    }

    @Override
    public void setOwner(OPTuneFragment owner) {
        mOwner = owner;
        ((OPTuneFragment.OPTuneWizardState) previous()).setOwner(owner);
    }
}
