/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.Spinner;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectToGuiUtilsEventListener;
import org.opng.mobilegcs.util.UAVObjectToGuiUtilsEventListenerAdapter;
import org.opng.mobilegcs.util.Variant;
import org.opng.mobilegcs.views.curve.CurveData;
import org.opng.mobilegcs.views.curve.CurveView;

/**
 * A simple {@link Fragment} subclass.
 */
public class StabiExpoFragment extends Fragment implements StabiGuiFragment {
    private final static String TAG = StabiExpoFragment.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    final static private float EXPO_CURVE_CONSTANT = 1.00695f;
    final static private int EXPO_CURVE_POINTS_COUNT = 100;
    private static boolean WARN = LOGLEVEL > 0;
    private View mRootView;
    private View mViewRoll;
    private View mViewPitch;
    private View mViewYaw;
    private CurveView mCurveExpo;
    private final UAVObjectToGuiUtilsEventListener mUAVObjectToGuiUtilsEventListener = new UAVObjectToGuiUtilsEventListenerAdapter() {
        @Override
        public void viewContentsChanged(View view, Variant value) {
            if (view instanceof SeekBar && value.isValid()) {
                final CurveData curveData;
                if (view.equals(mViewRoll)) {
                    curveData = mCurveExpo.getCurveData(0);
                } else if (view.equals(mViewPitch)) {
                    curveData = mCurveExpo.getCurveData(1);
                } else if (view.equals(mViewYaw)) {
                    curveData = mCurveExpo.getCurveData(2);
                } else {
                    curveData = null;
                }

                if (curveData != null) {
                    plotCurve(value.toInt(), curveData);
                }
            }
        }
    };
    private UAVObjectManagerLoaderHandler mUAVObjectManagerLoaderHandler;

    public StabiExpoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_stabi_expo, container, false);
        mViewRoll = mRootView.findViewById(R.id.seekExpoRoll);
        mViewPitch = mRootView.findViewById(R.id.seekExpoPitch);
        mViewYaw = mRootView.findViewById(R.id.seekExpoYaw);
        mCurveExpo = (CurveView) mRootView.findViewById(R.id.curveExpo);

        setSliderLimits(mViewRoll);
        setSliderLimits(mViewPitch);
        setSliderLimits(mViewYaw);

        if (mCurveExpo != null) {
            for (int count = 0; count < mCurveExpo.getCurveDataCount(); ++count) {
                final CurveData curveData = mCurveExpo.getCurveData(count);
                curveData.setReadOnly(true);
                curveData.setShowingLabels(false);
            }
        }

        // Set up our loader handler: get all the controls it needs and then create it
        Button apply = (Button) mRootView.findViewById(R.id.buttonCommonFooterApply);
        Button save = (Button) mRootView.findViewById(R.id.buttonCommonFooterSave);
        Button btnDefault = (Button) mRootView.findViewById(R.id.buttonCommonFooterDefault);
        Button restore = (Button) mRootView.findViewById(R.id.buttonCommonFooterRestore);
        CheckBox autoUpdate = (CheckBox) mRootView.findViewById(R.id.checkFcUpdateRealtime);
        Spinner bankSelect = (Spinner) mRootView.findViewById(R.id.spinnerStabiBank);

        mUAVObjectManagerLoaderHandler = new UAVObjectManagerLoaderHandler(getActivity(), this, autoUpdate, bankSelect, apply, save,
                btnDefault, restore, R.integer.stabi_expo_button_group);

        getLoaderManager().initLoader(0, null, mUAVObjectManagerLoaderHandler);

        return mRootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onStop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUAVObjectManagerLoaderHandler != null) {
            mUAVObjectManagerLoaderHandler.onResume();
        }
        curveFromSeekbar((SeekBar) mViewRoll, 0);
        curveFromSeekbar((SeekBar) mViewPitch, 1);
        curveFromSeekbar((SeekBar) mViewYaw, 2);
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        if (mRootView != null) {
            // If root view is not null, we should have the three controls
            // required
            helper.addObjectToGuiEventListener(mUAVObjectToGuiUtilsEventListener);
            helper.bindExpoWidgets(mViewRoll, mViewPitch, mViewYaw);
        }
    }

    private void plotCurve(int value, CurveData curve) {
        double factor = Math.pow(EXPO_CURVE_CONSTANT, value);
        double step = 1.0 / (EXPO_CURVE_POINTS_COUNT - 1);

        for (int i = 0; i < EXPO_CURVE_POINTS_COUNT; i++) {
            double val = i * step;
            curve.setPoint(i, (float) val * 100, (float) Math.pow(val, factor) * 100);
        }

        mCurveExpo.updateCurves();
        mCurveExpo.postInvalidate();
    }

    private void setSliderLimits(View slider) {
        if (slider != null && slider instanceof SeekBar) {
            slider.setTag(R.id.seekbar_min_value, -100);
            ((SeekBar) slider).setMax(200);
        }

    }

    private void curveFromSeekbar(SeekBar seekBar, int curveIndex) {
        if (seekBar != null) {
            plotCurve(seekBar.getProgress() - 100, mCurveExpo.getCurveData(curveIndex));
        }
    }
}
