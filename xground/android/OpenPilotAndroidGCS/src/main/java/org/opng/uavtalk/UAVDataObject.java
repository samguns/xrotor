/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

public abstract class UAVDataObject extends UAVObject implements Cloneable {

    private final boolean mIsSet;
    private UAVMetaObject mMetaObject;

    /**
     * Constructor for UAVDataObject
     *
     * @param objID        the object id to be created
     * @param isSingleInst set true if this is a single instance object, false otherwise
     * @param isSet        set true if this is a settings object, false otherwise
     * @param name         object name
     */
    protected UAVDataObject(long objID, Boolean isSingleInst, Boolean isSet, String name) {
        super(objID, isSingleInst, name);
        mMetaObject = null;
        this.mIsSet = isSet;
    }

    /**
     * Initialize instance ID and assign a metaobject
     */
    public void initialize(long instID, UAVMetaObject mobj) {
        //QMutexLocker locker(mutex);
        this.mMetaObject = mobj;
        super.initialize(instID);
    }

    @Override
    public boolean isMetadata() {
        return false;
    }

    /**
     * Assign a metaobject
     */
    public void initialize(UAVMetaObject mobj) {
        //QMutexLocker locker(mutex);
        this.mMetaObject = mobj;
    }

    /**
     * Returns true if this is a data object holding module settings
     */
    public boolean isSettings() {
        return mIsSet;
    }

    /**
     * Get the object's metadata
     */
    @Override
    public Metadata getMetadata() {
        if (mMetaObject != null) {
            return mMetaObject.getData();
        } else {
            return getDefaultMetadata();
        }
    }

    /**
     * Set the object's metadata
     */
    @Override
    public void setMetadata(Metadata mdata) {
        if (mMetaObject != null) {
            mMetaObject.setData(mdata);
        }
    }

    /**
     * Get the metaobject
     */
    public UAVMetaObject getMetaObject() {
        return mMetaObject;
    }

    // TODO: Make abstract
    public UAVDataObject clone(long instID) throws CloneNotSupportedException {
        return (UAVDataObject) super.clone();
    }

    public abstract UAVDataObject getDefaultInstance();

}
