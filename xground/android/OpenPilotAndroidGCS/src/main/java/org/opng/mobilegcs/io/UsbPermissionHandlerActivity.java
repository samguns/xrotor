/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.io;

import android.support.v7.app.AppCompatActivity;

/**
 * Used to handle USB device connection intents without obscuring the
 * activity that was active at the time the device was active. Basically
 * this activity allows us to use the activity intent filter to handle
 * USB connection events allowing us to tick the 'remember' box on the
 * permission dialog when it appears.
 */
public class UsbPermissionHandlerActivity extends AppCompatActivity {

    @Override
    protected void onResume() {
        super.onResume();

        // Can get the intent here with getIntent() if we need to at some point.
        // For now we just want to end this activity.

        finish();
    }
}
