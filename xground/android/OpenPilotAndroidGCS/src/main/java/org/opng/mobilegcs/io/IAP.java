/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.io;

import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.common.base.Strings;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.Stm32Crc;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class IAP {
    private static final String TAG = IAP.class.getSimpleName();
    private static final int LOGLEVEL = 3;
    public static final boolean WARN = LOGLEVEL > 1;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean ERROR = LOGLEVEL > 0;

    private static final int BOARD_ID_MB = 1;
    private static final int BOARD_ID_INS = 2;
    private static final int BOARD_ID_PIP = 3;
    private static final int BOARD_ID_REVO = 9;
    private static final int BUF_LEN = 64;
    private static final int IO_TIMEOUT = 200;
    private static final int UPLOAD_PACKET_MAX_RETRIES = 10;

    private final List<Device> mDevices = new ArrayList<>();
    private final List<Device> mDevicesAccessor = Collections.unmodifiableList(mDevices);
    private final boolean mReady;
    private final List<IAPEventListener> mListeners = new CopyOnWriteArrayList<>();
    private final Context mContext;
    private final ByteBuffer mTxBuffer;
    private final ByteBuffer mRxBuffer;
    private final RawSerialIO mRawSerialIO;
    private int mNumberOfDevices;
    private IAPTask mIAPTask;
    private ByteBuffer mFirmwareImage;
    private long mDeviceFirmwareStorageSize;
    private byte[] mDescription;

    public IAP(RawSerialIO rawSerialIO, Context context) {
        mRawSerialIO = rawSerialIO;
        mContext = context;
        mReady = true;
        mNumberOfDevices = 0;
        mTxBuffer = ByteBuffer.allocate(BUF_LEN);
        mRxBuffer = ByteBuffer.allocate(BUF_LEN);
        if (DEBUG) Log.d(TAG, "Ending async. read/write");
        // Perform any steps required by the current telemetry task to enable
        // raw IO.
//        mRawSerialIO.initRawIO(true);

        // This status request is here to get USBUABTalk to stop waiting for
        // a request by asking for data. We need to do this as initRawIO for
        // USBUAVTalk tries to halt the async. read/write thread but the thread
        // will be sitting waiting for data from the USB port which will never
        // come as we're booting into IAP mode. Therefore we request data from
        // the port.
        statusRequest();
        if (DEBUG) Log.d(TAG, "Ended async. read/write");
    }

    public static String statusToString(Status status) {
        switch (status) {
            case IAP_IDLE:
                return "IAP_IDLE";

            case UPLOADING:
                return "UPLOADING";

            case WRONG_PACKET_RECEIVED:
                return "WRONG_PACKET_RECEIVED";

            case TOO_MANY_PACKETS:
                return "TOO_MANY_PACKETS";

            case TOO_FEW_PACKETS:
                return "TOO_FEW_PACKETS";

            case LAST_OPERATION_SUCCESS:
                return "LAST_OPERATION_SUCCESS";

            case DOWNLOADING:
                return "DOWNLOADING";

            case IDLE:
                return "IDLE";

            case LAST_OPERATION_FAILED:
                return "LAST_OPERATION_FAILED";

            case OUTSIDE_DEV_CAPABILITIES:
                return "OUTSIDE_DEV_CAPABILITIES";

            case CRC_FAIL:
                return "CRC check FAILED";

            case FAILED_JUMP:
                return "Jmp to user FW failed";

            case ABORT:
                return "ABORT";

            case UPLOADING_STARTING:
                return "Uploading Starting";

            default:
                return "unknown";
        }
    }

    public List<Device> getDevices() {
        return mDevicesAccessor;
    }

    public int getNumberOfDevices() {
        return mNumberOfDevices;
    }

    public boolean saveByteArrayToFile(String filename, ByteBuffer array) {
        File file = new File(filename);
        FileChannel outChannel = null;
        boolean retVal = true;

        try {
            outChannel = new FileOutputStream(file, false).getChannel();
            array.flip();
            outChannel.write(array);
        } catch (FileNotFoundException e) {
            if (DEBUG) {
                Log.d(TAG, "Can't open file");
            }
            e.printStackTrace();
            retVal = false;
        } catch (IOException e) {
            e.printStackTrace();
            retVal = false;
        } finally {
            if (outChannel != null) {
                try {
                    outChannel.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return retVal;
    }

    /**
     * Tells the mainboard to enter DFU Mode.
     */
    public boolean enterIAP(int devNumber) {
        mTxBuffer.rewind();

        mTxBuffer.putInt(Commands.ENTER_DFU.getValue()); // DFU Command
        mTxBuffer.putInt(0); // DFU Count
        mTxBuffer.put((byte) devNumber); // DFU Data0
        mTxBuffer.put((byte) 0x01); // DFU Data1
        mTxBuffer.put((byte) 0x01); // DFU Data2
        mTxBuffer.put((byte) 0x01); // DFU Data3

        int result = sendData(mTxBuffer);
        if (result < 1) {
            return false;
        }
        if (DEBUG) Log.d(TAG, "ENTER_IAP: " + result + " bytes sent");
        return true;
    }

    /**
     * Sends the firmware description to the device
     */
    public Status uploadDescription(String description) {
        if (DEBUG) Log.d(TAG, "Starting UPLOADING description\n");
        ByteBuffer array;

        if (description.length() % 4 != 0) {
            // Pad string up to 4 char boundary
            int pad = description.length() / 4;
            pad = (pad + 1) * 4;
            description = Strings.padEnd(description, pad, ' ');
        }
        array = ByteBuffer.wrap(description.getBytes());

        return uploadDescription(array);
    }

    public Status uploadDescription(ByteBuffer array) {
        if (array == null) {
            return Status.ABORT;
        }

        if (!startUpload(array.capacity(), TransferTypes.DESCRIPTION, 0)) {
            return Status.ABORT;
        }
        if (!uploadData(array.capacity(), array,
                new ProgressCallback() {
                    @Override
                    public void updateProgress(int progress) {
                        fireProgressUpdated(progress);
                    }
                })) {
            return Status.ABORT;
        }
        if (!endOperation()) {
            return Status.ABORT;
        }
        Status ret = statusRequest();


        if (DEBUG) Log.d(TAG, "UPLOAD description Status=" + statusToString(ret));
        return ret;
    }

    /**
     * Downloads the description string for the current device.
     * You have to call enterIAP before calling this function.
     */
    public String downloadDescription(int numberOfChars) {
        ByteBuffer arr = ByteBuffer.allocate(numberOfChars);

        startDownloadT(arr, numberOfChars, TransferTypes.DESCRIPTION,
                new ProgressCallback() {
                    @Override
                    public void updateProgress(int progress) {
                        fireProgressUpdated(progress);
                    }
                });

        byte[] bytes = arr.array();

        // Strip padding, if any
        int index = Arrays.binarySearch(bytes, (byte) 255);
        return new String((index == -1) ? bytes : Arrays.copyOfRange(bytes, 0, index));
    }

    public ByteBuffer downloadDescriptionAsBA(int numberOfChars) {
        ByteBuffer arr = ByteBuffer.allocate(numberOfChars);

        startDownloadT(arr, numberOfChars, TransferTypes.DESCRIPTION,
                new ProgressCallback() {
                    @Override
                    public void updateProgress(int progress) {
                        fireProgressUpdated(progress);
                    }
                });

        return arr;
    }

    /**
     * Starts a firmware download
     *
     * @param firmwareArray: pointer to the location where we should store the firmware
     * @param device:        the device to use for the download
     */
    public boolean downloadFirmware(ByteBuffer firmwareArray, int device) {
        if (mIAPTask != null) {
            return false;
        }

        mIAPTask = new IAPTask(mDevices.get(device).SizeOfCode,
                TransferTypes.FW,
                firmwareArray);
        mIAPTask.execute();
        return true;
    }

    /**
     * Resets the device
     */
    public boolean resetDevice() {
        mTxBuffer.rewind();

        mTxBuffer.putInt(Commands.RESET.getValue()); // DFU Command
        mTxBuffer.putLong(0);

        return sendData(mTxBuffer) > 0;
    }

    public int abortOperation() {
        mTxBuffer.rewind();

        mTxBuffer.putInt(Commands.ABORT_OPERATION.getValue()); // DFU Command
        mTxBuffer.putLong(0);

        return sendData(mTxBuffer);
    }

    public boolean isReady() {
        return mReady;
    }

    /**
     * Starts the firmware (leaves bootloader and boots the main software)
     */
    public void jumpToApp(boolean safeBoot, boolean erase) {
        mTxBuffer.rewind();

        mTxBuffer.putInt(Commands.JUMP_FW.getValue()); // DFU Command
        mTxBuffer.putInt(0);
        mTxBuffer.putShort((short) 0);
        if (safeBoot) {
        /* force system to safe boot mode (hwsettings == defaults) */
            mTxBuffer.put((byte) 0x5A);
            mTxBuffer.put((byte) 0xFE);
        } else {
            mTxBuffer.putShort((short) 0);
        }
        if (erase) {
            // force data flash clear
            mTxBuffer.putShort((short) 0);
            mTxBuffer.put((byte) 0xFA);
            mTxBuffer.put((byte) 0x5F);
        } else {
            mTxBuffer.putInt(0);
        }

        mTxBuffer.putLong(0);

        sendData(mTxBuffer);
    }

    public Status statusRequest() {
        mTxBuffer.rewind();

        mTxBuffer.putInt(Commands.STATUS_REQUEST.getValue()); // DFU Command
        mTxBuffer.putLong(0);

        int result = sendData(mTxBuffer);
        if (DEBUG) Log.d(TAG, "StatusRequest: " + result + " bytes sent");

        if (result == -1) {
            return Status.ABORT;
        }

        mRxBuffer.rewind();
        result = receiveData(mRxBuffer);
        mRxBuffer.rewind();
        if (DEBUG) Log.d(TAG, "StatusRequest: " + result + " bytes received");

        if (mRxBuffer.get() == 0x01) {
            final Status status = Status.fromByte(mRxBuffer.get(5));
            if (DEBUG)
                Log.d(TAG, "StatusRequest: " + statusToString(status) + " Data: " + mRxBuffer.getInt(1));
            return status;
        } else {
            return Status.ABORT;
        }
    }

    /**
     * Ask the bootloader for the list of devices available
     */
    public boolean findDevices() {
        mDevices.clear();
        if (!requestCapabilities()) return false;

        mNumberOfDevices = mRxBuffer.get(6);
        int rwFlags = mRxBuffer.getShort(7);

        for (int i = 0; i < mNumberOfDevices; ++i) {
            if (requestCapabilities(i + 1)) {
                Device dev = new Device();
                dev.Readable = (rwFlags >> (i * 2) & 1) > 0;
                dev.Writable = (rwFlags >> (i * 2 + 1) & 1) > 0;
                dev.SizeOfCode = mRxBuffer.getInt(1);
                dev.BL_Version = mRxBuffer.get(6);
                dev.SizeOfDesc = mRxBuffer.get(7);

                dev.FW_CRC = mRxBuffer.getInt(9) & 0xFFFFFFFFL;
                dev.ID = mRxBuffer.getShort(13);
                mDevices.add(dev);
            }
        }
        if (DEBUG) Log.d(TAG, "Found " + mNumberOfDevices + " devices");
        for (int x = 0; x < mDevices.size(); ++x) {
            if (DEBUG) Log.d(TAG, "Device #" + x + 1);
            final Device device = mDevices.get(x);
            if (DEBUG) Log.d(TAG, "Device ID=" + device.ID);
            if (DEBUG) Log.d(TAG, "Device Readable=" + device.Readable);
            if (DEBUG) Log.d(TAG, "Device Writable=" + device.Writable);
            if (DEBUG) Log.d(TAG, "Device SizeOfCode=" + device.SizeOfCode);
            if (DEBUG) Log.d(TAG, "Device SizeOfDesc=" + device.SizeOfDesc);
            if (DEBUG) Log.d(TAG, "BL Version=" + device.BL_Version);
            if (DEBUG) Log.d(TAG, "FW CRC=" + device.FW_CRC);
        }
        return true;
    }

    public boolean endOperation() {
        mTxBuffer.rewind();

        mTxBuffer.putInt(Commands.OP_END.getValue()); // DFU Command
        mTxBuffer.putLong(0);
        mTxBuffer.putLong(0);

        int result = sendData(mTxBuffer);
        if (DEBUG) Log.d(TAG, result + " bytes sent");

        return result > 0;
    }

    /**
     * Starts a firmware upload (asynchronous)
     */
    public boolean uploadFirmware(String firmwareFilename, int device, boolean verify) {
        if (mIAPTask != null) {
            return false;
        }

        mIAPTask = new IAPTask(firmwareFilename, device, verify);
        mIAPTask.execute();
        return true;
    }

    /**
     * Starts a firmware upload (asynchronous)
     */
    public boolean uploadFirmware(ByteBuffer firmwareImage, int device, boolean verify) {
        if (mIAPTask != null) {
            return false;
        }

        mIAPTask = new IAPTask(firmwareImage, device, verify);
        mIAPTask.execute();
        return true;
    }

    public Status compareFirmware(String firmwareFilename, CompareType type, int device) {
        if (DEBUG) Log.d(TAG, "Starting Firmware Compare...\n");

        ByteArrayFileLoader byteArrayFileLoader = new ByteArrayFileLoader(firmwareFilename).invoke();
        if (byteArrayFileLoader.failed()) return Status.ABORT;
        int fileSize = byteArrayFileLoader.getFileSize();
        mFirmwareImage = byteArrayFileLoader.getArr();
        mDeviceFirmwareStorageSize = mDevices.get(device).SizeOfCode;

        if (DEBUG) Log.d(TAG, "Bytes Loaded=" + fileSize);

        if (type == CompareType.CRC) {
            long crc = padAndGenerateCrc(fileSize);
            if (crc == mDevices.get(device).FW_CRC) {
                if (DEBUG) Log.d(TAG, "Compare Successful CRC MATCH!\n");
            } else {
                if (DEBUG) Log.d(TAG, "Compare failed CRC DON'T MATCH!\n");
            }
            return statusRequest();
        } else {
            ByteBuffer arr2 = ByteBuffer.allocate(fileSize);
            startDownloadT(arr2, fileSize, TransferTypes.FW,
                    new ProgressCallback() {
                        @Override
                        public void updateProgress(int progress) {
                            fireProgressUpdated(progress);
                        }
                    });
            if (mFirmwareImage == arr2) {
                if (DEBUG) Log.d(TAG, "Compare Successful ALL Bytes MATCH!\n");
            } else {
                if (DEBUG) Log.d(TAG, "Compare failed Bytes DON'T MATCH!\n");
            }
            return statusRequest();
        }
    }

    public void addIAPEventListener(IAPEventListener listener) {
        mListeners.add(listener);
    }

    public void removeIAPEventListener(IAPEventListener listener) {
        mListeners.remove(listener);
    }

    /**
     * Downloads a certain number of bytes from a certain location, and stores in an array whose
     * pointer is passed as an argument
     */
    private Status startDownloadT(ByteBuffer downloadedData, int numberOfBytes,
                                  TransferTypes type, ProgressCallback progressCallback) {
        int lastPacketCount;

        // First of all, work out the number of DFU packets we should ask for:
        int numberOfPackets = numberOfBytes / 4 / 14;
        int pad = (numberOfBytes - numberOfPackets * 4 * 14) / 4;

        if (pad == 0) {
            lastPacketCount = 14;
        } else {
            ++numberOfPackets;
            lastPacketCount = pad;
        }

        mTxBuffer.rewind();

        mTxBuffer.putInt(Commands.DOWNLOAD_REQ.getValue()); // DFU Command
        mTxBuffer.putInt(numberOfPackets); // DFU Count
        mTxBuffer.put((byte) type.ordinal()); // DFU Data0
        mTxBuffer.put((byte) lastPacketCount); // DFU Data1
        mTxBuffer.put((byte) 1); // DFU Data2
        mTxBuffer.put((byte) 1); // DFU Data3

        int result = sendData(mTxBuffer);
        if (DEBUG)
            Log.d(TAG, "StartDownload: " + numberOfPackets + " packets. Last Packet Size=" + lastPacketCount + " " + result + " bytes sent");

        float percentage;
        int lastPercentage = 0;

        // Now get those packets:
        for (int x = 0; x < numberOfPackets; ++x) {
            int size;
            percentage = (float) (x + 1) / numberOfPackets * 100;
            if (lastPercentage != (int) percentage) {
                progressCallback.updateProgress((int) percentage);
            }
            lastPercentage = (int) percentage;

            mRxBuffer.rewind();
            receiveData(mRxBuffer);

            if (DEBUG)
                Log.d(TAG, result + " bytes received" + " Count=" + x + "-" + mRxBuffer.get() + ";" +
                        mRxBuffer.get() + ";" + mRxBuffer.get() + ";" + mRxBuffer.get() + " Data=" +
                        mRxBuffer.get() + ";" + mRxBuffer.get() + ";" + mRxBuffer.get() + ";" +
                        mRxBuffer.get());

            if (x == numberOfPackets - 1) {
                size = lastPacketCount * 4;
            } else {
                size = 14 * 4;
            }
            downloadedData.put(mRxBuffer.array(), 6, size);
        }

        return statusRequest();
    }

    private boolean requestCapabilities() {
        return requestCapabilities(0);
    }

    private boolean requestCapabilities(int device) {
        boolean retVal = true;
        mTxBuffer.rewind();
        mTxBuffer.putInt(Commands.REQ_CAPABILITIES.getValue()); // DFU Command
        mTxBuffer.putInt(0);
        mTxBuffer.put((byte) device);
        mTxBuffer.put((byte) 0);
        mTxBuffer.put((byte) 0);
        mTxBuffer.put((byte) 0);

        int result = sendData(mTxBuffer);
        if (result < 1) {
            retVal = false;
        }

        mRxBuffer.rewind();
        result = receiveData(mRxBuffer);
        if (result < 1) {
            retVal = false;
        }
        return retVal;
    }

    /**
     * Tells the board to get ready for an upload. It will in particular
     * erase the memory to make room for the data. You will have to query
     * its status to wait until erase is done before doing the actual upload.
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean startUpload(int numberOfBytes, TransferTypes type, long crc) {
        PacketCountCalculator packetCountCalculator = new PacketCountCalculator(numberOfBytes).invoke();
        int numberOfPackets = packetCountCalculator.getNumberOfPackets();
        int lastPacketSize = packetCountCalculator.getLastPacketSize();

        mTxBuffer.rewind();
        mTxBuffer.putInt(Commands.UPLOAD_START.getValue()); // DFU Command
        mTxBuffer.putInt(numberOfPackets); // DFU Count
        mTxBuffer.put((byte) type.ordinal()); // DFU Data0
        mTxBuffer.put((byte) (lastPacketSize)); // DFU Data1
        mTxBuffer.putInt((int) crc);
        if (DEBUG)
            Log.d(TAG, "Number of packets: " + numberOfPackets + " Size of last packet: " + lastPacketSize);

        int result = sendData(mTxBuffer);

        if (DEBUG) Log.d(TAG, result + " bytes sent");

        return result > 0;
    }


    /**
     * Does the actual data upload to the board. Needs to be called once the
     * board is ready to accept data following a StartUpload command, and it is erased.
     *
     * @param numberOfBytes    number of bytes to be uploaded
     * @param data             data to upload
     * @param progressCallback called back to notify progress changes
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    private boolean uploadData(int numberOfBytes, ByteBuffer data, ProgressCallback progressCallback) {
        PacketCountCalculator packetCountCalculator = new PacketCountCalculator(numberOfBytes).invoke();
        int numberOfPackets = packetCountCalculator.getNumberOfPackets();
        int lastPacketSize = packetCountCalculator.getLastPacketSize();

        if (DEBUG) Log.d(TAG, "Start Uploading: " + numberOfPackets + " DWORDS");
        float percentage;
        int lastPercentage = 0;
        for (int packetCount = 0; packetCount < numberOfPackets; ++packetCount) {
            percentage = (float) (packetCount + 1) / numberOfPackets * 100;
            if (lastPercentage != (int) percentage) {
                progressCallback.updateProgress((int) percentage);
            }
            lastPercentage = (int) percentage;
            if (!uploadPacket(data, numberOfPackets, lastPacketSize, packetCount, 0)) return false;
        }
        if (DEBUG) Log.d(TAG, "Uploaded: " + numberOfPackets + " DWORDS");
        return true;
    }

    /**
     * Used to upload a single 14 DWORD packet to the device. After uploading, this method
     * will look at the response to see if the correct packet was sent or an error occurred.
     * If an error occurred, the packet is resent; if an incorrect packet is sent, the correct
     * packet as indicated by the device will be sent. This will repeat up to UPLOAD_PACKET_MAX_RETRIES
     * before failing outright.
     *
     * @param data            all data to be sent
     * @param numberOfPackets number of packets to be sent
     * @param lastPacketSize  size of last packet
     * @param packetCount     current packet to send
     * @param retries         current retry count
     * @return true if we successfully uploaded the packet, false otherwise
     */
    private boolean uploadPacket(ByteBuffer data, int numberOfPackets, int lastPacketSize, int packetCount, int retries) {
        boolean retVal = retries < UPLOAD_PACKET_MAX_RETRIES;
        if (retVal) {
            int packetSize;
            if (packetCount == numberOfPackets - 1) {
                packetSize = lastPacketSize;
                if (DEBUG) Log.d(TAG, "Last packet number: " + packetCount);
            } else {
                packetSize = 14;
            }

            mTxBuffer.rewind();
            mTxBuffer.putInt(Commands.UPLOAD.getValue()); // DFU Command
            mTxBuffer.putInt(packetCount); // DFU Count

            byte[] array = data.array();

            for (int count = 0; count < packetSize; ++count) {
                int position = 14 * 4 * packetCount;
                mTxBuffer.put(array[count * 4 + position + 3]);
                mTxBuffer.put(array[count * 4 + position + 2]);
                mTxBuffer.put(array[count * 4 + position + 1]);
                mTxBuffer.put(array[count * 4 + position]);
            }

            int result = sendData(mTxBuffer);
            if (result < 1) {
                retVal = false;
            } else {
                mRxBuffer.rewind();
                int received = receiveData(mRxBuffer);
                mRxBuffer.rewind();

                if (received > -1) {
                    int receivedCPacket = mRxBuffer.getInt();
                    int expectedPacket = mRxBuffer.getInt();
                    Status status = Status.fromByte(mRxBuffer.get());

                    switch (status) {
                        case LAST_OPERATION_FAILED:
                            // Resend this packet
                            retVal = uploadPacket(data, numberOfPackets, lastPacketSize, receivedCPacket, retries + 1);
                            break;
                        case WRONG_PACKET_RECEIVED:
                            // Send expected packet
                            retVal = uploadPacket(data, numberOfPackets, lastPacketSize, expectedPacket, retries + 1);
                            break;
                    }
                }
            }
        }

        return retVal;
    }

    private Status uploadFirmwareT(String firmwareFilename, boolean verify, int device,
                                   ProgressCallback progressCallback) {

        if (DEBUG) Log.d(TAG, "Starting Firmware Uploading...");

        ByteArrayFileLoader byteArrayFileLoader = new ByteArrayFileLoader(firmwareFilename).invoke();
        if (byteArrayFileLoader.failed()) return Status.ABORT;
        int fileSize = byteArrayFileLoader.getFileSize();
        mFirmwareImage = byteArrayFileLoader.getArr();

        // This description will be passed back to the calling code when the
        // upload finished event is fired
        mDescription = byteArrayFileLoader.getDescription();
        mDeviceFirmwareStorageSize = mDevices.get(device).SizeOfCode;

        return uploadFirmwareCore(verify, progressCallback, fileSize);
    }

    private Status uploadFirmwareT(ByteBuffer firmware, boolean verify, int device,
                                   ProgressCallback progressCallback) {

        if (DEBUG) Log.d(TAG, "Starting Firmware Uploading...");
        int fileSize = firmware.limit();
        mFirmwareImage = firmware;
        mDeviceFirmwareStorageSize = mDevices.get(device).SizeOfCode;

        return uploadFirmwareCore(verify, progressCallback, fileSize);
    }

    private Status uploadFirmwareCore(boolean verify, ProgressCallback progressCallback, int fileSize) {
        Status ret;

        if (mDeviceFirmwareStorageSize < fileSize) {
            if (DEBUG) Log.d(TAG, "ERROR file to big for device");
            return Status.ABORT;
        }

        long crc = padAndGenerateCrc(fileSize);
        if (DEBUG) Log.d(TAG, "NEW FIRMWARE CRC=" + crc);

        int actualFileSize = (int) mDeviceFirmwareStorageSize;

        if (!startUpload(actualFileSize, TransferTypes.FW, crc)) {
            ret = statusRequest();
            if (DEBUG) Log.d(TAG, "StartUpload failed");
            if (DEBUG) Log.d(TAG, "StartUpload returned:" + statusToString(ret));
            return ret;
        }

        fireOperationProgress(mContext.getString(R.string.message_dfu_erasing));

        if (DEBUG) Log.d(TAG, "Erasing memory");

        ret = waitForStatus(Status.UPLOADING, 2000);

        if (DEBUG) Log.d(TAG, "Erase returned: " + statusToString(ret));

        if (ret != Status.UPLOADING) {
            return ret;
        }

        fireOperationProgress(mContext.getString(R.string.message_dfu_uploading_firmware));
        if (!uploadData(actualFileSize, mFirmwareImage, progressCallback)) {
            ret = statusRequest();
            if (DEBUG) Log.d(TAG, "UPLOAD failed (upload data)");
            if (DEBUG) Log.d(TAG, "UploadData returned:" + statusToString(ret));
            return ret;
        }
        if (DEBUG) Log.d(TAG, String.format("Calculated CRC: %#x", crc));
        if (!endOperation()) {
            ret = statusRequest();
            if (DEBUG) Log.d(TAG, "UPLOAD failed (end operation)");
            if (DEBUG) Log.d(TAG, "EndOperation returned:" + statusToString(ret));
            return ret;
        }
        ret = statusRequest();
        if (ret != Status.LAST_OPERATION_SUCCESS && ret != Status.CRC_FAIL) {
            return ret;
        }

        if (verify) {
            fireOperationProgress(mContext.getString(R.string.message_dfu_verify_firmware));
            if (DEBUG) Log.d(TAG, "Starting code verification\n");
            ByteBuffer arr2 = ByteBuffer.allocate(actualFileSize);
            startDownloadT(arr2, actualFileSize, TransferTypes.FW,
                    new ProgressCallback() {
                        @Override
                        public void updateProgress(int progress) {
                            fireProgressUpdated(progress);
                        }
                    });
            if (mFirmwareImage != arr2) {
                if (DEBUG) Log.d(TAG, "Verify:FAILED\n");
                return Status.ABORT;
            }
        }

        if (DEBUG) Log.d(TAG, "Status=" + ret);
        if (DEBUG) Log.d(TAG, "Firmware Uploading succeeded\n");
        return ret;
    }

    @Nullable
    private Status waitForStatus(Status status, int timeout) {
        Status ret;
        long timeThen = System.currentTimeMillis();
        long timeNow;
        ret = statusRequest();

        // It would appear that the device will ignore attempts to communicate while it
        // is erasing, resulting in an IOException. This code will loop until we either
        // get a response or timeout has elapsed. If it times out, ABORT will be returned.
        while (ret != status) {
            if (DEBUG) Log.d(TAG, "Waiting for response");
            timeNow = System.currentTimeMillis();
            if (timeNow - timeThen > timeout) {
                // Give up after two seconds
                if (DEBUG) Log.d(TAG, "Timed out waiting for response");
                ret = Status.ABORT;
                break;
            } else {
                timeThen = timeNow;
            }
            ret = statusRequest();
        }
        return ret;
    }

    /**
     * Send data to the bootloader through the serial port
     *
     * @param data bytes to send to device
     */
    private int sendData(ByteBuffer data) {
        data.rewind();
        int retVal = mRawSerialIO.rawWriteData(data);
        data.clear();

        return retVal;
    }


    /**
     * Receive data from the bootloader.
     */
    private int receiveData(ByteBuffer data) {
        return mRawSerialIO.rawReadData(data);
    }

    /**
     * Used to pad the firmware image up to the size of the device's storage space
     * and calculate the CRC.
     *
     * @param arraySize size of firmware image to upload
     */
    private long padAndGenerateCrc(int arraySize) {

        // Get either the ByteBuffer's bytes or copy them to a new array
        // with padding to pad ByteBuffer size up to given size. Note we
        // are assuming that arraySize is equal to or less than mDeviceFirmwareStorageSize
        // because we checked earlier that the file size doesn't exceed
        // the device storage space.
        byte[] paddedArray = arraySize == mDeviceFirmwareStorageSize ?
                mFirmwareImage.array() :
                Arrays.copyOf(mFirmwareImage.array(), (int) mDeviceFirmwareStorageSize);

        // Arrays.copyOf pads with zeros, we want all ones
        Arrays.fill(paddedArray, arraySize, (int) mDeviceFirmwareStorageSize, (byte) 255);

        if (DEBUG)
            Log.d(TAG, "Device space: " + mDeviceFirmwareStorageSize + " Array size: " + paddedArray.length);

        Stm32Crc crc32 = new Stm32Crc();
        crc32.reset();
        crc32.update(paddedArray);
//        crc32.update(0xFFFFFFFF);

        // Check to see if we needed to add padding
        if (arraySize != paddedArray.length) {
            // Added padding so we need to update the firmware image
            mFirmwareImage = ByteBuffer.wrap(paddedArray);
        }

        return crc32.getValue();
    }

    private void fireProgressUpdated(int progress) {
        if (DEBUG) Log.d(TAG, "Fire progress update: " + progress);
        for (IAPEventListener curListener : mListeners) {
            curListener.progressUpdated(progress);
        }
    }

    private void fireDownloadFinished(Status status) {
        for (IAPEventListener curListener : mListeners) {
            curListener.downloadFinished(status);
        }
    }

    private void fireUploadFinished(Status status) {
        for (IAPEventListener curListener : mListeners) {
            curListener.uploadFinished(status, mDescription);
        }
    }

    private void fireOperationProgress(String status) {
        for (IAPEventListener curListener : mListeners) {
            curListener.operationProgress(status);
        }
    }

    /**
     * Gets the type of board connected
     */
    public BoardType getBoardType(int boardNum) {
        BoardType brdType = BoardType.UNKNOWN;

        // First of all, check what Board type we are talking to
        int board = mDevices.get(boardNum).ID;

        if (DEBUG) Log.d(TAG, "Board model: " + board);
        switch (board >> 8) {
            case BOARD_ID_MB: // Mainboard family
                brdType = BoardType.MAINBOARD;
                break;
            case BOARD_ID_PIP: // PIP RF Modem
                brdType = BoardType.OPLM;
                break;
            case BOARD_ID_REVO: // Revo board
                brdType = BoardType.REVO;
                break;
        }
        return brdType;
    }

    enum TransferTypes {
        FW,
        DESCRIPTION
    }

    enum CompareType {
        CRC,
        BYTE_TO_BYTE
    }

    public enum Status {
        IAP_IDLE(0x00), // 0
        UPLOADING(0x01), // 1
        WRONG_PACKET_RECEIVED(0x02), // 2
        TOO_MANY_PACKETS(0x03), // 3
        TOO_FEW_PACKETS(0x04), // 4
        LAST_OPERATION_SUCCESS(0x05), // 5
        DOWNLOADING(0x06), // 6
        IDLE(0x07), // 7
        LAST_OPERATION_FAILED(0x08), // 8
        UPLOADING_STARTING(0x09), // 9
        OUTSIDE_DEV_CAPABILITIES(0x0a), // 10
        CRC_FAIL(0x0b), // 11
        FAILED_JUMP(0x0c), // 12
        ABORT(0x0d); // 13

        private final int mValue;

        Status(int value) {
            mValue = value;
        }

        public static Status fromByte(byte value) {
            Status retVal = null;

            switch (value) {
                case 0x00:
                    retVal = IAP_IDLE;
                    break;
                case 0x01:
                    retVal = UPLOADING;
                    break;
                case 0x02:
                    retVal = WRONG_PACKET_RECEIVED;
                    break;
                case 0x03:
                    retVal = TOO_MANY_PACKETS;
                    break;
                case 0x04:
                    retVal = TOO_FEW_PACKETS;
                    break;
                case 0x05:
                    retVal = LAST_OPERATION_SUCCESS;
                    break;
                case 0x06:
                    retVal = DOWNLOADING;
                    break;
                case 0x07:
                    retVal = IDLE;
                    break;
                case 0x08:
                    retVal = LAST_OPERATION_FAILED;
                    break;
                case 0x09:
                    retVal = UPLOADING_STARTING;
                    break;
                case 0x0a:
                    retVal = OUTSIDE_DEV_CAPABILITIES;
                    break;
                case 0x0b:
                    retVal = CRC_FAIL;
                    break;
                case 0x0c:
                    retVal = FAILED_JUMP;
                    break;
                case 0x0d:
                    retVal = ABORT;
                    break;
            }

            return retVal;
        }

        public int getValue() {
            return mValue;
        }
    }

    enum Commands {
        RESERVED(0xB00B0000), // 0
        REQ_CAPABILITIES(0xB00B0001), // 1
        REP_CAPABILITIES(0xB00B0002), // 2
        ENTER_DFU(0xB00B0003), // 3
        JUMP_FW(0xB00B0004), // 4
        RESET(0xB00B0005), // 5
        ABORT_OPERATION(0xB00B0006), // 6
        UPLOAD(0xB00B0007), // 7
        UPLOAD_START(0xB00B0107), // 7
        OP_END(0xB00B0008), // 8
        DOWNLOAD_REQ(0xB00B0009), // 9
        DOWNLOAD(0xB00B000a), // 10
        STATUS_REQUEST(0xB00B000b), // 11
        STATUS_REP(0xB00B000c); // 12

        private final int mValue;

        Commands(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }
    }

    enum BoardType {
        UNKNOWN(0),
        MAINBOARD(1),
        OPLM(3),
        REVO(9);

        private final int mValue;

        BoardType(int value) {
            mValue = value;
        }

        public int getValue() {
            return mValue;
        }
    }

    private interface ProgressCallback {
        void updateProgress(int progress);
    }

    public class Device {
        public int ID;
        public long FW_CRC;
        public int BL_Version;
        public int SizeOfDesc;
        public int SizeOfCode; // Size of space on device for holding firmware
        public boolean Readable;
        public boolean Writable;
    }

    private class ByteArrayFileLoader {
        private final String mFilename;
        private boolean mFailed;
        private int mFileSize;
        private ByteBuffer mArr;
        private byte[] mDescription;

        public ByteArrayFileLoader(String filename) {
            mFilename = filename;
        }

        boolean failed() {
            return mFailed;
        }

        public int getFileSize() {
            return mFileSize;
        }

        public ByteBuffer getArr() {
            return mArr;
        }

        public ByteArrayFileLoader invoke() {
            File file = new File(mFilename);
            mFileSize = (int) file.length();
            InputStream inStream = null;
            ByteArrayOutputStream outStream = null;
            try {
                byte[] buffer = new byte[1024];
                inStream = new BufferedInputStream(new FileInputStream(file));
                outStream = new ByteArrayOutputStream();
                int bytesRead = inStream.read(buffer);
                while (bytesRead > -1) {
                    outStream.write(buffer, 0, bytesRead);
                    bytesRead = inStream.read(buffer);
                }

                byte[] outStreamBytes = outStream.toByteArray();
                int fileSize = outStreamBytes.length;
                mDescription = Arrays.copyOfRange(outStreamBytes, fileSize - 100, fileSize);

                // Pad file to four byte boundary
                if (mFileSize % 4 != 0) {
                    int pad = mFileSize / 4;
                    ++pad;
                    pad = pad * 4;
                    pad = pad - mFileSize;
                    byte[] paddingBuffer = new byte[pad];
                    Arrays.fill(paddingBuffer, (byte) 255);
                    outStream.write(paddingBuffer);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                if (DEBUG) Log.d(TAG, "Can't open file");
                mFailed = true;
                return this;
            } catch (IOException e) {
                e.printStackTrace();
                if (DEBUG) Log.d(TAG, "Can't open file");
                mFailed = true;
                return this;
            } finally {
                if (inStream != null) {
                    try {
                        inStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (outStream != null) {
                    try {
                        outStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            if (DEBUG) Log.d(TAG, "Bytes Loaded=" + mFileSize);

            mArr = ByteBuffer.wrap(outStream.toByteArray());
            mFailed = false;
            return this;
        }

        public byte[] getDescription() {
            return mDescription;
        }
    }

    /**
     * Used to calculate the number of 14 DWORD packets required to upload
     * firmware. Also calculates the size of the last packet.
     */
    private class PacketCountCalculator {
        private final int mNumberOfBytes;
        private int mLastPacketCount;
        private int mNumberOfPackets;

        public PacketCountCalculator(int numberOfBytes) {
            mNumberOfBytes = numberOfBytes;
        }

        public int getLastPacketSize() {
            return mLastPacketCount;
        }

        public int getNumberOfPackets() {
            return mNumberOfPackets;
        }

        public PacketCountCalculator invoke() {
            mNumberOfPackets = mNumberOfBytes / 4 / 14;
            int pad = (mNumberOfBytes - (mNumberOfPackets * 4 * 14)) / 4;

            if (pad == 0) {
                mLastPacketCount = 14;
            } else {
                ++mNumberOfPackets;
                mLastPacketCount = pad;
            }
            return this;
        }
    }

    private class IAPTask extends AsyncTask<Void, Integer, IAP.Status> {
        private final Commands mRequestedOperation;
        private int mRequestSize;
        private TransferTypes mRequestTransferType;
        private ByteBuffer mRequestStorage;
        private String mRequestFilename;
        private boolean mRequestVerify;
        private int mRequestDevice;

        /**
         * Create task to upload firmware from the given file name.
         *
         * @param filename file to upload
         * @param device   device to upload to
         * @param verify   true to verify upload, false otherwise
         */
        public IAPTask(String filename, int device, boolean verify) {
            mRequestedOperation = Commands.UPLOAD;
            mRequestFilename = filename;
            mRequestDevice = device;
            mRequestVerify = verify;
        }

        /**
         * Create task to upload firmware from the given ByteBuffer
         *
         * @param firmwareImage firmware bytes to upload
         * @param device        device to upload to
         * @param verify        true to verify upload, false otherwise
         */
        public IAPTask(ByteBuffer firmwareImage, int device, boolean verify) {
            mRequestedOperation = Commands.UPLOAD;
            mRequestStorage = firmwareImage;
            mRequestDevice = device;
            mRequestVerify = verify;
        }

        /**
         * Create task to download firmware or description to the given ByteBuffer.
         *
         * @param sizeOfCode    expected maximum code size
         * @param transferType  either FW or DESCRIPTION
         * @param firmwareArray array to download to
         */
        public IAPTask(int sizeOfCode, TransferTypes transferType, ByteBuffer firmwareArray) {
            mRequestedOperation = Commands.DOWNLOAD;
            mRequestSize = sizeOfCode;
            mRequestTransferType = transferType;
            mRequestStorage = firmwareArray;
        }

        @Override
        protected IAP.Status doInBackground(Void... voids) {
            IAP.Status retVal = IAP.Status.IAP_IDLE;
            switch (mRequestedOperation) {
                case DOWNLOAD:
                    retVal = startDownloadT(mRequestStorage, mRequestSize, mRequestTransferType,
                            new ProgressCallback() {
                                @Override
                                public void updateProgress(int progress) {
                                    publishProgress(progress);
                                }
                            });
                    break;
                case UPLOAD:
                    if (mRequestStorage == null) {
                        retVal = uploadFirmwareT(mRequestFilename, mRequestVerify, mRequestDevice,
                                new ProgressCallback() {
                                    @Override
                                    public void updateProgress(int progress) {
                                        publishProgress(progress);
                                    }
                                });
                    } else {
                        retVal = uploadFirmwareT(mRequestStorage, mRequestVerify, mRequestDevice,
                                new ProgressCallback() {
                                    @Override
                                    public void updateProgress(int progress) {
                                        publishProgress(progress);
                                    }
                                });
                    }
                    break;
                default:
                    break;
            }
            return retVal;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            fireProgressUpdated(values[0]);
        }

        @Override
        protected void onPostExecute(IAP.Status status) {
            switch (mRequestedOperation) {
                case DOWNLOAD:
                    fireDownloadFinished(status);
                    break;
                case UPLOAD:
                    fireUploadFinished(status);
                    break;
            }

            mIAPTask = null;
        }
    }
}
