/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import org.opng.mobilegcs.R;

import java.util.ArrayList;

public class IconItemArrayAdapter<T extends AppMenuItem> extends ArrayAdapter<T> {

    public IconItemArrayAdapter(Context context, ArrayList<T> items, int itemLayout) {
        super(context, itemLayout, R.id.itemTitle, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = super.getView(position, convertView, parent);

        //TextView titleView = (TextView) view.findViewById(R.id.title);
        ImageView iconView = (ImageView) view.findViewById(R.id.itemIcon);

        //titleView.setText( mNavItems.get(position).mTitle );
        iconView.setImageResource(getItem(position).getIcon());

        return view;
    }
}
