/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.filemanager;

import java.io.File;
import java.io.FileFilter;

class BoardTypeFilter implements FileFilter {

    private final int mBoardId;

    public BoardTypeFilter(String filter) {
        mBoardId = Integer.parseInt(filter);
    }

    @Override
    public boolean accept(File file) {
        boolean retVal = file.isDirectory();

        if (!retVal) {
            switch (mBoardId) {
                case 0x0301: // OPLM
                    retVal = file.getName().contains("oplinkmini");
                    break;
                case 0x0903: // Revo
                case 0x0904:
                    retVal = file.getName().contains("revolution");
                    break;
                case 0x0905: // Revo Nano
                    retVal = file.getName().contains("revonano");
                    break;
                default:
                    break;

            }
        }
        return retVal;
    }
}
