/*
 * Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.uavtalk.uavobjects;

import org.opng.uavtalk.UAVObjectManager;

public class TelemObjectsInitialize {

    public static void register(UAVObjectManager objMngr) {
        try {
            objMngr.registerObject(new FirmwareIAPObj());
            objMngr.registerObject(new FlightTelemetryStats());
            objMngr.registerObject(new GCSTelemetryStats());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
