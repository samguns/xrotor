/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import java.nio.ByteBuffer;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class Variant {
    private static final NumberFormat mNumberFormat = NumberFormat.getInstance(Locale.getDefault());
    private Number mNumberValue = null;
    private String mStringValue = null;
    private boolean mIsUnsigned = false;
    private boolean mIsValid = true;
    private boolean mIsString;

    public Variant(Byte byteValue) {
        mNumberValue = byteValue;
    }

    /**
     * Use this constructor to represent an unsigned byte with
     * a short.
     *
     * @param byteValue  byte value to store
     * @param isUnsigned set true to indicate unsigned
     */
    public Variant(byte byteValue, boolean isUnsigned) {
        mNumberValue = (short) byteValue & 0xFF;
        mIsUnsigned = isUnsigned;
    }

    public Variant(Short shortValue) {
        mNumberValue = shortValue;
    }

    /**
     * Use this constructor to represent an unsigned short with
     * an int.
     *
     * @param shortValue short value to store
     * @param isUnsigned set true to indicate unsigned
     */
    public Variant(short shortValue, boolean isUnsigned) {
        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.putShort(shortValue);
        mNumberValue = ((int) buffer.get(0) & 0xFF) << 8 | ((int) buffer.get(1) & 0xFF);
        mIsUnsigned = isUnsigned;
    }

    public Variant(Integer intValue) {
        mNumberValue = intValue;
    }

    /**
     * Use this constructor to represent an unsigned int with
     * a long.
     *
     * @param intValue   int value to store
     * @param isUnsigned set true to indicate unsigned
     */
    public Variant(int intValue, boolean isUnsigned) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.putInt(intValue);
        mNumberValue = (long) ((int) buffer.get(0) & 0xFF) << 24 |
                ((int) buffer.get(1) & 0xFF) << 16 |
                ((int) buffer.get(2) & 0xFF) << 8 |
                (int) buffer.get(3) & 0xFF;
        mIsUnsigned = isUnsigned;
    }

    public Variant(Long longValue) {
        mNumberValue = longValue;
    }

    public Variant(Float floatValue) {
        mNumberValue = floatValue;
    }

    public Variant(Double doubleValue) {
        mNumberValue = doubleValue;
    }

    public Variant(String stringValue) {
        mStringValue = stringValue;
        mIsString = true;
    }

    private Variant() {
        mIsValid = false;
    }

    public Variant(Object object) {
        if (object instanceof Byte) {
            mNumberValue = (Byte) object;
        } else if (object instanceof Short) {
            mNumberValue = (Short) object;
        } else if (object instanceof Integer) {
            mNumberValue = (Integer) object;
        } else if (object instanceof Long) {
            mNumberValue = (Long) object;
        } else if (object instanceof Float) {
            mNumberValue = (Float) object;
        } else if (object instanceof Double) {
            mNumberValue = (Double) object;
        } else if (object instanceof String) {
            mStringValue = (String) object;
        }
    }

    public static Variant emptyVariant() {
        return new Variant();
    }

    public Byte toByte() {
        Byte retVal = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.byteValue();
        } else if (stringNotNullOrEmpty()) {
            retVal = Byte.valueOf(mStringValue);
        }

        return retVal;
    }

    public Short toUByte() {
        Short retVal = null;
        Number temp = null;

        if (mNumberValue != null) {
            retVal = (short) (mNumberValue.shortValue() & 0x00FF);
        } else if (stringNotNullOrEmpty()) {
            try {
                temp = mNumberFormat.parse(mStringValue).shortValue();
            } catch (NumberFormatException e) {
                temp = Double.valueOf(mStringValue);
            } catch (ParseException e) {
                temp = Short.valueOf(mStringValue);
            } finally {
                if (temp != null) {
                    retVal = (short) (temp.shortValue() & 0x00FF);
                }
            }
        }

        return retVal;
    }

    public Short toShort() {
        Short retVal = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.shortValue();
        } else if (stringNotNullOrEmpty()) {
            try {
                retVal = mNumberFormat.parse(mStringValue).shortValue();
            } catch (ParseException e) {
                retVal = Short.valueOf(mStringValue);
            }
        }

        return retVal;
    }

    public Integer toUShort() {
        Integer retVal = null;
        Number temp = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.intValue() & 0x0000FFFF;
        } else if (stringNotNullOrEmpty()) {
            try {
                temp = mNumberFormat.parse(mStringValue).intValue();
            } catch (NumberFormatException e) {
                temp = Double.valueOf(mStringValue);
            } catch (ParseException e) {
                temp = Integer.valueOf(mStringValue);
            } finally {
                if (temp != null) {
                    retVal = temp.intValue() & 0x0000FFFF;
                }
            }
        }

        return retVal;
    }

    public Integer toInt() {
        Integer retVal = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.intValue();
        } else if (stringNotNullOrEmpty()) {
            try {
                retVal = mNumberFormat.parse(mStringValue).intValue();
            } catch (ParseException e) {
                retVal = Integer.valueOf(mStringValue);
            }
        }

        return retVal;
    }

    public Long toUInt() {
        Long retVal = null;
        Number temp = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.longValue();
        } else if (stringNotNullOrEmpty()) {
            try {
                temp = mNumberFormat.parse(mStringValue).longValue();
            } catch (NumberFormatException e) {
                temp = Double.valueOf(mStringValue);
            } catch (ParseException e) {
                temp = Long.valueOf(mStringValue);
            } finally {
                if (temp != null) {
                    retVal = temp.longValue();
                }
            }
        }

        return retVal;
    }

    public Long toLong() {
        Long retVal = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.longValue();
        } else if (stringNotNullOrEmpty()) {
            try {
                retVal = mNumberFormat.parse(mStringValue).longValue();
            } catch (ParseException e) {
                retVal = Long.valueOf(mStringValue);
            }
        }

        return retVal;
    }

    public Float toFloat() {
        Float retVal = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.floatValue();
        } else if (stringNotNullOrEmpty()) {
            try {
                retVal = mNumberFormat.parse(mStringValue).floatValue();
            } catch (ParseException e) {
                retVal = Float.valueOf(mStringValue);
            }
        }

        return retVal;
    }

    public Double toDouble() {
        Double retVal = null;

        if (mNumberValue != null) {
            retVal = mNumberValue.doubleValue();
        } else if (stringNotNullOrEmpty()) {
            try {
                retVal = mNumberFormat.parse(mStringValue).doubleValue();
            } catch (ParseException e) {
                retVal = Double.valueOf(mStringValue);
            }
        }

        return retVal;
    }

    @Override
    public String toString() {
        return toString(null);
    }

    public String toString(NumberFormat formatter) {
        String retVal;
        if (mNumberValue != null) {
            if (formatter != null) {
                retVal = formatter.format(mNumberValue);
            } else {
                retVal = mNumberValue.toString();
            }
        } else { // Don't check string for null as we might actually want a null string
            retVal = mStringValue;
        }
        return retVal;
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isEmpty() {
        return mNumberValue == null && mStringValue == null;
    }

    public boolean isValid() {
        return mIsValid;
    }

    public boolean isString() {
        return mIsString;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((mNumberValue == null) ? 0 : mNumberValue.hashCode());
        result = prime * result
                + ((mStringValue == null) ? 0 : mStringValue.hashCode());
        result = prime * result
                + (mIsUnsigned ? 1 : 0);
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Variant other = (Variant) obj;
        if (mIsUnsigned != other.mIsUnsigned)
            return false;
        if (mNumberValue == null) {
            if (other.mNumberValue != null)
                return false;
        } else if (!mNumberValue.equals(other.mNumberValue))
            return false;
        if (mStringValue == null) {
            if (other.mStringValue != null)
                return false;
        } else if (!mStringValue.equals(other.mStringValue))
            return false;
        return true;
    }

    private boolean stringNotNullOrEmpty() {
        return mStringValue != null && mStringValue.length() > 0;
    }
}
