/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.view.ActionProvider;
import android.support.v4.view.MenuItemCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.systemalarms.SystemAlarmsActionProvider;
import org.opng.mobilegcs.telemetry.UAVTalkActionProvider;
import org.opng.mobilegcs.telemetry.ui.TelemetryStatusActionProvider;
import org.opng.uavtalk.UAVObjectManager;

import java.util.ArrayList;

/**
 * This class is intended to be encapsulated by an Activity that wishes to display
 * UAVTalk based widgets in its Action Bar. Use this class when your activity already
 * extends another Activity derived class and therefore can't extend {@link org.opng.mobilegcs.util.TelemetryActivity}.
 * <p/>
 * This class has various methods that should be called by various Activity lifecycle
 * callbacks to handle the lifetime and state of the telemetry widget. Internally an
 * instance of {@link org.opng.mobilegcs.util.TelemetryServiceHandler}
 * is used to start, communicate with and stop the OPTelemetryService as required.
 */
public class ActivityTelemetryConnectionHandler {
    public static final int TELEMETRY_STATUS = 1;
    public static final int SYSTEM_ALARMS = 2;
    // Logging setup
    private static final String TAG = "ActvtyTelemConnHandler";
    private static final int LOGLEVEL = 0;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static final String CONNECTION_STARTED = "ConnectionStarted";
    private static boolean WARN = LOGLEVEL > 0;
    private final TelemetryServiceHandler.ConnectionListener mTelemetryServiceHandlerListener;
    private final ArrayList<UAVTalkActionProvider> mActionProviders = new ArrayList<>();
    private TelemetryServiceHandler mTelemetryServiceHandler;
    private TelemetryServiceHandler.ConnectionListener mUserTelemetryServiceHandlerListener;
    private boolean mShouldRestartConnection;

    /**
     * Create an instance of this class.
     * The constructor will create a listener that will listen out for connect and disconnect
     * events from {@link org.opng.mobilegcs.util.TelemetryServiceHandler},
     * allowing active action bar widgets to start and stop updating their displays.
     */
    public ActivityTelemetryConnectionHandler() {
        if (DEBUG) Log.d(TAG, "Creating new instance");
        mTelemetryServiceHandlerListener = new TelemetryServiceHandler.ConnectionListener() {
            @Override
            public void onConnect(UAVObjectManager objectManager) {
                for (UAVTalkActionProvider curActionProvider : mActionProviders) {
                    if (curActionProvider != null) {
                        curActionProvider.connected(objectManager);
                    } else if (DEBUG)
                        Log.d(TAG, "ConnectionListener: onConnect with null action provider");
                }

                fireTelemetryConnected(objectManager);
            }

            @Override
            public void onDisconnect() {
                for (UAVTalkActionProvider curActionProvider : mActionProviders) {
                    if (curActionProvider != null) {
                        curActionProvider.disconnected();
                    } else if (DEBUG)
                        Log.d(TAG, "ConnectionListener: onDisconnect with null action provider");
                }

                fireTelemetryDisconnected();
            }
        };
    }

    /**
     * Lazily initialize the TelemetryServiceHandler. This will create the handler and bind
     * it to the telemetry service. A connection to telemetry will not be made until
     * doConnect is called.
     *
     * @return the TelemetryServiceHandler used to communicate with the OPTelemetryService
     */
    private TelemetryServiceHandler getTelemetryServiceHandler(Context context) {
        if (mTelemetryServiceHandler == null && context != null) {
            mTelemetryServiceHandler = new TelemetryServiceHandler(mTelemetryServiceHandlerListener);

            // Bind but don't connect to the service if it isn't already connected
            mTelemetryServiceHandler.bindToTelemetryService(context, false);
        }

        return mTelemetryServiceHandler;
    }

    /**
     * Used to start the OPTelemetryService if it isn't already running and connect
     * to the flight controller. This method is intended to be called from a menu item
     * handler but can also be used elsewhere.
     *
     * @param context application's context, usually the owning Activity
     */
    public void doConnect(Context context) {
        TelemetryServiceHandler handler = getTelemetryServiceHandler(context);

        if (handler != null) {
            handler.connect();
        }
    }

    /**
     * Used to disconnect the telemetry service from the flight controller. This method is
     * intended to be called from a menu item handler but can also be used elsewhere.
     *
     * @param context application's context, usually the owning Activity
     */
    public void doDisconnect(Context context) {
        TelemetryServiceHandler handler = getTelemetryServiceHandler(context);

        if (handler != null) {
            handler.disconnect();
        }
    }

    /**
     * Returns whether or not we are connected to telemetry.
     *
     * @param context used to create a TelemetryServiceHandler instance if it hasn't
     *                already been created
     * @return true if we are connected to telemetry, false otherwise
     */
    public boolean isTelemetryConnected(Context context) {
        TelemetryServiceHandler handler = getTelemetryServiceHandler(context);
        return handler.isTelemetryConnected();
    }

    /**
     * Returns whether or not we have a 'physical' connection: USB, bluetooth
     * or TCP/IP.
     *
     * @param context used to create a TelemetryServiceHandler instance if it hasn't
     *                already been created
     * @return true if we are communicating with the hardware, false otherwise
     */
    public boolean isTelemetryTaskConnected(Context context) {
        TelemetryServiceHandler handler = getTelemetryServiceHandler(context);
        return handler.isTelemetryTaskConnected();
    }

    public void startLogging(Context context) {
        TelemetryServiceHandler handler = getTelemetryServiceHandler(context);

        if (handler != null) {
            handler.startLogging();
        }

    }

    public void stopLogging(Context context) {
        TelemetryServiceHandler handler = getTelemetryServiceHandler(context);

        if (handler != null) {
            handler.stopLogging();
        }

    }

    /**
     * Used to unbind from the telemetry service. Usually this method does not need to be called
     * directly as it is also called from onActivityDestroy but it can be used to stop
     * the telemetry service directly.
     *
     * @param context application's context, usually the owning Activity
     */
    private void unbindFromTelemetryService(Context context) {
        if (DEBUG) Log.d(TAG, "Unbinding from telemetry");

        // Using _telemetryServiceHandler directly here to avoid lazily initialising it
        // if it's not already running
        if (mTelemetryServiceHandler != null) {
            mTelemetryServiceHandler.unbindFromTelemetryService(context);
        }
    }

    /**
     * This method is used to add one or more predefined widgets to the Activity's Action Bar.
     * The widgetFlags parameter defines which widgets will be displayed. This should be a
     * combination of the static int values defined at the top of this file.
     * This method will usually be called from the owning Activity's onCreateOptionsMenu method
     * but can be called at any point as long as you have a reference to the menu. The widgets
     * are added to new MenuItems which are then added to the given Menu.
     *
     * @param menu        menu to add the telemetry widget to
     * @param context     application's context, usually the owning Activity
     * @param widgetFlags set which of the predefined widgets to add to the action bar
     */
    public void addActionProviderWidgets(Menu menu, Context context, int widgetFlags) {
        if (menu != null) {
            if ((widgetFlags & TELEMETRY_STATUS) == TELEMETRY_STATUS) {
                if (DEBUG) Log.d(TAG, "Adding telemetry gauge to action bar");
                TelemetryStatusActionProvider telemetryStatusActionProvider = new TelemetryStatusActionProvider(context);
                createActionProviderMenuItem(menu, telemetryStatusActionProvider, R.string.title_action_telemetry_gauge);
            }

            if ((widgetFlags & SYSTEM_ALARMS) == SYSTEM_ALARMS) {
                if (DEBUG) Log.d(TAG, "Adding system alarms indicator to action bar");
                SystemAlarmsActionProvider alarmsActionProvider = new SystemAlarmsActionProvider(context);
                createActionProviderMenuItem(menu, alarmsActionProvider, R.string.title_action_system_alarms);
            }

            // Check to see if we are already connected and if so, connect up the
            // widgets and start them updating. This situation arises from
            // onDestroy being called on the hosting activity while we are still
            // connected to telemetry. When onActivityResume is called as the activity
            // restarts, we auto-connect to telemetry. This all happens before the
            // new widgets are created (by this method) and they miss the
            // connected event fired by the telemetry service handler.

            if (telemetryIsConnected(context)) {
                if (DEBUG) Log.d(TAG, "Restoring saved widget values");
                for (UAVTalkActionProvider curActionProvider : mActionProviders) {
                    curActionProvider.connected(mTelemetryServiceHandler.getObjManager());
                    curActionProvider.setInitialValues();
                }
            }
        }
    }

    /**
     * Should be called from the owning Activity's onStop method. This will stop
     * the widgets from updating while the Activity is hidden.
     *
     * @param context application's context, usually the owning Activity
     */
    public void onActivityStop(Context context) {
        if (DEBUG) Log.d(TAG, "Activity onStop()");
        // Stop the telemetry widget updating while our activity is stopped
        for (UAVTalkActionProvider curActionProvider : mActionProviders) {
            if (curActionProvider != null) {
                curActionProvider.pause(telemetryIsConnected(context));
            }
        }
    }

    /**
     * Should be called from the owning Activity's onResume method. This will
     * start the widgets updating again when the activity becomes visible.
     * This method will also restart the telemetry connection if we were connected
     * before the activity was paused or stopped.
     *
     * @param context application's context, usually the owning Activity
     */
    public void onActivityResume(Context context) {
        if (DEBUG) Log.d(TAG, "Activity onResume()");

        for (UAVTalkActionProvider curActionProvider : mActionProviders) {
            // Resume updates on the telemetry widget.
            if (curActionProvider != null) {
                curActionProvider.resume();
            }
        }

        // Check to see if we were already connected before an activity pause or stop
        if (mShouldRestartConnection) {
            if (DEBUG) Log.d(TAG, "Reconnecting on activity resume");
            mShouldRestartConnection = false;
            // This call will restart telemetry if it's not running or bind to existing
            // if it is already running.
            doConnect(context);
        }
    }

    /**
     * Should be called from the owning Activity's onDestroy method. This is used to
     * make sure we unbind from the telemetry service when the activity is shut down.
     * Once there are no more bindings with the telemetry service, it will be shut
     * down.
     *
     * @param context application's context, usually the owning Activity
     */
    public void onActivityDestroy(Context context) {
        if (DEBUG) Log.d(TAG, "Activity onDestroy()");
        unbindFromTelemetryService(context);
    }

    /**
     * Should be called from the owning Activity's onSaveInstanceState method. This method
     * will save the current connected state and also the current state of each widget
     * so that we can reconnect to the flight controller and set up the widgets as they
     * were before we stopped.
     *
     * @param outState application's context, usually the owning Activity
     */
    public void onActivitySaveInstanceState(Bundle outState) {
        // We want to save the connected state here so we can set a flag that indicates
        // whether or not we should reconnect when onActivityResume is called.
        boolean isConnected = telemetryIsConnected(null);
        outState.putBoolean(CONNECTION_STARTED,
                isConnected);
        if (DEBUG)
            Log.d(TAG, "Telemetry will " + (isConnected ? "" : "not ") + "restart on resume");

        for (UAVTalkActionProvider curActionProvider : mActionProviders) {
            curActionProvider.saveState(outState);
        }
    }

    /**
     * Should be called from the owning Activity's onCreate method if the Bundle parameter is
     * non-null. This will restore the telemetry service's connection with the flight controller
     * and initialize all the widgets.
     *
     * @param savedInstanceState saved state
     */
    public void restoreState(Bundle savedInstanceState) {
        mShouldRestartConnection = savedInstanceState.getBoolean(CONNECTION_STARTED, false);

        for (UAVTalkActionProvider curActionProvider : mActionProviders) {
            curActionProvider.restoreState(savedInstanceState);
        }
    }

    /**
     * Adds a listener that will be notified of telemetry connection and disconnection.
     *
     * @param connectionListener listener to notify
     */
    public void setUserTelemetryServiceHandlerListener(TelemetryServiceHandler.ConnectionListener connectionListener) {
        mUserTelemetryServiceHandlerListener = connectionListener;
    }

    /**
     * Use this method to see if telemetry is connected or not. If the context parameter
     * is provided and we are not bound to the telemetry service, it will be used to bind
     * the service (starting it first if necessary). If context is null, we will not be
     * able to bind to telemetry and unless it is already running, we will return false
     * from this method.
     *
     * @param context used to start and bind the telemetry service as necessary when this
     *                is non-null
     * @return true if we are connected to the telemetry service, false if we are not. This
     * will also return false if we have have not bound to the telemetry service and context
     * is null.
     */
    private boolean telemetryIsConnected(Context context) {
        TelemetryServiceHandler handler = getTelemetryServiceHandler(context);
        return handler != null && handler.isTelemetryConnected();
    }

    /**
     * This method will add the given ActionProvider to the given menu with the given title
     * resource.
     *
     * @param menu          menu to add provider to
     * @param provider      ActionProvider to add to given menu, this should also implement
     *                      {@link org.opng.mobilegcs.telemetry.UAVTalkActionProvider}
     * @param titleResource resource id for title string
     */
    @SuppressLint("AlwaysShowAction")
    private void createActionProviderMenuItem(Menu menu, ActionProvider provider, int titleResource) {
        MenuItem newItem = menu.add(Menu.NONE, Menu.NONE, Menu.NONE, titleResource);
        MenuItemCompat.setActionProvider(newItem, provider);
        newItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        newItem.setVisible(true);
        if (provider instanceof UAVTalkActionProvider) {
            mActionProviders.add((UAVTalkActionProvider) provider);
        }
    }

    private void fireTelemetryConnected(UAVObjectManager objectManager) {
        if (mUserTelemetryServiceHandlerListener != null) {
            mUserTelemetryServiceHandlerListener.onConnect(objectManager);
        }
    }

    private void fireTelemetryDisconnected() {
        if (mUserTelemetryServiceHandlerListener != null) {
            mUserTelemetryServiceHandlerListener.onDisconnect();
        }
    }
}