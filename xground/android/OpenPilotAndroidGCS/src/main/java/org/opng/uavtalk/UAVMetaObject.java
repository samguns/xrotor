/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class UAVMetaObject extends UAVObject {

    private final UAVObject mParent;
    private final Metadata mOwnMetadata;
    private Metadata mParentMetadata;

    public UAVMetaObject(long objID, String name, UAVDataObject parent) {
        super(objID, true, name);
        mParent = parent;

        mOwnMetadata = new Metadata();

        mOwnMetadata.flags = 0; // TODO: Fix flags
        mOwnMetadata.gcsTelemetryUpdatePeriod = 0;
        mOwnMetadata.loggingUpdatePeriod = 0;


        List<String> modesBitField = new ArrayList<>();
        modesBitField.add("FlightReadOnly");
        modesBitField.add("GCSReadOnly");
        modesBitField.add("FlightTelemetryAcked");
        modesBitField.add("GCSTelemetryAcked");
        modesBitField.add("FlightUpdatePeriodic");
        modesBitField.add("FlightUpdateOnChange");
        modesBitField.add("GCSUpdatePeriodic");
        modesBitField.add("GCSUpdateOnChange");
        modesBitField.add("LoggingUpdatePeriodic");
        modesBitField.add("LoggingUpdateOnChange");

        List<UAVObjectField> fields = new ArrayList<>();
        fields.add(new UAVObjectField("Modes", "Metadata Modes", UAVObjectField.FieldType.BITFIELD, modesBitField, new ArrayList<String>()));
        fields.add(new UAVObjectField("Flight Telemetry Update Period", "ms", UAVObjectField.FieldType.UINT16, 1, null));
        fields.add(new UAVObjectField("GCS Telemetry Update Period", "ms", UAVObjectField.FieldType.UINT16, 1, null));
        fields.add(new UAVObjectField("Logging Update Period", "ms", UAVObjectField.FieldType.UINT16, 1, null));

        int numBytes = 0;
        for (UAVObjectField field : fields) {
            numBytes += field.getNumBytes();
        }

        // Initialize object

        // Initialize parent
        super.initialize(0);
        initializeFields(fields, ByteBuffer.allocate(numBytes), numBytes);

        // Setup metadata of parent
        mParentMetadata = parent.getDefaultMetadata();
    }

    @Override
    public boolean isMetadata() {
        return true;
    }

    /**
     * Get the parent object
     */
    public UAVObject getParentObject() {
        return mParent;
    }

    /**
     * Get the metadata of the metaobject
     */
    @Override
    public Metadata getMetadata() {
        return mOwnMetadata;
    }

    /**
     * Set the metadata of the metaobject, this function will
     * do nothing since metaobjects have read-only metadata.
     */
    @Override
    public void setMetadata(Metadata mdata) {
        // can not update metaobject's metadata
    }

    /**
     * Get the default metadata
     */
    @Override
    public Metadata getDefaultMetadata() {
        return mOwnMetadata;
    }

    /**
     * Get the metadata held by the metaobject
     */
    public Metadata getData() {
        return mParentMetadata;
    }

    /**
     * Set the metadata held by the metaobject
     */
    public void setData(Metadata mdata) {
        mParentMetadata = mdata;
        fireUpdatedAuto();
        fireUpdatedManual();
    }


}
