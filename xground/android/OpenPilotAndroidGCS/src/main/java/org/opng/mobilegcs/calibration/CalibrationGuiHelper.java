/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.calibration;

import android.view.View;

import org.opng.mobilegcs.util.UAVObjectToGuiUtils;
import org.opng.mobilegcs.util.Variant;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.CalibrationControl;
import org.opng.uavtalk.uavobjects.CalibrationFactory;
import org.opng.uavtalk.uavobjects.CalibrationStatus;
import org.opng.uavtalk.uavobjects.CalibrationTrigger;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Used by fragments or activities that have views that need to be bound to the various
 * calibration UAVOs to support calibration work flows.
 */
class CalibrationGuiHelper {
    private final UAVObjectToGuiUtils mObjectToGuiUtils;
    private final List<CalibrationStateChangedListener> mCalibrationStateChangedListeners = new CopyOnWriteArrayList<>();
    private CalibrationControl mCalibrationControl;
    private CalibrationStatus mCalibrationStatus;
    private CalibrationFactory mCalibrationFactory;
    private CalibrationTrigger mCalibrationTrigger;
    private UAVObjectField mCalStateOperation;
    private UAVObjectField mLevelCalState;
    private UAVObjectField mGyroBiasCalState;
    private UAVObjectField mMagCalState;
    private UAVObjectField mAccelCalState;
    private String mOldCalState = "";
    private UAVObjectField mCalFactoryGyro;
    private UAVObjectField mCalFactoryAccel;
    private UAVObjectField mCalFactoryGyroBias;
    private UAVObjectField mCalFactoryGyroSelfTest;
    private UAVObjectField mCalFactoryGyroStr;
    private UAVObjectField mCalFactoryGyroTestStatus;
    private UAVObjectField mCalTriggerOperation;

    /**
     * Creates a new instance of this class using the given UAVObjectManager to access the
     * UAVOs to be bound and the given UAVObjectToGuiUtils to perform the binding of views
     * to UAVOs.
     *
     * @param uavObjectManager used to access the UAVOs of interest
     * @param objectToGuiUtils used to perform the view to UAVO bindings
     */
    public CalibrationGuiHelper(UAVObjectManager uavObjectManager, UAVObjectToGuiUtils objectToGuiUtils) {
        mObjectToGuiUtils = objectToGuiUtils;

        init(uavObjectManager);
    }

    /**
     * Used to bind a view that can be used to select the current calibration to
     * be run. Generally this will be a spinner.
     *
     * @param calibrationSelectionView view, usually a spinner, used to select
     *                                 current calibration
     */
    public void bindCalibrationSelectionWidget(View calibrationSelectionView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationControl,
                mCalibrationControl.getField(CalibrationControl.FIELD_OPERATION), calibrationSelectionView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind a view that will display the current calibration operation.
     *
     * @param currentCalibrationView view to display name of current operation
     */
    public void bindCurrentCalibrationWidget(View currentCalibrationView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationStatus,
                mCalibrationStatus.getField(CalibrationControl.FIELD_OPERATION), currentCalibrationView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind a view that will display the current calibration operation's
     * state: whether it's idle, active, completed or errored.
     *
     * @param currentOperationStateView view to display current operation's state
     */
    public void bindCurrentOperationStateWidget(View currentOperationStateView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationStatus,
                mCalibrationStatus.getField(CalibrationStatus.FIELD_OPERATIONSTATE), currentOperationStateView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind a view that will display the current calibration operation's
     * error, if it's in a errored state.
     *
     * @param errorView view to display current error
     */
    public void bindCalibrationErrorWidget(View errorView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationStatus,
                mCalibrationStatus.getField(CalibrationStatus.FIELD_ERROR), errorView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind a view that will display the current level calibration state.
     *
     * @param levelCalStateView view to display current level calibration state
     */
    public void bindLevelCalibrationStateWidget(View levelCalStateView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationStatus,
                mLevelCalState, levelCalStateView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind a view that will display the current gyro bias calibration state.
     *
     * @param gyroBiasStateView view to display current gyro bias state
     */
    public void bindGyroBiasCalibrationStateWidget(View gyroBiasStateView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationStatus,
                mGyroBiasCalState, gyroBiasStateView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind a view that will display the current mag. calibration state.
     *
     * @param magStateView view to display the current mag. calibration state
     */
    public void bindMagCalibrationStateWidget(View magStateView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationStatus,
                mMagCalState, magStateView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind a view that will display the current accel. calibration state.
     *
     * @param accelStateView view to display the current accel. calibration state
     */
    public void bindAccelCalibrationStateWidget(View accelStateView) {
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationStatus,
                mAccelCalState, accelStateView,
                0, 1, true, null, 0);
    }

    /**
     * Used to bind views that will be used to display the current x, y and z values
     * during gyro calibration.
     *
     * @param xView will  display current x value
     * @param yView will display current y value
     * @param zView will display current z value
     */
    public void bindGyroCoordViews(View xView, View yView, View zView){
        bindCalibrationFactoryCoordViews(CalibrationFactory.FIELD_GYRO, xView, yView, zView);
    }

    /**
     * Used to bind views that will be used to display the current x, y and z values
     * during accel. calibration.
     *
     * @param xView will  display current x value
     * @param yView will display current y value
     * @param zView will display current z value
     */
    public void bindAccelCoordViews(View xView, View yView, View zView){
        bindCalibrationFactoryCoordViews(CalibrationFactory.FIELD_ACCEL, xView, yView, zView);
    }

    /**
     * Used to bind views that will be used to display the current x, y and z values
     * during gyro bias calibration.
     *
     * @param xView will  display current x value
     * @param yView will display current y value
     * @param zView will display current z value
     */
    public void bindGyroBiasCoordViews(View xView, View yView, View zView){
        bindCalibrationFactoryCoordViews(CalibrationFactory.FIELD_GYROBIAS, xView, yView, zView);
    }

    /**
     * Used to bind views that will be used to display the current x, y and z values
     * during gyro self test.
     *
     * @param xView will  display current x value
     * @param yView will display current y value
     * @param zView will display current z value
     */
    public void bindGyroSelfTestCoordViews(View xView, View yView, View zView){
        bindCalibrationFactoryCoordViews(CalibrationFactory.FIELD_GYROSELFTEST, xView, yView, zView);
    }

    /**
     * Used to bind views that will be used to display the current x, y and z values
     * during gyro str calibration.
     *
     * @param xView will  display current x value
     * @param yView will display current y value
     * @param zView will display current z value
     */
    public void bindGyroSTRCoordViews(View xView, View yView, View zView){
        bindCalibrationFactoryCoordViews(CalibrationFactory.FIELD_GYROSTR, xView, yView, zView);
    }

    /**
     * Used to bind views that will be used to display the success or failure of
     * each gyro calibration routine.
     *
     * @param biasResultView     will display gyro bias calibration result
     * @param selfResultTestView will display gyro self test result
     * @param strResultView      will display gyro str calibration result
     */
    public void bindGyroTestResultViews(View biasResultView, View selfResultTestView, View strResultView) {
        int biasElementIdx;
        int selfTestElementIdx;
        int strElementIdx;

        biasElementIdx = mCalFactoryGyroTestStatus.getElementNames().indexOf(CalibrationFactory.GYROSELFTESTSTATUS_BIAS);
        selfTestElementIdx = mCalFactoryGyroTestStatus.getElementNames().indexOf(CalibrationFactory.GYROSELFTESTSTATUS_SELFTEST);
        strElementIdx = mCalFactoryGyroTestStatus.getElementNames().indexOf(CalibrationFactory.GYROSELFTESTSTATUS_STR);

        final String[] strings = {CalibrationFactory.GYROSELFTESTSTATUS_BAD, CalibrationFactory.GYROSELFTESTSTATUS_GOOD};
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationFactory,
                mCalFactoryGyroTestStatus, biasResultView,
                biasElementIdx, 1, true, null, 0, strings);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationFactory,
                mCalFactoryGyroTestStatus, selfResultTestView,
                selfTestElementIdx, 1, true, null, 0, strings);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationFactory,
                mCalFactoryGyroTestStatus, strResultView,
                strElementIdx, 1, true, null, 0, strings);
    }

    /**
     * Called to indicate to the level and accel. calibration processes that a requested
     * orientation change has been carried out. Orientation changes will be notified via
     * the views bound with bindLevelCalibrationStateWidget and bindAccelCalibrationStateWidget.
     * This method will generally be called from a button click handler.
     */
    public void signalOrientationChangeComplete(){
        mCalTriggerOperation.setValue(new Variant(CalibrationTrigger.ACCELCAL_NEWORIENTATIONREADY));
        mCalibrationTrigger.updated();
    }

    /**
     * Implement a CalibrationStateChangedListener and add it with this method
     * if you want to do something more complex with state changes than simply display
     * the current state.
     *
     * @param listener will be notified of calibration state changes
     */
    public void addCalibrationStateChangedListener(CalibrationStateChangedListener listener) {
        mCalibrationStateChangedListeners.add(listener);
    }

    /**
     * Remove the listener when it is finished with
     *
     * @param listener to remove
     */
    public void removeCalibrationStateChangedListener(CalibrationStateChangedListener listener) {
        mCalibrationStateChangedListeners.remove(listener);
    }

    private void init(UAVObjectManager uavObjectManager) {
        mCalibrationControl = (CalibrationControl) uavObjectManager.getObject("CalibrationControl");
        mCalibrationStatus = (CalibrationStatus) uavObjectManager.getObject("CalibrationStatus");
        mCalibrationFactory = (CalibrationFactory) uavObjectManager.getObject("CalibrationFactory");
        mCalibrationTrigger = (CalibrationTrigger) uavObjectManager.getObject("CalibrationTrigger");
        mCalTriggerOperation = mCalibrationTrigger.getField(CalibrationTrigger.FIELD_ACCELCAL);
        initCalibrationStatus();
        initCalibrationFactory();
    }

    private void initCalibrationStatus() {
        mCalStateOperation = mCalibrationStatus.getField(CalibrationStatus.FIELD_OPERATION);
        mLevelCalState = mCalibrationStatus.getField(CalibrationStatus.FIELD_LEVELCALIBRATIONSTATE);
        mGyroBiasCalState = mCalibrationStatus.getField(CalibrationStatus.FIELD_GYROBIASCALIBRATIONSTATE);
        mMagCalState = mCalibrationStatus.getField(CalibrationStatus.FIELD_MAGCALIBRATIONSTATE);
        mAccelCalState = mCalibrationStatus.getField(CalibrationStatus.FIELD_ACCELCALIBRATIONSTATE);

        // Set up event handling for calibration state changes
        mCalibrationStatus.addUAVObjectEventListener(new UAVObjectEventAdapter() {
            @Override
            public void objectUpdated(UAVObject obj) {
                // Only proceed if we have listeners for calibration events
                if (!mCalibrationStateChangedListeners.isEmpty()) {
                    // Get the current calibration
                    String currentOperation = mCalStateOperation.getValue().toString();

                    // Get the new state value and see if it's actually changed
                    // since the last call.
                    String newState;
                    boolean fireEvent = true;

                    switch (currentOperation) {
                        case CalibrationControl.OPERATION_LEVELCALIBRATION:
                            newState = mLevelCalState.getValue().toString();
                            break;
                        case CalibrationControl.OPERATION_GYROBIASCALIBRATION:
                            newState = mGyroBiasCalState.getValue().toString();
                            break;
                        case CalibrationControl.OPERATION_ACCELCALIBRATION:
                            newState = mAccelCalState.getValue().toString();
                            break;
                        case CalibrationControl.OPERATION_COMPASSCALIBRATION:
                            newState = mMagCalState.getValue().toString();
                            break;
                        default:
                            // We have a current operation that doesn't have
                            // state, signal that we don't want to fire our
                            // event.
                            newState = "Unknown";
                            fireEvent = false;
                            break;
                    }

                    // Has value changed and is it an event that gives a state?
                    if (fireEvent && !newState.equals(mOldCalState)) {
                        // State has changed, save new state and fire event
                        mOldCalState = newState;
                        fireCalibrationStateChanged(currentOperation, newState);
                    }
                }
            }
        });
    }

    private void initCalibrationFactory() {
        mCalFactoryGyro = mCalibrationFactory.getField(CalibrationFactory.FIELD_GYRO);
        mCalFactoryAccel = mCalibrationFactory.getField(CalibrationFactory.FIELD_ACCEL);
        mCalFactoryGyroBias = mCalibrationFactory.getField(CalibrationFactory.FIELD_GYROBIAS);
        mCalFactoryGyroSelfTest = mCalibrationFactory.getField(CalibrationFactory.FIELD_GYROSELFTEST);
        mCalFactoryGyroStr = mCalibrationFactory.getField(CalibrationFactory.FIELD_GYROSTR);
        mCalFactoryGyroTestStatus = mCalibrationFactory.getField(CalibrationFactory.FIELD_GYROSELFTESTSTATUS);
    }

    private void bindCalibrationFactoryCoordViews(String field, View xView, View yView, View zView){
        int xElementIdx;
        int yElementIdx;
        int zElementIdx;
        String x = "x";
        String y = "y";
        String z = "z";
        UAVObjectField fieldToBind = null;

        switch(field){
            case CalibrationFactory.FIELD_GYRO:
                fieldToBind = mCalFactoryGyro;
                x = CalibrationFactory.GYRO_X;
                y = CalibrationFactory.GYRO_Y;
                z = CalibrationFactory.GYRO_Z;
                break;
            case CalibrationFactory.FIELD_ACCEL:
                fieldToBind = mCalFactoryAccel;
                x = CalibrationFactory.ACCEL_X;
                y = CalibrationFactory.ACCEL_Y;
                z = CalibrationFactory.ACCEL_Z;
                break;
            case CalibrationFactory.FIELD_GYROBIAS:
                fieldToBind = mCalFactoryGyroBias;
                x = CalibrationFactory.GYROBIAS_X;
                y = CalibrationFactory.GYROBIAS_Y;
                z = CalibrationFactory.GYROBIAS_Z;
                break;
            case CalibrationFactory.FIELD_GYROSELFTEST:
                fieldToBind = mCalFactoryGyroSelfTest;
                x = CalibrationFactory.GYROSELFTEST_X;
                y = CalibrationFactory.GYROSELFTEST_Y;
                z = CalibrationFactory.GYROSELFTEST_Z;
                break;
            case CalibrationFactory.FIELD_GYROSTR:
                fieldToBind = mCalFactoryGyroStr;
                x = CalibrationFactory.GYROSTR_X;
                y = CalibrationFactory.GYROSTR_Y;
                z = CalibrationFactory.GYROSTR_Z;
                break;
        }

        xElementIdx = fieldToBind != null ? fieldToBind.getElementNames().indexOf(x) : -1;
        yElementIdx = fieldToBind != null ? fieldToBind.getElementNames().indexOf(y) : -1;
        zElementIdx = fieldToBind != null ? fieldToBind.getElementNames().indexOf(z) : -1;

        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationFactory,
                fieldToBind, xView,
                xElementIdx, 1, true, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationFactory,
                fieldToBind, yView,
                yElementIdx, 1, true, null, 0);
        mObjectToGuiUtils.addUAVObjectToWidgetRelation(mCalibrationFactory,
                fieldToBind, zView,
                zElementIdx, 1, true, null, 0);
    }

    private void fireCalibrationStateChanged(String changedField, String newState) {
        for (CalibrationStateChangedListener curListener : mCalibrationStateChangedListeners) {
            curListener.stateChanged(changedField, newState);
        }
    }

    /**
     * Implement this to create a listener for calibration operation state changed
     * events.
     */
    public interface CalibrationStateChangedListener {
        /**
         * Whenever a calibration operation that reports state changes changes
         * state, this method will be called with both the current operation and
         * its new state.
         *
         * @param currentOperation      operation reporting a state change
         * @param currentOperationState new state
         */
        void stateChanged(String currentOperation, String currentOperationState);
    }
}
