/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;

public class TcpUAVTalk extends TelemetryTask {

    private static final String TAG = TcpUAVTalk.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    public static final boolean WARN = LOGLEVEL > 1;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean ERROR = LOGLEVEL > 0;
    private final SharedPreferences mSharedPreferences;
    private Socket mTcpSocket;

    public TcpUAVTalk(OPTelemetryService s) {
        super(s);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(s.getApplicationContext());

    }

    @Override
    boolean attemptConnection() {

        if (DEBUG) Log.d(TAG, "TcpUAVTalk attempting connection");
        String ipAddress = mSharedPreferences.getString("pref_tcp_ip_address", "");
        String prefPort = mSharedPreferences.getString("pref_tcp_port", "8000");
        int port = Integer.parseInt(prefPort);

        String message = "Opening connection to " + ipAddress + " at address " + port;

        Log.d(TAG, message);
        mTelemetryService.toastMessage(message);

        InetAddress serverAddress;
        try {
            serverAddress = InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e1) {
            Log.e(TAG, e1.getMessage());
            return false;
        }

        // open socket and attach to input and output UAVTalk streams
        mTcpSocket = null;
        try {
            mTcpSocket = new Socket(serverAddress, port);

            mInStream = mTcpSocket.getInputStream();
            mOutStream = mTcpSocket.getOutputStream();
        } catch (IOException e1) {
            try {
                if (mTcpSocket != null)
                    mTcpSocket.close();
            } catch (Exception ignored) {
            }
            mTelemetryService.toastMessage("TCP Connection Failed");
            return false;
        }

        // This method is running on the main telemetry service thread but we want to
        // call attemptSucceeded on the UAVTalk telemetry task thread
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                attemptSucceeded();
            }
        });

        mTelemetryService.toastMessage("TCP Connection Opened");

        return true;
    }

    @Override
    void disconnect() {
        if (DEBUG) Log.d(TAG, "TcpUAVTalk shutting down");
        super.disconnect();

        if (mTcpSocket != null) {
            try {
                mTcpSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    protected int rawReadData(ByteBuffer dataIn) {
        int retVal = -1;
        if (mTcpSocket != null) {
            try {
                retVal = mInStream.read(dataIn.array());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return retVal;
    }

    @Override
    protected int rawWriteData(ByteBuffer dataOut) {
        int retVal = 1;
        if (mTcpSocket != null) {
            try {
                mOutStream.write(dataOut.array());
            } catch (IOException e) {
                e.printStackTrace();
                retVal = -1;
            }
        }
        return retVal;
    }
}
