/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.wizard;

import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;

import org.opng.mobilegcs.R;

public class WizardStateMachine implements Parcelable {

    public static final Creator<WizardStateMachine> CREATOR = new Creator<WizardStateMachine>() {
        @Override
        public WizardStateMachine createFromParcel(Parcel source) {
            return new WizardStateMachine(source);
        }

        @Override
        public WizardStateMachine[] newArray(int size) {
            return new WizardStateMachine[size];
        }
    };
    private Button mBtnNext;
    private Button mBtnPrevious;
    private WizardState mState;
    private Handler mHandler;

    public WizardStateMachine(WizardState startState, Button btnPrevious, Button btnNext, Handler handler) {
        mState = startState;
        initAfterRestoreFromParcel(btnPrevious, btnNext, handler);
    }

    private WizardStateMachine(Parcel in) {
        mState = in.readParcelable(getClass().getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(mState, flags);
    }

    /**
     * Call this method after this instance has been restored from a parcel.
     * This will setup the gui handling and re-run the current state of the
     * state machine.
     *
     * @param btnPrevious previous button
     * @param btnNext     next button
     * @param handler     handler for running GUI updates
     */
    public void initAfterRestoreFromParcel(Button btnPrevious, Button btnNext, Handler handler) {
        mBtnPrevious = btnPrevious;
        mBtnNext = btnNext;
        mHandler = handler;

        initButtons();
        mState.process();

        updateNextButtonText();
        setPreviousButtonState();
    }

    public WizardState getCurrentState() {
        return mState;
    }

    private void initButtons() {
        if (mBtnPrevious != null) {
            mBtnPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stepBack();
                }
            });
            setPreviousButtonState();
        }

        if (mBtnNext != null) {
            mBtnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stepForward();
                }
            });
            updateNextButtonText();
        }
    }

    private void stepForward() {
        mState = mState.next();
        mState.process();

        updateNextButtonText();
        setPreviousButtonState();
    }

    private void updateNextButtonText() {
        if (mBtnNext != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    if (mState.isStartState()) {
                        mBtnNext.setText(R.string.title_action_wizard_start);
                    } else if (mState.isEndState()) {
                        mBtnNext.setText(R.string.title_action_wizard_finish);
                    } else {
                        mBtnNext.setText(R.string.title_action_wizard_next);
                    }
                }
            });
        }
    }

    private void stepBack() {
        mState = mState.previous();
        mState.process();

        updateNextButtonText();
        setPreviousButtonState();
    }

    private void setPreviousButtonState() {
        if (mBtnPrevious != null) {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mBtnPrevious.setEnabled(!mState.isStartState());
                }
            });
        }
    }
}
