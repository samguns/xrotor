/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.optune;

import android.util.JsonReader;
import android.util.SparseArray;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;

/**
 * Contains OPTune variables read from JSON, either from a URL or if that is not
 * available, from a local file. Getters are provided to allow the user to select
 * pitch/roll and yaw PID values based on flying styles selected in the GUI.
 */
public class OpTuneVars {
    private final SparseArray<Double> mEscFactors = new SparseArray<>();
    private final SparseArray<Double> mPitchRollProportionalFactors = new SparseArray<>();
    private final SparseArray<Double> mPitchRollIntegralFactors = new SparseArray<>();
    private final SparseArray<Double> mPitchRollDerivativeFactors = new SparseArray<>();
    private final SparseArray<Double> mYawProportionalFactors = new SparseArray<>();
    private final SparseArray<Double> mYawIntegralFactors = new SparseArray<>();
    private final SparseArray<Double> mYawDerivativeFactors = new SparseArray<>();
    private String mTitle;
    private String mCredits;
    private String mNotes;
    private boolean mVarsInvalid;

    public OpTuneVars(InputStream is) throws UnsupportedEncodingException {
        readJson(new InputStreamReader(is, "UTF-8"));
    }

    public OpTuneVars(String jsonString) {
        readJson(new StringReader(jsonString));
    }

    private void readJson(Reader reader) {
        JsonReader jsonReader = null;
        try {
            jsonReader = new JsonReader(reader);
            jsonReader.beginObject();

            while (jsonReader.hasNext()) {
                String name = jsonReader.nextName();

                if (name.equals("title")) {
                    mTitle = jsonReader.nextString();
                } else if (name.equals("credits")) {
                    mCredits = jsonReader.nextString();
                } else if (name.equals("note")) {
                    mNotes = jsonReader.nextString();
                } else if (name.contains("esc")) {
                    mEscFactors.append(extractIndex(name), jsonReader.nextDouble());
                } else if (name.contains("prP")) {
                    mPitchRollProportionalFactors.append(extractIndex(name), jsonReader.nextDouble());
                } else if (name.contains("prI")) {
                    mPitchRollIntegralFactors.append(extractIndex(name), jsonReader.nextDouble());
                } else if (name.contains("prD")) {
                    mPitchRollDerivativeFactors.append(extractIndex(name), jsonReader.nextDouble());
                } else if (name.contains("y_P")) {
                    mYawProportionalFactors.append(extractIndex(name), jsonReader.nextDouble());
                } else if (name.contains("y_I")) {
                    mYawIntegralFactors.append(extractIndex(name), jsonReader.nextDouble());
                } else if (name.contains("y_D")) {
                    mYawDerivativeFactors.append(extractIndex(name), jsonReader.nextDouble());
                } else {
                    jsonReader.skipValue();
                }
            }

            jsonReader.endObject();
        } catch (IOException | NumberFormatException e) {
            mVarsInvalid = true;
        } finally {
            if (jsonReader != null) {
                try {
                    jsonReader.close();
                } catch (IOException e) {
                    mVarsInvalid = true;
                }
            }
        }
    }

    public String getTitle() {
        return mTitle;
    }

    public String getCredits() {
        return mCredits;
    }

    public String getNotes() {
        return mNotes;
    }

    public double getEscFactor(int factor) {
        return mEscFactors.get(factor);
    }

    public double getProportionalFactor(int factor) {
        return mPitchRollProportionalFactors.get(factor);
    }

    public double getIntegralFactor(int factor) {
        return mPitchRollIntegralFactors.get(factor);
    }

    public double getDerivativeFactor(int factor) {
        return mPitchRollDerivativeFactors.get(factor);
    }

    public double getYawProportionalFactor(int factor) {
        return mYawProportionalFactors.get(factor);
    }

    public double getYawIntegralFactor(int factor) {
        return mYawIntegralFactors.get(factor);
    }

    public double getYawDerivativeFactor(int factor) {
        return mYawDerivativeFactors.get(factor);
    }

    public boolean isValid() {
        return !mVarsInvalid;
    }

    private int extractIndex(String name) {
        final String substring = name.substring(3);

        return Integer.parseInt(substring);
    }
}
