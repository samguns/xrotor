/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.uavtalk;

import android.util.Log;
import android.support.v4.util.LongSparseArray;

import org.opng.mobilegcs.util.Crc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class UAVTalk {

    private static final String TAG = "UAVTalk";
    private static final int LOGLEVEL = 2;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean WARN = LOGLEVEL > 1;
    private static final boolean ERROR = LOGLEVEL > 0;
    /**
     * Constants
     */
    private static final int SYNC_VAL = 0x3C;
    private static final int TYPE_MASK = 0xF8;
    private static final int TYPE_VER = 0x20;
    //! Packet contains an object
    private static final int TYPE_OBJ = (TYPE_VER);
    //! Packet is a request for an object
    private static final int TYPE_OBJ_REQ = (TYPE_VER | 0x01);
    //! Packet is an object with a request for an ack
    private static final int TYPE_OBJ_ACK = (TYPE_VER | 0x02);
    //! Packet is an ack for an object
    private static final int TYPE_ACK = (TYPE_VER | 0x03);
    private static final int TYPE_NACK = (TYPE_VER | 0x04);
    // header : sync(1), type (1), size(2), object ID(4), instance ID(2)
    private static final int HEADER_LENGTH = 10;
    private static final int CHECKSUM_LENGTH = 1;
    private static final int MAX_PAYLOAD_LENGTH = 256;
    private static final int MAX_PACKET_LENGTH = (HEADER_LENGTH
            + MAX_PAYLOAD_LENGTH + CHECKSUM_LENGTH);
    private static final long ALL_INSTANCES = 0xFFFFFFFF;
    private final static char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
    /**
     * Private data
     */
    private final InputStream mInStream;
    private final OutputStream mOutStream;
    private final UAVObjectManager mObjMngr;
    // Variables used by the receive state machine
    private final ByteBuffer mRxTmpBuffer /* 4 */;
    private final ByteBuffer mRxBuffer;
    private final LongSparseArray<LongSparseArray<Transaction>> mTransMap = new LongSparseArray<>();
    private Thread mInputProcessingThread = null;
    private int mRxType;
    private long mRxObjId;
    private long mRxInstId;
    private int mUavoNumDataBytes;
    private int mRxBytesSoFar;

    private int mRxCS;
    private int mRxCount;
    private int mPacketSize;
    private RxStateType mRxState;
    private ComStats mComStats = new ComStats();
    private OnTransactionCompletedListener mTransactionCompletedListener = null;

    /**
     * Constructor
     */
    public UAVTalk(InputStream inStream, OutputStream outStream,
                   UAVObjectManager objMngr) {
        mObjMngr = objMngr;
        mInStream = inStream;
        mOutStream = outStream;

        mRxState = RxStateType.STATE_SYNC;
        mRxBytesSoFar = 0;

        resetStats();
        mRxTmpBuffer = ByteBuffer.allocate(4);
        mRxTmpBuffer.order(ByteOrder.LITTLE_ENDIAN);
        mRxBuffer = ByteBuffer.allocate(MAX_PAYLOAD_LENGTH);
        mRxBuffer.order(ByteOrder.LITTLE_ENDIAN);
    }

    /**
     * A reference to the thread for processing the incoming stream.  Currently this method is ONLY
     * used for unit testing
     */
    public Thread getInputProcessThread() {
        if (mInputProcessingThread == null)

            mInputProcessingThread = new Thread() {
                @Override
                public void run() {
                    while (!currentThread().isInterrupted()) {
                        try {
                            processInputStream();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            };
        return mInputProcessingThread;
    }

    /**
     * Reset the statistics counters
     */
    public void resetStats() {
        mComStats = new ComStats();
    }

    /**
     * Get the statistics counters
     */
    public ComStats getComStats() {
        return mComStats;
    }

    /**
     * Process any data in the queue
     */
    public void processInputStream() {
        int val;

        if (!Thread.currentThread().isInterrupted()) {
            try {
                val = mInStream.read();

                while (val != -1 && !Thread.currentThread().isInterrupted()) {
                    if (VERBOSE)
                        Log.v(TAG, "Read: " + val + " [0x" + Integer.toHexString(val) + "]");

                    processInputByte(val);

                    if (mRxState == RxStateType.STATE_COMPLETE) {
                        if (receiveObject(mRxType, mRxObjId, mRxInstId, mRxBuffer)) {
                            mComStats.rxObjectBytes += mUavoNumDataBytes;
                            mComStats.rxObjects++;
                        }
                    }

                    val = mInStream.read();

                }
                if (DEBUG) Log.d(TAG, "End of input stream reached");
                //                retVal = false;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (DEBUG) Log.d(TAG, "Exiting processInputStream");
    }

    /**
     * Request an update for the specified object, on success the object data
     * would have been updated by the GCS. \param[in] obj Object to update
     * \param[in] allInstances If set true then all instances will be updated
     * \return Success (true), Failure (false)
     *
     * @throws IOException
     */
    public boolean sendObjectRequest(UAVObject obj, boolean allInstances) throws IOException {
        long instId = allInstances ? ALL_INSTANCES : obj.getInstID();

        return objectTransaction(TYPE_OBJ_REQ, obj.getObjID(), instId, obj);
    }

    /**
     * Send the specified object through the telemetry link. \param[in] obj
     * Object to send \param[in] acked Selects if an ack is required \param[in]
     * allInstances If set true then all instances will be updated \return
     * Success (true), Failure (false)
     *
     * @throws IOException
     */
    public boolean sendObject(UAVObject obj, boolean acked, boolean allInstances) throws IOException {
        boolean retVal = false;
        if (obj != null) {
            long instId = allInstances ? ALL_INSTANCES : obj.getInstID();

            if (acked) {
                retVal = objectTransaction(TYPE_OBJ_ACK, obj.getObjID(), instId, obj);
            } else {
                retVal = objectTransaction(TYPE_OBJ, obj.getObjID(), instId, obj);
            }
        } else {
            if (ERROR) Log.d(TAG, "sendObject: object null");
        }

        return retVal;
    }

    /**
     * UAVTalk takes care of it's own transactions but if the caller knows
     * it wants to give up on one (after a timeout) then it can cancel it
     */
    public void cancelPendingTransaction(UAVObject obj) {
        Transaction trans = findTransaction(obj.getObjID(), obj.getInstID());
        if (trans != null) {
            closeTransaction(trans);
        }

    }

    /**
     * Execute the requested transaction on an object. \param[in] obj Object
     * \param[in] type Transaction type TYPE_OBJ: send object, TYPE_OBJ_REQ:
     * request object update TYPE_OBJ_ACK: send object with an ack \param[in]
     * allInstances If set true then all instances will be updated \return
     * Success (true), Failure (false)
     *
     * @throws IOException
     */
    private boolean objectTransaction(int type, long objectId, long instanceId, UAVObject obj) throws IOException {
        // Send object depending on if a response is needed
        if (type == TYPE_OBJ_ACK || type == TYPE_OBJ_REQ) {
            if (DEBUG) Log.d(TAG, "Object transaction for " + obj.getName());
            if (transmitObject(type, objectId, instanceId, obj)) {
                openTransaction(type, objectId, instanceId);
                return true;
            } else {
                return false;
            }
        } else {
            if (DEBUG) Log.d(TAG, "No transaction for " + obj.getName());
            return type == TYPE_OBJ && transmitObject(type, objectId, instanceId, obj);
        }
    }

    /**
     * Process an byte from the telemetry stream. \param[in] rxbyte Received
     * byte \return Success (true), Failure (false).
     */
    /*synchronized*/
    private void processInputByte(int rxbyte) {
        assert (mObjMngr != null);

        if (mRxState == RxStateType.STATE_COMPLETE || mRxState == RxStateType.STATE_ERROR) {
            mRxState = RxStateType.STATE_SYNC;
        }
        // Update stats
        mComStats.rxBytes++;

        // update packet byte count
        mRxBytesSoFar++;

        // Receive state machine
        switch (mRxState) {
            case STATE_SYNC:

                if (rxbyte != SYNC_VAL) {
                    // continue until sync byte is matched
                    mComStats.rxSyncErrors++;

                    break;
                }

                // Initialize and update CRC
                mRxCS = Crc.updateCRC(0, rxbyte);

                mRxBytesSoFar = 1;

                // case local byte counter, don't forget to zero it after use.
                mRxCount = 0;


                mRxState = RxStateType.STATE_TYPE;
                break;

            case STATE_TYPE:

                // Update CRC
                mRxCS = Crc.updateCRC(mRxCS, rxbyte);

                if ((rxbyte & TYPE_MASK) != TYPE_VER) {
                    if (ERROR) Log.e(TAG, "UAVTalk - error : bad type");
                    mComStats.rxErrors++;
                    mRxState = RxStateType.STATE_ERROR;
                    break;
                }

                mRxType = rxbyte;
                if (VERBOSE)
                    Log.v(TAG, "Received packet type: " + mRxType + " [0x" + Integer.toHexString(rxbyte) + "]");
                mPacketSize = 0;

                mRxState = RxStateType.STATE_SIZE;
                break;

            case STATE_SIZE:

                // Update CRC
                mRxCS = Crc.updateCRC(mRxCS, rxbyte);

                if (mRxCount == 0) {
                    mPacketSize += rxbyte;
                    mRxCount++;
                    break;
                }

                mPacketSize += (rxbyte << 8) & 0xff00;
                mRxCount = 0;

                if (mPacketSize < HEADER_LENGTH
                        || mPacketSize > HEADER_LENGTH + MAX_PAYLOAD_LENGTH) {
                    // incorrect packet size
                    if (ERROR) Log.e(TAG, "UAVTalk - error : incorrect packet size");
                    mComStats.rxErrors++;
                    mRxState = RxStateType.STATE_ERROR;
                    break;
                }

                mRxState = RxStateType.STATE_OBJID;
                mRxTmpBuffer.position(0);
                break;

            case STATE_OBJID:

                // Update CRC
                mRxCS = Crc.updateCRC(mRxCS, rxbyte);
                if (VERBOSE) Log.v(TAG, "processInputByte: STATE_OBJID, rxCount = " + mRxCount);
                mRxTmpBuffer.put(mRxCount++, (byte) (rxbyte & 0xff));
                if (mRxCount < 4)
                    break;

                mRxCount = 0;
                mRxObjId = mRxTmpBuffer.getInt();

                // Message always contain an instance ID
                mRxInstId = 0;
                mRxState = RxStateType.STATE_INSTID;
                mRxTmpBuffer.position(0);
                break;

            case STATE_INSTID:

                // Update CRC
                mRxCS = Crc.updateCRC(mRxCS, rxbyte);

                mRxTmpBuffer.put(mRxCount++, (byte) rxbyte);
                if (mRxCount < 2) {
                    break;
                }
                mRxCount = 0;

                mRxInstId = mRxTmpBuffer.getShort();
                mRxTmpBuffer.position(0);


                // Search for object, if not found reset state machine
                // Because java treats ints as only signed we need to do this manually
                if (mRxObjId < 0) {
                    mRxObjId = 0x100000000L + mRxObjId;
                }

                UAVObject rxObj = mObjMngr.getObject(mRxObjId);
                if (rxObj == null && mRxType != TYPE_OBJ_REQ) {
                    if (WARN) Log.w(TAG, "Unknown ID: " + mRxObjId);
                    mComStats.rxErrors++;
                    mRxState = RxStateType.STATE_ERROR;
                    break;
                }

                // Determine data length
                if (mRxType == TYPE_OBJ_REQ || mRxType == TYPE_ACK || mRxType == TYPE_NACK)
                    mUavoNumDataBytes = 0;
                else {
                    mUavoNumDataBytes = rxObj.getNumBytes();
                }

                // Check length and determine next state
                if (mUavoNumDataBytes >= MAX_PAYLOAD_LENGTH) {
                    if (WARN) Log.w(TAG, "Greater than max payload length");
                    mComStats.rxErrors++;
                    mRxState = RxStateType.STATE_ERROR;
                    break;
                }

                // Check the lengths match
                if ((mRxBytesSoFar + mUavoNumDataBytes) != mPacketSize) {
                    // packet error - mismatched packet size
                    if (WARN)
                        Log.w(TAG, String.format("UAVTalk - error : mismatched packet size %d (%s) - expected: %d; actual: %d",
                                mRxObjId,
                                rxObj != null ? rxObj.getName() : "Unknown",
                                mPacketSize, mRxBytesSoFar + mUavoNumDataBytes));
                    mComStats.rxErrors++;
                    mRxState = RxStateType.STATE_ERROR;
                    break;
                }

                // If there is a payload get it, otherwise receive checksum
                if (mUavoNumDataBytes > 0)
                    mRxState = RxStateType.STATE_DATA;
                else
                    mRxState = RxStateType.STATE_CS;

                break;

            case STATE_DATA:

                // Update CRC
                mRxCS = Crc.updateCRC(mRxCS, rxbyte);

                mRxBuffer.put(mRxCount++, (byte) (rxbyte & 0xff));
                if (mRxCount < mUavoNumDataBytes)
                    break;

                mRxCount = 0;
                mRxState = RxStateType.STATE_CS;
                break;

            case STATE_CS:

                // The CRC byte

                if (mRxCS != rxbyte) {
                    // packet error - faulty CRC
                    if (WARN) {
                        UAVObject obj = mObjMngr.getObject(mRxObjId);
                        Log.w(TAG, "Bad crc: (" + (obj != null ? obj.getName() : "") + ") calculated: " +
                                Integer.toHexString(mRxCS) + " actual: " + Integer.toHexString(rxbyte));
                    }
                    mComStats.rxCrcErrors++;
                    mRxState = RxStateType.STATE_ERROR;
                    break;
                }

                if (mRxBytesSoFar != (mPacketSize + CHECKSUM_LENGTH)) {
                    // packet error - mismatched packet size
                    if (WARN) Log.w(TAG, "Bad size");
                    mComStats.rxErrors++;
                    mRxState = RxStateType.STATE_ERROR;
                    break;
                }

                if (DEBUG) Log.d(TAG, "Received");
                mRxState = RxStateType.STATE_COMPLETE;
                break;

            default:
                if (WARN) Log.w(TAG, "Bad state");
                mRxState = RxStateType.STATE_ERROR;
        }
    }

    /**
     * Receive an object. This function process objects received through the
     * telemetry stream.
     * *
     * Parser errors are considered as transmission errors and are not NACKed.
     * Some senders (GCS) can timeout and retry if the message is not answered by an ack or nack.
     * <p/>
     * Object handling errors are considered as application errors and are NACked.
     * In that case we want to nack as there is no point in the sender retrying to send invalid objects.
     * <p/>
     * \param[in] type Type of received message (TYPE_OBJ,
     * TYPE_OBJ_REQ, TYPE_OBJ_ACK, TYPE_ACK) \param[in] obj Handle of the
     * received object \param[in] instId The instance ID of UAVOBJ_ALL_INSTANCES
     * for all instances. \param[in] data Data buffer \param[in] length Buffer
     * length \return Success (true), Failure (false)
     *
     * @throws IOException
     */
    private boolean receiveObject(int type, long objId, long instId, ByteBuffer data) throws IOException {

        if (DEBUG)
            Log.d(TAG, "Received object ID: " + objId + " [" + Long.toHexString(objId) + "]");
        assert (mObjMngr != null);

        UAVObject obj;
        boolean error = false;
        boolean allInstances = (instId == ALL_INSTANCES);

        // Process message type
        switch (type) {
            case TYPE_OBJ:
                // All instances, not allowed for OBJ messages
                if (!allInstances) {
                    if (DEBUG) Log.d(TAG, "Received object: " + mObjMngr.getObject(objId).getName());

                    // Get object and update its data
                    obj = updateObject(objId, instId, data);

                    if (obj != null) {
                        // Check if this object acks a pending OBJ_REQ message
                        // any OBJ message can ack a pending OBJ_REQ message
                        // even one that was not sent in response to the OBJ_REQ message
                        updateAck(type, objId, instId, obj);

                    } else {
                        error = true;
                    }
                } else {
                    error = true;
                }
                break;
            case TYPE_OBJ_ACK:
                // All instances, not allowed for OBJ_ACK messages
                if (!allInstances) {
                    if (DEBUG)
                        Log.d(TAG, "Received object ack: " + objId + " " + mObjMngr.getObject(objId).getName());
                    // Get object and update its data
                    obj = updateObject(objId, instId, data);
                    // Transmit ACK
                    error = obj == null || !transmitObject(TYPE_ACK, objId, instId, obj);
                } else {
                    error = true;
                }

                if (error) {
                    // failed to update object, transmit NACK
                    transmitObject(TYPE_NACK, objId, instId, null);
                }

                break;
            case TYPE_OBJ_REQ:
                // Get object, if all instances are requested get instance 0 of the
                // object
                if (DEBUG) Log.d(TAG, "Received object request: " + objId + " " +
                        mObjMngr.getObject(objId).getName());
                if (allInstances) {
                    // All instances, so get instance zero
                    obj = mObjMngr.getObject(objId);
                } else {
                    obj = mObjMngr.getObject(objId, instId);
                }
                // If object was found transmit it
                error = obj == null || !transmitObject(TYPE_OBJ, objId, instId, obj);

                if (error) {
                    // failed to send object, transmit NACK
                    transmitObject(TYPE_NACK, objId, instId, null);
                }

                break;
            case TYPE_ACK:
                if (DEBUG)
                    Log.d(TAG, "Received ACK: " + objId + " " + mObjMngr.getObject(objId).getName());
                // All instances, not allowed for ACK messages
                if (!allInstances) {
                    // Get object
                    obj = mObjMngr.getObject(objId, instId);
                    // Check if object exists:
                    if (obj != null) {
                        // Check if an ACK is pending
                        updateAck(type, objId, instId, obj);
                    } else {
                        error = true;
                    }
                }
                break;
            case TYPE_NACK:
                // All instances, not allowed for NACK messages
                if (!allInstances) {
                    if (DEBUG)
                        Log.d(TAG, "Received NACK: " + objId + " " + mObjMngr.getObject(objId).getName());
                    // Get object
                    obj = mObjMngr.getObject(objId, instId);
                    // Check if an ack is pending
                    if (obj != null) {
                        // Check if a NACK is pending
                        updateNack(objId, instId, obj);
                    } else {
                        error = true;
                    }
                }
                break;
            default:
                error = true;
        }
        // Done
        return !error;
    }

    /**
     * Update the data of an object from a byte array (unpack). If the object
     * instance could not be found in the list, then a new one is created.
     */
    private UAVObject updateObject(long objId, long instId,
                                   ByteBuffer data) {
        assert (mObjMngr != null);

        // Get object
        UAVObject obj = mObjMngr.getObject(objId, instId);

        // If the instance does not exist create it
        if (obj == null) {
            // Get the object type
            UAVObject tobj = mObjMngr.getObject(objId);
            if (tobj == null) {
                // TODO: Return a NAK since we don't know this object
                return null;
            }
            // Make sure this is a data object
            UAVDataObject dobj;
            try {
                dobj = (UAVDataObject) tobj;
            } catch (Exception e) {
                // Failed to cast to a data object
                return null;
            }

            // Create a new instance, unpack and register
            UAVDataObject instobj = null;
            try {
                instobj = dobj.clone(instId);
                if (!mObjMngr.registerObject(instobj)) {
                    return null;
                }
                if (DEBUG) Log.d(TAG, "Unpacking new object");
                instobj.unpack(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return instobj;
        } else {
            // Unpack data into object instance
            if (DEBUG) Log.d(TAG, "Unpacking existing object: " + obj.getName());
            obj.unpack(data);
            return obj;
        }
    }

    /**
     * Check if a transaction is pending and if yes complete it.
     */
    private void updateNack(long objId, long instId, UAVObject obj) {
        if ((obj == null)) throw new AssertionError();
        Transaction trans = findTransaction(objId, instId);
        if (trans != null) {
            closeTransaction(trans);
            if (mTransactionCompletedListener != null)
                mTransactionCompletedListener.TransactionFailed(obj);
        }
    }

    /**
     * Check if a transaction is pending that this acked object corresponds to
     * and if yes complete it.
     */
    private void updateAck(int type, long objectId, long instId, UAVObject obj) {
        if (DEBUG) Log.d(TAG, "Received ack: " + obj.getName());
        if ((obj == null)) throw new AssertionError();
        Transaction trans = findTransaction(objectId, instId);
        if (trans != null && trans.respType == type) {
            if (trans.respInstId == ALL_INSTANCES) {
                if (instId == 0) {
                    // last instance received, complete transaction
                    closeTransaction(trans);
                    if (mTransactionCompletedListener != null)
                        mTransactionCompletedListener.TransactionSucceeded(obj);
                }
            } else {
                closeTransaction(trans);
                if (mTransactionCompletedListener != null)
                    mTransactionCompletedListener.TransactionSucceeded(obj);
            }
        }
    }

    /**
     * Send an object through the telemetry link.
     *
     * @param obj  Object to send
     * @param type Transaction type
     * @return Success (true), Failure (false)
     * @throws IOException
     */
    private boolean transmitObject(int type, long objId, long instId, UAVObject obj) throws IOException {
        // Important note : obj can be null (when type is NACK for example) so protect all obj dereferences.

        // If all instances are requested on a single instance object it is an error
        if ((obj != null) && (instId == ALL_INSTANCES) && obj.isSingleInstance()) {
            instId = 0;
        }
        boolean allInstances = (instId == ALL_INSTANCES);

        // Process message type
        boolean ret;
        if (type == TYPE_OBJ || type == TYPE_OBJ_ACK) {
            if (allInstances) {
                // Send all instances in reverse order
                // This allows the receiver to detect when the last object has been received (i.e. when instance 0 is received)
                int numInst = mObjMngr.getNumInstances(objId);
                for (int n = 0; n < numInst; ++n) {
                    int i = numInst - n - 1;
                    UAVObject o = mObjMngr.getObject(objId, i);
                    if (!transmitSingleObject(type, objId, i, o)) {
                        break;
                    }
                }

                return true;
            } else {
                ret = transmitSingleObject(type, objId, instId, obj);
            }
        } else if (type == TYPE_OBJ_REQ) {
            ret = transmitSingleObject(TYPE_OBJ_REQ, objId, instId, null);
        } else if (type == TYPE_ACK || type == TYPE_NACK) {
            ret = !allInstances && transmitSingleObject(type, objId, instId, null);
        } else {
            return false;
        }

        return ret;
    }

    /**
     * Send an object through the telemetry link.
     *
     * @param obj  Object handle to send
     * @param type Transaction type \return Success (true), Failure (false)
     * @throws IOException
     */
    private boolean transmitSingleObject(int type, long objId, long instId, UAVObject obj) throws IOException {
        int length;

        assert (mObjMngr != null && mOutStream != null);

        // IMPORTANT : obj can be null (when type is NACK for example)

        ByteBuffer bbuf = ByteBuffer.allocate(MAX_PACKET_LENGTH);
        bbuf.order(ByteOrder.LITTLE_ENDIAN);

        // Determine data length
        if (type == TYPE_OBJ_REQ || type == TYPE_ACK || type == TYPE_NACK) {
            length = 0;
        } else {
            length = obj.getNumBytes();
        }

        // Set sync byte
        bbuf.put((byte) (SYNC_VAL & 0xff));

        // Set type
        bbuf.put((byte) (type & 0xff));

        // Set length
        bbuf.putShort((short) (HEADER_LENGTH + length));

        // Set objectID
        bbuf.putInt((int) objId);

        // Set instance ID
        bbuf.putShort((short) (instId & 0xffff));

        // Check length
        if (length >= MAX_PAYLOAD_LENGTH) {
            if (ERROR) Log.e(TAG, "transmitSingleObject: exceeded max payload length");
            ++mComStats.txErrors;
            return false;
        }

        // Copy data (if any)
        if (length > 0) {
            try {
                if (obj.pack(bbuf) == 0) {
                    if (ERROR)
                        Log.e(TAG, "transmitSingleObject: zero bytes read from object " + obj.getName());
                    ++mComStats.txErrors;
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                ++mComStats.txErrors;
                return false;
            }
        }

        // Calculate checksum
        bbuf.put((byte) (Crc.updateCRC(0, bbuf.array(), HEADER_LENGTH + length) & 0xff));

        int packlen = bbuf.position();
        bbuf.position(0);
        byte[] dst = new byte[packlen];
        bbuf.get(dst, 0, packlen);

        if (VERBOSE)
            Log.d(TAG, obj != null ? obj.getName() : "Unknown" + " Data -. " + bytesToHex(dst));

        if (VERBOSE) Log.d(TAG, "Data -. [" + bytesToHex(dst) + "]");

        mOutStream.write(dst);

        // Update stats
        ++mComStats.txObjects;
        mComStats.txBytes += packlen;
        mComStats.txObjectBytes += length;

        // Done
        return true;
    }

    private String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 3];
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF;
            hexChars[j * 3] = hexArray[v >>> 4];
            hexChars[j * 3 + 1] = hexArray[v & 0x0F];
            hexChars[j * 3 + 2] = ' ';
        }
        return new String(hexChars);
    }

    private void openTransaction(int type, long objId, long instId) {
        Transaction trans = new Transaction();

        trans.respType = (type == TYPE_OBJ_REQ) ? TYPE_OBJ : TYPE_ACK;
        trans.respObjId = objId;
        trans.respInstId = instId;

        if (DEBUG) Log.d(TAG, "Open transaction for " + objId);

        LongSparseArray<Transaction> objTransactions = mTransMap.get(trans.respObjId);
        if (objTransactions == null) {
            objTransactions = new LongSparseArray<>();
            mTransMap.put(trans.respObjId, objTransactions);
        }
        objTransactions.put(trans.respInstId, trans);
    }

    private Transaction findTransaction(long objId, long instId) {
        // Lookup the transaction in the transaction map
        LongSparseArray<Transaction> objTransactions = mTransMap.get(objId);
        if (objTransactions != null) {
            Transaction trans = objTransactions.get(instId);
            if (trans == null) {
                // see if there is an ALL_INSTANCES transaction
                trans = objTransactions.get(ALL_INSTANCES);
            }
            return trans;
        }
        return null;

    }

    private void closeTransaction(Transaction trans) {
        LongSparseArray<Transaction> objTransactions = mTransMap.get(trans.respObjId);
        if (objTransactions != null) {
            objTransactions.remove(trans.respInstId);

            if (DEBUG) Log.d(TAG, "Open transaction for " + trans.respObjId);
            // Keep the map even if it is empty
            // There are at most 100 different object IDs...
        }

    }

    private void closeAllTransactions() {
        for (int count = 0; count < mTransMap.size(); count++) {
            Long objId = mTransMap.keyAt(count);
            LongSparseArray<Transaction> objTransactions = mTransMap.get(objId);
            for (int innerCount = 0; innerCount < objTransactions.size(); innerCount++) {
                Long instId = objTransactions.keyAt(innerCount);

                if (DEBUG) {
                    Transaction trans = objTransactions.get(instId);
                    Log.d(TAG, "UAVTalk - closing active transaction for object" + trans.respObjId);
                }
                objTransactions.remove(instId);
            }
            mTransMap.remove(objId);
        }
    }

    void setOnTransactionCompletedListener(OnTransactionCompletedListener onTransactionListener) {
        this.mTransactionCompletedListener = onTransactionListener;
    }

    enum RxStateType {
        STATE_SYNC, STATE_TYPE, STATE_SIZE, STATE_OBJID, STATE_INSTID, STATE_DATA, STATE_CS, STATE_COMPLETE, STATE_ERROR
    }

    /**
     * Comm stats
     */
    public class ComStats {
        public int txBytes = 0;
        public int rxBytes = 0;
        public int txObjectBytes = 0;
        public int rxObjectBytes = 0;
        public int rxObjects = 0;
        public int txObjects = 0;
        public int txErrors = 0;
        public int rxErrors = 0;
        public int rxSyncErrors = 0;
        public int rxCrcErrors = 0;
    }

    private class Transaction {
        public int respType;
        public long respObjId;
        public long respInstId;
    }

    abstract class OnTransactionCompletedListener {
        abstract void TransactionSucceeded(UAVObject data);

        abstract void TransactionFailed(UAVObject data);
    }


}
