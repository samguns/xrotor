/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import org.opng.mobilegcs.AppMenu;/*
 * Eric Henaff Copyright (c) 2015.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

public class BaseFragmentAppMenuItem<T extends BaseFragment> extends AppMenuItem implements AppMenuItemFragmentOperations {

    private final Class<T> mFragmentClass;

    public BaseFragmentAppMenuItem(int id, String title, int icon, Class<T> fragment) {
        super(id, title, icon);
        mFragmentClass = fragment;
    }

    public BaseFragmentAppMenuItem(String title, int icon, Class<T> fragment) {
        super(title, icon);
        mFragmentClass = fragment;
    }

    public BaseFragmentAppMenuItem(int id, String title, Class<T> fragment) {
        super(id, title);
        mFragmentClass = fragment;
    }

    public BaseFragmentAppMenuItem(String title, Class<T> fragment) {
        super(title);
        mFragmentClass = fragment;
    }

    @Override
    public Fragment getFragment() throws IllegalAccessException, InstantiationException {
        Fragment f = mFragmentClass.newInstance();

        Bundle args = new Bundle();
        args.putInt(AppMenu.APP_MENU_ITEM, getId());
        f.setArguments(args);

        return f;
    }

    @Override
    public void Open(FragmentManager fragmentManager, int containerId) {
        // Requires API 19+
        //noinspection TryWithIdenticalCatches
        try {
            Fragment frag = fragmentManager.findFragmentByTag(mTitle + getId());

            if (frag == null)
                frag = getFragment();

            fragmentManager.beginTransaction()
                    .replace(containerId, frag, mTitle + getId())
                    .commit();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }
}
