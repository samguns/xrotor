/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.opng.uavtalk.UAVObjectField;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ObjectEditView extends GridLayout {

    private Map<String, View> mFields;
    private boolean mReadOnly = false;

    public ObjectEditView(Context context) {
        super(context);
        initObjectEditView();
    }

    public ObjectEditView(Context context, AttributeSet ats, int defaultStyle) {
        super(context, ats);
        initObjectEditView();
    }

    public ObjectEditView(Context context, AttributeSet ats) {
        super(context, ats);
        initObjectEditView();
    }

    public void setReadOnly() {
        mReadOnly = true;
    }

    private void initObjectEditView() {
        // Set orientation of layout to vertical
        setOrientation(GridLayout.VERTICAL);
        setColumnCount(2);
        mFields = new HashMap<>();
    }

    public List<View> addField(UAVObjectField field) {
        List<View> retVal = new ArrayList<>();
        for (int i = 0; i < field.getNumElements(); i++) {
            View addedView = addRow(getContext(), field, i);
            if (addedView != null) {
                retVal.add(addedView);
            }
        }

        return retVal;
    }

    private View addRow(Context context, UAVObjectField field, int idx) {
        int row = getRowCount();
        View fieldValue = null;

        TextView fieldName = new TextView(context);
        String curFieldName;
        if (field.getNumElements() == 1) {
            curFieldName = field.getName();
        } else {
            curFieldName = (field.getName() + "-" + field.getElementNames().get(idx));
        }
        if (!mFields.containsKey(curFieldName)) {
            fieldName.setText(curFieldName);
            addView(fieldName, new LayoutParams(spec(row), spec(0)));

            if (mReadOnly) {
                fieldValue = new TextView(context);
                fieldValue.setPadding(10, 10, 10, 10);
            } else {
                switch (field.getType()) {
                    case FLOAT32:
                        fieldValue = new EditText(context);
                        ((EditText) fieldValue).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED | InputType.TYPE_NUMBER_FLAG_DECIMAL);
                        break;
                    case INT8:
                    case INT16:
                    case INT32:
                        fieldValue = new EditText(context);
                        ((EditText) fieldValue).setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_SIGNED);
                        break;
                    case UINT8:
                    case UINT16:
                    case UINT32:
                        fieldValue = new EditText(context);
                        ((EditText) fieldValue).setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case ENUM:
                        fieldValue = new Spinner(context);
                        break;
                    case BITFIELD:
                        fieldValue = new EditText(context);
                        ((EditText) fieldValue).setInputType(InputType.TYPE_CLASS_NUMBER);
                        break;
                    case STRING:
                        fieldValue = new EditText(context);
                }
            }

            addView(fieldValue, new LayoutParams(spec(row), spec(1)));
            mFields.put(curFieldName, fieldValue);
        }

        return fieldValue;
    }

}
