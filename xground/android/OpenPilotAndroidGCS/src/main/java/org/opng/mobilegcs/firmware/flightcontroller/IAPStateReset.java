/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.flightcontroller;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import org.opng.mobilegcs.telemetry.TelemetryTask;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;


class IAPStateReset extends IAPState {
    private static final String TAG = IAPStateReset.class.getSimpleName();

    IAPStateReset(IAPState state) {
        super(state);
    }

    @Override
    public void process(boolean success) {
        if (!success) {
            if (DEBUG) Log.d(TAG, "Previous IAP step failed");
            mUploader.setCurrentIAPState(new IAPStateReady(this));
            finishHandleTransactionComplete();
            mUploader.fireBootProgress(Uploader.ProgressStep.FAILURE, 0);
            mUploader.fireBootFailed();
        } else {
            ProcessTask task = new ProcessTask();
            task.execute((Void[]) null);
        }
    }

    private class ProcessTask extends AsyncTask<Void, Void, Boolean> {

        private final CountDownLatch mCountDownLatch = new CountDownLatch(1);
        private TransportConnStateBroadcastReceiver mReceiver;

        @Override
        protected void onPreExecute() {
            // Register local broadcast listener to listen for usb or whatever to
            // reconnect. The listener will use a countdown latch to signal when
            // the transport layer is reconnected, allowing the task to continue
            // attempting to boot into IAP mode.
            IntentFilter intentFilter = new IntentFilter(TelemetryTask.INTENT_ACTION_TRANSPORT_CONNECTED);
            mReceiver = new TransportConnStateBroadcastReceiver();
            LocalBroadcastManager.getInstance(mUploader.getContext()).registerReceiver(mReceiver, intentFilter);
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            boolean bootSucceeded = true;
            try {
                if (DEBUG) Log.d(TAG, "Waiting for last IAP command to complete");
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            mUploader.fireBootProgress(Uploader.ProgressStep.JUMP_TO_BL, 4);

            // Done listening for transaction complete
            finishHandleTransactionComplete();

            // Explicitly restart telemetry connection
            mUploader.connectTelemetry();

            try {
                // Wait for transport layer reconnection
                if (DEBUG) Log.d(TAG, "Waiting for reconnection");
                if (mCountDownLatch.await(10000, TimeUnit.MILLISECONDS)) {
                    // Need to suspend telemetry to stop it from spamming the connection
                    if (DEBUG) Log.d(TAG, "Suspending telemetry for IAP");
                    mUploader.suspendTelemetry();

                    // Next state
                    mUploader.setCurrentIAPState(new IAPStateBootloader(IAPStateReset.this));

                    // Get board into bootloader
                    if (!mUploader.iapInit()) {
                        bootSucceeded = false;
                        if (DEBUG) Log.d(TAG, "Failed to init IAP");
                    } else {
                        if (DEBUG) Log.d(TAG, "Boot to IAP succeeded");
                    }
                } else {
                    // Timeout waiting for reconnection
                    bootSucceeded = false;
                    if (DEBUG) Log.d(TAG, "Timeout waiting for reconnection");
                }
            } catch (InterruptedException e) {
                bootSucceeded = false;
                if (DEBUG) Log.d(TAG, "Interrupted waiting for reconnection");
                e.printStackTrace();
            }

            if (!bootSucceeded) {
                doBootFailure();

            }

            return bootSucceeded;
        }

        @Override
        protected void onPostExecute(Boolean bootSucceeded) {
            // Remove the broadcast receiver now that we're done with it
            LocalBroadcastManager.getInstance(mUploader.getContext()).unregisterReceiver(mReceiver);

            if (bootSucceeded) {
                doBootSuccess();
            } else {
                doBootFailure();
            }
        }

        private void doBootFailure() {
            mUploader.setCurrentIAPState(new IAPStateReady(IAPStateReset.this));
            mUploader.fireBootProgress(Uploader.ProgressStep.FAILURE, 0);
            mUploader.fireBootFailed();
        }

        private void doBootSuccess() {
            mUploader.fireBootProgress(Uploader.ProgressStep.JUMP_TO_BL, 5);

            // Should handle this event to start up device specific gui
            mUploader.fireBootSucceeded();

            if (mUploader.isResetOnly()) {
                try {
                    Thread.sleep(3500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                mUploader.systemBoot();
            }
        }

        /**
         * Used to listen for transport layer (USB, bluetooth etc.) to reconnect. This will then
         * signal the task, allowing it to continue attempting to boot into IAP mode.
         */
        @SuppressLint("Registered")
        private class TransportConnStateBroadcastReceiver extends BroadcastReceiver {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (intent != null && intent.getAction().equals(TelemetryTask.INTENT_ACTION_TRANSPORT_CONNECTED)) {
                    if (DEBUG) Log.d(TAG, "Transport reconnected");
                    mCountDownLatch.countDown();
                }
            }
        }
    }
}
