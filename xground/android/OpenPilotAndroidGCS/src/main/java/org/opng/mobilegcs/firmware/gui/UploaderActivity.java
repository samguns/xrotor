/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.firmware.gui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.filemanager.FileManagerActivity;
import org.opng.mobilegcs.firmware.downloader.bamboo.gui.BambooDownloadActivity;
import org.opng.mobilegcs.firmware.flightcontroller.DeviceInfo;

import java.io.File;
import java.util.ArrayList;

public class UploaderActivity extends AppCompatActivity implements UploaderActivityFragment.OnUploaderActivityFragmentInteractionListener {
    private static final int SELECT_FIRMWARE = 1;
    private UploaderActivityFragment mUploaderActivityFragment;
    private boolean mColdBootConnected;
    private boolean mEnableUploadMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_uploader);

        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBarToolbar);

        // Show the Up button in the toolbar.
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }

        setTitle(R.string.title_uploader);

        mUploaderActivityFragment = (UploaderActivityFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_uploader, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.uploader_halt:
                mUploaderActivityFragment.halt();
                return true;
            case R.id.uploader_reset:
                mUploaderActivityFragment.reset();
                return true;
            case R.id.uploader_reboot:
                mUploaderActivityFragment.reboot();
                return true;
            case R.id.uploader_reboot_default:
                mUploaderActivityFragment.rebootDefault();
                return true;
            case R.id.uploader_reboot_erase:
                mUploaderActivityFragment.rebootErase();
                return true;
            case R.id.uploader_connect:
                mColdBootConnected = mUploaderActivityFragment.connect();
                return true;
            case R.id.action_open_file:
                // Create firmware directory if necessary and use it as our starting
                // directory in the file manager.
                File startPath = new File(getFilesDir(), "firmware");
                boolean startPathExists = true;

                if (!startPath.exists()) {
                    startPathExists = startPath.mkdir();
                }
                Intent intent = new Intent(this, FileManagerActivity.class);
                Bundle bundle = new Bundle();
                bundle.putBoolean(FileManagerActivity.FILE_MANAGER_DIR_ONLY, false);
                bundle.putBoolean(FileManagerActivity.FILE_MANAGER_IS_FOR_SAVE, false);
                bundle.putString(FileManagerActivity.FILE_MANAGER_START_DIR, startPathExists ? startPath.getAbsolutePath() : "");

                // Get selected device info so we can read the board ID. This will be used
                // to filter the file manager to display only firmware to match the selected
                // device.
                DeviceInfo selectedDevice = mUploaderActivityFragment.getSelectedDevice();
                if (selectedDevice != null) {
                    bundle.putString(FileManagerActivity.FILE_MANAGER_FILE_FILTER, Integer.toString(selectedDevice.getDeviceId()));
                }
                intent.putExtra(FileManagerActivity.FILE_MANAGER_OPEN, bundle);
                startActivityForResult(intent, SELECT_FIRMWARE);
                return true;
            case R.id.action_bamboo_download:
                Intent intentDownload = new Intent(this, BambooDownloadActivity.class);
                startActivity(intentDownload);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem itemUploaderHalt = menu.findItem(R.id.uploader_halt);
        MenuItem itemUploaderReset = menu.findItem(R.id.uploader_reset);
        MenuItem itemUploaderReboot = menu.findItem(R.id.uploader_reboot);
        MenuItem itemUploaderRebootDefault = menu.findItem(R.id.uploader_reboot_default);
        MenuItem itemUploaderRebootErase = menu.findItem(R.id.uploader_reboot_erase);
        MenuItem itemUploaderConnect = menu.findItem(R.id.uploader_connect);

        if (mUploaderActivityFragment.isTelemetryConnected()) {
            // Disable connect option as we can do a warm boot using one of the
            // other menu options
            itemUploaderConnect.setEnabled(false);

            // The rest of the items need telemetry connected to use warm boot
            // functionality
            itemUploaderHalt.setEnabled(true);
            itemUploaderReset.setEnabled(true);
            itemUploaderReboot.setEnabled(true);
            itemUploaderRebootDefault.setEnabled(true);
            itemUploaderRebootErase.setEnabled(true);
        } else {
            // No telemetry connected so we can offer cold boot
            itemUploaderConnect.setEnabled(true);

            // No telemetry connected, disable all of these
            itemUploaderHalt.setEnabled(false);
            itemUploaderReset.setEnabled(false);
            itemUploaderReboot.setEnabled(false);
            itemUploaderRebootDefault.setEnabled(false);
            itemUploaderRebootErase.setEnabled(false);
        }

        if (mColdBootConnected) {
            itemUploaderConnect.setTitle(R.string.title_action_uploader_disconnect);
        } else {
            itemUploaderConnect.setTitle(R.string.title_action_uploader_connect);
        }

        // Enable file open menu item based on whether or not uploader is
        // ready to be used (mEnableUploadMenu true or false)
        menu.findItem(R.id.action_open_file).setEnabled(mEnableUploadMenu);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_FIRMWARE) {
            if (resultCode == RESULT_OK) {
                // Firmware selected ok, get selected firmware from given intent
                Bundle bundle = data.getBundleExtra(FileManagerActivity.FILE_MANAGER_RESULT_DATA);
                ArrayList<String> paths = bundle.getStringArrayList(FileManagerActivity.FILE_MANAGER_BUNDLE_SELECTED_FILES_KEY);

                // Only expecting one filename
                if (paths != null && !paths.isEmpty()) {
                    mUploaderActivityFragment.setFirmwareFilename(paths.get(0));
                }
            }
        }
    }

    @Override
    public void onUploaderReady() {
        // Uploader is ready so indicate that file open menu should be enabled
        mEnableUploadMenu = true;
    }

    @Override
    public void onUploaderFinished() {
        // Uploader is no longer ready so indicate that file open menu should be
        // disabled
        mEnableUploadMenu = false;
    }
}
