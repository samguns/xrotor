/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;

import org.opng.mobilegcs.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicStabiResponsivenessFragment extends Fragment implements StabiGuiFragment {


    private View _rootView;
    private View mSeekBasicRollMax;
    private View mSeekBasicManualRateRoll;
    private View mSeekBasicManualRateYaw;

    public BasicStabiResponsivenessFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        _rootView = inflater.inflate(R.layout.fragment_basic_stabi_responsiveness, container, false);
        mSeekBasicRollMax = _rootView.findViewById(R.id.seekBasicRollMax);
        mSeekBasicManualRateRoll = _rootView.findViewById(R.id.seekBasicManualRateRoll);
        mSeekBasicManualRateYaw = _rootView.findViewById(R.id.seekBasicManualRateYaw);

        setSliderLimits(mSeekBasicRollMax, getResources().getInteger(R.integer.basic_response_roll_min),
                getResources().getInteger(R.integer.basic_response_roll_max));
        int responseRateMin = getResources().getInteger(R.integer.basic_response_rate_min);
        int responseRateMax = getResources().getInteger(R.integer.basic_response_rate_max);
        setSliderLimits(mSeekBasicManualRateRoll, responseRateMin, responseRateMax);
        setSliderLimits(mSeekBasicManualRateYaw, responseRateMin, responseRateMax);

        return _rootView;
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        helper.bindRollResponsivenessWidgets(mSeekBasicRollMax,
                mSeekBasicManualRateRoll, null);
        helper.bindRollResponsivenessWidgets(_rootView.findViewById(R.id.textBasicRollMax),
                _rootView.findViewById(R.id.textBasicManualRateRoll), null);
        helper.bindYawResponsivenessWidgets(null, mSeekBasicManualRateYaw, null);
        helper.bindYawResponsivenessWidgets(null, _rootView.findViewById(R.id.textBasicManualRateYaw), null);
    }

    private void setSliderLimits(View slider, int minValue, int maxValue) {
        if (slider != null && slider instanceof SeekBar) {
            slider.setTag(R.id.seekbar_min_value, minValue);
            ((SeekBar) slider).setMax(maxValue);
        }

    }
}
