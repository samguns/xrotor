/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.filemanager;

import com.google.common.io.Files;

import java.io.File;
import java.io.FileFilter;

class ExtensionFilter implements FileFilter {
    private final String mExtension;

    public ExtensionFilter(String extension) {
        mExtension = extension;
    }

    @Override
    public boolean accept(File file) {
        return Files.getFileExtension(file.getName()).equalsIgnoreCase(mExtension);
    }
}
