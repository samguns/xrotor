/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.flightcontroller;

import android.os.Parcel;
import android.os.Parcelable;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.io.IAP;

public class DeviceInfo implements Parcelable {
    public static final Creator<DeviceInfo> CREATOR = new Creator<DeviceInfo>() {
        @Override
        public DeviceInfo createFromParcel(Parcel in) {
            return new DeviceInfo(in);
        }

        @Override
        public DeviceInfo[] newArray(int size) {
            return new DeviceInfo[size];
        }
    };
    private final int mFWCodeSize;
    private final long mFWCrc;
    private final String mBLVersion;
    private final int mDeviceId;
    private final int mDeviceIndex;
    private final int mHardwareRev;
    private String mSerialNumber;
    private String mFirmwareFilename;
    // The following items can be determined from the device id and don't need
    // to be part of a parcel.
    private boolean mGotDeviceSpecificInfo;
    private int mFirmwareResId;
    private int mImageResId;

    public DeviceInfo(IAP.Device iapDevice, int deviceIndex) {
        mFWCodeSize = iapDevice.SizeOfCode;
        mFWCrc = iapDevice.FW_CRC;
        mBLVersion = String.valueOf(iapDevice.BL_Version);
        mDeviceId = iapDevice.ID;
        mDeviceIndex = deviceIndex;
        mHardwareRev = mDeviceId & 0x00FF;
    }

    private DeviceInfo(Parcel in) {
        mFWCodeSize = in.readInt();
        mFWCrc = in.readLong();
        mBLVersion = in.readString();
        mDeviceId = in.readInt();
        mDeviceIndex = in.readInt();
        mHardwareRev = in.readInt();
        mSerialNumber = in.readString();
        mFirmwareFilename = in.readString();
    }

    public String getBLVersion() {
        return mBLVersion;
    }

    public int getFWCodeSize() {
        return mFWCodeSize;
    }

    public long getFWCrc() {
        return mFWCrc;
    }

    public int getDeviceId() {
        return mDeviceId;
    }

    public int getDeviceIndex() {
        return mDeviceIndex;
    }

    public int getHardwareRev() {
        return mHardwareRev;
    }

    public String getSerialNumber() {
        return mSerialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        mSerialNumber = serialNumber;
    }

    public int getFirmwareResId() {
        if (!mGotDeviceSpecificInfo) {
            determineDeviceSpecificInfo();
            mGotDeviceSpecificInfo = true;
        }

        return mFirmwareResId;
    }

    public int getImageResId() {
        if (!mGotDeviceSpecificInfo) {
            determineDeviceSpecificInfo();
            mGotDeviceSpecificInfo = true;
        }

        return mImageResId;
    }

    public String getFirmwareFilename() {
        return mFirmwareFilename;
    }

    public void setFirmwareFilename(String firmwareFilename) {
        mFirmwareFilename = firmwareFilename;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(mFWCodeSize);
        parcel.writeLong(mFWCrc);
        parcel.writeString(mBLVersion);
        parcel.writeInt(mDeviceId);
        parcel.writeInt(mDeviceIndex);
        parcel.writeInt(mHardwareRev);
        parcel.writeString(mSerialNumber);
        parcel.writeString(mFirmwareFilename);
    }

    private void determineDeviceSpecificInfo() {
        switch (mDeviceId) {
            case 0x0301: // OPLM
                mImageResId = R.drawable.gcs_board_oplink;
                mFirmwareResId = R.raw.fw_oplinkmini;
                break;
            case 0x0903: // Revo
            case 0x0904:
                mImageResId = R.drawable.gcs_board_revo;
                mFirmwareResId = R.raw.fw_revolution;
                break;
            case 0x0905: // Revo Nano
                mImageResId = R.drawable.gcs_board_nano;
                mFirmwareResId = R.raw.fw_revonano;
                break;
            default:
                mImageResId = R.drawable.gcs_board_revo;
                break;
        }
    }
}
