/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.stabilization;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.opng.mobilegcs.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class BasicStabiAttitudeFragment extends Fragment implements StabiGuiFragment {


    private View mRootView;

    public BasicStabiAttitudeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mRootView = inflater.inflate(R.layout.fragment_basic_stabi_attitude, container, false);

        return mRootView;
    }

    @Override
    public void setupGuiHandler(StabilizationGuiHelper helper) {
        helper.bindPitchAttitudePiWidgets(mRootView.findViewById(R.id.seekBasicPitchAttitudeKp), 0.1,
                null, 1);
        helper.bindPitchAttitudePiWidgets(mRootView.findViewById(R.id.textBasicPitchAttitudeKp), 0.1,
                null, 1);
        helper.bindRollAttitudePiWidgets(mRootView.findViewById(R.id.seekBasicRollAttitudeKp), 0.1,
                null, 1);
        helper.bindRollAttitudePiWidgets(mRootView.findViewById(R.id.textBasicRollAttitudeKp), 0.1,
                null, 1);
        helper.bindYawAttitudePiWidgets(mRootView.findViewById(R.id.seekBasicYawAttitudeKp), 0.1,
                null, 1);
        helper.bindYawAttitudePiWidgets(mRootView.findViewById(R.id.textBasicYawAttitudeKp), 0.1,
                null, 1);
    }
}
