/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.firmware.flightcontroller;

import android.util.Log;

class IAPStateBootloader extends IAPState {
    private static final String TAG = IAPStateBootloader.class.getSimpleName();

    public IAPStateBootloader(IAPState state) {
        super(state);
    }

    private IAPStateBootloader(Uploader uploader) {
        super(uploader);
    }

    @Override
    public void process(boolean success) {
        if (DEBUG) Log.d(TAG, "Shouldn't be in this state, fail boot.");
        // Shouldn't reach this state.
        mUploader.fireBootProgress(Uploader.ProgressStep.FAILURE, 0);
        mUploader.fireBootFailed();
    }
}
