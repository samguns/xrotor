/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.filemanager;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.opng.mobilegcs.R;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnFileManagerInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FileManagerFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FileManagerFragment extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM_DIR_ONLY = "paramDirOnly";
    private static final String ARG_PARAM_IS_FOR_SAVE = "paramIsForSave";
    private static final String ARG_PARAM_START_DIR = "paramStartDir";
    private static final String ARG_PARAM_FILE_FILTER = "paramFileFilter";

    private static final int SELECTION_CONTAINS_FILES = 1;
    private static final int SELECTION_CONTAINS_DIRS = 2;
    private static final int SELECTION_CONTAINS_FILES_AND_DIRS = 3;
    private final List<File> mCurFileList = new ArrayList<>();
    private final FileSelectionModel mSelectionModel = new SingleFileSelectionModel();
    private final FileRecyclerViewAdapter mFileAdapter =
            new FileRecyclerViewAdapter(mCurFileList, mSelectionModel);
    private boolean mIsDirectoryOnly;
    private boolean mIsForSave;
    private String mStartDirectory;
    private String mFileFilter;
    private OnFileManagerInteractionListener mListener;
    private TextView mTextViewCurDirectory;
    private File mCurrentDirectory;
    private Button mButtonOpen;
    private Button mButtonSave;
    private FileManagerMode mFileManagerMode;
    private FileManagerButtonHandler mFileManagerButtonHandler;

    public FileManagerFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param isDirectoryOnly true if we are only browsing directories, not files
     * @param isForSave       true for a file save dialog, false for file open
     * @param startDir        Initial directory when showing fragment
     * @param fileFilter      file filter
     * @return A new instance of fragment FileManagerFragment.
     */
    public static FileManagerFragment newInstance(boolean isDirectoryOnly, boolean isForSave, String startDir, String fileFilter) {
        FileManagerFragment fragment = new FileManagerFragment();
        Bundle args = new Bundle();
        args.putBoolean(ARG_PARAM_DIR_ONLY, isDirectoryOnly);
        args.putBoolean(ARG_PARAM_IS_FOR_SAVE, isForSave);
        args.putString(ARG_PARAM_START_DIR, startDir);
        args.putString(ARG_PARAM_FILE_FILTER, fileFilter);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mIsDirectoryOnly = getArguments().getBoolean(ARG_PARAM_DIR_ONLY);

            // Only allow saving in file mode as saving a directory (in directory
            // mode) has no meaning: we only open directories.
            mIsForSave = getArguments().getBoolean(ARG_PARAM_IS_FOR_SAVE) && !mIsDirectoryOnly;
            mStartDirectory = getArguments().getString(ARG_PARAM_START_DIR, getContext().getFilesDir().getAbsolutePath());
            mFileFilter = getArguments().getString(ARG_PARAM_FILE_FILTER);
        } else {
            mStartDirectory = getContext().getFilesDir().getAbsolutePath();
        }

        mFileManagerMode = mIsForSave ? new FileManagerSaveMode() : new FileManagerOpenMode();
        mFileManagerButtonHandler = mIsForSave ? new FileManagerSaveModeButtonHandler() : new FileManagerOpenModeButtonHandler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_file_manager, container, false);

        initFileListView(rootView);
        initSelectionModelEvents();
        initNavigationControls(rootView);
        initButtons(rootView);

        mTextViewCurDirectory = (TextView) rootView.findViewById(R.id.textCurrentDir);

        displayDirectoryContent(new File(mStartDirectory));

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (OnFileManagerInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void initFileListView(View rootView) {
        RecyclerView fileList = (RecyclerView) rootView.findViewById(R.id.fileList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        fileList.setLayoutManager(linearLayoutManager);
        fileList.setAdapter(mFileAdapter);
    }

    private void initSelectionModelEvents() {
        mSelectionModel.addFileSelectionModelListener(new FileSelectionModelListener() {
            @Override
            public void selectionChanged(List<Integer> currentSelection) {
                handleSelectionChanged(currentSelection);
            }

            @Override
            public void itemLongClicked(int itemPosition) {
                performLongClickAction(itemPosition);
            }
        });
    }

    private void initNavigationControls(View rootView) {
        Button directoryUpButton = (Button) rootView.findViewById(R.id.buttonUp);
        directoryUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCurrentDirectory != null) {
                    File parentDir = mCurrentDirectory.getParentFile();

                    if (parentDir != null) {
                        displayDirectoryContent(parentDir);
                    }
                }
            }
        });
    }

    private void initButtons(View rootView) {
        mButtonOpen = (Button) rootView.findViewById(R.id.buttonOpen);
        mButtonSave = (Button) rootView.findViewById(R.id.buttonSave);

        mFileManagerMode.initButtons(mButtonOpen, mButtonSave);
        mFileManagerButtonHandler.initButtons(mButtonOpen, mButtonSave);
        Button buttonCancel = (Button) rootView.findViewById(R.id.buttonCancel);
        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onUserCancelled();
            }
        });
    }

    private void handleSelectionChanged(List<Integer> currentSelection) {
        mFileManagerMode.enableButtons(mButtonOpen, mButtonSave, currentSelection);
    }

    private void performLongClickAction(int itemPosition) {
        File item = mCurFileList.get(itemPosition);

        if (item.exists() && item.isDirectory()) {
            displayDirectoryContent(item);
        }
    }

    private void displayDirectoryContent(File directory) {
        if (directory.exists() && directory.isDirectory()) {
            mCurrentDirectory = directory;
            mTextViewCurDirectory.setText(directory.getAbsolutePath());
            mCurFileList.clear();
            mSelectionModel.reset();
            File[] files = mIsDirectoryOnly ?
                    directory.listFiles() :
                    directory.listFiles(new BoardTypeFilter(mFileFilter));
            Collections.addAll(mCurFileList, files != null ? files : new File[]{});
            mFileAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Returns the directory we are currently viewing as the user's choice. This will
     * only return a result in directory only mode. If we are in directory only mode,
     * the calling parent activity will receive the selected directory/directories
     * and return to the activity that launched it.
     */
    private void returnCurrentDirAsUserChoice() {
        if (mIsDirectoryOnly) {
            List<File> selection = new ArrayList<>();
            selection.add(mCurrentDirectory);
            mListener.onUserSelectedFile(selection);
        }
    }

    /**
     * Returns the selected items as the user's choice. This will only return a result in
     * files only mode. In files only mode, the calling parent activity will receive the
     * selected file/s and return to the activity that launched it.
     *
     * @param selectedItems list of items selected by the user
     */
    private void returnSelectedFilesAsUserChoice(List<Integer> selectedItems) {
        if (!mIsDirectoryOnly) {
            List<File> selectedFiles = new ArrayList<>();
            for (Integer curInteger : selectedItems) {
                selectedFiles.add(mCurFileList.get(curInteger));
            }
            mListener.onUserSelectedFile(selectedFiles);
        }
    }

    /**
     * Returns a value indicating whether or not the selection contains just files, just directories
     * or both.
     *
     * @param currentSelection currently selected items
     * @return SELECTION_CONTAINS_DIRS for directories only, SELECTION_CONTAINS_FILES for files only
     * and SELECTION_CONTAINS_FILES_AND_DIRS if it contains both. A value of zero indicates nothing
     * is selected.
     */
    private int getSelectionMakeup(List<Integer> currentSelection) {
        boolean foundFile = false;
        boolean foundDir = false;
        int retVal = 0;

        for (Integer curItem : currentSelection) {
            File file = mCurFileList.get(curItem);
            if (file.isDirectory()) {
                foundDir = true;
                retVal |= SELECTION_CONTAINS_DIRS;
            } else {
                foundFile = true;
                retVal |= SELECTION_CONTAINS_FILES;
            }

            if (foundDir && foundFile) {
                // We have a mixture of files and directories selected
                // in file only mode
                break;
            }
        }

        return retVal;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFileManagerInteractionListener {
        void onUserSelectedFile(List<File> files);

        void onUserCancelled();
    }

    /**
     * Implement this interface to handle the enablement of gui items in the file
     * manager.
     */
    private interface FileManagerMode {

        /**
         * Called to set the initial state of the buttons.
         *
         * @param buttonOpen open button
         * @param buttonSave save button
         */
        void initButtons(Button buttonOpen, Button buttonSave);

        /**
         * Called to enable or disable the buttons
         *
         * @param buttonOpen       open button
         * @param buttonSave       save button
         * @param currentSelection currently selected items
         */
        void enableButtons(Button buttonOpen, Button buttonSave, List<Integer> currentSelection);
    }

    private interface FileManagerButtonHandler {

        void initButtons(Button buttonOpen, Button buttonSave);
    }

    /**
     * Implements FileManagerMode for save and open.
     */
    private class FileManagerSaveMode extends FileManagerOpenMode {

        @Override
        public void initButtons(Button buttonOpen, Button buttonSave) {
            buttonOpen.setVisibility(View.VISIBLE);
            buttonSave.setVisibility(View.VISIBLE);
        }

        @Override
        public void enableButtons(Button buttonOpen, Button buttonSave, List<Integer> currentSelection) {
            // Call superclass here to handle open button
            super.enableButtons(buttonOpen, buttonSave, currentSelection);

            // Only enable save button if we have a single selected file (to overwrite)
            buttonSave.setEnabled(currentSelection.size() == 1 &&
                    mCurFileList.get(currentSelection.get(0)).isFile());
        }
    }

    /**
     * Implements FileManagerMode for open only.
     */
    private class FileManagerOpenMode implements FileManagerMode {

        @Override
        public void initButtons(Button buttonOpen, Button buttonSave) {
            buttonOpen.setVisibility(View.VISIBLE);
            buttonSave.setVisibility(View.GONE);
        }

        @Override
        public void enableButtons(Button buttonOpen, Button buttonSave, List<Integer> currentSelection) {
            boolean enabled = true;
            int selectionMakeup = getSelectionMakeup(currentSelection);

            // If we are in file only mode, we enable the open button in all cases
            // except where we have a mix of files and directories selected.
            // If we are in directory only mode, we always enable the open button
            // as we always have the option of exiting with the current directory
            // as the 'opened' directory if a mix of files and directories is selected.
            if (!mIsDirectoryOnly) {
                enabled = selectionMakeup != SELECTION_CONTAINS_FILES_AND_DIRS;
            }
            buttonOpen.setEnabled(enabled);
        }
    }

    private class FileManagerSaveModeButtonHandler extends FileManagerOpenModeButtonHandler {

        @Override
        public void initButtons(Button buttonOpen, Button buttonSave) {
            // Call superclass method to set up handler for open button
            super.initButtons(buttonOpen, buttonSave);
            buttonSave.setOnClickListener(new SaveButtonOnClickListener());
        }

        private class SaveButtonOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View view) {

            }
        }
    }

    private class FileManagerOpenModeButtonHandler implements FileManagerButtonHandler {

        @Override
        public void initButtons(Button buttonOpen, Button buttonSave) {
            buttonOpen.setOnClickListener(new OpenButtonOnClickListener());
        }

        private class OpenButtonOnClickListener implements View.OnClickListener {
            @Override
            public void onClick(View view) {
                // Work out what to do based on makeup of selection
                List<Integer> selectedItems = mSelectionModel.getSelectedItems();
                switch (getSelectionMakeup(selectedItems)) {
                    case SELECTION_CONTAINS_FILES:
                        // User has made their selection of files to open
                        returnSelectedFilesAsUserChoice(selectedItems);
                        break;
                    case SELECTION_CONTAINS_DIRS:
                        if (selectedItems.size() == 1) {
                            // Open button will display content of single selected directory
                            displayDirectoryContent(mCurFileList.get(selectedItems.get(0)));
                        } else if (selectedItems.size() > 1) {
                            // Multiple selection in directory only mode, return current open directory
                            // (the one whose contents we are viewing) as the selected directory
                            returnCurrentDirAsUserChoice();
                        }
                        break;
                    case SELECTION_CONTAINS_FILES_AND_DIRS:
                    default:
                        // Either the selection contains files and directories or no selection at
                        // all. This only has meaning for directory only mode. Method call below
                        // will check the mode.
                        returnCurrentDirAsUserChoice();
                        break;
                }
            }
        }
    }
}
