/*
 * Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.uavtalk.uavobjects;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObjectField;

/**
The telemetry statistics from the ground computer

generated from gcstelemetrystats.xml
 **/
public class GCSTelemetryStats extends UAVDataObject {

	public static final String FIELD_TXDATARATE = "TxDataRate";
	public static final String FIELD_TXBYTES = "TxBytes";
	public static final String FIELD_TXFAILURES = "TxFailures";
	public static final String FIELD_TXRETRIES = "TxRetries";
	public static final String FIELD_RXDATARATE = "RxDataRate";
	public static final String FIELD_RXBYTES = "RxBytes";
	public static final String FIELD_RXFAILURES = "RxFailures";
	public static final String FIELD_RXSYNCERRORS = "RxSyncErrors";
	public static final String FIELD_RXCRCERRORS = "RxCrcErrors";
	public static final String FIELD_STATUS = "Status";
	public static final String STATUS_DISCONNECTED = "Disconnected";
	public static final String STATUS_HANDSHAKEREQ = "HandshakeReq";
	public static final String STATUS_HANDSHAKEACK = "HandshakeAck";
	public static final String STATUS_CONNECTED = "Connected";


	public GCSTelemetryStats() {
		super(OBJID, ISSINGLEINST, ISSETTINGS, NAME);
		
		List<UAVObjectField> fields = new ArrayList<>();
		

		List<String> TxDataRateElemNames = new ArrayList<>();
		TxDataRateElemNames.add("0");
		fields.add( new UAVObjectField("TxDataRate", "bytes/sec", UAVObjectField.FieldType.FLOAT32, TxDataRateElemNames, null, "") );

		List<String> TxBytesElemNames = new ArrayList<>();
		TxBytesElemNames.add("0");
		fields.add( new UAVObjectField("TxBytes", "bytes", UAVObjectField.FieldType.UINT32, TxBytesElemNames, null, "") );

		List<String> TxFailuresElemNames = new ArrayList<>();
		TxFailuresElemNames.add("0");
		fields.add( new UAVObjectField("TxFailures", "count", UAVObjectField.FieldType.UINT32, TxFailuresElemNames, null, "") );

		List<String> TxRetriesElemNames = new ArrayList<>();
		TxRetriesElemNames.add("0");
		fields.add( new UAVObjectField("TxRetries", "count", UAVObjectField.FieldType.UINT32, TxRetriesElemNames, null, "") );

		List<String> RxDataRateElemNames = new ArrayList<>();
		RxDataRateElemNames.add("0");
		fields.add( new UAVObjectField("RxDataRate", "bytes/sec", UAVObjectField.FieldType.FLOAT32, RxDataRateElemNames, null, "") );

		List<String> RxBytesElemNames = new ArrayList<>();
		RxBytesElemNames.add("0");
		fields.add( new UAVObjectField("RxBytes", "bytes", UAVObjectField.FieldType.UINT32, RxBytesElemNames, null, "") );

		List<String> RxFailuresElemNames = new ArrayList<>();
		RxFailuresElemNames.add("0");
		fields.add( new UAVObjectField("RxFailures", "count", UAVObjectField.FieldType.UINT32, RxFailuresElemNames, null, "") );

		List<String> RxSyncErrorsElemNames = new ArrayList<>();
		RxSyncErrorsElemNames.add("0");
		fields.add( new UAVObjectField("RxSyncErrors", "count", UAVObjectField.FieldType.UINT32, RxSyncErrorsElemNames, null, "") );

		List<String> RxCrcErrorsElemNames = new ArrayList<>();
		RxCrcErrorsElemNames.add("0");
		fields.add( new UAVObjectField("RxCrcErrors", "count", UAVObjectField.FieldType.UINT32, RxCrcErrorsElemNames, null, "") );

		List<String> StatusElemNames = new ArrayList<>();
		StatusElemNames.add("0");
		List<String> StatusEnumOptions = new ArrayList<>();
		StatusEnumOptions.add("Disconnected");
		StatusEnumOptions.add("HandshakeReq");
		StatusEnumOptions.add("HandshakeAck");
		StatusEnumOptions.add("Connected");
		fields.add( new UAVObjectField("Status", "", UAVObjectField.FieldType.ENUM, StatusElemNames, StatusEnumOptions, "") );


		// Compute the number of bytes for this object
		int numBytes = 0;
		for (UAVObjectField field : fields) {
            numBytes += field.getNumBytes();
        }
		NUMBYTES = numBytes;

		// Initialize object
		initializeFields(fields, ByteBuffer.allocate(NUMBYTES), NUMBYTES);
		// Set the default field values
		setDefaultFieldValues();
		// Set the object description
		setDescription(DESCRIPTION);
	}

	/**
	 * Create a Metadata object filled with default values for this object
	 * @return Metadata object with default values
	 */
	public Metadata getDefaultMetadata() {
		UAVObject.Metadata metadata = new UAVObject.Metadata();
    	metadata.flags =
		    UAVObject.Metadata.AccessModeNum(UAVObject.AccessMode.ACCESS_READWRITE) << UAVOBJ_ACCESS_SHIFT |
		    UAVObject.Metadata.AccessModeNum(UAVObject.AccessMode.ACCESS_READWRITE) << UAVOBJ_GCS_ACCESS_SHIFT |
		    0 << UAVOBJ_TELEMETRY_ACKED_SHIFT |
		    0 << UAVOBJ_GCS_TELEMETRY_ACKED_SHIFT |
		    UAVObject.Metadata.UpdateModeNum(UAVObject.UpdateMode.UPDATEMODE_MANUAL) << UAVOBJ_TELEMETRY_UPDATE_MODE_SHIFT |
		    UAVObject.Metadata.UpdateModeNum(UAVObject.UpdateMode.UPDATEMODE_PERIODIC) << UAVOBJ_GCS_TELEMETRY_UPDATE_MODE_SHIFT |
		    UAVObject.Metadata.UpdateModeNum(UAVObject.UpdateMode.UPDATEMODE_MANUAL) << UAVOBJ_LOGGING_UPDATE_MODE_SHIFT;
    	metadata.flightTelemetryUpdatePeriod = 0;
    	metadata.gcsTelemetryUpdatePeriod = 5000;
    	metadata.loggingUpdatePeriod = 0;
 
		return metadata;
	}

	/**
	 * Initialize object fields with the default values.
	 * If a default value is not specified the object fields
	 * will be initialized to zero.
	 */
	private void setDefaultFieldValues()
	{

	}

	/**
	 * Create a clone of this object, a new instance ID must be specified.
	 * Do not use this function directly to create new instances, the
	 * UAVObjectManager should be used instead.
	 */
	public UAVDataObject clone(long instID) {
		// TODO: Need to get specific instance to clone
		try {
			GCSTelemetryStats obj = new GCSTelemetryStats();
			obj.initialize(instID, this.getMetaObject());
			return obj;
		} catch  (Exception e) {
			return null;
		}
	}

	/**
	 * Returns a new instance of this UAVDataObject with default field
	 * values. This is intended to be used by 'reset to default' functionality.
	 * 
	 * @return new instance of this class with default values.
	 */
	@Override
	public UAVDataObject getDefaultInstance(){
		return new GCSTelemetryStats();
	}

	/**
	 * Static function to retrieve an instance of the object.
	 */
	public GCSTelemetryStats GetInstance(UAVObjectManager objMngr, long instID)
	{
	    return (GCSTelemetryStats)(objMngr.getObject(GCSTelemetryStats.OBJID, instID));
	}

	// Constants
	private static final long OBJID = 0xCAD1DC0AL;
	private static final String NAME = "GCSTelemetryStats";
	private static final String DESCRIPTION = "The telemetry statistics from the ground computer";
	private static final boolean ISSINGLEINST = 1 > 0;
	private static final boolean ISSETTINGS = 0 > 0;
	private static int NUMBYTES = 0;


}
