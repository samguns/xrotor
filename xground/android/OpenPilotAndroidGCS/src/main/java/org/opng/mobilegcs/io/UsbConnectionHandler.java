/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.io;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbConstants;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.HashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Used to handle the acquisition of a USB device. This class handles permission
 * requests and connect/disconnect events. Simply create an instance of this class
 * in code that requires a USB connection (CDC only) and pass it
 * an implementation of the UsbConnectionHandlerCallback interface. Call initUsb to
 * request permission, if required and start listening for connection events. When
 * this class is finished with, call disconnect to unregister the permission and
 * connection event handlers.
 */
public class UsbConnectionHandler {
    private static final String TAG = UsbConnectionHandler.class.getSimpleName();
    private static final int LOGLEVEL = 3;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean WARN = LOGLEVEL > 1;
    private static final boolean ERROR = LOGLEVEL > 0;
    //! USB constants
    private static final int OPENPILOT_VENDOR_ID = 0x20A0;
    private static final int USB_PRODUCT_ID_REVOLUTION = 0x4250;
    private static final int USB_PRODUCT_ID_REVONANO = 0x4251;
    private static final int USB_PRODUCT_ID_OPLINK = 0x4252;
    private static final String ACTION_USB_PERMISSION = "com.access.device.USB_PERMISSION";
    private final UsbConnectionHandlerCallback mUsbConnectionHandlerCallback;
    private final ExecutorService mUsbIntentExecutorService = Executors.newFixedThreadPool(2);
    /**
     * Receives a requested broadcast from the operating system.
     * In this case the following actions are handled:
     * USB_PERMISSION
     * UsbManager.EXTRA_PERMISSION_GRANTED
     * <p/>
     * Note that this will be run on the main telemetry service thread
     */
    private final BroadcastReceiver mUsbPermissionReceiver;
    /**
     * Receives a requested broadcast from the operating system.
     * In this case the following actions are handled:
     * UsbManager.ACTION_USB_DEVICE_ATTACHED
     * UsbManager.ACTION_USB_DEVICE_DETACHED
     * <p/>
     * Note that this will be run on the main telemetry service thread.
     */
    private final BroadcastReceiver mUsbReceiver;
    private UsbDevice mCurrentDevice;
    private UsbManager mUsbManager;
    private boolean mUsbReceiverRegistered = false;
    private boolean mUsbPermissionReceiverRegistered = false;

    /**
     * Creates a new instance of this class with the given callback for handling
     * device connection, validation and disconnection.
     *
     * @param usbConnectionHandlerCallback implement this to handle various events
     */
    public UsbConnectionHandler(UsbConnectionHandlerCallback usbConnectionHandlerCallback) {
        mUsbConnectionHandlerCallback = usbConnectionHandlerCallback;
        mUsbPermissionReceiver = initPermissionReceiver();
        mUsbReceiver = initBroadcastReceiver();
    }

    /**
     * Call this method to start listening for USB connection events.
     *
     * @param context context to set up broadcast receivers
     * @return true if we have a connection now, false otherwise
     */
    public boolean initUsb(Context context) {
        if (DEBUG) Log.d(TAG, "connect()");

        startListenUsbDeviceConnected(context);

        // Go through all the devices plugged in
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        if (deviceList != null) {
            if (DEBUG) Log.d(TAG, "Found " + deviceList.size() + " devices");
            for (UsbDevice dev : deviceList.values()) {
                if (DEBUG) Log.d(TAG, "Testing device: " + dev);
                if (validateFoundDevice(context, dev)) {
                    break;
                }
            }
        }
        if (DEBUG) Log.d(TAG, "Registered the deviceAttachedFilter");

        return deviceList != null && deviceList.size() > 0;
    }

    /**
     * Call this to cleanup when we're done with this class.
     *
     * @param context context used to unregister broadcast receivers
     */
    public void disconnect(Context context) {
        unregisterUsbReceiver(context);
        unregisterUsbPermissionReceiver(context);
    }

    /**
     * Dump information on connected devices to logcat.
     */
    private void dumpUsbDeviceInfo() {
        HashMap<String, UsbDevice> deviceList = mUsbManager.getDeviceList();
        if (deviceList != null) {
            if (DEBUG)
                Log.d(TAG, "dumpUsbDeviceInfo Found: " + deviceList.size() + " devices");
            for (UsbDevice dev : deviceList.values()) {
                if (DEBUG) Log.d(TAG, "dumpUsbDeviceInfo device: " + dev);
            }
        }
    }

    /**
     * Create a new broadcast receiver to handle USB device connect and disconnect
     * events. If a device is disconnected, the UsbConnectionHandlerCallback.disconnectDevice
     * method will be called back. If a new device is connected, the
     * UsbConnectionHandlerCallback.validated method will be called back to allow
     * the calling code to validate the device.
     *
     * @return broadcast receiver to handle USB device connect and disconnect
     * events
     */
    @NonNull
    private BroadcastReceiver initBroadcastReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (DEBUG)
                    Log.d(TAG, "Broadcast receiver caught intent: " + intent);
                dumpUsbDeviceInfo();
                String action = intent.getAction();
                // Validate the action against the actions registered

                if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    // A device has been detached from the device, so close all the connections
                    // and restart the search for a new device being attached
                    UsbDevice device = intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (device != null) {
                        if (DEBUG) Log.d(TAG, "Device detached: " + device);
                        if (mCurrentDevice != null) {
                            if (device.equals(mCurrentDevice)) {
                                mCurrentDevice = null;
                                if (DEBUG)
                                    Log.d(TAG, "Matching device disconnected");
                                // call your method that cleans up and closes
                                // communication with the device
                                mUsbConnectionHandlerCallback.disconnectDevice();

                                // Start listening for device connection again
                                startListenUsbDeviceConnected(context);
                            }
                        }
                    }
                } else if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                    // A device has been attached. If not already connected to a device,
                    // validate if this device is supported
                    UsbDevice searchDevice = intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (DEBUG)
                        Log.d(TAG, "Device found " + searchDevice);
                    if ((searchDevice != null) && (mCurrentDevice == null)) {
                        // Check that this device is one that we support (ie. a CC/CC3D/Revo etc.)
                        // Calling this will request permission to connect with the device which will
                        // ultimately be handled by mUsbPermissionReceiver
                        validateFoundDevice(context, searchDevice);
                    } else if (DEBUG) {
                        if (searchDevice == null) {
                            Log.d(TAG, "Failed to get device from UsbManager");
                        } else {
                            Log.d(TAG, "Already have device attached");
                        }
                    }
                }
            }
        };
    }

    /**
     * Creates a new broadcast receiver to handle USB permission requests. If
     * permission is granted, the UsbConnectionHandlerCallback.connectToDevice
     * method will be called back with the device to connect to.
     *
     * @return a new broadcast receiver to handle USB permission requests
     */
    @NonNull
    private BroadcastReceiver initPermissionReceiver() {
        return new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (DEBUG)
                    Log.d(TAG, "Broadcast receiver caught intent: " + intent);
                String action = intent.getAction();
                // Validate the action against the actions registered
                if (ACTION_USB_PERMISSION.equals(action)) {
                    // A permission response has been received, validate if the user has
                    // GRANTED, or DENIED permission
                    UsbDevice deviceConnected = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);

                    if (DEBUG)
                        Log.d(TAG, "Device Permission requested " + deviceConnected);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        // Permission has been granted, so connect to the device
                        // If this fails, then keep looking
                        connectOnGainPermission(context, deviceConnected);
                    } else {
                        // Permission has not been granted, so keep looking for another
                        // device to be attached....
                        if (DEBUG)
                            Log.d(TAG, "Device Permission Denied " + deviceConnected);
                        mCurrentDevice = null;
                    }
                }
            }
        };
    }

    private void connectOnGainPermission(Context context, UsbDevice deviceConnected) {
        if (deviceConnected != null && mCurrentDevice == null) {
            // call method to setup device communication
            mCurrentDevice = deviceConnected;
            if (DEBUG)
                Log.d(TAG, "Device Permission Acquired " + mCurrentDevice);
            if (!connectToDeviceInterface(mCurrentDevice)) {
                if (DEBUG)
                    Log.d(TAG, "Unable to connect to device");
                mCurrentDevice = null;

                // This connection failed, need to start listening for a new one as
                // our usb permission receiver has been removed by this point
                startListenUsbDeviceConnected(context);
            }
        }
    }

    /**
     * Unregister the permission broadcast receiver.
     *
     * @param context context used to unregister receiver, should be
     *                the same one used to register it
     */
    private void unregisterUsbPermissionReceiver(Context context) {
        if (mUsbPermissionReceiverRegistered) {
            if (DEBUG)
                Log.d(TAG, "USBUAVTalk unregister mUsbPermissionReceiver");
            context.unregisterReceiver(mUsbPermissionReceiver);
            mUsbPermissionReceiverRegistered = false;
        }
    }

    /**
     * Unregister the USB connection broadcast receiver.
     *
     * @param context context used to unregister receiver, should be
     *                the same one used to register it
     */
    private void unregisterUsbReceiver(Context context) {
        if (mUsbReceiverRegistered) {
            if (DEBUG) Log.d(TAG, "USBUAVTalk unregister mUsbReceiver");
            context.unregisterReceiver(mUsbReceiver);
            mUsbReceiverRegistered = false;
        }
    }

    /**
     * Registers the permission and connection broadcast receivers with the
     * given context. The broadcast receivers should have previously been
     * created with initBroadcastReceiver and initPermissionReceiver.
     *
     * @param context used to register the broadcast receivers.
     */
    private void startListenUsbDeviceConnected(Context context) {

//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        if (!mUsbPermissionReceiverRegistered) {
            if (DEBUG) Log.d(TAG, "Start listening for usb permission");
            // Register to get permission requested dialog
            mUsbManager = (UsbManager) context.getSystemService(Context.USB_SERVICE);
            IntentFilter permissionFilter = new IntentFilter(ACTION_USB_PERMISSION);
            context.registerReceiver(mUsbPermissionReceiver, permissionFilter);
            mUsbPermissionReceiverRegistered = true;
        }

        if (!mUsbReceiverRegistered) {
            if (DEBUG) Log.d(TAG, "Start listening for usb attach/detach");
            // Register to be notified of USB device connection
            IntentFilter deviceAttachedFilter = new IntentFilter();
            deviceAttachedFilter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
            deviceAttachedFilter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
            context.registerReceiver(mUsbReceiver, deviceAttachedFilter);
            mUsbReceiverRegistered = true;
        }
    }

    /**
     * Checks that the given device supports CDC and is an OpenPilot/OPNG device.
     * The UsbConnectionHandlerCallback.validated method is called
     * to allow the calling code to further validate or otherwise allow/deny
     * the connection. If validation succeeds the code will start listening
     * for connections if it isn't already doing so before requesting connection
     * permission from the user.
     *
     * @param context      used to register broadcast receivers if they are currently
     *                     unregistered
     * @param searchDevice device that has just been connected
     * @return true if validation succeeded, false otherwise
     */
    private boolean validateFoundDevice(final Context context, final UsbDevice searchDevice) {
        boolean retVal = false;
        //A vendor id is a global identifier for the manufacturer. A product id refers to the product itself, and is unique to the manufacturer. The vendor id, product id combination refers to a particular product manufactured by a vendor.
        if (DEBUG) Log.d(TAG, "validateFoundDevice: " + searchDevice);

        if ((searchDevice.getDeviceClass() & UsbConstants.USB_CLASS_CDC_DATA) == UsbConstants.USB_CLASS_CDC_DATA &&
                searchDevice.getVendorId() == OPENPILOT_VENDOR_ID) {
            //Requesting permission
            if (DEBUG) Log.d(TAG, "Device validated: " + searchDevice);

            if (mUsbConnectionHandlerCallback.validated()) {
                // Set up permission receiver if it's previously been unregistered
                startListenUsbDeviceConnected(context);
                if (mUsbManager.hasPermission(searchDevice)) {
                    connectOnGainPermission(context, searchDevice);
                } else {
                    mUsbIntentExecutorService.submit(new Runnable() {
                        @Override
                        public void run() {
                            PendingIntent mPermissionIntent = PendingIntent.getBroadcast(context, 0, new Intent(ACTION_USB_PERMISSION), 0);
                            mUsbManager.requestPermission(searchDevice, mPermissionIntent);
                        }
                    });
                }
                retVal = true;
            }
        }

        return retVal;
    }

    /**
     * Called when permission to connect to the given device has been granted by
     * the user. This will call the UsbConnectionHandlerCallback.connectToDevice
     * method to allow the calling code to set up communication with the connected
     * device.
     *
     * @param connectDevice device to start communicating with
     * @return true if communication initiated successfully, false otherwise
     */
    private boolean connectToDeviceInterface(UsbDevice connectDevice) {
        int productId = connectDevice.getProductId();
        return mUsbConnectionHandlerCallback.connectToDevice(connectDevice, mUsbManager, productId == USB_PRODUCT_ID_OPLINK ||
                productId == USB_PRODUCT_ID_REVONANO ||
                productId == USB_PRODUCT_ID_REVOLUTION);
    }

    /**
     * Implement this in calling code to handle device validation, connection
     * and disconnection.
     */
    public interface UsbConnectionHandlerCallback {
        /**
         * Called to allow the calling code to perform its own validation of a
         * connected device.
         *
         * @return true if connection permission can be sought, false otherwise
         */
        boolean validated();

        /**
         * Called to allow the calling code to initiate communication with the
         * given device. Use the given UsbManager instance to open the given
         * device.
         *
         * @param device        device to initiate communication with
         * @param usbManager    usb manager to use to open device connection
         * @param allowedDevice this will be true if the device is to be used,
         *                      false otherwise. It is passed to the calling
         *                      code to allow it to display an error message
         *                      in the event of attempts to connect to an
         *                      unsupported device
         * @return true if communication was successfully initiated, false otherwise
         */
        boolean connectToDevice(UsbDevice device, UsbManager usbManager, boolean allowedDevice);

        /**
         * Called to allow the calling code to end communication with the
         * device and perform any cleanup. This callback must call the
         * disconnect method.
         */
        void disconnectDevice();
    }
}