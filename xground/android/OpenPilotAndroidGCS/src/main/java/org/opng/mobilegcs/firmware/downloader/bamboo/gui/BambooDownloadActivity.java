/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.firmware.downloader.bamboo.gui;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.filemanager.FileManagerActivity;
import org.opng.mobilegcs.views.PasswordPromptDialogFragment;

import java.util.ArrayList;

public class BambooDownloadActivity extends AppCompatActivity implements BambooDownloadFragment.OnFragmentInteractionListener,
        PasswordPromptDialogFragment.PasswordPromptListener {
    private static final int SELECT_DOWNLOAD_LOCATION = 1;
    private BambooDownloadFragment mBambooDownloadFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bamboo_download);

        Toolbar actionBarToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(actionBarToolbar);

        // Show the Up button in the toolbar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mBambooDownloadFragment =
                (BambooDownloadFragment) getSupportFragmentManager().findFragmentById(R.id.fragmentBambooDownload);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_DOWNLOAD_LOCATION) {
            if (resultCode == RESULT_OK) {
                // Firmware selected ok, get selected firmware from given intent
                Bundle bundle = data.getBundleExtra(FileManagerActivity.FILE_MANAGER_RESULT_DATA);
                ArrayList<String> paths = bundle.getStringArrayList(FileManagerActivity.FILE_MANAGER_BUNDLE_SELECTED_FILES_KEY);

                // Only expecting one filename
                if (paths != null && !paths.isEmpty()) {
                    mBambooDownloadFragment.setFileSavePath(paths.get(0));
                }
            }
        }
    }

    @Override
    public void onBrowseClicked() {
        Intent intent = new Intent(this, FileManagerActivity.class);
        Bundle bundle = new Bundle();
        bundle.putBoolean(FileManagerActivity.FILE_MANAGER_DIR_ONLY, true);
        bundle.putBoolean(FileManagerActivity.FILE_MANAGER_IS_FOR_SAVE, true);
        bundle.putString(FileManagerActivity.FILE_MANAGER_START_DIR, mBambooDownloadFragment.getFileSavePath());
        intent.putExtra(FileManagerActivity.FILE_MANAGER_OPEN, bundle);
        startActivityForResult(intent, SELECT_DOWNLOAD_LOCATION);
    }

    @Override
    public void promptForPassword() {
        PasswordPromptDialogFragment passwordPromptDialogFragment = new PasswordPromptDialogFragment();
        passwordPromptDialogFragment.show(getFragmentManager(), "PasswordPromptDialogFragment");
    }

    @Override
    public void onLoginClicked(String authenticationString) {
        if (mBambooDownloadFragment != null) {
            mBambooDownloadFragment.fetchBranchesForPlan(authenticationString);
        }
    }

    @Override
    public void onCancelLoginClicked() {

    }
}
