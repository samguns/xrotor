/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.systemalarms;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.UAVObjectLoader;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.SystemAlarms;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class SystemAlarmsFragment extends Fragment implements LoaderManager.LoaderCallbacks<UAVObjectManager> {
    private static final int LOGLEVEL = 1;
    private static final boolean DEBUG = LOGLEVEL > 1;
    public static boolean WARN = LOGLEVEL > 0;
    // Logging settings
    private final String TAG = SystemAlarmsFragment.class.getSimpleName();
    private final AlarmObjectEventListener mAlarmObjectEventListener = new AlarmObjectEventListener();
    private final ArrayList<AlarmInfo> mAlarmIndicators = new ArrayList<>();
    private UAVObject mAlarmsObject;
    private Handler mHandler;

    public SystemAlarmsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_system_alarms, container, false);

        getLoaderManager().initLoader(0, null, this);

        mHandler = new Handler();

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        disableObjectUpdates();
    }

    @Override
    public void onResume() {
        super.onResume();
        enableObjectUpdates();
    }

    @Override
    public void onStop() {
        super.onStop();
        disableObjectUpdates();
    }

    @Override
    public Loader<UAVObjectManager> onCreateLoader(int id, Bundle args) {
        return new UAVObjectLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<UAVObjectManager> loader, UAVObjectManager data) {
        mAlarmsObject = data.getObject("SystemAlarms");
        initAlarms();
        enableObjectUpdates();
        updateAlarms(mAlarmsObject);
    }

    @Override
    public void onLoaderReset(Loader<UAVObjectManager> loader) {

    }

    private void enableObjectUpdates() {
        if (mAlarmsObject != null) {
            if (DEBUG) Log.d(TAG, "Adding alarm updated listener");
            mAlarmsObject.addUAVObjectEventListener(mAlarmObjectEventListener);
        }
    }

    private void disableObjectUpdates() {
        if (mAlarmsObject != null) {
            if (DEBUG) Log.d(TAG, "Removing alarm updated listener");
            mAlarmsObject.removeUAVObjectEventListener(mAlarmObjectEventListener);
        }
    }

    private void initAlarms() {
        View rootView = getView();
        if (mAlarmsObject != null && rootView != null) {
            // Get alarm field from UAVObject and iterate through all its elements
            UAVObjectField alarmField = mAlarmsObject.getField(SystemAlarms.FIELD_ALARM);
            if (alarmField != null) {
                List<String> elementNames = alarmField.getElementNames();

                for (int elementCount = 0; elementCount < alarmField.getNumElements(); ++elementCount) {
                    // For each element, find its corresponding indicator in the view
                    // and set its initial value.
                    TextView curIndicator = null;
                    String alarmName = elementNames.get(elementCount);

                    switch (alarmName) {
                        case SystemAlarms.ALARM_SYSTEMCONFIGURATION:
                            curIndicator = (TextView) rootView.findViewById(R.id.textConfig);
                            break;
                        case SystemAlarms.ALARM_BOOTFAULT:
                            curIndicator = (TextView) rootView.findViewById(R.id.textBoot);
                            break;
                        case SystemAlarms.ALARM_OUTOFMEMORY:
                            curIndicator = (TextView) rootView.findViewById(R.id.textMemory);
                            break;
                        case SystemAlarms.ALARM_STACKOVERFLOW:
                            curIndicator = (TextView) rootView.findViewById(R.id.textStack);
                            break;
                        case SystemAlarms.ALARM_CPUOVERLOAD:
                            curIndicator = (TextView) rootView.findViewById(R.id.textCPU);
                            break;
                        case SystemAlarms.ALARM_EVENTSYSTEM:
                            curIndicator = (TextView) rootView.findViewById(R.id.textEvent);
                            break;
                        case SystemAlarms.ALARM_TELEMETRY:
                            curIndicator = (TextView) rootView.findViewById(R.id.textTelemetry);
                            break;
                        case SystemAlarms.ALARM_RECEIVER:
                            curIndicator = (TextView) rootView.findViewById(R.id.textInput);
                            break;
                        case SystemAlarms.ALARM_MANUALCONTROL:
                            // Leave curIndicator null, obsolete alarm? Leave it in to
                            // make sure element indices match up with array positions
                            // in _alarmIndicators
                            break;
                        case SystemAlarms.ALARM_ACTUATOR:
                            curIndicator = (TextView) rootView.findViewById(R.id.textOutput);
                            break;
                        case SystemAlarms.ALARM_ATTITUDE:
                            curIndicator = (TextView) rootView.findViewById(R.id.textAttitude);
                            break;
                        case SystemAlarms.ALARM_SENSORS:
                            curIndicator = (TextView) rootView.findViewById(R.id.textSensors);
                            break;
                        case SystemAlarms.ALARM_MAGNETOMETER:
                            curIndicator = (TextView) rootView.findViewById(R.id.textMagnetometer);
                            break;
                        case SystemAlarms.ALARM_AIRSPEED:
                            curIndicator = (TextView) rootView.findViewById(R.id.textAirspeed);
                            break;
                        case SystemAlarms.ALARM_STABILIZATION:
                            curIndicator = (TextView) rootView.findViewById(R.id.textStabilization);
                            break;
                        case SystemAlarms.ALARM_GUIDANCE:
                            curIndicator = (TextView) rootView.findViewById(R.id.textPath);
                            break;
                        case SystemAlarms.ALARM_PATHPLAN:
                            curIndicator = (TextView) rootView.findViewById(R.id.textPlan);
                            break;
                        case SystemAlarms.ALARM_BATTERY:
                            curIndicator = (TextView) rootView.findViewById(R.id.textBattery);
                            break;
                        case SystemAlarms.ALARM_FLIGHTTIME:
                            curIndicator = (TextView) rootView.findViewById(R.id.textFlightTime);
                            break;
                        case SystemAlarms.ALARM_I2C:
                            curIndicator = (TextView) rootView.findViewById(R.id.textI2c);
                            break;
                        case SystemAlarms.ALARM_GPS:
                            curIndicator = (TextView) rootView.findViewById(R.id.textGPS);
                            break;
                    }
                    mAlarmIndicators.add(new AlarmInfo(alarmField.getValue(elementCount).toString(), curIndicator));
                }
            }
        }
    }

    private void updateAlarms(UAVObject obj) {
        if (obj != null && obj instanceof SystemAlarms) {
            if (DEBUG) Log.d(TAG, "Updating alarms");

            // Loop through the alarms, find the corresponding AlarmInfo and
            // set the current alarm value if it's changed.
            UAVObjectField alarmField = obj.getField(SystemAlarms.FIELD_ALARM);
            for (int elementCount = 0; elementCount < alarmField.getNumElements(); ++elementCount) {
                mAlarmIndicators.get(elementCount).setAlarmStatus(alarmField.getValue(elementCount).toString());
            }
        }
    }

    private class AlarmObjectEventListener extends UAVObjectEventAdapter {
        @Override
        public void objectUpdated(UAVObject obj) {
            if (DEBUG) Log.d(TAG, "Handle alarm updated event");

            updateAlarms(obj);
        }
    }

    private class AlarmInfo {
        private final TextView _indicator;
        private AlarmStatus _currentStatus;

        AlarmInfo(String alarmInitialValue, TextView alarmIndicator) {
            _currentStatus = AlarmStatus.fromString(alarmInitialValue);
            _indicator = alarmIndicator;
            if (_indicator != null) {
                updateIndicator();
            }
        }

        void setAlarmStatus(String newValue) {
            AlarmStatus newStatus = AlarmStatus.fromString(newValue);

            if (DEBUG)
                Log.d(TAG, "Current status: " + _currentStatus.toString() + " ; New status: " + newValue);

            if (_indicator != null && !newStatus.equals(_currentStatus)) {
                _currentStatus = newStatus;

                mHandler.post(new Runnable() {

                    @Override
                    public void run() {
                        updateIndicator();
                    }
                });
            }
        }

        private void updateIndicator() {

            switch (_currentStatus) {
                case CRITICAL:
                    if (DEBUG) Log.d(TAG, "Setting " + _indicator.getText() + " CRITICAL");
                    _indicator.setBackgroundResource(R.drawable.alarm_background_critical);
                    _indicator.setTextColor(Color.WHITE);
                    break;
                case ERROR:
                    if (DEBUG) Log.d(TAG, "Setting " + _indicator.getText() + " ERROR");
                    _indicator.setBackgroundResource(R.drawable.alarm_background_error);
                    _indicator.setTextColor(Color.RED);
                    break;
                case OK:
                    if (DEBUG) Log.d(TAG, "Setting " + _indicator.getText() + " OK");
                    _indicator.setBackgroundResource(R.drawable.alarm_background_ok);
                    _indicator.setTextColor(0xff4d4d4d);
                    break;
                case WARNING:
                    if (DEBUG) Log.d(TAG, "Setting " + _indicator.getText() + " WARNING");
                    _indicator.setBackgroundResource(R.drawable.alarm_background_warn);
                    _indicator.setTextColor(Color.WHITE);
                    break;
                default:
                    if (DEBUG) Log.d(TAG, "Setting " + _indicator.getText() + " DEFAULT");
                    _indicator.setBackgroundResource(R.drawable.alarm_background);
                    _indicator.setTextColor(Color.WHITE);
                    break;
            }
        }
    }
}
