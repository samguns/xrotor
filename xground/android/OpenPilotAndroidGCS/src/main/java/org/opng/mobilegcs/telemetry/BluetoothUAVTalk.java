/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.telemetry;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.ParcelUuid;
import android.util.Log;

import java.io.IOException;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Set;
import java.util.UUID;

/**
 * http://developer.android.com/guide/topics/connectivity/bluetooth.html
 */
public class BluetoothUAVTalk extends TelemetryTask {

    private static final String TAG = BluetoothUAVTalk.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    public static final boolean WARN = LOGLEVEL > 1;
    private static final boolean VERBOSE = LOGLEVEL > 3;
    private static final boolean DEBUG = LOGLEVEL > 2;
    private static final boolean ERROR = LOGLEVEL > 0;

    private static final String UUID_SPP_DEVICE = "00001101-0000-1000-8000-00805F9B34FB";
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothSocket mBluetoothSocket;

    private Thread mReadThread;
    private Thread mWriteThread;

    public BluetoothUAVTalk(OPTelemetryService service) {
        super(service);

    }

    @Override
    boolean attemptConnection() {
        if (DEBUG) Log.d(TAG, "connect()");

        // get bluetooth radio
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            if (DEBUG) Log.d(TAG, "Null adapters");
        }


        try {
            openAndroidConnection();

        } catch (IOException e) {
            attemptedFailed();
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private void openAndroidConnection() throws IOException {
        if (DEBUG) Log.d(TAG, "openAndroidConnection : Connect");

        // Reset the bluetooth connection
        resetConnection();

        // Retrieve the stored device
        BluetoothDevice device = null;

        if (mPrefs == null)
            Log.d(TAG, "prefs is null");

        final String addressName = mPrefs != null ? mPrefs.getBluetoothDeviceAddress() : null;

        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        mTelemetryService.sendOrderedBroadcast(enableBtIntent, "android.permission.BLUETOOTH_ADMIN", new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e(TAG, "Received " + context + intent);

                //queryDevices();
            }
        }, null, Activity.RESULT_OK, null, null);


        if (addressName != null) {
            // strip name, use address part - stored as <address>;<name>
            final String part[] = addressName.split(";");
            try {
                device = mBluetoothAdapter.getRemoteDevice(part[0]);
            } catch (IllegalArgumentException ex) {
                // invalid configuration (device may have been removed)
                // NOP fall through to 'no device'
            }
        }
        // no device
        if (device == null)
            device = findSerialBluetoothBoard();

        if (DEBUG) Log.d(TAG, "Trying to connect to device with address " + device.getAddress());
        if (DEBUG) Log.d(TAG, "BT Create Socket Call...");
        mBluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(UUID
                .fromString(UUID_SPP_DEVICE));

        if (DEBUG) Log.d(TAG, "BT Cancel Discovery Call...");
        mBluetoothAdapter.cancelDiscovery();

        if (DEBUG) Log.d(TAG, "BT Connect Call...");
        try {
            mBluetoothSocket.connect(); // Here the IOException will rise on BT
        } catch (IOException e) {

            try {
                mTelemetryService.toastMessage("Bluetooth Connection Failed");
                mBluetoothSocket.close();
                return;
            } catch (IOException e2) {
                if (ERROR) Log.e(TAG, "unable to close() socket during connection failure", e2);
            }
        }
        // protocol/handshake error.

        if (DEBUG) Log.d(TAG, "## BT Connected ##");
        mOutStream = mBluetoothSocket.getOutputStream();
        mInStream = mBluetoothSocket.getInputStream();

        // This method is running on the main telemetry service thread but we want to
        // call attemptSucceeded on the UAVTalk telemetry task thread
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                attemptSucceeded();
            }
        });

        mTelemetryService.toastMessage("Bluetooth Device Opened");


    }

    @SuppressLint("NewApi")
    private BluetoothDevice findSerialBluetoothBoard() throws UnknownHostException {
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        // If there are paired devices
        if (pairedDevices.size() > 0) {
            // Loop through paired devices
            for (BluetoothDevice device : pairedDevices) {
                // Add the name and address to an array adapter to show in a
                // ListView
                if (DEBUG) Log.d(TAG, device.getName() + " #" + device.getAddress() + "#");
                for (ParcelUuid id : device.getUuids()) {
                    // TODO maybe this will not work on newer devices
                    if (DEBUG) Log.d(TAG, "id:" + id.toString());
                    if (id.toString().equalsIgnoreCase(UUID_SPP_DEVICE)) {
                        Log.d(TAG, ">> Selected: " + device.getName() + " Using: " + id.toString());
                        return device;
                    }
                }
            }
        }
        throw new UnknownHostException("No Bluetooth Device found");
    }


    private void resetConnection() throws IOException {
        if (mInStream != null) {
            mInStream.close();
            mInStream = null;
        }

        if (mOutStream != null) {
            mOutStream.close();
            mOutStream = null;
        }

        if (mBluetoothSocket != null) {
            mBluetoothSocket.close();
            mBluetoothSocket = null;
        }

    }

    @Override
    void disconnect() {
        if (DEBUG) Log.d(TAG, "BlueToothUAVTalk shutting down");

        super.disconnect();

        try {
            if (mReadThread != null) {
                mReadThread.interrupt(); // Make sure not blocking for data
                mReadThread.join();
                mReadThread = null;
                if (DEBUG) Log.d(TAG, "Bluetooth read thread ended");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            if (mWriteThread != null) {
                mWriteThread.interrupt();
                mWriteThread.join();
                mWriteThread = null;
                if (DEBUG) Log.d(TAG, "Bluetooth write thread ended");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (mBluetoothSocket != null) {
            if (mBluetoothSocket.isConnected()) {
                try {
                    mBluetoothSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected int rawReadData(ByteBuffer dataIn) {
        int retVal = -1;
        if (mBluetoothSocket != null) {
            try {
                retVal = mInStream.read(dataIn.array());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return retVal;
    }

    @Override
    protected int rawWriteData(ByteBuffer dataOut) {
        int retVal = 1;
        if (mBluetoothSocket != null) {
            try {
                mOutStream.write(dataOut.array());
            } catch (IOException e) {
                e.printStackTrace();
                retVal = -1;
            }
        }
        return retVal;
    }
}
