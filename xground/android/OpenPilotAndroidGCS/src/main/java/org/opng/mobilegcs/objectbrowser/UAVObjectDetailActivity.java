/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.objectbrowser;

import android.os.Bundle;

import org.opng.mobilegcs.R;
import org.opng.mobilegcs.util.ActivityTelemetryConnectionHandler;
import org.opng.mobilegcs.util.TelemetryActivity;

/**
 * An activity representing a single UAVObject detail screen. This activity is
 * only used on handset devices. On tablet-size devices, item details are
 * presented side-by-side with a list of items in a
 * {@link UAVObjectListActivity}.
 * <p/>
 * This activity is mostly just a 'shell' activity containing nothing more than
 * a {@link UAVObjectDetailFragment}.
 */
public class UAVObjectDetailActivity extends TelemetryActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setActionProvidersToShow(ActivityTelemetryConnectionHandler.TELEMETRY_STATUS |
                ActivityTelemetryConnectionHandler.SYSTEM_ALARMS);
        setContentView(R.layout.activity_uavobject_detail);

        // Show the Up button in the action bar.
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        // savedInstanceState is non-null when there is fragment state
        // saved from previous configurations of this activity
        // (e.g. when rotating the screen from portrait to landscape).
        // In this case, the fragment will automatically be re-added
        // to its container so we don't need to manually add it.
        // For more information, see the Fragments API guide at:
        //
        // http://developer.android.com/guide/components/fragments.html
        //
        if (savedInstanceState == null) {
            // Create the detail fragment and add it to the activity
            // using a fragment transaction.
            Bundle arguments = new Bundle();
            arguments.putString(
                    UAVObjectDetailFragment.ARG_ITEM_ID,
                    getIntent().getStringExtra(
                            UAVObjectDetailFragment.ARG_ITEM_ID));
            UAVObjectDetailFragment fragment = new UAVObjectDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.uavobject_detail_container, fragment).commit();
        }
    }
}
