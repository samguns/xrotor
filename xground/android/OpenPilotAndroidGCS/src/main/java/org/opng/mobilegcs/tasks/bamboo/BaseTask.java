/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *
 */
package org.opng.mobilegcs.tasks.bamboo;

import android.os.AsyncTask;

import org.opng.mobilegcs.firmware.downloader.bamboo.Plan;

import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

public abstract class BaseTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    protected Exception mThrownException;
    protected SSLContext mSslContext;

    protected boolean initKeyStoreHandling(KeyStore keyStore) {
        boolean retVal = false;

        if(keyStore != null){
            String algorithm = TrustManagerFactory.getDefaultAlgorithm();
            try {
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(algorithm);
                trustManagerFactory.init(keyStore);
                mSslContext = SSLContext.getInstance("TLS");
                mSslContext.init(null, trustManagerFactory.getTrustManagers(), null);
                retVal = true;
            } catch (NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
                mThrownException = e;
            }
        }

        return retVal;
    }

    public interface FetchCallback {
        void fetchComplete(Boolean result);

        void needPassword();

        void handleException(Exception exception);
    }
}
