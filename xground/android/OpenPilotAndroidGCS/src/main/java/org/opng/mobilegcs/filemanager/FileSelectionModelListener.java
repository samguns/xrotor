/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */
package org.opng.mobilegcs.filemanager;

import java.util.List;

interface FileSelectionModelListener {
    void selectionChanged(List<Integer> currentSelection);

    void itemLongClicked(int itemPosition);
}
