/*
 * David Willis Copyright (c) 2016.
 * Terms of Use:
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 */

package org.opng.mobilegcs.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.opng.mobilegcs.BuildConfig;
import org.opng.mobilegcs.R;
import org.opng.mobilegcs.telemetry.OPTelemetryService;
import org.opng.mobilegcs.views.HorizontalNumberPickerView;
import org.opng.mobilegcs.views.curve.CurveData;
import org.opng.mobilegcs.views.curve.CurveView;
import org.opng.uavtalk.UAVDataObject;
import org.opng.uavtalk.UAVObject;
import org.opng.uavtalk.UAVObjectEventAdapter;
import org.opng.uavtalk.UAVObjectField;
import org.opng.uavtalk.UAVObjectManager;
import org.opng.uavtalk.uavobjects.ObjectPersistence;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

public class UAVObjectToGuiUtils {
    private final static String TAG = UAVObjectToGuiUtils.class.getSimpleName();
    private static final int LOGLEVEL = 2;
    private static final boolean DEBUG = LOGLEVEL > 1;
    private static boolean WARN = LOGLEVEL > 0;
    private final Set<UAVObjectToGuiUtilsEventListener> mUAVObjectToGuiUtilsEventListeners = new CopyOnWriteArraySet<>();
    private final List<ObjectToView> mObjectsOfInterest = new ArrayList<>();
    private final Map<UAVObject, List<ObjectToView>> mObjToGuiMapping = new HashMap<>();
    private final UAVObjectManager mObjectManager;
    private final UAVObjectUtilManager mUAVObjectUtilManager;
    private final SparseArray<List<ObjectToView>> mDefaultReloadGroups = new SparseArray<>();
    private final Map<View, ObjectToView> mShadowsList = new HashMap<>();
    private final Map<Button, String> mHelpButtonList = new HashMap<>();
    private final List<Button> mReloadButtonList = new ArrayList<>();
    private final ExecutorService mReloadExecutor = Executors.newFixedThreadPool(2);
    private final LocalBroadcastManager mLocalBroadcastManager;
    private final ObjectUpdatedListener mObjectUpdatedListener = new ObjectUpdatedListener();
    private final Context mContext;
    private final Handler mHandler;
    private final Map<View, Boolean> mDisableChangedListenerMap = new ConcurrentHashMap<>();
    private final Map<View, String[]> mBooleanWidgetValues = new HashMap<>();
    private int mCurrentBoard;
    private boolean mIsConnected;
    private boolean mAllowViewUpdates;
    private SmartSave mSmartSave;
    private boolean mDirty;
    private String mOutOfLimitsStyle;
    private CountDownLatch mTimeoutLatch;
    public UAVObjectToGuiUtils(Context context,
                               UAVObjectManager objManager, UAVObjectUtilManager utilMngr) {
        mContext = context;
        mObjectManager = objManager;
        mUAVObjectUtilManager = utilMngr;
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(context);
        mHandler = new Handler();
        setupConnectionBroadcastListener();
    }

    public int getCurrentBoard() {
        return mCurrentBoard;
    }

    /**
     * Add a UAVObject field to widget relation to the management system
     *
     * @param object              name of the object to add
     * @param field               name of the field to add
     * @param view                pointer to the widget which will display/define the field
     *                            value
     * @param index               index of the element of the field to add to this relation.
     *                            If the given view can show all elements of the given field
     *                            simultaneously, (a CurveView with a point per element for
     *                            example) then this value should be -1
     * @param scale               scale value of this relation
     * @param isLimited           bool to signal if this widget contents is limited in value
     * @param defaultReloadGroups default and reload groups this relation belongs to
     * @param instID              instance ID of the object used on this relation
     */
    public void addUAVObjectToWidgetRelation(UAVObject object, UAVObjectField field,
                                             View view, int index, double scale, boolean isLimited,
                                             List<Integer> defaultReloadGroups, long instID) {
        if (addShadowView(object, field, view, index, scale, isLimited,
                defaultReloadGroups, instID))
            return;
        if (BuildConfig.DEBUG && scale == 0) {
            throw new AssertionError("Scale should not be set to zero");
        }
        if (BuildConfig.DEBUG && object == null) {
            throw new AssertionError("UAVObject is null");
        }
//            _objectUpdates.put(obj, Boolean.TRUE);
        // TODO: Do we need to remove this listener at some point?
//            obj.addUAVObjectEventListener(new UAVObjectEventAdapter(){
//
//                @Override
//                public void objectUpdated(UAVObject obj) {
//                    UAVObjectToGuiUtils.this.objectUpdated(obj);
//                    refreshWidgetsValues(obj);
//                }
//            });
//        object.addUAVObjectEventListener(_objUpdatedListener);

        // Check to see if the given view is a check style widget: checkbox or switch
        if (view instanceof Checkable) {
            // Do we have values set up for true and false?
            if (!mBooleanWidgetValues.containsKey(view)) {
                // Nothing set up: add default "True" and "False"
                mBooleanWidgetValues.put(view, new String[]{"True", "False"});
            }
        }

        ObjectToView ov = new ObjectToView();
        ov.field = field;
        ov.object = object;
        ov.view = view;
        ov.index = index;
        ov.scale = scale;
        ov.isLimited = isLimited;
        mObjectsOfInterest.add(ov);
        addObjectToMapping(object, ov);
        if (mSmartSave != null) {
            mSmartSave.addObject((UAVDataObject) object);
        }
        if (view == null) {
            if (defaultReloadGroups != null) {
                for (Integer i : defaultReloadGroups) {
                    List<ObjectToView> currentGroup = mDefaultReloadGroups.get(i);
                    if (currentGroup != null) {
                        currentGroup.add(ov);
                    } else {
                        mDefaultReloadGroups.put(i,
                                new ArrayList<ObjectToView>());
                        mDefaultReloadGroups.get(i).add(ov);
                    }
                }
            }
        } else {
            connectWidgetUpdatesToSlot(view);
            if (defaultReloadGroups != null)
                addWidgetToDefaultReloadGroups(view, defaultReloadGroups);
            mShadowsList.put(view, ov);
            loadWidgetLimits(view, field, index, isLimited, scale);
        }
    }

    /**
     * Add a UAVObject field to widget relation to the management system. This
     * overload takes a String[] parameter that can be used to set string values
     * for to associate with the checked or unchecked state of a Checkable widget.
     * If this overload is not used with a Checkable widget, the default values
     * of "True" and "False" will be used.
     *
     * @param object              name of the object to add
     * @param field               name of the field to add
     * @param view                pointer to the widget which will display/define the field
     *                            value
     * @param index               index of the element of the field to add to this relation
     * @param scale               scale value of this relation
     * @param isLimited           bool to signal if this widget contents is limited in value
     * @param defaultReloadGroups default and reload groups this relation belongs to
     * @param instID              instance ID of the object used on this relation
     * @param strings             pair of strings used to represent the checked (strings[0])
     *                            and unchecked (strings[1]) states of a Checkable widget
     */
    public void addUAVObjectToWidgetRelation(UAVObject object, UAVObjectField field,
                                             View view, int index, double scale, boolean isLimited,
                                             List<Integer> defaultReloadGroups, long instID, String[] strings) {
        mBooleanWidgetValues.put(view, strings);
        addUAVObjectToWidgetRelation(object, field, view, index, scale, isLimited, defaultReloadGroups, instID);
    }

    /**
     * Display the given value in the given widget. If the widget is set
     * to scale the value, this scale will still be applied. This is
     * intended to be used to display a value in a widget without updating
     * the connected object.
     *
     * @param view  view to display given value in
     * @param value value to display in given view
     */
    public void displayValueInWidget(View view, Variant value) {
        ObjectToView ov = mShadowsList.get(view);

        if (ov != null) {
            setWidgetFromVariant(view, value, ov.scale);
        }
    }

    public void saveObjectToSD(UAVDataObject obj) {
        if (mSmartSave == null) {
            mSmartSave = new SmartSave(mUAVObjectUtilManager);
        }
        mSmartSave.save(obj);
    }

    public void saveObjectToRam(UAVDataObject obj) {
        if (mSmartSave == null) {
            mSmartSave = new SmartSave(mUAVObjectUtilManager);
        }
        mSmartSave.apply(obj);
    }

    /**
     * Use this method to enable saves for the given object when saves
     * have previously been disabled with disableSavesForObject().
     *
     * @param object UAVObject to re-enable saves for
     */
    public void enableSavesForObject(UAVObject object) {
        if (mSmartSave != null) {
            mSmartSave.enableSavesForObject(object);
        }
    }
//    /**
//     * Add a widget to the dirty detection pool
//     *
//     * @param view
//     *            to add to the detection pool
//     */
//    public void addView(View view) {
//        addUAVObjectToWidgetRelation("", "", view, 0, 1, false, null, 0);
//    }

//    /**
//     * Add an object to the management system
//     *
//     * @param objectName
//     *            name of the object to add to the management system
//     */
//    void addUAVObject(String objectName, List<Integer> reloadGroups) {
//        addUAVObjectToWidgetRelation(objectName, "", null, 0, 1, false,
//                reloadGroups, 0);
//    }

//    public void addUAVObject(UAVObject objectName, List<Integer> reloadGroups) {
//        String objstr = "";
//        if (objectName != null)
//            objstr = objectName.getName();
//        addUAVObject(objstr, reloadGroups);
//    }

//    public void addUAVObjectToWidgetRelation(UAVObject obj,
//            UAVObjectField field, View view, int index, double scale,
//            boolean isLimited, List<Integer> defaultReloadGroups, long instID) {
//        String objstr = "";
//        String fieldstr = "";
//        if (obj != null) {
//            objstr = obj.getName();
//        }
//        if (field != null) {
//            fieldstr = field.getName();
//        }
//        addUAVObjectToWidgetRelation(objstr, fieldstr, view, index, scale,
//                isLimited, defaultReloadGroups, instID);
//    }

    /**
     * There may be times when we want to disable saving for a given UAVObject.
     * For example, we may have a GUI used to set up an optional firmware module
     * where the widget used to enable/disable the module is bound to, in this
     * case, the HwSettings UAVObject. When we enable/disable the module and
     * save settings to the flight controller, we don't want to attempt a save
     * of settings for a disabled module as this will appear to cause an error
     * on the upload or save button.<p/>
     * To avoid this, call this method to disable saves on the given object when
     * attempting to save it would result in a NACK.<p/>
     * Remember to call enableSavesForObject to re-enable saves as appropriate.
     *
     * @param object UAVObject to disable saves for
     */
    public void disableSavesForObject(UAVObject object) {
        if (mSmartSave != null) {
            mSmartSave.disableSavesForObject(object);
        }
    }

    /**
     * Util function to get a pointer to the object manager
     *
     * @return pointer to the UAVObjectManager
     */
    private UAVObjectManager getObjectManager() {
        if (BuildConfig.DEBUG && mObjectManager == null) {
            throw new AssertionError("Object manager is null");
        }
        return mObjectManager;
    }

    private void onAutopilotDisconnect() {
        mIsConnected = false;
        enableControls(false);
//        invalidateObjects();
    }

    private void onAutopilotConnect() {
        if (mUAVObjectUtilManager != null)
            mCurrentBoard = mUAVObjectUtilManager.getBoardModel();
        mIsConnected = true;
        for (ObjectToView ov : mObjectsOfInterest) {
            loadWidgetLimits(ov.view, ov.field, ov.index, ov.isLimited,
                    ov.scale);
        }
        setDirty(false);
        enableControls(true);
        refreshWidgetsValues(null);
    }

    /**
     * Method used to populate the views with the initial values
     */
    public void populateViews() {
        boolean dirtyBack = mDirty;
        firePopulateViewsRequested();
        for (ObjectToView ov : mObjectsOfInterest) {
            if (ov.object != null && ov.field != null && ov.view != null) {
                setWidgetFromField(ov.view, ov.field, ov.index, ov.scale,
                        ov.isLimited);

                // Update shadows. We need to do this as usually the shadow
                // views are only updated when the view they shadow is updated.
                // If the view being shadowed already has the value being set,
                // the shadow will not be updated, whether it already has that
                // value or not. This is usually an issue when a fragment is
                // restored.
                for (Shadow curShadow : ov.getShadowsList()) {
                    setWidgetFromField(curShadow.view, ov.field, ov.index, curShadow.scale,
                            curShadow.isLimited);
                }
            }
        }
        setDirty(dirtyBack);
    }

    /**
     * Method used to refresh the widgets contents of the widgets with relation
     * to object field added to the framework pool
     */
    private void refreshWidgetsValues(final UAVObject obj) {
        if (!mAllowViewUpdates)
            return;

        mHandler.post(new Runnable() {

            @Override
            public void run() {
                boolean dirtyBack = mDirty;
                fireRefreshViewsValuesRequested();
                for (ObjectToView ow : mObjectsOfInterest) {
                    if (ow.object != null && ow.field != null && ow.view != null && !ow.isDisabled) {
                        if (ow.object == obj || obj == null)
                            setWidgetFromField(ow.view, ow.field, ow.index, ow.scale,
                                    ow.isLimited);
                    }
                }
                setDirty(dirtyBack);
            }
        });

    }

    /**
     * Method used to update the uavobject fields from widgets with relation to
     * object field added to the framework pool
     */
    private void updateObjectsFromWidgets() {
        fireUpdateObjectsFromViewsRequested();
        for (ObjectToView ow : mObjectsOfInterest) {
            if (ow.object != null && ow.field != null && !ow.isDisabled) {
                setFieldFromWidget(ow.view, ow.field, ow.index, ow.scale);
            }

        }
    }

    /**
     * Method used to handle help button presses.
     */
    private void helpButtonPressed(View view) {
        if (view instanceof Button) {
            String url = mHelpButtonList.get(view);
            if (!url.isEmpty()) {
                Intent showUrl = new Intent(Intent.ACTION_VIEW);
                showUrl.setData(Uri.parse(url));
                mContext.startActivity(showUrl);
            }
        }
    }

    /**
     * Add update and save buttons to the form multiple buttons can be added for
     * the same function
     *
     * @param update pointer to the update button
     * @param save   pointer to the save button
     */
    public void addApplySaveButtons(Button update, Button save) {
        if (mSmartSave == null) {
            mSmartSave = new SmartSave(mUAVObjectUtilManager);
            mSmartSave.addEventListener(new SmartSaveEventListener() {

                @Override
                public void saveSuccessful() {
                    clearDirty();
                }

                @Override
                public void preProcessOperations() {
                    updateObjectsFromWidgets();
                }

                @Override
                public void endOp() {
                    enableObjUpdates();
                    fireAfterSave();
                }

                @Override
                public void beginOp() {
                    fireBeforeSave();
                    disableObjUpdates();
                }
            });
        }
        if (update != null && save != null) {
            mSmartSave.addButtons(save, update);
        } else if (update != null) {
            mSmartSave.addApplyButton(update);
        } else if (save != null) {
            mSmartSave.addSaveButton(save);
        }
        if (!mObjToGuiMapping.isEmpty()) {
            for (UAVObject curObject : mObjToGuiMapping.keySet()) {
                mSmartSave.addObject((UAVDataObject) curObject);
            }
        }

        // Assume that if object manager is not null, we are connected as the
        // object manager
        // is only available after a connection has been made.
        if (mObjectManager != null) {
            enableControls(true);
        } else {
            enableControls(false);
        }
    }

    /**
     * method used the enable or disable the SAVE, UPLOAD and RELOAD buttons
     *
     * @param enable set to true to enable the buttons or false to disable them
     */
    private void enableControls(boolean enable) {
        if (mSmartSave != null) {
            mSmartSave.enableControls(enable);
        }
        for (Button button : mReloadButtonList) {
            button.setEnabled(enable);
        }
    }

    /**
     * Method called when one of the widgets' contents added to the framework
     * changes
     */
    private void widgetsContentsChanged(View view) {
        // Has change handling been disabled for this view?
        Boolean disabled = mDisableChangedListenerMap.get(view);
        if (disabled == null || disabled.equals(Boolean.FALSE)) {
            fireViewContentsChanged(view, Variant.emptyVariant());
            double scale = 1;
            ObjectToView oTw = mShadowsList.get(view);
            if (oTw != null) {
                if (oTw.view.equals(view)) {
                    final Variant variantFromWidget = getVariantFromWidget(view, oTw.scale,
                            oTw.getUnits(), oTw.index);
                    // Given view and view from shadow list are the same instance, just check the limits
                    scale = oTw.scale;
                    if (oTw.index > -1) {
                        // Actual index, check field element value against current limits
                        checkWidgetsLimits(
                                view,
                                oTw.field,
                                oTw.index,
                                oTw.isLimited,
                                variantFromWidget, oTw.scale);
                    } else {
                        // Index indicates this is multi element viewing control (currently only
                        // CurveView). Need to check all elements' values
                        for (int idx = 0; idx < oTw.field.getNumElements(); ++idx) {
                            checkWidgetsLimits(
                                    view,
                                    oTw.field,
                                    idx,
                                    oTw.isLimited,
                                    getVariantFromWidget(view, oTw.scale,
                                            oTw.getUnits(), idx), oTw.scale);
                        }
                    }
                    fireViewContentsChanged(oTw.view, variantFromWidget);
                } else {
                    // Given view and view from shadow list are different, check all the corresponding
                    // shadows' limits against the given view's value
                    for (Shadow sh : oTw.getShadowsList()) {
                        if (sh.view.equals(view)) {
                            scale = sh.scale;
                            if (oTw.index > -1) {
                                // Actual index, check field element value against current limits
                                checkWidgetsLimits(
                                        view,
                                        oTw.field,
                                        oTw.index,
                                        sh.isLimited,
                                        getVariantFromWidget(view, scale,
                                                oTw.getUnits(), oTw.index), scale);
                            } else {
                                // Index indicates this is multi element viewing control (currently only
                                // CurveView). Need to check all elements' values
                                for (int idx = 0; idx < oTw.field.getNumElements(); ++idx) {
                                    checkWidgetsLimits(
                                            view,
                                            oTw.field,
                                            idx,
                                            sh.isLimited,
                                            getVariantFromWidget(view, scale,
                                                    oTw.getUnits(), idx), scale);
                                }
                            }
                        }
                    }
                }
                if (!oTw.view.equals(view)) {
                    // Given view and shadow list view are different, we've already checked the corresponding
                    // shadows' limits, check the shadow list's view limits against the given view's value.
                    disconnectWidgetUpdatesToSlot(oTw.view);
                    Variant variantFromWidget = getVariantFromWidget(view, scale, oTw.getUnits(), oTw.index);
                    checkWidgetsLimits(oTw.view, oTw.field, oTw.index,
                            oTw.isLimited,
                            variantFromWidget,
                            oTw.scale);
                    if (oTw.index > -1) {
                        // Applies to most widgets, only concerned with one element
                        // of field
                        variantFromWidget = getVariantFromWidget(view, scale, oTw.getUnits(), oTw.index);
                        setWidgetFromVariant(oTw.view,
                                variantFromWidget,
                                oTw.scale);
                    } else {
                        // Currently only applies to CurveView which displays all element
                        // values for a field
                        for (int idx = 0; idx < oTw.field.getNumElements(); ++idx) {
                            setWidgetFromVariant(oTw.view,
                                    getVariantFromWidget(view, scale, oTw.getUnits(), idx),
                                    oTw.scale, oTw.getUnits(), idx);
                        }
                    }
                    fireViewContentsChanged(oTw.view, variantFromWidget);
                    connectWidgetUpdatesToSlot(oTw.view);
                }
                for (Shadow sh : oTw.getShadowsList()) {
                    // Finally, check all the shadow views' limits against the given view's value
                    // and update to match
                    if (!sh.view.equals(view)) {
                        disconnectWidgetUpdatesToSlot(sh.view);
                        Variant variantFromWidget = getVariantFromWidget(view, scale,
                                oTw.getUnits(), oTw.index);
                        checkWidgetsLimits(
                                sh.view,
                                oTw.field,
                                oTw.index,
                                sh.isLimited,
                                variantFromWidget, sh.scale);
                        if (oTw.index > -1) {
                            // Applies to most widget, only concerned with one element
                            // of field
                            variantFromWidget = getVariantFromWidget(view, scale,
                                    oTw.getUnits(), oTw.index);
                            setWidgetFromVariant(
                                    sh.view, variantFromWidget
                                    , sh.scale);
                        } else {
                            // Currently only applies to CurveView which displays all element
                            // values for a field
                            for (int idx = 0; idx < oTw.field.getNumElements(); ++idx) {
                                setWidgetFromVariant(
                                        sh.view,
                                        getVariantFromWidget(view, scale,
                                                oTw.getUnits(), idx), sh.scale, oTw.getUnits(), idx);
                            }
                        }
                        fireViewContentsChanged(sh.view, variantFromWidget);
                        connectWidgetUpdatesToSlot(sh.view);
                    }
                }
            }
            if (mSmartSave != null)
                mSmartSave.resetIcons();
            setDirty(true);
        }
    }

    /**
     * Method used clear the activity/fragment's dirty status flag
     */
    private void clearDirty() {
        setDirty(false);
    }

    /**
     * Checks if the activity/fragment is dirty (unsaved changes)
     *
     * @return true if the form has unsaved changes
     */
    public boolean isDirty() {
        return mIsConnected && mDirty;
    }

    /**
     * Sets the activity/fragment's dirty status flag
     *
     * @param value value to set dirty flag
     */
    private void setDirty(boolean value) {
        mDirty = value;
    }

    /**
     * Method used to disable view contents changes when related object field
     * changes.
     */
    public void disableObjUpdates() {
        mAllowViewUpdates = false;
        for (UAVObject curObject : mObjToGuiMapping.keySet()) {
            curObject.removeUAVObjectEventListener(mObjectUpdatedListener);
        }
    }

    /**
     * Method used to enable view contents changes when related object field
     * changes.
     */
    public void enableObjUpdates() {
        mAllowViewUpdates = true;
        for (UAVObject curObject : mObjToGuiMapping.keySet()) {
            curObject.addUAVObjectEventListener(mObjectUpdatedListener);
        }
    }

    /**
     * Method used to disable view contents changes when related object field
     * changes for the given object. This will disable updates for the given
     * object for all mapped views.
     *
     * @param obj object to disable updates for
     */
    public void disableObjUpdates(UAVObject obj) {
        setObjUpdatesEnablment(obj, true);
    }

    /**
     * Method used to enable view contents changes when related object field
     * changes for the given object. This will enable updates for the given
     * object for all mapped views.
     *
     * @param obj object to disable updates for
     */
    public void enableObjUpdates(UAVObject obj) {
        setObjUpdatesEnablment(obj, false);
    }

    /**
     * Adds a new help button
     *
     * @param button pointer to the help button
     * @param url    url to open in the browser when the help button is pressed
     */
    public void addHelpButton(Button button, String url) {
        mHelpButtonList.put(button, url);
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                helpButtonPressed(v);
            }
        });
    }

    /**
     * Call this to apply changes to uav objects
     */
    public void apply() {
        if (mSmartSave != null) {
            mSmartSave.apply();
        }
    }

    /**
     * Call this to save changes to uav objects
     */
    public void save() {
        if (mSmartSave != null) {
            mSmartSave.save();
        }
    }

    /**
     * Adds a new shadow view. Shadow views are views which have a relation
     * to an object already present on the framework pool i.e. already added
     * through addUAVObjectToWidgetRelation This function doesn't have to be
     * used directly, addUAVObjectToWidgetRelation will call it if a previous
     * relation exists.<p/>
     * If a previous relation exists and its view is a TextView and the new
     * view is not, the new view will be swapped with that in the previous
     * relation. If this is not the case but the new view is NumberPicker,
     * the new view will be swapped with that in the previous relation. When
     * a swap occurs, the new view essentially becomes the main relation
     * and the old view becomes the shadow.<p/>
     * In all other cases, the new view will become the shadow.
     *
     * @return returns false if the shadow view relation failed to be added
     * (no previous relation existed)
     */
    private boolean addShadowView(UAVObject object, UAVObjectField field, View view,
                                  int index, double scale, boolean isLimited,
                                  List<Integer> defaultReloadGroups, long instID) {
        for (ObjectToView oTw : mObjectsOfInterest) {
            if (oTw.object == null || oTw.view == null || oTw.field == null)
                continue;
            if (oTw.object.getName().equals(object.getName()) && oTw.field.getName().equals(field.getName())
                    && oTw.index == index && oTw.object.getInstID() == instID) {
                // Got a match for a UAVObject/field combination
                Shadow sh;
                // prefer anything else to TextView
                if (oTw.view instanceof TextView && !(view instanceof TextView)) {
                    // This will make the existing UAVO to TextView association the shadow
                    // and swap the new UAVO/view mapping for the existing one
                    sh = swapShadow(oTw, view, scale, isLimited);
                }
                // prefer NumberPicker to anything else
                else if ((!(oTw.view instanceof NumberPicker)
                        && view instanceof NumberPicker) ||
                        (!(oTw.view instanceof HorizontalNumberPickerView)
                                && view instanceof HorizontalNumberPickerView)) {
                    // This will make the existing UAVO to view association the shadow
                    // and swap the new UAVO/NumberPicker mapping for the existing one
                    sh = swapShadow(oTw, view, scale, isLimited);
                } else {
                    sh = new Shadow();
                    sh.isLimited = isLimited;
                    sh.scale = scale;
                    sh.view = view;
                }
                mShadowsList.put(view, oTw);
                oTw.getShadowsList().add(sh);
                connectWidgetUpdatesToSlot(view);
                if (defaultReloadGroups != null)
                    addWidgetToDefaultReloadGroups(view, defaultReloadGroups);
                loadWidgetLimits(view, oTw.field, oTw.index, isLimited, scale);
                return true;
            }
        }
        return false;
    }

    /**
     * This method will take the given ObjectToView instance and copy its
     * isLimited, scale and view properties to a new shadow. The given
     * ObjectToView instance will then have these properties replaced with
     * the other values passed to this method.
     *
     * @param oTw       ObjectToView instance we are swapping with a new shadow
     * @param view      new View to set on oTw
     * @param scale     new scale to set on oTw
     * @param isLimited new isLimited to set on oTw
     * @return new Shadow instance with old values from oTw
     */
    private Shadow swapShadow(ObjectToView oTw, View view, double scale, boolean isLimited) {
        Shadow sh;
        sh = new Shadow();
        sh.isLimited = oTw.isLimited;
        sh.scale = oTw.scale;
        sh.view = oTw.view;
        oTw.isLimited = isLimited;
        oTw.scale = scale;
        oTw.view = view;
        return sh;
    }

    /**
     * Adds a view to a list of default/reload groups default/reload groups are
     * groups of views to be set with default or reloaded (values from
     * persistent memory) when a defined button is pressed
     *
     * @param view   view to be added to the groups
     * @param groups list of the groups on which to add the widget
     */
    private void addWidgetToDefaultReloadGroups(View view, List<Integer> groups) {
        for (ObjectToView oTw : mObjectsOfInterest) {
            boolean addOTW = false;
            if (oTw.view.equals(view)) {
                addOTW = true;
            } else {
                for (Shadow sh : oTw.getShadowsList()) {
                    if (sh.view.equals(view)) {
                        addOTW = true;
                    }
                }
            }
            if (addOTW) {
                for (Integer i : groups) {
                    List<ObjectToView> currentGroup = mDefaultReloadGroups.get(i);
                    if (currentGroup != null) {
                        currentGroup.add(oTw);
                    } else {
                        mDefaultReloadGroups.put(i,
                                new ArrayList<ObjectToView>());
                        mDefaultReloadGroups.get(i).add(oTw);
                    }
                }
            }
        }
    }

//    /**
//     * Called when an uav object is updated
//     *
//     * @param obj
//     *            pointer to the object which has just been updated
//     */
//    private void objectUpdated(UAVObject obj) {
//        _objectUpdates.put(obj, Boolean.TRUE);
//    }

    /**
     * Checks if all objects added to the pool have already been updated
     *
     * @return true if all objects added to the pool have already been updated
     */
//    public boolean allObjectsUpdated() {
//        if (DEBUG)
//            Log.d(TAG, "ConfigTaskWidge:allObjectsUpdated called");
//        boolean ret = true;
//        for (UAVObject obj : _objectUpdates.keySet()) {
//            ret = ret & _objectUpdates.get(obj);
//        }
//        if (DEBUG)
//            Log.d(TAG, "Returned:" + ret);
//        return ret;
//    }

    /**
     * Adds a button to a default group
     *
     * @param button      pointer to the default button
     * @param buttonGroup number of the group
     */
    public void addDefaultButton(Button button, int buttonGroup) {
        button.setTag(R.id.button_group, buttonGroup);
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                defaultButtonClicked(v);
            }
        });
    }

//    /**
//     * Invalidates all the uav objects "is updated" flag
//     */
//    public void invalidateObjects() {
//        for (UAVObject obj : _objectUpdates.keySet()) {
//            _objectUpdates.put(obj, Boolean.FALSE);
//        }
//    }

    /**
     * Adds a button to a reload group
     *
     * @param button      pointer to the reload button
     * @param buttonGroup number of the group
     */
    public void addReloadButton(Button button, int buttonGroup) {
        button.setTag(R.id.button_group, buttonGroup);
        mReloadButtonList.add(button);
        button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                reloadButtonClicked(v);
            }
        });
    }

    public void addEventListener(UAVObjectToGuiUtilsEventListener listener) {
        mUAVObjectToGuiUtilsEventListeners.add(listener);
    }

    public void removeEventListener(UAVObjectToGuiUtilsEventListener listener) {
        mUAVObjectToGuiUtilsEventListeners.remove(listener);
    }

    private void addObjectToMapping(UAVObject object, ObjectToView ov) {
        List<ObjectToView> viewMappings = mObjToGuiMapping.get(object);

        if (viewMappings == null) {
            viewMappings = new ArrayList<>();
            mObjToGuiMapping.put(object, viewMappings);
        }

        viewMappings.add(ov);
    }

    // TODO: Implement auto load
    // /**
    // * Auto loads views based on the Dynamic property named "objrelation"
    // * Check the wiki for more information
    // */
    // public void autoLoadWidgets()
    // {
    // Button saveButtonWidget=null;
    // Button applyButtonWidget=null;
    // for(View widget: _rootView)
    // {
    // QVariant info=widget.property("objrelation");
    // if(info.isValid())
    // {
    // uiRelationAutomation uiRelation;
    // uiRelation.buttonType=none;
    // uiRelation.scale=1;
    // uiRelation.element=QString();
    // uiRelation.haslimits=false;
    // foreach(QString str, info.toStringList())
    // {
    // QString prop=str.split(":").at(0);
    // QString value=str.split(":").at(1);
    // if(prop== "objname")
    // uiRelation.objname=value;
    // else if(prop== "fieldname")
    // uiRelation.fieldname=value;
    // else if(prop=="element")
    // uiRelation.element=value;
    // else if(prop== "scale")
    // {
    // if(value=="null")
    // uiRelation.scale=1;
    // else
    // uiRelation.scale=value.toDouble();
    // }
    // else if(prop== "haslimits")
    // {
    // if(value=="yes")
    // uiRelation.haslimits=true;
    // else
    // uiRelation.haslimits=false;
    // }
    // else if(prop== "button")
    // {
    // if(value=="save")
    // uiRelation.buttonType=save_button;
    // else if(value=="apply")
    // uiRelation.buttonType=apply_button;
    // else if(value=="reload")
    // uiRelation.buttonType=reload_button;
    // else if(value=="default")
    // uiRelation.buttonType=default_button;
    // else if(value=="help")
    // uiRelation.buttonType=help_button;
    // }
    // else if(prop== "buttongroup")
    // {
    // foreach(QString s,value.split(","))
    // {
    // uiRelation.buttonGroup.append(s.toInt());
    // }
    // }
    // else if(prop=="url")
    // uiRelation.url=str.mid(str.indexOf(":")+1);
    // }
    // if(!uiRelation.buttonType==none)
    // {
    // QPushButton * button=NULL;
    // switch(uiRelation.buttonType)
    // {
    // case save_button:
    // saveButtonWidget=qobject_cast<QPushButton *>(widget);
    // if(saveButtonWidget)
    // addApplySaveButtons(NULL,saveButtonWidget);
    // break;
    // case apply_button:
    // applyButtonWidget=qobject_cast<QPushButton *>(widget);
    // if(applyButtonWidget)
    // addApplySaveButtons(applyButtonWidget,NULL);
    // break;
    // case default_button:
    // button=qobject_cast<QPushButton *>(widget);
    // if(button)
    // addDefaultButton(button,uiRelation.buttonGroup.at(0));
    // break;
    // case reload_button:
    // button=qobject_cast<QPushButton *>(widget);
    // if(button)
    // addReloadButton(button,uiRelation.buttonGroup.at(0));
    // break;
    // case help_button:
    // button=qobject_cast<QPushButton *>(widget);
    // if(button)
    // addHelpButton(button,uiRelation.url);
    // break;
    //
    // default:
    // break;
    // }
    // }
    // else
    // {
    // QWidget * wid=qobject_cast<QWidget *>(widget);
    // if(wid)
    // addUAVObjectToWidgetRelation(uiRelation.objname,uiRelation.fieldname,wid,uiRelation.element,uiRelation.scale,uiRelation.haslimits,&uiRelation.buttonGroup);
    // }
    // }
    // }
    // refreshWidgetsValues();
    // forceShadowUpdates();
    // foreach(objectToWidget * ow,objOfInterest)
    // {
    // if(ow.widget)
    // qDebug()<<"Master:"<<ow.widget.objectName();
    // foreach(shadow * sh,ow.shadowsList)
    // {
    // if(sh.widget)
    // qDebug()<<"Child"<<sh.widget.objectName();
    // }
    // }
    // }

    private void setObjUpdatesEnablment(UAVObject obj, boolean enabled) {
        List<ObjectToView> mappings = mObjToGuiMapping.get(obj);

        if (mappings != null) {
            for (ObjectToView curMapping : mappings) {
                curMapping.isDisabled = enabled;
            }
        }
    }

    private void setupConnectionBroadcastListener() {
        IntentFilter filter = new IntentFilter();
        filter.addCategory(OPTelemetryService.INTENT_CATEGORY_GCS);
        filter.addAction(OPTelemetryService.INTENT_ACTION_CONNECTED);
        filter.addAction(OPTelemetryService.INTENT_ACTION_DISCONNECTED);
        ConnectionBroadcastReceiver connectionBroadcastReceiver = new ConnectionBroadcastReceiver();
        mLocalBroadcastManager.registerReceiver(connectionBroadcastReceiver,
                filter);
    }

    private void fireViewContentsChanged(View view, Variant value) {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.viewContentsChanged(view, value);
        }
    }

    private void firePopulateViewsRequested() {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.populateViewsRequested();
        }
    }

    private void fireRefreshViewsValuesRequested() {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.refreshViewsValuesRequested();
        }
    }

    private void fireUpdateObjectsFromViewsRequested() {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.updateObjectsFromViewsRequested();
        }
    }

    private void fireAutoPilotConnected() {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.autoPilotConnected();
        }
    }

    private void fireAutoPilotDisconnected() {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.autoPilotDisconnected();
        }
    }

    private void fireDefaultRequested(int group) {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.defaultRequested(group);
        }
    }

    private void fireBeforeSave() {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.beforeSave();
        }
    }

    private void fireAfterSave() {
        for (UAVObjectToGuiUtilsEventListener curListener : mUAVObjectToGuiUtilsEventListeners) {
            curListener.afterSave();
        }
    }

    /**
     * Called when a default button is clicked
     */
    private void defaultButtonClicked(View view) {
        final Object tag = view.getTag(R.id.button_group);
        if (tag != null) {
            int group = (Integer) tag;
            fireDefaultRequested(group);
            List<ObjectToView> list = mDefaultReloadGroups.get(group);
            for (ObjectToView oTw : list) {
                if (oTw.object == null || oTw.field == null)
                    continue;
                UAVDataObject dataObject = ((UAVDataObject) oTw.object).getDefaultInstance();
                setWidgetFromField(oTw.view, dataObject.getField(oTw.field.getName()),
                        oTw.index, oTw.scale, oTw.isLimited);
            }
        }
    }

    /**
     * Called when a reload button is clicked
     */
    private void reloadButtonClicked(final View view) {
        mReloadExecutor.submit(new Runnable() {

            @Override
            public void run() {
                final Object tag = view.getTag(R.id.button_group);
                if (tag != null) {
                    int group = (Integer) tag;
                    List<ObjectToView> list = mDefaultReloadGroups.get(group);
                    if (list == null) {
                        return;
                    }
                    final UAVObject objper = getObjectManager().getObject(
                            "ObjectPersistence");

                    List<Temphelper> temp = new ArrayList<>();
                    for (ObjectToView oTw : list) {
                        if (oTw.object != null) {
                            Temphelper value = new Temphelper();
                            value.objid = oTw.object.getObjID();
                            value.objinstid = oTw.object.getInstID();
                            if (temp.contains(value))
                                continue;
                            else
                                temp.add(value);
                            objper.getField(ObjectPersistence.FIELD_OBJECTID).setValue(
                                    new Variant(oTw.object.getObjID()));
                            objper.getField(ObjectPersistence.FIELD_INSTANCEID).setValue(
                                    new Variant(oTw.object.getInstID()));
                            objper.getField(ObjectPersistence.FIELD_OPERATION).setValue(new Variant(ObjectPersistence.OPERATION_LOAD));
                            objper.getField(ObjectPersistence.FIELD_SELECTION).setValue(new Variant(ObjectPersistence.SELECTION_SINGLEOBJECT));

                            // Reset latch
                            mTimeoutLatch = new CountDownLatch(1);

                            // Run reload request
                            mReloadExecutor.submit(new Runnable() {

                                @Override
                                public void run() {
                                    objper.updated();

                                    // Signal completion via latch
                                    mTimeoutLatch.countDown();
                                }
                            });

                            // Wait for our latch to countdown to zero (initialized
                            // to one)
                            try {
                                if (mTimeoutLatch.await(500, TimeUnit.MILLISECONDS)) {
                                    if (DEBUG)
                                        Log.d(TAG, "Reloading did not timeout");
                                    oTw.object.requestUpdate();
                                    if (oTw.view != null) {
                                        setWidgetFromField(oTw.view, oTw.field,
                                                oTw.index, oTw.scale, oTw.isLimited);
                                    }
                                } else if (DEBUG)
                                    Log.d(TAG, "Reloading TIMEOUT");
                            } catch (InterruptedException e) {
                                Log.d(TAG, "Reloading thread interrupted ");
                            }
                        }
                    }
                }

            }
        });
    }

    /**
     * Connects view "contents changed" signals to a slot
     */
    private void connectWidgetUpdatesToSlot(final View view) {
        if (view == null) {
            return;
        }
        Boolean disabled = mDisableChangedListenerMap.get(view);

        if (disabled != null) {
            // Re-enable disabled event handler
            mDisableChangedListenerMap.put(view, Boolean.FALSE);
        } else {
            // Not been previously disabled: must be first time to add listener for this view
            if (view instanceof Spinner) {
                Spinner spinner = (Spinner) view;
                spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view,
                                               int position, long id) {
                        widgetsContentsChanged(view);
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            } else if (view instanceof SeekBar) {
                SeekBar seekBar = (SeekBar) view;
                seekBar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                    }

                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress,
                                                  boolean fromUser) {
                        widgetsContentsChanged(seekBar);
                    }
                });
            } else if (view instanceof NumberPicker) {
                NumberPicker numberPicker = (NumberPicker) view;
                numberPicker.setOnValueChangedListener(new OnValueChangeListener() {

                    @Override
                    public void onValueChange(NumberPicker picker, int oldVal,
                                              int newVal) {
                        widgetsContentsChanged(picker);
                    }
                });
            } else if (view instanceof HorizontalNumberPickerView) {
                HorizontalNumberPickerView numberPicker = (HorizontalNumberPickerView) view;
                numberPicker.setOnValueChangedListener(new HorizontalNumberPickerView.OnValueChangeListener() {

                    @Override
                    public void onValueChange(HorizontalNumberPickerView picker, double oldVal,
                                              double newVal) {
                        widgetsContentsChanged(picker);
                    }
                });
            } else if (view instanceof CheckBox) {
                CheckBox checkBox = (CheckBox) view;
                checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView,
                                                 boolean isChecked) {
                        widgetsContentsChanged(buttonView);
                    }
                });
            } else if (view instanceof Button) {
                Button button = (Button) view;
                button.setOnClickListener(new OnClickListener() {

                    @Override
                    public void onClick(View v) {
                        widgetsContentsChanged(v);
                    }
                });
            } else if (view instanceof TextView) {
                TextView editText = (TextView) view;
                editText.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start,
                                              int before, int count) {
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start,
                                                  int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        widgetsContentsChanged(view);
                    }
                });
            } else if (view instanceof CurveView) {
                ((CurveView) view).setOnValueChangedListener(new CurveView.OnValueChangedListener() {
                    @Override
                    public void onValueChanged(CurveView sender, CurveData data) {
                        widgetsContentsChanged(sender);
                    }
                });
            } else {
                if (DEBUG)
                    Log.d(TAG, "widget to uavobject relation not implemented "
                            + view.getClass().getSimpleName());
            }
        }
    }

    /**
     * Disable value changed listener for widget by setting it to an empty
     * listener implementation.
     */
    private void disconnectWidgetUpdatesToSlot(View view) {
        if (view == null) {
            return;
        }

        // Flag view to disable event listener
        mDisableChangedListenerMap.put(view, Boolean.TRUE);
    }

    /**
     * Sets a widget value from an UAVObject field
     *
     * @param widget pointer for the widget to set
     * @param field  pointer to the UAVObject field to use
     * @param index  index of the element to use
     * @param scale  scale to be used on the assignment
     */
    private void setFieldFromWidget(View widget, UAVObjectField field,
                                    int index, double scale) {
        if (widget != null && field != null) {
            boolean succeeded = true;
            if (index > -1) {
                // We are setting a single field value
                Variant value = getVariantFromWidget(widget, scale, field.getUnits(), index);
                if (DEBUG) Log.d(TAG, field.getName() + ": " + value);
                if (value != null && !value.isEmpty()) {
                    field.setValue(value, index);
                    if (DEBUG) Log.d(TAG, field.getName() + ": " + field.getValue(index));
                } else {
                    succeeded = false;
                }
            } else {
                // We are setting all the values in the field
                for (int idx = 0; idx < field.getNumElements(); ++idx) {
                    Variant value = getVariantFromWidget(widget, scale, field.getUnits(), idx);
                    if (DEBUG) Log.d(TAG, field.getName() + ": " + value);
                    if (value != null && !value.isEmpty()) {
                        field.setValue(value, idx);
                        if (DEBUG) Log.d(TAG, field.getName() + ": " + field.getValue(idx));
                    } else {
                        succeeded = false;
                    }
                }
            }
            if (DEBUG) {
                // Nesting this if statement to stop pointless boolean expression warning
                // due to debug being either always true or always false.
                if (!succeeded) {
                    Log.d(TAG, "widget to uavobject relation not implemented "
                            + widget.getClass().getSimpleName());
                }
            }
        }
    }

    /**
     * Gets a variant from a widget
     *
     * @param view  pointer to the widget from where to get the value
     * @param scale scale to be used on the assignment
     * @param units used to determine format of value in text view
     * @param index used with curve view to determine which point's value
     *              we are getting
     * @return returns the value of the widget times the scale
     */
    private Variant getVariantFromWidget(View view, double scale, String units, int index) {
        Variant retVal = Variant.emptyVariant();
        if (view instanceof Spinner) {
            retVal = new Variant(((Spinner) view).getSelectedItem());
        } else if (view instanceof NumberPicker) {
            retVal = new Variant(((NumberPicker) view).getValue() * scale);
        } else if (view instanceof HorizontalNumberPickerView) {
            retVal = new Variant(((HorizontalNumberPickerView) view).getValue() * scale);
        } else if (view instanceof SeekBar) {
            // Look to see if we have a min value tag set and use it to adjust
            // the value as appropriate
            Integer seekMinValue = (Integer) view.getTag(R.id.seekbar_min_value);
            int minValue = seekMinValue != null ? seekMinValue : 0;
            retVal = new Variant((((SeekBar) view).getProgress() + minValue) * scale);
        } else if (view instanceof Checkable) {
            String[] trueFalse = mBooleanWidgetValues.get(view);
            retVal = new Variant(((Checkable) view).isChecked() ? trueFalse[0]
                    : trueFalse[1]);
        } else if (view instanceof TextView) {
            final CharSequence text = ((TextView) view).getText();
            if (text != null) {
                String value = text.toString();
                if ((scale == 0) || (scale == 1)) {
                    if (units.equals("hex")) {
                        retVal = new Variant(Long.valueOf(value, 16));
                    } else {
                        retVal = new Variant(value);
                    }
                } else {
                    retVal = new Variant(Double.valueOf(value) * scale);
                }
            }
        } else if (view instanceof CurveView) {
            CurveView curveView = (CurveView) view;

            // Note we currently only support binding to a single curve although
            // the curve view can show multiple curves.
            CurveData curveData = curveView.getCurveData(0);
            retVal = new Variant(curveData.getPoint(index));
        }

        return retVal;
    }

    /**
     * Sets a widget from a variant
     *
     * @param view  pointer for the widget to set
     * @param value value to be used on the assignment
     * @param scale scale to be used on the assignment
     * @param units the units for the value
     * @param index used with curve view to determine which point's value
     *              we are setting
     * @return returns true if the assignment was successful
     */
    private boolean setWidgetFromVariant(View view, Variant value,
                                         double scale, String units, int index) {
        if (value != null) {
            if (view instanceof Spinner) {
                Spinner spinner = (Spinner) view;
                @SuppressWarnings("unchecked") ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) spinner
                        .getAdapter();
                spinner.setSelection(arrayAdapter.getPosition(value.toString()));
                return true;
            } else if (view instanceof Checkable) {
                // Note that we need to check for Checkable before TextView
                // as most Checkable widgets derive from TextView...
                boolean bvalue = value.toString().equals(mBooleanWidgetValues.get(view)[0]);
                Checkable checkBox = (Checkable) view;
                checkBox.setChecked(bvalue);
                return true;
            } else if (view instanceof NumberPicker) {
                NumberPicker numberPicker = (NumberPicker) view;
                Long longValue = value.toLong();
                numberPicker.setValue((int) Math.ceil(longValue
                        / scale));
                return true;
            } else if (view instanceof HorizontalNumberPickerView) {
                HorizontalNumberPickerView numberPicker = (HorizontalNumberPickerView) view;
                numberPicker.setValue(value.toDouble() / scale);
                return true;
            } else if (view instanceof SeekBar) {
                SeekBar seekBar = (SeekBar) view;
                // Look to see if we have a min value tag set and use it to adjust
                // the value as appropriate
                Integer seekMinValue = (Integer) view.getTag(R.id.seekbar_min_value);
                int minValue = seekMinValue != null ? seekMinValue : 0;
                seekBar.setProgress((int) Math.round((value.toDouble() / scale)) - minValue);
                return true;
            } else if (view instanceof TextView) {
                TextView editText = (TextView) view;
                if ((scale == 0) || (scale == 1)) {
                    if (units.equals("hex")) {
                        editText.setText(Long.toHexString(value.toLong())
                                .toUpperCase());
                    } else {
                        editText.setText(value.toString());
                    }
                } else {
                    editText.setText(String.format(Locale.getDefault(), "%f", (value.toDouble() / scale)));
                }
                return true;
            } else if (view instanceof CurveView) {
                CurveView curveView = (CurveView) view;

                // Note we currently only support binding to a single curve although
                // the curve view can show multiple curves.
                CurveData curveData = curveView.getCurveData(0);
                curveData.setPoint(index, value.toFloat(), true);
            } else
                return false;
        }
        if (DEBUG) Log.d(TAG, "Value Variant is null");
        return false;
    }

    /**
     * Sets a widget from a variant
     *
     * @param view  pointer for the widget to set
     * @param value value to be used on the assignment
     * @param scale scale to be used on the assignment
     */
    private void setWidgetFromVariant(View view, Variant value, double scale) {
        setWidgetFromVariant(view, value, scale, "", 0);
    }

    /**
     * Sets a widget from a UAVObject field
     *
     * @param widget    pointer to the widget to set
     * @param field     pointer to the field from where to get the value from
     * @param index     index of the element to use, set to -1 to use all elements
     * @param scale     scale to be used on the assignment
     * @param hasLimits set to true if you want to limit the values (check wiki)
     */
    private void setWidgetFromField(View widget, UAVObjectField field,
                                    int index, double scale, boolean hasLimits) {
        if (widget != null && field != null) {
            if (widget instanceof Spinner) {
                if (((Spinner) widget).getCount() == 0)
                    loadWidgetLimits(widget, field, index, hasLimits, scale);
            }
            if (index > -1) {
                Variant var = field.getValue(index);
                checkWidgetsLimits(widget, field, index, hasLimits, var, scale);
                if (!setWidgetFromVariant(widget, var, scale, field.getUnits(), index)) {
                    if (DEBUG)
                        Log.d(TAG, "widget to uavobject relation not implemented "
                                + widget.getClass().getSimpleName());
                }
            } else {
                for (int idx = 0; idx < field.getNumElements(); ++idx) {
                    Variant var = field.getValue(idx);
                    checkWidgetsLimits(widget, field, index, hasLimits, var, scale);
                    setWidgetFromVariant(widget, var, scale, field.getUnits(), idx);
                }
            }
        }
    }

    private void checkWidgetsLimits(View view, UAVObjectField field,
                                    int index, boolean hasLimits, Variant value, double scale) {
        if (!hasLimits)
            return;
        if (!field.isWithinLimits(value, index, mCurrentBoard)) {
            // TODO: work out how to style widget to indicate invalid values
            // if(!widget.property("styleBackup").isValid())
            // widget.setProperty("styleBackup",widget.styleSheet());
            // widget.setStyleSheet(outOfLimitsStyle);
            view.setTag(R.id.was_over_limits, true);
            if (view instanceof Spinner) {
                Spinner spinner = (Spinner) view;
                @SuppressWarnings("unchecked") ArrayAdapter<String> arrayAdapter = (ArrayAdapter<String>) spinner
                        .getAdapter();
                if (arrayAdapter.getPosition(value.toString()) < 0) {
                    arrayAdapter.add(value.toString());
                }
            } else if (view instanceof NumberPicker) {
                NumberPicker picker = (NumberPicker) view;
                int roundedValue = (int) Math.round(value.toDouble() / scale);
                if (roundedValue > picker.getMaxValue()) {
                    picker.setMaxValue(roundedValue);
                } else if (roundedValue < picker.getMinValue()) {
                    picker.setMinValue(roundedValue);
                }
            } else if (view instanceof HorizontalNumberPickerView) {
                HorizontalNumberPickerView picker = (HorizontalNumberPickerView) view;
                double scaledValue = value.toDouble() / scale;
                if (scaledValue > picker.getMaxValue()) {
                    picker.setMaxValue(scaledValue);
                } else if (scaledValue < picker.getMinValue()) {
                    picker.setMinValue(scaledValue);
                }
            } else if (view instanceof SeekBar) {
                SeekBar seekBar = (SeekBar) view;
                Integer seekBarMinValue = (Integer) seekBar.getTag(R.id.seekbar_min_value);
                int roundedValue = (int) Math.round(value.toDouble() / scale);
                if (roundedValue > seekBar.getMax()
                        + (seekBarMinValue != null ? seekBarMinValue
                        : 0)) {
                    seekBar.setMax(roundedValue);
                } else if (roundedValue < (seekBarMinValue != null ? seekBarMinValue : 0)) {
                    seekBar.setTag(
                            R.id.seekbar_min_value,
                            (int) Math.round(value.toDouble()
                                    / scale));
                }
            }

        } else if (view.getTag(R.id.was_over_limits) != null) {
            final Object tag = view.getTag(R.id.was_over_limits);
            if (tag != null && (Boolean) tag) {
                view.setTag(R.id.was_over_limits, false);
                // TODO: work out how to style widget to indicate invalid values
                // if(widget.property("styleBackup").isValid())
                // {
                // QString style=widget.property("styleBackup").toString();
                // widget.setStyleSheet(style);
                // }
                loadWidgetLimits(view, field, index, hasLimits, scale);
            }
        }
    }

    private void loadWidgetLimits(View widget, UAVObjectField field, int index,
                                  boolean hasLimits, double scale) {
        if (widget == null || field == null) {
            return;
        }
        if (widget instanceof Spinner) {
            Spinner spinner = (Spinner) widget;
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(mContext, android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(arrayAdapter);
            List<String> option = field.getOptions();
            if (hasLimits) {
                for (String str : option) {
                    if (field.isWithinLimits(new Variant(str), index,
                            mCurrentBoard))
                        arrayAdapter.add(str);
                }
            } else
                arrayAdapter.addAll(option);
        }
        if (hasLimits) {
            if (widget instanceof NumberPicker) {
                NumberPicker picker = (NumberPicker) widget;
                if (field.getMaxLimit(index, mCurrentBoard).isValid()) {
                    picker.setMaxValue((int) Math.round(field.getMaxLimit(index,
                            mCurrentBoard).toDouble()
                            / scale));
                }
                if (field.getMinLimit(index, mCurrentBoard).isValid()) {
                    picker.setMinValue((int) Math.round(field.getMinLimit(index,
                            mCurrentBoard).toDouble()
                            / scale));
                }
            } else if (widget instanceof HorizontalNumberPickerView) {
                HorizontalNumberPickerView picker = (HorizontalNumberPickerView) widget;
                if (field.getMaxLimit(index, mCurrentBoard).isValid()) {
                    picker.setMaxValue(field.getMaxLimit(index,
                            mCurrentBoard).toDouble()
                            / scale);
                }
                if (field.getMinLimit(index, mCurrentBoard).isValid()) {
                    picker.setMinValue(field.getMinLimit(index,
                            mCurrentBoard).toDouble()
                            / scale);
                }
            } else if (widget instanceof SeekBar) {
                int minValue = 0;
                int maxValue = 0;
                SeekBar seekBar = (SeekBar) widget;
                if (field.getMaxLimit(index, mCurrentBoard).isValid()) {
                    maxValue = (int) Math.round(field.getMaxLimit(index,
                            mCurrentBoard).toDouble()
                            / scale);
                }

                if (field.getMinLimit(index, mCurrentBoard).isValid()) {
                    minValue = (int) (field.getMinLimit(index, mCurrentBoard)
                            .toDouble() / scale);
                }

                if (minValue != 0) {
                    // We have a non-zero minimum value but SeekBar doesn't support
                    // this. We need to save the minimum value in the control's tag
                    // and use it to modify the current value to be the corrected
                    // value. We also need to set the maximum on the SeekBar to the
                    // difference between min and max.
                    seekBar.setMax(maxValue - minValue);
                    seekBar.setTag(R.id.seekbar_min_value,
                            minValue);
                } else {
                    seekBar.setMax(maxValue);
                }
            }
        }
    }

    class Shadow {
        View view;
        double scale;
        boolean isLimited;
    }

    class ObjectToView {
        UAVObject object;
        UAVObjectField field;
        View view;
        int index;
        double scale;
        boolean isLimited;
        boolean isDisabled;
        private List<Shadow> shadowsList;

        String getUnits() {
            if (field != null) {
                return field.getUnits();
            }
            return "";
        }

        public List<Shadow> getShadowsList() {
            if (shadowsList == null) {
                shadowsList = new ArrayList<>();
            }

            return shadowsList;
        }
    }

    class Temphelper {
        long objid;
        long objinstid;

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + getOuterType().hashCode();
            result = prime * result + (int) (objid ^ (objid >>> 32));
            result = prime * result + (int) (objinstid ^ (objinstid >>> 32));
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            Temphelper other = (Temphelper) obj;
            return getOuterType().equals(other.getOuterType()) &&
                    objid == other.objid &&
                    objinstid == other.objinstid;
        }

        private UAVObjectToGuiUtils getOuterType() {
            return UAVObjectToGuiUtils.this;
        }
    }

    private class GuiRelationAutomation {
        String objname;
        String fieldname;
        String element;
        String url;
        double scale;
        boolean haslimits;
        List<Integer> buttonGroup;
    }

    @SuppressLint("Registered")
    private final class ConnectionBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null) {
                final String action = intent.getAction();
                if (action != null) {
                    if (action.compareTo(
                            OPTelemetryService.INTENT_ACTION_CONNECTED) == 0) {
                        onAutopilotConnect();
                        fireAutoPilotConnected();
                    } else if (action.compareTo(
                            OPTelemetryService.INTENT_ACTION_DISCONNECTED) == 0) {
                        onAutopilotDisconnect();
                        fireAutoPilotDisconnected();
                    }
                }
            }
        }
    }

    private final class ObjectUpdatedListener extends UAVObjectEventAdapter {

        @Override
        public void objectUpdated(UAVObject obj) {
            refreshWidgetsValues(obj);
        }

    }
}
