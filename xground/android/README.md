# OpenPilot AndroidGCS

## Setup

* If using AndroidStudio (preferred) you can setup everything using this tool.
* Once Android Studio is installed launch 'android sdk'

## Building
* create a gradle.properties in ~/.gradle


    RELEASE_STORE_FILE={location of keystore}

    RELEASE_STORE_PASSWORD={store password}

    RELEASE_KEY_ALIAS=android

    RELEASE_KEY_PASSWORD={key password}

    #required to copy uavo's
    OPENPILOT_BUILD_DIR={OpenPilot Build directory}

    # for local builds, whatever value you want, will be overriden in Bamboo
    BUILD_NUMBER=1

OPENPILOT_BUILD_DIR will be used to copy uavos and calculate uavo hash.

## Google Maps
Debug key for Maps, add to AndroidManifest.xml

 AIzaSyC-A6zrED9yMOxuYsgo3xoJsrgntGsKvWI

## Unit Testing

* http://developer.android.com/tools/testing/testing_android.html
