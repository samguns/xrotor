/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core hardware abstraction layer
 * @{
 * @addtogroup   PIOS_SOFT_UART Software UART Functions
 * @brief STM32F4xx Hardware dependent software UART functionality
 * @{
 *
 * @file       pios_soft_uart.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @brief      Software UART commands
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */


#include "pios.h"

#if defined(PIOS_INCLUDE_SOFT_UART)

#include <pios_soft_uart_priv.h>

#define PIOS_SOFT_UART_FRAME_SIZE 12
#define PIOS_SOFT_UART_FIRST_BYTE 1
#define PIOS_SOFT_UART_BIT_MASK   0x01


enum pios_soft_uart_dev_magic {
    PIOS_SOFT_UART_DEV_MAGIC = 0x57414E47,
};

struct pios_soft_uart_dev {
    enum pios_soft_uart_dev_magic   magic;
    const struct pios_soft_uart_cfg *cfg;
    const struct pios_exti_cfg      *tx_pin;
    const struct pios_exti_cfg      *rx_pin;

    uint32_t baudrate;
    uint8_t  txBuffer[PIOS_SOFT_UART_BUFFER_SIZE];      /* UART Tx transfer Buffer */
    uint16_t txXferSize; /* UART Tx Transfer size */
    uint16_t txXferCount; /* UART Tx Transfer Counter */

    uint8_t  rxBuffer[PIOS_SOFT_UART_BUFFER_SIZE];
    uint16_t rxXferSize;
    uint16_t rxXferCount; /* UART Rx Transfer Counter */

    bool     isOneWireSerial;
    bool     isJobPending;
    bool     isInverted;
    uint32_t errorCode;
    SOFT_UART_StateTypeDef state;

    pios_com_callback rx_in_cb;
    uint32_t rx_in_context;
    pios_com_callback tx_out_cb;
    uint32_t tx_out_context;
};

static struct pios_soft_uart_dev pios_soft_uart_device;

static uint32_t pFirstFrame_Tx[PIOS_SOFT_UART_FRAME_SIZE];
static uint32_t pSecondFrame_Tx[PIOS_SOFT_UART_FRAME_SIZE];
static uint32_t pFirstFrame_Rx[PIOS_SOFT_UART_FRAME_SIZE];
static uint32_t pSecondFrame_Rx[PIOS_SOFT_UART_FRAME_SIZE];

static bool PIOS_SOFT_UART_validateDev(struct pios_soft_uart_dev *dev);
static void PIOS_SOFT_UART_setupTimer(const struct pios_soft_uart_cfg *cfg,
                                      uint32_t baud);
static void PIOS_SOFT_UART_setupTx(const struct pios_soft_uart_cfg *cfg,
                                   const struct pios_exti_cfg *tx_pin_cfg);
static void PIOS_SOFT_UART_setupRx(const struct pios_soft_uart_cfg *cfg,
                                   const struct pios_exti_cfg *rx_pin_cfg);

static void PIOS_SOFT_UART_txDMA(struct pios_soft_uart_dev *dev);
static void PIOS_SOFT_UART_rxDMA(struct pios_soft_uart_dev *dev);
static void PIOS_SOFT_UART_formatTxFrame(struct pios_soft_uart_dev *dev,
                                         uint8_t data, uint32_t *pFrame_Tx);
static uint8_t PIOS_SOFT_UART_formatRxFrame(struct pios_soft_uart_dev *dev,
                                            uint32_t *pFrame_Rx, uint8_t data);
static void PIOS_SOFT_UART_formatInvertedTxFrame(struct pios_soft_uart_dev *dev,
                                                 uint8_t data, uint32_t *pFrame_Tx);
static uint8_t PIOS_SOFT_UART_formatInvertedRxFrame(struct pios_soft_uart_dev *dev,
                                                    uint32_t *pFrame_Rx, uint8_t data);
static void PIOS_SOFT_UART_sendFrame(struct pios_soft_uart_dev *dev);
static void PIOS_SOFT_UART_receiveFrame(struct pios_soft_uart_dev *dev);
static void PIOS_SOFT_UART_txDMATransferComplete(void);
static void PIOS_SOFT_UART_rxDMATransferComplete(void);

static void PIOS_SOFT_UART_changeBaud(uint32_t dev_id, uint32_t baud);
static void PIOS_SOFT_UART_registerRxCallback(uint32_t dev_id, pios_com_callback rx_in_cb, uint32_t context);
static void PIOS_SOFT_UART_registerTxCallback(uint32_t dev_id, pios_com_callback tx_out_cb, uint32_t context);
static void PIOS_SOFT_UART_txStart(uint32_t dev_id, uint16_t tx_bytes_avail);
static void PIOS_SOFT_UART_rxStartWithSize(uint32_t dev_id, uint16_t rx_bytes);
static void PIOS_SOFT_UART_setTransceiverPins(uint32_t dev_id,
                                              uint32_t tx_line_context, uint32_t rx_line_context);
static void PIOS_SOFT_UART_setInverted(uint32_t dev_id, bool isInverted);
const struct pios_com_driver pios_soft_uart_com_driver = {
    .set_baud     = PIOS_SOFT_UART_changeBaud,
    .tx_start     = PIOS_SOFT_UART_txStart,
    .bind_tx_cb   = PIOS_SOFT_UART_registerTxCallback,
    .bind_rx_cb   = PIOS_SOFT_UART_registerRxCallback,
    .rx_start_with_size    = PIOS_SOFT_UART_rxStartWithSize,
    .set_transceiver_lines = PIOS_SOFT_UART_setTransceiverPins,
    .set_inverted = PIOS_SOFT_UART_setInverted,
};


/**
 * Initialise a software UART device
 */
uint32_t PIOS_SOFT_UART_Init(uint32_t *dev_id,
                             const struct pios_soft_uart_cfg *soft_uart_cfg)
{
    struct pios_soft_uart_dev *dev = &pios_soft_uart_device;

    assert_param(soft_uart_cfg);

    memset(dev, 0, sizeof(struct pios_soft_uart_dev));
    dev->magic        = PIOS_SOFT_UART_DEV_MAGIC;
    dev->cfg          = soft_uart_cfg;
    dev->baudrate     = soft_uart_cfg->uartInit.BaudRate;
    dev->state        = SOFT_UART_STATE_RESET;
    dev->errorCode    = SOFT_UART_ERROR_NONE;
    dev->isJobPending = false;
    dev->isInverted   = false;
    *dev_id = (uint32_t)dev;
    return STATUS_OK;
}

uint32_t PIOS_SOFT_UART_getError(void)
{
    return pios_soft_uart_device.errorCode;
}

uint32_t PIOS_SOFT_UART_getState(void)
{
    return pios_soft_uart_device.state;
}

bool PIOS_SOFT_UART_extiIRQHandler(void)
{
    uint32_t tmpFormat = 0;
    uint32_t tmpData   = 0;

    struct pios_soft_uart_dev *dev = &pios_soft_uart_device;

    /* Disable EXTI line Rx interrupt */
    EXTI->IMR &= ~(dev->rx_pin->exti.init.EXTI_Line);

    if ((dev->rxXferCount % 2) != 0) {
        tmpFormat = (uint32_t)pSecondFrame_Rx;
    } else {
        tmpFormat = (uint32_t)pFirstFrame_Rx;
    }

    /* Start receiver mode */
    PIOS_SOFT_UART_receiveFrame(dev);

    if (dev->rxXferCount > PIOS_SOFT_UART_FIRST_BYTE) {
        uint8_t byte;

        if (dev->isInverted == false) {
            byte = PIOS_SOFT_UART_formatRxFrame(dev, (uint32_t *)tmpFormat, (uint8_t)tmpData);
        } else {
            byte = PIOS_SOFT_UART_formatInvertedRxFrame(dev, (uint32_t *)tmpFormat, (uint8_t)tmpData);
        }

        *(dev->rxBuffer + (dev->rxXferCount - 2)) = byte;
    }

    return true;
}

void PIOS_SOFT_UART_txDMAIRQHandler(void)
{
    if (DMA_GetFlagStatus(pios_soft_uart_device.cfg->txDmaStream, DMA_FLAG_TEIF1) != RESET) {
        pios_soft_uart_device.errorCode = SOFT_UART_ERROR_DMA;
    }

    DMA_ClearFlag(pios_soft_uart_device.cfg->txDmaStream, DMA_FLAG_TCIF1);

    PIOS_SOFT_UART_txDMATransferComplete();
}

void PIOS_SOFT_UART_rxDMAIRQHandler(void)
{
    pios_soft_uart_device.rxXferCount++;

    PIOS_SOFT_UART_setupTimer(pios_soft_uart_device.cfg,
                              pios_soft_uart_device.baudrate);

    /* Enable EXTI line Rx interrupt for next frame */
    EXTI->IMR |= pios_soft_uart_device.rx_pin->exti.init.EXTI_Line;

    if (DMA_GetFlagStatus(pios_soft_uart_device.cfg->rxDmaStream, DMA_FLAG_TEIF2) != RESET) {
        pios_soft_uart_device.errorCode |= SOFT_UART_ERROR_DMA;
    }

    DMA_ClearFlag(pios_soft_uart_device.cfg->rxDmaStream, DMA_FLAG_TCIF2);

    PIOS_SOFT_UART_rxDMATransferComplete();
}


static bool PIOS_SOFT_UART_validateDev(struct pios_soft_uart_dev *dev)
{
    return dev->magic == PIOS_SOFT_UART_DEV_MAGIC;
}

static void PIOS_SOFT_UART_setupTimer(const struct pios_soft_uart_cfg *cfg,
                                      uint32_t baud)
{
    TIM_TimeBaseInitTypeDef timerInitCfg;

    timerInitCfg = cfg->timerInit;
    timerInitCfg.TIM_Period = ((PIOS_PERIPHERAL_APB2_CLOCK / baud) - 1);

    TIM_Cmd(cfg->timer, DISABLE);
    TIM_TimeBaseInit(cfg->timer, &timerInitCfg);
    TIM_InternalClockConfig(cfg->timer);
    TIM_ARRPreloadConfig(cfg->timer, ENABLE);
}

static void PIOS_SOFT_UART_setupTx(const struct pios_soft_uart_cfg *cfg,
                                   const struct pios_exti_cfg *tx_pin_cfg)
{
    GPIO_InitTypeDef gpioInitCfg;
    DMA_InitTypeDef dmaInitCfg;

    gpioInitCfg = tx_pin_cfg->pin.init;
    gpioInitCfg.GPIO_Mode  = GPIO_Mode_OUT;
    gpioInitCfg.GPIO_OType = GPIO_OType_PP;
    gpioInitCfg.GPIO_Speed = GPIO_High_Speed;
    GPIO_Init(tx_pin_cfg->pin.gpio, &gpioInitCfg);
    if (pios_soft_uart_device.isInverted == false) {
        GPIO_SetBits(tx_pin_cfg->pin.gpio, gpioInitCfg.GPIO_Pin);
    } else {
        GPIO_ResetBits(tx_pin_cfg->pin.gpio, gpioInitCfg.GPIO_Pin);
    }

    dmaInitCfg = cfg->txDmaInit;
    dmaInitCfg.DMA_PeripheralBaseAddr = (uint32_t)&(tx_pin_cfg->pin.gpio->BSRRL);
    DMA_Init(cfg->txDmaStream, &dmaInitCfg);

    NVIC_Init(&(cfg->txDmaIrq.init));
}

static void PIOS_SOFT_UART_setupRx(const struct pios_soft_uart_cfg *cfg,
                                   const struct pios_exti_cfg *rx_pin_cfg)
{
    GPIO_InitTypeDef gpioInitCfg;
    DMA_InitTypeDef dmaInitCfg;

    gpioInitCfg = rx_pin_cfg->pin.init;
    gpioInitCfg.GPIO_Mode = GPIO_Mode_IN;
    GPIO_Init(rx_pin_cfg->pin.gpio, &gpioInitCfg);

    if (pios_soft_uart_device.isInverted == false) {
        PIOS_EXTI_Init(rx_pin_cfg);
    } else {
        struct pios_exti_cfg inverted_rx_pin_cfg;
        memcpy(&inverted_rx_pin_cfg, rx_pin_cfg, sizeof(struct pios_exti_cfg));
        inverted_rx_pin_cfg.exti.init.EXTI_Trigger = EXTI_Trigger_Rising;
        inverted_rx_pin_cfg.pin.init.GPIO_PuPd     = GPIO_PuPd_DOWN;

        PIOS_EXTI_Init(&inverted_rx_pin_cfg);
    }

    dmaInitCfg = cfg->rxDmaInit;
    dmaInitCfg.DMA_PeripheralBaseAddr = (uint32_t)&(rx_pin_cfg->pin.gpio->BSRRL);
    DMA_Init(cfg->rxDmaStream, &dmaInitCfg);

    NVIC_Init(&(cfg->rxDmaIrq.init));
}

static void PIOS_SOFT_UART_changeBaud(uint32_t dev_id, uint32_t baud)
{
    struct pios_soft_uart_dev *dev = (struct pios_soft_uart_dev *)dev_id;
    bool valid = PIOS_SOFT_UART_validateDev(dev);

    PIOS_Assert(valid);

    TIM_TimeBaseInitTypeDef timerInitCfg;
    timerInitCfg = dev->cfg->timerInit;
    timerInitCfg.TIM_Period = ((PIOS_PERIPHERAL_APB2_CLOCK / baud) - 1);

    TIM_Cmd(dev->cfg->timer, DISABLE);
    TIM_TimeBaseInit(dev->cfg->timer, &timerInitCfg);
    TIM_InternalClockConfig(dev->cfg->timer);
    TIM_ARRPreloadConfig(dev->cfg->timer, ENABLE);

    dev->baudrate = baud;
}

static void PIOS_SOFT_UART_registerRxCallback(uint32_t dev_id, pios_com_callback rx_in_cb, uint32_t context)
{
    struct pios_soft_uart_dev *dev = (struct pios_soft_uart_dev *)dev_id;
    bool valid = PIOS_SOFT_UART_validateDev(dev);

    PIOS_Assert(valid);

    dev->rx_in_context = context;
    dev->rx_in_cb = rx_in_cb;
}

static void PIOS_SOFT_UART_registerTxCallback(uint32_t dev_id, pios_com_callback tx_out_cb, uint32_t context)
{
    struct pios_soft_uart_dev *dev = (struct pios_soft_uart_dev *)dev_id;
    bool valid = PIOS_SOFT_UART_validateDev(dev);

    PIOS_Assert(valid);

    dev->tx_out_context = context;
    dev->tx_out_cb = tx_out_cb;
}

static void PIOS_SOFT_UART_txStart(uint32_t dev_id, __attribute__((unused)) uint16_t tx_bytes_avail)
{
    uint16_t bytes_to_tx = 0;
    struct pios_soft_uart_dev *dev = (struct pios_soft_uart_dev *)dev_id;
    bool valid = PIOS_SOFT_UART_validateDev(dev);

    PIOS_Assert(valid);

    if ((dev->state != SOFT_UART_STATE_READY) &&
        (dev->state != SOFT_UART_STATE_BUSY_RX)) {
        dev->errorCode = SOFT_UART_ERROR_BUSY;
        return;
    }

    if (dev->tx_out_cb == NULL) {
        return;
    }

    if (true == dev->isOneWireSerial) {
        PIOS_SOFT_UART_setupTx(dev->cfg, dev->tx_pin);
    }

    PIOS_SOFT_UART_setupTimer(dev->cfg, dev->baudrate);

    bool need_yield = false;
    bytes_to_tx = dev->tx_out_cb(dev->tx_out_context, dev->txBuffer,
                                 PIOS_SOFT_UART_BUFFER_SIZE, NULL, &need_yield);

    if (0 == bytes_to_tx) {
        return;
    }

    dev->txXferSize = bytes_to_tx;
    dev->errorCode  = SOFT_UART_ERROR_NONE;
    PIOS_SOFT_UART_txDMA(dev);

#if defined(PIOS_INCLUDE_FREERTOS)
    if (need_yield) {
        portYIELD();
    }
#endif /* PIOS_INCLUDE_FREERTOS */
}

static void PIOS_SOFT_UART_txDMA(struct pios_soft_uart_dev *dev)
{
    dev->txXferCount = PIOS_SOFT_UART_FIRST_BYTE;

    if (dev->state == SOFT_UART_STATE_BUSY_RX) {
        dev->state = SOFT_UART_STATE_BUSY_TX_RX;
    } else {
        dev->state = SOFT_UART_STATE_BUSY_TX;
    }

    if (dev->isInverted == false) {
        PIOS_SOFT_UART_formatTxFrame(dev, dev->txBuffer[0], (uint32_t *)pFirstFrame_Tx);
    } else {
        PIOS_SOFT_UART_formatInvertedTxFrame(dev, dev->txBuffer[0], (uint32_t *)pFirstFrame_Tx);
    }

    TIM_CCxCmd(dev->cfg->timer, TIM_Channel_1, TIM_CCx_Enable);

    PIOS_SOFT_UART_sendFrame(dev);

    if (dev->txXferCount < dev->txXferSize) {
        if (dev->isInverted == false) {
            PIOS_SOFT_UART_formatTxFrame(dev, dev->txBuffer[dev->txXferCount], (uint32_t *)pSecondFrame_Tx);
        } else {
            PIOS_SOFT_UART_formatInvertedTxFrame(dev, dev->txBuffer[dev->txXferCount], (uint32_t *)pSecondFrame_Tx);
        }
    }
}

static void PIOS_SOFT_UART_rxStartWithSize(uint32_t dev_id, uint16_t rx_bytes)
{
    struct pios_soft_uart_dev *dev = (struct pios_soft_uart_dev *)dev_id;
    bool valid = PIOS_SOFT_UART_validateDev(dev);

    PIOS_Assert(valid);

    if ((dev->state != SOFT_UART_STATE_READY) &&
        (dev->state != SOFT_UART_STATE_BUSY_TX)) {
        dev->errorCode = SOFT_UART_ERROR_BUSY;
        return;
    }

    if (dev->rx_in_cb == NULL) {
        return;
    }

    dev->rxXferSize = rx_bytes;

    if (true == dev->isOneWireSerial) {
        if (dev->state == SOFT_UART_STATE_BUSY_TX) {
            dev->isJobPending = true;
            return;
        }
        PIOS_SOFT_UART_setupRx(dev->cfg, dev->rx_pin);
        EXTI->IMR |= dev->rx_pin->exti.init.EXTI_Line;
    }

    PIOS_SOFT_UART_setupTimer(dev->cfg, dev->baudrate);

    dev->errorCode = SOFT_UART_ERROR_NONE;
    PIOS_SOFT_UART_rxDMA(dev);
}

static void PIOS_SOFT_UART_setTransceiverPins(uint32_t dev_id,
                                              uint32_t tx_line_context, uint32_t rx_line_context)
{
    struct pios_soft_uart_dev *dev = (struct pios_soft_uart_dev *)dev_id;
    bool valid = PIOS_SOFT_UART_validateDev(dev);

    PIOS_Assert(valid);
    assert_param(tx_line_context);
    assert_param(rx_line_context);

    if (tx_line_context == rx_line_context) {
        dev->isOneWireSerial = true;
    }

    dev->tx_pin = (struct pios_exti_cfg *)tx_line_context;
    dev->rx_pin = (struct pios_exti_cfg *)rx_line_context;

    /* Since TX & RX of 1-wire serial share the same pin, it has to initialize
     * GPIO & DMA when starting tx/rx.
     */
    if (dev->isOneWireSerial == true) {
        PIOS_SOFT_UART_setupTx(dev->cfg, dev->tx_pin);
        dev->state = SOFT_UART_STATE_READY;
        return;
    }

    PIOS_SOFT_UART_setupTx(dev->cfg, dev->tx_pin);
    PIOS_SOFT_UART_setupRx(dev->cfg, dev->rx_pin);

    dev->state = SOFT_UART_STATE_READY;
}

static void PIOS_SOFT_UART_setInverted(uint32_t dev_id, bool isInverted)
{
    struct pios_soft_uart_dev *dev = (struct pios_soft_uart_dev *)dev_id;
    bool valid = PIOS_SOFT_UART_validateDev(dev);

    PIOS_Assert(valid);

    dev->isInverted = isInverted;
}

static void PIOS_SOFT_UART_rxDMA(struct pios_soft_uart_dev *dev)
{
    dev->rxXferCount = PIOS_SOFT_UART_FIRST_BYTE;

    if (dev->state == SOFT_UART_STATE_BUSY_TX) {
        dev->state = SOFT_UART_STATE_BUSY_TX_RX;
    } else {
        dev->state = SOFT_UART_STATE_BUSY_RX;
    }

    TIM_CCxCmd(dev->cfg->timer, TIM_Channel_2, TIM_CCx_Enable);
}

static void PIOS_SOFT_UART_formatTxFrame(struct pios_soft_uart_dev *dev,
                                         uint8_t data, uint32_t *pFrame_Tx)
{
    uint32_t counter   = 0;
    uint32_t bitmask   = 0;
    uint32_t length    = 0;
    uint32_t cntparity = 0;

    length  = dev->cfg->uartInit.WordLength;
    bitmask = (uint32_t)dev->tx_pin->pin.init.GPIO_Pin;

    // Start bit
    pFrame_Tx[0] = (bitmask << 16);

    if (SOFT_UART_PARITY_NONE == dev->cfg->uartInit.Parity) {
        for (counter = 0; counter < length; counter++) {
            if (((data >> counter) & PIOS_SOFT_UART_BIT_MASK) != 0) {
                pFrame_Tx[counter + 1] = bitmask;
            } else {
                pFrame_Tx[counter + 1] = (bitmask << 16);
            }
        }
    } else {
        for (counter = 0; counter < (length - 1); counter++) {
            if (((data >> counter) & PIOS_SOFT_UART_BIT_MASK) != 0) {
                pFrame_Tx[counter + 1] = bitmask;
                cntparity++;
            } else {
                pFrame_Tx[counter + 1] = (bitmask << 16);
            }
        }
    }

    switch (dev->cfg->uartInit.Parity) {
    case SOFT_UART_PARITY_ODD:
        if ((cntparity % 2) != SET) {
            pFrame_Tx[length] = bitmask;
        } else {
            pFrame_Tx[length] = (bitmask << 16);
        }

        break;

    case SOFT_UART_PARITY_EVEN:
        if ((cntparity % 2) != SET) {
            pFrame_Tx[length] = (bitmask << 16);
        } else {
            pFrame_Tx[length] = bitmask;
        }

        break;

    default:
        break;
    }

    // Stop bit
    pFrame_Tx[length + 1] = bitmask;
    if (dev->cfg->uartInit.StopBits == SOFT_UART_STOPBITS_2) {
        pFrame_Tx[length + 2] = bitmask;
    }
}

static uint8_t PIOS_SOFT_UART_formatRxFrame(struct pios_soft_uart_dev *dev,
                                            uint32_t *pFrame_Rx, uint8_t data)
{
    uint32_t counter      = 0;
    uint32_t length       = 0;
    uint32_t tmp          = 0;
    uint32_t cntparity    = 0;
    uint8_t sample_offset = 0;

    if (dev->cfg->uartInit.Parity != SOFT_UART_PARITY_NONE) {
        length = dev->cfg->uartInit.WordLength - 1;
    } else {
        length = dev->cfg->uartInit.WordLength;
    }

    if ((pFrame_Rx[dev->cfg->uartInit.WordLength + 1] & dev->rx_pin->pin.init.GPIO_Pin) !=
        dev->rx_pin->pin.init.GPIO_Pin) {
        sample_offset = 1;
    }

    for (counter = 0; counter < length; counter++) {
        if ((pFrame_Rx[counter + 1 + sample_offset] & (dev->rx_pin->pin.init.GPIO_Pin)) ==
            dev->rx_pin->pin.init.GPIO_Pin) {
            data = (0x01 << counter) | data;
            cntparity++;
        }
    }

    /* Process parity bit */
    if (dev->cfg->uartInit.Parity == SOFT_UART_PARITY_ODD) {
        if (((cntparity % 2) != SET) &&
            ((pFrame_Rx[length + 1 + sample_offset] & dev->rx_pin->pin.init.GPIO_Pin) !=
             dev->rx_pin->pin.init.GPIO_Pin)) {
            dev->errorCode |= SOFT_UART_ERROR_PE;
        }
    }

    if (dev->cfg->uartInit.Parity == SOFT_UART_PARITY_EVEN) {
        if (((cntparity % 2) != RESET) &&
            ((pFrame_Rx[length + 1 + sample_offset] & dev->rx_pin->pin.init.GPIO_Pin) !=
             dev->rx_pin->pin.init.GPIO_Pin)) {
            dev->errorCode |= SOFT_UART_ERROR_PE;
        }
    }

    cntparity = 0;
    tmp = data;

    return tmp;
}

static void PIOS_SOFT_UART_formatInvertedTxFrame(struct pios_soft_uart_dev *dev,
                                                 uint8_t data, uint32_t *pFrame_Tx)
{
    uint32_t counter   = 0;
    uint32_t bitmask   = 0;
    uint32_t length    = 0;
    uint32_t cntparity = 0;

    length  = dev->cfg->uartInit.WordLength;
    bitmask = (uint32_t)dev->tx_pin->pin.init.GPIO_Pin;

    // Start bit
    pFrame_Tx[0] = bitmask;

    if (SOFT_UART_PARITY_NONE == dev->cfg->uartInit.Parity) {
        for (counter = 0; counter < length; counter++) {
            if (((data >> counter) & PIOS_SOFT_UART_BIT_MASK) != 0) {
                pFrame_Tx[counter + 1] = (bitmask << 16);
            } else {
                pFrame_Tx[counter + 1] = bitmask;
            }
        }
    } else {
        for (counter = 0; counter < (length - 1); counter++) {
            if (((data >> counter) & PIOS_SOFT_UART_BIT_MASK) != 0) {
                pFrame_Tx[counter + 1] = (bitmask << 16);
                cntparity++;
            } else {
                pFrame_Tx[counter + 1] = bitmask;
            }
        }
    }

    switch (dev->cfg->uartInit.Parity) {
    case SOFT_UART_PARITY_ODD:
        if ((cntparity % 2) != SET) {
            pFrame_Tx[length] = (bitmask << 16);
        } else {
            pFrame_Tx[length] = bitmask;
        }

        break;

    case SOFT_UART_PARITY_EVEN:
        if ((cntparity % 2) != SET) {
            pFrame_Tx[length] = bitmask;
        } else {
            pFrame_Tx[length] = (bitmask << 16);
        }

        break;

    default:
        break;
    }

    // Stop bit
    pFrame_Tx[length + 1] = (bitmask << 16);
    if (dev->cfg->uartInit.StopBits == SOFT_UART_STOPBITS_2) {
        pFrame_Tx[length + 2] = (bitmask << 16);
    }
}

static uint8_t PIOS_SOFT_UART_formatInvertedRxFrame(struct pios_soft_uart_dev *dev,
                                                    uint32_t *pFrame_Rx, uint8_t data)
{
    uint32_t counter      = 0;
    uint32_t length       = 0;
    uint32_t tmp          = 0;
    uint32_t cntparity    = 0;
    uint8_t sample_offset = 0;

    if (dev->cfg->uartInit.Parity != SOFT_UART_PARITY_NONE) {
        length = dev->cfg->uartInit.WordLength - 1;
    } else {
        length = dev->cfg->uartInit.WordLength;
    }

    if ((pFrame_Rx[dev->cfg->uartInit.WordLength + 1] & dev->rx_pin->pin.init.GPIO_Pin) ==
        dev->rx_pin->pin.init.GPIO_Pin) {
        sample_offset = 1;
    }

    for (counter = 0; counter < length; counter++) {
        if ((pFrame_Rx[counter + 1 + sample_offset] & (dev->rx_pin->pin.init.GPIO_Pin)) !=
            dev->rx_pin->pin.init.GPIO_Pin) {
            data = (0x01 << counter) | data;
            cntparity++;
        }
    }

    /* Process parity bit */
    if (dev->cfg->uartInit.Parity == SOFT_UART_PARITY_ODD) {
        if (((cntparity % 2) != SET) &&
            ((pFrame_Rx[length + 1 + sample_offset] & dev->rx_pin->pin.init.GPIO_Pin) ==
             dev->rx_pin->pin.init.GPIO_Pin)) {
            dev->errorCode |= SOFT_UART_ERROR_PE;
        }
    }

    if (dev->cfg->uartInit.Parity == SOFT_UART_PARITY_EVEN) {
        if (((cntparity % 2) != RESET) &&
            ((pFrame_Rx[length + 1 + sample_offset] & dev->rx_pin->pin.init.GPIO_Pin) ==
             dev->rx_pin->pin.init.GPIO_Pin)) {
            dev->errorCode |= SOFT_UART_ERROR_PE;
        }
    }

    cntparity = 0;
    tmp = data;

    return tmp;
}

static void PIOS_SOFT_UART_sendFrame(struct pios_soft_uart_dev *dev)
{
    uint32_t tmp_sr   = 0;
    uint32_t tmp_ds   = 0;
    uint32_t tmp_size = 0;

    if ((dev->txXferCount % 2) != 0) {
        tmp_sr = (uint32_t)pFirstFrame_Tx;
    } else {
        tmp_sr = (uint32_t)pSecondFrame_Tx;
    }

    tmp_ds   = (uint32_t)&(dev->tx_pin->pin.gpio->BSRRL);
    tmp_size = (uint16_t)(dev->cfg->uartInit.WordLength +
                          dev->cfg->uartInit.StopBits + 1);

    dev->cfg->txDmaStream->NDTR = tmp_size;
    dev->cfg->txDmaStream->PAR  = tmp_ds;
    dev->cfg->txDmaStream->M0AR = tmp_sr;
    DMA_ITConfig(dev->cfg->txDmaStream, DMA_IT_TC, ENABLE);
    DMA_ITConfig(dev->cfg->txDmaStream, DMA_IT_TE, ENABLE);
    DMA_Cmd(dev->cfg->txDmaStream, ENABLE);
    TIM_DMACmd(dev->cfg->timer, TIM_DMA_CC1, ENABLE);
    TIM_Cmd(dev->cfg->timer, ENABLE);
}

static void PIOS_SOFT_UART_receiveFrame(struct pios_soft_uart_dev *dev)
{
    uint32_t tmp_sr   = 0;
    uint32_t tmp_ds   = 0;
    uint32_t tmp_size = 0;
    uint32_t tmp_arr  = 0;

    if ((dev->rxXferCount % 2) != 0) {
        tmp_ds = (uint32_t)pFirstFrame_Rx;
    } else {
        tmp_ds = (uint32_t)pSecondFrame_Rx;
    }

    tmp_arr  = dev->cfg->timerInit.TIM_Period;
    tmp_sr   = (uint32_t)&(dev->rx_pin->pin.gpio->IDR);
    tmp_size = (uint16_t)(dev->cfg->uartInit.WordLength +
                          dev->cfg->uartInit.StopBits + 1);

    DMA_ITConfig(dev->cfg->rxDmaStream, DMA_IT_TC, ENABLE);
    DMA_ITConfig(dev->cfg->rxDmaStream, DMA_IT_TE, ENABLE);

    dev->cfg->rxDmaStream->NDTR = tmp_size;
    dev->cfg->rxDmaStream->PAR  = tmp_sr;
    dev->cfg->rxDmaStream->M0AR = tmp_ds;
    DMA_Cmd(dev->cfg->rxDmaStream, ENABLE);

    TIM_SetCompare2(dev->cfg->timer,
                    TIM_GetCounter(dev->cfg->timer) + ((tmp_arr / 2) % tmp_arr));
    TIM_DMACmd(dev->cfg->timer, TIM_DMA_CC2, ENABLE);
    TIM_Cmd(dev->cfg->timer, ENABLE);
}

static void PIOS_SOFT_UART_txDMATransferComplete(void)
{
    uint32_t tmpbuffer = 0;
    struct pios_soft_uart_dev *dev = &pios_soft_uart_device;

    // Incremente frame counter
    dev->txXferCount++;
    if (dev->txXferCount <= dev->txXferSize) {
        PIOS_SOFT_UART_sendFrame(dev);

        if ((dev->txXferCount % 2) != 0) {
            tmpbuffer = (uint32_t)pSecondFrame_Tx;
        } else {
            tmpbuffer = (uint32_t)pFirstFrame_Tx;
        }

        if (dev->isInverted == false) {
            PIOS_SOFT_UART_formatTxFrame(dev, dev->txBuffer[dev->txXferCount],
                                         (uint32_t *)tmpbuffer);
        } else {
            PIOS_SOFT_UART_formatInvertedTxFrame(dev, dev->txBuffer[dev->txXferCount],
                                                 (uint32_t *)tmpbuffer);
        }
    } else {
        // No more data to be transfered
        DMA_ITConfig(dev->cfg->txDmaStream, DMA_IT_TC, DISABLE);
        TIM_CCxCmd(dev->cfg->timer, TIM_Channel_1, TIM_CCx_Disable);
        dev->txXferCount = 0;
        dev->errorCode   = SOFT_UART_ERROR_NONE;

        if (dev->state == SOFT_UART_STATE_BUSY_TX_RX) {
            dev->state = SOFT_UART_STATE_BUSY_RX;
        } else {
            dev->state = SOFT_UART_STATE_READY;
        }

        if ((dev->isOneWireSerial == true) &&
            (dev->isJobPending == true)) {
            PIOS_SOFT_UART_setupTimer(dev->cfg, dev->baudrate);
            PIOS_SOFT_UART_setupRx(dev->cfg, dev->rx_pin);
            PIOS_SOFT_UART_rxDMA(dev);
            dev->isJobPending = false;
            EXTI->IMR |= dev->rx_pin->exti.init.EXTI_Line;
        }
    }
}

static void PIOS_SOFT_UART_rxDMATransferComplete(void)
{
    uint32_t tmpFormat = 0;
    uint32_t tmpData   = 0;
    uint8_t byte = 0xff;
    bool rx_need_yield = false;
    struct pios_soft_uart_dev *dev = &pios_soft_uart_device;

    if (dev->rxXferCount <= dev->rxXferSize) {
        return;
    }

    EXTI->IMR &= ~(dev->rx_pin->exti.init.EXTI_Line);

    if ((dev->rxXferCount % 2) == 0) {
        tmpFormat = (uint32_t)pFirstFrame_Rx;
    } else {
        tmpFormat = (uint32_t)pSecondFrame_Rx;
    }

    if (dev->isInverted == false) {
        byte = PIOS_SOFT_UART_formatRxFrame(dev, (uint32_t *)tmpFormat, (uint8_t)tmpData);
    } else {
        byte = PIOS_SOFT_UART_formatInvertedRxFrame(dev, (uint32_t *)tmpFormat, (uint8_t)tmpData);
    }
    *(dev->rxBuffer + (dev->rxXferCount - 2)) = byte;
    (dev->rx_in_cb)(dev->rx_in_context, dev->rxBuffer, (dev->rxXferCount - 1),
                    NULL, &rx_need_yield);

    DMA_ITConfig(dev->cfg->rxDmaStream, DMA_IT_TC, DISABLE);
    TIM_CCxCmd(dev->cfg->timer, TIM_Channel_2, TIM_CCx_Disable);
    dev->rxXferCount = 0;
    dev->errorCode   = SOFT_UART_ERROR_NONE;

    if (dev->state == SOFT_UART_STATE_BUSY_TX_RX) {
        dev->state = SOFT_UART_STATE_BUSY_TX;
    } else {
        dev->state = SOFT_UART_STATE_READY;
    }

#if defined(PIOS_INCLUDE_FREERTOS)
    if (rx_need_yield) {
        portYIELD();
    }
#endif /* PIOS_INCLUDE_FREERTOS */
}
#endif /* PIOS_INCLUDE_SOFT_UART */

/**
 * @}
 * @}
 */
