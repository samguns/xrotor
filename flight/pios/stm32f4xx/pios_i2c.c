/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core hardware abstraction layer
 * @{
 * @addtogroup   PIOS_I2C I2C Functions
 * @brief STM32F4xx Hardware dependent I2C functionality
 * @{
 *
 * @file       pios_i2c.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2012.
 * @brief      I2C Enable/Disable routines
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pios.h"

#ifdef TRACE_I2C
traceLabel trace_bus_claim;
traceLabel trace_bus_release;
traceLabel trace_transfer_start;
traceLabel trace_transfer_stop;
traceLabel trace_addr;
#endif /* TRACE_I2C */

#ifdef PIOS_INCLUDE_I2C

#ifdef PIOS_INCLUDE_FREERTOS
#define USE_FREERTOS
#endif

#include <pios_i2c_priv.h>

static bool PIOS_I2C_validate(struct pios_i2c_adapter *i2c_adapter)
{
    return i2c_adapter->magic == PIOS_I2C_DEV_MAGIC;
}

#if defined(PIOS_INCLUDE_FREERTOS) && 0
static struct pios_i2c_dev *PIOS_I2C_alloc(void)
{
    struct pios_i2c_dev *i2c_adapter;

    i2c_adapter = (struct pios_i2c_adapter *)pios_malloc(sizeof(*i2c_adapter));
    if (!i2c_adapter) {
        return NULL;
    }

    i2c_adapter->magic = PIOS_I2C_DEV_MAGIC;
    return i2c_adapter;
}
#else
static struct pios_i2c_adapter pios_i2c_adapters[PIOS_I2C_MAX_DEVS];
static uint8_t pios_i2c_num_adapters;
static struct pios_i2c_adapter *PIOS_I2C_alloc(void)
{
    struct pios_i2c_adapter *i2c_adapter;

    if (pios_i2c_num_adapters >= PIOS_I2C_MAX_DEVS) {
        return NULL;
    }

    i2c_adapter = &pios_i2c_adapters[pios_i2c_num_adapters++];
    i2c_adapter->magic = PIOS_I2C_DEV_MAGIC;

    return i2c_adapter;
}
#endif /* if defined(PIOS_INCLUDE_FREERTOS) && 0 */

/**
 * Initialize I2C driver
 */
int32_t PIOS_I2C_Init(uint32_t *i2c_id, const struct pios_i2c_adapter_cfg *cfg)
{
#ifdef TRACE_I2C
    trace_bus_claim      = xTraceOpenLabel("I2C bus claim");
    trace_bus_release    = xTraceOpenLabel("I2C bus release");
    trace_transfer_start = xTraceOpenLabel("I2C transfer start");
    trace_transfer_stop  = xTraceOpenLabel("I2C transfer stop");
    trace_addr = xTraceOpenLabel("I2C address");
#endif /* TRACE_I2C */
    PIOS_DEBUG_Assert(i2c_id);
    PIOS_DEBUG_Assert(cfg);

    struct pios_i2c_adapter *i2c_adapter;

    i2c_adapter = (struct pios_i2c_adapter *)PIOS_I2C_alloc();
    if (!i2c_adapter) {
        return STATUS_ERROR;
    }

    /* Return the new device structure */
    *i2c_id = (uint32_t)i2c_adapter;

    /* Bind the configuration to the device instance */
    i2c_adapter->cfg = cfg;

    i2c_adapter->sem_busy  = xSemaphoreCreateBinary();
    xSemaphoreGive(i2c_adapter->sem_busy);
    i2c_adapter->sem_ready = xSemaphoreCreateBinary();

    /* Initialize the GPIO pins to the peripheral function */
    if (i2c_adapter->cfg->remapSCL) {
        GPIO_PinAFConfig(i2c_adapter->cfg->scl.gpio,
                         __builtin_ctz(i2c_adapter->cfg->scl.init.GPIO_Pin),
                         i2c_adapter->cfg->remapSCL);
    }
    if (i2c_adapter->cfg->remapSDA) {
        GPIO_PinAFConfig(i2c_adapter->cfg->sda.gpio,
                         __builtin_ctz(i2c_adapter->cfg->sda.init.GPIO_Pin),
                         i2c_adapter->cfg->remapSDA);
    }
    GPIO_Init(i2c_adapter->cfg->scl.gpio,
              (GPIO_InitTypeDef *)&(i2c_adapter->cfg->scl.init));
    GPIO_Init(i2c_adapter->cfg->sda.gpio,
              (GPIO_InitTypeDef *)&(i2c_adapter->cfg->sda.init));

    /* Reset the I2C block */
    I2C_DeInit(i2c_adapter->cfg->regs);

    /* Initialize the I2C block */
    I2C_Init(i2c_adapter->cfg->regs, (I2C_InitTypeDef *)&(i2c_adapter->cfg->init));

    /* Configure and enable I2C interrupts */
    NVIC_Init((NVIC_InitTypeDef *)&(i2c_adapter->cfg->event.init));
    NVIC_Init((NVIC_InitTypeDef *)&(i2c_adapter->cfg->error.init));
    NVIC_Init((NVIC_InitTypeDef *)&(i2c_adapter->cfg->dma.irq_rx.init));
    NVIC_Init((NVIC_InitTypeDef *)&(i2c_adapter->cfg->dma.irq_tx.init));

    I2C_ITConfig(i2c_adapter->cfg->regs, I2C_IT_BUF, DISABLE);
    I2C_ITConfig(i2c_adapter->cfg->regs, I2C_IT_EVT | I2C_IT_ERR, ENABLE);

    // Enable interface
    I2C_Cmd(i2c_adapter->cfg->regs, ENABLE);

    /* No error */
    return STATUS_OK;
}

/* Perform an I2C transfer */
int32_t PIOS_I2C_Transfer(uint32_t i2c_id, const struct pios_i2c_txn txn_list[], uint32_t num_txns)
{
    extern uint32_t _sram_start;
    extern uint32_t _sram_end;

    struct pios_i2c_adapter *i2c_adapter = (struct pios_i2c_adapter *)i2c_id;

    if (!PIOS_I2C_validate(i2c_adapter)) {
        return STATUS_ERROR;
    }

    PIOS_DEBUG_Assert(txn_list);
    PIOS_DEBUG_Assert(num_txns);

    for (uint32_t i = 0; i < num_txns; i++) {
        if ((txn_list[i].buf < (uint8_t *)&_sram_start) ||
            (txn_list[i].buf > (uint8_t *)&_sram_end)) {
            // Invalid address for DMA
#ifdef PIOS_ASSERT_DMA_FAULT
            PIOS_Assert(0);
#endif /* PIOS_ASSERT_DMA_FAULT */
            return STATUS_FAULT;
        }
    }


    I2C_TypeDef *I2Cx = i2c_adapter->cfg->regs;

    // wait until I2C idle
    xSemaphoreTake(i2c_adapter->sem_busy, portMAX_DELAY);
#ifdef TRACE_I2C
    vTraceUserEvent(trace_bus_claim);
#endif /* TRACE_I2C */

    // Note the first and last txn entries for the interrupt sequence to follow
    i2c_adapter->active_txn = &txn_list[0];
    i2c_adapter->last_txn   = &txn_list[num_txns - 1];
    i2c_adapter->status     = STATUS_OK;

    // There is no callback
    i2c_adapter->callback   = NULL;

#ifdef TRACE_I2C
    vTraceUserEvent(trace_transfer_start);
#endif /* TRACE_I2C */

    // Send I2C START
    I2C_GenerateSTART(I2Cx, ENABLE);

    // Wait for the completion of all transfers
    xSemaphoreTake(i2c_adapter->sem_ready, portMAX_DELAY);

    return i2c_adapter->status;
}

int32_t PIOS_I2C_Transfer_Callback(uint32_t i2c_id, const struct pios_i2c_txn txn_list[], uint32_t num_txns, void *callback)
{
    struct pios_i2c_adapter *i2c_adapter = (struct pios_i2c_adapter *)i2c_id;

    if (!PIOS_I2C_validate(i2c_adapter)) {
        return STATUS_ERROR;
    }

    PIOS_Assert(callback);

    PIOS_DEBUG_Assert(txn_list);
    PIOS_DEBUG_Assert(num_txns);

    I2C_TypeDef *I2Cx = i2c_adapter->cfg->regs;

    // TODO Use queues so that the caller need not wait for the bus
    // wait until I2C idle
    xSemaphoreTake(i2c_adapter->sem_busy, portMAX_DELAY);
#ifdef TRACE_I2C
    vTraceUserEvent(trace_bus_claim);
#endif /* TRACE_I2C */

    // Note the first and last txn entries for the interrupt sequence to follow
    i2c_adapter->active_txn = &txn_list[0];
    i2c_adapter->last_txn   = &txn_list[num_txns - 1];
    i2c_adapter->status     = STATUS_OK;

    // Register the callback to call when txn sequence is complete
    i2c_adapter->callback   = callback;

#ifdef TRACE_I2C
    vTraceUserEvent(trace_transfer_start);
#endif /* TRACE_I2C */

    // Send I2C START
    I2C_GenerateSTART(I2Cx, ENABLE);

    return i2c_adapter->status;
}

void PIOS_I2C_EV_IRQ_Handler(uint32_t i2c_id)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    struct pios_i2c_adapter *i2c_adapter   = (struct pios_i2c_adapter *)i2c_id;

    if (!PIOS_I2C_validate(i2c_adapter)) {
        return;
    }

    I2C_TypeDef *I2Cx = i2c_adapter->cfg->regs;
    const struct pios_i2c_txn *cur_txn = i2c_adapter->active_txn;

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISRBegin(i2c_adapter->cfg->event.init.NVIC_IRQChannel);
#endif
    if (I2C_GetITStatus(I2Cx, I2C_IT_SB)) {
        // The start bit has been sent
        // DMA must be enabled ahead of the address being written
        I2C_DMACmd(I2Cx, ENABLE);

#ifdef TRACE_I2C
        vTracePrintF(trace_addr,
                     cur_txn->rw == PIOS_I2C_TXN_READ ?
                     "0x%02x R" : "0x%02x W",
                     cur_txn->addr);
#endif /* TRACE_I2C */
        // Send slave Address for write
        I2C_Send7bitAddress(I2Cx,
                            cur_txn->addr << 1,
                            cur_txn->rw == PIOS_I2C_TXN_READ ?
                            I2C_Direction_Receiver : I2C_Direction_Transmitter);
    } else if (I2C_GetITStatus(I2Cx, I2C_IT_ADDR)) {
        // The address has been sent
        if (cur_txn->rw == PIOS_I2C_TXN_READ) {
            DMA_InitTypeDef dma_init;

            dma_init = i2c_adapter->cfg->dma.rx.init;

            DMA_DeInit(i2c_adapter->cfg->dma.rx.channel);
            dma_init.DMA_Memory0BaseAddr = (uint32_t)cur_txn->buf;
            dma_init.DMA_MemoryInc  = DMA_MemoryInc_Enable;
            dma_init.DMA_BufferSize = cur_txn->len;

            DMA_Init(i2c_adapter->cfg->dma.rx.channel, &(dma_init));
            // Enable DMA NACK automatic generation
            I2C_DMALastTransferCmd(I2Cx, ENABLE);
            DMA_ITConfig(i2c_adapter->cfg->dma.rx.channel, DMA_IT_TC, ENABLE);
            if (cur_txn->len < 2) {
                // Disable acknowledge of received data
                I2C_AcknowledgeConfig(I2Cx, DISABLE);
            } else {
                // Enable acknowledge of received data
                I2C_AcknowledgeConfig(I2Cx, ENABLE);
            }
            DMA_Cmd(i2c_adapter->cfg->dma.rx.channel, ENABLE);
        } else {
            DMA_InitTypeDef dma_init;

            dma_init = i2c_adapter->cfg->dma.tx.init;

            DMA_DeInit(i2c_adapter->cfg->dma.tx.channel);
            dma_init.DMA_Memory0BaseAddr = (uint32_t)cur_txn->buf;
            dma_init.DMA_MemoryInc  = DMA_MemoryInc_Enable;
            dma_init.DMA_BufferSize = cur_txn->len;

            DMA_Init(i2c_adapter->cfg->dma.tx.channel, &(dma_init));
            DMA_ITConfig(i2c_adapter->cfg->dma.tx.channel, DMA_IT_TC, ENABLE);
            DMA_Cmd(i2c_adapter->cfg->dma.tx.channel, ENABLE);
        }

        // Clear the ADDR flag
        (void)I2C_GetLastEvent(I2Cx);
    }

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISREnd(xHigherPriorityTaskWoken);
#endif
    if (xHigherPriorityTaskWoken == pdTRUE) {
        portYIELD();
    }
}


void PIOS_I2C_ER_IRQ_Handler(uint32_t i2c_id)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    struct pios_i2c_adapter *i2c_adapter   = (struct pios_i2c_adapter *)i2c_id;

    if (!PIOS_I2C_validate(i2c_adapter)) {
        return;
    }

    I2C_TypeDef *I2Cx = i2c_adapter->cfg->regs;

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISRBegin(i2c_adapter->cfg->error.init.NVIC_IRQChannel);
#endif

    if (I2C_GetITStatus(I2Cx, I2C_IT_AF)) {
        // Acknowledge failure
        i2c_adapter->status = STATUS_RETRY;
        I2C_ClearITPendingBit(I2Cx, I2C_IT_AF);
    } else if (I2C_GetITStatus(I2Cx, I2C_IT_BERR)) {
        // Bus error failure
        i2c_adapter->status = STATUS_BERR;
        I2C_ClearITPendingBit(I2Cx, I2C_IT_BERR);
    } else if (I2C_GetITStatus(I2Cx, I2C_IT_ARLO)) {
        // Treat arbitration lost as a bus error failure
        i2c_adapter->status = STATUS_BERR;
        I2C_ClearITPendingBit(I2Cx, I2C_IT_BERR);
    } else {
        goto no_error;
    }

    // Send I2C STOP
    I2C_GenerateSTOP(I2Cx, ENABLE);

    if (i2c_adapter->callback) {
        i2c_adapter->callback(i2c_adapter->status, &xHigherPriorityTaskWoken);
    } else {
        // End of transmission of data
        xSemaphoreGiveFromISR(i2c_adapter->sem_ready,
                              &xHigherPriorityTaskWoken);
    }

    // I2C adaptor free for next transfer
    xSemaphoreGiveFromISR(i2c_adapter->sem_busy,
                          &xHigherPriorityTaskWoken);

no_error:

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISREnd(xHigherPriorityTaskWoken);
#endif
    if (xHigherPriorityTaskWoken == pdTRUE) {
        portYIELD();
    }
}

void PIOS_I2C_RX_IRQ_Handler(uint32_t i2c_id)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    struct pios_i2c_adapter *i2c_adapter   = (struct pios_i2c_adapter *)i2c_id;

    if (!PIOS_I2C_validate(i2c_adapter)) {
        return;
    }

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISRBegin(i2c_adapter->cfg->dma.irq_rx.init.NVIC_IRQChannel);
#endif

    I2C_TypeDef *I2Cx = i2c_adapter->cfg->regs;

    DMA_ClearFlag(i2c_adapter->cfg->dma.rx.channel,
                  i2c_adapter->cfg->dma.irq_rx.flags);

    if (i2c_adapter->active_txn++ == i2c_adapter->last_txn) {
#ifdef TRACE_I2C
        vTraceUserEvent(trace_transfer_stop);
#endif /* TRACE_I2C */

        // Send I2C STOP
        I2C_GenerateSTOP(I2Cx, ENABLE);

        if (i2c_adapter->callback) {
            i2c_adapter->callback(i2c_adapter->status, &xHigherPriorityTaskWoken);
        } else {
            // End of transmission of data
            xSemaphoreGiveFromISR(i2c_adapter->sem_ready,
                                  &xHigherPriorityTaskWoken);
        }

#ifdef TRACE_I2C
        vTraceUserEvent(trace_bus_release);
#endif /* TRACE_I2C */
        // I2C adaptor free for next transfer
        xSemaphoreGiveFromISR(i2c_adapter->sem_busy,
                              &xHigherPriorityTaskWoken);
    } else {
#ifdef TRACE_I2C
        vTraceUserEvent(trace_transfer_start);
#endif /* TRACE_I2C */

        // Send I2C (RE)START
        I2C_GenerateSTART(I2Cx, ENABLE);
    }

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISREnd(xHigherPriorityTaskWoken);
#endif
    if (xHigherPriorityTaskWoken == pdTRUE) {
        portYIELD();
    }
}

void PIOS_I2C_TX_IRQ_Handler(uint32_t i2c_id)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
    struct pios_i2c_adapter *i2c_adapter   = (struct pios_i2c_adapter *)i2c_id;

    if (!PIOS_I2C_validate(i2c_adapter)) {
        return;
    }

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISRBegin(i2c_adapter->cfg->dma.irq_tx.init.NVIC_IRQChannel);
#endif

    I2C_TypeDef *I2Cx = i2c_adapter->cfg->regs;

    DMA_ClearFlag(i2c_adapter->cfg->dma.tx.channel,
                  i2c_adapter->cfg->dma.irq_tx.flags);

    DMA_Cmd(i2c_adapter->cfg->dma.tx.channel, DISABLE);

    // TODO Nasty in an ISR!
    while (!I2C_GetFlagStatus(I2Cx, I2C_FLAG_BTF)) {
        ;
    }

    if (i2c_adapter->active_txn++ == i2c_adapter->last_txn) {
#ifdef TRACE_I2C
        vTraceUserEvent(trace_transfer_stop);
#endif /* TRACE_I2C */

        // Send I2C STOP
        I2C_GenerateSTOP(I2Cx, ENABLE);

        if (i2c_adapter->callback) {
            i2c_adapter->callback(i2c_adapter->status, &xHigherPriorityTaskWoken);
        } else {
            // End of transmission of data
            xSemaphoreGiveFromISR(i2c_adapter->sem_ready,
                                  &xHigherPriorityTaskWoken);
        }

#ifdef TRACE_I2C
        vTraceUserEvent(trace_bus_release);
#endif /* TRACE_I2C */
        // I2C adaptor free for next transfer
        xSemaphoreGiveFromISR(i2c_adapter->sem_busy,
                              &xHigherPriorityTaskWoken);
    } else {
#ifdef TRACE_I2C
        vTraceUserEvent(trace_transfer_start);
#endif /* TRACE_I2C */

        // Send I2C (RE)START
        I2C_GenerateSTART(I2Cx, ENABLE);
    }

#if configUSE_TRACE_FACILITY && INCLUDE_ISR_TRACING
    vTraceStoreISREnd(xHigherPriorityTaskWoken);
#endif
    if (xHigherPriorityTaskWoken == pdTRUE) {
        portYIELD();
    }
}
#endif /* PIOS_INCLUDE_I2C */

/**
 * @}
 * @}
 */
