/**
 ******************************************************************************
 *
 * @file       pios_soft_uart.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @brief      A software UART implementation
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * @attention
 *
 * <h2><center>&copy; COPYRIGHT(c) 2015 STMicroelectronics</center></h2>
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */

#ifndef PIOS_SOFT_UART_H
#define PIOS_SOFT_UART_H

typedef struct {
    uint32_t BaudRate;
    uint32_t WordLength;
    uint32_t StopBits;
    uint32_t Parity;
    uint32_t Mode;
} SOFT_UART_InitTypeDef;


typedef enum {
    SOFT_UART_STATE_RESET = 0x00, /*!< Peripheral is not yet Initialized */
    SOFT_UART_STATE_READY, /*!< Peripheral Initialized and ready for use */
    SOFT_UART_STATE_BUSY, /*!< an internal process is ongoing */
    SOFT_UART_STATE_BUSY_TX, /*!< Data Transmission process is ongoing */
    SOFT_UART_STATE_BUSY_RX, /*!< Data Reception process is ongoing */
    SOFT_UART_STATE_BUSY_TX_RX, /*!< Data Transmission and Reception process is ongoing */
    SOFT_UART_STATE_TIMEOUT, /*!< Timeout state */
    SOFT_UART_STATE_ERROR /*!< Error */
} SOFT_UART_StateTypeDef;

#define SOFT_UART_ERROR_NONE    0x00   /*!< No error */
#define SOFT_UART_ERROR_PE      0x01   /*!< Parity error */
#define SOFT_UART_ERROR_NE      0x02   /*!< Noise error */
#define SOFT_UART_ERROR_FE      0x04   /*!< Frame error */
#define SOFT_UART_ERROR_ORE     0x08   /*!< Overrun error */
#define SOFT_UART_ERROR_DMA     0x10   /*!< DMA transfer error */
#define SOFT_UART_ERROR_BUSY    0x20   /*!< Line busy error */

#define SOFT_UART_WORDLENGTH_5B 5
#define SOFT_UART_WORDLENGTH_6B 6
#define SOFT_UART_WORDLENGTH_7B 7
#define SOFT_UART_WORDLENGTH_8B 8
#define SOFT_UART_WORDLENGTH_9B 9

#define SOFT_UART_STOPBITS_1    1
#define SOFT_UART_STOPBITS_2    2

#define SOFT_UART_PARITY_NONE   0
#define SOFT_UART_PARITY_EVEN   1
#define SOFT_UART_PARITY_ODD    2

#define SOFT_UART_MODE_RX       0
#define SOFT_UART_MODE_TX       1
#define SOFT_UART_MODE_TX_RX    2

uint32_t PIOS_SOFT_UART_getError(void);
uint32_t PIOS_SOFT_UART_getState(void);

#endif /* PIOS_SOFT_UART_H */
