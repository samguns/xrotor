/**
 ******************************************************************************
 *
 * @file       pios_soft_uart_priv.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @brief      A software UART implementation
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#ifndef PIOS_SOFT_UART_PRIV_H
#define PIOS_SOFT_UART_PRIV_H

#include <stdint.h>
#include <pios_gpio.h>
#include <pios_stm32.h>
#include <pios_gpio_priv.h>
#include <pios_soft_uart.h>
#include <stm32f4xx.h>
#include <stm32f4xx_tim.h>
#include <stm32f4xx_dma.h>
#include <stm32f4xx_exti.h>

#define SOFT_UART_BAUDRATE_4800                4800
#define SOFT_UART_BAUDRATE_9600                9600
#define SOFT_UART_BAUDRATE_19200               19200
#define SOFT_UART_BAUDRATE_38400               38400
#define SOFT_UART_BAUDRATE_57600               57600
#define SOFT_UART_BAUDRATE_115200              115200

#define PIOS_SOFT_UART_OUTPUT_DEFAULT_BAUDRATE SOFT_UART_BAUDRATE_19200
#define PIOS_SOFT_UART_BUFFER_SIZE             260

struct pios_soft_uart_cfg {
    SOFT_UART_InitTypeDef   uartInit;

    TIM_TypeDef *timer;
    TIM_TimeBaseInitTypeDef timerInit;

    DMA_Stream_TypeDef      *txDmaStream;
    DMA_InitTypeDef txDmaInit;

    DMA_Stream_TypeDef      *rxDmaStream;
    DMA_InitTypeDef  rxDmaInit;

    struct stm32_irq txDmaIrq;
    struct stm32_irq rxDmaIrq;
};

extern const struct pios_com_driver pios_soft_uart_com_driver;

bool PIOS_SOFT_UART_extiIRQHandler(void);
void PIOS_SOFT_UART_txDMAIRQHandler(void);
void PIOS_SOFT_UART_rxDMAIRQHandler(void);
uint32_t PIOS_SOFT_UART_Init(uint32_t *dev_id,
                             const struct pios_soft_uart_cfg *soft_uart_cfg);

#endif /* PIOS_SOFT_UART_PRIV_H */

/**
 * @}
 * @}
 */
