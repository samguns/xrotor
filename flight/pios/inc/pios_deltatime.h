/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core hardware abstraction layer
 * @{
 * @addtogroup PIOS_DELTATIME time measurement Functions
 * @brief PiOS Delay functionality
 * @{
 *
 * @file       pios_deltatime.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      Settings functions header
 *
 *****************************************************************************/
#ifndef PIOS_DELTATIME_H
#define PIOS_DELTATIME_H

#if defined(__cplusplus)
extern "C" {
#endif

#include <pios_delay.h>
#include <stdint.h>

struct PiOSDeltatimeConfigStruct {
    uint32_t last;
    float    average;
    float    min;
    float    max;
    float    alpha;
    float    dT;
};
typedef struct PiOSDeltatimeConfigStruct PiOSDeltatimeConfig;

/* Public Functions */
void PIOS_DELTATIME_Init(PiOSDeltatimeConfig *config, float average, float min, float max, float alpha);

float PIOS_DELTATIME_GetAverageSeconds(PiOSDeltatimeConfig *config);
float PIOS_DELTATIME_GetActualSeconds(PiOSDeltatimeConfig *config);

inline float PIOS_DELTATIME(PiOSDeltatimeConfig *config, uint32_t cur_time)
{
    PIOS_DEBUG_Assert(config);
    uint32_t dTick;
    if (cur_time > config->last) {
        dTick = cur_time - config->last;
    } else {
        dTick = UINT32_MAX - config->last + cur_time;
    }
    config->last = cur_time;
    config->dT   = PIOS_DELAY_ConvertRaw2uSecs(dTick) * 1.0e-6f;

    if (config->dT < config->min) {
        config->dT = config->min;
    }
    if (config->dT > config->max) {
        config->dT = config->max;
    }
    config->average = config->average * (1.0f - config->alpha) + config->dT * config->alpha;
    return config->dT;
}

#if defined(__cplusplus)
}
#endif

#endif /* PIOS_DELTATIME_H */

/**
 * @}
 * @}
 */
