/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core API
 * @{
 * @addtogroup PIOS_SYS System Functions
 * @brief PIOS common error return codes
 * @{
 *
 * @file       pios_errno.h
 * @author     The OpenPilot Next Generation Team, http://www.opng.org Copyright (C) 2015
 * @brief      Common error return codes
 * @see        The GNU Public License (GPL) Version 3
 *
 ******************************************************************************
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef PIOS_ERRNO_H
#define PIOS_ERRNO_H

/* Where referenced, FreeRTOS definitions come from:
 *    flight/pios/common/libraries/FreeRTOS/Source/include/projdefs.h
 */
#define STATUS_OK      0       // Success

// Aligns with FreeRTOS errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY
#define STATUS_NOMEM   -1      // Insufficient memory
#define STATUS_ERROR   -2      // Unspecified error (avoid)
#define STATUS_RETRY   -3      // Operation should be retried
#define STATUS_BLOCKED -4 // Aligns with FreeRTOS errQUEUE_BLOCKED
#define STATUS_YIELD   -5      // Aligns with FreeRTOS errQUEUE_YIELD
#define STATUS_BUSY    -6      // Operation could not be completed immediately
#define STATUS_TIMEOUT -7 // Operation timed out
#define STATUS_INVAL   -8      // Invalid parameter
#define STATUS_NODEV   -9      // No such device
#define STATUS_BADDEV  -10     // Bad device structure
#define STATUS_FAULT   -11     // Bad address
#define STATUS_BERR    -12     // Bus error
#define STATUS_DEVERR  -13     // Device error
#define STATUS_CRC     -14     // CRC error
#define STATUS_LENGTH  -15     // Bad length

#endif /* PIOS_ERRNO_H */
