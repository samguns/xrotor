/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core hardware abstraction layer
 * @{
 * @addtogroup PIOS_MPU9250 MPU9250 Functions
 * @brief Deals with the hardware interface to the 9 DOF sensor.
 * @{
 *
 * @file       pios_mp9250.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2014.
 * @brief      MPU9250 9-axis gyro, accel and mag chip
 * @see        The GNU Public License (GPL) Version 3
 *
 ******************************************************************************
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pios.h"
#include "openpilot.h"
#include <pios_mpu9250.h>
#ifdef PIOS_INCLUDE_MPU9250
#include <stdint.h>
#include <pios_constants.h>
#include <pios_sensors.h>
#include "pios_mpu9250_config.h"
/* Global Variables */

#ifdef TRACE_GYRO_TIMING
traceLabel gyro_strobe;
#endif /* TRACE_GYRO_TIMING */

const uint32_t mpu9250_sample_freq[] = {
#ifdef PIOS_MPU9250_MAG
    4000, // PIOS_MPU9250_LOWPASS_250_HZ
#else /* PIOS_MPU9250_MAG */
    8000, // PIOS_MPU9250_LOWPASS_250_HZ
#endif /* PIOS_MPU9250_MAG */
    1000, // PIOS_MPU9250_LOWPASS_184_HZ
    1000, // PIOS_MPU9250_LOWPASS_92_HZ
    1000, // PIOS_MPU9250_LOWPASS_41_HZ
    1000, // PIOS_MPU9250_LOWPASS_20_HZ
    1000, // PIOS_MPU9250_LOWPASS_10_HZ
    1000, // PIOS_MPU9250_LOWPASS_5_HZ
#ifdef PIOS_MPU9250_MAG
    4000, // PIOS_MPU9250_LOWPASS_3600_HZ
#else /* PIOS_MPU9250_MAG */
    8000, // PIOS_MPU9250_LOWPASS_3600_HZ
#endif /* PIOS_MPU9250_MAG */
};

enum pios_mpu9250_dev_magic {
    PIOS_MPU9250_DEV_MAGIC = 0x9da9b3ed,
};

struct mpu9250_dev {
    uint32_t spi_id;
    uint32_t slave_num;
    QueueHandle_t queue;
    xSemaphoreHandle strobe;
    uint32_t strobe_rate;
    uint32_t isr_count;
    const struct pios_mpu9250_cfg  *cfg;
    enum pios_mpu9250_range gyro_range;
    enum pios_mpu9250_accel_range  accel_range;
    enum pios_mpu9250_filter filter;
    enum pios_mpu9250_accel_filter accel_filter;
    enum pios_mpu9250_dev_magic    magic;
    float mag_sens_adj[PIOS_MPU9250_MAG_ASA_NB_BYTE];
};

#ifdef PIOS_MPU9250_ACCEL
#define PIOS_MPU9250_ACCEL_SAMPLES_BYTES (6)
#else
#define PIOS_MPU9250_ACCEL_SAMPLES_BYTES (0)
#endif

#ifdef PIOS_MPU9250_MAG
#define PIOS_MPU9250_MAG_SAMPLES_BYTES   (8)
#else
#define PIOS_MPU9250_MAG_SAMPLES_BYTES   (0)
#endif

#define PIOS_MPU9250_GYRO_SAMPLES_BYTES  (6)
#define PIOS_MPU9250_TEMP_SAMPLES_BYTES  (2)

#define PIOS_MPU9250_SAMPLES_BYTES \
    (PIOS_MPU9250_ACCEL_SAMPLES_BYTES + \
     PIOS_MPU9250_GYRO_SAMPLES_BYTES + \
     PIOS_MPU9250_TEMP_SAMPLES_BYTES + \
     PIOS_MPU9250_MAG_SAMPLES_BYTES)

#ifdef PIOS_MPU9250_ACCEL
#define PIOS_MPU9250_SENSOR_FIRST_REG    PIOS_MPU9250_ACCEL_X_OUT_MSB
#else
#define PIOS_MPU9250_SENSOR_FIRST_REG    PIOS_MPU9250_TEMP_OUT_MSB
#endif

#if defined(PIOS_MPU9250_MAG) && !defined(PIOS_MPU9250_ACCEL)
#error ERROR: PIOS_MPU9250_ACCEL not defined! THIS CONFIGURATION IS NOT SUPPORTED
#endif

typedef union {
    uint8_t buffer[2 + PIOS_MPU9250_SAMPLES_BYTES];
    struct {
        uint8_t dummy;
#ifdef PIOS_MPU9250_ACCEL
        uint8_t Accel_X_h;
        uint8_t Accel_X_l;
        uint8_t Accel_Y_h;
        uint8_t Accel_Y_l;
        uint8_t Accel_Z_h;
        uint8_t Accel_Z_l;
#endif
        uint8_t Temperature_h;
        uint8_t Temperature_l;
        uint8_t Gyro_X_h;
        uint8_t Gyro_X_l;
        uint8_t Gyro_Y_h;
        uint8_t Gyro_Y_l;
        uint8_t Gyro_Z_h;
        uint8_t Gyro_Z_l;
#ifdef PIOS_MPU9250_MAG
        uint8_t st1;
        uint8_t Mag_X_l;
        uint8_t Mag_X_h;
        uint8_t Mag_Y_l;
        uint8_t Mag_Y_h;
        uint8_t Mag_Z_l;
        uint8_t Mag_Z_h;
        uint8_t st2;
#endif
    } data;
} __attribute__((__packed__)) mpu9250_data_t;

#define GET_SENSOR_DATA(mpudataptr, sensor) (mpudataptr.data.sensor##_h << 8 | mpudataptr.data.sensor##_l)

static PIOS_SENSORS_3Axis_SensorsWithTemp *queue_data = 0;
static PIOS_SENSORS_3Axis_SensorsWithTemp *mag_data   = 0;
static volatile bool mag_ready = false;
#define SENSOR_COUNT         2
#define SENSOR_DATA_SIZE     (sizeof(PIOS_SENSORS_3Axis_SensorsWithTemp) + sizeof(Vector3i16_s) * SENSOR_COUNT)
#define MAG_SENSOR_DATA_SIZE (sizeof(PIOS_SENSORS_3Axis_SensorsWithTemp) + sizeof(Vector3i16_s))
// ! Global structure for this device device
static struct mpu9250_dev *dev;
volatile bool mpu9250_configured = false;
static mpu9250_data_t mpu9250_data;

// ! Private functions
static struct mpu9250_dev *PIOS_MPU9250_alloc(const struct pios_mpu9250_cfg *cfg);
static int32_t PIOS_MPU9250_Validate(struct mpu9250_dev *dev);
static int32_t PIOS_MPU9250_Config(struct pios_mpu9250_cfg const *cfg);
static int32_t PIOS_MPU9250_SetReg(uint8_t address, uint8_t data);
static int32_t PIOS_MPU9250_GetReg(uint8_t address, uint8_t *data);
static void PIOS_MPU9250_SetSpeed(const bool fast);
static bool PIOS_MPU9250_HandleData();
static bool PIOS_MPU9250_ReadSensor(bool *woken);
static int32_t PIOS_MPU9250_Test(void);
#if defined(PIOS_MPU9250_MAG)
static int32_t PIOS_MPU9250_Mag_Test(void);
static int32_t PIOS_MPU9250_Mag_Init(void);
#endif
static int32_t PIOS_MPU9250_DummyReadGyros(void);


/* Driver Framework interfaces */
// Gyro/accel interface
int32_t PIOS_MPU9250_Main_driver_Test(uintptr_t context, float *factorySTAccel, float *factorySTGyro);
int32_t PIOS_MPU9250_Main_driver_Reconfigure(__attribute__((unused)) uintptr_t context);
int32_t PIOS_MPU9250_Main_driver_Reset(uintptr_t context);
void PIOS_MPU9250_Main_driver_get_scale(float *scales, uint8_t size, uintptr_t context);
QueueHandle_t PIOS_MPU9250_Main_driver_get_queue(uintptr_t context);
void PIOS_MPU9250_Main_driver_Wait(uintptr_t context);

const PIOS_SENSORS_Driver PIOS_MPU9250_Main_Driver = {
    .test        = PIOS_MPU9250_Main_driver_Test,
    .poll        = NULL,
    .fetch       = NULL,
    .reset       = PIOS_MPU9250_Main_driver_Reset,
    .get_queue   = PIOS_MPU9250_Main_driver_get_queue,
    .get_scale   = PIOS_MPU9250_Main_driver_get_scale,
    .wait        = PIOS_MPU9250_Main_driver_Wait,
    .reconfigure = PIOS_MPU9250_Main_driver_Reconfigure,
    .is_polled   = false,
};

// mag sensor interface
int32_t PIOS_MPU9250_Mag_driver_Test(uintptr_t context, float *, float *);
int32_t PIOS_MPU9250_Mag_driver_Reset(uintptr_t context);
void PIOS_MPU9250_Mag_driver_get_scale(float *scales, uint8_t size, uintptr_t context);
void PIOS_MPU9250_Mag_driver_fetch(void *, uint8_t size, uintptr_t context);
bool PIOS_MPU9250_Mag_driver_poll(uintptr_t context);

const PIOS_SENSORS_Driver PIOS_MPU9250_Mag_Driver = {
    .test        = PIOS_MPU9250_Mag_driver_Test,
    .poll        = PIOS_MPU9250_Mag_driver_poll,
    .fetch       = PIOS_MPU9250_Mag_driver_fetch,
    .reset       = PIOS_MPU9250_Mag_driver_Reset,
    .get_queue   = NULL,
    .get_scale   = PIOS_MPU9250_Mag_driver_get_scale,
    .reconfigure = NULL,
    .is_polled   = true,
};

void PIOS_MPU9250_MainRegister()
{
    PIOS_SENSORS_Register(&PIOS_MPU9250_Main_Driver, PIOS_SENSORS_TYPE_3AXIS_GYRO_ACCEL, 0);
}

void PIOS_MPU9250_MagRegister()
{
#ifdef PIOS_MPU9250_MAG
    PIOS_SENSORS_Register(&PIOS_MPU9250_Mag_Driver, PIOS_SENSORS_TYPE_3AXIS_MAG, 0);
#endif /* PIOS_MPU9250_MAG */
}
/**
 * @brief Allocate a new device
 */
static struct mpu9250_dev *PIOS_MPU9250_alloc(const struct pios_mpu9250_cfg *cfg)
{
    struct mpu9250_dev *mpu9250_dev;

    mpu9250_dev = (struct mpu9250_dev *)pios_malloc(sizeof(*mpu9250_dev));
    PIOS_Assert(mpu9250_dev);

    mpu9250_dev->magic       = PIOS_MPU9250_DEV_MAGIC;

    mpu9250_dev->queue       = xQueueCreate(cfg->max_downsample + 1, SENSOR_DATA_SIZE);
    PIOS_Assert(mpu9250_dev->queue);

    mpu9250_dev->strobe      = xSemaphoreCreateBinary();
    mpu9250_dev->strobe_rate = 8; // Sensible initial value until properly configured
    mpu9250_dev->isr_count   = mpu9250_dev->strobe_rate;

    PIOS_Assert(mpu9250_dev->strobe);

    queue_data = (PIOS_SENSORS_3Axis_SensorsWithTemp *)pios_malloc(SENSOR_DATA_SIZE);
    PIOS_Assert(queue_data);

    queue_data->count     = SENSOR_COUNT;

    /* Ensure the timestamp is zeroed so it can be used as a check for
     * interrupts running.
     */
    queue_data->timestamp = 0;

    mag_data = (PIOS_SENSORS_3Axis_SensorsWithTemp *)pios_malloc(MAG_SENSOR_DATA_SIZE);
    mag_data->count = 1;
    PIOS_Assert(mag_data);

#ifdef TRACE_GYRO_TIMING
    gyro_strobe = xTraceOpenLabel("Gyro Strobe");
#endif /* TRACE_GYRO_TIMING */

    return mpu9250_dev;
}

/**
 * @brief Validate the handle to the spi device
 * @returns STATUS_OK for valid device or STATUS_ERROR otherwise
 */
static int32_t PIOS_MPU9250_Validate(struct mpu9250_dev *vdev)
{
    if (vdev == NULL) {
        return STATUS_INVAL;
    }
    if (vdev->magic != PIOS_MPU9250_DEV_MAGIC) {
        return STATUS_BADDEV;
    }
    if (vdev->spi_id == 0) {
        return STATUS_NODEV;
    }
    return STATUS_OK;
}

/**
 * @brief Initialize the MPU9250 3-axis gyro sensor.
 * @return STATUS_OK for success, STATUS_ERROR1 for failure
 */
int32_t PIOS_MPU9250_Init(uint32_t spi_id, uint32_t slave_num, const struct pios_mpu9250_cfg *cfg)
{
    int32_t status;

    dev = PIOS_MPU9250_alloc(cfg);
    if (dev == NULL) {
        return STATUS_NOMEM;
    }

    dev->spi_id    = spi_id;
    dev->slave_num = slave_num;
    dev->cfg = cfg;

    /* Configure the MPU9250 Sensor */
    if ((status = PIOS_MPU9250_Config(cfg))) {
        return status;
    }

    /* Set up EXTI line */
    PIOS_EXTI_Init(cfg->exti_cfg);

    // Perform an SPI read from the gyro to clear any current interrupt status and trigger another interrupt
    if ((status = PIOS_MPU9250_DummyReadGyros())) {
        return status;
    }

    // Allow time for the device to interrupt
    PIOS_DELAY_WaitmS(10);


    // Report a failure if there was no interrupt
    if (queue_data->timestamp == 0) {
        return STATUS_DEVERR;
    }

    return STATUS_OK;
}

/**
 * @brief Initialize the MPU9250 3-axis gyro sensor
 * \return none
 * \param[in] PIOS_MPU9250_ConfigTypeDef struct to be used to configure sensor.
 *
 */
static int32_t PIOS_MPU9250_Config(struct pios_mpu9250_cfg const *cfg)
{
    int32_t status;
    uint8_t power;
    uint8_t data;

    // When debugging need to ensure any internal I2C accesses are stopped before hitting reset
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV0_CTRL, 0x00))) {
        // Failure
        return status;
    }

    // Allow time for the I2C accesses to terminate
    PIOS_DELAY_WaitmS(1);

    if ((status = PIOS_MPU9250_Test())) {
        // Failure
        return status;
    }

    // Reset chip
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_PWR_MGMT_REG, PIOS_MPU9250_PWRMGMT_IMU_RST))) {
        // Failure
        return status;
    }

    // Allow time for the device to reset
    PIOS_DELAY_WaitmS(10);

    // Reset I2C, sensors and FIFO
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_USER_CTRL_REG,
                                      PIOS_MPU9250_USERCTL_DIS_I2C |
                                      PIOS_MPU9250_USERCTL_I2C_MST_RST |
                                      PIOS_MPU9250_USERCTL_SIG_COND))) {
        // Failure
        return status;
    }

    // Allow time for functions to reset
    PIOS_DELAY_WaitmS(10);

    // Power management and user configuration
    // By default, do not include accelerometer and external sense data.
#if defined(PIOS_MPU9250_ACCEL)
    power = 0;
#else
    power = PIOS_MPU9250_PWRMGMT2_DISABLE_ACCEL;
#endif
    if (((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_PWR_MGMT_REG, cfg->Pwr_mgmt_clk)) ||
         (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_USER_CTRL_REG, cfg->User_ctl)) ||
         (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_FIFO_EN_REG, cfg->Fifo_store)) ||
         (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_PWR_MGMT2_REG, power)))) {
        // Failure
        return status;
    }

#if defined(PIOS_MPU9250_ACCEL)
    if ((status = PIOS_MPU9250_ConfigureRanges(cfg->gyro_range,
                                               cfg->accel_range,
                                               cfg->filter,
                                               cfg->accel_filter))) {
        // Failure
        return status;
    }
#endif

    // Interrupt configuration
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_INT_CFG_REG, cfg->interrupt_cfg))) {
        // Failure
        return status;
    }

#ifdef PIOS_MPU9250_MAG
    if ((status = PIOS_MPU9250_Mag_Init())) {
        // Failure
        return status;
    }
#endif

    // Interrupt enable
    if (((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_INT_EN_REG, cfg->interrupt_en)) ||
         (status = PIOS_MPU9250_GetReg(PIOS_MPU9250_INT_EN_REG, &data)))) {
        // Failure
        return status;
    }

    if (data != cfg->interrupt_en) {
        return STATUS_DEVERR;
    }

    if ((status = PIOS_MPU9250_GetReg(PIOS_MPU9250_INT_STATUS_REG, &data))) {
        // Failure
        return status;
    }

    mpu9250_configured = true;

    return STATUS_OK;
}


/**
 * @brief Configures Gyro, accel and Filter ranges/setings
 * @return STATUS_OK if successful, STATUS_ERROR if device has not been initialized
 */
int32_t PIOS_MPU9250_ConfigureRanges(
    enum pios_mpu9250_range gyroRange,
    enum pios_mpu9250_accel_range accelRange,
    enum pios_mpu9250_filter filterSetting,
    enum pios_mpu9250_accel_filter accelFilterSetting)
{
    uint32_t status;

    if (dev == NULL) {
        return STATUS_INVAL;
    }

    // Determine the number of samples between timing strobe signals to the sensor task
    dev->strobe_rate = mpu9250_sample_freq[filterSetting] / PIOS_SILA_RATE;

    // update filter settings
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_DLPF_CFG_REG, filterSetting)) ||
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_ACCEL_CFG2_REG, accelFilterSetting)) ||
        // Gyro range
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_GYRO_CFG_REG, gyroRange)) ||
        // Sample rate divider, chosen upon digital filtering settings
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_SMPLRT_DIV_REG,
                                      ((filterSetting == PIOS_MPU9250_LOWPASS_250_HZ) ||
                                       (filterSetting == PIOS_MPU9250_LOWPASS_3600_HZ)) ?
                                      dev->cfg->Smpl_rate_div_no_dlp : dev->cfg->Smpl_rate_div_dlp))) {
        // Failure
        return status;
    }


    dev->filter       = filterSetting;
    dev->accel_filter = accelFilterSetting;
    dev->gyro_range   = gyroRange;
    dev->accel_range  = accelRange;

#if defined(PIOS_MPU9250_ACCEL)
    // Set the accel range
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_ACCEL_CFG_REG, accelRange))) {
        return status;
    }
#endif

    return STATUS_OK;
}

/**
 * @brief Claim the SPI bus for the accel communications and select this chip
 * @return STATUS_OK if successful, STATUS_BADDEV for invalid device, STATUS_BERR if unable to claim bus
 */
static int32_t PIOS_MPU9250_ClaimBus(bool fast_spi)
{
    if (PIOS_MPU9250_Validate(dev) != 0) {
        return STATUS_BADDEV;
    }
    if (PIOS_SPI_ClaimBus(dev->spi_id) != 0) {
        return STATUS_BERR;
    }
    PIOS_MPU9250_SetSpeed(fast_spi);
    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 0);

    return STATUS_OK;
}


static void PIOS_MPU9250_SetSpeed(const bool fast)
{
    static bool first_set  = true;
    static bool last_speed = false;

    if (first_set || (fast != last_speed)) {
        if (fast) {
            PIOS_SPI_SetClockSpeed(dev->spi_id, dev->cfg->fast_prescaler);
        } else {
            PIOS_SPI_SetClockSpeed(dev->spi_id, dev->cfg->std_prescaler);
        }
        first_set  = false;
        last_speed = fast;
    }
}

/**
 * @brief Claim the SPI bus for the accel communications and select this chip
 * @return STATUS_OK if successful, STATUS_BADDEV for invalid device, STATUS_BERR if unable to claim bus
 * @param woken[in,out] If non-NULL, will be set to true if woken was false and a higher priority
 *                      task has is now eligible to run, else unchanged
 */
static int32_t PIOS_MPU9250_ClaimBusISR(bool *woken, bool fast_spi)
{
    if (PIOS_MPU9250_Validate(dev) != 0) {
        return STATUS_BADDEV;
    }
    if (PIOS_SPI_ClaimBusISR(dev->spi_id, woken) != 0) {
        return STATUS_BERR;
    }

    PIOS_MPU9250_SetSpeed(fast_spi);
    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 0);

    return STATUS_OK;
}

/**
 * @brief Release the SPI bus for the accel communications and end the transaction
 * @return STATUS_OK if successful
 */
static int32_t PIOS_MPU9250_ReleaseBus()
{
    if (PIOS_MPU9250_Validate(dev) != 0) {
        return STATUS_ERROR;
    }

    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 1);
    return PIOS_SPI_ReleaseBus(dev->spi_id);
}

/**
 * @brief Release the SPI bus for the accel communications and end the transaction
 * @return STATUS_OK if successful
 * @param woken[in,out] If non-NULL, will be set to true if woken was false and a higher priority
 *                      task has is now eligible to run, else unchanged
 */
static int32_t PIOS_MPU9250_ReleaseBusISR(bool *woken)
{
    if (PIOS_MPU9250_Validate(dev) != 0) {
        return STATUS_BADDEV;
    }
    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 1);

    return PIOS_SPI_ReleaseBusISR(dev->spi_id, woken);
}

/**
 * @brief Read a register from MPU9250
 * @returns The register value or STATUS_BERR if failure to get bus
 * @param reg[in] Register address to be read
 */
static int32_t PIOS_MPU9250_GetReg(uint8_t reg, uint8_t *data)
{
    if (PIOS_MPU9250_ClaimBus(false) != 0) {
        return STATUS_BERR;
    }

    PIOS_SPI_TransferByte(dev->spi_id, (0x80 | reg)); // request byte
    *data = PIOS_SPI_TransferByte(dev->spi_id, 0); // receive response

    PIOS_MPU9250_ReleaseBus();

    // Allow time for the device to recover
    // Ocassional odd behaviour without this delay
    PIOS_DELAY_WaituS(10);

    return STATUS_OK;
}

/**
 * @brief Writes one byte to the MPU9250
 * \param[in] reg Register address
 * \param[in] data Byte to write
 * \return 0 if operation was successful
 * \return STATUS_BERR if unable to claim SPI bus
 * \return STATUS_DEVERR if unable to send the command
 * \return STATUS_DEVERR if unable to receive the response
 */
static int32_t PIOS_MPU9250_SetReg(uint8_t reg, uint8_t data)
{
    if (PIOS_MPU9250_ClaimBus(false) != 0) {
        return STATUS_ERROR;
    }

    PIOS_SPI_TransferByte(dev->spi_id, 0x7f & reg);
    PIOS_SPI_TransferByte(dev->spi_id, data);

    PIOS_MPU9250_ReleaseBus();

    // Allow time for the device to recover
    PIOS_DELAY_WaitmS(1);

    return STATUS_OK;
}


/**
 * @brief Perform a dummy read in order to restart interrupt generation
 * \returns STATUS_OK if successful
 */
static int32_t PIOS_MPU9250_DummyReadGyros()
{
    int32_t status;
    uint8_t mpu9250_send_buf[1 + PIOS_MPU9250_SAMPLES_BYTES] = { (PIOS_MPU9250_SENSOR_FIRST_REG) | 0x80 };

    if ((status = PIOS_MPU9250_ClaimBus(true))) {
        // Failure
        return status;
    }

    if ((status = PIOS_SPI_TransferBlock(dev->spi_id, &mpu9250_send_buf[0], &mpu9250_data.buffer[0], sizeof(mpu9250_data_t), NULL))) {
        // Failure
        return false;
    }

    PIOS_MPU9250_ReleaseBus();

    return STATUS_OK;
}


/*
 * @brief Read the identification bytes from the MPU9250 sensor
 * \return ID read from MPU9250 or STATUS_ERROR if failure
 */
int32_t PIOS_MPU9250_ReadID(uint8_t *mpu9250_id)
{
    return PIOS_MPU9250_GetReg(PIOS_MPU9250_WHOAMI, mpu9250_id);
}

static float PIOS_MPU9250_GetScale()
{
    switch (dev->gyro_range) {
    case PIOS_MPU9250_SCALE_250_DEG:
    case PIOS_MPU9250_SCALE_250_TST:
        return 250.0f / 32768.0f;

    case PIOS_MPU9250_SCALE_500_DEG:
        return 500.0f / 32768.0f;

    case PIOS_MPU9250_SCALE_1000_DEG:
        return 1000.0f / 32768.0f;

    case PIOS_MPU9250_SCALE_2000_DEG:
    default:
        return 2000.0f / 32768.0f;
    }
}

static float PIOS_MPU9250_GetAccelScale()
{
    switch (dev->accel_range) {
    case PIOS_MPU9250_ACCEL_2G:
    case PIOS_MPU9250_ACCEL_2G_TST:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 16384.0f;

    case PIOS_MPU9250_ACCEL_4G:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 8192.0f;

    case PIOS_MPU9250_ACCEL_8G:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 4096.0f;

    case PIOS_MPU9250_ACCEL_16G:
    default:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 2048.0f;
    }
}

/**
 * @brief Run self-test operation.
 * \return 0 if test succeeded
 * \return non-zero value if test failed
 */
static int32_t PIOS_MPU9250_Test(void)
{
    int32_t status;
    /* Verify that ID matches */
    uint8_t mpu9250_id;

    if ((status = PIOS_MPU9250_ReadID(&mpu9250_id))) {
        // Failure
        return status;
    }

    if (mpu9250_id != PIOS_MPU9250_GYRO_ACC_ID) {
        return STATUS_DEVERR;
    }

    return STATUS_OK;
}

#if defined(PIOS_MPU9250_MAG)
/**
 * @brief Read a mag register from MPU9250
 * @returns The register value or STATUS_ERROR if failure to get bus
 * @param reg[in] Register address to be read
 */
static int32_t PIOS_MPU9250_Mag_GetReg(uint8_t reg, uint8_t *data)
{
    int32_t status;

    if ( // Set the I2C slave address and read command.
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_ADDR,
                                      PIOS_MPU9250_MAG_I2C_ADDR | PIOS_MPU9250_MAG_I2C_READ_FLAG)) ||
        // Set the address of the register to read.
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_REG, reg)) ||
        // Trigger the byte transfer.
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_CTRL, PIOS_MPU9250_I2C_SLV_ENABLE))) {
        // Failure
        return status;
    }

    // Wait for the I2C read to complete
    PIOS_DELAY_WaitmS(1);

    // Read result.
    return PIOS_MPU9250_GetReg(PIOS_MPU9250_I2C_SLV4_DI, data);
}

/**
 * @brief Writes one byte to the MPU9250
 * \param[in] reg Register address
 * \param[in] data Byte to write
 */
static int32_t PIOS_MPU9250_Mag_SetReg(uint8_t reg, uint8_t data)
{
    int32_t status;

    if ( // Set the I2C slave address.
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_ADDR, PIOS_MPU9250_MAG_I2C_ADDR)) ||
        // Set the address of the register to write.
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_REG, reg)) ||
        // Set the byte to write.
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_DO, data)) ||
        // Trigger the byte transfer.
        (status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_CTRL, PIOS_MPU9250_I2C_SLV_ENABLE))) {
        return status;
    }

    return status;
}

/**
 * @rief Get ASAx registers from fuse ROM
 *  Hadj = H*((ASA-128)*0.5/128+1)
 * \return 0 if test succeeded
 * \return non-zero value if test failed
 */
static int32_t PIOS_MPU9250_Mag_Sensitivity(void)
{
    int32_t status;
    int i;

    /* Put mag in power down state before changing mode */
    if ((status = PIOS_MPU9250_Mag_SetReg(PIOS_MPU9250_CNTL1, PIOS_MPU9250_MAG_POWER_DOWN_MODE))) {
        // Failure
        return status;
    }

    /* Enable fuse ROM for access */
    if ((status = PIOS_MPU9250_Mag_SetReg(PIOS_MPU9250_CNTL1, PIOS_MPU9250_MAG_FUSE_ROM_MODE))) {
        // Failure
        return status;
    }

    if (PIOS_MPU9250_ClaimBus(false) != 0) {
        return STATUS_BERR;
    }

    /* Set addres and read flag */
    PIOS_SPI_TransferByte(dev->spi_id, PIOS_MPU9250_I2C_SLV0_ADDR);
    PIOS_SPI_TransferByte(dev->spi_id, PIOS_MPU9250_MAG_I2C_ADDR | PIOS_MPU9250_MAG_I2C_READ_FLAG);

    /* Set the address of the register to read. */
    PIOS_SPI_TransferByte(dev->spi_id, PIOS_MPU9250_I2C_SLV0_REG);
    PIOS_SPI_TransferByte(dev->spi_id, PIOS_MPU9250_ASAX);

    /* Trigger the byte transfer. */
    PIOS_SPI_TransferByte(dev->spi_id, PIOS_MPU9250_I2C_SLV0_CTRL);
    PIOS_SPI_TransferByte(dev->spi_id, PIOS_MPU9250_I2C_SLV_ENABLE | 0x3);

    PIOS_DELAY_WaitmS(2);

    /* Read the mag data from SPI block */
    for (i = 0; i < 0x3; i++) {
        PIOS_SPI_TransferByte(dev->spi_id, (PIOS_MPU9250_EXT_SENS_DATA_00 | 0x80) + i);
        int8_t ret = PIOS_SPI_TransferByte(dev->spi_id, 0x0);
        if (ret < 0) {
            PIOS_MPU9250_ReleaseBus();
            return STATUS_DEVERR;
        }
        dev->mag_sens_adj[i] = 1.0f; // 1.0f + ((float)((uint8_t)ret - 128)) / 256.0f;
    }

    PIOS_MPU9250_ReleaseBus();


    /* Put mag in power down state before changing mode */
    return PIOS_MPU9250_Mag_SetReg(PIOS_MPU9250_CNTL1, PIOS_MPU9250_MAG_POWER_DOWN_MODE);
}

/**
 * @brief Read a mag register from MPU9250
 * @returns The register value or STATUS_ERROR if failure to get bus
 * @param reg[in] Register address to be read
 */
static int32_t PIOS_MPU9250_Mag_Init(void)
{
    int32_t status;

    // Ensure slave 0 is disabled
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV0_CTRL, 0x00))) {
        // Failure
        return status;
    }

    // I2C multi-master init - STOP/START rather than RESTART
    if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_MST_CTRL, PIOS_MPU9250_I2C_MST_P_NSR | PIOS_MPU9250_I2C_MST_CLOCK_500))) {
        // Failure
        return status;
    }

    // Confirm Mag ID.
    if ((status = PIOS_MPU9250_Mag_Test())) {
        // Failure
        return status;
    }

    // Reset Mag.
    if ((status = PIOS_MPU9250_Mag_SetReg(PIOS_MPU9250_CNTL2, PIOS_MPU9250_MAG_RESET))) {
        // Failure
        return status;
    }

    // read fuse ROM to get the sensitivity adjustment values.
    PIOS_MPU9250_Mag_Sensitivity();

    if ( // Shadow Mag updates
        ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_MST_DELAY_CTRL, PIOS_MPU9250_DELAY_ES_SHADOW))) ||
        // Make sure no other registers will be triggered before entering continuous mode.
        ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV4_CTRL, 0x00))) ||
        ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV0_DO, 0x00))) ||
        // Making sure register are accessible.
        ((status = PIOS_MPU9250_Mag_SetReg(PIOS_MPU9250_CNTL1, PIOS_MPU9250_MAG_OUTPUT_16BITS | PIOS_MPU9250_MAG_CONTINUOUS_MODE2))) ||
        // Get ST1, the 6 mag data and ST2.
        // This is to save 2 SPI access.
        // Set the I2C slave address and read command.
        ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV0_ADDR, PIOS_MPU9250_MAG_I2C_ADDR | PIOS_MPU9250_MAG_I2C_READ_FLAG))) ||
        // Set the address of the register to read.
        ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV0_REG, PIOS_MPU9250_ST1))) ||
        // Trigger the byte transfer.
        ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_I2C_SLV0_CTRL, PIOS_MPU9250_I2C_SLV_ENABLE | PIOS_MPU9250_MAG_SAMPLES_BYTES)))) {
        // Failure
        return status;
    }

    return status;
}

/*
 * @brief Read the mag identification bytes from the MPU9250 sensor
 */
int32_t PIOS_MPU9250_Mag_ReadID(uint8_t *mpu9250_mag_id)
{
    int32_t status;

    if ((status = PIOS_MPU9250_Mag_GetReg(PIOS_MPU9250_WIA, mpu9250_mag_id))) {
        // Failure
        return status;
    }

    return status;
}

/**
 * @brief Run self-test operation.
 * \return 0 if test succeeded
 * \return non-zero value if test failed
 */
static int32_t PIOS_MPU9250_Mag_Test(void)
{
    int32_t status;
    uint8_t mpu9250_mag_id;

    if ((status = PIOS_MPU9250_Mag_ReadID(&mpu9250_mag_id))) {
        // Failure
        return status;
    }

    if (mpu9250_mag_id != PIOS_MPU9250_MAG_ID) {
        // Reset I2C and try again
        if ((status = PIOS_MPU9250_SetReg(PIOS_MPU9250_USER_CTRL_REG,
                                          PIOS_MPU9250_USERCTL_I2C_MST_RST))) {
            // Failure
            return status;
        }
        if ((status = PIOS_MPU9250_Mag_ReadID(&mpu9250_mag_id))) {
            // Failure
            return status;
        }

        if (mpu9250_mag_id != PIOS_MPU9250_MAG_ID) {
            return STATUS_DEVERR;
        }
    }

    return STATUS_OK;
}
#endif /* if defined(PIOS_MPU9250_MAG) */

/**
 * @brief EXTI IRQ Handler.  Read all the data from onboard buffer
 * @return a boolean to the EXTI IRQ Handler wrapper indicating if a
 *         higher priority task is now eligible to run
 */

bool PIOS_MPU9250_IRQHandler(void)
{
#if defined(PIOS_ENABLE_DEBUG_PINS) && defined(DEBUG_GYRO_TIMING)
    PIOS_DEBUG_PinHigh(1);
#endif
    bool woken = false;

    if (!mpu9250_configured) {
        return woken;
    }

    // Timestamp the data
    queue_data->timestamp = PIOS_DELAY_GetRaw();

    bool status = PIOS_MPU9250_ReadSensor(&woken);
    PIOS_Assert(status);

#if defined(PIOS_ENABLE_DEBUG_PINS) && defined(DEBUG_GYRO_TIMING)
    PIOS_DEBUG_PinLow(1);
#endif
    return woken;
}

static bool PIOS_MPU9250_SPICallback(bool *read_ok, __attribute__((unused)) uint8_t crc)
{
    bool woken = false;

    // Transfer is now complete
    PIOS_MPU9250_ReleaseBusISR(&woken);

    if (read_ok) {
        woken |= PIOS_MPU9250_HandleData();
    }

    if (--dev->isr_count == 0) {
        dev->isr_count = dev->strobe_rate;
#if defined(PIOS_ENABLE_DEBUG_PINS) && defined(DEBUG_GYRO_TIMING)
        PIOS_DEBUG_PinHigh(0);
#endif
        xSemaphoreGive(dev->strobe);
#ifdef TRACE_GYRO_TIMING
        vTraceUserEvent(gyro_strobe);
#endif /* TRACE_GYRO_TIMING */
    }

    return woken;
}

static bool PIOS_MPU9250_HandleData()
{
    // Rotate the sensor to OP convention.  The datasheet defines X as towards the right
    // and Y as forward.  OP convention transposes this.  Also the Z is defined negatively
    // to our convention
    if (!queue_data) {
        return false;
    }

#ifdef PIOS_MPU9250_MAG
    bool mag_valid = mpu9250_data.data.st1 & PIOS_MPU9250_MAG_DATA_RDY;
#endif

    // Currently we only support rotations on top so switch X/Y accordingly
    switch (dev->cfg->orientation) {
    case PIOS_MPU9250_TOP_0DEG:
#ifdef PIOS_MPU9250_ACCEL
        queue_data->sample[0].y = GET_SENSOR_DATA(mpu9250_data, Accel_X); // chip X
        queue_data->sample[0].x = GET_SENSOR_DATA(mpu9250_data, Accel_Y); // chip Y
#endif
        queue_data->sample[1].y = GET_SENSOR_DATA(mpu9250_data, Gyro_X); // chip X
        queue_data->sample[1].x = GET_SENSOR_DATA(mpu9250_data, Gyro_Y); // chip Y
#ifdef PIOS_MPU9250_MAG
        if (mag_valid) {
            mag_data->sample[0].y = GET_SENSOR_DATA(mpu9250_data, Mag_Y) * dev->mag_sens_adj[1]; // chip Y
            mag_data->sample[0].x = GET_SENSOR_DATA(mpu9250_data, Mag_X) * dev->mag_sens_adj[0]; // chip X
        }
#endif
        break;
    case PIOS_MPU9250_TOP_90DEG:
        // -1 to bring it back to -32768 +32767 range
#ifdef PIOS_MPU9250_ACCEL
        queue_data->sample[0].y = -1 - (GET_SENSOR_DATA(mpu9250_data, Accel_Y)); // chip Y
        queue_data->sample[0].x = GET_SENSOR_DATA(mpu9250_data, Accel_X); // chip X
#endif
        queue_data->sample[1].y = -1 - (GET_SENSOR_DATA(mpu9250_data, Gyro_Y)); // chip Y
        queue_data->sample[1].x = GET_SENSOR_DATA(mpu9250_data, Gyro_X); // chip X
#ifdef PIOS_MPU9250_MAG
        if (mag_valid) {
            mag_data->sample[0].y = GET_SENSOR_DATA(mpu9250_data, Mag_X) * dev->mag_sens_adj[0]; // chip X
            mag_data->sample[0].x = -1 - (GET_SENSOR_DATA(mpu9250_data, Mag_Y)) * dev->mag_sens_adj[1]; // chip Y
        }

#endif
        break;
    case PIOS_MPU9250_TOP_180DEG:
#ifdef PIOS_MPU9250_ACCEL
        queue_data->sample[0].y = -1 - (GET_SENSOR_DATA(mpu9250_data, Accel_X)); // chip X
        queue_data->sample[0].x = -1 - (GET_SENSOR_DATA(mpu9250_data, Accel_Y)); // chip Y
#endif
        queue_data->sample[1].y = -1 - (GET_SENSOR_DATA(mpu9250_data, Gyro_X)); // chip X
        queue_data->sample[1].x = -1 - (GET_SENSOR_DATA(mpu9250_data, Gyro_Y)); // chip Y
#ifdef PIOS_MPU9250_MAG
        if (mag_valid) {
            mag_data->sample[0].y = -1 - (GET_SENSOR_DATA(mpu9250_data, Mag_Y)) * dev->mag_sens_adj[1]; // chip Y
            mag_data->sample[0].x = -1 - (GET_SENSOR_DATA(mpu9250_data, Mag_X)) * dev->mag_sens_adj[0]; // chip X
        }
#endif
        break;
    case PIOS_MPU9250_TOP_270DEG:
#ifdef PIOS_MPU9250_ACCEL
        queue_data->sample[0].y = GET_SENSOR_DATA(mpu9250_data, Accel_Y); // chip Y
        queue_data->sample[0].x = -1 - (GET_SENSOR_DATA(mpu9250_data, Accel_X)); // chip X
#endif
        queue_data->sample[1].y = GET_SENSOR_DATA(mpu9250_data, Gyro_Y); // chip Y
        queue_data->sample[1].x = -1 - (GET_SENSOR_DATA(mpu9250_data, Gyro_X)); // chip X
#ifdef PIOS_MPU9250_MAG
        if (mag_valid) {
            mag_data->sample[0].y = -1 - (GET_SENSOR_DATA(mpu9250_data, Mag_X)) * dev->mag_sens_adj[0]; // chip X
            mag_data->sample[0].x = GET_SENSOR_DATA(mpu9250_data, Mag_Y) * dev->mag_sens_adj[1]; // chip Y
        }
#endif
        break;
    }
#ifdef PIOS_MPU9250_ACCEL
    queue_data->sample[0].z = -1 - (GET_SENSOR_DATA(mpu9250_data, Accel_Z));
#endif
    queue_data->sample[1].z = -1 - (GET_SENSOR_DATA(mpu9250_data, Gyro_Z));
    const int16_t temp = GET_SENSOR_DATA(mpu9250_data, Temperature);
    queue_data->temperature = 2100 + ((float)(temp - PIOS_MPU9250_TEMP_OFFSET)) * (100.0f / PIOS_MPU9250_TEMP_SENSITIVITY);
    mag_data->temperature   = queue_data->temperature;
#ifdef PIOS_MPU9250_MAG
    if (mag_valid) {
        mag_data->timestamp   = queue_data->timestamp;
        mag_data->sample[0].z = GET_SENSOR_DATA(mpu9250_data, Mag_Z) * dev->mag_sens_adj[2]; // chip Z
        mag_ready = true;
    }
#endif

    BaseType_t higherPriorityTaskWoken;
    xQueueSendToBackFromISR(dev->queue, queue_data, &higherPriorityTaskWoken);
    return higherPriorityTaskWoken == pdTRUE;
}

static bool PIOS_MPU9250_ReadSensor(bool *woken)
{
    // This is being DMAed, so it can't be on the stack
    static uint8_t mpu9250_send_buf[1 + PIOS_MPU9250_SAMPLES_BYTES] = { (PIOS_MPU9250_SENSOR_FIRST_REG) | 0x80 };

    if (PIOS_MPU9250_ClaimBusISR(woken, true) != 0) {
        /* Ideally this should never happen, but if so the bus is still
         * claimed, due to re-calibration accesses. In order to provide
         * the timing strobe to the sensors module task, process the
         * previously read data again.
         */
        *woken |= PIOS_MPU9250_HandleData();

        if (--dev->isr_count == 0) {
            dev->isr_count = dev->strobe_rate;
            /* If called here this strobe will be about 65us earlier than
             * if the SPI read had happened, but that shouldn't be an issue.
             */
            xSemaphoreGive(dev->strobe);
#ifdef TRACE_GYRO_TIMING
            vTraceUserEvent(gyro_strobe);
#endif /* TRACE_GYRO_TIMING */
        }
    }
    // Use a callback to handle the transfer
    else if (PIOS_SPI_TransferBlock(dev->spi_id, &mpu9250_send_buf[0], &mpu9250_data.buffer[0], sizeof(mpu9250_data_t), PIOS_MPU9250_SPICallback) < 0) {
        PIOS_MPU9250_ReleaseBusISR(woken);
        return false;
    }

    return true;
}

// Sensor driver implementation
int32_t PIOS_MPU9250_Main_driver_Test(__attribute__((unused)) uintptr_t context, float *factorySTGyro, float *factorySTAccel)
{
    int32_t status;
    uint8_t gyro_st[3];
    uint8_t accel_st[3];

    if (factorySTAccel != NULL && factorySTGyro != NULL) {
        // Retrieve accelerometer and gyro factory Self-Test Code from USR_Reg
        if (((status = PIOS_MPU9250_GetReg(PIOS_MPU9250_SELF_TEST_X_ACCEL, &accel_st[0]))) || // X-axis accel self-test results
            ((status = PIOS_MPU9250_GetReg(PIOS_MPU9250_SELF_TEST_Y_ACCEL, &accel_st[1]))) || // Y-axis accel self-test results
            ((status = PIOS_MPU9250_GetReg(PIOS_MPU9250_SELF_TEST_Z_ACCEL, &accel_st[2]))) || // Z-axis accel self-test results
            ((status = PIOS_MPU9250_GetReg(PIOS_MPU9250_SELF_TEST_X_GYRO, &gyro_st[0]))) || // X-axis gyro self-test results
            ((status = PIOS_MPU9250_GetReg(PIOS_MPU9250_SELF_TEST_Y_GYRO, &gyro_st[1]))) || // Y-axis gyro self-test results
            ((status = PIOS_MPU9250_GetReg(PIOS_MPU9250_SELF_TEST_Z_GYRO, &gyro_st[2])))) { // Z-axis gyro self-test results
            // Failure
            return status;
        }

        // Retrieve factory self-test value from self-test code reads
        float Accel_X = (float)(2620.0f / 2048.0f) * (powf(1.01f, ((float)accel_st[0] - 1.0f))); // FT[Xa] factory trim calculation
        float Accel_Y = (float)(2620.0f / 2048.0f) * (powf(1.01f, ((float)accel_st[1] - 1.0f))); // FT[Ya] factory trim calculation
        float Accel_Z = (float)(2620.0f / 2048.0f) * (powf(1.01f, ((float)accel_st[2] - 1.0f))); // FT[Za] factory trim calculation
        float Gyro_X  = (float)(2620.0f / 128.0f) * (powf(1.01f, ((float)gyro_st[0] - 1.0f))); // FT[Xg] factory trim calculation
        float Gyro_Y  = (float)(2620.0f / 128.0f) * (powf(1.01f, ((float)gyro_st[1] - 1.0f))); // FT[Yg] factory trim calculation
        float Gyro_Z  = (float)(2620.0f / 128.0f) * (powf(1.01f, ((float)gyro_st[2] - 1.0f))); // FT[Zg] factory trim calculation


        // Currently we only support rotations on top so switch X/Y accordingly
        switch (dev->cfg->orientation) {
        case PIOS_MPU9250_TOP_0DEG:
            factorySTAccel[1] = Accel_X;
            factorySTAccel[0] = Accel_Y;
            factorySTGyro[1]  = Gyro_X;
            factorySTGyro[0]  = Gyro_Y;
            break;
        case PIOS_MPU9250_TOP_90DEG:
            // -1 to bring it back to -32768 +32767 range
            factorySTAccel[1] = -1 - Accel_Y;
            factorySTAccel[0] = Accel_X;
            factorySTGyro[1]  = -1 - Gyro_Y;
            factorySTGyro[0]  = Gyro_X;
            break;
        case PIOS_MPU9250_TOP_180DEG:
            factorySTAccel[1] = -1 - Accel_X; // chip X
            factorySTAccel[0] = -1 - Accel_Y; // chip Y
            factorySTGyro[1]  = -1 - Gyro_X; // chip X
            factorySTGyro[0]  = -1 - Gyro_Y; // chip Y
            break;
        case PIOS_MPU9250_TOP_270DEG:
            factorySTAccel[1] = Accel_Y; // chip Y
            factorySTAccel[0] = -1 - Accel_X; // chip X
            factorySTGyro[1]  = Gyro_Y; // chip Y
            factorySTGyro[0]  = -1 - Gyro_X; // chip X
            break;
        }
        factorySTAccel[2] = -1 - Accel_Z;
        factorySTGyro[2]  = -1 - Gyro_Z;
    }

    return PIOS_MPU9250_Test();
}

int32_t PIOS_MPU9250_Main_driver_Reconfigure(__attribute__((unused)) uintptr_t context)
{
    return PIOS_MPU9250_CONFIG_Configure();
}
void PIOS_MPU9250_Main_driver_Wait(__attribute__((unused)) uintptr_t context)
{
    xSemaphoreTake(dev->strobe, portMAX_DELAY);
}

int32_t PIOS_MPU9250_Main_driver_Reset(__attribute__((unused)) uintptr_t context)
{
    uint8_t data;

    return PIOS_MPU9250_GetReg(PIOS_MPU9250_INT_STATUS_REG, &data);
}

void PIOS_MPU9250_Main_driver_get_scale(float *scales, uint8_t size, __attribute__((unused)) uintptr_t contet)
{
    PIOS_Assert(size >= 2);
    scales[0] = PIOS_MPU9250_GetAccelScale();
    scales[1] = PIOS_MPU9250_GetScale();
}

QueueHandle_t PIOS_MPU9250_Main_driver_get_queue(__attribute__((unused)) uintptr_t context)
{
    return dev->queue;
}


/* PIOS sensor driver implementation */
int32_t PIOS_MPU9250_Mag_driver_Test(__attribute__((unused)) uintptr_t context, __attribute__((unused)) float *unused1, __attribute__((unused)) float *unused2)
{
    return PIOS_MPU9250_Test();
}

int32_t PIOS_MPU9250_Mag_driver_Reset(__attribute__((unused)) uintptr_t context)
{
    return PIOS_MPU9250_DummyReadGyros();
}

void PIOS_MPU9250_Mag_driver_get_scale(float *scales, uint8_t size, __attribute__((unused))  uintptr_t context)
{
    PIOS_Assert(size > 0);
    scales[0] = 1;
}

void PIOS_MPU9250_Mag_driver_fetch(void *data, uint8_t size, __attribute__((unused))  uintptr_t context)
{
    mag_ready = false;
    PIOS_Assert(size > 0);
    memcpy(data, mag_data, MAG_SENSOR_DATA_SIZE);
}

bool PIOS_MPU9250_Mag_driver_poll(__attribute__((unused)) uintptr_t context)
{
    return mag_ready;
}

#endif /* PIOS_INCLUDE_MPU9250 */

/**
 * @}
 * @}
 */
