/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core hardware abstraction layer
 * @{
 * @addtogroup PIOS_USB_DESC USB Descriptor definitions
 * @brief USB Descriptor definitions for CDC
 * @{
 *
 * @file       pios_usb_desc_cdc_only.c
 * @author     The OPNG Team, http://www.opng.org Copyright (C) 2015.
 * @brief      USB Descriptor definitions for CDC
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pios.h"

#ifdef PIOS_INCLUDE_USB

#include "pios_usb_desc_cdc_only_priv.h" /* exported API */
#include "pios_usb_defs.h" /* struct usb_*, USB_* */
#include "pios_usb_board_data.h" /* PIOS_USB_BOARD_* */
#include "pios_usbhook.h" /* PIOS_USBHOOK_Register* */

static const struct usb_device_desc device_desc = {
    .bLength = sizeof(struct usb_device_desc),
    .bDescriptorType    = USB_DESC_TYPE_DEVICE,
    .bcdUSB  = htousbs(0x0200),
    .bDeviceClass    = 0xef,
    .bDeviceSubClass = 0x02,
    .bDeviceProtocol = 0x01,
    .bMaxPacketSize0 = 64, /* Must be 64 for high-speed devices */
    .idVendor      = htousbs(USB_VENDOR_ID_OPENPILOT),
    .idProduct     = htousbs(PIOS_USB_BOARD_PRODUCT_ID),
    .bcdDevice     = htousbs(PIOS_USB_BOARD_DEVICE_VER),
    .iManufacturer = USB_STRING_DESC_VENDOR,
    .iProduct      = USB_STRING_DESC_PRODUCT,
    .iSerialNumber = USB_STRING_DESC_SERIAL,
    .bNumConfigurations = 1,
};

struct usb_config_cdc {
    struct usb_configuration_desc         config;
    struct usb_interface_association_desc iad;
    struct usb_interface_desc             cdc_control_if;
    struct usb_cdc_header_func_desc       cdc_header;
    struct usb_cdc_callmgmt_func_desc     cdc_callmgmt;
    struct usb_cdc_acm_func_desc          cdc_acm;
    struct usb_cdc_union_func_desc        cdc_union;
    struct usb_endpoint_desc cdc_mgmt_in;
    struct usb_interface_desc             cdc_data_if;
    struct usb_endpoint_desc        cdc_in;
    struct usb_endpoint_desc        cdc_out;
#ifdef PIOS_INCLUDE_USB_CDC2
    struct usb_interface_association_desc iad2;
    struct usb_interface_desc       cdc_control_if2;
    struct usb_cdc_header_func_desc cdc_header2;
    struct usb_cdc_callmgmt_func_desc cdc_callmgmt2;
    struct usb_cdc_acm_func_desc    cdc_acm2;
    struct usb_cdc_union_func_desc  cdc_union2;
    struct usb_endpoint_desc        cdc_mgmt_in2;
    struct usb_interface_desc       cdc_data_if2;
    struct usb_endpoint_desc        cdc_in2;
    struct usb_endpoint_desc        cdc_out2;
#endif /* PIOS_INCLUDE_USB_CDC2 */
} __attribute__((packed));

static const struct usb_config_cdc config_cdc = {
    .config                  = {
        .bLength             = sizeof(struct usb_configuration_desc),
        .bDescriptorType     = USB_DESC_TYPE_CONFIGURATION,
        .wTotalLength        = htousbs(sizeof(struct usb_config_cdc)),
#ifdef PIOS_INCLUDE_USB_CDC2
        .bNumInterfaces      = 4,
#else /* PIOS_INCLUDE_USB_CDC2 */
        .bNumInterfaces      = 2,
#endif /* PIOS_INCLUDE_USB_CDC2 */
        .bConfigurationValue = 1,
        .iConfiguration      = 0,
        .bmAttributes        = 0xC0,
        .bMaxPower           = 250 / 2,                               /* in units of 2ma */
    },
    // First CDC interface
    .iad                     = {
        .bLength             = sizeof(struct usb_interface_association_desc),
        .bDescriptorType   = USB_DESC_TYPE_IAD,
        .bFirstInterface   = 0,
        .bInterfaceCount   = 2,
        .bFunctionClass    = USB_INTERFACE_CLASS_CDC, /* Communication */
        .bFunctionSubClass = USB_CDC_DESC_SUBTYPE_ABSTRACT_CTRL, /* Abstract Control Model */
        .bFunctionProtocol = 1, /* V.25ter, Common AT commands */
        .iInterface          = 0,
    },
    .cdc_control_if          = {
        .bLength             = sizeof(struct usb_interface_desc),
        .bDescriptorType    = USB_DESC_TYPE_INTERFACE,
        .bInterfaceNumber   = 0,
        .bAlternateSetting  = 0,
        .bNumEndpoints      = 1,
        .bInterfaceClass    = USB_INTERFACE_CLASS_CDC,
        .bInterfaceSubClass = USB_CDC_DESC_SUBTYPE_ABSTRACT_CTRL, /* Abstract Control Model */
        .nInterfaceProtocol = 1,       /* V.25ter, Common AT commands */
        .iInterface          = 0,
    },
    .cdc_header              = {
        .bLength = sizeof(struct usb_cdc_header_func_desc),
        .bDescriptorType     = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType  = USB_CDC_DESC_SUBTYPE_HEADER,
        .bcdCDC  = htousbs(0x0110),
    },
    .cdc_callmgmt            = {
        .bLength             = sizeof(struct usb_cdc_callmgmt_func_desc),
        .bDescriptorType    = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType = USB_CDC_DESC_SUBTYPE_CALLMGMT,
        .bmCapabilities     = 0x00, /* No call handling */
        .bDataInterface     = 1,
    },
    .cdc_acm                 = {
        .bLength             = sizeof(struct usb_cdc_acm_func_desc),
        .bDescriptorType    = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType = USB_CDC_DESC_SUBTYPE_ABSTRACT_CTRL,
        .bmCapabilities     = 0x02, /* line coding and serial state */
    },
    .cdc_union               = {
        .bLength             = sizeof(struct usb_cdc_union_func_desc),
        .bDescriptorType    = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType = USB_CDC_DESC_SUBTYPE_UNION,
        .bMasterInterface   = 0,
        .bSlaveInterface    = 1,
    },
    .cdc_mgmt_in             = {
        .bLength             = sizeof(struct usb_endpoint_desc),
        .bDescriptorType  = USB_DESC_TYPE_ENDPOINT,
        .bEndpointAddress = USB_EP_IN(USB_CDC1_EP_INT),
        .bmAttributes     = USB_EP_ATTR_TT_INTERRUPT,
        .wMaxPacketSize   = htousbs(PIOS_USB_BOARD_CDC_MGMT_LENGTH),
        .bInterval           = 4,                                      /* ms */
    },
    .cdc_data_if             = {
        .bLength             = sizeof(struct usb_interface_desc),
        .bDescriptorType    = USB_DESC_TYPE_INTERFACE,
        .bInterfaceNumber   = 1,
        .bAlternateSetting  = 0,
        .bNumEndpoints      = 2,
        .bInterfaceClass    = USB_INTERFACE_CLASS_DATA,
        .bInterfaceSubClass = 0,
        .nInterfaceProtocol = 0, /* No class specific protocol */
        .iInterface          = 0,
    },
    .cdc_in                  = {
        .bLength             = sizeof(struct usb_endpoint_desc),
        .bDescriptorType  = USB_DESC_TYPE_ENDPOINT,
        .bEndpointAddress = USB_EP_IN(USB_CDC1_EP_BULK),
        .bmAttributes     = USB_EP_ATTR_TT_BULK,
        .wMaxPacketSize   = htousbs(PIOS_USB_BOARD_CDC_DATA_LENGTH),
        .bInterval           = 0,                                      /* ms */
    },
    .cdc_out                 = {
        .bLength             = sizeof(struct usb_endpoint_desc),
        .bDescriptorType  = USB_DESC_TYPE_ENDPOINT,
        .bEndpointAddress = USB_EP_OUT(USB_CDC1_EP_BULK),
        .bmAttributes     = USB_EP_ATTR_TT_BULK,      /* Bulk */
        .wMaxPacketSize   = htousbs(PIOS_USB_BOARD_CDC_DATA_LENGTH),
        .bInterval           = 0,                                      /* ms */
    },
#ifdef PIOS_INCLUDE_USB_CDC2
    // Second CDC interface
    .iad2                    = {
        .bLength             = sizeof(struct usb_interface_association_desc),
        .bDescriptorType   = USB_DESC_TYPE_IAD,
        .bFirstInterface   = 2,
        .bInterfaceCount   = 2,
        .bFunctionClass    = USB_INTERFACE_CLASS_CDC, /* Communication */
        .bFunctionSubClass = USB_CDC_DESC_SUBTYPE_ABSTRACT_CTRL, /* Abstract Control Model */
        .bFunctionProtocol = 1, /* V.25ter, Common AT commands */
        .iInterface          = 0,
    },
    .cdc_control_if2         = {
        .bLength             = sizeof(struct usb_interface_desc),
        .bDescriptorType    = USB_DESC_TYPE_INTERFACE,
        .bInterfaceNumber   = 2,
        .bAlternateSetting  = 0,
        .bNumEndpoints      = 1,
        .bInterfaceClass    = USB_INTERFACE_CLASS_CDC,
        .bInterfaceSubClass = USB_CDC_DESC_SUBTYPE_ABSTRACT_CTRL, /* Abstract Control Model */
        .nInterfaceProtocol = 1,       /* V.25ter, Common AT commands */
        .iInterface          = 0,
    },
    .cdc_header2             = {
        .bLength = sizeof(struct usb_cdc_header_func_desc),
        .bDescriptorType     = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType  = USB_CDC_DESC_SUBTYPE_HEADER,
        .bcdCDC  = htousbs(0x0110),
    },
    .cdc_callmgmt2           = {
        .bLength             = sizeof(struct usb_cdc_callmgmt_func_desc),
        .bDescriptorType    = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType = USB_CDC_DESC_SUBTYPE_CALLMGMT,
        .bmCapabilities     = 0x00, /* No call handling */
        .bDataInterface     = 3,
    },
    .cdc_acm2                = {
        .bLength             = sizeof(struct usb_cdc_acm_func_desc),
        .bDescriptorType    = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType = USB_CDC_DESC_SUBTYPE_ABSTRACT_CTRL,
        .bmCapabilities     = 0x02, /* line coding and serial state */
    },
    .cdc_union2              = {
        .bLength             = sizeof(struct usb_cdc_union_func_desc),
        .bDescriptorType    = USB_DESC_TYPE_CLASS_SPECIFIC,
        .bDescriptorSubType = USB_CDC_DESC_SUBTYPE_UNION,
        .bMasterInterface   = 2,
        .bSlaveInterface    = 3,
    },
    .cdc_mgmt_in2            = {
        .bLength             = sizeof(struct usb_endpoint_desc),
        .bDescriptorType  = USB_DESC_TYPE_ENDPOINT,
        .bEndpointAddress = USB_EP_IN(USB_CDC2_EP_INT),
        .bmAttributes     = USB_EP_ATTR_TT_INTERRUPT,
        .wMaxPacketSize   = htousbs(PIOS_USB_BOARD_CDC_MGMT_LENGTH),
        .bInterval           = 4,                                      /* ms */
    },
    .cdc_data_if2            = {
        .bLength             = sizeof(struct usb_interface_desc),
        .bDescriptorType    = USB_DESC_TYPE_INTERFACE,
        .bInterfaceNumber   = 3,
        .bAlternateSetting  = 0,
        .bNumEndpoints      = 2,
        .bInterfaceClass    = USB_INTERFACE_CLASS_DATA,
        .bInterfaceSubClass = 0,
        .nInterfaceProtocol = 0, /* No class specific protocol */
        .iInterface          = 0,
    },
    .cdc_in2                 = {
        .bLength             = sizeof(struct usb_endpoint_desc),
        .bDescriptorType  = USB_DESC_TYPE_ENDPOINT,
        .bEndpointAddress = USB_EP_IN(USB_CDC2_EP_BULK),
        .bmAttributes     = USB_EP_ATTR_TT_BULK,
        .wMaxPacketSize   = htousbs(PIOS_USB_BOARD_CDC_DATA_LENGTH),
        .bInterval           = 0,                                      /* ms */
    },
    .cdc_out2                = {
        .bLength             = sizeof(struct usb_endpoint_desc),
        .bDescriptorType  = USB_DESC_TYPE_ENDPOINT,
        .bEndpointAddress = USB_EP_OUT(USB_CDC2_EP_BULK),
        .bmAttributes     = USB_EP_ATTR_TT_BULK,      /* Bulk */
        .wMaxPacketSize   = htousbs(PIOS_USB_BOARD_CDC_DATA_LENGTH),
        .bInterval           = 0,                                      /* ms */
    }
#endif /* PIOS_INCLUDE_USB_CDC2 */
};

int32_t PIOS_USB_DESC_CDC_ONLY_Init(void)
{
    PIOS_USBHOOK_RegisterConfig(1, (uint8_t *)&config_cdc, sizeof(config_cdc));

    PIOS_USBHOOK_RegisterDevice((uint8_t *)&device_desc, sizeof(device_desc));

    return STATUS_OK;
}

#endif /* PIOS_INCLUDE_USB */
