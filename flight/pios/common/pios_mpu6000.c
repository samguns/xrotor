/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core haoftware; you can rnedtt
 * @{
 * @addtogroup PIOS_MPU6000 MPU6000 Functions
 * @brief Deals with the hardware interface to the 3-axis gyro
 * @{
 *
 * @file       pios_mpu000.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2012.
 * @brief      MPU6000 6-axis gyro and accel chip
 * @see        The GNU Public License (GPL) Version 3
 *
 ******************************************************************************
 */
/*istribu
 * This program is free software; you can rnedtt ad/oe ir modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pios.h"
#include <pios_mpu6000.h>
#ifdef PIOS_INCLUDE_MPU6000
#include <stdint.h>
#include <pios_constants.h>
#include <pios_sensors.h>
#include "uavobjectmanager.h"
#include <pios_mpu6000_config.h>

/* Global Variables */

#ifdef TRACE_GYRO_TIMING
traceLabel gyro_strobe;
#endif /* TRACE_GYRO_TIMING */

const uint32_t mpu6000_sample_freq[] = {
    8000, // PIOS_MPU6000_LOWPASS_256_HZ
    1000, // PIOS_MPU6000_LOWPASS_188_HZ
    1000, // PIOS_MPU6000_LOWPASS_98_HZ
    1000, // PIOS_MPU6000_LOWPASS_42_HZ
    1000, // PIOS_MPU6000_LOWPASS_20_HZ
    1000, // PIOS_MPU6000_LOWPASS_10_HZ
    1000, // PIOS_MPU6000_LOWPASS_5_HZ
};

enum pios_mpu6000_dev_magic {
    PIOS_MPU6000_DEV_MAGIC = 0x9da9b3ed,
};

// sensor driver interface
int32_t PIOS_MPU6000_driver_Test(uintptr_t context, __attribute__((unused)) float *unused1, __attribute__((unused)) float *unused2);
int32_t PIOS_MPU6000_driver_Reconfigure(uintptr_t context);
int32_t PIOS_MPU6000_driver_Reset(uintptr_t context);
void PIOS_MPU6000_driver_get_scale(float *scales, uint8_t size, uintptr_t context);
void PIOS_MPU6000_Main_driver_Wait(uintptr_t context);
QueueHandle_t PIOS_MPU6000_driver_get_queue(uintptr_t context);

const PIOS_SENSORS_Driver PIOS_MPU6000_Driver = {
    .test        = PIOS_MPU6000_driver_Test,
    .poll        = NULL,
    .fetch       = NULL,
    .reset       = PIOS_MPU6000_driver_Reset,
    .get_queue   = PIOS_MPU6000_driver_get_queue,
    .get_scale   = PIOS_MPU6000_driver_get_scale,
    .wait        = PIOS_MPU6000_Main_driver_Wait,
    .reconfigure = PIOS_MPU6000_driver_Reconfigure,
    .is_polled   = false,
};
//


struct mpu6000_dev {
    uint32_t spi_id;
    uint32_t slave_num;
    QueueHandle_t queue;
    xSemaphoreHandle strobe;
    uint32_t strobe_rate;
    uint32_t isr_count;
    const struct pios_mpu6000_cfg *cfg;
    enum pios_mpu6000_range gyro_range;
    enum pios_mpu6000_accel_range accel_range;
    enum pios_mpu6000_filter filter;
    enum pios_mpu6000_dev_magic   magic;
};

#define PIOS_MPU6000_SAMPLES_BYTES    14
#define PIOS_MPU6000_SENSOR_FIRST_REG PIOS_MPU6000_ACCEL_X_OUT_MSB

typedef union {
    uint8_t buffer[1 + PIOS_MPU6000_SAMPLES_BYTES];
    struct {
        uint8_t dummy;
        uint8_t Accel_X_h;
        uint8_t Accel_X_l;
        uint8_t Accel_Y_h;
        uint8_t Accel_Y_l;
        uint8_t Accel_Z_h;
        uint8_t Accel_Z_l;
        uint8_t Temperature_h;
        uint8_t Temperature_l;
        uint8_t Gyro_X_h;
        uint8_t Gyro_X_l;
        uint8_t Gyro_Y_h;
        uint8_t Gyro_Y_l;
        uint8_t Gyro_Z_h;
        uint8_t Gyro_Z_l;
    } data;
} mpu6000_data_t;

#define GET_SENSOR_DATA(mpudataptr, sensor) (mpudataptr.data.sensor##_h << 8 | mpudataptr.data.sensor##_l)

// ! Global structure for this device device
static struct mpu6000_dev *dev;
volatile bool mpu6000_configured = false;
static mpu6000_data_t mpu6000_data;
static PIOS_SENSORS_3Axis_SensorsWithTemp *queue_data = 0;
#define SENSOR_COUNT     2
#define SENSOR_DATA_SIZE (sizeof(PIOS_SENSORS_3Axis_SensorsWithTemp) + sizeof(Vector3i16_s) * SENSOR_COUNT)
// ! Private functions
static struct mpu6000_dev *PIOS_MPU6000_alloc(const struct pios_mpu6000_cfg *cfg);
static int32_t PIOS_MPU6000_Validate(struct mpu6000_dev *dev);
static int32_t PIOS_MPU6000_Config(struct pios_mpu6000_cfg const *cfg);
static int32_t PIOS_MPU6000_SetReg(uint8_t address, uint8_t buffer);
static int32_t PIOS_MPU6000_GetReg(uint8_t address, uint8_t *data);
static void PIOS_MPU6000_SetSpeed(const bool fast);
static bool PIOS_MPU6000_HandleData();
static bool PIOS_MPU6000_ReadSensor(bool *woken);

static int32_t PIOS_MPU6000_Test(void);
static int32_t PIOS_MPU6000_DummyReadGyros(void);

void PIOS_MPU6000_Register()
{
    PIOS_SENSORS_Register(&PIOS_MPU6000_Driver, PIOS_SENSORS_TYPE_3AXIS_GYRO_ACCEL, 0);
}
/**
 * @brief Allocate a new device
 */
static struct mpu6000_dev *PIOS_MPU6000_alloc(const struct pios_mpu6000_cfg *cfg)
{
    struct mpu6000_dev *mpu6000_dev;

    mpu6000_dev = (struct mpu6000_dev *)pios_malloc(sizeof(*mpu6000_dev));
    PIOS_Assert(mpu6000_dev);

    mpu6000_dev->magic       = PIOS_MPU6000_DEV_MAGIC;

    mpu6000_dev->queue       = xQueueCreate(cfg->max_downsample + 1, SENSOR_DATA_SIZE);
    PIOS_Assert(mpu6000_dev->queue);

    mpu6000_dev->strobe      = xSemaphoreCreateBinary();
    mpu6000_dev->strobe_rate = 8; // Sensible initial value until properly configured
    mpu6000_dev->isr_count   = mpu6000_dev->strobe_rate;

    PIOS_Assert(mpu6000_dev->strobe);

    queue_data = (PIOS_SENSORS_3Axis_SensorsWithTemp *)pios_malloc(SENSOR_DATA_SIZE);
    PIOS_Assert(queue_data);

    /* Ensure the timestamp is zeroed so it can be used as a check for
     * interrupts running.
     */
    queue_data->timestamp = 0;
    queue_data->count     = SENSOR_COUNT;

#ifdef TRACE_GYRO_TIMING
    gyro_strobe = xTraceOpenLabel("Gyro Strobe");
#endif /* TRACE_GYRO_TIMING */

    return mpu6000_dev;
}

/**
 * @brief Validate the handle to the spi device
 * @returns 0 for valid device or STATUS_ERROR otherwise
 */
static int32_t PIOS_MPU6000_Validate(struct mpu6000_dev *vdev)
{
    if (vdev == NULL) {
        return STATUS_INVAL;
    }
    if (vdev->magic != PIOS_MPU6000_DEV_MAGIC) {
        return STATUS_BADDEV;
    }
    if (vdev->spi_id == 0) {
        return STATUS_NODEV;
    }
    return STATUS_OK;
}

/**
 * @brief Initialize the MPU6000 3-axis gyro sensor.
 * @return 0 for success, STATUS_ERROR for failure
 */
int32_t PIOS_MPU6000_Init(uint32_t spi_id, uint32_t slave_num, const struct pios_mpu6000_cfg *cfg)
{
    int32_t status;

    dev = PIOS_MPU6000_alloc(cfg);
    if (dev == NULL) {
        return STATUS_NOMEM;
    }

    dev->spi_id    = spi_id;
    dev->slave_num = slave_num;
    dev->cfg = cfg;

    /* Configure the MPU6000 Sensor */
    PIOS_MPU6000_Config(cfg);

    /* Set up EXTI line */
    PIOS_EXTI_Init(cfg->exti_cfg);

    // Perform an SPI read from the gyro to clear any current interrupt status and trigger another interrupt
    if ((status = PIOS_MPU6000_DummyReadGyros())) {
        return status;
    }

    // Allow time for the device to interrupt
    PIOS_DELAY_WaitmS(10);

    // Report a failure if there was no interrupt
    if (queue_data->timestamp == 0) {
        return STATUS_DEVERR;
    }

    return STATUS_OK;
}

/**
 * @brief Initialize the MPU6000 3-axis gyro sensor
 * \return none
 * \param[in] PIOS_MPU6000_ConfigTypeDef struct to be used to configure sensor.
 *
 */
static int32_t PIOS_MPU6000_Config(struct pios_mpu6000_cfg const *cfg)
{
    int32_t status;
    uint8_t data;

    if ((status = PIOS_MPU6000_Test())) {
        // Failure
        return status;
    }

    // Reset chip
    if ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_PWR_MGMT_REG, PIOS_MPU6000_PWRMGMT_IMU_RST))) {
        // Failure
        return status;
    }

    // Allow time for the device to reset
    PIOS_DELAY_WaitmS(100);

    // Reset I2C, sensors and FIFO
    if ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_USER_CTRL_REG,
                                      PIOS_MPU6000_USERCTL_DIS_I2C |
                                      PIOS_MPU6000_USERCTL_SIG_COND |
                                      PIOS_MPU6000_USERCTL_FIFO_RST))) {
        // Failure
        return status;
    }

    // Allow time for functions to reset
    PIOS_DELAY_WaitmS(100);

    if (((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_PWR_MGMT_REG, cfg->Pwr_mgmt_clk))) ||
        ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_INT_CFG_REG, cfg->interrupt_cfg))) ||
        ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_INT_EN_REG, cfg->interrupt_en))) ||
        ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_FIFO_EN_REG, cfg->Fifo_store)))) {
        // Failure
        return status;
    }

    if ((status = PIOS_MPU6000_ConfigureRanges(cfg->gyro_range,
                                               cfg->accel_range,
                                               cfg->filter))) {
        // Failure
        return status;
    }

    if (((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_USER_CTRL_REG, cfg->User_ctl))) ||
        ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_PWR_MGMT_REG, cfg->Pwr_mgmt_clk))) ||
        ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_INT_CFG_REG, cfg->interrupt_cfg))) ||
        ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_INT_EN_REG, cfg->interrupt_en) != 0))) {
        // Failure
        return status;
    }

    if ((status = PIOS_MPU6000_GetReg(PIOS_MPU6000_INT_EN_REG, &data))) {
        // Failure
        return status;
    }

    if (data != cfg->interrupt_en) {
        return STATUS_DEVERR;
    }

    if ((status = PIOS_MPU6000_GetReg(PIOS_MPU6000_INT_STATUS_REG, &data))) {
        // Failure
        return status;
    }

    mpu6000_configured = true;

    return STATUS_OK;
}
/**
 * @brief Configures Gyro, accel and Filter ranges/setings
 * @return STATUS_OK if successful, STATUS_ERROR if device has not been initialized
 */
int32_t PIOS_MPU6000_ConfigureRanges(
    enum pios_mpu6000_range gyroRange,
    enum pios_mpu6000_accel_range accelRange,
    enum pios_mpu6000_filter filterSetting)
{
    int32_t status;

    if (dev == NULL) {
        return STATUS_NODEV;
    }

    // Determine the number of samples between timing strobe signals to the sensor task
    dev->strobe_rate = mpu6000_sample_freq[filterSetting] / PIOS_SILA_RATE;

    // update filter settings
    if ((status = PIOS_MPU6000_SetReg(PIOS_MPU6000_DLPF_CFG_REG, filterSetting)) ||
        // Gyro range
        (status = PIOS_MPU6000_SetReg(PIOS_MPU6000_GYRO_CFG_REG, gyroRange)) ||
        // Accell range
        (status = PIOS_MPU6000_SetReg(PIOS_MPU6000_ACCEL_CFG_REG, accelRange)) ||
        // Sample rate divider, chosen upon digital filtering settings
        (status = PIOS_MPU6000_SetReg(PIOS_MPU6000_SMPLRT_DIV_REG,
                                      ((filterSetting == PIOS_MPU6000_LOWPASS_256_HZ)) ?
                                      dev->cfg->Smpl_rate_div_no_dlp : dev->cfg->Smpl_rate_div_dlp))) {
        // Failure
        return status;
    }

    dev->filter      = filterSetting;
    dev->gyro_range  = gyroRange;
    dev->accel_range = accelRange;

    return STATUS_OK;
}

/**
 * @brief Claim the SPI bus for the accel communications and select this chip
 * @return STATUS_OK if successful, STATUS_DEVERR for invalid device, STATUS_BERR if unable to claim bus
 */
static int32_t PIOS_MPU6000_ClaimBus(bool fast_spi)
{
    if (PIOS_MPU6000_Validate(dev) != 0) {
        return STATUS_BADDEV;
    }
    if (PIOS_SPI_ClaimBus(dev->spi_id) != 0) {
        return STATUS_BERR;
    }
    PIOS_MPU6000_SetSpeed(fast_spi);
    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 0);
    return STATUS_OK;
}


static void PIOS_MPU6000_SetSpeed(const bool fast)
{
    static bool first_set  = true;
    static bool last_speed = false;

    if (first_set || (fast != last_speed)) {
        if (fast) {
            PIOS_SPI_SetClockSpeed(dev->spi_id, dev->cfg->fast_prescaler);
        } else {
            PIOS_SPI_SetClockSpeed(dev->spi_id, dev->cfg->std_prescaler);
        }
        first_set  = false;
        last_speed = fast;
    }
}

/**
 * @brief Claim the SPI bus for the accel communications and select this chip
 * @return STATUS_OK if successful, STATUS_DEVERR for invalid device, STATUS_BERR if unable to claim bus
 * @param woken[in,out] If non-NULL, will be set to true if woken was false and a higher priority
 *                      task has is now eligible to run, else unchanged
 */
static int32_t PIOS_MPU6000_ClaimBusISR(bool *woken, bool fast_spi)
{
    if (PIOS_MPU6000_Validate(dev) != 0) {
        return STATUS_BADDEV;
    }
    if (PIOS_SPI_ClaimBusISR(dev->spi_id, woken) != 0) {
        return STATUS_BERR;
    }
    PIOS_MPU6000_SetSpeed(fast_spi);
    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 0);
    return STATUS_OK;
}

/**
 * @brief Release the SPI bus for the accel communications and end the transaction
 * @return STATUS_OK if successful
 */
static int32_t PIOS_MPU6000_ReleaseBus()
{
    if (PIOS_MPU6000_Validate(dev) != 0) {
        return STATUS_BADDEV;
    }
    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 1);
    return PIOS_SPI_ReleaseBus(dev->spi_id);
}

/**
 * @brief Release the SPI bus for the accel communications and end the transaction
 * @return STATUS_OK if successful
 * @param woken[in,out] If non-NULL, will be set to true if woken was false and a higher priority
 *                      task has is now eligible to run, else unchanged
 */
static int32_t PIOS_MPU6000_ReleaseBusISR(bool *woken)
{
    if (PIOS_MPU6000_Validate(dev) != 0) {
        return STATUS_BADDEV;
    }
    PIOS_SPI_RC_PinSet(dev->spi_id, dev->slave_num, 1);
    return PIOS_SPI_ReleaseBusISR(dev->spi_id, woken);
}

/**
 * @brief Read a register from MPU6000
 * @returns The register value or STATUS_ERROR if failure to get bus
 * @param reg[in] Register address to be read
 */
static int32_t PIOS_MPU6000_GetReg(uint8_t reg, uint8_t *data)
{
    if (PIOS_MPU6000_ClaimBus(false) != 0) {
        return STATUS_BERR;
    }

    PIOS_SPI_TransferByte(dev->spi_id, (0x80 | reg)); // request byte
    *data = PIOS_SPI_TransferByte(dev->spi_id, 0); // receive response

    PIOS_MPU6000_ReleaseBus();

    return STATUS_OK;
}

/**
 * @brief Writes one byte to the MPU6000
 * \param[in] reg Register address
 * \param[in] data Byte to write
 * \return 0 if operation was successful
 * \return STATUS_BERR if unable to claim SPI bus
 * \return STATUS_DEVERR if unable to access i2c device
 */
static int32_t PIOS_MPU6000_SetReg(uint8_t reg, uint8_t data)
{
    if (PIOS_MPU6000_ClaimBus(false) != 0) {
        return STATUS_BERR;
    }

    PIOS_SPI_TransferByte(dev->spi_id, 0x7f & reg);
    PIOS_SPI_TransferByte(dev->spi_id, data);

    PIOS_MPU6000_ReleaseBus();

    return STATUS_OK;
}

/**
 * @brief Perform a dummy read in order to restart interrupt generation
 * \returns 0 if succesful
 */
int32_t PIOS_MPU6000_DummyReadGyros()
{
    int32_t status;
    uint8_t mpu6000_send_buf[1 + PIOS_MPU6000_SAMPLES_BYTES] = { PIOS_MPU6000_SENSOR_FIRST_REG | 0x80 };

    if (((status = PIOS_MPU6000_ClaimBus(true))) ||
        ((status = PIOS_SPI_TransferBlock(dev->spi_id, &mpu6000_send_buf[0], &mpu6000_data.buffer[0], sizeof(mpu6000_data_t), NULL)))) {
        return status;
    }

    PIOS_MPU6000_ReleaseBus();

    return STATUS_OK;
}

/*
 * @brief Read the identification bytes from the MPU6000 sensor
 * \return ID read from MPU6000 or STATUS_ERROR if failure
 */
int32_t PIOS_MPU6000_ReadID(uint8_t *mpu6000_id)
{
    return PIOS_MPU6000_GetReg(PIOS_MPU6000_WHOAMI, mpu6000_id);
}

/**
 * \brief Reads the queue handle
 * \return Handle to the queue or null if invalid device
 */
xQueueHandle PIOS_MPU6000_GetQueue()
{
    if (PIOS_MPU6000_Validate(dev) != 0) {
        return (xQueueHandle)NULL;
    }

    return dev->queue;
}


static float PIOS_MPU6000_GetScale()
{
    switch (dev->gyro_range) {
    case PIOS_MPU6000_SCALE_250_DEG:
        return 1.0f / 131.0f;

    case PIOS_MPU6000_SCALE_500_DEG:
        return 1.0f / 65.5f;

    case PIOS_MPU6000_SCALE_1000_DEG:
        return 1.0f / 32.8f;

    case PIOS_MPU6000_SCALE_2000_DEG:
    default:
        return 1.0f / 16.4f;
    }
}

static float PIOS_MPU6000_GetAccelScale()
{
    switch (dev->accel_range) {
    case PIOS_MPU6000_ACCEL_2G:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 16384.0f;

    case PIOS_MPU6000_ACCEL_4G:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 8192.0f;

    case PIOS_MPU6000_ACCEL_8G:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 4096.0f;

    case PIOS_MPU6000_ACCEL_16G:
    default:
        return PIOS_CONST_MKS_GRAV_ACCEL_F / 2048.0f;
    }
}

/**
 * @brief Run self-test operation.
 * \return 0 if test succeeded
 * \return non-zero value if test succeeded
 */
static int32_t PIOS_MPU6000_Test(void)
{
    int32_t status;
    /* Verify that ID matches (MPU6000 ID is 0x69) */
    uint8_t mpu6000_id;

    if ((status = PIOS_MPU6000_ReadID(&mpu6000_id))) {
        // Failure
        return status;
    }

    if (mpu6000_id != 0x68) {
        return STATUS_DEVERR;
    }

    return STATUS_OK;
}

/**
 * @brief EXTI IRQ Handler.  Read all the data from onboard buffer
 * @return a boleoan to the EXTI IRQ Handler wrapper indicating if a
 *         higher priority task is now eligible to run
 */

bool PIOS_MPU6000_IRQHandler(void)
{
#if defined(PIOS_ENABLE_DEBUG_PINS) && defined(DEBUG_GYRO_TIMING)
    PIOS_DEBUG_PinHigh(1);
#endif
    bool woken = false;

    if (!mpu6000_configured) {
        return false;
    }

    // Timestamp the data
    queue_data->timestamp = PIOS_DELAY_GetRaw();

    bool read_ok = PIOS_MPU6000_ReadSensor(&woken);
    PIOS_Assert(read_ok);

#if defined(PIOS_ENABLE_DEBUG_PINS) && defined(DEBUG_GYRO_TIMING)
    PIOS_DEBUG_PinLow(1);
#endif
    return woken;
}

static bool PIOS_MPU6000_SPICallback(bool *read_ok, __attribute__((unused)) uint8_t crc)
{
    bool woken = false;

    // Transfer is now complete
    PIOS_MPU6000_ReleaseBusISR(&woken);

    if (read_ok) {
        woken |= PIOS_MPU6000_HandleData();
    }

    if (--dev->isr_count == 0) {
        dev->isr_count = dev->strobe_rate;
#if defined(PIOS_ENABLE_DEBUG_PINS) && defined(DEBUG_GYRO_TIMING)
        PIOS_DEBUG_PinHigh(0);
#endif
        xSemaphoreGive(dev->strobe);
#ifdef TRACE_GYRO_TIMING
        vTraceUserEvent(gyro_strobe);
#endif /* TRACE_GYRO_TIMING */
    }

    return woken;
}

static bool PIOS_MPU6000_HandleData()
{
    if (!queue_data) {
        return false;
    }

    // Rotate the sensor to OP convention.  The datasheet defines X as towards the right
    // and Y as forward.  OP convention transposes this.  Also the Z is defined negatively
    // to our convention

    // Currently we only support rotations on top so switch X/Y accordingly
    switch (dev->cfg->orientation) {
    case PIOS_MPU6000_TOP_0DEG:
        queue_data->sample[0].y = GET_SENSOR_DATA(mpu6000_data, Accel_X); // chip X
        queue_data->sample[0].x = GET_SENSOR_DATA(mpu6000_data, Accel_Y); // chip Y
        queue_data->sample[1].y = GET_SENSOR_DATA(mpu6000_data, Gyro_X); // chip X
        queue_data->sample[1].x = GET_SENSOR_DATA(mpu6000_data, Gyro_Y); // chip Y
        break;
    case PIOS_MPU6000_TOP_90DEG:
        // -1 to bring it back to -32768 +32767 range
        queue_data->sample[0].y = -1 - (GET_SENSOR_DATA(mpu6000_data, Accel_Y)); // chip Y
        queue_data->sample[0].x = GET_SENSOR_DATA(mpu6000_data, Accel_X); // chip X
        queue_data->sample[1].y = -1 - (GET_SENSOR_DATA(mpu6000_data, Gyro_Y)); // chip Y
        queue_data->sample[1].x = GET_SENSOR_DATA(mpu6000_data, Gyro_X); // chip X
        break;
    case PIOS_MPU6000_TOP_180DEG:
        queue_data->sample[0].y = -1 - (GET_SENSOR_DATA(mpu6000_data, Accel_X)); // chip X
        queue_data->sample[0].x = -1 - (GET_SENSOR_DATA(mpu6000_data, Accel_Y)); // chip Y
        queue_data->sample[1].y = -1 - (GET_SENSOR_DATA(mpu6000_data, Gyro_X)); // chip X
        queue_data->sample[1].x = -1 - (GET_SENSOR_DATA(mpu6000_data, Gyro_Y)); // chip Y
        break;
    case PIOS_MPU6000_TOP_270DEG:
        queue_data->sample[0].y = GET_SENSOR_DATA(mpu6000_data, Accel_Y); // chip Y
        queue_data->sample[0].x = -1 - (GET_SENSOR_DATA(mpu6000_data, Accel_X)); // chip X
        queue_data->sample[1].y = GET_SENSOR_DATA(mpu6000_data, Gyro_Y); // chip Y
        queue_data->sample[1].x = -1 - (GET_SENSOR_DATA(mpu6000_data, Gyro_X)); // chip X
        break;
    }
    queue_data->sample[0].z = -1 - (GET_SENSOR_DATA(mpu6000_data, Accel_Z));
    queue_data->sample[1].z = -1 - (GET_SENSOR_DATA(mpu6000_data, Gyro_Z));
    const int16_t temp = GET_SENSOR_DATA(mpu6000_data, Temperature);
    queue_data->temperature = 3500 + ((float)(temp + 512)) * (1.0f / 3.4f);

    BaseType_t higherPriorityTaskWoken;
    xQueueSendToBackFromISR(dev->queue, (void *)queue_data, &higherPriorityTaskWoken);
    return higherPriorityTaskWoken == pdTRUE;
}

static bool PIOS_MPU6000_ReadSensor(bool *woken)
{
    static uint8_t mpu6000_send_buf[1 + PIOS_MPU6000_SAMPLES_BYTES] = { PIOS_MPU6000_SENSOR_FIRST_REG | 0x80 };

    if (PIOS_MPU6000_ClaimBusISR(woken, true) != 0) {
        /* Ideally this should never happen, but if so the bus is still
         * claimed, due to re-calibration accesses. In order to provide
         * the timing strobe to the sensors module task, process the
         * previously read data again.
         */
        *woken |= PIOS_MPU6000_HandleData();

        if (--dev->isr_count == 0) {
            dev->isr_count = dev->strobe_rate;
            /* If called here this strobe will be about 65us earlier than
             * if the SPI read had happened, but that shouldn't be an issue.
             */
            xSemaphoreGive(dev->strobe);
#ifdef TRACE_GYRO_TIMING
            vTraceUserEvent(gyro_strobe);
#endif /* TRACE_GYRO_TIMING */
        }
    }
    // Use a callback to handle the transfer
    else if (PIOS_SPI_TransferBlock(dev->spi_id, &mpu6000_send_buf[0], &mpu6000_data.buffer[0], sizeof(mpu6000_data_t), PIOS_MPU6000_SPICallback) < 0) {
        PIOS_MPU6000_ReleaseBusISR(woken);
        return false;
    }

    return true;
}

// Sensor driver implementation
int32_t PIOS_MPU6000_driver_Test(__attribute__((unused)) uintptr_t context, __attribute__((unused)) float *unused1, __attribute__((unused)) float *unused2)
{
    return PIOS_MPU6000_Test();
}

void PIOS_MPU6000_Main_driver_Wait(__attribute__((unused)) uintptr_t context)
{
    xSemaphoreTake(dev->strobe, portMAX_DELAY);
}
int32_t PIOS_MPU6000_driver_Reconfigure(__attribute__((unused)) uintptr_t context)
{
    return PIOS_MPU6000_CONFIG_Configure();
}

int32_t PIOS_MPU6000_driver_Reset(__attribute__((unused)) uintptr_t context)
{
    return PIOS_MPU6000_DummyReadGyros();
}

void PIOS_MPU6000_driver_get_scale(float *scales, uint8_t size, __attribute__((unused)) uintptr_t contet)
{
    PIOS_Assert(size >= 2);
    scales[0] = PIOS_MPU6000_GetAccelScale();
    scales[1] = PIOS_MPU6000_GetScale();
}

QueueHandle_t PIOS_MPU6000_driver_get_queue(__attribute__((unused)) uintptr_t context)
{
    return dev->queue;
}
#endif /* PIOS_INCLUDE_MPU6000 */

/**
 * @}
 * @}
 */
