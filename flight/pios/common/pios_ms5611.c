/**
 ******************************************************************************
 * @addtogroup PIOS PIOS Core hardware abstraction layer
 * @{
 * @addtogroup PIOS_MS5611 MS5611 Functions
 * @brief Hardware functions to deal with the altitude pressure sensor
 * @{
 *
 * @file       pios_ms5611.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2012.
 * @brief      MS5611 Pressure Sensor Routines
 * @see        The GNU Public License (GPL) Version 3
 *
 ******************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "pios.h"
#ifdef PIOS_INCLUDE_MS5611
#include <pios_ms5611.h>
#define POW2(x) (1 << x)

#ifdef TRACE_MS5611
traceLabel conversion_start_enter;
traceLabel conversion_start_exit;
traceLabel read_adc_enter;
traceLabel read_adc_exit;
traceLabel read_adc_abort;
traceLabel callback_null;
traceLabel callback_temp;
traceLabel callback_pres;
#endif /* TRACE_MS5611 */

// TODO: Clean this up.  Getting around old constant.
#define PIOS_MS5611_OVERSAMPLING   oversampling

// Option to change the interleave between Temp and Pressure conversions
// Undef for normal operation
#define PIOS_MS5611_SLOW_TEMP_RATE 20
#ifndef PIOS_MS5611_SLOW_TEMP_RATE
#define PIOS_MS5611_SLOW_TEMP_RATE 1
#endif
// Running moving average smoothing factor
#define PIOS_MS5611_TEMP_SMOOTHING 10
//
/* Local Types */
typedef struct {
    uint16_t C[6];
} MS5611CalibDataTypeDef;

typedef enum {
    MS5611_CONVERSION_TYPE_None = 0,
    MS5611_CONVERSION_TYPE_PressureConv,
    MS5611_CONVERSION_TYPE_TemperatureConv
} ConversionTypeTypeDef;

/* Note that this driver only supports a single device instance with a single
 * state
 */
typedef enum {
    MS5611_FSM_INIT = 0,
    MS5611_FSM_START_TEMP,
    MS5611_FSM_READ_TEMP,
    MS5611_FSM_START_PRES,
    MS5611_FSM_READ_PRES,
    MS5611_FSM_CALCULATE,
} MS5611_FSM_State;

/* Glocal Variables */
ConversionTypeTypeDef CurrentRead = MS5611_CONVERSION_TYPE_None;

/* Local Variables */
MS5611CalibDataTypeDef CalibData;

/* Variables may be placed in CCSRAM which does not support DMA, so ensure the following
 * variable is in SRAM by placing it in the .data segment by declaring it static.
 */
static uint8_t ms5611_addr;

/* Straight from the datasheet */
static uint32_t RawTemperature;
static uint32_t RawPressure;
static int64_t Pressure;
static int64_t Temperature;
static int64_t FilteredTemperature;
static int32_t lastConversionStart;

static uint32_t conversionDelayMs;
static uint32_t conversionDelayUs;

static int32_t PIOS_MS5611_Read(uint8_t address, uint8_t *buffer, uint8_t len);
static int32_t PIOS_MS5611_Read_Callback(uint8_t address, uint8_t *buffer, uint8_t len, void *callback);
xSemaphoreHandle sem_read_pending;
static int32_t PIOS_MS5611_WriteCommand(uint8_t command);
static int32_t PIOS_MS5611_WriteCommand_NonBlocking(uint8_t command);
xSemaphoreHandle sem_write_pending;
static uint32_t PIOS_MS5611_GetDelay();
static uint32_t PIOS_MS5611_GetDelayUs();

// Second order temperature compensation. Temperature offset
static int64_t compensation_t2;

// Move into proper driver structure with cfg stored
static uint32_t oversampling;
static const struct pios_ms5611_cfg *dev_cfg;
static int32_t i2c_id;
static PIOS_SENSORS_1Axis_SensorsWithTemp results;

// sensor driver interface
int32_t PIOS_MS5611_driver_Test(uintptr_t context, float *, float *);
int32_t PIOS_MS5611_driver_Reset(uintptr_t context);
void PIOS_MS5611_driver_get_scale(float *scales, uint8_t size, uintptr_t context);
void PIOS_MS5611_driver_fetch(void *, uint8_t size, uintptr_t context);
bool PIOS_MS5611_driver_poll(uintptr_t context);

const PIOS_SENSORS_Driver PIOS_MS5611_Driver = {
    .test        = PIOS_MS5611_driver_Test,
    .poll        = PIOS_MS5611_driver_poll,
    .fetch       = PIOS_MS5611_driver_fetch,
    .reset       = PIOS_MS5611_driver_Reset,
    .get_queue   = NULL,
    .get_scale   = PIOS_MS5611_driver_get_scale,
    .reconfigure = NULL,
    .is_polled   = true,
};
/**
 * Initialise the MS5611 sensor
 */
int32_t ms5611_read_flag;
void PIOS_MS5611_Init(const struct pios_ms5611_cfg *cfg, int32_t i2c_device)
{
#ifdef TRACE_MS5611
    conversion_start_enter = xTraceOpenLabel("MS5611 start enter");
    conversion_start_exit  = xTraceOpenLabel("MS5611 start exit");
    read_adc_enter = xTraceOpenLabel("MS5611 read enter");
    read_adc_exit  = xTraceOpenLabel("MS5611 read exit");
    read_adc_abort = xTraceOpenLabel("MS5611 read abort");
    callback_null  = xTraceOpenLabel("MS5611 callback null");
    callback_temp  = xTraceOpenLabel("MS5611 callback temp");
    callback_pres  = xTraceOpenLabel("MS5611 callback pres");
#endif /* TRACE_MS5611 */

    i2c_id = i2c_device;

    sem_write_pending = xSemaphoreCreateBinary();
    xSemaphoreGive(sem_write_pending);
    sem_read_pending  = xSemaphoreCreateBinary();
    xSemaphoreGive(sem_read_pending);

    oversampling = cfg->oversampling;
    conversionDelayMs = PIOS_MS5611_GetDelay();
    conversionDelayUs = PIOS_MS5611_GetDelayUs();

    dev_cfg = cfg; // Store cfg before enabling interrupt

    PIOS_MS5611_WriteCommand(MS5611_RESET);
    PIOS_DELAY_WaitmS(20);

    // This must not be on the stack as it's accessed using DMA
    static uint8_t data[2];

    // reset temperature compensation values
    compensation_t2 = 0;
    /* Calibration parameters */
    for (int i = 0; i < 6; i++) {
        while (PIOS_MS5611_Read(MS5611_CALIB_ADDR + i * 2, data, 2)) {}
        ;
        CalibData.C[i] = (data[0] << 8) | data[1];
    }

    /* Reset filter */
    FilteredTemperature = INT32_MIN;
}

/**
 * @brief Return the delay for the current osr
 */
static uint32_t PIOS_MS5611_GetDelay()
{
    switch (oversampling) {
    case MS5611_OSR_256:
        return 1;

    case MS5611_OSR_512:
        return 2;

    case MS5611_OSR_1024:
        return 3;

    case MS5611_OSR_2048:
        return 5;

    case MS5611_OSR_4096:
        return 10;

    default:
        break;
    }
    return 10;
}

/**
 * @brief Return the delay for the current osr in uS
 */
static uint32_t PIOS_MS5611_GetDelayUs()
{
    switch (oversampling) {
    case MS5611_OSR_256:
        return 600;

    case MS5611_OSR_512:
        return 1170;

    case MS5611_OSR_1024:
        return 2280;

    case MS5611_OSR_2048:
        return 4540;

    case MS5611_OSR_4096:
        return 9040;

    default:
        break;
    }
    return 10;
}

// Callback routine for reads of temperature
#define MS5611_TEMP_SIZE 3
static int64_t deltaTemp;
static uint8_t TemperatureRawData[MS5611_TEMP_SIZE];
void temperature_callback(int32_t status, __attribute__((unused)) portBASE_TYPE *xHigherPriorityTaskWoken)
{
#ifdef TRACE_MS5611
    vTraceUserEvent(callback_temp);
#endif /* TRACE_MS5611 */

    xSemaphoreGiveFromISR(sem_read_pending, xHigherPriorityTaskWoken);

    if (status != STATUS_OK) {
        // Can't use this data
        return;
    }

    RawTemperature = (TemperatureRawData[0] << 16) | (TemperatureRawData[1] << 8) | TemperatureRawData[2];
    // Difference between actual and reference temperature
    // dT = D2 - TREF = D2 - C5 * 2^8
    deltaTemp   = ((int32_t)RawTemperature) - (CalibData.C[4] * POW2(8));
    // Actual temperature (-40…85°C with 0.01°C resolution)
    // TEMP = 20°C + dT * TEMPSENS = 2000 + dT * C6 / 2^23
    Temperature = 2000l + ((deltaTemp * CalibData.C[5]) / POW2(23));
    if (FilteredTemperature != INT32_MIN) {
        FilteredTemperature = (FilteredTemperature * (PIOS_MS5611_TEMP_SMOOTHING - 1)
                               + Temperature) / PIOS_MS5611_TEMP_SMOOTHING;
    } else {
        FilteredTemperature = Temperature;
    }
}

// Callback routine for reads of pressure
#define MS5611_PRES_SIZE 3
static uint8_t PressureRawData[MS5611_PRES_SIZE];
uint32_t pressureTimestamp;
void pressure_callback(int32_t status, __attribute__((unused)) portBASE_TYPE *xHigherPriorityTaskWoken)
{
#ifdef TRACE_MS5611
    vTraceUserEvent(callback_pres);
#endif /* TRACE_MS5611 */
    int64_t Offset;
    int64_t Sens;
    // used for second order temperature compensation
    int64_t Offset2 = 0;
    int64_t Sens2   = 0;

    xSemaphoreGiveFromISR(sem_read_pending, xHigherPriorityTaskWoken);

    if (status != STATUS_OK) {
        // Can't use this data
        return;
    }

    pressureTimestamp = PIOS_DELAY_GetRaw();
    // check if temperature is less than 20°C
    if (FilteredTemperature < 2000) {
        // Apply compensation
        // T2 = dT^2 / 2^31
        // OFF2 = 5 ⋅ (TEMP – 2000)^2/2
        // SENS2 = 5 ⋅ (TEMP – 2000)^2/2^2

        int64_t tcorr = (FilteredTemperature - 2000) * (FilteredTemperature - 2000);
        Offset2 = (5 * tcorr) / 2;
        Sens2   = (5 * tcorr) / 4;
        compensation_t2 = (deltaTemp * deltaTemp) >> 31;
        // Apply the "Very low temperature compensation" when temp is less than -15°C
        if (FilteredTemperature < -1500) {
            // OFF2 = OFF2 + 7 ⋅ (TEMP + 1500)^2
            // SENS2 = SENS2 + 11 ⋅ (TEMP + 1500)^2 / 2
            int64_t tcorr2 = (FilteredTemperature + 1500) * (FilteredTemperature + 1500);
            Offset2 += 7 * tcorr2;
            Sens2   += (11 * tcorr2) / 2;
        }
    } else {
        compensation_t2 = 0;
        Offset2 = 0;
        Sens2 = 0;
    }
    RawPressure = ((PressureRawData[0] << 16) | (PressureRawData[1] << 8) | PressureRawData[2]);
    // Offset at actual temperature
    // OFF = OFFT1 + TCO * dT = C2 * 2^16 + (C4 * dT) / 2^7
    Offset   = ((int64_t)CalibData.C[1]) * POW2(16) + (((int64_t)CalibData.C[3]) * deltaTemp) / POW2(7) - Offset2;
    // Sensitivity at actual temperature
    // SENS = SENST1 + TCS * dT = C1 * 2^15 + (C3 * dT) / 2^8
    Sens     = ((int64_t)CalibData.C[0]) * POW2(15) + (((int64_t)CalibData.C[2]) * deltaTemp) / POW2(8) - Sens2;
    // Temperature compensated pressure (10…1200mbar with 0.01mbar resolution)
    // P = D1 * SENS - OFF = (D1 * SENS / 2^21 - OFF) / 2^15
    Pressure = (((((int64_t)RawPressure) * Sens) / POW2(21)) - Offset) / POW2(15);
}

/**
 * Return the most recently computed temperature in kPa
 */
static float PIOS_MS5611_GetTemperature(void)
{
    // Apply the second order low and very low temperature compensation offset
    return ((float)(FilteredTemperature - compensation_t2)) / 100.0f;
}

/**
 * Return the most recently computed pressure in Pa
 */
static float PIOS_MS5611_GetPressure(void)
{
    return (float)Pressure;
}

/**
 * Reads one or more bytes into a buffer
 * \param[in] the command indicating the address to read
 * \param[out] buffer destination buffer which must not be in CCSRAM
 * \param[in] len number of bytes which should be read
 * \return STATUS_OK if operation was successful
 * \return STATUS_BERR if error during I2C transfer
 */
static int32_t PIOS_MS5611_Read(uint8_t address, uint8_t *buffer, uint8_t len)
{
    struct pios_i2c_txn txn_list[] = {
        {
            .addr = MS5611_I2C_ADDR,
            .rw   = PIOS_I2C_TXN_WRITE,
            .len  = 1,
            .buf  = &ms5611_addr,
        },
        {
            .addr = MS5611_I2C_ADDR,
            .rw   = PIOS_I2C_TXN_READ,
        }
    };

    // Fill in the command
    ms5611_addr     = address;
    txn_list[1].buf = buffer;
    txn_list[1].len = len;

    return PIOS_I2C_Transfer(i2c_id, txn_list, NELEMENTS(txn_list));
}

/**
 * Reads one or more bytes into a buffer
 * \param[in] the command indicating the address to read
 * \param[out] buffer destination buffer which must not be in CCSRAM
 * \param[in] len number of bytes which should be read
 * \param[in] callback callback routine to call on transfer completion
 * \return STATUS_OK if operation was successful
 * \return STATUS_BERR if error during I2C transfer
 *
 * Note that this routine must not be called until any prior callback has occurred
 */
static int32_t PIOS_MS5611_Read_Callback(uint8_t address, uint8_t *buffer, uint8_t len, void *callback)
{
    // Declare this structure static as it will be processed after this routine has exited
    static struct pios_i2c_txn txn_list[] = {
        {
            .addr = MS5611_I2C_ADDR,
            .rw   = PIOS_I2C_TXN_WRITE,
            .len  = 1,
            .buf  = &ms5611_addr,
        },
        {
            .addr = MS5611_I2C_ADDR,
            .rw   = PIOS_I2C_TXN_READ,
        }
    };

    xSemaphoreTake(sem_read_pending, portMAX_DELAY);

    // Fill in the command
    ms5611_addr     = address;
    txn_list[1].buf = buffer;
    txn_list[1].len = len;

    return PIOS_I2C_Transfer_Callback(i2c_id, txn_list, NELEMENTS(txn_list), callback);
}

static void *null_callback(portBASE_TYPE *xHigherPriorityTaskWoken)
{
#ifdef TRACE_MS5611
    vTraceUserEvent(callback_null);
#endif /* TRACE_MS5611 */
    xSemaphoreGiveFromISR(sem_write_pending, xHigherPriorityTaskWoken);
    return NULL;
}

/**
 * Writes one or more bytes to the MS5611
 * \param[in] address Register address
 * \param[in] buffer source buffer
 * \return STATUS_OK if operation was successful
 * \return STATUS_BERR if error during I2C transfer
 */

static int32_t PIOS_MS5611_WriteCommand_NonBlocking(uint8_t command)
{
    /* Automatic variables may be placed in CCSRAM which does not support DMA, so ensure the following
     * variable is in SRAM by placing it in the .data segment by declaring it static.
     */
    static uint8_t ms5611_cmd;
    // Declare this structure static as it will be processed after this routine has exited
    static const struct pios_i2c_txn ms5611_cmd_txn_list[] = {
        {
            .addr = MS5611_I2C_ADDR,
            .rw   = PIOS_I2C_TXN_WRITE,
            .len  = 1,
            .buf  = &ms5611_cmd,
        }
    };

    xSemaphoreTake(sem_write_pending, portMAX_DELAY);

    // Record the command as it's on the stack
    ms5611_cmd = command;

    // No need to wait for this command to finish so pass a null callback handler
    return PIOS_I2C_Transfer_Callback(i2c_id, ms5611_cmd_txn_list, NELEMENTS(ms5611_cmd_txn_list), null_callback);
}

/**
 * Writes one or more bytes to the MS5611
 * \param[in] address Register address
 * \param[in] buffer source buffer
 * \return STATUS_OK if operation was successful
 * \return STATUS_BERR if error during I2C transfer
 */
static int32_t PIOS_MS5611_WriteCommand(uint8_t command)
{
    /* Automatic variables may be placed in CCSRAM which does not support DMA, so ensure the following
     * variable is in SRAM by placing it in the .data segment by declaring them static.
     */
    static uint8_t ms5611_cmd;
    const struct pios_i2c_txn txn_list[] = {
        {
            .addr = MS5611_I2C_ADDR,
            .rw   = PIOS_I2C_TXN_WRITE,
            .len  = 1,
            .buf  = &ms5611_cmd,
        }
    };

    // Record the command as it's on the stack
    ms5611_cmd = command;

    return PIOS_I2C_Transfer(i2c_id, txn_list, NELEMENTS(txn_list));
}

/**
 * @brief Run self-test operation.
 * \return STATUS_OK if self-test succeed, STATUS_DEVERR if failed
 */
int32_t PIOS_MS5611_Test()
{
    return STATUS_OK;
}


/* PIOS sensor driver implementation */
void PIOS_MS5611_Register()
{
    PIOS_SENSORS_Register(&PIOS_MS5611_Driver, PIOS_SENSORS_TYPE_1AXIS_BARO, 0);
}

int32_t PIOS_MS5611_driver_Test(__attribute__((unused)) uintptr_t context, __attribute__((unused)) float *unused1, __attribute__((unused)) float *unused2)
{
    return PIOS_MS5611_Test();
}

int32_t PIOS_MS5611_driver_Reset(__attribute__((unused)) uintptr_t context)
{
    return STATUS_OK;
}

void PIOS_MS5611_driver_get_scale(float *scales, uint8_t size, __attribute__((unused))  uintptr_t context)
{
    PIOS_Assert(size > 0);
    scales[0] = 1;
}

void PIOS_MS5611_driver_fetch(void *data, __attribute__((unused)) uint8_t size, __attribute__((unused)) uintptr_t context)
{
    PIOS_Assert(data);
    memcpy(data, (void *)&results, sizeof(PIOS_SENSORS_1Axis_SensorsWithTemp));
}

bool PIOS_MS5611_driver_poll(__attribute__((unused)) uintptr_t context)
{
    static uint8_t temp_press_interleave_count = 1;
    static MS5611_FSM_State next_state = MS5611_FSM_INIT;

    switch (next_state) {
    case MS5611_FSM_INIT:
    case MS5611_FSM_START_TEMP:
#ifdef TRACE_MS5611
        vTraceUserEvent(conversion_start_enter);
#endif /* TRACE_MS5611 */
        if (PIOS_MS5611_WriteCommand_NonBlocking(MS5611_TEMP_ADDR + oversampling) == STATUS_OK) {
#ifdef TRACE_MS5611
            vTraceUserEvent(conversion_start_exit);
#endif /* TRACE_MS5611 */
            lastConversionStart = PIOS_DELAY_GetRaw();
            next_state = MS5611_FSM_READ_TEMP;
        }
        return false;

    case MS5611_FSM_READ_TEMP:
        /* Read the temperature conversion */
        if ((2 * conversionDelayUs) > PIOS_DELAY_DiffuS(lastConversionStart)) {
#ifdef TRACE_MS5611
            vTraceUserEvent(read_adc_abort);
#endif /* TRACE_MS5611 */
            return false;
        }

#ifdef TRACE_MS5611
        vTraceUserEvent(read_adc_enter);
#endif /* TRACE_MS5611 */
        if (PIOS_MS5611_Read_Callback(MS5611_ADC_READ,
                                      TemperatureRawData,
                                      MS5611_TEMP_SIZE,
                                      temperature_callback) == STATUS_OK) {
#ifdef TRACE_MS5611
            vTraceUserEvent(read_adc_exit);
#endif /* TRACE_MS5611 */
            next_state = MS5611_FSM_CALCULATE;
        }
        return false;

    case MS5611_FSM_START_PRES:
#ifdef TRACE_MS5611
        vTraceUserEvent(conversion_start_enter);
#endif /* TRACE_MS5611 */
        if (PIOS_MS5611_WriteCommand_NonBlocking(MS5611_PRES_ADDR + oversampling) == STATUS_OK) {
#ifdef TRACE_MS5611
            vTraceUserEvent(conversion_start_exit);
#endif /* TRACE_MS5611 */
            lastConversionStart = PIOS_DELAY_GetRaw();
            next_state = MS5611_FSM_READ_PRES;
        }
        return false;

    case MS5611_FSM_READ_PRES:
        /* Read the pressure conversion */
        if ((2 * conversionDelayUs) > PIOS_DELAY_DiffuS(lastConversionStart)) {
#ifdef TRACE_MS5611
            vTraceUserEvent(read_adc_abort);
#endif /* TRACE_MS5611 */
            return false;
        }

#ifdef TRACE_MS5611
        vTraceUserEvent(read_adc_enter);
#endif /* TRACE_MS5611 */
        if (PIOS_MS5611_Read_Callback(MS5611_ADC_READ,
                                      PressureRawData,
                                      MS5611_PRES_SIZE,
                                      pressure_callback) == STATUS_OK) {
#ifdef TRACE_MS5611
            vTraceUserEvent(read_adc_exit);
#endif /* TRACE_MS5611 */
            next_state = MS5611_FSM_CALCULATE;
        }
        return false;

    case MS5611_FSM_CALCULATE:
        temp_press_interleave_count--;
        if (!temp_press_interleave_count) {
            temp_press_interleave_count = PIOS_MS5611_SLOW_TEMP_RATE;
            next_state = MS5611_FSM_START_TEMP;
        } else {
            next_state = MS5611_FSM_START_PRES;
        }

        results.timestamp   = pressureTimestamp;
        results.temperature = PIOS_MS5611_GetTemperature();
        results.sample = PIOS_MS5611_GetPressure();
        return true;

    default:
        // it should not be there
        PIOS_Assert(0);
    }
    return false;
}


#endif /* PIOS_INCLUDE_MS5611 */

/**
 * @}
 * @}
 */
