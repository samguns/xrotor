# -*- coding: utf-8 -*-
############################################################################
 #
 # file       opng_uploader.py
 # author     The OpenPilotNextGeneration Team, http://www.opng.org Copyright (C) 2015.
 #
 # This program is free software; you can redistribute it and/or modify
 # it under the terms of the GNU General Public License as published by
 # the Free Software Foundation; either version 3 of the License, or
 # (at your option) any later version.
 #
 # This program is distributed in the hope that it will be useful, but
 # WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 # or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 # for more details.
 #
 # You should have received a copy of the GNU General Public License along
 # with this program; if not, write to the Free Software Foundation, Inc.,
 # 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
############################################################################

from __future__ import with_statement

import sys
import os
import time
import argparse
import binascii

from serial import *
from sys import platform as _platform

class uploader(object):
    REQ_CAPABILITIES    = str('\xB0\x0B\x00\x01')
    REP_CAPABILITIES    = str('\xB0\x0B\x00\x02')
    ENTER_IAP           = str('\xB0\x0B\x00\x03')
    JUMP_TO_FW          = str('\xB0\x0B\x00\x04')
    RESET               = str('\xB0\x0B\x00\x05')
    ABORT_OPERATION     = str('\xB0\x0B\x00\x06')
    UPLOAD_START        = str('\xB0\x0B\x01\x07')
    UPLOAD              = str('\xB0\x0B\x00\x07')
    OP_END              = str('\xB0\x0B\x00\x08')
    DOWNLOAD_REQ        = str('\xB0\x0B\x00\x09')
    DOWNLOAD            = str('\xB0\x0B\x00\x0a')
    STATUS_REQUEST      = str('\xB0\x0B\x00\x0b')

    IAPidle = 0
    uploading = 1
    wrong_packet_received = 2
    too_many_packets = 3
    too_few_packets = 4
    Last_operation_Success = 5
    downloading = 6
    idle = 7
    Last_operation_failed = 8
    uploadingStarting = 9
    outsideDevCapabilities = 10
    CRC_Fail = 11
    failed_jump = 12
    abort = 13

    def __init__(self, port, baudrate):
        self.port = Serial(port, baudrate)

    def close(self):
        if self.port is not None:
            self.port.close()

    def serial_write(self, c):
        self.port.write(c)

    def serial_read(self):
        buffer_list = []
        buffer = self.port.read(63)
        if buffer:
            '''
            n = self.port.inWaiting()
            if n:
                buffer = buffer + self.port.read(n)
            '''

            for byte in buffer:
                buffer_list.append(byte)
            return buffer_list
        return None

    def findDevice(self):
        data_len = str('\x00\x00\x00\x00')
        dev_id = str('\x01\x00\x00\x00')
        request = self.REQ_CAPABILITIES + data_len + dev_id
        self.serial_write(request)
        data_string = self.serial_read()
        if data_string is not None:
            self.DEV_ID = binascii.hexlify(data_string[13]) + \
                          binascii.hexlify(data_string[14])
            self.BL_VER = binascii.hexlify(data_string[6])
            self.FW_CRC = binascii.hexlify(data_string[9]) + \
                          binascii.hexlify(data_string[10]) + \
                          binascii.hexlify(data_string[11]) + \
                          binascii.hexlify(data_string[12])
            self.SIZE_OF_CODE = binascii.hexlify(data_string[1]) + \
                                binascii.hexlify(data_string[2]) + \
                                binascii.hexlify(data_string[3]) + \
                                binascii.hexlify(data_string[4])

    def _statusRequest(self):
        data_len = str('\x00\x00\x00\x00')
        dev_id = str('\x00\x00\x00\x00')
        request = self.STATUS_REQUEST + data_len + dev_id
        self.serial_write(request)

        result = self.serial_read()
        #print type(result), repr(result)
        if result is not None:
            response = int(binascii.hexlify(result[0]), 16)
            if response == 0x01:
                return int(binascii.hexlify(result[5]), 16)
            else:
                return self.abort
        else:
            return self.abort

    def resetDevice(self):
        data_len = str('\x00\x00\x00\x00')
        dev_id = str('\x00\x00\x00\x00')
        request = self.RESET + data_len + dev_id
        self.serial_write(request)

    def jump2app(self, erase_settings = False):
        data_len = str('\x00\x00\x00\x00')
        safe_boot = str('\x00\x00\x5a\xfe')
        if erase_settings is True:
            erase_cmd = str('\x00\x00\xfa\x5f\x00\x00\x00\x01\x00\x00\x00\x00')
        else:
            erase_cmd = str('\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00')

        request = self.JUMP_TO_FW + data_len + safe_boot + erase_cmd
        self.serial_write(request)

    def enterIAP(self):
        data_len = str('\x00\x00\x00\x00')
        dev_id = str('\x00\x01\x01\x01')
        request = self.ENTER_IAP + data_len + dev_id
        self.serial_write(request)

    def _endOperation(self):
        data_len = str('\x00\x00\x00\x00')
        dev_id = str('\x00\x00\x00\x00')
        request = self.OP_END + data_len + dev_id
        self.serial_write(request)

    def _openFirmware(self, path):
        try:
            self.fw_size = os.path.getsize(path)
            #print self.fw_size
        except os.error:
            self.fw_image = None
            return

        with open(path, 'rb') as f:
            self.fw_image = f.read()
            f.seek(self.fw_size - 100)
            self.fw_descImage = f.read(100)
            self.desc_size = 100
            if self.fw_size % 4 != 0:
                pad_size = self.fw_size / 4
                pad_size += 1
                pad_size *= 4
                pad_size -= self.fw_size
                for i in range(0, pad_size):
                    pad.append('\xff')

                self.fw_image += str(pad)
                self.fw_size += pad_size

            if self.fw_size > self.SIZE_OF_CODE:
                self.fw_image = None

    def _Crc32Fast(self, crc, data):
        CrcTable = [0x00000000, 0x04C11DB7, 0x09823B6E, 0x0D4326D9, 0x130476DC, 0x17C56B6B, 0x1A864DB2, 0x1E475005,
                    0x2608EDB8, 0x22C9F00F, 0x2F8AD6D6, 0x2B4BCB61, 0x350C9B64, 0x31CD86D3, 0x3C8EA00A, 0x384FBDBD]

        crc = crc ^ data

        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]
        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]
        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]
        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]
        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]
        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]
        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]
        crc = (crc << 4) ^ CrcTable[(crc >> 28) & 0x0F]

        return crc

    def _caculateCRC(self):
        pad_size = int(self.SIZE_OF_CODE, 16) - self.fw_size
        pad = bytearray()
        for i in range(pad_size):
            pad.append('\xff')

        self.fw_image += str(pad)
        self.fw_size += pad_size

        self.crc32 = 0xffffffff
        for x in range(self.fw_size / 4):
            aux = int(binascii.hexlify(self.fw_image[x * 4 + 3]), 16) & 0xff
            aux <<= 8
            aux += int(binascii.hexlify(self.fw_image[x * 4 + 2]), 16) & 0xff
            aux <<= 8
            aux += int(binascii.hexlify(self.fw_image[x * 4 + 1]), 16) & 0xff
            aux <<= 8
            aux += int(binascii.hexlify(self.fw_image[x * 4]), 16) & 0xff

            self.crc32 = self._Crc32Fast(self.crc32, aux) & 0xFFFFFFFF

        print hex(self.crc32)

    def _startUpload(self, numberOfBytes, type, crc32):
        numberOfPackets = numberOfBytes / 4 / 14
        pad = (numberOfBytes - numberOfPackets * 4 * 14) / 4

        if pad == 0:
            lastPacketCount = 14
        else:
            numberOfPackets += 1
            lastPacketCount = pad

        data_len = to_bytes([(numberOfPackets >> 24) & 0xff])
        data_len += to_bytes([(numberOfPackets >> 16) & 0xff])
        data_len += to_bytes([(numberOfPackets >> 8) & 0xff])
        data_len += to_bytes([numberOfPackets & 0xff])

        type_str = to_bytes([type])
        last_pkt = to_bytes([lastPacketCount])

        crc = to_bytes([(crc32 >> 24) & 0xff])
        crc += to_bytes([(crc32 >> 16) & 0xff])
        crc += to_bytes([(crc32 >> 8) & 0xff])
        crc += to_bytes([crc32 & 0xff])

        request = self.UPLOAD_START + data_len + type_str + last_pkt + crc
        self.serial_write(request)

    def _uploadData(self, numberOfBytes, data):
        numberOfPackets = numberOfBytes / 4 / 14
        pad = (numberOfBytes - numberOfPackets * 4 * 14) / 4

        if pad == 0:
            lastPacketCount = 14
        else:
            numberOfPackets += 1
            lastPacketCount = pad

        for packetcount in range(numberOfPackets):
            if (packetcount % 100) == 0:
                print '#',

            if packetcount == (numberOfPackets - 1):
                packetsize = lastPacketCount
            else:
                packetsize = 14

            data_len = to_bytes([(packetcount >> 24) & 0xff])
            data_len += to_bytes([(packetcount >> 16) & 0xff])
            data_len += to_bytes([(packetcount >> 8) & 0xff])
            data_len += to_bytes([packetcount & 0xff])

            request = self.UPLOAD
            request += data_len

            for i in range(packetsize):
                pos = 14 * 4 * packetcount
                request += data[i * 4 + pos + 3]
                request += data[i * 4 + pos + 2]
                request += data[i * 4 + pos + 1]
                request += data[i * 4 + pos]

            self.serial_write(request)
            self.port.read(9)

        print ''
        print 'Done'

    def uploadFirmware(self, fw_path):
        self._openFirmware(fw_path)
        self._caculateCRC()

        self._startUpload(self.fw_size, 0, self.crc32)
        print 'Erasing, please wait...'

        continue_upload = False
        for i in range(10):
            ret = self._statusRequest()
            if ret == self.uploading:
                continue_upload = True
                break

        if continue_upload is not True:
            return

        print 'Uploading firmware'
        self._uploadData(self.fw_size, self.fw_image)
        self._endOperation()

        flash_status = False
        for i in range(3):
            ret = self._statusRequest()
            if ret == self.Last_operation_Success:
                flash_status = True
                break
            if ret == self.CRC_Fail:
                print 'CRC fail'

        if flash_status is not True:
            print 'Firmware uploading failed'
        else:
            print 'Firmware uploading succeeded'

        print 'Uploading fimrware description'
        self._startUpload(self.desc_size, 1, 0)
        self._uploadData(self.desc_size, self.fw_descImage)
        self._endOperation()
        flash_status = False
        for i in range(3):
            ret = self._statusRequest()
            if ret == self.Last_operation_Success:
                flash_status = True
                break

        if flash_status is not True:
            print 'Description uploading failed'
        else:
            print 'Description uploading succeeded'

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--firmware', action="store", required=True,
                        help="Firmware file to be flashed")
    parser.add_argument('--baud', action="store", type=int,
                        default=115200,
                        help='Buadrate of the serial port')
    parser.add_argument('--port', action="store", required=True,
                        help='Serial port(s) to which an OPNG board is connected')
    args = parser.parse_args()

    try:
        while True:
            portlist=[]
            patterns=args.port.split(",")

            if "linux" in _platform or "darwin" in _platform:
                import glob
                for pattern in patterns:
                    portlist += glob.glob(pattern)
            else:
                portlist = patterns

            for port in portlist:
                try:
                    if "linux" in _platform:
                        if not "COM" in port and not "tty.usb" in port:
                            up = uploader(port, args.baud)
                    elif "darwin" in _platform:
                        if not "COM" in port and not "ACM" in port:
                            up = uploader(port, args.baud)
                    elif "win" in _platform:
                        if not "/" in port:
                            up = uploader(port, args.baud)
                except Exception as err:
                    print err
                    time.sleep(0.05)

                    continue

            try:
                up.findDevice()
                print("Found board: %r bootloader version: %r firmware CRC: %r size of code: %r" %
                      (up.DEV_ID, up.BL_VER, up.FW_CRC, up.SIZE_OF_CODE))
                up.enterIAP()
                up.uploadFirmware(args.firmware)

                #up.jump2app()
                up.resetDevice()
                up.close()
                sys.exit(1)
            except Exception as e:
                print e
                print("attempting reboot on %s..." % port)
                # up.resetDevice()
                up.close()
                time.sleep(0.5)
                continue
            except KeyboardInterrupt:
                up.close()
                break

    except KeyboardInterrupt:
        up.close()
        pass