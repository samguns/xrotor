/**
 ******************************************************************************
 * @addtogroup CopterControlBL CopterControl BootLoader
 * @brief These files contain the code to the CopterControl Bootloader.
 *
 * @{
 * @file       op_dfu.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      This file contains the DFU commands handling code
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* Includes ------------------------------------------------------------------*/
#include "pios.h"
#include <stdbool.h>
#include <stdarg.h>
#include "op_dfu.h"
#include "pios_bl_helper.h"
#include <pios_board_info.h>
// programmable devices
Device devicesTable[10];
uint8_t numberOfDevices = 0;

#define COMMAND 0
#define COUNT   4
#define DATA    8

DFUProgType currentProgrammingDestination; // flash, flash_trough spi
uint8_t currentDeviceCanRead;
uint8_t currentDeviceCanWrite;
Device currentDevice;

uint8_t Buffer[64];
uint8_t SendBuffer[64];
uint32_t Command         = 0;
uint32_t Aditionals      = 0;
uint32_t SizeOfTransfer  = 0;
uint32_t Expected_CRC    = 0;
uint8_t SizeOfLastPacket = 0;
uint32_t Next_Packet     = 0;
uint8_t TransferType;
uint32_t Count = 0;
uint32_t Data;
uint8_t Data0;
uint8_t Data1;
uint8_t Data2;
uint8_t Data3;
uint32_t Opt[3];

// Download vars
uint32_t downSizeOfLastPacket = 0;
uint32_t downPacketTotal = 0;
uint32_t downPacketCurrent    = 0;
DFUTransfer downType = 0;
/* Extern variables ----------------------------------------------------------*/
extern DFUStates DeviceState;
extern uint8_t JumpToApp;
extern int32_t platform_senddata(const uint8_t *msg, uint16_t msg_len);
extern void platform_sendDebugData(const uint8_t *msg, uint16_t msg_len);
/* Private function prototypes -----------------------------------------------*/
static uint32_t baseOfAdressType(uint8_t type);
static uint8_t isBiggerThanAvailable(uint8_t type, uint32_t size);
static void OPDfuIni(uint8_t discover);
static void BooBDebug(__attribute__((unused)) const char *format, ...);
static void BooBAck(uint32_t count, uint32_t expect_count, DFUStates state);
bool flash_read(uint8_t *buffer, uint32_t adr, DFUProgType type);
/* Private functions ---------------------------------------------------------*/
void sendData(uint8_t *buf, uint16_t size);
uint32_t CalcFirmCRC(void);

void DataDownload(__attribute__((unused)) DownloadAction action)
{
    if ((DeviceState == downloading)) {
        uint8_t packetSize;
        uint32_t offset;
        uint32_t partoffset;
        SendBuffer[0] = 0x01;
        SendBuffer[1] = downPacketCurrent >> 24;
        SendBuffer[2] = downPacketCurrent >> 16;
        SendBuffer[3] = downPacketCurrent >> 8;
        SendBuffer[4] = downPacketCurrent;
        if (downPacketCurrent == downPacketTotal - 1) {
            packetSize = downSizeOfLastPacket;
        } else {
            packetSize = 14;
        }
        for (uint8_t x = 0; x < packetSize; ++x) {
            partoffset = (downPacketCurrent * 14 * 4) + (x * 4);
            offset     = baseOfAdressType(downType) + partoffset;
            if (!flash_read(SendBuffer + (5 + x * 4), offset,
                            currentProgrammingDestination)) {
                DeviceState = Last_operation_failed;
            }
        }
        downPacketCurrent = downPacketCurrent + 1;
        if (downPacketCurrent > downPacketTotal - 1) {
            DeviceState = Last_operation_Success;
            Aditionals  = DOWNLOADING;
        }
        sendData(SendBuffer, 63);
    }
}

static uint32_t unpack_uint32(uint8_t *buffer)
{
    uint32_t ret = buffer[0] << 24;

    ret += buffer[1] << 16;
    ret += buffer[2] << 8;
    ret += buffer[3];
    return ret;
}

static void pack_uint32(uint32_t value, uint8_t *buffer)
{
    buffer[0] = value >> 24;
    buffer[1] = value >> 16;
    buffer[2] = value >> 8;
    buffer[3] = value;
}


void processComand(uint8_t *xReceive_Buffer)
{
    Command = unpack_uint32(&xReceive_Buffer[COMMAND]);
    Count   = unpack_uint32(&xReceive_Buffer[COUNT]);
    Data    = unpack_uint32(&xReceive_Buffer[DATA]);
    Data0   = xReceive_Buffer[DATA];
    Data1   = xReceive_Buffer[DATA + 1];
    Data2   = xReceive_Buffer[DATA + 2];
    Data3   = xReceive_Buffer[DATA + 3];
    for (uint32_t i = 0; i < 3; i++) {
        Opt[i] = unpack_uint32(&xReceive_Buffer[DATA + 4 * (i + 1)]);
    }

    switch (Command) {
    case ENTER_IAP:
        if (((DeviceState == BLidle) && (Data0 < numberOfDevices))
            || (DeviceState == IAPidle)) {
            if (Data0 > 0) {
                OPDfuIni(true);
            }

            BooBDebug("ENTER_IAP\r\n");

            DeviceState = IAPidle;
            currentProgrammingDestination = devicesTable[Data0].programmingType;
            currentDeviceCanRead  = devicesTable[Data0].readWriteFlags & 0x01;
            currentDeviceCanWrite = devicesTable[Data0].readWriteFlags >> 1
                                    & 0x01;
            currentDevice = devicesTable[Data0];
            uint8_t result = 0;
            switch (currentProgrammingDestination) {
            case Self_flash:
                result = PIOS_BL_HELPER_FLASH_Ini();
                break;
            case Remote_flash_via_spi:
                result = true;
                break;
            default:
                DeviceState = Last_operation_failed;
                Aditionals  = (uint16_t)Command;
            }
            if (result != 1) {
                DeviceState = Last_operation_failed;
                Aditionals  = (uint32_t)Command;
            }
        }
        break;
    case UPLOAD_START:
        if ((DeviceState == IAPidle) || (DeviceState == uploading)) {
            if (Next_Packet == 0) {
                BooBDebug("UPLOAD_START\r\n");

                TransferType     = Data0;
                SizeOfTransfer   = Count;
                Next_Packet      = 1;
                Expected_CRC     = unpack_uint32(&xReceive_Buffer[DATA + 2]);
                SizeOfLastPacket = Data1;

                if (isBiggerThanAvailable(TransferType, (SizeOfTransfer - 1)
                                          * 14 * 4 + SizeOfLastPacket * 4) == true) {
                    DeviceState = outsideDevCapabilities;
                    Aditionals  = (uint32_t)Command;
                } else {
                    uint8_t result = 1;
                    if (TransferType == FW) {
                        switch (currentProgrammingDestination) {
                        case Self_flash:
                            result = PIOS_BL_HELPER_FLASH_Start();
                            break;
                        case Remote_flash_via_spi:
                            result = false;
                            break;
                        default:
                            break;
                        }
                    }
                    if (result != 1) {
                        DeviceState = Last_operation_failed;
                        Aditionals  = (uint32_t)Command;
                    } else {
                        DeviceState = uploading;
                    }
                }
            } else {
                DeviceState = Last_operation_failed;
                Aditionals  = (uint32_t)Command;

                BooBDebug("UPLOAD_START failed\r\n");
            }
        }
        break;
    case UPLOADING:
        if ((DeviceState == IAPidle) || (DeviceState == uploading)) {
            if (Next_Packet != 0) {
                if (Count > SizeOfTransfer) {
                    DeviceState = too_many_packets;
                    Aditionals  = Count;

                    BooBDebug("UPLOADING too_many_packets\r\n");
                } else if (Count == Next_Packet - 1) {
                    uint8_t numberOfWords = 14;
                    if (Count == SizeOfTransfer - 1) { // is this the last packet?
                        numberOfWords = SizeOfLastPacket;
                    }
                    uint8_t result = 0;
                    uint32_t offset;
                    uint32_t aux;;
                    switch (currentProgrammingDestination) {
                    case Self_flash:
                        for (uint8_t x = 0; x < numberOfWords; ++x) {
                            offset = 4 * x;
                            Data   = unpack_uint32(&xReceive_Buffer[DATA + offset]);
                            aux    = baseOfAdressType(TransferType) + (uint32_t)(
                                Count * 14 * 4 + x * 4);
                            result = 0;
                            for (int retry = 0; retry < MAX_WRI_RETRYS; ++retry) {
                                if (result == 0) {
                                    result = (FLASH_ProgramWord(aux, Data)
                                              == FLASH_COMPLETE) ? 1 : 0;
                                }
                            }
                        }
                        break;
                    case Remote_flash_via_spi:
                        result = false; // No support for this for the OPLink Mini
                        break;
                    default:
                        result = 0;
                        break;
                    }
                    if (result != 1) {
                        Aditionals = (uint32_t)Command;

                        BooBAck(Count, Next_Packet, Last_operation_failed);
                        BooBDebug("UPLOADING Last_operation_failed\r\n");
                    } else {
                        BooBAck(Count, Next_Packet, DeviceState);

                        ++Next_Packet;
                    }
                } else {
                    Aditionals = Count;
                    BooBAck(Count, Next_Packet, wrong_packet_received);
                    BooBDebug("wrong_packet_received %d %d\r\n", Count, Next_Packet);
                }
            } else {
                DeviceState = Last_operation_failed;
                Aditionals  = Command;

                BooBDebug("UPLOADING not in IAPidle\r\n");
            }
        }
        break;
    case REQ_CAPABILITY:
        BooBDebug("REQ_CAPABILITY\r\n");

        OPDfuIni(true);
        Buffer[0] = 0x01;
        if (Data0 == 0) {
            Buffer[1] = 0;
            Buffer[2] = 0;
            Buffer[3] = 0;
            Buffer[4] = 0;
            Buffer[5] = 0;
            Buffer[6] = numberOfDevices;
            uint16_t WRFlags = 0;
            for (int x = 0; x < numberOfDevices; ++x) {
                WRFlags = ((devicesTable[x].readWriteFlags << (x * 2))
                           | WRFlags);
            }
            Buffer[7] = WRFlags >> 8;
            Buffer[8] = WRFlags;
        } else {
            pack_uint32(devicesTable[Data0 - 1].sizeOfCode, &Buffer[1]);
            Buffer[5]  = Data0;
            Buffer[6]  = devicesTable[Data0 - 1].BL_Version;
            Buffer[7]  = devicesTable[Data0 - 1].sizeOfDescription;
            Buffer[8]  = devicesTable[Data0 - 1].devID;
            pack_uint32(devicesTable[Data0 - 1].FW_Crc, &Buffer[9]);
            Buffer[13] = devicesTable[Data0 - 1].devID >> 8;
            Buffer[14] = devicesTable[Data0 - 1].devID;
        }
        sendData(Buffer, 63);
        break;
    case JUMP_TO_FW:
        if (Data == 0x5AFE) {
            /* Force board into safe mode */
            PIOS_IAP_WriteBootCount(0xFFFF);
        }
        // pass any Opt value to the firmware
        PIOS_IAP_WriteBootCmd(0, Opt[0]);
        PIOS_IAP_WriteBootCmd(1, Opt[1]);
        PIOS_IAP_WriteBootCmd(2, Opt[2]);

        FLASH_Lock();
        JumpToApp = 1;
        break;
    case RESET:
        PIOS_SYS_Reset();
        break;
    case ABORT_OPERATION:
        Next_Packet = 0;
        DeviceState = IAPidle;
        break;

    case OP_END:
        if (DeviceState == uploading) {
            if (Next_Packet - 1 == SizeOfTransfer) {
                Next_Packet = 0;
                if ((TransferType != FW) || (Expected_CRC == CalcFirmCRC())) {
                    DeviceState = Last_operation_Success;
                } else {
                    DeviceState = CRC_Fail;
                }
            }
            if (Next_Packet - 1 < SizeOfTransfer) {
                Next_Packet = 0;
                DeviceState = too_few_packets;
            }
            BooBDebug("Expected_CRC: 0x%X Actual: 0x%X\r\n", Expected_CRC, CalcFirmCRC());
        }
        break;
    case DOWNLOAD_REQ:
        if (DeviceState == IAPidle) {
            downType = Data0;
            downPacketTotal = Count;
            downSizeOfLastPacket = Data1;
            if (isBiggerThanAvailable(downType, (downPacketTotal - 1) * 14
                                      + downSizeOfLastPacket) == 1) {
                DeviceState = outsideDevCapabilities;
                Aditionals  = (uint32_t)Command;
            } else {
                downPacketCurrent = 0;
                DeviceState = downloading;
            }
        } else {
            DeviceState = Last_operation_failed;
            Aditionals  = (uint32_t)Command;
        }
        break;

    case REQ_STATUS:
        BooBDebug("Status Request\r\n");

        Buffer[0] = 0x01;
        if (DeviceState == wrong_packet_received) {
            pack_uint32(Aditionals, &Buffer[1]);
            BooBDebug("wrong_packet_received\r\n");
        } else {
            Buffer[1] = 0;
            Buffer[2] = ((uint16_t)Aditionals) >> 8;
            Buffer[3] = ((uint16_t)Aditionals);
            Buffer[4] = 0;
        }
        Buffer[5] = DeviceState;
        Buffer[6] = 0;
        Buffer[7] = 0;
        Buffer[8] = 0;
        sendData(Buffer, 63);
        if (DeviceState == Last_operation_Success) {
            DeviceState = IAPidle;
        }
        break;
    default:
        break;
    }
}

void OPDfuIni(uint8_t discover)
{
    const struct pios_board_info *bdinfo = &pios_board_info_blob;
    Device dev;

    dev.programmingType   = Self_flash;
    dev.readWriteFlags    = (BOARD_READABLE | (BOARD_WRITABLE << 1));
    dev.startOfUserCode   = bdinfo->fw_base;
    dev.sizeOfCode = bdinfo->fw_size;
    dev.sizeOfDescription = bdinfo->desc_size;
    dev.BL_Version  = bdinfo->bl_rev;
    dev.FW_Crc      = CalcFirmCRC();
    dev.devID = (bdinfo->board_type << 8) | (bdinfo->board_rev);
    dev.devType     = bdinfo->hw_type;
    numberOfDevices = 1;
    devicesTable[0] = dev;
    if (discover) {
        // TODO check other devices trough spi or whatever
    }
}
uint32_t baseOfAdressType(DFUTransfer type)
{
    switch (type) {
    case FW:
        return currentDevice.startOfUserCode;

        break;
    case Descript:
        return currentDevice.startOfUserCode + currentDevice.sizeOfCode;

        break;
    default:

        return 0;
    }
}
uint8_t isBiggerThanAvailable(DFUTransfer type, uint32_t size)
{
    switch (type) {
    case FW:
        return (size > currentDevice.sizeOfCode) ? 1 : 0;

        break;
    case Descript:
        return (size > currentDevice.sizeOfDescription) ? 1 : 0;

        break;
    default:
        return true;
    }
}

uint32_t CalcFirmCRC()
{
    switch (currentProgrammingDestination) {
    case Self_flash:
        return PIOS_BL_HELPER_CRC_Memory_Calc();

        break;
    case Remote_flash_via_spi:
        return 0;

        break;
    default:
        return 0;

        break;
    }
}
void sendData(uint8_t *buf, uint16_t size)
{
    platform_senddata(buf, size);
}

bool flash_read(uint8_t *buffer, uint32_t adr, DFUProgType type)
{
    switch (type) {
    case Remote_flash_via_spi:
        return false; // We should not get this for the OPLink Mini

        break;
    case Self_flash:
        for (uint8_t x = 0; x < 4; ++x) {
            buffer[x] = *PIOS_BL_HELPER_FLASH_If_Read(adr + x);
        }
        return true;

        break;
    default:
        return false;
    }
}

static void BooBDebug(__attribute__((unused)) const char *format, ...)
{
#ifdef PIOS_INCLUDE_BOOB_DEBUG_OUTPUT
    va_list args;
    char buffer[63];

    memset(buffer, 0, sizeof(buffer));
    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer), format, args);
    va_end(args);

    platform_sendDebugData((uint8_t *)buffer, sizeof(buffer));
#endif /* PIOS_INCLUDE_BOOB_DEBUG_OUTPUT */
}

#define COUNT_OFFSET        0
#define EXPECT_COUNT_OFFSET 4
#define STATE_OFFSET        8
#define ACK_PACKET_LENGTH   9

static void BooBAck(uint32_t count, uint32_t expect_count, DFUStates state)
{
    pack_uint32(count, &Buffer[COUNT_OFFSET]);
    pack_uint32(expect_count, &Buffer[EXPECT_COUNT_OFFSET]);
    Buffer[STATE_OFFSET] = state;

    sendData(Buffer, ACK_PACKET_LENGTH);
}
