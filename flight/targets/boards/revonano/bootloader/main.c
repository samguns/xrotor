/**
 ******************************************************************************
 * @addtogroup RevolutionBL Revolution BootLoader
 * @brief These files contain the code to the Revolution Bootloader.
 *
 * @{
 * @file       main.c
 * @author     The OPNG Team, http://www.opng.org Copyright (C) 2015.
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      This is the file with the main function of the Revolution BootLoader
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <pios.h>
#include <pios_board_info.h>
#include <op_dfu.h>
#include <pios_iap.h>
#include <fifo_buffer.h>
#include <pios_com_msg.h>
#include <pios_com.h>
#include <pios_usbhook.h> /* PIOS_USBHOOK_* */
#include <stdbool.h>
#include <mathmisc.h>


// External functions
extern void PIOS_Board_Init(void);
extern void dataDownload(__attribute__((unused)) DownloadAction action);

#define BSL_HOLD_STATE     ((PIOS_USB_DETECT_GPIO_PORT->IDR & PIOS_USB_DETECT_GPIO_PIN) ? 0 : 1)

// Timeout after which bootloader jumps to main firmware
#define BOOT_TIMEOUT       6000000 // Six seconds
// Timeout to handle lost El Capitan data
#define RETRANSMIT_TIMEOUT 10000000 // Ten seconds
// LEDs PWM
// Fix the PWM period at 100Hz to avoid flicker
#define LED_PWM_PERIOD     (1000000 / 100)
bool ledPWM(float sweep_freq);

// Bootloader state
DFUStates blState;

/* Private function prototypes -----------------------------------------------*/
// Set brown-out reset setting
void checkBOR();
uint8_t processRX();

// Jump to the application
void jump_to_app();

int main()
{
    PIOS_DELAY_Init();

    bool USB_connected    = false;
    bool User_DFU_request = false;

    PIOS_SYS_Init();
    PIOS_Board_Init();
    PIOS_IAP_Init();

    // Make sure the brown out reset value for this chip
    // is 2.7 volts
    checkBOR();

    USB_connected = PIOS_USB_CheckAvailable(0);

    // If an In-Application-Programming request has been made flag use of DFU
    if (PIOS_IAP_CheckRequest() == true) {
        PIOS_DELAY_WaitmS(1000);
        User_DFU_request = true;
        PIOS_IAP_ClearRequest();
    }

    // Set the initial bootloader state
    if (User_DFU_request) {
        // Start IAP
        blState = IAPidle;
    } else if (USB_connected) {
        // USB is connected so prepare to receive bootloader commands over USB
        blState = BLidle;
    } else {
        // Jump straight into the application
        jump_to_app();
    }

    uint32_t boot_time = PIOS_DELAY_GetuS();

    while (true) {
        float freqHeartbeat;
        float freqAlarm;

        switch (blState) {
        case Last_operation_Success:
        case uploadingStarting:
        case IAPidle:
            freqHeartbeat = 10.0f;
            freqAlarm     = 0.0f;
            PIOS_LED_Off(PIOS_LED_ALARM);
#if defined(PIOS_INCLUDE_USB_CDC2) && defined(PIOS_INCLUDE_DEBUG_CONSOLE)
            {
                const uint8_t msg[] = "Be careful, only REAL BOOB is allowed!\r\n";
                PIOS_COM_SendBuffer(PIOS_COM_DEBUG, msg, sizeof(msg));
            }
#endif /* PIOS_INCLUDE_USB_CDC2 && PIOS_INCLUDE_DEBUG_CONSOLE */
            break;
        case uploading:
            freqHeartbeat = 0.0f;
            freqAlarm     = 5.0f;
            PIOS_LED_Off(PIOS_LED_HEARTBEAT);
            break;
        case downloading:
            freqHeartbeat = 1.0f;
            freqAlarm     = 4.0f;
            break;
        case BLidle:
            freqHeartbeat = 1.0f;
            freqAlarm     = 0.0f;
            PIOS_LED_Off(PIOS_LED_ALARM);
#if defined(PIOS_INCLUDE_USB_CDC2) && defined(PIOS_INCLUDE_DEBUG_CONSOLE)
            {
                const uint8_t msg[] = "Waiting to see a BOOB\r\n";
                PIOS_COM_SendBuffer(PIOS_COM_DEBUG, msg, sizeof(msg));
            }
#endif /* PIOS_INCLUDE_USB_CDC2 && PIOS_INCLUDE_DEBUG_CONSOLE */
            break;
        default: // error
            freqHeartbeat = 5.0f;
            freqAlarm     = 5.0f;
        }

        if (!is_zero(freqHeartbeat)) {
            if (ledPWM(freqHeartbeat)) {
                PIOS_LED_On(PIOS_LED_HEARTBEAT);
            } else {
                PIOS_LED_Off(PIOS_LED_HEARTBEAT);
            }
        }

        if (!is_zero(freqAlarm)) {
            if (ledPWM(freqAlarm)) {
                PIOS_LED_On(PIOS_LED_ALARM);
            } else {
                PIOS_LED_Off(PIOS_LED_ALARM);
            }
        }
        uint32_t time_since_boot = PIOS_DELAY_GetuSSince(boot_time);
        if ((time_since_boot > BOOT_TIMEOUT) && ((blState == BLidle) || (blState == IAPidle && !USB_connected))) {
#if defined(PIOS_INCLUDE_USB_CDC2) && defined(PIOS_INCLUDE_DEBUG_CONSOLE)
            const uint8_t msg[] = "No BOOB today, loading main firmware\r\n";
            PIOS_COM_SendBuffer(PIOS_COM_DEBUG, msg, sizeof(msg));
#endif /* PIOS_INCLUDE_USB_CDC2 && PIOS_INCLUDE_DEBUG_CONSOLE */
            jump_to_app();
        }

        processRX();
        dataDownload(start);
    }
}


// Jump to the application
void jump_to_app()
{
    const struct pios_board_info *bdinfo = &pios_board_info_blob;

    PIOS_LED_On(PIOS_LED_HEARTBEAT);
    // Look at cm3_vectors struct in startup. In a fw image the first uint32_t contains the address of the top of irqstack
    uint32_t fwIrqStackBase = (*(__IO uint32_t *)bdinfo->fw_base) & 0xFFFE0000;
    // Check for the two possible irqstack locations (sram or core coupled sram)
    if (fwIrqStackBase == 0x20000000 || fwIrqStackBase == 0x10000000) {
        uint32_t JumpAddress;
        typedef void (*pFunction)(void);
        pFunction Jump_To_Application;

        /* Jump to user application */
        FLASH_Lock();
        RCC_APB2PeriphResetCmd(0xffffffff, ENABLE);
        RCC_APB1PeriphResetCmd(0xffffffff, ENABLE);
        RCC_APB2PeriphResetCmd(0xffffffff, DISABLE);
        RCC_APB1PeriphResetCmd(0xffffffff, DISABLE);

        PIOS_USBHOOK_Deactivate();

        JumpAddress = *(__IO uint32_t *)(bdinfo->fw_base + 4);
        Jump_To_Application = (pFunction)JumpAddress;
        /* Initialize user application's Stack Pointer */
        __set_MSP(*(__IO uint32_t *)bdinfo->fw_base);
        Jump_To_Application();
    } else {
        blState = failed_jump;
        return;
    }
}

// Determine if LED should be on (true) or off (false)
bool ledPWM(float sweep_freq)
{
    uint32_t timebase       = PIOS_DELAY_GetuS();
    uint32_t sweep_duration = 1000000 / sweep_freq;
    float brightness        = (float)(timebase % sweep_duration) / sweep_duration;
    float pwm_position      = timebase % LED_PWM_PERIOD;
    // curr_sweep counts once per full pass
    uint32_t curr_sweep     = timebase / sweep_duration;

    // Reverse direction in odd sweeps
    if (curr_sweep & 1) {
        brightness = 1.0f - brightness;
    }

    // If the count within the current period is greater than the pwm_duty turn on the LED
    return (pwm_position > (brightness * LED_PWM_PERIOD)) ? true : false;
}

extern void resendData();

uint8_t processRX()
{
    uint8_t buf[64];
    uint16_t rx_bytes;
    static uint32_t last_rx = 0;
    static uint32_t interval_rx;

    rx_bytes = PIOS_COM_ReceiveBuffer(PIOS_COM_TELEM_USB, buf, sizeof(buf), 0);
    if (rx_bytes > 0) {
        last_rx = PIOS_DELAY_GetuS();
        processComand(buf);
    } else {
        // El Capitan's first release drops data being sent to it, so retry if needed
        if (last_rx == 0) {
            last_rx = PIOS_DELAY_GetuS();
        }
        interval_rx = PIOS_DELAY_GetuSSince(last_rx);
        if (interval_rx > RETRANSMIT_TIMEOUT) {
            last_rx = PIOS_DELAY_GetuS();
            resendData();
        }
    }

    return true;
}

/**
 * Check the brown out reset threshold is 2.7 volts and if not
 * resets it.  This solves an issue that can prevent boards
 * powering up with some BEC
 */
void checkBOR()
{
    uint8_t bor = FLASH_OB_GetBOR();

    if (bor != OB_BOR_LEVEL3) {
        FLASH_OB_Unlock();
        FLASH_OB_BORConfig(OB_BOR_LEVEL3);
        FLASH_OB_Launch();
        while (FLASH_WaitForLastOperation() == FLASH_BUSY) {
            ;
        }
        FLASH_OB_Lock();
        while (FLASH_WaitForLastOperation() == FLASH_BUSY) {
            ;
        }
    }
}

int32_t platformSendData(const uint8_t *msg, uint16_t msg_len)
{
    return PIOS_COM_SendBuffer(PIOS_COM_TELEM_USB, msg, msg_len);
}

#if defined(PIOS_INCLUDE_USB_CDC2) && defined(PIOS_INCLUDE_DEBUG_CONSOLE)
void platformSendDebugMsg(const uint8_t *msg, uint16_t msg_len)
{
    PIOS_COM_SendBuffer(PIOS_COM_DEBUG, msg, msg_len);
}
#endif /* PIOS_INCLUDE_USB_CDC2 && PIOS_INCLUDE_DEBUG_CONSOLE */
