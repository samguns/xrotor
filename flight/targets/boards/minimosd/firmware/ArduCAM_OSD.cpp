/**
 ******************************************************************************
 * @file       ArduCAM_OSD.cpp
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @addtogroup OpenPilotSystem OpenPilot System
 * @{
 * @addtogroup OpenPilotCore OpenPilot Core
 * @{
 * @brief Initialisation and main loop
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/*

   Copyright (c) 2011.  All rights reserved.
   An Open Source Arduino based OSD and Camera Control project.

   Program  : ArduCAM-OSD (Supports the variant: minimOSD)
   Version  : V1.9, 14 February 2012
   Author(s): Sandro Benigno
   Coauthor(s):
   Jani Hirvinen   (All the EEPROM routines)
   Michael Oborne  (OSD Configutator)
   Mike Smith      (BetterStream and Fast Serial libraries)
   Special Contribuitor:
   Andrew Tridgell by all the support on MAVLink
   Doug Weibel by his great orientation since the start of this project
   Contributors: James Goppert, Max Levine
   and all other members of DIY Drones Dev team
   Thanks to: Chris Anderson, Jordi Munoz


   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>

 */

/* ************************************************************ */
/* **************** MAIN PROGRAM - MODULES ******************** */
/* ************************************************************ */

#undef PROGMEM
#define PROGMEM __attribute__((section(".progmem.data")))

#undef PSTR
#define PSTR(s) \
    (__extension__({ static prog_char __c[] PROGMEM = (s); &__c[0]; } \
                   ))


/* **********************************************/
/* ***************** INCLUDES *******************/

// #define membug
// #define FORCEINIT  // You should never use this unless you know what you are doing


// AVR Includes
#include <FastSerial.h>
#include <AP_Common.h>
#include <AP_Math.h>
#include <math.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
// Get the common arduino functions
#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#else
#include "wiring.h"
#endif
#include <SimpleTimer.h>

#ifdef membug
#include <MemoryFree.h>
#endif

// Configurations
#include "OSD_Config.h"
#include "ArduCam_Max7456.h"
#include "OSD_Vars.h"
#include "BOOT_Func.h"
#include "OSD_Func.h"
#include "OSD_Panels.h"

#include "UAVTalk.h"
#include "FlightBatt.h"
#include "AnalogRssi.h"

// Global configuration for minimosd
extern MinimOSDsettingsDataPacked minimosd_config;

/* *************************************************/
/* ***************** DEFINITIONS *******************/

// OSD Hardware
#define MinimOSD

#define TELEMETRY_SPEED 57600 // How fast our MAVLink telemetry is coming to Serial port

// Objects and Serial definitions
FastSerialPort0(Serial);
OSD *osd; // OSD object

SimpleTimer displayTimer;

void unplugSlaves();
void OnDisplayTimer();

/* **********************************************/
void uploadFont()
{
    uint16_t byte_count = 0;
    byte bit_count;
    byte ascii_binary[0x08];

    // move these local to prevent ram usage
    uint8_t character_bitmap[0x40];
    int font_count = 0;

    osd->clear();
    osd->setPanel(6, 9);
    osd->openPanel();
    osd->printf_P(PSTR("Update CharSet"));
    osd->closePanel();


    Serial.printf_P(PSTR("Ready for Font\n"));

    while (font_count < 256) {
        int8_t incomingByte = Serial.read();
        switch (incomingByte) { // parse and decode mcm file
        case 0x0d: // carridge return, end of line
            // Serial.println("cr");
            if (bit_count == 8 && (ascii_binary[0] == 0x30 || ascii_binary[0] == 0x31)) {
                // turn 8 ascii binary bytes to single byte '01010101' = 0x55
                // fill in 64 bytes of character data
                // made this local to prevent needing a global
                byte ascii_byte;

                ascii_byte = 0;

                if (ascii_binary[0] == 0x31) { // ascii '1'
                    ascii_byte = ascii_byte + 128;
                }

                if (ascii_binary[1] == 0x31) {
                    ascii_byte = ascii_byte + 64;
                }

                if (ascii_binary[2] == 0x31) {
                    ascii_byte = ascii_byte + 32;
                }

                if (ascii_binary[3] == 0x31) {
                    ascii_byte = ascii_byte + 16;
                }

                if (ascii_binary[4] == 0x31) {
                    ascii_byte = ascii_byte + 8;
                }

                if (ascii_binary[5] == 0x31) {
                    ascii_byte = ascii_byte + 4;
                }

                if (ascii_binary[6] == 0x31) {
                    ascii_byte = ascii_byte + 2;
                }

                if (ascii_binary[7] == 0x31) {
                    ascii_byte = ascii_byte + 1;
                }

                character_bitmap[byte_count] = ascii_byte;
                byte_count++;
                bit_count = 0;
            } else {
                bit_count = 0;
            }
            break;
        case 0x0a: // line feed, ignore
            // Serial.println("ln");
            break;
        case 0x30: // ascii '0'
        case 0x31: // ascii '1'
            ascii_binary[bit_count] = incomingByte;
            bit_count++;
            break;
        default:
            break;
        }

        // we have one completed character
        // write the character to NVM
        if (byte_count == 64) {
            osd->write_NVM(font_count, character_bitmap);
            byte_count = 0;
            font_count++;
            Serial.printf_P(PSTR("Char Done\n"));
        }
    }

    // character_bitmap[]
}
/* ***************** SETUP() *******************/

void setup()
{
    pinMode(MAX7456_SELECT, OUTPUT); // OSD CS

    Serial.begin(TELEMETRY_SPEED);
    // setup mavlink port

#ifdef membug
    Serial.println(freeMem());
#endif

    osd = new (OSD);
    // Prepare OSD for displaying
    unplugSlaves();
    osd->init();

    // Start
    startPanels();
    delay(500);

    // OSD debug for development (Shown at start)
#ifdef membug
    osd->setPanel(1, 1);
    osd->openPanel();
    osd->printf("%i", freeMem());
    osd->closePanel();
#endif

    // Just to easy up development things
#ifdef FORCEINIT
    InitializeOSD();
#endif

    // Show bootloader bar
    loadBar();

// JRChange: Flight Batt on MinimOSD:
    flight_batt_init();

    analog_rssi_init();

    // Startup MAVLink timers
    displayTimer.Set(&OnDisplayTimer, 100);

    // House cleaning, clear display and enable timers
    osd->clear();
    displayTimer.Enable();
} // END of setup();


/* ***********************************************/
/* ***************** MAIN LOOP *******************/

// Mother of all happenings, The loop()
// As simple as possible.
void loop()
{
    if (uavtalk_read()) {
        OnDisplayTimer();
    } else {
        displayTimer.Run();
    }
}

/* *********************************************** */
/* ******** functions used in main loop() ******** */
void OnDisplayTimer() // duration is up to approx. 10ms depending on choosen display features
{
    if (minimosd_config.battery_source == MINIMOSDSETTINGS_BATTERY_SOURCE_OSD) {
        flight_batt_read();
    }

    if (minimosd_config.rssi_source == MINIMOSDSETTINGS_RSSI_SOURCE_OSD) {
        analog_rssi_read();
        rssi = (int16_t)osd_rssi;
        if (!minimosd_config.rssi_raw) {
            rssi = (int16_t)((float)(rssi - minimosd_config.rssi_cal_min) / (float)(minimosd_config.rssi_cal_max - minimosd_config.rssi_cal_min) * 100.0f);
        }
        if (rssi < -99) {
            rssi = -99;
        }
    } else {
        rssi = oplm_rssi;
    }

#ifdef JR_SPECIALS
    calculateCompassPoint(); // calculate the compass point which is shown in panHeading
#endif

    updateTravelDistance(); // calculate travel distance
    setHeadingPattern(); // generate the heading pattern
    setHomeVars(osd); // calculate and set Distance from home and Direction to home
    writePanels(); // writing enabled panels (check OSD_Panels Tab)
}


void unplugSlaves()
{
    // Unplug list of SPI
    digitalWrite(MAX7456_SELECT, HIGH); // unplug OSD
}
