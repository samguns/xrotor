/**
 ******************************************************************************
 * @file       Spi.cpp
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @addtogroup OpenPilotSystem OpenPilot System
 * @{
 * @addtogroup OpenPilotCore OpenPilot Core
 * @{
 * @brief SPI bus driver
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

// Get the common arduino functions
#if defined(ARDUINO) && ARDUINO >= 100
        #include "Arduino.h"
#else
        #include "wiring.h"
#endif
#include "Spi.h"

// ---------- constructor ----------------------------------------------------

SPI::SPI()
{
    // initialize the SPI pins
    pinMode(SCK_PIN, OUTPUT);
    pinMode(MOSI_PIN, OUTPUT);
    pinMode(MISO_PIN, INPUT);
    pinMode(SS_PIN, OUTPUT); // <------- !!! (Remember! This pin will select USB host chip Max3421)

    // enable SPI Master, MSB, SPI mode 0, FOSC/4
    mode(0);
}

// ------------------ mode ---------------------------------------------------

void SPI::mode(byte config)
{
    byte tmp;

    // enable SPI master with configuration byte specified
    SPCR = 0;
    SPCR = (config & 0x7F) | (1 << SPE) | (1 << MSTR);
    tmp  = SPSR;
    tmp  = SPDR;
}

// ------------------ transfer -----------------------------------------------

byte SPI::transfer(byte value)
{
    SPDR = value;
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    return SPDR;
}

byte SPI::transfer(byte value, byte period)
{
    SPDR = value;
    if (period > 0) {
        delayMicroseconds(period);
    }
    while (!(SPSR & (1 << SPIF))) {
        ;
    }
    return SPDR;
}


// ---------- preinstantiate SPI object --------------------------------------

SPI Spi = SPI();
