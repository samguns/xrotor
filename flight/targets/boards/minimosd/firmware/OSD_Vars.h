/**
 ******************************************************************************
 * @file       OSD_Vars.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @addtogroup OpenPilotSystem OpenPilot System
 * @{
 * @addtogroup OpenPilotCore OpenPilot Core
 * @{
 * @brief Global variables header
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

//
//
//


#include "ArduCam_Max7456.h"
#include <UAVTalk.h>

extern uint8_t overspeed;
extern uint8_t stall;

extern uint8_t battv;
extern float osd_vbat_A;
extern int16_t osd_curr_A;
extern int8_t osd_battery_remaining_A;

extern uint8_t osd_flightmode;

extern uint8_t osd_satellites_visible;
extern uint8_t osd_fix_type;
extern float osd_lat;
extern float osd_lon;
extern float osd_alt;
extern float osd_climb;
extern float osd_groundspeed;
extern float osd_travel_distance;
extern float osd_heading;

extern int16_t osd_roll;
extern int16_t osd_pitch;
extern int16_t osd_yaw;
extern uint16_t osd_throttle;

extern uint8_t osd_alt_cnt;
extern float osd_alt_prev;
extern uint8_t osd_got_home;
extern float osd_home_lat;
extern float osd_home_lon;
extern float osd_home_alt;
extern long osd_home_distance;
extern uint8_t osd_home_direction; // arrow direction pointing to home (1-16 to CW loop)

// OpenPilot UAVTalk:
extern uint8_t op_uavtalk_mode;
extern uint8_t op_alarm;
extern uint8_t osd_armed;
extern uint8_t osd_time_hour;
extern uint8_t osd_time_minute;
extern int16_t revo_baro_alt;
extern int8_t oplm_rssi;
extern uint8_t oplm_linkquality;
extern uint8_t osd_receiver_quality;
extern float osd_txpid_cur[3];
// Calibration state
extern CalibrationStatusOperationOptions osd_cal_Operation;
extern CalibrationStatusOperationStateOptions osd_cal_OperationState;
extern CalibrationStatusErrorOptions osd_cal_Error;
extern CalibrationStatusLevelCalibrationStateOptions osd_cal_LevelCalibrationState;
extern CalibrationStatusGyroBiasCalibrationStateOptions osd_cal_GyroBiasCalibrationState;
extern CalibrationStatusMagCalibrationStateOptions osd_cal_MagCalibrationState;
extern CalibrationStatusAccelCalibrationStateOptions osd_cal_AccelCalibrationState;
extern CalibrationStatusTxCalibrationStateOptions osd_cal_TxCalibrationState;
extern uint16_t osd_channel[9];

// Flight Batt on MinimOSD:
extern int volt_div_ratio;
extern int curr_amp_per_volt;
extern int curr_amp_offset;
// Flight Batt on MinimOSD and Revo
extern uint16_t osd_total_A;
// Flight Batt on Revo
extern uint16_t osd_est_flight_time;

// *************************************************************************************************************
// rssi variables
extern uint8_t osd_rssi;
extern int16_t rssi;
