/**
 ******************************************************************************
 * @file       OSD_Panels.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @addtogroup OpenPilotSystem OpenPilot System
 * @{
 * @addtogroup OpenPilotCore OpenPilot Core
 * @{
 * @brief Panel widget display header
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef OSD_PANELS_H
#define OSD_PANELS_H

void startPanels();
void writePanels();
void switchPanels();
void panBoot(int first_col, int first_line);

// Extract functions (get bits from the positioning bytes
#define ISa(panel, whichBit) getBit(panA_REG[panel], whichBit)
#define ISb(panel, whichBit) getBit(panB_REG[panel], whichBit)
#define ISc(panel, whichBit) getBit(panC_REG[panel], whichBit)
#define ISd(panel, whichBit) getBit(panD_REG[panel], whichBit)
#define ISe(panel, whichBit) getBit(panE_REG[panel], whichBit)

#endif /* OSD_PANELS_H */
