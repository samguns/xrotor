/**
 ******************************************************************************
 * @file       BOOT_Func.cpp.cpp
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @addtogroup OpenPilotSystem OpenPilot System
 * @{
 * @addtogroup OpenPilotCore OpenPilot Core
 * @{
 * @brief Boot splash screen
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "Arduino.h"
#include "OSD_Panels.h"
#include "ArduCam_Max7456.h"

/* ******************************************************************/
/* *********************** BOOT UP FUNCTIONS ********************** */


///////////////////////////////////////////////////////
// Function: loadBar(void)
//
// On bootup time we will show loading bar for defined BOOTTIME seconds
// This is interesting to avoid writing to APM during bootup if OSD's TX is connected
// After that, it continue in normal mode eg starting to listen MAVLink commands

#ifdef USE_WITH_MINRXOSD
#define BOOTTIME 8000 // Time in milliseconds that we show boot loading bar and wait user input
#define barX     5
#define barY     13
#else
#define BOOTTIME 2000 // Time in milliseconds that we show boot loading bar and wait user input
#define barX     5
#define barY     12
#endif

void loadBar() // change name due we don't have CLI anymore
{
    int waitTimer;
    byte barStep = 0;

    // Write plain panel to let users know what to do
    panBoot(barX, barY);

    delay(500); // To give small extra waittime to users
// Serial.flush();

    // Our main loop to wait input from user.
    for (waitTimer = 0; waitTimer <= BOOTTIME; waitTimer++) {
        // How often we update our progress bar is depending on modulo
        if (waitTimer % (BOOTTIME / 8) == 0) {
            barStep++;

            // Update bar it self
            osd->setPanel(barX + 12, barY);
            osd->openPanel();
            switch (barStep) {
            case 1:
                osd->printf_P(PSTR("\xf1\xf2\xf2\xf2\xf2\xf2\xf2"));
                break;
            case 2:
                osd->printf_P(PSTR("\xef\xf2\xf2\xf2\xf2\xf2\xf2"));
                break;
            case 3:
                osd->printf_P(PSTR("\xee\xf0\xf2\xf2\xf2\xf2\xf2"));
                break;
            case 4:
                osd->printf_P(PSTR("\xee\xee\xf0\xf2\xf2\xf2\xf2"));
                break;
            case 5:
                osd->printf_P(PSTR("\xee\xee\xee\xf0\xf2\xf2\xf2"));
                break;
            case 6:
                osd->printf_P(PSTR("\xee\xee\xee\xee\xf0\xf2\xf2"));
                break;
            case 7:
                osd->printf_P(PSTR("\xee\xee\xee\xee\xee\xf0\xf2"));
                break;
            case 8:
                osd->printf_P(PSTR("\xee\xee\xee\xee\xee\xee\xf0"));
                break;
            case 9:
                osd->printf_P(PSTR("\xee\xee\xee\xee\xee\xee\xee"));
                break;
            }
            osd->closePanel();
        }

        delay(1); // Minor delay to make sure that we stay here long enough
    }
}
