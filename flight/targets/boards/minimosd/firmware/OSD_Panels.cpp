/**
 ******************************************************************************
 * @file       OSD_Panels.cpp
 * @author     Steve Evans
 * @brief Panel widget display
 *****************************************************************************/

#include "Arduino.h"
#include "ArduCam_Max7456.h"
#include "OSD_Config.h"
#include "OSD_Func.h"
#include "OSD_version.h"
#include "OSD_Panels.h"
#include "UAVTalk.h"

/* On changes to calibrationstatus, check dependencies which include sensor cal status states */
#if CALIBRATIONSTATUS_OBJID != 0xC1E0148A
#error CalibrationStatus UAVO has changed
#endif

// Global configuration for minimosd
extern MinimOSDsettingsDataPacked minimosd_config;

#ifdef FLIGHT_BATT_ON_MINIMOSD
#include "FlightBatt.h"
#endif

#define WARN_FLASH_TIME   1000    // [ms]	time with which the warnings are flashing
#define WARN_RECOVER_TIME 4000 // [ms]	time we stay in the first panel after last warning
#define WARN_MAX          6       // number of implemented warnings

#define MODE_SWITCH_TIME  2000    // [ms]	time for mode switching

#define TIME_RESET_AMPERE 2 // [A]	current above which the on time is set to 00:00

// Position calibration display
#define CALIBRATION_COL   2
#define CALIBRATION_ROW   1

/******* GLOBAL VARS *******/

static boolean warning_active       = false;

static float convert_speed          = 0;
static float convert_length         = 0;
static int16_t convert_length_large = 0;
static uint8_t unit_speed           = 0;
static uint8_t unit_length          = 0;
static uint8_t unit_length_large    = 0;

uint8_t overspeed         = 0;
uint8_t stall             = 0;

uint8_t battv             = 0;                      // Battery warning voltage - units Volt *10
float osd_vbat_A          = 0;                 // Battery A voltage in milivolt
int16_t osd_curr_A        = 0;                 // Battery A current
uint8_t batt_warn_level   = 0;

uint8_t osd_flightmode    = 0;                   // FlightMode of the FlightControl

uint8_t osd_satellites_visible = 0; // number of satelites
uint8_t osd_fix_type      = 0;               // GPS lock 0-1=no fix, 2=2D, 3=3D
float osd_lat             = 0;                    // latitude
float osd_lon             = 0;                    // longitude
float osd_alt             = 0;                    // altitude
float osd_climb           = 0;                  // climb rate
float osd_groundspeed     = 0;            // ground speed
float osd_travel_distance = 0; // travel distance
float osd_heading         = 0;                // ground course heading

int16_t osd_roll          = 0;                   // roll from FC
int16_t osd_pitch         = 0;                  // pitch from FC
int16_t osd_yaw           = 0;                    // relative heading form FC
uint16_t osd_throttle     = 0;               // throttle

uint8_t osd_alt_cnt       = 0;                // counter for stable osd_alt
float osd_alt_prev        = 0;               // previous altitude
uint8_t osd_got_home      = 0;               // tells if got home position or not
float osd_home_lat        = 0;               // home latitude
float osd_home_lon        = 0;               // home longitude
float osd_home_alt        = 0;               // home altitude
long osd_home_distance    = 0;          // distance from home
uint8_t osd_home_direction; // arrow direction pointing to home (1-16 to CW loop)

// OpenPilot UAVTalk:
uint8_t op_uavtalk_mode      = 0;            // OP UAVTalk mode, start with normal behavior
uint8_t op_alarm = 0; // OP alarm info
uint8_t osd_armed = 0; // OP armed info
uint8_t osd_time_hour        = 0;              // OP GPS time hour info
uint8_t osd_time_minute      = 0;            // OP GPS tiem minute info
int16_t revo_baro_alt        = 0;              // Revo baro altitude
int8_t oplm_rssi = 0; // OPLM RSSI
uint8_t oplm_linkquality     = 0;           // OPLM LinkQuality
uint8_t osd_receiver_quality = 0; // Receiver Link Quality
float osd_txpid_cur[3]       = {};          // Current TXPID setting

// Calibration state
CalibrationStatusOperationOptions osd_cal_Operation = CALIBRATIONSTATUS_OPERATION_NONE;
CalibrationStatusOperationStateOptions osd_cal_OperationState;
CalibrationStatusErrorOptions osd_cal_Error;
CalibrationStatusLevelCalibrationStateOptions osd_cal_LevelCalibrationState;
CalibrationStatusGyroBiasCalibrationStateOptions osd_cal_GyroBiasCalibrationState;
CalibrationStatusMagCalibrationStateOptions osd_cal_MagCalibrationState;
CalibrationStatusAccelCalibrationStateOptions osd_cal_AccelCalibrationState;
CalibrationStatusTxCalibrationStateOptions osd_cal_TxCalibrationState;

uint16_t osd_channel[9] = {};

// Flight Batt on MinimOSD:
int volt_div_ratio    = 0;             // Volt * 100
int curr_amp_per_volt = 0; // Ampere * 100
int curr_amp_offset   = 0;            // Ampere * 10000
// Flight Batt on MinimOSD and Revo
uint16_t osd_total_A  = 0;                // Battery total current [mAh]
// Flight Batt on Revo
uint16_t osd_est_flight_time = 0; // Battery estimated flight time [sec]

// *************************************************************************************************************
// rssi variables
uint8_t osd_rssi = 0; // value from PacketRxOk or analogRSSI
int16_t rssi     = -99; // scaled value 0-100%

/******* MAIN FUNCTIONS *******/

/******* SPECIAL PANELS *******/

// Show those fancy 2 char arrows
#define ARROW_CODE (0x90 - 2) // code of the first MAX7456 special arrow char -2
void showArrow(uint8_t rotate_arrow)
{
    rotate_arrow = (rotate_arrow < 2) ? 2 : rotate_arrow * 2;
    osd->printf("%c%c", ARROW_CODE + rotate_arrow, ARROW_CODE + rotate_arrow + 1);
}


#ifdef AH_ORIGINAL_VERSION

// Calculate and shows Artificial Horizon
void showHorizon(int start_col, int start_row)
{
    int x, nose, row, minval, hit, subval = 0;
    const int cols = 12;
    const int rows = 5;
    int col_hit[cols];
    float pitch, roll;

    (abs(osd_pitch) == 90) ? pitch = 89.99 * (90 / osd_pitch) * -0.017453293 : pitch = osd_pitch * -0.017453293;
    (abs(osd_roll) == 90) ? roll   = 89.99 * (90 / osd_roll) * 0.017453293 : roll = osd_roll * 0.017453293;

    nose = round(tan(pitch) * (rows * 9));
    for (int col = 1; col <= cols; col++) {
        x = (col * 12) - (cols * 6) - 6; // center X point at middle of each col
        col_hit[col - 1] = (tan(roll) * x) + nose + (rows * 9) - 1; // calculating hit point on Y plus offset to eliminate negative values
        // col_hit[(col-1)] = nose + (rows * 9);
    }

    for (int col = 0; col < cols; col++) {
        hit = col_hit[col];
        if (hit > 0 && hit < (rows * 18)) {
            row    = rows - ((hit - 1) / 18);
            minval = rows * 18 - row * 18 + 1;
            subval = hit - minval;
            subval = round((subval * 9) / 18);
            if (subval == 0) {
                subval = 1;
            }
            printHit(start_col + col, start_row + row - 1, subval);
        }
    }
}

void printHit(byte col, byte row, byte subval)
{
    osd->openSingle(col, row);
    char subval_char;
    switch (subval) {
    case 1:
        // osd->printf_P(PSTR("\x06"));
        subval_char = 0x06;
        break;
    case 2:
        // osd->printf_P(PSTR("\x07"));
        subval_char = 0x07;
        break;
    case 3:
        // osd->printf_P(PSTR("\x08"));
        subval_char = 0x08;
        break;
    case 4:
        // osd->printf_P(PSTR("\x09"));
        subval_char = 0x09;
        break;
    case 5:
        // osd->printf_P(PSTR("\x0a"));
        subval_char = 0x0a;
        break;
    case 6:
        // osd->printf_P(PSTR("\x0b"));
        subval_char = 0x0b;
        break;
    case 7:
        // osd->printf_P(PSTR("\x0c"));
        subval_char = 0x0c;
        break;
    case 8:
        // osd->printf_P(PSTR("\x0d"));
        subval_char = 0x0d;
        break;
    case 9:
        // osd->printf_P(PSTR("\x0e"));
        subval_char = 0x0e;
        break;
    }
    osd->printf("%c", subval_char);
}

#endif // AH_ORIGINAL_VERSION


#ifdef AH_REFACTORED_VERSION
// with different factors we can adapt do different cam optics
#define AH_PITCH_FACTOR  0.017453293             // conversion factor for pitch
#define AH_ROLL_FACTOR   0.017453293             // conversion factor for roll
#define AH_COLS          12                      // number of artificial horizon columns
#define AH_ROWS          5                       // number of artificial horizon rows
#define CHAR_COLS        12                      // number of MAX7456 char columns
#define CHAR_ROWS        18                      // number of MAX7456 char rows
#define CHAR_SPECIAL     9                       // number of MAX7456 special chars for the artificial horizon
#define LINE_CODE        (0x06 - 1)              // code of the first MAX7456 special char -1
#define AH_TOTAL_LINES   AH_ROWS * CHAR_ROWS
#define AH_SPECIAL_LINES AH_ROWS * CHAR_SPECIAL

// Calculate and show artificial horizon
void showHorizon(int start_col, int start_row)
{
    int col, row, pitch_line, middle, hit, subval;

    pitch_line = round(tan(-AH_PITCH_FACTOR * osd_pitch) * AH_SPECIAL_LINES);
    for (col = 1; col <= AH_COLS; col++) {
        middle = col * CHAR_COLS - (AH_COLS * CHAR_COLS / 2) - CHAR_COLS / 2; // center X point at middle of each column
        hit    = tan(AH_ROLL_FACTOR * osd_roll) * middle + pitch_line + AH_SPECIAL_LINES - 1;      // calculating hit point on Y plus offset to eliminate negative values
        if (hit > 0 && hit < AH_TOTAL_LINES) {
            row    = AH_ROWS - ((hit - 1) / CHAR_ROWS);
            subval = hit - (AH_TOTAL_LINES - row * CHAR_ROWS + 1);
            subval = round(subval * CHAR_SPECIAL / CHAR_ROWS);
            if (subval == 0) {
                subval = 1;
            }
            osd->openSingle(start_col + col - 1, start_row + row - 1);
            osd->printf("%c", LINE_CODE + subval);
        }
    }
}

#endif // AH_REFACTORED_VERSION


#ifdef AH_ZERO_CENTERED

// For using this, you must load a special mcm file where the artificial horizon zero left/right arrows are centered!
// e.g. AH_ZeroCentered002.mcm
// with different factors we can adapt do different cam optics
#define AH_PITCH_FACTOR 0.010471976 // conversion factor for pitch
#define AH_ROLL_FACTOR  0.017453293             // conversion factor for roll
#define AH_COLS         12                      // number of artificial horizon columns
#define AH_ROWS         5                       // number of artificial horizon rows
#define CHAR_COLS       12                      // number of MAX7456 char columns
#define CHAR_ROWS       18                      // number of MAX7456 char rows
#define CHAR_SPECIAL    9                       // number of MAX7456 special chars for the artificial horizon
#define LINE_CODE       (0x06 - 1)              // code of the first MAX7456 special char -1
#define AH_TOTAL_LINES  AH_ROWS * CHAR_ROWS

// Calculate and show artificial horizon
// used formula: y = m * x + n <=> y = tan(a) * x + n
void showHorizon(int start_col, int start_row)
{
    int col, row, pitch_line, middle, hit, subval;

    pitch_line = round(tan(-AH_PITCH_FACTOR * osd_pitch) * AH_TOTAL_LINES) + AH_TOTAL_LINES / 2; // 90 total lines
    for (col = 1; col <= AH_COLS; col++) {
        middle = col * CHAR_COLS - (AH_COLS / 2 * CHAR_COLS) - CHAR_COLS / 2; // -66 to +66	center X point at middle of each column
        hit    = tan(AH_ROLL_FACTOR * osd_roll) * middle + pitch_line;               // 1 to 90	calculating hit point on Y plus offset
        if (hit >= 1 && hit <= AH_TOTAL_LINES) {
            row    = (hit - 1) / CHAR_ROWS;                                            // 1 to 5 bottom-up
            subval = (hit - (row * CHAR_ROWS) + 1) / (CHAR_ROWS / CHAR_SPECIAL); // 1 to 9
            osd->openSingle(start_col + col - 1, start_row + AH_ROWS - row - 1);
            osd->printf("%c", LINE_CODE + subval);
        }
    }
}

#endif // AH_ZERO_CENTERED


#ifdef AH_BETTER_RESOLUTION

// For using this, you must load a special mcm file with the new staggered artificial horizon chars!
// e.g. AH_BetterResolutionCharset002.mcm
// with different factors we can adapt do different cam optics
#define AH_PITCH_FACTOR      0.010471976             // conversion factor for pitch
#define AH_ROLL_FACTOR       0.017453293             // conversion factor for roll
#define AH_COLS              12                      // number of artificial horizon columns
#define AH_ROWS              5                       // number of artificial horizon rows
#define CHAR_COLS            12                      // number of MAX7456 char columns
#define CHAR_ROWS            18                      // number of MAX7456 char rows
#define CHAR_SPECIAL         9                       // number of MAX7456 special chars for the artificial horizon
#define AH_TOTAL_LINES       AH_ROWS * CHAR_ROWS     // helper define

#define LINE_SET_STRAIGHT__  (0x06 - 1)              // code of the first MAX7456 straight char -1
#define LINE_SET_STRAIGHT_O  (0x3B - 3)              // code of the first MAX7456 straight overflow char -3
#define LINE_SET_P___STAG_1  (0x3C - 1)              // code of the first MAX7456 positive staggered set 1 char -1
#define LINE_SET_P___STAG_2  (0x45 - 1)              // code of the first MAX7456 positive staggered set 2 char -1
#define LINE_SET_N___STAG_1  (0x4E - 1)              // code of the first MAX7456 negative staggered set 1 char -1
#define LINE_SET_N___STAG_2  (0x57 - 1)              // code of the first MAX7456 negative staggered set 2 char -1
#define LINE_SET_P_O_STAG_1  (0xD4 - 2)              // code of the first MAX7456 positive overflow staggered set 1 char -2
#define LINE_SET_P_O_STAG_2  (0xDA - 1)              // code of the first MAX7456 positive overflow staggered set 2 char -1
#define LINE_SET_N_O_STAG_1  (0xD6 - 2)              // code of the first MAX7456 negative overflow staggered set 1 char -2
#define LINE_SET_N_O_STAG_2  (0xDD - 1)              // code of the first MAX7456 negative overflow staggered set 2 char -1

#define OVERFLOW_CHAR_OFFSET 6 // offset for the overflow subvals

#define ANGLE_1              9                       // angle above we switch to line set 1
#define ANGLE_2              25                      // angle above we switch to line set 2

// Calculate and show artificial horizon
// used formula: y = m * x + n <=> y = tan(a) * x + n
void showHorizon(int start_col, int start_row)
{
    int col, row, pitch_line, middle, hit, subval;
    int roll;
    int line_set = LINE_SET_STRAIGHT__;
    int line_set_overflow = LINE_SET_STRAIGHT_O;
    int subval_overflow   = 9;

    // preset the line char attributes
    roll = osd_roll;
    if ((roll >= 0 && roll < 90) || (roll >= -179 && roll < -90)) { // positive angle line chars
        roll = roll < 0 ? roll + 179 : roll;
        if (abs(roll) > ANGLE_2) {
            line_set = LINE_SET_P___STAG_2;
            line_set_overflow = LINE_SET_P_O_STAG_2;
            subval_overflow = 7;
        } else if (abs(roll) > ANGLE_1) {
            line_set = LINE_SET_P___STAG_1;
            line_set_overflow = LINE_SET_P_O_STAG_1;
            subval_overflow = 8;
        }
    } else { // negative angle line chars
        roll = roll > 90 ? roll - 179 : roll;
        if (abs(roll) > ANGLE_2) {
            line_set = LINE_SET_N___STAG_2;
            line_set_overflow = LINE_SET_N_O_STAG_2;
            subval_overflow = 7;
        } else if (abs(roll) > ANGLE_1) {
            line_set = LINE_SET_N___STAG_1;
            line_set_overflow = LINE_SET_N_O_STAG_1;
            subval_overflow = 8;
        }
    }

    pitch_line = round(tan(-AH_PITCH_FACTOR * osd_pitch) * AH_TOTAL_LINES) + AH_TOTAL_LINES / 2; // 90 total lines
    for (col = 1; col <= AH_COLS; col++) {
        middle = col * CHAR_COLS - (AH_COLS / 2 * CHAR_COLS) - CHAR_COLS / 2; // -66 to +66	center X point at middle of each column
        hit    = tan(AH_ROLL_FACTOR * osd_roll) * middle + pitch_line;               // 1 to 90	calculating hit point on Y plus offset
        if (hit >= 1 && hit <= AH_TOTAL_LINES) {
            row    = (hit - 1) / CHAR_ROWS;                                            // 0 to 4 bottom-up
            subval = (hit - (row * CHAR_ROWS) + 1) / (CHAR_ROWS / CHAR_SPECIAL); // 1 to 9

            // print the line char
            osd->openSingle(start_col + col - 1, start_row + AH_ROWS - row - 1);
            osd->printf("%c", line_set + subval);

            // check if we have to print an overflow line char
            if (subval >= subval_overflow && row < 4) { // only if it is a char which needs overflow and if it is not the upper most row
                osd->openSingle(start_col + col - 1, start_row + AH_ROWS - row - 2);
                osd->printf("%c", line_set_overflow + subval - OVERFLOW_CHAR_OFFSET);
            }
        }
    }
}

#endif // AH_BETTER_RESOLUTION


/******************************************************************/
// Panel  : panOff
// Needs  : -
// Output : -
/******************************************************************/
void panOff(void)
{}


/******************************************************************/
// Panel  : panWarn
// Needs  : X, Y locations
// Output : Warnings if there are any
/******************************************************************/
void panWarn(int first_col, int first_line)
{
    static char *warning_string;
    static uint8_t last_warning_type        = 1;
    static uint8_t warning_type = 0;
    static unsigned long warn_text_timer    = 0;
    static unsigned long warn_recover_timer = 0;
    int cycle;

    if (millis() > warn_text_timer) { // if the text or blank text has been shown for a while
        if (warning_type) { // there was a warning, so we now blank it out for a while
            last_warning_type = warning_type; // save the warning type for cycling
            warning_type = 0;
            warning_string    = "            ";                    // blank the warning
            warn_text_timer   = millis() + WARN_FLASH_TIME / 2;   // set clear warning time
        } else {
            cycle = last_warning_type; // start the warning checks cycle where we left it last time
            do { // cycle through the warning checks
                if (++cycle > WARN_MAX) {
                    cycle = 1;
                }
                switch (cycle) {
                case 1: // DISARMED
                    if (osd_armed < 2) {
                        warning_type   = cycle;
                        warning_string = "  disarmed  ";
                    }
                    break;
                case 2: // No telemetry communication
                    if (uavtalk_state() != TELEMETRYSTATS_STATE_CONNECTED) {
                        warning_type   = cycle;
                        warning_string = " no tel com ";
                    }
                    break;
                case 3: // NO GPS FIX
                    if (osd_fix_type < 2 && osd_got_home) { // to allow flying in the woods (what I really like) without this annoying warning,
                        warning_type   = cycle;                   // this warning is only shown if GPS was valid before (osd_got_home)
                        warning_string = " no gps fix ";
                    }
                    break;
                case 4: // BATT LOW
                    if (minimosd_config.warning_bat && (osd_vbat_A < battv / 10.0 || osd_total_A >= minimosd_config.battery_mah_warning)) {
                        warning_type   = cycle;
                        warning_string = "  batt low  ";
                    }
                    break;
                case 5: // RSSI LOW
                    if (minimosd_config.warning_rssi && rssi < minimosd_config.rssi_warning && rssi != -99 && !minimosd_config.rssi_raw) {
                        warning_type   = cycle;
                        warning_string = "  rssi low  ";
                    }
                    break;

                case 6: // LINK QUALITY
                    if (minimosd_config.warning_link_quality && osd_receiver_quality < minimosd_config.link_quality_warning) {
                        warning_type   = cycle;
                        warning_string = "link quality";
                    }
                    break;
                }
            } while (!warning_type && cycle != last_warning_type);
            if (warning_type) { // if there a warning
                warning_active     = true;                          // then set warning active
                warn_text_timer    = millis() + WARN_FLASH_TIME;   // set show warning time
                warn_recover_timer = millis() + WARN_RECOVER_TIME;
            } else { // if not, we do not want the delay, so a new error shows up immediately
                if (millis() > warn_recover_timer) { // if recover time over since last warning
                    warning_active = false; // no warning active anymore
                }
            }
        }

        osd->setPanel(first_col, first_line);
        osd->openPanel();
        osd->printf("%s", warning_string);
        osd->closePanel();
    }
}

/******* PANELS *******/


/******************************************************************/
// Panel  : panBoot
// Needs  : X, Y locations
// Output : Booting up text and empty bar after that
/******************************************************************/
void panBoot(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf_P(PSTR("booting up:\xed\xf2\xf2\xf2\xf2\xf2\xf2\xf2\xf3"));
    osd->closePanel();
}


/******************************************************************/
// Panel  : panLogo
// Needs  : X, Y locations
// Output : Startup OSD LOGO
/******************************************************************/
void panLogo()
{
    osd->setPanel(7, 3);
    osd->openPanel();
    osd->printf_P(PSTR("\x20\x20\x20\x20\x20\x20\xba\xbb\xbc\xbd\xbe|\x20\x20\x20\x20\x20\x20\xca\xcb\xcc\xcd\xce||"));
    char *ver = fwCommitTagName();
    for (int i = 0; ver[i]; i++) {
        ver[i] = tolower(ver[i]);
    }
    osd->printf("%s", ver);
    osd->closePanel();
    osd->setPanel(7, 9);
    osd->openPanel();
    osd->printf("hash: 0x%04x", (fwCommitHashPrefix() >> 16) & 0xffff);
    osd->printf("%04x", fwCommitHashPrefix() & 0xffff);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panGPSats
// Needs  : X, Y locations
// Output : 1 symbol and number of locked satellites
/******************************************************************/
void panGPSats(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
#ifdef JR_SPECIALS // I like it more this way
    osd->printf("%c%3i", 0x0f, osd_satellites_visible);
#else
    osd->printf("%c%2i", 0x0f, osd_satellites_visible);
#endif
    osd->closePanel();
}


/******************************************************************/
// Panel  : panGPL
// Needs  : X, Y locations
// Output : 1 static symbol open lock or 2D or 3D sign
/******************************************************************/
void panGPL(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%c", osd_fix_type <= 1 ? osd_fix_type * 0x10 : osd_fix_type - 1);
#ifdef OP_DEBUG // I use this place for debug info
    osd->printf(" %02x", op_alarm);
#endif
    osd->closePanel();
}


/******************************************************************/
// Panel  : panGPS
// Needs  : X, Y locations
// Output : two row numeric value of current GPS location with LAT/LON symbols
/******************************************************************/
void panGPS(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
#ifdef JR_SPECIALS // I like it more one row style
    osd->printf("%c%10.6f     %c%10.6f", 0x83, (double)(osd_lat), 0x84, (double)(osd_lon));
#else
    osd->printf("%c%11.6f|%c%11.6f", 0x83, (double)osd_lat, 0x84, (double)osd_lon);
#endif
    osd->closePanel();
}


/******************************************************************/
// Panel  : panHomeDis
// Needs  : X, Y locations
// Output : Distance to home
/******************************************************************/
void panHomeDis(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%c%5.0f%c", 0x1F, (double)((osd_home_distance) * convert_length), unit_length);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panHomeDir
// Needs  : X, Y locations
// Output : 2 symbols that are combined as one arrow, shows direction to home
/******************************************************************/
void panHomeDir(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    showArrow((uint8_t)osd_home_direction);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panHomeAlt
// Needs  : X, Y locations
// Output : Hom altitude
/******************************************************************/
void panHomeAlt(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
#ifdef REVO_ADD_ONS
    osd->printf("%c%5.0f%c", 0xE7, (double)(revo_baro_alt * convert_length), unit_length);
#else
    osd->printf("%c%5.0f%c", 0xE7, (double)((osd_alt - osd_home_alt) * convert_length), unit_length);
#endif
    osd->closePanel();
}


/******************************************************************/
// Panel  : panAlt
// Needs  : X, Y locations
// Output : Altitude
/******************************************************************/
void panAlt(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%c%5.0f%c", 0xE6, (double)(osd_alt * convert_length), unit_length);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panVel
// Needs  : X, Y locations
// Output : Velocity
/******************************************************************/
void panVel(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
#ifdef JR_SPECIALS // I like it more this way
    osd->printf("%c%5.0f%c", 0xE9, (double)(osd_groundspeed * convert_speed), unit_speed);
#else
    osd->printf("%c%3.0f%c", 0xE9, (double)(osd_groundspeed * convert_speed), unit_speed);
#endif
    osd->closePanel();
}


/******************************************************************/
// Panel  : panDistance
// Needs  : X, Y locations
// Output : travel distance
/******************************************************************/
void panDistance(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    if ((osd_travel_distance * convert_length) > 1000.0) {
        osd->printf("%c%5.2f%c", 0xFE, ((osd_travel_distance * convert_length) / convert_length_large), unit_length_large);
    } else {
        osd->printf("%c%5.0f%c", 0xFE, (osd_travel_distance * convert_length), unit_length);
    }
    osd->closePanel();
}


/******************************************************************/
// Panel  : panClimb
// Needs  : X, Y locations
// Output : Climb Rate
/******************************************************************/
void panClimb(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
#ifdef JR_SPECIALS // I like it more this way
    osd->printf("%c%5.0f%c", 0x16, (double)(osd_climb), 0x88);
#else
    osd->printf("%c%3.0f%c", 0x16, (double)(osd_climb), 0x88);
#endif
    osd->closePanel();
}


/******************************************************************/
// Panel  : panHeading
// Needs  : X, Y locations
// Output : Symbols with numeric compass heading value
/******************************************************************/
void panHeading(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
#ifdef JR_SPECIALS // show heading with compass point
    osd->printf("%4.0f%c%s", (double)osd_heading, 0xb0, CompassPoint);
#else
    osd->printf("%4.0f%c", (double)osd_heading, 0xb0);
#endif
    osd->closePanel();
}


/******************************************************************/
// Panel  : panRose
// Needs  : X, Y locations
// Output : a dynamic compass rose that changes along the heading information
/******************************************************************/
void panRose(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%s|%c%s%c", "\x20\xc0\xc0\xc0\xc0\xc0\xc7\xc0\xc0\xc0\xc0\xc0\x20", 0xd0, buf_show, 0xd1);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panRSSI
// Needs  : X, Y locations
// Output : RSSI %
/******************************************************************/
void panRSSI(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    if (minimosd_config.rssi_enable && minimosd_config.link_quality_enable) {
        osd->printf("%c%3i%c/%3i%c", 0xE1, rssi, 0x25, osd_receiver_quality, 0x25);
    } else if (minimosd_config.rssi_enable) {
        osd->printf("%c%3i%c", 0xE1, rssi, 0x25);
    } else if (minimosd_config.link_quality_enable) {
        osd->printf("%c%3i%c", 0xE1, osd_receiver_quality, 0x25);
    }
    osd->closePanel();
#ifdef REVO_ADD_ONS
    osd->setPanel(first_col - 2, first_line - 1);
    osd->openPanel();
    osd->printf("%4i%c%3i%c", oplm_rssi, 0x8B, oplm_linkquality, 0x8C);
    osd->closePanel();
#endif
}


/******************************************************************/
// Panel  : panRoll
// Needs  : X, Y locations
// Output : -+ value of current Roll from vehicle with degree symbols and roll symbol
/******************************************************************/
void panRoll(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%4i%c%c", osd_roll, 0xb0, 0xb2);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panPitch
// Needs  : X, Y locations
// Output : -+ value of current Pitch from vehicle with degree symbols and pitch symbol
/******************************************************************/
void panPitch(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%4i%c%c", osd_pitch, 0xb0, 0xb1);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panThr
// Needs  : X, Y locations
// Output : Throttle
/******************************************************************/
void panThr(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%c%3.0i%c", 0x87, osd_throttle, 0x25);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panFlightMode
// Needs  : X, Y locations
// Output : current flight modes
/******************************************************************/
void panFlightMode(int first_col, int first_line)
{
    char *mode_str = "";

    osd->setPanel(first_col, first_line);
    osd->openPanel();
    if (osd_flightmode == 0) {
        mode_str = "man"; // MANUAL
    } else if (osd_flightmode == 1) {
        mode_str = "st1"; // STABILIZED1
    } else if (osd_flightmode == 2) {
        mode_str = "st2"; // STABILIZED2
    } else if (osd_flightmode == 3) {
        mode_str = "st3"; // STABILIZED3
    } else if (osd_flightmode == 4) {
        mode_str = "st4"; // STABILIZED4
    } else if (osd_flightmode == 5) {
        mode_str = "st5"; // STABILIZED5
    } else if (osd_flightmode == 6) {
        mode_str = "st6"; // STABILIZED6
    } else if (osd_flightmode == 7) {
        mode_str = "ph "; // POSITIONHOLD
    } else if (osd_flightmode == 8) {
        mode_str = "cl "; // COURSELOCK
    } else if (osd_flightmode == 9) {
        mode_str = "vr "; // VELOCITYROAM
    } else if (osd_flightmode == 10) {
        mode_str = "hl "; // HOMELEASH
    } else if (osd_flightmode == 11) {
        mode_str = "pa "; // ABSOLUTEPOSITION
    } else if (osd_flightmode == 12) {
        mode_str = "rtb"; // RETURNTOBASE
    } else if (osd_flightmode == 13) {
        mode_str = "lan"; // LAND
    } else if (osd_flightmode == 14) {
        mode_str = "pp "; // PATHPLANNER
    } else if (osd_flightmode == 15) {
        mode_str = "poi"; // POI
    } else if (osd_flightmode == 16) {
        mode_str = "ac "; // AUTOCRUISE
    } else if (osd_flightmode == 17) {
        mode_str = "at "; // AUTOTAKEOFF
    }
    osd->printf("%c%s", 0xE0, mode_str);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panBattery A (Voltage 1)
// Needs  : X, Y locations
// Output : Voltage value as in XX.X and symbol of over all battery status
/******************************************************************/
void panBatt_A(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%c%5.2f%c", 0xE2, (double)osd_vbat_A, 0x8E);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panCur_A
// Needs  : X, Y locations
// Output : Current
/******************************************************************/
void panCur_A(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%c%5.2f%c", 0xE4, osd_curr_A * .01, 0x8F);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panBatteryConsumed
// Needs  : X, Y locations
// Output : Battery
/******************************************************************/
void panBatteryConsumed(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%c%5i%c", 0xB9, osd_total_A, 0x82);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panTxPID
// Needs  : X, Y locations
// Output : Current TxPID settings
/******************************************************************/
void panTxPID(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%1.5f", (double)osd_txpid_cur[0]);
    osd->closePanel();
    osd->setPanel(first_col, first_line + 1);
    osd->openPanel();
    osd->printf("%1.5f", (double)osd_txpid_cur[1]);
    osd->closePanel();
    osd->setPanel(first_col, first_line + 2);
    osd->openPanel();
    osd->printf("%1.5f", (double)osd_txpid_cur[2]);
    osd->closePanel();
}


/******************************************************************/
// Panel  : panTime
// Needs  : X, Y locations
// Output : Time from bootup or start
/******************************************************************/
void panTime(int first_col, int first_line)
{
    int start_time;

    start_time = (int)(millis() / 1000);

    osd->setPanel(first_col, first_line);
    osd->openPanel();
    if (minimosd_config.battery_source == MINIMOSDSETTINGS_BATTERY_SOURCE_OSD) {
        osd->printf("%c%2i%c%02i", 0xB3, ((int)(start_time / 60)) % 60, 0x3A, start_time % 60);
    } else {
        osd->printf("%c%2i%c%02i|%c%2i%c%02i", 0xB3, ((int)(start_time / 60)) % 60, 0x3A, start_time % 60, 0x65, ((int)(osd_est_flight_time / 60)) % 60, 0x3A, osd_est_flight_time % 60);
    }
    osd->closePanel();
}


/******************************************************************/
// Panel  : panHorizon
// Needs  : X, Y locations
// Output : artificial horizon
/******************************************************************/
void panHorizon(int first_col, int first_line)
{
    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf_P(PSTR("\xc8\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\xc9|"));
    osd->printf_P(PSTR("\xc8\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\xc9|"));
    osd->printf_P(PSTR("\xd8\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\xd9|"));
    osd->printf_P(PSTR("\xc8\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\xc9|"));
    osd->printf_P(PSTR("\xc8\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\x20\xc9"));
    osd->closePanel();
    showHorizon((first_col + 1), first_line);
}


/******************************************************************/
// Panel  : panUAVPosition
// Needs  : X, Y locations of center
// Needs  : globals: osd_home_lat, osd_lat, osd_home_lon, osd_lon
// Output : shows the UAV position in a radar like style
// Status : do flight test
/******************************************************************/

#define USE_DIRECTED   // use directed UAV icons (special mcm file needed)

#define STEP_WIDTH     250                     // every STEP_WIDTH in [m] it is down-scaled

#define SCALE_X        (7.0 / STEP_WIDTH)      // SCALE_X * 2 chars grid in which the uav is drawed
#define SCALE_Y        (4.5 / STEP_WIDTH)      // SCALE_Y * 2 chars grid in which the uav is drawed

#define RADAR_CHAR     0xF9                    // code of the radar symbol
#define UAV_CHAR_START 0x17 // code of the first of 8 directed UAV icons

void panUAVPosition(int center_col, int center_line)
{
    static int last_x = 0;
    static int last_y = 0;

#ifdef USE_DIRECTED
    int index;
    index = (int)((osd_heading + 22.5) / 45.0);
    index = index > 7 ? 0 : index;
#endif

    // calculate distances from home in lat (y) and lon (x) direction in [m]
    int dy   = (int)(-111319.5 * (osd_home_lat - osd_lat));
    int dx   = (int)(-111319.5 * (osd_home_lon - osd_lon) * cos(fabs(osd_home_lat) * 0.0174532925));

    // calculate display offset in y and x direction
    int zoom = max((int)(abs(dy) / STEP_WIDTH), (int)(abs(dx) / STEP_WIDTH)) + 1;
    osd->setPanel(center_col + 8, center_line);
    osd->openPanel();
    osd->printf("%c%5i%c", RADAR_CHAR, (int)(zoom * STEP_WIDTH * convert_length), unit_length);
    osd->closePanel();
    int y = (int)(dy / (zoom / SCALE_Y));
    int x = (int)(dx / (zoom / SCALE_X) + 0.5); // for even grid correction

    // clear UAV
    osd->openSingle(center_col + last_x, center_line - last_y);
    osd->printf_P(PSTR(" "));
    last_x = x;
    last_y = y;
    // show UAV
    osd->openSingle(center_col + x, center_line - y);
#ifdef USE_DIRECTED
    osd->printf("%c", UAV_CHAR_START + index);
#else
    osd->printf_P(PSTR("\xF4"));
#endif
    // show home
    osd->setPanel(center_col, center_line);
    osd->openPanel();
    osd->printf_P(PSTR("\xF5\xF6"));
    osd->closePanel();
}

/******* HELPER FUNCTIONS *******/

void set_converts()
{
    if (minimosd_config.units == MINIMOSDSETTINGS_UNITS_METRIC) {
        convert_speed        = 3.6;
        convert_length       = 1.0;
        convert_length_large = 1000;
        unit_speed           = 0x81;
        unit_length          = 0x8D;
        unit_length_large    = 0xFD;
    } else {
        convert_speed        = 2.23;
        convert_length       = 3.28;
        convert_length_large = 5280;
        unit_speed           = 0xfb;
        unit_length          = 0x66;
        unit_length_large    = 0xFA;
    }
}

/******************************************************************/
// Panel  : startPanels
// Output : Logo panel and initialization
/******************************************************************/
void startPanels()
{
    osd->clear();
    panLogo(); // display logo
    set_converts(); // initialize the units
}


/******************************************************************/
// Panel  : panTxInputs
// Needs  : X, Y locations
// Output : Current
/******************************************************************/
void panTxInputs(int first_col, int first_line)
{
    static uint16_t min_channel[9] = {};
    static uint16_t max_channel[9] = {};

    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("  min  cur  max");
    osd->closePanel();

    for (int i = 0; i < 9; i++) {
        if ((osd_channel[i] > 0) && (osd_channel[i] < min_channel[i])) {
            min_channel[i] = osd_channel[i];
        }
        if ((osd_channel[i] > 0) && (osd_channel[i] > max_channel[i])) {
            max_channel[i] = osd_channel[i];
        }
        osd->setPanel(first_col, first_line + i + 1);
        osd->openPanel();
        if (osd_channel[i] > 2048) {
            min_channel[i] = 65535;
            max_channel[i] = 0;
            osd->printf("%i ---- ---- ----", i + 1);
        } else {
            osd->printf("%i %4i %4i %4i", i + 1, min_channel[i], osd_channel[i], max_channel[i]);
        }
        osd->closePanel();
    }
}


/******************************************************************/
// Panel  : writePadText
// Output : Write the given text padded with spaces
/******************************************************************/
#define PAD_COLS 30

void writePadText(int first_col, int first_line, const prog_char_t *str)
{
    char text_line[PAD_COLS + 1];
    int i;

    // Copy the string from program FLASH
    strncpy_PF(text_line, (uint_farptr_t)str, PAD_COLS);
    // Pad to the end of the buffer with spaces
    for (i = strlen(text_line); i < PAD_COLS; i++) {
        text_line[i] = ' ';
    }
    // Add NULL termination
    text_line[PAD_COLS] = '\0';

    osd->setPanel(first_col, first_line);
    osd->openPanel();
    osd->printf("%s", text_line);
    osd->closePanel();
}


/******************************************************************/
// Panel  : writeCalPanels
// Output : Write the calibration display panels
/******************************************************************/
void writeCalPanels(int first_col, int first_line)
{
    const prog_char_t *cal_operation[] = {
        PSTR("idle"),
        PSTR("level calibration"),
        PSTR("set home location"),
        PSTR("gyro bias calibration"),
        PSTR("accel calibration"),
        PSTR("compass calibration"),
        PSTR("compass recalibration"),
        PSTR("tx calibration"),
        PSTR("save"),
        PSTR("accel self test")
    };

    const prog_char_t *cal_state[] = {
        PSTR("idle"),
        PSTR("active"),
        PSTR("completed"),
        PSTR("error")
    };

    const prog_char_t *cal_error[] = {
        PSTR(""),
        PSTR("disarm required"),
        PSTR("gps alarm present"),
        PSTR("home location set"),
        PSTR("gyro bias error"),
        PSTR("gyro self-test error"),
        PSTR("level cal error"),
        PSTR("attitude alarm prevents level cal"),
        PSTR("maintenance prevents tx cal"),
        PSTR("accel self-test fail"),
        PSTR("accel cal unit-test fail"),
        PSTR("accel sensor fail"),
        PSTR("accel cal fail"),
        PSTR("mag sampling timeout"),
        PSTR("mag calc timeout"),
        PSTR("mag calc not ready"),
        PSTR("too close to previous position")
    };

    writePadText(first_col, first_line, cal_operation[osd_cal_Operation]);
    writePadText(first_col, first_line + 1, cal_state[osd_cal_OperationState]);
    writePadText(first_col, first_line + 3, cal_error[osd_cal_Error]);
}


/******************************************************************/
// Panel  : writeLevelCalPanel
// Output : Write the level calibration display panel
/******************************************************************/
void writeLevelCalPanel(int first_col, int first_line)
{
    const prog_char_t *gyro_cal_state[] = {
        PSTR("idle"),
        PSTR("position 1"),
        PSTR("position 1"),
        PSTR("sample"),
        PSTR("save"),
        PSTR("complete"),
        PSTR("error")
    };

    writeCalPanels(first_col, first_line);
    writePadText(first_col, first_line + 2, gyro_cal_state[osd_cal_LevelCalibrationState]);
}


/******************************************************************/
// Panel  : writeGyroCalPanel
// Output : Write the gyro calibration display panel
/******************************************************************/
void writeGyroCalPanel(int first_col, int first_line)
{
    const prog_char_t *gyro_cal_state[] = {
        PSTR("idle"),
        PSTR("pre-selftest"),
        PSTR("selftest"),
        PSTR("selftest completed"),
        PSTR("sample"),
        PSTR("save"),
        PSTR("complete"),
        PSTR("error")
    };

    writeCalPanels(first_col, first_line);
    writePadText(first_col, first_line + 2, gyro_cal_state[osd_cal_GyroBiasCalibrationState]);
}


/******************************************************************/
// Panel  : writeMagCalPanel
// Output : Write the mag calibration display panel
/******************************************************************/
void writeMagCalPanel(int first_col, int first_line)
{
    const prog_char_t *mag_cal_state[] = {
        PSTR("idle"),
        PSTR("sample"),
        PSTR("calculate/4"),
        PSTR("calculate/7"),
        PSTR("calculate/10"),
        PSTR("complete"),
        PSTR("error")
    };

    writeCalPanels(first_col, first_line);
    writePadText(first_col, first_line + 2, mag_cal_state[osd_cal_MagCalibrationState]);
}


/******************************************************************/
// Panel  : writeAccelCalPanel
// Output : Write the accel calibration display panel
/******************************************************************/
void writeAccelCalPanel(int first_col, int first_line)
{
    const prog_char_t *gyro_cal_state[] = {
        PSTR("idle"),
        PSTR("pre selftest"),
        PSTR("selftest"),
        PSTR("post selftest"),
        PSTR("selftest complete"),
        PSTR("place nose down"),
        PSTR("place right side down"),
        PSTR("place nose up"),
        PSTR("place left side down"),
        PSTR("pitch forward 45"),
        PSTR("roll right 45"),
        PSTR("pitch back 45"),
        PSTR("roll left 45"),
        PSTR("inverted pitch forward 45"),
        PSTR("inverted roll right 45"),
        PSTR("inverted pitch back 45"),
        PSTR("inverted roll left 45"),
        PSTR("place upside down"),
        PSTR("place horizontal"),
        PSTR("sample"),
        PSTR("calculate"),
        PSTR("save"),
        PSTR("complete"),
        PSTR("error")
    };

    writeCalPanels(first_col, first_line);
    writePadText(first_col, first_line + 2, gyro_cal_state[osd_cal_AccelCalibrationState]);
}


/******************************************************************/
// Panel  : writeTxCalPanel
// Output : Write the tx calibration display panel
/******************************************************************/
void writeTxCalPanel(int first_col, int first_line)
{
    const prog_char_t *tx_cal_state[] = {
        PSTR("idle"),
        PSTR("transmitter type"),
        PSTR("identify collective"),
        PSTR("identify throttle"),
        PSTR("identify roll"),
        PSTR("identify pitch"),
        PSTR("identify yaw"),
        PSTR("identify flight mode"),
        PSTR("identify accessory 1"),
        PSTR("identify accessory 2"),
        PSTR("identify accessory 3"),
        PSTR("verify"),
        PSTR("save"),
        PSTR("complete"),
        PSTR("error")
    };

    writeCalPanels(first_col, first_line);
    writePadText(first_col, first_line + 2, tx_cal_state[osd_cal_TxCalibrationState]);
    panTxInputs(first_col, first_line + 4);
}


/******************************************************************/
// Panel  : writeNavPanels
// Output : Write the navigation display panels
/******************************************************************/
void writeNavPanels()
{
    if (minimosd_config.warning_enable) {
        panWarn(minimosd_config.warning_col, minimosd_config.warning_row);
    }

    // these GPS related panels are active under all circumstances
    if (minimosd_config.sat_count_enable) {
        panGPSats(minimosd_config.sat_count_col,
                  minimosd_config.sat_count_row); // number of visible sats
    }
    if (minimosd_config.gps_lock_enable) {
        panGPL(minimosd_config.gps_lock_col,
               minimosd_config.gps_lock_row); // sat fix type
    }
    // these GPS related panels are active if GPS was valid before
    if (osd_got_home) {
        if (minimosd_config.gps_coords_enable) {
            panGPS(minimosd_config.gps_coords_col, minimosd_config.gps_coords_row); // lat & lon
        }
        if (minimosd_config.home_dist_enable) {
            panHomeDis(minimosd_config.home_dist_col, minimosd_config.home_dist_row);
        }
        if (minimosd_config.home_dir_enable) {
            panHomeDir(minimosd_config.home_dir_col, minimosd_config.home_dir_row);
        }
    }

    // these GPS related panels are active if GPS was valid before and we have a sat fix
    if (osd_got_home && osd_fix_type > 1) {
        if (minimosd_config.home_altitude_enable) {
            panHomeAlt(minimosd_config.home_altitude_col, minimosd_config.home_altitude_row);
        }
        if (minimosd_config.altitude_enable) {
            panAlt(minimosd_config.altitude_col, minimosd_config.altitude_row);
        }
        if (minimosd_config.velocity_enable) {
            panVel(minimosd_config.velocity_col, minimosd_config.velocity_row);
        }
        if (minimosd_config.climb_enable) {
            panClimb(minimosd_config.climb_col, minimosd_config.climb_row);
        }
        if (minimosd_config.heading_enable) {
            panHeading(minimosd_config.heading_col, minimosd_config.heading_row);
        }
        if (minimosd_config.rose_enable) {
            panRose(minimosd_config.rose_col, minimosd_config.rose_row);
        }
    }

    if (minimosd_config.rssi_enable || minimosd_config.link_quality_enable) {
        panRSSI(minimosd_config.rssi_col,
                minimosd_config.rssi_row); // RSSI/Link Quality
    }
    if (minimosd_config.roll_enable) {
        panRoll(minimosd_config.roll_col, minimosd_config.roll_row);
    }
    if (minimosd_config.pitch_enable) {
        panPitch(minimosd_config.pitch_col, minimosd_config.pitch_row);
    }
    if (minimosd_config.throttle_enable) {
        panThr(minimosd_config.throttle_col, minimosd_config.throttle_row);
    }
    if (minimosd_config.flight_mode_enable) {
        panFlightMode(minimosd_config.flight_mode_col, minimosd_config.flight_mode_row);
    }
    if (minimosd_config.battery_voltage_enable) {
        panBatt_A(minimosd_config.battery_voltage_col, minimosd_config.battery_voltage_row);
    }
    if (minimosd_config.battery_current_enable) {
        panCur_A(minimosd_config.battery_current_col, minimosd_config.battery_current_row);
    }
    if (minimosd_config.battery_mah_enable) {
        panBatteryConsumed(minimosd_config.battery_mah_col, minimosd_config.battery_mah_row);
    }
    if (minimosd_config.txpid_enable) {
        panTxPID(minimosd_config.txpid_col, minimosd_config.txpid_row);
    }
    if (minimosd_config.time_enable) {
        panTime(minimosd_config.time_col, minimosd_config.time_row);
    }

    if (minimosd_config.horizon_enable) {
        panHorizon(minimosd_config.horizon_col, minimosd_config.horizon_row);
    }

    // these GPS related panels are active if GPS was valid before
    if ((osd_got_home) && (minimosd_config.radar_enable)) {
        panUAVPosition(minimosd_config.radar_col, minimosd_config.radar_row);
    }

#ifdef membug
    // OSD debug for development
    osd->setPanel(13, 4);
    osd->openPanel();
    osd->printf("%i", freeMem());
    osd->closePanel();
#endif
}

/******************************************************************/
// Panel  : writePanels
// Output : Write the panels
/******************************************************************/
void writePanels()
{
    static CalibrationStatusOperationOptions cur_cal_operation = CALIBRATIONSTATUS_OPERATION_NONE;

    // Clear the display if switching panels
    if (osd_cal_Operation != cur_cal_operation) {
        osd->clear();
        cur_cal_operation = osd_cal_Operation;
    }

    switch (cur_cal_operation) {
    case CALIBRATIONSTATUS_OPERATION_NONE:
        writeNavPanels();
        break;

    case CALIBRATIONSTATUS_OPERATION_LEVELCALIBRATION:
        writeLevelCalPanel(CALIBRATION_COL, CALIBRATION_ROW);
        break;

    case CALIBRATIONSTATUS_OPERATION_GYROBIASCALIBRATION:
        writeGyroCalPanel(CALIBRATION_COL, CALIBRATION_ROW);
        break;

    case CALIBRATIONSTATUS_OPERATION_COMPASSCALIBRATION:
    case CALIBRATIONSTATUS_OPERATION_COMPASSRECALIBRATION:
        writeMagCalPanel(CALIBRATION_COL, CALIBRATION_ROW);
        break;

    case CALIBRATIONSTATUS_OPERATION_ACCELCALIBRATION:
    case CALIBRATIONSTATUS_OPERATION_ACCELSELFTEST:
        writeAccelCalPanel(CALIBRATION_COL, CALIBRATION_ROW);
        break;

    case CALIBRATIONSTATUS_OPERATION_TXCALIBRATION:
        writeTxCalPanel(CALIBRATION_COL, CALIBRATION_ROW);
        break;

    case CALIBRATIONSTATUS_OPERATION_SETHOMELOCATION:
    case CALIBRATIONSTATUS_OPERATION_SAVECALIBRATIONRESULTS:
        writeCalPanels(CALIBRATION_COL, CALIBRATION_ROW);
        break;

    default:
        break;
    }
}
