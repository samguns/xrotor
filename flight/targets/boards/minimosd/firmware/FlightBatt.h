/**
 ******************************************************************************
 *
 * @file       FlightBatt.h
 * @author     Joerg-D. Rothfuchs
 * @brief      Implements voltage and current measurement of the flight battery
 *             on the Ardupilot Mega MinimOSD using built-in ADC reference.
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/> or write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


// !!! For using this, you have to solder a little bit on the MinimOSD, see the wiki !!!


#ifndef FLIGHT_BATT_H_
#define FLIGHT_BATT_H_
#include "UAVTalk.h"

// Global configuration for minimosd
extern MinimOSDsettingsDataPacked minimosd_config;

#define VOLTAGE_PIN minimosd_config.battery_voltage_pin + 1
#define CURRENT_PIN minimosd_config.battery_current_pin + 1

#define REF_VOLTAGE 1.1 // INTERNAL: a built-in reference, equal to 1.1 volts on the ATmega168 or ATmega328
#define LOW_VOLTAGE 9.6 // filter start value for 3s LiPo


// Vref 1.1V based: This is the start value for calibrating a 16k0/1k1 voltage divider usable up to 4s LiPo.
// VOLT_DIV_RATIO is used to define the voltage divider resistor ratio used for measuring the input voltage.
// Suitable resistor values should be chosen to limit the input voltage to a value below 1.1V (Internal reference of the A2D).
// Good starting points for VOLT_DIV_RATIO (minimosd_config.battery_voltage_cal) on a 4S battery and some commonly used resistor values are:
// 16k0/1k1 = 15.54
// 22k/1k5  = 15.66
// 15k/1k   = 16.00  (micro MinimOSD as a 15k/1k on-board resistor divider).
#define VOLT_DIV_RATIO    minimosd_config.battery_voltage_cal

// !!! for the +-50A Current Sensor(AC/DC) DFRobot SEN0098 we need approx. a 1/4 voltage divider 3k0/1k1 so that we stay below 1.1 V -> 2*50A * 0.04V/A / (4.1/1.1) = 1.073 V !!!
// Vref 1.1V based: This is the start value for calibrating a +-50A Current Sensor(AC/DC) DFRobot SEN0098 Sensitivity: 40 mV/A
#define CURR_AMP_PER_VOLT minimosd_config.battery_current_cal
// Vref 1.1V based: This is the start value for calibrating a +-50A Current Sensor(AC/DC) DFRobot SEN0098 Sensitivity: 40 mV/A
#define CURR_AMPS_OFFSET  minimosd_config.battery_current_offset

#define CURRENT_VOLTAGE(x) (((x) * REF_VOLTAGE / 1024.0) * VOLT_DIV_RATIO)
#define CURRENT_AMPS(x)    (((x) * REF_VOLTAGE / 1024.0) - (CURR_AMPS_OFFSET / 10000.0)) * (CURR_AMP_PER_VOLT / 100.0)


void flight_batt_init(void);
void flight_batt_read(void);


#endif /* FLIGHT_BATT_H_ */
