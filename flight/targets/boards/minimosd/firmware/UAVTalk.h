/**
 ******************************************************************************
 *
 * @file       UAVTalk.h
 * @author     Joerg-D. Rothfuchs
 * @brief      Implements a subset of the telemetry communication between
 *             OpenPilot CC, CC3D, Revolution and Ardupilot Mega MinimOSD
 *             with code from OpenPilot and MinimOSD.
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, see <http://www.gnu.org/licenses/> or write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */


#ifndef UAVTALK_H_
#define UAVTALK_H_

#include <inttypes.h>
#include "OSD_Config.h"

// Slightly messy workaround so that uavo synthetic headers can be included
#define PIOS_STATIC_ASSERT(n)
typedef void *xQueueHandle;

/* Common definition required for all UAVOs */
typedef void *UAVTalkConnection;

#include <uavobjectmanager.h>
#define _offset(type, field) (int)&((type *)NULL)->field

#include <flighttelemetrystats.h>
#include <attitudestate.h>
#include <flightstatus.h>
#include <manualcontrolcommand.h>
#include <gpspositionsensor.h>
#include <gpsvelocitysensor.h>
#include <flightbatterystate.h>
#include <receiverstatus.h>
#include <txpidstatus.h>
#include <barosensor.h>
#include <oplinkstatus.h>
#include <minimosdsettings.h>
#include <minimosdstatus.h>
#include <calibrationstatus.h>

// Low level protocol definitions
#define UAVTALK_MODE_PASSIVE                  0x01            // do not send any UAVTalk packets

#define FLIGHTTELEMETRYSTATS_CONNECT_INTERVAL 1000
#define FLIGHTTELEMETRYSTATS_CONNECT_TIMEOUT  10000
#define GCSTELEMETRYSTATS_SEND_PERIOD         1000

#define HEADER_LEN                            10

#define RESPOND_OBJ_LEN                       HEADER_LEN
#define REQUEST_OBJ_LEN                       HEADER_LEN

#define UAVTALK_SYNC_VAL                      0x3C

#define UAVTALK_TYPE_MASK                     0xF8
#define UAVTALK_TYPE_VER                      0x20

#define UAVTALK_TYPE_OBJ                      (UAVTALK_TYPE_VER | 0x00)
#define UAVTALK_TYPE_OBJ_REQ                  (UAVTALK_TYPE_VER | 0x01)
#define UAVTALK_TYPE_OBJ_ACK                  (UAVTALK_TYPE_VER | 0x02)
#define UAVTALK_TYPE_ACK                      (UAVTALK_TYPE_VER | 0x03)
#define UAVTALK_TYPE_NACK                     (UAVTALK_TYPE_VER | 0x04)


typedef enum {
    UAVTALK_PARSE_STATE_WAIT_SYNC = 0,
    UAVTALK_PARSE_STATE_GOT_SYNC,
    UAVTALK_PARSE_STATE_GOT_MSG_TYPE,
    UAVTALK_PARSE_STATE_GOT_LENGTH,
    UAVTALK_PARSE_STATE_GOT_OBJID,
    UAVTALK_PARSE_STATE_GOT_INSTID,
    UAVTALK_PARSE_STATE_GOT_DATA,
    UAVTALK_PARSE_STATE_GOT_CRC
} uavtalk_parse_state_t;


typedef enum {
    TELEMETRYSTATS_STATE_DISCONNECTED = 0,
    TELEMETRYSTATS_STATE_HANDSHAKEREQ,
    TELEMETRYSTATS_STATE_HANDSHAKEACK,
    TELEMETRYSTATS_STATE_CONNECTED
} telemetrystats_state_t;


typedef struct __uavtalk_message {
    uint8_t  Sync;
    uint8_t  MsgType;
    uint16_t Length;
    uint32_t ObjID;
    uint16_t InstID;
    uint8_t  Data[255];
    uint8_t  Crc;
} uavtalk_message_t;


int uavtalk_read(void);
int uavtalk_state(void);


#endif /* UAVTALK_H_ */
