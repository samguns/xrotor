ARCH = avr

BOARD_TYPE          := 0xff
BOARD_REVISION      := 0x00

# Output format (can be ihex or binary or both).
#  binary to create a load-image in raw-binary format i.e. for SAM-BA,
#  ihex to create a load-image in Intel hex format
LOADFORMAT = ihex
#LOADFORMAT = binary
#LOADFORMAT = both
