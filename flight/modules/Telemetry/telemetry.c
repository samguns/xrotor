/**
 ******************************************************************************
 * @addtogroup OpenPilotModules OpenPilot Modules
 * @{
 * @addtogroup TelemetryModule Telemetry Module
 * @brief Main telemetry module
 * Starts three tasks (RX, TX, and priority TX) that watch event queues
 * and handle all the telemetry of the UAVobjects
 * @{
 *
 * @file       telemetry.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @brief      Telemetry module, handles telemetry and UAVObject updates
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <project_xflight.h>
#include <uavtalk.h>
#include <alarms.h>

#include "telemetry.h"

#include "flighttelemetrystats.h"
#include "gcstelemetrystats.h"
#include "hwsettings.h"
#include "taskinfo.h"

// Private constants
#define MAX_QUEUE_SIZE         TELEM_QUEUE_SIZE
#define TASK_PRIORITY          (tskIDLE_PRIORITY + 2)
#define REQ_TIMEOUT_MS         250
#define MAX_RETRIES            2
#define STATS_UPDATE_PERIOD_MS 4000
#define CONNECTION_TIMEOUT_MS  8000

// Linked list of channels
static channelContext *channels = NULL;

static xTaskHandle telemetryTaskHandle;
static xSemaphoreHandle task_wake_sem;

// Telemetry stats
static uint32_t txErrors;
static uint32_t txRetries;
static uint32_t timeOfLastObjectUpdate;

static void telemetryTask(void *context);
static int32_t transmitData(channelContext *channel, uint8_t *data, int32_t length);
static void registerObject(UAVOBase *base, void *context);
static void updateObject(
    channelContext *channel,
    UAVOBase *base,
    int32_t eventType);
static int32_t processObjEvent(
    channelContext *channel,
    UAVObjEvent *ev);
static int32_t setUpdatePeriod(
    channelContext *channel,
    UAVOBase *base,
    int32_t updatePeriodMs);
static int32_t setLoggingPeriod(
    channelContext *channel,
    UAVOBase *base,
    int32_t updatePeriodMs);
static void updateTelemetryStats(channelContext *channel);
static void gcsTelemetryStatsUpdated(channelContext *channel);
static void updateSettings(uint32_t port);
static uint32_t defGetPort(channelContext *channel);
static uint32_t usbGetPort(channelContext *channel);


/**
 * Initialise the telemetry module
 * \return STATUS_OK on success
 */
int32_t TelemetryStart(void)
{
    // Register each channel
    for (channelContext *channel = channels; channel; channel = channel->next) {
        UAVObjIterate(&registerObject, (void *)channel);

        // Listen to objects of interest
#ifdef PIOS_TELEM_PRIORITY_QUEUE
        GCSTelemetryStatsConnectQueue(channel->priorityQueue);
#else /* PIOS_TELEM_PRIORITY_QUEUE */
        GCSTelemetryStatsConnectQueue(channel->queue);
#endif /* PIOS_TELEM_PRIORITY_QUEUE */
    }

    xTaskCreate(telemetryTask,
                "Telemetry",
                PIOS_TELEM_STACK_SIZE,
                NULL,
                TASK_PRIORITY,
                &telemetryTaskHandle);

    PIOS_TASK_MONITOR_RegisterTask(TASKINFO_RUNNING_TELEMETRY,
                                   telemetryTaskHandle);

    return STATUS_OK;
}

/* Intialise a telemetry channel */
int32_t TelemetryInitializeChannel(uint32_t (*getPort)(struct _channelContext *channel), uint32_t port)
{
    channelContext *channel = (channelContext *)pios_malloc(sizeof(channelContext));

    if (!channel) {
        return STATUS_NOMEM;
    }

    // Add channel to linked list
    channel->next    = channels;
    channels = channel;

    channel->port    = port;
    channel->getPort = getPort;

    // No retry active initially
    channel->retry   = false;

    // Register the semaphore on the input channel
    PIOS_COM_RegisterUserRxSem(port, task_wake_sem);

    // Create object queues
    channel->queue = xQueueCreate(MAX_QUEUE_SIZE,
                                  sizeof(UAVObjEvent));
#if defined(PIOS_TELEM_PRIORITY_QUEUE)
    channel->priorityQueue = xQueueCreate(MAX_QUEUE_SIZE,
                                          sizeof(UAVObjEvent));
#endif /* PIOS_TELEM_PRIORITY_QUEUE */

    // Initialise UAVTalk
    channel->uavTalkCon = UAVTalkInitialize(&transmitData, channel);

    // Create periodic event that will be used to update the telemetry stats
    UAVObjEvent ev;
    memset(&ev, 0, sizeof(UAVObjEvent));

#ifdef PIOS_TELEM_PRIORITY_QUEUE
    EventPeriodicQueueCreate(&ev,
                             channel->priorityQueue,
                             channel->uavTalkCon,
                             task_wake_sem,
                             STATS_UPDATE_PERIOD_MS);
#else /* PIOS_TELEM_PRIORITY_QUEUE */
    EventPeriodicQueueCreate(&ev,
                             channel->queue,
                             channel->uavTalkCon,
                             task_wake_sem,
                             STATS_UPDATE_PERIOD_MS);
#endif /* PIOS_TELEM_PRIORITY_QUEUE */

    return STATUS_OK;
}

/**
 * Initialise the telemetry module
 * \return STATUS_OK on success
 */
int32_t TelemetryInitialize(void)
{
    HwSettingsInitialize();

    FlightTelemetryStatsInitialize();
    GCSTelemetryStatsInitialize();

    // Initialize vars
    timeOfLastObjectUpdate = 0;

    // Reset link stats
    txErrors  = 0;
    txRetries = 0;

    // Create a semaphore to wake the telemetry task
    vSemaphoreCreateBinary(task_wake_sem);

    // Only initialise local channel if telemetry port enabled
    if (PIOS_COM_TELEM_RF) {
        // Set the local telemetry baud rate
        updateSettings(PIOS_COM_TELEM_RF);
        // Initialise channel
        TelemetryInitializeChannel(defGetPort, PIOS_COM_TELEM_RF);
    }

#ifdef PIOS_INCLUDE_RFM22B
    OPLinkSettingsInitialize();
    OPLinkSettingsData data;

    OPLinkSettingsGet(&data);
    if (!data.PPMOnly) {
        TelemetryInitializeChannel(defGetPort, PIOS_COM_RF);
    }
#endif /* PIOS_INCLUDE_RFM22B */

    // Initialise channel
    TelemetryInitializeChannel(usbGetPort, 0);

    return STATUS_OK;
}

MODULE_INITCALL(TelemetryInitialize, TelemetryStart);


// Connect both queue and callback to the specified object
static void registerObject(UAVOBase *base, void *context)
{
    channelContext *channel = (channelContext *)context;

    if (UAVObjIsMetaobject(base)) {
        // Only connect change notifications for meta objects.  No periodic updates
#ifdef PIOS_TELEM_PRIORITY_QUEUE
        UAVObjConnectQueueSem(base, channel->priorityQueue, task_wake_sem, EV_MASK_ALL_UPDATES);
#else /* PIOS_TELEM_PRIORITY_QUEUE */
        UAVObjConnectQueueSem(base, channel->queue, task_wake_sem, EV_MASK_ALL_UPDATES);
#endif /* PIOS_TELEM_PRIORITY_QUEUE */
    } else {
        // Setup object for periodic updates
        updateObject(
            channel,
            base,
            EV_NONE);
    }
}


/**
 * Update object's queue connections and timer, depending on object's settings
 * \param[in] telemetry channel context
 * \param[in] base Object to updates
 */
static void updateObject(
    channelContext *channel,
    UAVOBase *base,
    int32_t eventType)
{
    UAVObjMetadata metadata;
    UAVObjUpdateMode updateMode, loggingMode;
    int32_t eventMask;

    if (UAVObjIsMetaobject(base)) {
        // This function updates the periodic updates for the object.
        // Meta Objects cannot have periodic updates.
        PIOS_Assert(false);
        return;
    }

    // Get metadata
    UAVObjGetMetadata(base, channel->uavTalkCon, &metadata);
    updateMode  = UAVObjGetTelemetryUpdateMode(&metadata);
    loggingMode = UAVObjGetLoggingUpdateMode(&metadata);

    // Setup object depending on update mode
    eventMask   = 0;
    switch (updateMode) {
    case UPDATEMODE_PERIODIC:
        // Set update period
        setUpdatePeriod(channel,
                        base,
                        metadata.telemetryUpdatePeriod);
        // Connect queue
        eventMask |= EV_UPDATED_PERIODIC | EV_UPDATED_MANUAL | EV_UPDATE_REQ;
        break;
    case UPDATEMODE_ONCHANGE:
        // Set update period
        setUpdatePeriod(channel, base, 0);
        // Connect queue
        eventMask |= EV_UPDATED | EV_UPDATED_MANUAL | EV_UPDATE_REQ;
        break;
    case UPDATEMODE_THROTTLED:
        if ((eventType == EV_UPDATED_PERIODIC) || (eventType == EV_NONE)) {
            // If we received a periodic update, we can change back to update on change
            eventMask |= EV_UPDATED | EV_UPDATED_MANUAL | EV_UPDATE_REQ;
            // Set update period on initialization and metadata change
            if (eventType == EV_NONE) {
                setUpdatePeriod(channel,
                                base,
                                metadata.telemetryUpdatePeriod);
            }
        } else {
            // Otherwise, we just received an object update, so switch to periodic for the timeout period to prevent more updates
            eventMask |= EV_UPDATED_PERIODIC | EV_UPDATED_MANUAL | EV_UPDATE_REQ;
        }
        break;
    case UPDATEMODE_MANUAL:
        // Set update period
        setUpdatePeriod(channel, base, 0);
        // Connect queue
        eventMask |= EV_UPDATED_MANUAL | EV_UPDATE_REQ;
        break;
    }
    switch (loggingMode) {
    case UPDATEMODE_PERIODIC:
        // Set update period
        setLoggingPeriod(channel, base, metadata.loggingUpdatePeriod);
        // Connect queue
        eventMask |= EV_LOGGING_PERIODIC | EV_LOGGING_MANUAL;
        break;
    case UPDATEMODE_ONCHANGE:
        // Set update period
        setLoggingPeriod(channel, base, 0);
        // Connect queue
        eventMask |= EV_UPDATED | EV_LOGGING_MANUAL;
        break;
    case UPDATEMODE_THROTTLED:
        if ((eventType == EV_LOGGING_PERIODIC) || (eventType == EV_NONE)) {
            // If we received a periodic update, we can change back to update on change
            eventMask |= EV_UPDATED | EV_LOGGING_MANUAL;
            // Set update period on initialization and metadata change
            if (eventType == EV_NONE) {
                setLoggingPeriod(channel,
                                 base,
                                 metadata.loggingUpdatePeriod);
            }
        } else {
            // Otherwise, we just received an object update, so switch to periodic for the timeout period to prevent more updates
            eventMask |= EV_LOGGING_PERIODIC | EV_LOGGING_MANUAL;
        }
        break;
    case UPDATEMODE_MANUAL:
        // Set update period
        setLoggingPeriod(channel, base, 0);
        // Connect queue
        eventMask |= EV_LOGGING_MANUAL;
        break;
    }

    // note that all setting objects have implicitly IsPriority=true
#ifdef PIOS_TELEM_PRIORITY_QUEUE
    if (UAVObjIsPriority(base)) {
        UAVObjConnectQueueSem(base, channel->priorityQueue, task_wake_sem, eventMask);
    } else
#endif /* PIOS_TELEM_PRIORITY_QUEUE */
    UAVObjConnectQueueSem(base, channel->queue, task_wake_sem, eventMask);
}


/**
 * Processes queue events
 */
static int32_t processObjEvent(
    channelContext *channel,
    UAVObjEvent *ev)
{
    UAVObjMetadata metadata;
    UAVObjUpdateMode updateMode;
    int32_t retries;
    int32_t status = STATUS_OK;

    if (ev->base == 0) {
        updateTelemetryStats(channel);
    } else if (ev->base == GCSTelemetryStatsBase()) {
        gcsTelemetryStatsUpdated(channel);
    } else {
        // Get object metadata
        UAVObjGetMetadata(ev->base, channel->uavTalkCon, &metadata);
        updateMode = UAVObjGetTelemetryUpdateMode(&metadata);

        // Act on event
        retries    = 0;
        status     = STATUS_ERROR;
        if ((ev->event == EV_UPDATED && (updateMode == UPDATEMODE_ONCHANGE || updateMode == UPDATEMODE_THROTTLED))
            || ev->event == EV_UPDATED_MANUAL
            || (ev->event == EV_UPDATED_PERIODIC && updateMode != UPDATEMODE_THROTTLED)) {
            // Send update to GCS (with retries)
            status = UAVTalkSendObject(channel->uavTalkCon,
                                       ev->base,
                                       ev->instId,
                                       UAVObjGetTelemetryAcked(&metadata), REQ_TIMEOUT_MS);

            if (status == STATUS_TIMEOUT) {
                txRetries++;
                if (++channel->retries < MAX_RETRIES) {
                    status = STATUS_RETRY;
                }
            }

            channel->retry = (status == STATUS_RETRY);

            if (status == STATUS_ERROR) {
                ++txErrors;
            }
        } else if (ev->event == EV_UPDATE_REQ) {
            // Request object update from GCS (with retries)
            status = UAVTalkSendObjectRequest(channel->uavTalkCon,
                                              ev->base,
                                              ev->instId,
                                              REQ_TIMEOUT_MS);
            if (status == STATUS_TIMEOUT) {
                txRetries++;
                if (++channel->retries < MAX_RETRIES) {
                    status = STATUS_RETRY;
                }
            }

            channel->retry = (status == STATUS_RETRY);

            if (status == STATUS_ERROR) {
                ++txErrors;
            }
        }
        // If this is a metaobject then make necessary telemetry updates
        if (UAVObjIsMetaobject(ev->base)) {
            // linked object will be the actual object the metadata are for
            updateObject(
                channel,
                UAVObjGetLinkedObj(ev->base),
                EV_NONE);
        } else {
            if (updateMode == UPDATEMODE_THROTTLED) {
                // If this is UPDATEMODE_THROTTLED, the event mask changes on every event.
                updateObject(
                    channel,
                    ev->base,
                    ev->event);
            }
        }
    }
    // Log UAVObject if necessary
    if (ev->base) {
        updateMode = UAVObjGetLoggingUpdateMode(&metadata);
        if ((ev->event == EV_UPDATED && (updateMode == UPDATEMODE_ONCHANGE || updateMode == UPDATEMODE_THROTTLED))
            || ev->event == EV_LOGGING_MANUAL
            || (ev->event == EV_LOGGING_PERIODIC && updateMode != UPDATEMODE_THROTTLED)) {
            if (ev->instId == UAVOBJ_ALL_INSTANCES) {
                status = UAVObjGetNumInstances(ev->base);
                for (retries = 0; retries < status; retries++) {
                    UAVObjInstanceWriteToLog(channel->uavTalkCon, ev->base, retries);
                }
            } else {
                UAVObjInstanceWriteToLog(channel->uavTalkCon, ev->base, ev->instId);
            }
        }
        if (updateMode == UPDATEMODE_THROTTLED) {
            // If this is UPDATEMODE_THROTTLED, the event mask changes on every event.
            updateObject(
                channel,
                ev->base,
                ev->event);
        }
    }

    return status;
}

/**
 * Telemetry transmit, regular priority
 */
static bool telemetryTx(channelContext *channel)
{
    bool busy = false;

    // Handle any retry before fresh data
    if (channel->retry) {
        if (processObjEvent(channel, &channel->ev) == STATUS_RETRY) {
            // Can't proceed until this concludes
            return false;
        }
    }

    /**
     * Tries to empty the high priority queue before handling any standard priority item
     */
#ifdef PIOS_TELEM_PRIORITY_QUEUE
    // empty priority queue, non-blocking
    while (xQueueReceive(channel->priorityQueue, &channel->ev, 0) == pdTRUE) {
        // Process event
        if (processObjEvent(channel, &channel->ev) == STATUS_RETRY) {
            // Can't proceed until this concludes
            return false;
        }
        busy = true;
    }
    // check regular queue and process update - non-blocking
    if (xQueueReceive(channel->queue, &channel->ev, 0) == pdTRUE) {
        // Process event
        if (processObjEvent(channel, &channel->ev) == STATUS_RETRY) {
            // Can't proceed until this concludes
            return false;
        }
        busy = true;
    }
#else
    if (xQueueReceive(channel->queue, &channel->ev, 0) == pdTRUE) {
        // Process event
        if (processObjEvent(channel, &channel->ev) == STATUS_RETRY) {
            // Can't proceed until this concludes
            return false;
        }
        busy = true;
    }
#endif /* PIOS_TELEM_PRIORITY_QUEUE */

    return busy;
}


/**
 * Telemetry receive. Processes queue events and periodic updates.
 */
static bool telemetryRx(channelContext *channel)
{
    uint8_t serial_data[16];
    uint16_t bytes_to_process;
    uint32_t port = channel->getPort(channel);

    if (port) {
        bytes_to_process = PIOS_COM_ReceiveBuffer(port, serial_data, sizeof(serial_data), 0);
        if (bytes_to_process > 0) {
            UAVTalkProcessInputStream(channel->uavTalkCon, serial_data, bytes_to_process);
            return true;
        }
    }

    return false;
}


/**
 * Telemetry task. Processes queue events and periodic updates.
 */
static void telemetryTask(__attribute__((unused)) void *context)
{
    bool busy;

    // Task loop
    while (true) {
        busy = false;
        // Process each channel
        for (channelContext *channel = channels; channel; channel = channel->next) {
            busy |= telemetryRx(channel);
            busy |= telemetryTx(channel);
        }

        if (!busy) {
            // Block until there is received data or something put in the queue
            xSemaphoreTake(task_wake_sem, portMAX_DELAY);
        }
    }
}


/**
 * Transmit data buffer to the modem or USB port.
 * \param[in] channel Channel on which to send
 * \param[in] data Data buffer to send
 * \param[in] length Length of buffer
 * \return number of bytes transmitted on success
 */
static int32_t transmitData(channelContext *channel, uint8_t *data, int32_t length)
{
    uint32_t port = channel->getPort(channel);

    if (port) {
        return PIOS_COM_SendBuffer(port, data, length);
    }

    return STATUS_ERROR;
}


/**
 * Set update period of object (it must be already setup for periodic updates)
 * \param[in] telemetry channel context
 * \param[in] base The object to update
 * \param[in] updatePeriodMs The update period in ms, if zero then periodic updates are disabled
 * \return STATUS_OK on success
 */
static int32_t setUpdatePeriod(
    channelContext *channel,
    UAVOBase *base,
    int32_t updatePeriodMs)
{
    UAVObjEvent ev;
    int32_t ret;

    // Add or update object for periodic updates
    ev.base   = base;
    ev.instId = UAVOBJ_ALL_INSTANCES;
    ev.event  = EV_UPDATED_PERIODIC;
    ev.lowPriority = true;

#ifdef PIOS_TELEM_PRIORITY_QUEUE
    xQueueHandle targetQueue = UAVObjIsPriority(base) ? channel->priorityQueue :
                               channel->queue;
#else /* PIOS_TELEM_PRIORITY_QUEUE */
    xQueueHandle targetQueue = channel->queue;
#endif /* PIOS_TELEM_PRIORITY_QUEUE */

    ret = EventPeriodicQueueUpdate(&ev, targetQueue, channel->uavTalkCon, updatePeriodMs);
    if (ret == STATUS_ERROR) {
        ret = EventPeriodicQueueCreate(&ev, targetQueue, channel->uavTalkCon, task_wake_sem, updatePeriodMs);
    }
    return ret;
}

/**
 * Set logging update period of object (it must be already setup for periodic updates)
 * \param[in] telemetry channel context
 * \param[in] base The object to update
 * \param[in] updatePeriodMs The update period in ms, if zero then periodic updates are disabled
 * \return STATUS_OK on success
 */
static int32_t setLoggingPeriod(
    channelContext *channel,
    UAVOBase *base,
    int32_t updatePeriodMs)
{
    UAVObjEvent ev;
    int32_t ret;

    // Add or update object for periodic updates
    ev.base   = base;
    ev.instId = UAVOBJ_ALL_INSTANCES;
    ev.event  = EV_LOGGING_PERIODIC;
    ev.lowPriority = true;

#ifdef PIOS_TELEM_PRIORITY_QUEUE
    xQueueHandle targetQueue = UAVObjIsPriority(base) ? channel->priorityQueue :
                               channel->queue;
#else /* PIOS_TELEM_PRIORITY_QUEUE */
    xQueueHandle targetQueue = channel->queue;
#endif /* PIOS_TELEM_PRIORITY_QUEUE */

    ret = EventPeriodicQueueUpdate(&ev, targetQueue, channel->uavTalkCon, updatePeriodMs);
    if (ret == STATUS_ERROR) {
        ret = EventPeriodicQueueCreate(&ev, targetQueue, channel->uavTalkCon, task_wake_sem, updatePeriodMs);
    }
    return ret;
}

/**
 * Called each time the GCS telemetry stats object is updated.
 * Trigger a flight telemetry stats update if a connection is not
 * yet established.
 */
static void gcsTelemetryStatsUpdated(channelContext *channel)
{
    FlightTelemetryStatsStatusOptions flightStatus = UAVTalkGetFlightStatus(channel->uavTalkCon);
    GCSTelemetryStatsStatusOptions gcsStatus = UAVTalkGetGCSStatus(channel->uavTalkCon);

    if (flightStatus != FLIGHTTELEMETRYSTATS_STATUS_CONNECTED || gcsStatus != GCSTELEMETRYSTATS_STATUS_CONNECTED) {
        updateTelemetryStats(channel);
    }
}

/**
 * Update telemetry statistics and handle connection handshake
 */
static void updateTelemetryStats(channelContext *channel)
{
    UAVTalkStats utalkStats;
    FlightTelemetryStatsData flightStats;
    GCSTelemetryStatsData gcsStats;
    uint8_t forceUpdate;
    uint8_t connectionTimeout;
    uint32_t timeNow;
    GCSTelemetryStatsStatusOptions gcsStatus;
    FlightTelemetryStatsStatusOptions flightStatus;

    // Get stats
    UAVTalkGetStats(channels->uavTalkCon, &utalkStats, true);
    // Process each subsequent channel
    for (channelContext *channel_it = channels->next; channel_it; channel_it = channel_it->next) {
        UAVTalkAddStats(channel_it->uavTalkCon, &utalkStats, true);
    }

    // Get object data
    FlightTelemetryStatsGet(&flightStats);
    GCSTelemetryStatsGet(&gcsStats);

    // Get the connection status
    flightStatus = UAVTalkGetFlightStatus(channel->uavTalkCon);
    gcsStatus    = UAVTalkGetGCSStatus(channel->uavTalkCon);


    // Update stats object
    if (flightStatus == FLIGHTTELEMETRYSTATS_STATUS_CONNECTED) {
        flightStats.TxDataRate    = (float)utalkStats.txBytes / ((float)STATS_UPDATE_PERIOD_MS / 1000.0f);
        flightStats.TxBytes      += utalkStats.txBytes;
        flightStats.TxFailures   += txErrors;
        flightStats.TxRetries    += txRetries;

        flightStats.RxDataRate    = (float)utalkStats.rxBytes / ((float)STATS_UPDATE_PERIOD_MS / 1000.0f);
        flightStats.RxBytes      += utalkStats.rxBytes;
        flightStats.RxFailures   += utalkStats.rxErrors;
        flightStats.RxSyncErrors += utalkStats.rxSyncErrors;
        flightStats.RxCrcErrors  += utalkStats.rxCrcErrors;
    } else {
        flightStats.TxDataRate   = 0;
        flightStats.TxBytes      = 0;
        flightStats.TxFailures   = 0;
        flightStats.TxRetries    = 0;

        flightStats.RxDataRate   = 0;
        flightStats.RxBytes      = 0;
        flightStats.RxFailures   = 0;
        flightStats.RxSyncErrors = 0;
        flightStats.RxCrcErrors  = 0;
    }
    txErrors  = 0;
    txRetries = 0;

    // Check for connection timeout
    timeNow   = xTaskGetTickCount() * portTICK_RATE_MS;
    if (utalkStats.rxObjects > 0) {
        timeOfLastObjectUpdate = timeNow;
    }
    if ((timeNow - timeOfLastObjectUpdate) > CONNECTION_TIMEOUT_MS) {
        connectionTimeout = 1;
    } else {
        connectionTimeout = 0;
    }

    // Update connection state
    forceUpdate = 1;

    if (flightStatus == FLIGHTTELEMETRYSTATS_STATUS_DISCONNECTED) {
        // Wait for connection request
        if (gcsStatus == GCSTELEMETRYSTATS_STATUS_HANDSHAKEREQ) {
            flightStatus = FLIGHTTELEMETRYSTATS_STATUS_HANDSHAKEACK;
        }
    } else if (flightStatus == FLIGHTTELEMETRYSTATS_STATUS_HANDSHAKEACK) {
        // Wait for connection
        if (gcsStatus == GCSTELEMETRYSTATS_STATUS_CONNECTED) {
            flightStatus = FLIGHTTELEMETRYSTATS_STATUS_CONNECTED;
        } else if (gcsStatus == GCSTELEMETRYSTATS_STATUS_DISCONNECTED) {
            flightStatus = FLIGHTTELEMETRYSTATS_STATUS_DISCONNECTED;
        }
    } else if (flightStatus == FLIGHTTELEMETRYSTATS_STATUS_CONNECTED) {
        if (gcsStatus != GCSTELEMETRYSTATS_STATUS_CONNECTED || connectionTimeout) {
            flightStatus = FLIGHTTELEMETRYSTATS_STATUS_DISCONNECTED;
        } else {
            forceUpdate = 0;
        }
    } else {
        flightStatus = FLIGHTTELEMETRYSTATS_STATUS_DISCONNECTED;
    }

    // TODO: check whether is there any error condition worth raising an alarm
    // Disconnection is actually a normal (non)working status so it is not raising alarms anymore.
    if (flightStatus == FLIGHTTELEMETRYSTATS_STATUS_CONNECTED) {
        AlarmsClear(SYSTEMALARMS_ALARM_TELEMETRY);
    }

    // Get the connection status
    UAVTalkSetFlightStatus(channel->uavTalkCon, flightStatus);
    UAVTalkSetGCSStatus(channel->uavTalkCon, gcsStatus);

    // Update object
    FlightTelemetryStatsSet(&flightStats);

    // Force telemetry update if not connected
    if (forceUpdate) {
        FlightTelemetryStatsUpdated();
    }
}


/**
 * Return the port to use on this channel
 */
static uint32_t defGetPort(channelContext *channel)
{
    return channel->port;
}


/**
 * Return the port to use on the USB channel. This uses a global variable to
 * support CDC port toggline behaviour. See usb2ComBridgeSetCtrlLine().
 */
static uint32_t usbGetPort(__attribute__((unused)) channelContext *channel)
{
    return PIOS_COM_TELEM_USB;
}


/**
 * Update the telemetry settings, called on startup.
 * FIXME: This should be in the TelemetrySettings object. But objects
 * have too much overhead yet. Also the telemetry has no any specific
 * settings, etc. Thus the HwSettings object which contains the
 * telemetry port speed is used for now.
 */
static void updateSettings(uint32_t port)
{
    // Retrieve settings
    HwSettingsTelemetrySpeedOptions speed;

    HwSettingsTelemetrySpeedGet(&speed);

    // Set port speed
    switch (speed) {
    case HWSETTINGS_TELEMETRYSPEED_2400:
        PIOS_COM_ChangeBaud(port, 2400);
        break;
    case HWSETTINGS_TELEMETRYSPEED_4800:
        PIOS_COM_ChangeBaud(port, 4800);
        break;
    case HWSETTINGS_TELEMETRYSPEED_9600:
        PIOS_COM_ChangeBaud(port, 9600);
        break;
    case HWSETTINGS_TELEMETRYSPEED_19200:
        PIOS_COM_ChangeBaud(port, 19200);
        break;
    case HWSETTINGS_TELEMETRYSPEED_38400:
        PIOS_COM_ChangeBaud(port, 38400);
        break;
    case HWSETTINGS_TELEMETRYSPEED_57600:
        PIOS_COM_ChangeBaud(port, 57600);
        break;
    case HWSETTINGS_TELEMETRYSPEED_115200:
        PIOS_COM_ChangeBaud(port, 115200);
        break;
    }
}


/**
 * @}
 * @}
 */
