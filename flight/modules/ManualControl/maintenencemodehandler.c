/**
 ******************************************************************************
 * @file       maintenancemodehandler.c
 * @author     Rodney Grainger Copyright (C) 2015.
 * @brief      Interprets the control input in ManualControlCommand to produce
 *             stick commands
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *
 *****************************************************************************/


#include <openpilot.h>
#include <manualcontrolcommand.h>
#include <flightstatus.h>
#include <manualcontrolsettings.h>
#include <maintenancemodesettings.h>
#include <maintenancemodeinfo.h>
#include <systemsettings.h>
#include <calibrationcontrol.h>
#include <calibrationtrigger.h>


/******************************************************************************
 *  Stick Zones
 *
 * Each stick gimbal has two axes, Horizontal and Vertical yielding four axes in
 * total that need to be tracked.
 * Each axis is divided into three zones: MIN, CENTRE, & MAX.
 * The channel data received from from ManualControllCommand are scaled to a
 * a range of -1 to +1, so:
 *      MIN < -0.9
 *      MAX > +0.9.
 *      CENTRE is anywhere between those two values.
 * Since each stick has two axes, it is always in two zones at the same time.
 *
 *  +-------+-----------+-------+
 *  | UP    |    UP     |    UP |
 *  | LEFT  |           | RIGHT |
 *  +-------+-----------+-------+---(+0.9)
 *  |       |           |       |
 *  | LEFT  |  CENTRE   | RIGHT |
 *  |       |           |       |
 *  +-------+-----------+-------+---(-0.9)
 *  | DOWN  |           | DOWN  |
 *  | LEFT  |   DOWN    | RIGHT |
 *  +-------+-----------+-------+
 *          |           |
 *          |           +-----------(+0.9)
 *          |
 *          +-----------------------(-0.9)
 */

// Private constants
#define MAINT_MODE_WIGGLE_COUNT            20
#define MAINT_MODE_WIGGLE_INTERVAL         250
#define MAINT_MODE_TOGGLE_LOCKOUT_DURATION 10000

// Axes of interest
#define THRUST                             0
#define ROLL                               1
#define PITCH                              2
#define YAW                                3

#define NUMBER_OF_AXES                     4

#define NUMBER_OF_STICK_POSITIONS          9

#define ZONE_THRESHOLD                     0.90f
#define THROTTLE_LOW_ZONE_THRESHOLH        0.1f

// Private types
typedef enum {
    AXIS_UNKNOWN,
    AXIS_MIN,
    AXIS_MAX,
    AXIS_CENTRE
} AxisZone;

typedef struct {
    portTickType timer;
    AxisZone     zone;
    AxisZone     lastZone;
    bool firstPass;
} AxisData;

// Private variables
static AxisData axis[NUMBER_OF_AXES];

// Private functions
static void detectMaintenanceModeWiggle(ManualControlCommandData *cmd, ManualControlSettingsData *settings);
static void detectStickCommands(ManualControlCommandData *cmd, ManualControlSettingsData *settings);

// Calibration Commands
static void scNop(void);
static void scCalNone(void);
static void scCalLevel(void);
static void scCalSetHome(void);
static void scCalCompass(void);
static void scCalAccel(void);
static void scCalGyroBias(void);
static void scCalGyroSelfTest(void);
static void scCalAccelSelfTest(void);
static void scCalTemp(void);
static void scCalSave(void);
static void scCalTrgNewPos(void);
static void scCalTrgBack(void);

static void calCommand(MaintenanceModeInfoPositionOptions cmd);

// Calibration Command Map Table [LEFT][RIGHT]
static void(*const calCommandTable[NUMBER_OF_STICK_POSITIONS][NUMBER_OF_STICK_POSITIONS]) (void) = {
    [MAINTENANCEMODEINFO_POSITION_CENTRE] =    { scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop },
    [MAINTENANCEMODEINFO_POSITION_UPLEFT] =    { scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scCalSave, // Dn
                                                 scNop,
                                                 scNop },
    [MAINTENANCEMODEINFO_POSITION_UP] =        { scNop,
                                                 scNop,
                                                 scCalNone,   // Up
                                                 scNop,
                                                 scCalTrgBack, // Rt
                                                 scNop,
                                                 scCalTrgNewPos, // Dn
                                                 scNop,
                                                 scNop },
    [MAINTENANCEMODEINFO_POSITION_UPRIGHT] =   { scNop,
                                                 scCalCompass, // UpL
                                                 scCalAccel, // Up
                                                 scCalGyroBias, // UpR
                                                 scCalGyroSelfTest, // Rt
                                                 scCalAccelSelfTest, // DnR
                                                 scCalTemp, // Dn
                                                 scCalLevel, // DnL
                                                 scCalSetHome }, // Lt
    [MAINTENANCEMODEINFO_POSITION_RIGHT] =     { scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop },
    [MAINTENANCEMODEINFO_POSITION_DOWNRIGHT] = { scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop },
    [MAINTENANCEMODEINFO_POSITION_DOWN] =      { scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop },
    [MAINTENANCEMODEINFO_POSITION_DOWNLEFT] =  { scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop },
    [MAINTENANCEMODEINFO_POSITION_LEFT] =      { scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop,
                                                 scNop },
};

// Helper functions
static void scNop(void)
{
    // NOP
}


static void scCalNone(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_NONE);
}


static void scCalLevel(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_LEVELCALIBRATION);
}


static void scCalSetHome(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_SETHOMELOCATION);
}


static void scCalCompass(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_COMPASSCALIBRATION);
}


static void scCalAccel(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_ACCELCALIBRATION);
}


static void scCalGyroBias(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_GYROBIASCALIBRATION);
}


static void scCalGyroSelfTest(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_GYROSELFTEST);
}


static void scCalAccelSelfTest(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_ACCELSELFTEST);
}


static void scCalTemp(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_TEMPERATURECALIBRATION);
}


static void scCalSave(void)
{
    calCommand(CALIBRATIONCONTROL_OPERATION_SAVECALIBRATIONRESULTS);
}


static void calCommand(MaintenanceModeInfoPositionOptions cmd)
{
    CalibrationControlData calCtrl;

    CalibrationControlGet(&calCtrl);

    calCtrl.Operation = cmd;
    CalibrationControlSet(&calCtrl);
}


static void scCalTrgNewPos(void)
{
    CalibrationTriggerData calTrigger;

    CalibrationTriggerGet(&calTrigger);

    calTrigger.AccelCal = CALIBRATIONTRIGGER_ACCELCAL_NEWORIENTATIONREADY;
    CalibrationTriggerSet(&calTrigger);
}


static void scCalTrgBack(void)
{
    CalibrationTriggerData calTrigger;

    CalibrationTriggerGet(&calTrigger);

    calTrigger.AccelCal = CALIBRATIONTRIGGER_ACCELCAL_BACK;
    CalibrationTriggerSet(&calTrigger);
}


/**
 * @brief Initialise the maintenancemode handler
 * @input: None
 * @output: None
 */
void maintenancemodeHandlerInit(void)
{
    // It sets an alarm if the maintenance mode is active on system start
    MaintenanceModeSettingsData maintenanceMode;

    MaintenanceModeSettingsGet(&maintenanceMode);

    if (maintenanceMode.ActiveOnReset == MAINTENANCEMODESETTINGS_ACTIVEONRESET_TRUE) {
        MaintenanceModeInfoData info;
        MaintenanceModeInfoGet(&info);

        info.MaintenanceModeActive = MAINTENANCEMODEINFO_MAINTENANCEMODEACTIVE_TRUE;
        MaintenanceModeInfoSet(&info);
    }
}

/**
 * @brief Handler to derive system commands based on transmitter stick positions
 * @input: ManualControlCommand,
 * @output: MaintenanceMode.Commands
 */
void maintenancemodeHandler(FlightStatusData *flightStatus, ManualControlCommandData *manual_control_data)
{
    ManualControlSettingsData manual_control_settings;

    ManualControlSettingsGet(&manual_control_settings);

    if (flightStatus->Armed == FLIGHTSTATUS_ARMED_DISARMED) {
        detectMaintenanceModeWiggle(manual_control_data, &manual_control_settings);
        detectStickCommands(manual_control_data, &manual_control_settings);
    }
}


/**
 * @brief Toggle maintenance mode when Yaw stick is quickly wiggled from side to side more than 20 times
 *        NOTE: ArmedState should be policed in calling function, as we don't want to enter maintenance
 *        when the vehicle is armed.
 * @param[in] armState The current state of the arming state
 * @param[in] cmd Current state of the manual control data
 */
static void detectMaintenanceModeWiggle(ManualControlCommandData *cmd, ManualControlSettingsData *settings)
{
    static bool firstPass = true;
    static uint32_t wiggleCount;
    static uint8_t lastStickPos;
    static portTickType toggleTimeout;
    static portTickType lockoutDuration;

    uint8_t stickPos;

    MaintenanceModeSettingsData maintenanceMode;

    MaintenanceModeSettingsGet(&maintenanceMode);

    if ((settings->TxMode == MANUALCONTROLSETTINGS_TXMODE_UNKNOWN) ||
        (maintenanceMode.StickHoldTime == 0)) {
        return;
    }

    stickPos = (cmd->Yaw <= -ZONE_THRESHOLD) ? AXIS_MIN : (cmd->Yaw >= ZONE_THRESHOLD) ? AXIS_MAX : AXIS_CENTRE;
    portTickType sysTime = xTaskGetTickCount();

    if (firstPass) {
        firstPass       = false;
        lastStickPos    = stickPos;
        wiggleCount     = 0;
        toggleTimeout   = 0;
        lockoutDuration = 0;
    }

    if ((stickPos == lastStickPos) || (sysTime < lockoutDuration)) {
        return;
    }

    lastStickPos = stickPos;

    // Was the stick toggle quick enough?
    if ((wiggleCount != 0) && (sysTime >= toggleTimeout)) {
        wiggleCount = 0;
    }

    // Next stick toggle must occur before the next MODE_TOGGLE_INTERVAL
    toggleTimeout = sysTime + MAINT_MODE_WIGGLE_INTERVAL;
    if (stickPos != AXIS_CENTRE) {
        wiggleCount++;
    }

    if (wiggleCount >= MAINT_MODE_WIGGLE_COUNT) {
        // Ensure that we stay in this state for a while
        lockoutDuration = sysTime + MAINT_MODE_TOGGLE_LOCKOUT_DURATION;
        wiggleCount     = 0;

        MaintenanceModeInfoData maintenanceInfo;
        MaintenanceModeInfoGet(&maintenanceInfo);

        if (maintenanceInfo.MaintenanceModeActive != MAINTENANCEMODEINFO_MAINTENANCEMODEACTIVE_FALSE) {
            maintenanceInfo.MaintenanceModeActive = MAINTENANCEMODEINFO_MAINTENANCEMODEACTIVE_FALSE;
        } else {
            maintenanceInfo.MaintenanceModeActive = MAINTENANCEMODEINFO_MAINTENANCEMODEACTIVE_TRUE;
        }
        MaintenanceModeInfoSet(&maintenanceInfo);

        // Cancel any system Commands in progress if user exits out of Maintenance Mode
        if (maintenanceInfo.MaintenanceModeActive == MAINTENANCEMODEINFO_MAINTENANCEMODEACTIVE_FALSE) {
            CalibrationControlData calCtrl;
            CalibrationControlGet(&calCtrl);

            calCtrl.Operation = CALIBRATIONCONTROL_OPERATION_NONE;
            CalibrationControlSet(&calCtrl);
        }
    }
}


/**
 * @brief Attempt to recognise a stick command on each axis of Throttle, Roll, Pitch, & Yaw.
 * #param[in] cmd Current state of the manual control data
 */
static void detectStickCommands(ManualControlCommandData *cmd, ManualControlSettingsData *settings)
{
    static bool change = false;
    float stickPosition;
    AxisData *pAxis    = NULL;

    MaintenanceModeSettingsData maintenanceMode;

    MaintenanceModeSettingsGet(&maintenanceMode);

    SystemSettingsData system;
    SystemSettingsGet(&system);

    if ((settings->TxMode == MANUALCONTROLSETTINGS_TXMODE_UNKNOWN) ||
        (maintenanceMode.StickHoldTime == 0)) {
        return;
    }

    // To reduce CPU overhead, only process one axis on each pass through this function.
    // Find next active axis to process
    static uint8_t channel = NELEMENTS(axis);
    do {
        if (++channel >= NELEMENTS(axis)) {
            channel = 0;
        }
        if (ManualControlSettingsChannelGroupsToArray(settings->ChannelGroups)[channel] < MANUALCONTROLSETTINGS_CHANNELGROUPS_NONE) {
            pAxis = &axis[channel];
        } else {
            axis[channel].lastZone = axis[channel].zone = AXIS_UNKNOWN;
        }
    } while (!pAxis && (channel < NELEMENTS(axis)));

    if (pAxis == NULL) {
        return; // No ChannelGroups set
    }

    switch (channel) {
    case THRUST:
        // On a Heli, the "throttle stick" is Collective so it makes more sense to use it rather than throttle, as it's
        // range is consistent with the -1 to +1 range of the other axes.
        // TODO: Find out more about collective & Throttle.  I don't know helis and I'm guessing here.
        if (system.ThrustControl == SYSTEMSETTINGS_THRUSTCONTROL_THROTTLE) {
            stickPosition = settings->ChannelMax.Throttle > settings->ChannelMin.Throttle ? cmd->Throttle : -cmd->Throttle;
        } else {
            stickPosition = settings->ChannelMax.Collective > settings->ChannelMin.Collective ? cmd->Collective : -cmd->Collective;
        }
        break;
    case ROLL:
        stickPosition = settings->ChannelMax.Roll > settings->ChannelMin.Roll ? cmd->Roll : -cmd->Roll;
        break;
    case PITCH:
        stickPosition = settings->ChannelMax.Pitch > settings->ChannelMin.Pitch ? cmd->Pitch : -cmd->Pitch;
        break;
    case YAW:
        stickPosition = settings->ChannelMax.Yaw > settings->ChannelMin.Yaw ? cmd->Yaw : -cmd->Yaw;
        break;
    default:
        // Should never get here
        channel = 0;
        return;
    }

    portTickType sysTime = xTaskGetTickCount();

    // Throttle is a special case, as zero is at the bottom instead of in the middle
    // TODO: Find out more about collective & Throttle.  I don't know helis so I'm guessing here
    // TODO  Right now it looks like hysteresis is not needed on the input values
    if ((channel == THRUST) && (system.ThrustControl == SYSTEMSETTINGS_THRUSTCONTROL_THROTTLE)) {
        pAxis->zone = (stickPosition < THROTTLE_LOW_ZONE_THRESHOLH) ? AXIS_MIN : (stickPosition > ZONE_THRESHOLD) ? AXIS_MAX : AXIS_CENTRE;
    } else {
        pAxis->zone = (stickPosition < -ZONE_THRESHOLD) ? AXIS_MIN : (stickPosition > ZONE_THRESHOLD) ? AXIS_MAX : AXIS_CENTRE;
    }

    if (pAxis->firstPass) {
        pAxis->lastZone  = pAxis->zone;
        pAxis->firstPass = false;
        return;
    }

    if ((pAxis->zone != pAxis->lastZone)) {
        pAxis->lastZone = pAxis->zone;
        pAxis->timer    = sysTime + (pAxis->zone == AXIS_CENTRE ? maintenanceMode.StickReleaseTime : maintenanceMode.StickHoldTime);
    } else {
        change = (pAxis->timer != 0) && (sysTime > pAxis->timer);
    }

    if (change) {
        uint8_t index;

        // Only continue processing once there are no active timers.  We only want to report once when all sticks are stable.
        for (index = 0; index < NELEMENTS(axis); index++) {
            if (sysTime < axis[index].timer) {
                return;
            }
        }

        // This must only be done after above checks
        for (index = 0; index < NELEMENTS(axis); index++) {
            axis[index].zone  = axis[index].lastZone;
            axis[index].timer = 0;
        }

        // Group axes together depending on Transmitter mode.  Grouping is (Vertical Axis + Horizontal Axis).
        // TODO Find a better way to do this so that we are Tx mode agnostic
        #define PITCH_ZONE  (axis[PITCH].zone << 4)
        #define THRUST_ZONE (axis[THRUST].zone << 4)
        #define YAW_ZONE    (axis[YAW].zone)
        #define ROLL_ZONE   (axis[ROLL].zone)

        MaintenanceModeInfoPositionElem command[MAINTENANCEMODEINFO_POSITION_NUMELEM];
        switch (settings->TxMode) {
        case MANUALCONTROLSETTINGS_TXMODE_MODE1:
            command[MAINTENANCEMODEINFO_POSITION_LEFTSTICK]  = PITCH_ZONE + YAW_ZONE;
            command[MAINTENANCEMODEINFO_POSITION_RIGHTSTICK] = THRUST_ZONE + ROLL_ZONE;
            break;
        case MANUALCONTROLSETTINGS_TXMODE_MODE2:
            command[MAINTENANCEMODEINFO_POSITION_LEFTSTICK]  = THRUST_ZONE + YAW_ZONE;
            command[MAINTENANCEMODEINFO_POSITION_RIGHTSTICK] = PITCH_ZONE + ROLL_ZONE;
            break;
        case MANUALCONTROLSETTINGS_TXMODE_MODE3:
            command[MAINTENANCEMODEINFO_POSITION_LEFTSTICK]  = PITCH_ZONE + ROLL_ZONE;
            command[MAINTENANCEMODEINFO_POSITION_RIGHTSTICK] = THRUST_ZONE + YAW_ZONE;
            break;
        case MANUALCONTROLSETTINGS_TXMODE_MODE4:
            command[MAINTENANCEMODEINFO_POSITION_LEFTSTICK]  = THRUST_ZONE + ROLL_ZONE;
            command[MAINTENANCEMODEINFO_POSITION_RIGHTSTICK] = PITCH_ZONE + YAW_ZONE;
            break;
        default:
            return;
        }

        MaintenanceModeInfoData info;
        MaintenanceModeInfoGet(&info);

        // Build a composite state for each stick made of Vertical Axis in High Nibble, and Horizontal axis in Low nibble
        #define V_AXIS_MAX    (AXIS_MAX << 4)
        #define V_AXIS_CENTRE (AXIS_CENTRE << 4)
        #define V_AXIS_MIN    (AXIS_MIN << 4)
        #define H_AXIS_MAX    AXIS_MAX
        #define H_AXIS_CENTRE AXIS_CENTRE
        #define H_AXIS_MIN    AXIS_MIN

        for (index = 0; index < MAINTENANCEMODEINFO_POSITION_NUMELEM; index++) {
            MaintenanceModeInfoPositionToArray(info.Position)[index] =
                (command[index] == (V_AXIS_CENTRE + H_AXIS_CENTRE)) ? MAINTENANCEMODEINFO_POSITION_CENTRE :
                (command[index] == (V_AXIS_MAX + H_AXIS_MIN)) ? MAINTENANCEMODEINFO_POSITION_UPLEFT :
                (command[index] == (V_AXIS_MAX + H_AXIS_CENTRE)) ? MAINTENANCEMODEINFO_POSITION_UP :
                (command[index] == (V_AXIS_MAX + H_AXIS_MAX)) ? MAINTENANCEMODEINFO_POSITION_UPRIGHT :
                (command[index] == (V_AXIS_CENTRE + H_AXIS_MAX)) ? MAINTENANCEMODEINFO_POSITION_RIGHT :
                (command[index] == (V_AXIS_MIN + H_AXIS_MAX)) ? MAINTENANCEMODEINFO_POSITION_DOWNRIGHT :
                (command[index] == (V_AXIS_MIN + H_AXIS_CENTRE)) ? MAINTENANCEMODEINFO_POSITION_DOWN :
                (command[index] == (V_AXIS_MIN + H_AXIS_MIN)) ? MAINTENANCEMODEINFO_POSITION_DOWNLEFT :
                (command[index] == (V_AXIS_CENTRE + H_AXIS_MIN)) ? MAINTENANCEMODEINFO_POSITION_LEFT : MAINTENANCEMODEINFO_POSITION_UNKNOWN;
        }

        // Update this UAVO at any time as they are benign.
        MaintenanceModeInfoSet(&info);

        // Generate a command event based on the combination of BOTH sticks.
        // Only execute Calibration Commands when Maintenance Mode is active
        if (info.MaintenanceModeActive == MAINTENANCEMODEINFO_MAINTENANCEMODEACTIVE_TRUE) {
            (*calCommandTable[info.Position.LeftStick][info.Position.RightStick])();
        } else {
            // In the future, info.MaintenanceModeActive will be changed from a simple boolean to an enumeration
            // to allow for different modes, or even to allow stick commands to be used as switches for flight control,
            // eg. change flight mode for use on simple transmitters without flight mode switches (obviously whilst disarmed).
            // ... eg:
            // (*someOtherTable[info.Command.LeftStick][info.Command.RightStick])();
            // ...
        }
    }
}


/**
 * @}
 * @}
 */
