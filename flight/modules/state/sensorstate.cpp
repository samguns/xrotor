/**
 ******************************************************************************
 * @file       sensorstate.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#include "project_xflight.h"
#include "revosettings.h"
#include "sensorstate.h"

SensorState::SensorState()
    : lastAlarm(FILTERRESULT_UNINITIALISED), alarmcounter(0)
{}

void SensorState::init()
{
    magHandler.initialise();
    gpsPosHandler.init();
    baroHandler.init(&gpsPosHandler);
    airSpeedHandler.init(&baroHandler);
    altitudeState.initialise(&baroHandler, &gpsPosHandler, &gpsVelHandler, &accelHandler);
    cfState.init(&baroHandler, &airSpeedHandler, &gpsPosHandler, &gpsVelHandler, &accelHandler, &magHandler, &gyroHandler, &altitudeState);
    ekfState.init(&baroHandler, &airSpeedHandler, &gpsPosHandler, &gpsVelHandler, &accelHandler, &magHandler, &gyroHandler, &altitudeState);
    fusionAlgorithm = REVOSETTINGS_FUSIONALGORITHM_NONE;
    settingsUpdated();
}

SensorState::~SensorState()
{}

void SensorState::settingsUpdated()
{
    RevoSettingsGet(&revoSettings);

    // check if a new filter chain should be initialized
    if (fusionAlgorithm != revoSettings.FusionAlgorithm) {
        FlightStatusData fs;
        FlightStatusGet(&fs);
        if (fs.Armed == FLIGHTSTATUS_ARMED_DISARMED || fusionAlgorithm == REVOSETTINGS_FUSIONALGORITHM_NONE) {
            fusionAlgorithm = revoSettings.FusionAlgorithm;
            switch (fusionAlgorithm) {
            case REVOSETTINGS_FUSIONALGORITHM_BASICCOMPLEMENTARY:
            case REVOSETTINGS_FUSIONALGORITHM_COMPLEMENTARYMAG:
            case REVOSETTINGS_FUSIONALGORITHM_COMPLEMENTARYMAGGPSOUTDOOR:
                cfState.setFilterOption(fusionAlgorithm);
                break;
            case REVOSETTINGS_FUSIONALGORITHM_INS13INDOOR:
            case REVOSETTINGS_FUSIONALGORITHM_GPSNAVIGATIONINS13:
                ekfState.setFilterOption(fusionAlgorithm);
                break;
            default:
                break;
            }
        }
    }
}

void SensorState::updateState()
{
    filterResult result = FILTERRESULT_OK;

    switch (fusionAlgorithm) {
    case REVOSETTINGS_FUSIONALGORITHM_BASICCOMPLEMENTARY:
        result = cfState.filter_bc();
        break;
    case REVOSETTINGS_FUSIONALGORITHM_COMPLEMENTARYMAG:
        result = cfState.filter_cm();
        break;
    case REVOSETTINGS_FUSIONALGORITHM_COMPLEMENTARYMAGGPSOUTDOOR:
        result = cfState.filter_cmgo();
        break;
    case REVOSETTINGS_FUSIONALGORITHM_INS13INDOOR:
        result = ekfState.filter_ins13_indoor();
    case REVOSETTINGS_FUSIONALGORITHM_GPSNAVIGATIONINS13:
        result = ekfState.filter_ins13_gps();
        break;
    default:
        result = FILTERRESULT_UNINITIALISED;
        break;
    }

    // throttle alarms, raise alarm flags immediately
    // but require system to run for a while before decreasing
    // to prevent alarm flapping
    if (result >= lastAlarm) {
        lastAlarm    = result;
        alarmcounter = 0;
    } else {
        if (alarmcounter < 100) {
            alarmcounter++;
        } else {
            lastAlarm    = result;
            alarmcounter = 0;
        }
    }

    // clear alarms if everything is alright, then schedule callback execution after timeout
    if (lastAlarm == FILTERRESULT_WARNING) {
        AlarmsSet(SYSTEMALARMS_ALARM_ATTITUDE, SYSTEMALARMS_ALARM_WARNING);
    } else if (lastAlarm == FILTERRESULT_CRITICAL) {
        AlarmsSet(SYSTEMALARMS_ALARM_ATTITUDE, SYSTEMALARMS_ALARM_CRITICAL);
    } else if (lastAlarm >= FILTERRESULT_ERROR) {
        AlarmsSet(SYSTEMALARMS_ALARM_ATTITUDE, SYSTEMALARMS_ALARM_ERROR);
    } else {
        AlarmsClear(SYSTEMALARMS_ALARM_ATTITUDE);
    }
}
