/**
 ******************************************************************************
 * @file       gyrohandler.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#include "project_xflight.h"
#include "ekfbias.h"
#include "gyrohandler.h"

GyroHandler::GyroHandler()
    : updated(false)
{
    gyro[0] = 0.0f;
    gyro[1] = 0.0f;
    gyro[2] = 0.0f;
}

GyroHandler::~GyroHandler()
{}
