/**
 ******************************************************************************
 * @file       cfstate.cpp
 * @author     Alex Beck
 * @brief      Complementary filter to calculate Attitude from Accels and Gyros
 *             and optionally magnetometers:
 *
 ******************************************************************************/
#include <project_xflight.h>
#include "statetypes.h"
#include <attitudesettings.h>
#include <attitudestate.h>
#include <flightstatus.h>
#include <homelocation.h>
#include <calibrationmags.h>
#include <libmatrix/orientation.h>
#include <positionstate.h>
#include <velocitystate.h>
#include <airspeedstate.h>
#include <pios_notify.h>

// state module headers
#include "cfstate.h"
#include "barohandler.h"
#include "gpsposhandler.h"
#include "gpsvelhandler.h"
#include "accelhandler.h"
#include "gyrohandler.h"
#include "maghandler.h"
#include "altitudestate.h"
#include "airspeedhandler.h"

// Private constants

#define CALIBRATION_DELAY_MS    4000
#define CALIBRATION_DURATION_MS 6000

CFState::CFState()
{}

CFState::~CFState()
{}

void CFState::settingsUpdated()
{
    FlightStatusGet(&flightStatus);
    AttitudeSettingsGet(&attitudeSettings);
    HomeLocationGet(&homeLocation);
}

void CFState::init(BaroHandler *baro_handler, AirSpeedHandler *airspeed, GPSPosHandler *gps_pos_handler, GPSVelocityHandler *gps_vel_handler,
                   AccelHandler *accel_handler, MagHandler *mag_handler, GyroHandler *gyro_handler, AltitudeState *altitude_state)
{
    baroHandler        = baro_handler;
    gpsPositionHandler = gps_pos_handler;
    gpsVelocityHandler = gps_vel_handler;
    accelHandler       = accel_handler;
    magHandler         = mag_handler;
    gyroHandler        = gyro_handler;
    altitudeState      = altitude_state;
    airspeedHandler    = airspeed;

    settingsUpdated();

    // set init with or without mag TODO
    useMag        = 1;
    first_run     = 1;
    accelUpdated  = 0;
    magCalibrated = true;

    const float fakeDt = 0.0025f;
    if (attitudeSettings.AccelTau < 0.0001f) {
        accel_alpha = 0; // not trusting this to resolve to 0
        accel_filter_enabled = false;
    } else {
        accel_alpha = expf(-fakeDt / this->attitudeSettings.AccelTau);
        accel_filter_enabled = true;
    }

    // reset gyro Bias
    gyroBias[0] = 0.0f;
    gyroBias[1] = 0.0f;
    gyroBias[2] = 0.0f;
}

void CFState::setFilterOption(RevoSettingsFusionAlgorithmOptions fusion_option)
{
    fusionOption = fusion_option;
    switch (fusionOption) {
    case REVOSETTINGS_FUSIONALGORITHM_BASICCOMPLEMENTARY:
        useMag = 0;
        baroHandler->disableGPS();

        break;
    case REVOSETTINGS_FUSIONALGORITHM_COMPLEMENTARYMAG:
        useMag = 1;
        baroHandler->disableGPS();

        break;
    case REVOSETTINGS_FUSIONALGORITHM_COMPLEMENTARYMAGGPSOUTDOOR:
        useMag = 1;
        baroHandler->enableGPS();

        break;
    default:
        break;
    }
}


inline void CFState::exportUAVO()
{
    gyroHandler->exportUAVO(gyroBias);
    accelHandler->exportUAVO(accels_filtered);
    if (useMag) {
        magHandler->clearSetFlag();
    }

    // export the position; gps provides NE and altitude state provides down
    PositionStateData position;
    PositionStateGet(&position);
    position.North = 0.0f;
    position.East  = 0.0f;
    position.Down  = 0.0f;
    if (gpsPositionHandler->isSet()) {
        position.North = gpsPositionHandler->North();
        position.East  = gpsPositionHandler->East();
    }
    position.Down = altitudeState->positionDown();
    PositionStateSet(&position);

    VelocityStateData velocity;
    VelocityStateGet(&velocity);
    velocity.North = 0.0f;
    velocity.East  = 0.0f;
    velocity.Down  = 0.0f;
    if (gpsVelocityHandler->isSet()) {
        velocity.North = gpsVelocityHandler->North();
        velocity.East  = gpsVelocityHandler->East();
    }
    velocity.Down = altitudeState->velocityDown();
    VelocityStateSet(&velocity);

    airspeedHandler->exportUAVO();

    // attitude nees manual conversion from quaternion to euler
    // if (IS_SET(updated, SENSORUPDATES_attitude)) {
    AttitudeStateData s;
    AttitudeStateGet(&s);
    s.q1 = attitude[0];
    s.q2 = attitude[1];
    s.q3 = attitude[2];
    s.q4 = attitude[3];
    Quaternion2RPY(&s.q1, &s.Roll);
    AttitudeStateSet(&s);
}


inline void CFState::apply_accel_filter(float *raw, float *filtered)
{
    if (accel_filter_enabled) {
        filtered[0] = filtered[0] * accel_alpha + raw[0] * (1 - accel_alpha);
        filtered[1] = filtered[1] * accel_alpha + raw[1] * (1 - accel_alpha);
        filtered[2] = filtered[2] * accel_alpha + raw[2] * (1 - accel_alpha);
    } else {
        filtered[0] = raw[0];
        filtered[1] = raw[1];
        filtered[2] = raw[2];
    }
}

inline filterResult CFState::complementaryFilter()
{
    float dT;
    float mag[3];
    float accel[3];

    accel[0] = (*accelHandler)[0];
    accel[1] = (*accelHandler)[1];
    accel[2] = (*accelHandler)[2];

    float gyro[3];
    gyro[0]  = (*gyroHandler)[0];
    gyro[1]  = (*gyroHandler)[1];
    gyro[2]  = (*gyroHandler)[2];

    // During initialization and
    if (first_run) {
#if defined(PIOS_INCLUDE_HMC5X83)
        // wait until mags have been updated
        if (useMag && !magHandler->isSet()) {
            return FILTERRESULT_ERROR;
        }
        mag[0] = (*magHandler)[0];
        mag[1] = (*magHandler)[1];
        mag[2] = (*magHandler)[2];
#else
        mag[0] = 100.0f;
        mag[1] = 0.0f;
        mag[2] = 0.0f;
#endif
        float magBias[3];
        CalibrationMagsOnboardBiasArrayGet(magBias);
        // don't trust Mag for initial orientation if it has not been calibrated
        if (fabsf(magBias[0]) < 1e-6f && fabsf(magBias[1]) < 1e-6f && fabsf(magBias[2]) < 1e-6f) {
            magCalibrated = false;
            mag[0] = 100.0f;
            mag[1] = 0.0f;
            mag[2] = 0.0f;
        }

        AttitudeStateData attitudeState; // base on previous state
        AttitudeStateGet(&attitudeState);
        inited = 0;

        // Set initial attitude. Use accels to determine roll and pitch, rotate magnetic measurement accordingly,
        // so pseudo "north" vector can be estimated even if the board is not level
        attitudeState.Roll = atan2f(-accel[1], -accel[2]);
        float zn  = cosf(attitudeState.Roll) * mag[2] + sinf(attitudeState.Roll) * mag[1];
        float yn  = cosf(attitudeState.Roll) * mag[1] - sinf(attitudeState.Roll) * mag[2];

        // rotate accels z vector according to roll
        float azn = cosf(attitudeState.Roll) * accel[2] + sinf(attitudeState.Roll) * accel[1];
        attitudeState.Pitch = atan2f(accel[0], -azn);

        float xn  = cosf(attitudeState.Pitch) * mag[0] + sinf(attitudeState.Pitch) * zn;

        attitudeState.Yaw = atan2f(-yn, xn);
        // TODO: This is still a hack
        // Put this in a proper generic function in CoordinateConversion.c
        // should take 4 vectors: g (0,0,-9.81), accels, Be (or 1,0,0 if no home loc) and magnetometers (or 1,0,0 if no mags)
        // should calculate the rotation in 3d space using proper cross product math
        // SUBTODO: formulate the math required

        attitudeState.Roll  = RAD2DEG(attitudeState.Roll);
        attitudeState.Pitch = RAD2DEG(attitudeState.Pitch);
        attitudeState.Yaw   = RAD2DEG(attitudeState.Yaw);

        RPY2Quaternion(&attitudeState.Roll, attitude);

        first_run = 0;
        accels_filtered[0] = 0.0f;
        accels_filtered[1] = 0.0f;
        accels_filtered[2] = 0.0f;
        grot_filtered[0]   = 0.0f;
        grot_filtered[1]   = 0.0f;
        grot_filtered[2]   = 0.0f;
        timeval   = PIOS_DELAY_GetRaw(); // Cycle counter used for precise timing
        starttime = xTaskGetTickCount(); // Tick counter used for long time intervals

        return FILTERRESULT_OK; // must return OK on initial initialization, so attitude will init with a valid quaternion
    }

    if (inited == 0 && xTaskGetTickCount() - starttime < CALIBRATION_DELAY_MS / portTICK_RATE_MS) {
        // wait 4 seconds for the user to get his hands off in case the board was just powered
        timeval = PIOS_DELAY_GetRaw();
        return FILTERRESULT_ERROR;
    } else if (inited == 0 && xTaskGetTickCount() - starttime < (CALIBRATION_DELAY_MS + CALIBRATION_DURATION_MS) / portTICK_RATE_MS) {
        // For first 6 seconds use accels to get gyro bias
        attitudeSettings.AccelKp     = 1.0f;
        attitudeSettings.AccelKi     = 0.0f;
        attitudeSettings.YawBiasRate = 0.23f;
        accel_filter_enabled   = false;
        rollPitchBiasRate      = 0.01f;
        attitudeSettings.MagKp = this->magCalibrated ? 1.0f : 0.0f;
        PIOS_NOTIFY_StartNotification(NOTIFY_DRAW_ATTENTION, NOTIFY_PRIORITY_REGULAR);
    } else if ((attitudeSettings.ZeroDuringArming == ATTITUDESETTINGS_ZERODURINGARMING_TRUE) && (flightStatus.Armed == FLIGHTSTATUS_ARMED_ARMING)) {
        attitudeSettings.AccelKp     = 1.0f;
        attitudeSettings.AccelKi     = 0.0f;
        attitudeSettings.YawBiasRate = 0.23f;
        accel_filter_enabled   = false;
        rollPitchBiasRate      = 0.01f;
        attitudeSettings.MagKp = this->magCalibrated ? 1.0f : 0.0f;
        inited = 0;
        PIOS_NOTIFY_StartNotification(NOTIFY_DRAW_ATTENTION, NOTIFY_PRIORITY_REGULAR);
    } else if (this->inited == 0) {
        // Reload settings (all the rates)
        AttitudeSettingsGet(&attitudeSettings);
        rollPitchBiasRate = 0.0f;
        if (accel_alpha > 0.0f) {
            accel_filter_enabled = true;
        }
        inited = 1;
    }

    // Compute the dT using the cpu clock
    dT = PIOS_DELAY_DiffuS(this->timeval) / 1000000.0f;
    timeval = PIOS_DELAY_GetRaw();
    if (dT < 0.001f) { // safe bounds
        dT = 0.001f;
    }

    AttitudeStateData attitudeState; // base on previous state
    AttitudeStateGet(&attitudeState);

    // Get the current attitude estimate
    quat_copy(&attitudeState.q1, attitude);

    // Apply smoothing to accel values, to reduce vibration noise before main calculations.
    apply_accel_filter(accel, accels_filtered);

    // Rotate gravity to body frame and cross with accels
    float grot[3];
    grot[0] = -(2.0f * (attitude[1] * attitude[3] - attitude[0] * attitude[2]));
    grot[1] = -(2.0f * (attitude[2] * attitude[3] + attitude[0] * attitude[1]));
    grot[2] = -(attitude[0] * attitude[0] - attitude[1] * attitude[1] - attitude[2] * attitude[2] + attitude[3] * attitude[3]);

    float accel_err[3];
    apply_accel_filter(grot, grot_filtered);

    CrossProduct((const float *)accels_filtered, (const float *)grot_filtered, accel_err);

    // Account for accel magnitude
    float accel_mag = sqrtf(accels_filtered[0] * accels_filtered[0] + accels_filtered[1] * accels_filtered[1] + accels_filtered[2] * accels_filtered[2]);
    if (accel_mag < 1.0e-3f) {
        return FILTERRESULT_CRITICAL; // safety feature copied from CC
    }

    // Account for filtered gravity vector magnitude
    float grot_mag;
    if (accel_filter_enabled) {
        grot_mag = sqrtf(grot_filtered[0] * grot_filtered[0] + grot_filtered[1] * grot_filtered[1] + grot_filtered[2] * grot_filtered[2]);
    } else {
        grot_mag = 1.0f;
    }
    if (grot_mag < 1.0e-3f) {
        return FILTERRESULT_CRITICAL;
    }

    accel_err[0] /= (accel_mag * grot_mag);
    accel_err[1] /= (accel_mag * grot_mag);
    accel_err[2] /= (accel_mag * grot_mag);

    float mag_err[3] = { 0.0f };
    if (useMag && magHandler->isSet()) {
        // Rotate gravity to body frame and cross with accels
        float brot[3];
        float Rbe[3][3];

        Quaternion2R(attitude, Rbe);

        rot_mult(Rbe, homeLocation.Be, brot);

        float mag_len = sqrtf(mag[0] * mag[0] + mag[1] * mag[1] + mag[2] * mag[2]);
        mag[0]  /= mag_len;
        mag[1]  /= mag_len;
        mag[2]  /= mag_len;

        float bmag = sqrtf(brot[0] * brot[0] + brot[1] * brot[1] + brot[2] * brot[2]);
        brot[0] /= bmag;
        brot[1] /= bmag;
        brot[2] /= bmag;

        // Only compute if neither vector is null
        if (bmag < 1.0f || mag_len < 1.0f) {
            mag_err[0] = mag_err[1] = mag_err[2] = 0.0f;
        } else {
            CrossProduct((const float *)mag, (const float *)brot, mag_err);
        }
    } else {
        mag_err[0] = mag_err[1] = mag_err[2] = 0.0f;
    }

    // Correct rates based on integral coefficient
    gyro[0]     -= gyroBias[0];
    gyro[1]     -= gyroBias[1];
    gyro[2]     -= gyroBias[2];

    // Accumulate integral of error.  Scale here so that units are (deg/s) but Ki has units of s
    gyroBias[0] -= accel_err[0] * attitudeSettings.AccelKi - gyro[0] * rollPitchBiasRate;
    gyroBias[1] -= accel_err[1] * attitudeSettings.AccelKi - gyro[1] * rollPitchBiasRate;
    if (useMag) {
        gyroBias[2] -= -mag_err[2] * this->attitudeSettings.MagKi - gyro[2] * rollPitchBiasRate;
    } else {
        gyroBias[2] -= -gyro[2] * rollPitchBiasRate;
    }

    float gyrotmp[3] = { gyro[0], gyro[1], gyro[2] };
    // Correct rates based on proportional coefficient
    gyrotmp[0] += accel_err[0] * attitudeSettings.AccelKp / dT;
    gyrotmp[1] += accel_err[1] * attitudeSettings.AccelKp / dT;
    if (useMag) {
        gyrotmp[2] += accel_err[2] * attitudeSettings.AccelKp / dT + mag_err[2] * attitudeSettings.MagKp / dT;
    } else {
        gyrotmp[2] += accel_err[2] * attitudeSettings.AccelKp / dT;
    }

    // Work out time derivative from INSAlgo writeup
    // Also accounts for the fact that gyros are in deg/s
    float qdot[4];
    qdot[0]     = DEG2RAD(-attitude[1] * gyrotmp[0] - attitude[2] * gyrotmp[1] - attitude[3] * gyrotmp[2]) * dT / 2;
    qdot[1]     = DEG2RAD(attitude[0] * gyrotmp[0] - attitude[3] * gyrotmp[1] + attitude[2] * gyrotmp[2]) * dT / 2;
    qdot[2]     = DEG2RAD(attitude[3] * gyrotmp[0] + attitude[0] * gyrotmp[1] - attitude[1] * gyrotmp[2]) * dT / 2;
    qdot[3]     = DEG2RAD(-attitude[2] * gyrotmp[0] + attitude[1] * gyrotmp[1] + attitude[0] * gyrotmp[2]) * dT / 2;

    // Take a time step
    attitude[0] = attitude[0] + qdot[0];
    attitude[1] = attitude[1] + qdot[1];
    attitude[2] = attitude[2] + qdot[2];
    attitude[3] = attitude[3] + qdot[3];

    if (attitude[0] < 0.0f) {
        attitude[0] = -attitude[0];
        attitude[1] = -attitude[1];
        attitude[2] = -attitude[2];
        attitude[3] = -attitude[3];
    }

    // Renomalize
    float qmag = sqrtf(attitude[0] * attitude[0] + attitude[1] * attitude[1] + attitude[2] * attitude[2] + attitude[3] * attitude[3]);
    attitude[0] = attitude[0] / qmag;
    attitude[1] = attitude[1] / qmag;
    attitude[2] = attitude[2] / qmag;
    attitude[3] = attitude[3] / qmag;

    // If quaternion has become inappropriately short or is nan reinit.
    // THIS SHOULD NEVER ACTUALLY HAPPEN
    if ((fabsf(qmag) < 1.0e-3f) || isnan(qmag)) {
        first_run = 1;
        return FILTERRESULT_WARNING;
    }

    if (inited) {
        return FILTERRESULT_OK;
    } else {
        return FILTERRESULT_CRITICAL; // return "critical" for now, so users can see the zeroing period, switch to more graceful notification later
    }
}

/**
 * Collect all required state variables, then run complementary filter
 */
filterResult CFState::filter_bc()
{
    filterResult result = FILTERRESULT_OK;

    if (gyroHandler->isSet()) {
        if (accelHandler->isSet()) {
            altitudeState->filter();

            result = complementaryFilter();
            exportUAVO();
        }
    }
    return result;
}

filterResult CFState::filter_cm()
{
    filterResult result = FILTERRESULT_OK;

    if (gyroHandler->isSet()) {
        if (accelHandler->isSet()) {
            magHandler->filter();
            altitudeState->filter();
            result = complementaryFilter();
            exportUAVO();
        }
    }
    return result;
}
filterResult CFState::filter_cmgo()
{
    filterResult result = FILTERRESULT_OK;

    if (gyroHandler->isSet()) {
        if (accelHandler->isSet()) {
            magHandler->filter();
            altitudeState->filter();
            result = complementaryFilter();
            exportUAVO();
        }
    }
    return result;
}


/**
 * @}
 * @}
 */
