/**
 ******************************************************************************
 * @file       altitudestate.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      altitude state management for use with comp filter
 *
 *****************************************************************************/

#include <project_xflight.h>
#include "statetypes.h"
#include <attitudestate.h>
#include <altitudefiltersettings.h>
#include <homelocation.h>
#include <libmatrix/orientation.h>

#include "altitudestate.h"
#include "barohandler.h"
#include "accelhandler.h"
#include "gpsposhandler.h"
#include "gpsvelhandler.h"

// Private constants

// duration of accel bias initialization phase
#define INITIALIZATION_DURATION_MS 5000

#define DT_ALPHA                   1e-2f
#define DT_MIN                     1e-6f
#define DT_MAX                     1.0f
#define DT_AVERAGE                 1e-3f

AltitudeState::AltitudeState()
{}
AltitudeState::~AltitudeState()
{}

void AltitudeState::initialise(BaroHandler *baro_handler, GPSPosHandler *gps_position_handler, GPSVelocityHandler *gps_velocity_handler, AccelHandler *accel_handler)
{
    baroHandler        = baro_handler;
    gpsPositionHandler = gps_position_handler;
    gpsVelocityHandler = gps_velocity_handler;
    accelHandler       = accel_handler;
    altitudeState      = 0.0f;
    velocityState      = 0.0f;
    accelBiasState     = 0.0f;
    accelState = 0.0f;
    pos[0]     = 0.0f;
    pos[1]     = 0.0f;
    pos[2]     = 0.0f;
    vel[0]     = 0.0f;
    vel[1]     = 0.0f;
    vel[2]     = 0.0f;
    PIOS_DELTATIME_Init(&dt1config, DT_AVERAGE, DT_MIN, DT_MAX, DT_ALPHA);
    PIOS_DELTATIME_Init(&dt2config, DT_AVERAGE, DT_MIN, DT_MAX, DT_ALPHA);
    baroLast   = 0.0f;
    accelLast  = 0.0f;
    first_run  = 1;
    settingsUpdated();
}

void AltitudeState::settingsUpdated()
{
    HomeLocationg_eGet(&gravity);
    AltitudeFilterSettingsGet(&settings);
}

filterResult AltitudeState::filter()
{
    if (first_run) {
        // Initialize to current altitude reading at initial location
        if (baroHandler->isSet()) {
            first_run = 0;
            initTimer = xTaskGetTickCount();
        }
    } else {
        if (accelHandler->isSet()) {
            AttitudeStateData att;
            AttitudeStateGet(&att);

            att.Roll  = atan2f(-(*accelHandler)[1], -(*accelHandler)[2]);
            float azn = cosf(att.Roll) * (*accelHandler)[2] + sinf(att.Roll) * (*accelHandler)[1];
            att.Pitch = atan2f((*accelHandler)[0], -azn);
            float xn  = cosf(att.Pitch) * 100.0f;
            att.Yaw   = atan2f(-0.0f, xn);
            att.Roll  = RAD2DEG(att.Roll);
            att.Pitch = RAD2DEG(att.Pitch);
            att.Yaw   = RAD2DEG(att.Yaw);
            RPY2Quaternion(&att.Roll, &att.q1);
            float Rbe[3][3];
            Quaternion2R(&att.q1, Rbe);
            float current = -(Rbe[0][2] * (*accelHandler)[0] + Rbe[1][2] * (*accelHandler)[1] + Rbe[2][2] * (*accelHandler)[2] + gravity);

            // low pass filter accelerometers
            accelState = (1.0f - settings.AccelLowPassKp) * accelState + settings.AccelLowPassKp * current;
            if (((xTaskGetTickCount() - initTimer) / portTICK_RATE_MS) < INITIALIZATION_DURATION_MS) {
                // allow the offset to reach quickly the target value in case of small AccelDriftKi
                accelBiasState = (1.0f - settings.InitializationAccelDriftKi) * accelBiasState + settings.InitializationAccelDriftKi * accelState;
            } else {
                // correct accel offset (low pass zeroing)
                accelBiasState = (1.0f - settings.AccelDriftKi) * accelBiasState + settings.AccelDriftKi * accelState;
            }
            // correct velocity and position state (integration)
            // low pass for average dT, compensate timing jitter from scheduler
            //
            float dT = PIOS_DELTATIME_GetAverageSeconds(&dt1config);
            float speedLast = velocityState;

            velocityState += 0.5f * (accelLast + (accelState - accelBiasState)) * dT;
            accelLast      = accelState - accelBiasState;

            altitudeState += 0.5f * (speedLast + velocityState) * dT;
        }

        if (baroHandler->isSet()) {
            // correct the altitude state (simple low pass)
            altitudeState = (1.0f - settings.BaroKp) * altitudeState + settings.BaroKp * baroHandler->altitude();

            // correct the velocity state (low pass differentiation)
            // low pass for average dT, compensate timing jitter from scheduler
            float dT = PIOS_DELTATIME_GetAverageSeconds(&dt2config);
            velocityState = (1.0f - (settings.BaroKp * settings.BaroKp)) * velocityState + (settings.BaroKp * settings.BaroKp) * (baroHandler->altitude() - baroLast) / dT;
            baroLast = baroHandler->altitude();
        }
    }

    return FILTERRESULT_OK;
}

/**
 * @}
 * @}
 */
