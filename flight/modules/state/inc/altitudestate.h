/**
 ******************************************************************************
 * @file       altitudestate.h
 * @author     Alex Beck, OPNG. Copyright (C) 2015.
 * @brief      Interface class for altitude state
 *
 *****************************************************************************/
#ifndef ALTITUDESTATE_H
#define ALTITUDESTATE_H

#include <project_xflight.h>
#include <statetypes.h>
#include "altitudefiltersettings.h"

class BaroHandler;
class GPSPosHandler;
class GPSVelocityHandler;
class AccelHandler;

class AltitudeState {
public:
    AltitudeState();
    ~AltitudeState();
    void initialise(BaroHandler *baro_handler, GPSPosHandler *gps_pos_handler, GPSVelocityHandler *gps_vel_handler, AccelHandler *accel_handler);

    void settingsUpdated();
    filterResult filter();

    float positionDown()
    {
        return -altitudeState;
    }
    float velocityDown()
    {
        return -velocityState;
    }

protected:
    BaroHandler *baroHandler;
    GPSPosHandler *gpsPositionHandler;
    GPSVelocityHandler *gpsVelocityHandler;
    AccelHandler *accelHandler;
    float altitudeState; // state = altitude,velocity,accel_offset,accel
    float velocityState;
    float accelBiasState;
    float accelState;
    float pos[3]; // position updates from other filters
    float vel[3]; // position updates from other filters

    PiOSDeltatimeConfig dt1config;
    PiOSDeltatimeConfig dt2config;
    float accelLast;
    float baroLast;
    bool first_run;
    portTickType initTimer;
    AltitudeFilterSettingsData settings;
    float gravity;
    sensorUpdates updated;
};

#endif // ifndef ALTITUDESTATE_H
