/**
 ******************************************************************************
 * @file       barohandler.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Baro sensor handler
 *
 *****************************************************************************/
#ifndef BAROHANDLER_H
#define BAROHANDLER_H

#include <project_xflight.h>
#include <statetypes.h>
#include <barosensor.h>
#include <gpsposhandler.h>

#define INIT_CYCLES 100

class BaroHandler {
public:
    BaroHandler();
    ~BaroHandler();
    void init(GPSPosHandler *);

    // call on every sensor update
    void sensorUpdated()
    {
        BaroSensorGet(&sensor);

        if ((!isnan(sensor.Altitude) && !isinf(sensor.Altitude))) {
            PIOS_DEBUG_PinHigh(3);
            updated = true;

            if (first_run) {
                // Make sure initial location is initialized properly before continuing
                if (useGPS && gpsPositionHandler->isSet()) {
                    if (first_run == INIT_CYCLES) {
                        gpsAlt = gpsPositionHandler->Down();
                        first_run--;
                    }
                }
                // Initialize to current altitude reading at initial location
                if (first_run < INIT_CYCLES || !useGPS) {
                    baroOffset = (((float)(INIT_CYCLES)-first_run) / (float)(INIT_CYCLES)) * baroOffset +
                                 (first_run / (float)(INIT_CYCLES)) * (sensor.Altitude + gpsAlt);
                    baroAlt    = sensor.Altitude;
                    first_run--;
                    updated    = false;
                }
            } else {
                // Track barometric altitude offset with a low pass filter
                // based on GPS altitude if available
                if (useGPS && gpsPositionHandler->isSet()) {
                    baroOffset = baroOffset * baroGPSOffsetCorrectionAlpha +
                                 (1.0f - baroGPSOffsetCorrectionAlpha) * (baroAlt + gpsPositionHandler->Down());
                }
                // calculate bias corrected altitude
                baroAlt = sensor.Altitude;
                sensor.Altitude -= baroOffset;
                PIOS_DEBUG_PinLow(3);
            }
        }
    }

    // export the updated state post state assessment and clear the sensor update flag
    void clearSetFlag()
    {
        if (updated) {
            // clear the updated flag on cycle completion
            updated = false;
        }
    }

    bool initialisationCompleted()
    {
        return first_run == 0;
    }

    void settingsUpdated();
    bool isSet()
    {
        return updated;
    }
    float altitude()
    {
        return sensor.Altitude;
    }
    void enableGPS();
    void disableGPS();

protected:
    BaroSensorData sensor;

    float baroOffset;
    float baroGPSOffsetCorrectionAlpha;
    float baroAlt;
    float gpsAlt;
    int16_t first_run;
    bool useGPS;
    bool updated;
    GPSPosHandler *gpsPositionHandler;
};

#endif // ifndef BAROHANDLER_H
