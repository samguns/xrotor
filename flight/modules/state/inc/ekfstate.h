/**
 ******************************************************************************
 * @file       ekfstate.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for altitude state
 *
 *****************************************************************************/
#ifndef EKFSTATE_H
#define EKFSTATE_H

#include <project_xflight.h>
#include <statetypes.h>
#include "attitudesettings.h"
#include "homelocation.h"
#include "flightstatus.h"
#include "revosettings.h"
#include "ekfconfiguration.h"
#include "ekfstatevariance.h"

class BaroHandler;
class GPSPosHandler;
class GPSVelocityHandler;
class AccelHandler;
class MagHandler;
class GyroHandler;
class AltitudeState;
class AirSpeedHandler;

#define SENSOR_INDEX_MIN  0
#define POS_FROM          0
#define POS_X             0
#define POS_Y             1
#define POS_Z             2
#define POS_TO            2

#define VEL_FROM          3
#define VEL_X             3
#define AIRSPEED_X        3
#define VEL_Y             4
#define VEL_Z             5
#define VEL_TO            5

#define MAG_FROM          6
#define MAG_2D_FROM       6
#define MAG_X             6
#define MAG_Y             7
#define MAG_Z             8
#define MAG_2D_TO         7
#define MAG_TO            8

#define BARO_Z            9
#define SENSOR_INDEX_MAX  9

#define POS_SENSORS       0x007 // 0000 0111
#define HORIZ_POS_SENSORS 0x003 // 0000 0011
#define VER_POS_SENSORS   0x004 // 0000 0100
#define AIRSPEED_SENSORS  0x008 // 0000 1000
#define HORIZ_SENSORS     0x018 // 0001 1000
#define VERT_SENSORS      0x020 // 0010 0000
#define MAG_SENSORS       0x1C0 // 1 1100 0000
#define MAG_2D_SENSORS    0x0C0 // 0 1100 0000
#define BARO_SENSOR       0x200 // 10 0000 0000

#define FULL_SENSORS      0x3FF

#define NAV_POS_X         ekf.X[0]
#define NAV_POS_Y         ekf.X[1]
#define NAV_POS_Z         ekf.X[2]
#define NAV_VEL_X         ekf.X[3]
#define NAV_VEL_Y         ekf.X[4]
#define NAV_VEL_Z         ekf.X[5]
#define NAV_Q_0           ekf.X[6]
#define NAV_Q_1           ekf.X[7]
#define NAV_Q_2           ekf.X[8]
#define NAV_Q_3           ekf.X[9]
#define NAV_GYRO_BIAS_X   ekf.X[10]
#define NAV_GYRO_BIAS_Y   ekf.X[11]
#define NAV_GYRO_BIAS_Z   ekf.X[12]

typedef enum {
    MAG_FUSION_NONE = 0,
    MAG_FUSION_3D   = 1,
    MAG_FUSION_2D   = 2,
} EKFMagFusionOption;

// constants/macros/typdefs
#define NUMX 13 // number of states, X is the state vector
#define NUMW 9 // number of plant noise inputs, w is disturbance noise vector
#define NUMV 10 // number of measurements, v is the measurement noise vector
#define NUMU 6 // number of deterministic inputs, U is the input vector

class EKFState {
public:
    EKFState();
    ~EKFState();
    void init(BaroHandler *baro_handler, AirSpeedHandler *airspeed, GPSPosHandler *gps_pos_handler,
              GPSVelocityHandler *gps_vel_handler, AccelHandler *accel_handler,
              MagHandler *mag_handler, GyroHandler *gyro_handler, AltitudeState *altitude_state);

    void settingsUpdated();
    filterResult filter_ins13_indoor();
    filterResult filter_ins13_gps();

    void setFilterOption(RevoSettingsFusionAlgorithmOptions fusion_option);
    SystemAlarmsExtendedAlarmStatusOptions configCheck();

protected:
    filterResult ins13filterPendingInit();
    filterResult ins13filter();
    void initEKF(float accel[3], float mag[3], float pos[3]);
    inline void predict(float gyro[3], float accel[3], float dT);
    void exportUAVO();
    inline bool invalid_var(float data);

    BaroHandler *baroHandler;
    GPSPosHandler *gpsPositionHandler;
    GPSVelocityHandler *gpsVelocityHandler;
    AccelHandler *accelHandler;
    MagHandler *magHandler;
    GyroHandler *gyroHandler;
    AltitudeState *altitudeState;
    AirSpeedHandler *airSpeedHandler;

    EKFConfigurationData ekfConfiguration;
    HomeLocationData homeLocation;
    RevoSettingsFusionAlgorithmOptions fusionOption;
    PiOSDeltatimeConfig dtconfigPredict;
    PiOSDeltatimeConfig dtconfigEstimate;
    int32_t init_stage;
    uint8_t inited;
    FlightStatusArmedOptions armed;
    bool usePos;
    bool initialized;

    struct EKFData {
        // linearized system matrices
        float F[NUMX][NUMX];
        float G[NUMX][NUMW];
        float H[NUMV][NUMX];
        // local magnetic unit vector in NED frame
        float Be[3];
        // local gravity vector
        float g_e;
        // covariance matrix and state vector
        float P[NUMX][NUMX];
        float X[NUMX];
        // input noise and measurement noise variances
        float Q[NUMW];
        float R[NUMV];
    } ekf;

    EKFStateVarianceData varianceData;

    // Exposed Function Prototypes
    void INSGPSInit();
    void INSStatePrediction(float gyro_data[3], float accel_data[3], float dT);
    void INSCorrection(float mag_data[3], const EKFMagFusionOption option, float Pos[3], float Vel[3], float BaroAlt, uint16_t SensorsUsed, uint8_t sensor_index_from, uint8_t sensor_index_to);

    void INSResetP(float PDiag[13]);
    void INSGetP(float PDiag[13]);
    void INSSetState(float pos[3], float vel[3], float q[4], float gyro_bias[3], float accel_bias[3]);
    void INSSetPosVelVar(float PosVar[3], float VelVar[3]);
    void INSSetGyroBias(float gyro_bias[3]);
    void INSSetAccelVar(float accel_var[3]);
    void INSSetGyroVar(float gyro_var[3]);
    void INSSetGyroBiasVar(float gyro_bias_var[3]);
    void INSSetMagNorth(float B[3]);
    void INSSetG(float g_e);
    void INSSetMagVar(float scaled_mag_var[3]);
    void INSSetBaroVar(float baro_var);
    void INSPosVelReset(float pos[3], float vel[3]);

    static inline void CovariancePrediction(const float F[NUMX][NUMX], const float G[NUMX][NUMW],
                                            const float Q[NUMW], float dT, float P[NUMX][NUMX]);
    static inline void SerialUpdate(const float H[NUMV][NUMX], const float R[NUMV], const float Z[NUMV],
                                    const float Y[NUMV], float P[NUMX][NUMX], float X[NUMX],
                                    const uint16_t SensorsUsed, uint8_t sensor_index_from, uint8_t sensor_index_to);
    static inline void RungeKutta(float X[NUMX], const float U[NUMU], const float g_e, float dT);
    static inline void RungeKutta2ndOrder(float X[NUMX], const float U[NUMU], const float g_e, float dT);
    static inline void StateEq(const float X[NUMX], const float U[NUMU], const float g_e, float Xdot[NUMX]);
    static inline void LinearizeFG(const float X[NUMX], const float U[NUMU], float F[NUMX][NUMX],
                                   float G[NUMX][NUMW]);
    static inline void MeasurementEq(const float X[NUMX], const float Be[3], const EKFMagFusionOption option, float Y[NUMV]);

    static inline void LinearizeH(const float X[NUMX], const float Be[3], const EKFMagFusionOption option, float H[NUMV][NUMX]);


    uint16_t ins_get_num_states();
};

#endif // ifndef EKFSTATE_H
