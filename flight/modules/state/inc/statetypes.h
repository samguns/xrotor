/*
 * @file       statetypes.h
 * @author     Alex Beck
 *
 */
#ifndef STATETYPES_H
#define STATETYPES_H

#include <project_xflight.h>

// Enumeration for filter result
typedef enum {
    FILTERRESULT_UNINITIALISED = -1,
    FILTERRESULT_OK       = 0,
    FILTERRESULT_WARNING  = 1,
    FILTERRESULT_CRITICAL = 2,
    FILTERRESULT_ERROR    = 3,
} filterResult;


typedef enum {
    SENSORUPDATES_gyro         = 1 << 0,
        SENSORUPDATES_accel    = 1 << 1,
        SENSORUPDATES_mag      = 1 << 2,
        SENSORUPDATES_boardMag = 1 << 10,
        SENSORUPDATES_auxMag   = 1 << 9,
        SENSORUPDATES_attitude = 1 << 3,
        SENSORUPDATES_pos      = 1 << 4,
        SENSORUPDATES_vel      = 1 << 5,
        SENSORUPDATES_airspeed = 1 << 6,
        SENSORUPDATES_baro     = 1 << 7,
        SENSORUPDATES_lla      = 1 << 8,
} sensorUpdates;

#define MAGSTATUS_OK      1
#define MAGSTATUS_AUX     2
#define MAGSTATUS_INVALID 0

#endif // STABILIZATION_H

/**
 * @}
 * @}
 */
