/**
 ******************************************************************************
 * @file       cfstate.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for altitude state
 *
 *****************************************************************************/
#ifndef CFSTATE_H
#define CFSTATE_H

#include <project_xflight.h>
#include <statetypes.h>
#include "attitudesettings.h"
#include "homelocation.h"
#include "flightstatus.h"
#include "revosettings.h"

class BaroHandler;
class GPSPosHandler;
class GPSVelocityHandler;
class AccelHandler;
class MagHandler;
class GyroHandler;
class AltitudeState;
class AirSpeedHandler;

class CFState {
public:
    CFState();
    ~CFState();
    void init(BaroHandler *baro_handler, AirSpeedHandler *airspeed, GPSPosHandler *gps_pos_handler,
              GPSVelocityHandler *gps_vel_handler, AccelHandler *accel_handler,
              MagHandler *mag_handler, GyroHandler *gyro_handler, AltitudeState *altitude_state);
    void setFilterOption(RevoSettingsFusionAlgorithmOptions fusion_option);

    void settingsUpdated();
    filterResult filter_bc();
    filterResult filter_cm();
    filterResult filter_cmgo();

protected:

    filterResult complementaryFilter();
    void exportUAVO();
    inline void apply_accel_filter(float *raw, float *filtered);

    BaroHandler *baroHandler;
    GPSPosHandler *gpsPositionHandler;
    GPSVelocityHandler *gpsVelocityHandler;
    AccelHandler *accelHandler;
    MagHandler *magHandler;
    GyroHandler *gyroHandler;
    AltitudeState *altitudeState;
    AirSpeedHandler *airspeedHandler;

    // Private types
    AttitudeSettingsData attitudeSettings;
    HomeLocationData homeLocation;
    FlightStatusData flightStatus;
    float currentAccel[3];
    float currentMag[3];
    float accels_filtered[3];
    float grot_filtered[3];
    float gyroBias[3];
    float attitude[4];
    float accel_alpha;
    float rollPitchBiasRate;
    int32_t timeval;
    int32_t starttime;
    uint8_t inited;
    uint32_t updated;
    RevoSettingsFusionAlgorithmOptions fusionOption;
    bool magCalibrated;
    bool first_run;
    bool useMag;
    bool accelUpdated;
    bool magUpdated;
    bool accel_filter_enabled;
};

#endif // ifndef CFSTATE_H
