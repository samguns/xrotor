/**
 ******************************************************************************
 * @file       sensorstate.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#ifndef SENSORSTATE_H
#define SENSORSTATE_H

#include "project_xflight.h"
#include <statetypes.h>
#include "revosettings.h"

#include <maghandler.h>
#include <accelhandler.h>
#include <barohandler.h>
#include <gyrohandler.h>
#include <gpsposhandler.h>
#include <gpsvelhandler.h>
#include <airspeedhandler.h>
#include <altitudestate.h>
#include <cfstate.h>
#include <ekfstate.h>

class SensorState {
public:
    SensorState();
    ~SensorState();

    void init();
    void settingsUpdated();
    void updateState();

    MagHandler magHandler;
    AccelHandler accelHandler;
    BaroHandler baroHandler;
    GyroHandler gyroHandler;
    GPSPosHandler gpsPosHandler;
    GPSVelocityHandler gpsVelHandler;
    AirSpeedHandler airSpeedHandler;
    AltitudeState altitudeState;
    CFState cfState;
    EKFState ekfState;


protected:
    // all sensor UAVO objects
    RevoSettingsData revoSettings;
    RevoSettingsFusionAlgorithmOptions fusionAlgorithm;
    filterResult lastAlarm;
    uint16_t alarmcounter;
};

#endif // ifndef SENSORSTATE_H
