/**
 ******************************************************************************
 * @file       gpsvelhandler.h
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#ifndef GPSVELHANDLER_H
#define GPSVELHANDLER_H

#include <project_xflight.h>
#include <statetypes.h>
#include <gpsvelocitysensor.h>
#include <velocitystate.h>

class GPSVelocityHandler {
public:
    GPSVelocityHandler();
    ~GPSVelocityHandler();

    void sensorUpdated()
    {
        GPSVelocitySensorGet(&sensor);
        if ((!isnan(sensor.North) && !isinf(sensor.North)) && (!isnan(sensor.East) && !isinf(sensor.East)) && (!isnan(sensor.Down) && !isinf(sensor.Down))) {
            updated = true;
        }
    }

    bool isSet()
    {
        return updated;
    }
    float Down()
    {
        return sensor.Down;
    }
    float North()
    {
        return sensor.North;
    }
    float East()
    {
        return sensor.East;
    }
    void exportUAVO()
    {
        if (updated) {
            VelocityStateData s;
            VelocityStateGet(&s);
            s.North = sensor.North;
            s.East  = sensor.East;
            s.Down  = sensor.Down;
            VelocityStateSet(&s);
            updated = false;
        }
    }


protected:
    GPSVelocitySensorData sensor;
    bool updated;
};

#endif // ifndef GPSVELHANDLER_H
