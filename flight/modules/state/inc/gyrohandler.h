/**
 ******************************************************************************
 * @file       gyrohandler.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#ifndef GYROHANDLER_H
#define GYROHANDLER_H

#include <project_xflight.h>
#include <statetypes.h>
#include <gyrosensor.h>
#include <ekfbias.h>

class GyroHandler {
public:
    GyroHandler();
    ~GyroHandler();

    // call on every sensor update
    void sensorUpdated()
    {
        GyroSensorGet(&sensor);
        if ((!isnan(sensor.x) && !isinf(sensor.x)) && (!isnan(sensor.y) && !isinf(sensor.y)) && (!isnan(sensor.z) && !isinf(sensor.z))) {
            PIOS_DEBUG_PinHigh(0);
            updated = true;
            gyro[0] = sensor.x;
            gyro[1] = sensor.y;
            gyro[2] = sensor.z;
            PIOS_DEBUG_PinLow(0);
        }
    }

    // export the updated state post state assessment and clear the sensor update flag
    void exportUAVO(float *bias)
    {
        if (updated) {
            EKFBiasData EKFBias;
            EKFBiasGet(&EKFBias);
            EKFBias.gyro_bias.X = bias[0];
            EKFBias.gyro_bias.Y = bias[1];
            EKFBias.gyro_bias.Z = bias[2];
            EKFBiasSet(&EKFBias);

            // clear the updated flag on cycle completion
            updated = false;
        }
    }

    bool isSet()
    {
        return updated;
    }

    float & operator[](int x)
    {
        return gyro[x];
    }

    uint32_t timestamp()
    {
        return sensor.timestamp;
    }

protected:
    GyroSensorData sensor;
    float gyro[3];
    bool updated;
};

#endif // ifndef GYROHANDLER_H
