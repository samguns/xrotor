/**
 ******************************************************************************
 * @file       maghandler.h
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Interface for mag handler
 *
 *****************************************************************************/
#ifndef MAGHANDLER_H
#define MAGHANDLER_H

#include <project_xflight.h>
#include <statetypes.h>
#include <magsensor.h>
#include <auxmagsensor.h>
#include <calibrationmags.h>
#include <revosettings.h>
#include <attitudestate.h>
#include <magstate.h>

#define MAGSTATUS_OK      1
#define MAGSTATUS_AUX     2
#define MAGSTATUS_INVALID 0

class MagHandler {
public:
    MagHandler();
    ~MagHandler();
    void initialise();
    void settingsUpdated();
    filterResult filter();

    // is set AND valid
    bool isSet()
    {
        return magIsSet && (magStatus != MAGSTATUS_INVALID);
    }

    float & operator[](int x)
    {
        return mag[x];
    }
    void exportUAVO()
    {
        // we don't check for validity...we want to see that in the UAVO inspector for debugging
        if (magIsSet) {
            MagStateData s;

            MagStateGet(&s);
            s.x = mag[0];
            s.y = mag[1];
            s.z = mag[2];
            switch (magStatus) {
            case MAGSTATUS_OK:
                s.Source = MAGSTATE_SOURCE_ONBOARD;
                break;
            case MAGSTATUS_AUX:
                s.Source = MAGSTATE_SOURCE_AUX;
                break;
            default:
                s.Source = MAGSTATE_SOURCE_INVALID; // should never happen
            }
            MagStateSet(&s);
        }
    }

    void clearSetFlag()
    {
        magIsSet = false;
    }

    void sensorBoardUpdated()
    {
        if (calibrationMags.Usage != CALIBRATIONMAGS_USAGE_EXTERNALONLY) {
            MagSensorGet(&board_sensor);
            if ((!isnan(board_sensor.calibrated.x) && !isinf(board_sensor.calibrated.x)) && (!isnan(board_sensor.calibrated.y) && !isinf(board_sensor.calibrated.y)) && (!isnan(board_sensor.calibrated.z) && !isinf(board_sensor.calibrated.z))) {
                boardMag[0]    = board_sensor.calibrated.x;
                boardMag[1]    = board_sensor.calibrated.y;
                boardMag[2]    = board_sensor.calibrated.z;
                boardMagError  = board_sensor.calibrated.error * 0.01f;
                sensorUpdated |= SENSORUPDATES_boardMag;
            }
        }
    }

    void sensorAuxUpdated()
    {
        if (calibrationMags.Usage != CALIBRATIONMAGS_USAGE_ONBOARDONLY) {
            AuxMagSensorGet(&aux_sensor);
            if ((!isnan(aux_sensor.calibrated.x) && !isinf(aux_sensor.calibrated.x)) && (!isnan(aux_sensor.calibrated.y) && !isinf(aux_sensor.calibrated.y)) && (!isnan(aux_sensor.calibrated.z) && !isinf(aux_sensor.calibrated.z))) {
                auxMag[0]      = aux_sensor.calibrated.x;
                auxMag[1]      = aux_sensor.calibrated.y;
                auxMag[2]      = aux_sensor.calibrated.z;
                auxMagError    = aux_sensor.calibrated.error * 0.01f;
                sensorUpdated |= SENSORUPDATES_auxMag;
            }
        }
    }

protected:

    float getMagError(float mag_arg[3])
    {
        // vector norm
        float magnitude = vector_lengthf(mag_arg, 3);
        // absolute value of relative error against Be
        float error     = fabsf(magnitude - magBe) * invMagBe;

        return error;
    }

    bool checkMagValidity(float error, bool setAlarms);

    MagSensorData board_sensor;
    AuxMagSensorData aux_sensor;
    CalibrationMagsData calibrationMags;
    RevoSettingsData revoSettings;
    float homeLocationBe[3];
    float magBe;
    float invMagBe;
    float auxMag[3];
    float auxMagError;
    float boardMag[3];
    float boardMagError;
    float Rxy;
    float Rot[3][3];
    float mag[3];
    uint32_t sensorUpdated;
    bool magIsSet;
    uint8_t warningcount;
    uint8_t errorcount;
    uint8_t auxMagStatus;
    uint8_t boardMagStatus;
    uint8_t magStatus;
};

#endif // ifndef MAGHANDLER_H
