/**
 ******************************************************************************
 * @file       gpsposhandler.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#ifndef GPSPOSHANDLER_H
#define GPSPOSHANDLER_H

#include <project_xflight.h>
#include <statetypes.h>
#include <gpssettings.h>
#include <gpspositionsensor.h>
#include <homelocation.h>
#include <CoordinateConversions.h>

class GPSPosHandler {
public:
    GPSPosHandler();
    ~GPSPosHandler();
    void init();
    void settingsUpdated();
    void sensorUpdated()
    {
        GPSPositionSensorGet(&gpsdata);
        PIOS_DEBUG_PinHigh(5);

        if (home.Set == HOMELOCATION_SET_TRUE) {
            // store it in a standard Cartesian position state
            // check if we have a valid GPS signal (not checked by StateEstimation istelf)
            if ((gpsdata.PDOP < settings.MaxPDOP) && (gpsdata.Satellites >= settings.MinSatellites) &&
                (gpsdata.Status == GPSPOSITIONSENSOR_STATUS_FIX3D) &&
                (gpsdata.Latitude != 0 || gpsdata.Longitude != 0)) {
                int32_t LLAi[3] = {
                    gpsdata.Latitude,
                    gpsdata.Longitude,
                    (int32_t)((gpsdata.Altitude + gpsdata.GeoidSeparation) * 1e4f),
                };
                LLA2Base(LLAi, HomeECEF, HomeRne, pos);
                positionUpdated = true;
            }
        }
        PIOS_DEBUG_PinLow(5);
    }

    void exportUAVO();

    bool isSet()
    {
        return positionUpdated;
    }
    float Down()
    {
        return pos[2];
    }
    float North()
    {
        return pos[0];
    }
    float East()
    {
        return pos[1];
    }

protected:
    GPSPositionSensorData gpsdata;
    GPSSettingsData settings;
    HomeLocationData home;
    double HomeECEF[3];
    float HomeRne[3][3];
    float pos[3];
    bool positionUpdated;
};

#endif // ifndef GPSPOSHANDLER_H
