/**
 ******************************************************************************
 * @file       accelhandler.h
 * @author     Alex Beck, OPNG. Copyright (C) 2015.
 * @brief      Accelerometer handler
 *
 *****************************************************************************/
#ifndef ACCELHANDLER_H
#define ACCELHANDLER_H

#include <project_xflight.h>
#include <statetypes.h>
#include <accelsensor.h>
#include <accelstate.h>

class AccelHandler {
public:
    AccelHandler();
    ~AccelHandler();

    // call on every sensor update
    void sensorUpdated()
    {
        AccelSensorGet(&sensor);
        if ((!isnan(sensor.x) && !isinf(sensor.x)) && (!isnan(sensor.y) && !isinf(sensor.y)) && (!isnan(sensor.z) && !isinf(sensor.z))) {
            PIOS_DEBUG_PinHigh(1);
            accel[0] = sensor.x;
            accel[1] = sensor.y;
            accel[2] = sensor.z;
            updated  = true;
            PIOS_DEBUG_PinLow(1);
        }
    }

    // export the updated state post state assessment and clear the sensor update flag
    void exportUAVO(float *accel_from_state)
    {
        if (updated) {
            AccelStateData s;
            AccelStateGet(&s);
            s.x     = accel_from_state[0];
            s.y     = accel_from_state[1];
            s.z     = accel_from_state[2];
            AccelStateSet(&s);
            updated = false;
        }
    }

    // export the updated state post state assessment and clear the sensor update flag
    void exportUAVO()
    {
        if (updated) {
            AccelStateData s;
            AccelStateGet(&s);
            s.x     = accel[0];
            s.y     = accel[1];
            s.z     = accel[2];
            AccelStateSet(&s);
            updated = false;
        }
    }


    bool isSet()
    {
        return updated;
    }

    float & operator[](int x)
    {
        return accel[x];
    }

protected:
    AccelSensorData sensor;
    float accel[3];
    bool updated;
};

#endif // ifndef ACCELHANDLER_H
