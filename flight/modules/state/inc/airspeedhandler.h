/**
 ******************************************************************************
 * @file       airspeedhandler.h
 * @author     Alex Beck, OPNG. Copyright (C) 2015.
 * @brief      Airspeed sensor handler
 *
 *****************************************************************************/
#ifndef AIRSPEEDHANDLER_H
#define AIRSPEEDHANDLER_H

#include <project_xflight.h>
#include <statetypes.h>
#include <airspeedsensor.h>
class BaroHandler;

class AirSpeedHandler {
public:
    AirSpeedHandler();
    ~AirSpeedHandler();

    void init(BaroHandler *baro_handler);

    void sensorUpdated();

    bool isSet()
    {
        return updated;
    }

    // airspeed[0] = s.CalibratedAirspeed;
    float calibratedAirSpeed()
    {
        return sensor.CalibratedAirspeed;
    }
    // airspeed[1] = s.TrueAirspeed;
    float trueAirSpeed()
    {
        return sensor.TrueAirspeed;
    }
    void exportUAVO();

protected:
    AirspeedSensorData sensor;
    BaroHandler *baroHandler;
    float altitude;
    bool updated;
};

#endif // ifndef AIRSPEEDHANDLER_H
