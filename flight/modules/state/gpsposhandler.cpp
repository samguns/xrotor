/**
 ******************************************************************************
 * @file       gpsposhandler.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#include "project_xflight.h"
#include <CoordinateConversions.h>

#include <homelocation.h>
#include <positionstate.h>
#include <gpssettings.h>
#include <gpspositionsensor.h>
#include "gpsposhandler.h"

GPSPosHandler::GPSPosHandler()
    : positionUpdated(false)
{
    pos[0]   = 0.0f;
    pos[1]   = 0.0f;
    pos[2]   = 0.0f;
    home.Set = HOMELOCATION_SET_FALSE;
}

GPSPosHandler::~GPSPosHandler()
{}

void GPSPosHandler::init()
{
    settingsUpdated();
}

void GPSPosHandler::settingsUpdated()
{
    GPSSettingsGet(&settings);
    HomeLocationGet(&home);
    if (home.Set == HOMELOCATION_SET_TRUE) {
        // calculate home location coordinate reference
        int32_t LLAi[3] = {
            home.Latitude,
            home.Longitude,
            (int32_t)(home.Altitude * 1e4f),
        };
        LLA2ECEF(LLAi, HomeECEF);
        RneFromLLA(LLAi, HomeRne);
    }
}


// TODO this is not the EKF Pos!!!!
void GPSPosHandler::exportUAVO()
{
    if (positionUpdated) {
        PositionStateData s;
        PositionStateGet(&s);
        s.North = pos[0];
        s.East  = pos[1];
        s.Down  = pos[2];
        PositionStateSet(&s);

        // clear the updated flag on cycle completion
        positionUpdated = false;
    }
}
