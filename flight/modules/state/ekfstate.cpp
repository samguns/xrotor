/**
 ******************************************************************************
 * @file       ekfstate.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Extended Kalman Filter.
 *
 ******************************************************************************/
#include "project_xflight.h"

#include <ekfconfiguration.h>
#include <ekfstatevariance.h>
#include <attitudestate.h>
#include <systemalarms.h>
#include <homelocation.h>
#include <positionstate.h>
#include <velocitystate.h>
#include <flightstatus.h>
#include <libmatrix/orientation.h>

#include <math.h>
#include <stdint.h>
#include <pios_math.h>
#include <mathmisc.h>

#include "ekfstate.h"
// state module headers
#include "barohandler.h"
#include "gpsposhandler.h"
#include "gpsvelhandler.h"
#include "accelhandler.h"
#include "gyrohandler.h"
#include "maghandler.h"
#include "altitudestate.h"
#include "airspeedhandler.h"


// Private constants

#define DT_ALPHA 1e-3f
#define DT_MIN   0.0f
#define DT_MAX   10000.0f
#define DT_INIT  (1.0f / PIOS_SILA_RATE) // initialize with board sensor rate

EKFState::EKFState()
{}
EKFState::~EKFState()
{}

void EKFState::init(BaroHandler *baro_handler, AirSpeedHandler *airspeed_handler, GPSPosHandler *gps_pos_handler,
                    GPSVelocityHandler *gps_vel_handler, AccelHandler *accel_handler,
                    MagHandler *mag_handler, GyroHandler *gyro_handler, AltitudeState *altitude_state)
{
    baroHandler        = baro_handler;
    airSpeedHandler    = airspeed_handler;
    gpsPositionHandler = gps_pos_handler;
    gpsVelocityHandler = gps_vel_handler;
    accelHandler       = accel_handler;
    magHandler = mag_handler;
    gyroHandler        = gyro_handler;
    altitudeState      = altitude_state;

    usePos     = 1;
    inited     = false;
    init_stage = 0;
    PIOS_DELTATIME_Init(&dtconfigPredict, DT_INIT, DT_MIN, DT_MAX, DT_ALPHA);
    PIOS_DELTATIME_Init(&dtconfigEstimate, DT_INIT, DT_MIN, DT_MAX, DT_ALPHA);

    settingsUpdated();
}

// check for invalid variance values
inline bool EKFState::invalid_var(float data)
{
    if (isnan(data) || isinf(data)) {
        return true;
    }
    if (data < 1e-15f) { // var should not be close to zero. And not negative either.
        return true;
    }
    return false;
}

SystemAlarmsExtendedAlarmStatusOptions EKFState::configCheck()
{
    EKFConfigurationGet(&ekfConfiguration);
    int t;
    // plausibility check
    for (t = 0; t < EKFCONFIGURATION_P_NUMELEM; t++) {
        if (invalid_var(EKFConfigurationPToArray(ekfConfiguration.P)[t])) {
            // return 2; // todo validity check state and alarm??
            return SYSTEMALARMS_EXTENDEDALARMSTATUS_EKFCONFIGURATIONBADVALUES;
        }
    }
    for (t = 0; t < EKFCONFIGURATION_Q_NUMELEM; t++) {
        if (invalid_var(EKFConfigurationQToArray(ekfConfiguration.Q)[t])) {
            return SYSTEMALARMS_EXTENDEDALARMSTATUS_EKFCONFIGURATIONBADVALUES;
        }
    }
    for (t = 0; t < EKFCONFIGURATION_R_NUMELEM; t++) {
        if (invalid_var(EKFConfigurationRToArray(ekfConfiguration.R)[t])) {
            return SYSTEMALARMS_EXTENDEDALARMSTATUS_EKFCONFIGURATIONBADVALUES;
        }
    }

    HomeLocationGet(&homeLocation);
    // Don't require HomeLocation.Set to be true but at least require a mag configuration (allows easily
    // switching between indoor and outdoor mode with Set = false)
    if ((homeLocation.Be[0] * homeLocation.Be[0] + homeLocation.Be[1] * homeLocation.Be[1] + homeLocation.Be[2] * homeLocation.Be[2] < 1e-5f)) {
        return SYSTEMALARMS_EXTENDEDALARMSTATUS_EKFCALIBRATIONREQUIRED;
    }
    return SYSTEMALARMS_EXTENDEDALARMSTATUS_NONE;
}

void EKFState::settingsUpdated()
{
    EKFConfigurationGet(&ekfConfiguration);
    HomeLocationGet(&homeLocation);
    FlightStatusArmedGet(&armed);
    if (armed) {
        ekf.Q[8] = 2e-4f; // gyro bias random walk variance (rad/s^2)^2
    } else {
        ekf.Q[8] = 2e-8f; // gyro bias random walk variance (rad/s^2)^2
    }

    INSSetMagNorth(homeLocation.Be);
    INSSetG(homeLocation.g_e);
}

void EKFState::setFilterOption(RevoSettingsFusionAlgorithmOptions fusion_option)
{
    fusionOption = fusion_option;
    switch (fusionOption) {
    case REVOSETTINGS_FUSIONALGORITHM_INS13INDOOR:
        baroHandler->disableGPS();
        usePos = 0;

        break;
    case REVOSETTINGS_FUSIONALGORITHM_GPSNAVIGATIONINS13:
        baroHandler->enableGPS();
        usePos = 1;

        break;
    default:
        break;
    }

    if (!usePos) {
        // position and velocity variance used in indoor mode
        float PosVar[3] = { ekfConfiguration.FakeR.FakeGPSPosIndoor,
                            ekfConfiguration.FakeR.FakeGPSPosIndoor,
                            ekfConfiguration.FakeR.FakeGPSPosIndoor };
        float VelVar[3] = { ekfConfiguration.FakeR.FakeGPSVelIndoor,
                            ekfConfiguration.FakeR.FakeGPSVelIndoor,
                            ekfConfiguration.FakeR.FakeGPSVelIndoor };

        INSSetPosVelVar(PosVar, VelVar);
    } else {
        // position and velocity variance used in outdoor mode
        float PosVar[3] = { ekfConfiguration.R.GPSPosNorth,
                            ekfConfiguration.R.GPSPosEast,
                            ekfConfiguration.R.GPSPosDown };
        float VelVar[3] = { ekfConfiguration.R.GPSVelNorth,
                            ekfConfiguration.R.GPSVelEast,
                            ekfConfiguration.R.GPSVelDown };
        INSSetPosVelVar(PosVar, VelVar);
    }
}

void EKFState::initEKF(float accel[3], float mag[3], float pos[3])
{
    INSGPSInit();

    // variance is measured in mGaus, but internally the EKF works with a normalized  vector. Scale down by Be^2
    float Be2 = homeLocation.Be[0] * homeLocation.Be[0] + homeLocation.Be[1] * homeLocation.Be[1] + homeLocation.Be[2] * homeLocation.Be[2];
    float scaled_mag_var[3] = { ekfConfiguration.R.MagX / Be2,
                                ekfConfiguration.R.MagY / Be2,
                                ekfConfiguration.R.MagZ / Be2 };
    INSSetMagVar(scaled_mag_var);

    float accel_var[3] = { ekfConfiguration.Q.AccelX,
                           ekfConfiguration.Q.AccelY,
                           ekfConfiguration.Q.AccelZ };
    INSSetAccelVar(accel_var);

    float gyro_var[3] = { ekfConfiguration.Q.GyroX,
                          ekfConfiguration.Q.GyroY,
                          ekfConfiguration.Q.GyroZ };
    INSSetGyroVar(gyro_var);

    float gyro_bias_var[3] = { ekfConfiguration.Q.GyroDriftX,
                               ekfConfiguration.Q.GyroDriftY,
                               ekfConfiguration.Q.GyroDriftZ };
    INSSetGyroBiasVar(gyro_bias_var);

    INSSetBaroVar(ekfConfiguration.R.BaroZ);

    INSSetMagNorth(homeLocation.Be);
    INSSetG(homeLocation.g_e);

    // Initialize the gyro bias
    float gyro_bias[3] = { 0.0f, 0.0f, 0.0f };
    INSSetGyroBias(gyro_bias);

    AttitudeStateData attitudeState;
    AttitudeStateGet(&attitudeState);

    // Set initial attitude. Use accels to determine roll and pitch, rotate magnetic measurement accordingly,
    // so pseudo "north" vector can be estimated even if the board is not level
    attitudeState.Roll = atan2f(-accel[1], -accel[2]);
    float zn  = cosf(attitudeState.Roll) * mag[2] + sinf(attitudeState.Roll) * mag[1];
    float yn  = cosf(attitudeState.Roll) * mag[1] - sinf(attitudeState.Roll) * mag[2];

    // rotate accels z vector according to roll
    float azn = cosf(attitudeState.Roll) * accel[2] + sinf(attitudeState.Roll) * accel[1];
    attitudeState.Pitch = atan2f(accel[0], -azn);

    float xn  = cosf(attitudeState.Pitch) * mag[0] + sinf(attitudeState.Pitch) * zn;

    attitudeState.Yaw = atan2f(-yn, xn);

    // TODO: This is still a hack
    // Put this in a proper generic function in CoordinateConversion.c
    // should take 4 vectors: g (0,0,-9.81), accels, Be (or 1,0,0 if no home loc) and magnetometers (or 1,0,0 if no mags)
    // should calculate the rotation in 3d space using proper cross product math
    // SUBTODO: formulate the math required

    attitudeState.Roll  = RAD2DEG(attitudeState.Roll);
    attitudeState.Pitch = RAD2DEG(attitudeState.Pitch);
    attitudeState.Yaw   = RAD2DEG(attitudeState.Yaw);

    RPY2Quaternion(&attitudeState.Roll, &NAV_Q_0);

    float zeros[3] = { 0.0f, 0.0f, 0.0f };
    INSSetState(pos, (float *)zeros, &NAV_Q_0, (float *)zeros, (float *)zeros);

    INSResetP(EKFConfigurationPToArray(ekfConfiguration.P));
}


inline filterResult EKFState::ins13filterPendingInit()
{
    float mag[3];
    float accel[3];
    float gyro_dps[3];

    // note that mag check isSet only returns true if valid
    if (!inited && magHandler->isSet() && baroHandler->initialisationCompleted() && (!usePos || (usePos && gpsPositionHandler->isSet()))) {
        accel[0]    = (*accelHandler)[0];
        accel[1]    = (*accelHandler)[1];
        accel[2]    = (*accelHandler)[2];

        gyro_dps[0] = (*gyroHandler)[0];
        gyro_dps[1] = (*gyroHandler)[1];
        gyro_dps[2] = (*gyroHandler)[2];

        float dT = PIOS_DELTATIME(&dtconfigPredict, gyroHandler->timestamp());
        PIOS_DELTATIME(&dtconfigEstimate, gyroHandler->timestamp());

        // Don't initialize until all sensors are read
        if (init_stage == 0) {
            mag[0] = (*magHandler)[0];
            mag[1] = (*magHandler)[1];
            mag[2] = (*magHandler)[2];
            float pos[3];
            pos[0] = 0.0f;
            pos[1] = 0.0f;
            pos[2] = 0.0f;
            if (usePos && gpsPositionHandler->isSet()) {
                pos[0] = gpsPositionHandler->North();
                pos[1] = gpsPositionHandler->East();
                pos[2] = gpsPositionHandler->Down();
            }
            // Reset the INS algorithm
            initEKF(accel, mag, pos);
        } else {
            // Run prediction a bit before any corrections
            float gyros[3] = { DEG2RAD(gyro_dps[0]), DEG2RAD(gyro_dps[1]), DEG2RAD(gyro_dps[2]) };
            INSStatePrediction(gyros, accel, dT);
        }

        init_stage++;
        if (init_stage > 10) {
            inited = true;
        }

        return FILTERRESULT_OK;
    }

    if (!inited) {
        return FILTERRESULT_CRITICAL;
    }
    return FILTERRESULT_OK;
}

// ekf prediction step
inline filterResult EKFState::ins13filter()
{
    float gyro_dps[3] = { 0.0f, 0.0f, 0.0f };
    float accel[3]    = { 0.0f, 0.0f, 0.0f };

    PIOS_DEBUG_PinHigh(4);

    // avail of these pre-checked
    accel[0]    = (*accelHandler)[0];
    accel[1]    = (*accelHandler)[1];
    accel[2]    = (*accelHandler)[2];
    gyro_dps[0] = (*gyroHandler)[0];
    gyro_dps[1] = (*gyroHandler)[1];
    gyro_dps[2] = (*gyroHandler)[2];

    // delta time from last prediction run to current gyro timestamp
    // float dT = PIOS_DELTATIME_GetActualSeconds(&dtconfigPredict);
    float dT = PIOS_DELTATIME(&dtconfigPredict, gyroHandler->timestamp());

    float gyros[3] = { DEG2RAD(gyro_dps[0]), DEG2RAD(gyro_dps[1]), DEG2RAD(gyro_dps[2]) };
    INSStatePrediction(gyros, accel, dT);

    // Assess if we have any measurement data
    uint8_t sensors    = 0;
    uint8_t sensor_index_from = SENSOR_INDEX_MAX;
    uint8_t sensor_index_to = SENSOR_INDEX_MIN;
    float mag[3]       = { 0.0f, 0.0f, 0.0f };
    float pos[3]       = { 0.0f, 0.0f, 0.0f };
    float vel[3]       = { 0.0f, 0.0f, 0.0f };
    float baroAltitude = 0.0f;

    EKFMagFusionOption magOption = MAG_FUSION_NONE; // todo test this defaulted to none to reduce calcs when not needed
    if (magHandler->isSet()) {
        mag[0] = (*magHandler)[0];
        mag[1] = (*magHandler)[1];
        mag[2] = (*magHandler)[2];
        if (ekfConfiguration.MagFusion == EKFCONFIGURATION_MAGFUSION_3D) {
            sensors  |= MAG_SENSORS;
            sensor_index_from = MIN(sensor_index_from, MAG_FROM);
            sensor_index_to = MAX(sensor_index_to, MAG_TO);
            magOption = MAG_FUSION_3D;
        } else {
            sensors  |= MAG_2D_SENSORS;
            sensor_index_from = MIN(sensor_index_from, MAG_2D_FROM);
            sensor_index_to = MAX(sensor_index_to, MAG_2D_TO);
            magOption = MAG_FUSION_2D;
        }
    }

    if (baroHandler->isSet()) {
        sensors |= BARO_SENSOR;
        baroAltitude      = baroHandler->altitude();
        sensor_index_from = MIN(sensor_index_from, BARO_Z);
        sensor_index_to   = MAX(sensor_index_to, BARO_Z);

        if (!usePos) {
            sensors |= POS_SENSORS;
            sensors |= HORIZ_SENSORS | VERT_SENSORS;
            sensor_index_from = POS_FROM; // MIN(sensor_index_from, POS_FROM);
            sensor_index_to   = MAX(sensor_index_to, POS_TO);
            sensor_index_from = MIN(sensor_index_from, VEL_FROM);
            sensor_index_to   = MAX(sensor_index_to, VEL_TO);
        }
    }

    if (usePos) {
        if (gpsPositionHandler->isSet()) {
            pos[0]   = gpsPositionHandler->North();
            pos[1]   = gpsPositionHandler->East();
            pos[2]   = gpsPositionHandler->Down();
            sensors |= POS_SENSORS;
            sensor_index_from = POS_FROM; // MIN(sensor_index_from, POS_FROM);
            sensor_index_to = MAX(sensor_index_to, POS_TO);
        }

        if (gpsVelocityHandler->isSet()) {
            vel[0]   = gpsVelocityHandler->North();
            vel[1]   = gpsVelocityHandler->East();
            vel[2]   = gpsVelocityHandler->Down();
            sensors |= HORIZ_SENSORS | VERT_SENSORS;
            sensor_index_from = MIN(sensor_index_from, VEL_FROM);
            sensor_index_to = MAX(sensor_index_to, VEL_TO);
        }
    }

    if (airSpeedHandler->isSet() && ((!IS_SET(sensors, HORIZ_SENSORS) && !IS_SET(sensors, POS_SENSORS)) | !usePos)) {
        sensors |= AIRSPEED_SENSORS;
        sensor_index_from = MIN(sensor_index_from, AIRSPEED_X);
        sensor_index_to   = MAX(sensor_index_to, AIRSPEED_X);
        // rotate airspeed vector into NED frame - airspeed is measured in X axis only
        float R[3][3];
        Quaternion2R(&NAV_Q_0, R);
        float vtas[3] = { airSpeedHandler->trueAirSpeed(), 0.0f, 0.0f };
        rot_mult(R, vtas, vel);
    }

    if (sensors) {
        // we need to catchup the right delta time since last covariance prediction
        float dTcov1 = PIOS_DELTATIME(&dtconfigEstimate, gyroHandler->timestamp());

        // Advance the covariance estimate
        CovariancePrediction(ekf.F, ekf.G, ekf.Q, dTcov1, ekf.P);
        EKFStateVarianceGet(&varianceData);
        INSGetP(EKFStateVariancePToArray(varianceData.P));
        varianceData._P.PositionNorth = dT;
        varianceData._P.PositionEast  = dTcov1;
        EKFStateVarianceSet(&varianceData);
        for (int t = 0; t < EKFSTATEVARIANCE_P_NUMELEM; t++) {
            if (!IS_REAL(EKFStateVariancePToArray(varianceData.P)[t]) || EKFStateVariancePToArray(varianceData.P)[t] <= 0.0f) {
                INSResetP(EKFConfigurationPToArray(ekfConfiguration.P));
                init_stage = -1;
                break;
            }
        }

        INSCorrection(mag, magOption, pos, vel, baroAltitude, sensors, sensor_index_from, sensor_index_to);
    }

    PIOS_DEBUG_PinLow(4);

    if (init_stage < 0) {
        init_stage = 0;
        return FILTERRESULT_WARNING;
    } else {
        return FILTERRESULT_OK;
    }
}

inline void EKFState::exportUAVO()
{
    float gyro_bias_dps[3] = { -RAD2DEG(NAV_GYRO_BIAS_X), -RAD2DEG(NAV_GYRO_BIAS_Y), -RAD2DEG(NAV_GYRO_BIAS_Z) };

    gyroHandler->exportUAVO(gyro_bias_dps); // this also clears the updated flag
    accelHandler->exportUAVO(); // this also clears the updated flag
    magHandler->clearSetFlag();
    baroHandler->clearSetFlag();

    // export the position; gps provides NE and altitude state provides down
    PositionStateData position;
    PositionStateGet(&position);
    position.North = NAV_POS_X;
    position.East  = NAV_POS_Y;
    position.Down  = NAV_POS_Z;
    PositionStateSet(&position);

    VelocityStateData velocity;
    VelocityStateGet(&velocity);
    velocity.North = NAV_VEL_X;
    velocity.East  = NAV_VEL_Y;
    velocity.Down  = NAV_VEL_Z;
    VelocityStateSet(&velocity);
    airSpeedHandler->exportUAVO();

    AttitudeStateData s;
    AttitudeStateGet(&s);
    s.q1 = NAV_Q_0;
    s.q2 = NAV_Q_1;
    s.q3 = NAV_Q_2;
    s.q4 = NAV_Q_3;
    Quaternion2RPY(&s.q1, &s.Roll);
    AttitudeStateSet(&s);
}

/**
 * Collect all required state variables, then run complementary filter
 */
filterResult EKFState::filter_ins13_indoor()
{
    filterResult result = FILTERRESULT_OK;

    if (gyroHandler->isSet()) {
        if (accelHandler->isSet()) {
            magHandler->filter();
            if (!inited) {
                result = ins13filterPendingInit();
            } else {
                result = ins13filter();
                exportUAVO();
            }
        }
    }
    return result;
}

filterResult EKFState::filter_ins13_gps()
{
    filterResult result = FILTERRESULT_OK;

    if (gyroHandler->isSet()) {
        if (accelHandler->isSet()) {
            magHandler->filter();
            if (!inited) {
                result = ins13filterPendingInit();
            } else {
                result = ins13filter();
                exportUAVO();
            }
        }
    }
    return result;
}


#if 0
// velocity filter

// Private constants

    #define DT_ALPHA 1e-3f
    #define DT_MIN   1e-6f
    #define DT_MAX   1.0f
    #define DT_INIT  (1.0f / PIOS_SILA_RATE) // initialize with board sensor rate


int32_t VelocityState::init()
{
    vel[0] = 0.0f;
    vel[1] = 0.0f;
    vel[2] = 0.0f;
    velocityBias[0] = 0.0f;
    velocityBias[1] = 0.0f;
    velocityBias[2] = 0.0f;
    oldPos[0] = 0.0f;
    oldPos[1] = 0.0f;
    oldPos[2] = 0.0f;
    inited    = 0;

    RevoSettingsVelocityPostProcessingLowPassAlphaGet(&alpha);

    PIOS_DELTATIME_Init(&dtconfig, DT_INIT, DT_MIN, DT_MAX, DT_ALPHA);
}

filterResult VelocityState::filter()
{
    if (IS_SET(state->updated, SENSORUPDATES_pos)) {
        float dT;
        dT = PIOS_DELTATIME_GetAverageSeconds(&dtconfig);

        // position updates allow us to calculate an ad-hoc velocity estimate
        // it will be very noisy and likely quite wrong at every given point in time,
        // but its long term average error should be quite low

        if (inited >= 1) { // only calculate velocity estimate if previous position is known
            vel[0] = (state->pos[0] - oldPos[0]) / dT;
            vel[1] = (state->pos[1] - oldPos[1]) / dT;
            vel[2] = (state->pos[2] - oldPos[2]) / dT;
            inited = 2;
        } else {
            // mark previous position as known
            inited = 1;
        }
        oldPos[0] = state->pos[0];
        oldPos[1] = state->pos[1];
        oldPos[2] = state->pos[2];
    }
    if (IS_SET(state->updated, SENSORUPDATES_vel)) {
        // if we have both a velocity estimate from filter and a velocity estimate from postion
        // we can calculate a bias estimate. It must be heavily low pass filtered to become useful
        // this assumes that the bias is relatively constant over time
        if (inited >= 2) { // only calculate bias if velocity estimate is calculated
            velocityBias[0] *= alpha;
            velocityBias[0] += (1.0f - alpha) * (vel[0] - state->vel[0]);
            velocityBias[1] *= alpha;
            velocityBias[1] += (1.0f - alpha) * (vel[1] - state->vel[1]);
            velocityBias[2] *= alpha;
            velocityBias[2] += (1.0f - alpha) * (vel[2] - state->vel[2]);
        }
        state->vel[0] += velocityBias[0];
        state->vel[1] += velocityBias[1];
        state->vel[2] += velocityBias[2];
    }
    return FILTERRESULT_OK;
}

#endif // if 0


// Private functions


// Private variables

// speed optimizations, describe matrix sparsity
// derived from state equations in
// LinearizeFG() and LinearizeH():
//
// usage F:        usage G:   usage H:
// -0123456789abc  012345678  0123456789abc
// 0...X.........  .........  X............
// 1....X........  .........  .X...........
// 2.....X.......  .........  ..X..........
// 3......XXXX...  ...XXX...  ...X.........
// 4......XXXX...  ...XXX...  ....X........
// 5......XXXX...  ...XXX...  .....X.......
// 6.....ooXXXXXX  XXX......  ......XXXX...
// 7.....oXoXXXXX  XXX......  ......XXXX...
// 8.....oXXoXXXX  XXX......  ......XXXX...
// 9.....oXXXoXXX  XXX......  ..X..........
// a.............  ......Xoo
// b.............  ......oXo
// c.............  ......ooX

static int8_t FrowMin[NUMX] = { 3, 4, 5, 6, 6, 6, 5, 5, 5, 5, 13, 13, 13 };
static int8_t FrowMax[NUMX] = { 3, 4, 5, 9, 9, 9, 12, 12, 12, 12, -1, -1, -1 };

static int8_t GrowMin[NUMX] = { 9, 9, 9, 3, 3, 3, 0, 0, 0, 0, 6, 7, 8 };
static int8_t GrowMax[NUMX] = { -1, -1, -1, 5, 5, 5, 2, 2, 2, 2, 6, 7, 8 };

static int8_t HrowMin[NUMV] = { 0, 1, 2, 3, 4, 5, 6, 6, 6, 2 };
static int8_t HrowMax[NUMV] = { 0, 1, 2, 3, 4, 5, 9, 9, 9, 2 };


// *************  Exposed Functions ****************
// *************************************************

uint16_t EKFState::ins_get_num_states()
{
    return NUMX;
}

void EKFState::INSGPSInit() // pretty much just a place holder for now
{
    ekf.Be[0] = 1.0f;
    ekf.Be[1] = 0.0f;
    ekf.Be[2] = 0.0f; // local magnetic unit vector

    for (int i = 0; i < NUMX; i++) {
        for (int j = 0; j < NUMX; j++) {
            ekf.P[i][j] = 0.0f; // zero all terms
            ekf.F[i][j] = 0.0f;
        }

        for (int j = 0; j < NUMW; j++) {
            ekf.G[i][j] = 0.0f;
        }

        for (int j = 0; j < NUMV; j++) {
            ekf.H[j][i] = 0.0f;
        }

        ekf.X[i] = 0.0f;
    }
    for (int i = 0; i < NUMW; i++) {
        ekf.Q[i] = 0.0f;
    }
    for (int i = 0; i < NUMV; i++) {
        ekf.R[i] = 0.0f;
    }


    ekf.P[0][0]   = ekf.P[1][1] = ekf.P[2][2] = 25.0f;            // initial position variance (m^2)
    ekf.P[3][3]   = ekf.P[4][4] = ekf.P[5][5] = 5.0f;             // initial velocity variance (m/s)^2
    ekf.P[6][6]   = ekf.P[7][7] = ekf.P[8][8] = ekf.P[9][9] = 1e-5f;  // initial quaternion variance
    ekf.P[10][10] = ekf.P[11][11] = ekf.P[12][12] = 1e-6f; // initial gyro bias variance (rad/s)^2

    ekf.X[0]      = ekf.X[1] = ekf.X[2] = ekf.X[3] = ekf.X[4] = ekf.X[5] = 0.0f; // initial pos and vel (m)
    ekf.X[6]      = 1.0f;
    ekf.X[7]      = ekf.X[8] = ekf.X[9] = 0.0f;      // initial quaternion (level and North) (m/s)
    ekf.X[10]     = ekf.X[11] = ekf.X[12] = 0.0f; // initial gyro bias (rad/s)

    ekf.Q[0]      = ekf.Q[1] = ekf.Q[2] = 1e-5f;        // gyro noise variance (rad/s)^2
    ekf.Q[3]      = ekf.Q[4] = ekf.Q[5] = 1e-5f;      // accelerometer noise variance (m/s^2)^2
    ekf.Q[6]      = ekf.Q[7] = 1e-6f;     // gyro bias random walk variance (rad/s^2)^2
    ekf.Q[8]      = 1e-6f;     // gyro bias random walk variance (rad/s^2)^2

    ekf.R[POS_X]  = ekf.R[POS_Y] = 0.004f;   // High freq GPS horizontal position noise variance (m^2)
    ekf.R[POS_Z]  = 0.036f;          // High freq GPS vertical position noise variance (m^2)
    ekf.R[VEL_X]  = ekf.R[VEL_Y] = 0.004f;   // High freq GPS horizontal velocity noise variance (m/s)^2
    ekf.R[VEL_Z]  = 100.0f;          // High freq GPS vertical velocity noise variance (m/s)^2
    ekf.R[MAG_X]  = ekf.R[MAG_Y] = ekf.R[MAG_Z] = 0.005f;    // magnetometer unit vector noise variance
    ekf.R[BARO_Z] = .25f; // High freq altimeter noise variance (m^2)
}

void EKFState::INSResetP(float PDiag[NUMX])
{
    uint8_t i, j;

    // if PDiag[i] nonzero then clear row and column and set diagonal element
    for (i = 0; i < NUMX; i++) {
        if (PDiag != 0) {
            for (j = 0; j < NUMX; j++) {
                ekf.P[i][j] = ekf.P[j][i] = 0.0f;
            }
            ekf.P[i][i] = PDiag[i];
        }
    }
}

void EKFState::INSGetP(float PDiag[NUMX])
{
    uint8_t i;

    // retrieve diagonal elements (aka state variance)
    if (PDiag != 0) {
        for (i = 0; i < NUMX; i++) {
            PDiag[i] = ekf.P[i][i];
        }
    }
}

void EKFState::INSSetState(float pos[3], float vel[3], float q[4], float gyro_bias[3], __attribute__((unused)) float accel_bias[3])
{
    /* Note: accel_bias not used in 13 state INS */
    ekf.X[0]  = pos[0];
    ekf.X[1]  = pos[1];
    ekf.X[2]  = pos[2];
    ekf.X[3]  = vel[0];
    ekf.X[4]  = vel[1];
    ekf.X[5]  = vel[2];
    ekf.X[6]  = q[0];
    ekf.X[7]  = q[1];
    ekf.X[8]  = q[2];
    ekf.X[9]  = q[3];
    ekf.X[10] = gyro_bias[0];
    ekf.X[11] = gyro_bias[1];
    ekf.X[12] = gyro_bias[2];
}

void EKFState::INSPosVelReset(float pos[3], float vel[3])
{
    for (int i = 0; i < 6; i++) {
        for (int j = i; j < NUMX; j++) {
            ekf.P[i][j] = 0; // zero the first 6 rows and columns
            ekf.P[j][i] = 0;
        }
    }

    ekf.P[0][0] = ekf.P[1][1] = ekf.P[2][2] = 25; // initial position variance (m^2)
    ekf.P[3][3] = ekf.P[4][4] = ekf.P[5][5] = 5; // initial velocity variance (m/s)^2

    ekf.X[0]    = pos[0];
    ekf.X[1]    = pos[1];
    ekf.X[2]    = pos[2];
    ekf.X[3]    = vel[0];
    ekf.X[4]    = vel[1];
    ekf.X[5]    = vel[2];
}

void EKFState::INSSetPosVelVar(float PosVar[3], float VelVar[3])
{
    ekf.R[0] = PosVar[0];
    ekf.R[1] = PosVar[1];
    ekf.R[2] = PosVar[2];
    ekf.R[3] = VelVar[0];
    ekf.R[4] = VelVar[1];
    ekf.R[5] = VelVar[2];
}

void EKFState::INSSetGyroBias(float gyro_bias[3])
{
    ekf.X[10] = gyro_bias[0];
    ekf.X[11] = gyro_bias[1];
    ekf.X[12] = gyro_bias[2];
}

void EKFState::INSSetAccelVar(float accel_var[3])
{
    ekf.Q[3] = accel_var[0];
    ekf.Q[4] = accel_var[1];
    ekf.Q[5] = accel_var[2];
}

void EKFState::INSSetGyroVar(float gyro_var[3])
{
    ekf.Q[0] = gyro_var[0];
    ekf.Q[1] = gyro_var[1];
    ekf.Q[2] = gyro_var[2];
}

void EKFState::INSSetGyroBiasVar(float gyro_bias_var[3])
{
    ekf.Q[6] = gyro_bias_var[0];
    ekf.Q[7] = gyro_bias_var[1];
    ekf.Q[8] = gyro_bias_var[2];
}

void EKFState::INSSetMagVar(float scaled_mag_var[3])
{
    ekf.R[6] = scaled_mag_var[0];
    ekf.R[7] = scaled_mag_var[1];
    ekf.R[8] = scaled_mag_var[2];
}

void EKFState::INSSetBaroVar(float baro_var)
{
    ekf.R[9] = baro_var;
}

void EKFState::INSSetMagNorth(float B[3])
{
    float mag = sqrtf(B[0] * B[0] + B[1] * B[1] + B[2] * B[2]);

    ekf.Be[0] = B[0] / mag;
    ekf.Be[1] = B[1] / mag;
    ekf.Be[2] = B[2] / mag;
}

void EKFState::INSSetG(float g_e)
{
    ekf.g_e = g_e;
}


void EKFState::INSStatePrediction(float gyro_data[3], float accel_data[3], float dT)
{
    float U[6];
    float invqmag;

    // rate gyro inputs in units of rad/s
    U[0] = gyro_data[0];
    U[1] = gyro_data[1];
    U[2] = gyro_data[2];

    // accelerometer inputs in units of m/s
    U[3] = accel_data[0];
    U[4] = accel_data[1];
    U[5] = accel_data[2];

    // EKF prediction step
    LinearizeFG(ekf.X, U, ekf.F, ekf.G);
    // RungeKutta2ndOrder(ekf.X, U, ekf.g_e, dT);
    RungeKutta(ekf.X, U, ekf.g_e, dT);
    invqmag   = fast_invsqrtf(ekf.X[6] * ekf.X[6] + ekf.X[7] * ekf.X[7] + ekf.X[8] * ekf.X[8] + ekf.X[9] * ekf.X[9]);
    ekf.X[6] *= invqmag;
    ekf.X[7] *= invqmag;
    ekf.X[8] *= invqmag;
    ekf.X[9] *= invqmag;
}


void EKFState::INSCorrection(float mag_data[3], const EKFMagFusionOption magOption, float Pos[3], float Vel[3],
                             float BaroAlt, uint16_t SensorsUsed, uint8_t sensor_index_from, uint8_t sensor_index_to)
{
    float Z[10] = { 0 };
    float Y[10] = { 0 };

    // GPS Position in meters and in local NED frame
    Z[POS_X]  = Pos[0];
    Z[POS_Y]  = Pos[1];
    Z[POS_Z]  = Pos[2];

    // GPS Velocity in meters and in local NED frame
    Z[VEL_X]  = Vel[0];
    Z[VEL_Y]  = Vel[1];
    Z[VEL_Z]  = Vel[2];

    // barometric altimeter in meters and in local NED frame
    Z[BARO_Z] = BaroAlt;

    // magnetometer data in any units (use unit vector) and in body frame
    if (magOption == MAG_FUSION_3D) {
        float invBmag = fast_invsqrtf(mag_data[0] * mag_data[0] + mag_data[1] * mag_data[1] + mag_data[2] * mag_data[2]);
        Z[MAG_X] = mag_data[0] * invBmag;
        Z[MAG_Y] = mag_data[1] * invBmag;
        Z[MAG_Z] = mag_data[2] * invBmag;
    } else if (magOption == MAG_FUSION_2D) {
        {
            // magnetometer data in any units (use unit vector) and in body frame
            float Rbe_a[3][3];
            float q0 = ekf.X[6];
            float q1 = ekf.X[7];
            float q2 = ekf.X[8];
            float q3 = ekf.X[9];
            float k1 = 1.0f / sqrtf(powf(q0 * q1 * 2.0f + q2 * q3 * 2.0f, 2.0f) + powf(q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3, 2.0f));
            float k2 = sqrtf(-powf(q0 * q2 * 2.0f - q1 * q3 * 2.0f, 2.0f) + 1.0f);

            Rbe_a[0][0] = k2;
            Rbe_a[0][1] = 0.0f;
            Rbe_a[0][2] = q0 * q2 * -2.0f + q1 * q3 * 2.0f;
            Rbe_a[1][0] = k1 * (q0 * q1 * 2.0f + q2 * q3 * 2.0f) * (q0 * q2 * 2.0f - q1 * q3 * 2.0f);
            Rbe_a[1][1] = k1 * (q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3);
            Rbe_a[1][2] = k1 * sqrtf(-powf(q0 * q2 * 2.0f - q1 * q3 * 2.0f, 2.0f) + 1.0f) * (q0 * q1 * 2.0f + q2 * q3 * 2.0f);
            Rbe_a[2][0] = k1 * (q0 * q2 * 2.0f - q1 * q3 * 2.0f) * (q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3);
            Rbe_a[2][1] = -k1 * (q0 * q1 * 2.0f + q2 * q3 * 2.0f);
            Rbe_a[2][2] = k1 * k2 * (q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3);

            Z[MAG_X]    = Rbe_a[0][0] * mag_data[0] + Rbe_a[1][0] * mag_data[1] + Rbe_a[2][0] * mag_data[2];
            Z[MAG_Y]    = Rbe_a[0][1] * mag_data[0] + Rbe_a[1][1] * mag_data[1] + Rbe_a[2][1] * mag_data[2];
            Z[MAG_Z]    = Rbe_a[0][2] * mag_data[0] + Rbe_a[1][2] * mag_data[1] + Rbe_a[2][2] * mag_data[2];
        }
    } else {
        // none
        Z[MAG_X] = 100.0f;
        Z[MAG_Y] = 0.0f;
        Z[MAG_Z] = 0.0f;
    }

    LinearizeH(ekf.X, ekf.Be, magOption, ekf.H);
    MeasurementEq(ekf.X, ekf.Be, magOption, Y);
    SerialUpdate(ekf.H, ekf.R, Z, Y, ekf.P, ekf.X, SensorsUsed, sensor_index_from, sensor_index_to);

    // TODO Should we export Y? i.e MagState is the EKF value for mag. Pos, V, q, yro bias already captured. And not calculated for gyro and accel.

    float invqmag = fast_invsqrtf(ekf.X[6] * ekf.X[6] + ekf.X[7] * ekf.X[7] + ekf.X[8] * ekf.X[8] + ekf.X[9] * ekf.X[9]);
    ekf.X[6] *= invqmag;
    ekf.X[7] *= invqmag;
    ekf.X[8] *= invqmag;
    ekf.X[9] *= invqmag;

    // Make sure no gyro bias gets to more than 5 deg / s. This should be more than
    // enough for well behaving sensors.
    const float GYRO_BIAS_LIMIT = DEG2RAD(5.0f);
    for (int i = 10; i < 13; i++) {
        if (ekf.X[i] < -GYRO_BIAS_LIMIT) {
            ekf.X[i] = -GYRO_BIAS_LIMIT;
        } else if (ekf.X[i] > GYRO_BIAS_LIMIT) {
            ekf.X[i] = GYRO_BIAS_LIMIT;
        }
    }
}

// *************  CovariancePrediction *************
// Does the prediction step of the Kalman filter for the covariance matrix
// Output, Pnew, overwrites P, the input covariance
// Pnew = (I+F*T)*P*(I+F*T)' + T^2*G*Q*G'
// Q is the discrete time covariance of process noise
// Q is vector of the diagonal for a square matrix with
// dimensions equal to the number of disturbance noise variables
// The General Method is very inefficient,not taking advantage of the sparse F and G
// The first Method is very specific to this implementation
// ************************************************

void EKFState::CovariancePrediction(const float F[NUMX][NUMX], const float G[NUMX][NUMW],
                                    const float Q[NUMW], float dT, float P[NUMX][NUMX])
{
    // Pnew = (I+F*T)*P*(I+F*T)' + (T^2)*G*Q*G' = (T^2)[(P/T + F*P)*(I/T + F') + G*Q*G')]

    const float dT1  = 1.0f / dT; // multiplication is faster than division on fpu.
    const float dTsq = dT * dT;

    float Dummy[NUMX][NUMX];
    int8_t i;
    int8_t k;

    for (i = 0; i < NUMX; i++) { // Calculate Dummy = (P/T +F*P)
        const float *Firow   = F[i];
        float *Pirow = P[i];
        float *Dirow = Dummy[i];
        const int8_t Fistart = FrowMin[i];
        const int8_t Fiend   = FrowMax[i];
        int8_t j;

        for (j = 0; j < NUMX; j++) {
            Dirow[j] = Pirow[j] * dT1; // Dummy = P / T ...
        }
        for (k = Fistart; k <= Fiend; k++) {
            for (j = 0; j < NUMX; j++) {
                Dirow[j] += Firow[k] * P[k][j]; // [] + F * P
            }
        }
    }
    for (i = 0; i < NUMX; i++) { // Calculate Pnew = (T^2) [Dummy/T + Dummy*F' + G*Qw*G']
        float *Dirow = Dummy[i];
        const float *Girow = G[i];
        float *Pirow = P[i];
        const int8_t Gistart = GrowMin[i];
        const int8_t Giend   = GrowMax[i];
        int8_t j;


        for (j = i; j < NUMX; j++) { // Use symmetry, ie only find upper triangular
            float Ptmp         = Dirow[j] * dT1; // Pnew = Dummy / T ...

            const float *Fjrow = F[j];
            int8_t Fjstart     = FrowMin[j];
            int8_t Fjend       = FrowMax[j];
            k = Fjstart;

            while (k <= Fjend - 3) {
                Ptmp += Dirow[k] * Fjrow[k]; // [] + Dummy*F' ...
                Ptmp += Dirow[k + 1] * Fjrow[k + 1];
                Ptmp += Dirow[k + 2] * Fjrow[k + 2];
                Ptmp += Dirow[k + 3] * Fjrow[k + 3];
                k    += 4;
            }
            while (k <= Fjend) {
                Ptmp += Dirow[k] * Fjrow[k];
                k++;
            }

            const float *Gjrow   = G[j];
            const int8_t Gjstart = MAX(Gistart, GrowMin[j]);
            const int8_t Gjend   = MIN(Giend, GrowMax[j]);
            k = Gjstart;
            while (k <= Gjend - 2) {
                Ptmp += Q[k] * Girow[k] * Gjrow[k]; // [] + G*Q*G' ...
                Ptmp += Q[k + 1] * Girow[k + 1] * Gjrow[k + 1];
                Ptmp += Q[k + 2] * Girow[k + 2] * Gjrow[k + 2];
                k    += 3;
            }
            if (k <= Gjend) {
                Ptmp += Q[k] * Girow[k] * Gjrow[k];
                if (k <= Gjend - 1) {
                    Ptmp += Q[k + 1] * Girow[k + 1] * Gjrow[k + 1];
                }
            }

            P[j][i] = Pirow[j] = Ptmp * dTsq; // [] * (T^2)
        }
    }
}

// *************  SerialUpdate *******************
// Does the update step of the Kalman filter for the covariance and estimate
// Outputs are Xnew & Pnew, and are written over P and X
// Z is actual measurement, Y is predicted measurement
// Xnew = X + K*(Z-Y), Pnew=(I-K*H)*P,
// where K=P*H'*inv[H*P*H'+R]
// NOTE the algorithm assumes R (measurement covariance matrix) is diagonal
// i.e. the measurment noises are uncorrelated.
// It therefore uses a serial update that requires no matrix inversion by
// processing the measurements one at a time.
// Algorithm - see Grewal and Andrews, "Kalman Filtering,2nd Ed" p.121 & p.253
// - or see Simon, "Optimal State Estimation," 1st Ed, p.150
// The SensorsUsed variable is a bitwise mask indicating which sensors
// should be used in the update.
// ************************************************
void EKFState::SerialUpdate(const float H[NUMV][NUMX], const float R[NUMV], const float Z[NUMV],
                            const float Y[NUMV], float P[NUMX][NUMX], float X[NUMX],
                            const uint16_t SensorsUsed, uint8_t sensor_index_from, uint8_t sensor_index_to)
{
    float HP[NUMX], HPHR, Error;
    uint8_t i, j, k, m;
    float Km[NUMX];

    PIOS_DEBUG_Assert(sensor_index_to < NUMV);

    // Iterate through all the possible measurements and apply the
    // appropriate corrections
    for (m = sensor_index_from; m < (sensor_index_to + 1); m++) {
        if (SensorsUsed & (0x01 << m)) { // use this sensor for update
            for (j = 0; j < NUMX; j++) { // Find Hp = H*P
                HP[j] = 0.0f;
            }

            for (k = HrowMin[m]; k <= HrowMax[m]; k++) {
                for (j = 0; j < NUMX; j++) { // Find Hp = H*P
                    HP[j] += H[m][k] * P[k][j];
                }
            }
            HPHR = R[m]; // Find  HPHR = H*P*H' + R
            for (k = HrowMin[m]; k <= HrowMax[m]; k++) {
                HPHR += HP[k] * H[m][k];
            }
            float invHPHR = 1.0f / HPHR;
            for (k = 0; k < NUMX; k++) {
                Km[k] = HP[k] * invHPHR; // find K = HP/HPHR
            }
            for (i = 0; i < NUMX; i++) { // Find P(m)= P(m-1) + K*HP
                for (j = i; j < NUMX; j++) {
                    P[i][j] = P[j][i] = P[i][j] - Km[i] * HP[j];
                }
            }

            Error = Z[m] - Y[m];
            for (i = 0; i < NUMX; i++) { // Find X(m)= X(m-1) + K*Error
                X[i] = X[i] + Km[i] * Error;
            }
        }
    }

    // Make sure no gyro bias gets to more than 5 deg / s. This should be more than
    // enough for well behaving sensors.
    const float GYRO_BIAS_LIMIT = DEG2RAD(5.0f);
    for (int idum = 10; idum < 13; idum++) {
        if (X[idum] < -GYRO_BIAS_LIMIT) {
            X[idum] = -GYRO_BIAS_LIMIT;
        } else if (X[idum] > GYRO_BIAS_LIMIT) {
            X[idum] = GYRO_BIAS_LIMIT;
        }
    }
}

// *************  RungeKutta **********************
// Does a 4th order Runge Kutta numerical integration step
// Output, Xnew, is written over X
// NOTE the algorithm assumes time invariant state equations and
// constant inputs over integration step
// ************************************************

void EKFState::RungeKutta(float X[NUMX], const float U[NUMU], const float g_e, float dT)
{
    const float dT2 = dT * 0.5f;
    float K1[NUMX], K2[NUMX], K3[NUMX], K4[NUMX], Xlast[NUMX];
    uint8_t i;

    for (i = 0; i < NUMX; i++) {
        Xlast[i] = X[i]; // make a working copy
    }

    StateEq(X, U, g_e, K1); // k1 = f(x,u)
    for (i = 0; i < NUMX; i++) {
        X[i] = Xlast[i] + dT2 * K1[i];
    }

    StateEq(X, U, g_e, K2); // k2 = f(x+0.5*dT*k1,u)
    for (i = 0; i < NUMX; i++) {
        X[i] = Xlast[i] + dT2 * K2[i];
    }

    StateEq(X, U, g_e, K3); // k3 = f(x+0.5*dT*k2,u)
    for (i = 0; i < NUMX; i++) {
        X[i] = Xlast[i] + dT * K3[i];
    }


    StateEq(X, U, g_e, K4); // k4 = f(x+dT*k3,u)

    for (i = 0; i < NUMX; i++) {
        X[i] = Xlast[i] + (K1[i] + 2.0f * K2[i] + 2.0f * K3[i] + K4[i]) * (dT / 6.0f);
    }
}


void EKFState::RungeKutta2ndOrder(float X[NUMX], const float U[NUMU], const float g_e, float dT)
{
    const float dT2 = dT * 0.5f;
    float K1[NUMX], K2[NUMX], Xlast[NUMX];
    uint8_t i;

    for (i = 0; i < NUMX; i++) {
        Xlast[i] = X[i]; // make a working copy
    }

    StateEq(X, U, g_e, K1); // k1 = f(x,u)

    for (i = 0; i < NUMX; i++) {
        X[i] = Xlast[i] + dT * K1[i];
    }

    StateEq(X, U, g_e, K2); // k2 = f(x+dT*k1,u)

    for (i = 0; i < NUMX; i++) {
        X[i] = Xlast[i] + (K1[i] + K2[i]) * dT2;
    }
}


// *************  Model Specific Stuff  ***************************
// ***  StateEq, MeasurementEq, LinerizeFG, and LinearizeH ********
//
// State Variables = [Pos Vel Quaternion GyroBias NO-AccelBias]
// Deterministic Inputs = [AngularVel Accel]
// Disturbance Noise = [GyroNoise AccelNoise GyroRandomWalkNoise NO-AccelRandomWalkNoise]
//
// Measurement Variables = [Pos Vel BodyFrameMagField Altimeter]
// Inputs to Measurement = [EarthFrameMagField]
//
// Notes: Pos and Vel in earth frame
// AngularVel and Accel in body frame
// MagFields are unit vectors
// Xdot is output of StateEq()
// F and G are outputs of LinearizeFG(), all elements not set should be zero
// y is output of OutputEq()
// H is output of LinearizeH(), all elements not set should be zero
// ************************************************

void EKFState::StateEq(const float X[NUMX], const float U[NUMU], const float g_e, float Xdot[NUMX])
{
    // ax=U[3]-X[13]; ay=U[4]-X[14]; az=U[5]-X[15];  // subtract the biases on accels
    const float ax = U[3];
    const float ay = U[4];
    const float az = U[5]; // NO BIAS STATES ON ACCELS
    const float wx = U[0] - X[10];
    const float wy = U[1] - X[11];
    const float wz = U[2] - X[12]; // subtract the biases on gyros
    const float q0 = X[6];
    const float q1 = X[7];
    const float q2 = X[8];
    const float q3 = X[9];

    // Pdot = V
    Xdot[0] = X[3];
    Xdot[1] = X[4];
    Xdot[2] = X[5];

    const float q0q0 = q0 * q0;
    const float q1q1 = q1 * q1;
    const float q2q2 = q2 * q2;
    const float q3q3 = q3 * q3;
    const float q1q3 = q1 * q3;

    // Vdot = Reb*a
    Xdot[3]  = (q0q0 + q1q1 - q2q2 - q3q3) * ax + 2.0f * (q1 * q2 - q0 * q3) * ay + 2.0f * (q1q3 + q0 * q2) * az;
    Xdot[4]  = 2.0f * (q1 * q2 + q0 * q3) * ax + (q0q0 - q1q1 + q2q2 - q3q3) * ay + 2 * (q2 * q3 - q0 * q1) * az;
    Xdot[5]  = 2.0f * (q1q3 - q0 * q2) * ax + 2 * (q2 * q3 + q0 * q1) * ay + (q0q0 - q1q1 - q2q2 + q3q3) * az + g_e;

    // qdot = Q*w
    Xdot[6]  = (-q1 * wx - q2 * wy - q3 * wz) * 0.5f;
    Xdot[7]  = (q0 * wx - q3 * wy + q2 * wz) * 0.5f;
    Xdot[8]  = (q3 * wx + q0 * wy - q1 * wz) * 0.5f;
    Xdot[9]  = (-q2 * wx + q1 * wy + q0 * wz) * 0.5f;

    // best guess is that bias stays constant
    Xdot[10] = Xdot[11] = Xdot[12] = 0;
}

/**
 * Linearize the state equations around the current state estimate.
 * @param[in] X the current state estimate
 * @param[in] U the control inputs
 * @param[out] F the linearized natural dynamics
 * @param[out] G the linearized influence of disturbance model
 *
 * so the prediction of the next state is
 *   Xdot = F * X + G * U
 * where X is the current state and U is the current input
 *
 * For reference the state order (in F) is pos, vel, attitude, gyro bias, accel bias
 * and the input order is gyro, bias
 */
void EKFState::LinearizeFG(const float X[NUMX], const float U[NUMU], float F[NUMX][NUMX], float G[NUMX][NUMW])
{
    // ax=U[3]-X[13]; ay=U[4]-X[14]; az=U[5]-X[15];  // subtract the biases on accels
    const float ax = U[3];
    const float ay = U[4];
    const float az = U[5]; // NO BIAS STATES ON ACCELS
    const float wx = U[0] - X[10];
    const float wy = U[1] - X[11];
    const float wz = U[2] - X[12]; // subtract the biases on gyros
    const float q0 = X[6];
    const float q1 = X[7];
    const float q2 = X[8];
    const float q3 = X[9];

    // Pdot = V
    F[0][3] = F[1][4] = F[2][5] = 1.0f;

    // dVdot/dq
    F[3][6] = 2.0f * (q0 * ax - q3 * ay + q2 * az);
    F[3][7] = 2.0f * (q1 * ax + q2 * ay + q3 * az);
    F[3][8] = 2.0f * (-q2 * ax + q1 * ay + q0 * az);
    F[3][9] = 2.0f * (-q3 * ax - q0 * ay + q1 * az);
    F[4][6] = 2.0f * (q3 * ax + q0 * ay - q1 * az);
    F[4][7] = 2.0f * (q2 * ax - q1 * ay - q0 * az);
    F[4][8] = 2.0f * (q1 * ax + q2 * ay + q3 * az);
    F[4][9] = 2.0f * (q0 * ax - q3 * ay + q2 * az);
    F[5][6] = 2.0f * (-q2 * ax + q1 * ay + q0 * az);
    F[5][7] = 2.0f * (q3 * ax + q0 * ay - q1 * az);
    F[5][8] = 2.0f * (-q0 * ax + q3 * ay - q2 * az);
    F[5][9] = 2.0f * (q1 * ax + q2 * ay + q3 * az);

    // dVdot/dabias & dVdot/dna - the equations for how the accel input and accel bias influence velocity are the same
    // dVdot/dabias & dVdot/dna  - NO BIAS STATES ON ACCELS - S0 REPEAT FOR G BELOW
    // F[3][13]=G[3][3]=-q0*q0-q1*q1+q2*q2+q3*q3; F[3][14]=G[3][4]=2*(-q1*q2+q0*q3);         F[3][15]=G[3][5]=-2*(q1*q3+q0*q2);
    // F[4][13]=G[4][3]=-2*(q1*q2+q0*q3);         F[4][14]=G[4][4]=-q0*q0+q1*q1-q2*q2+q3*q3; F[4][15]=G[4][5]=2*(-q2*q3+q0*q1);
    // F[5][13]=G[5][3]=2*(-q1*q3+q0*q2);         F[5][14]=G[5][4]=-2*(q2*q3+q0*q1);         F[5][15]=G[5][5]=-q0*q0+q1*q1+q2*q2-q3*q3;

    // dqdot/dq
    F[6][6]  = 0;
    F[6][7]  = -wx * 0.5f;
    F[6][8]  = -wy * 0.5f;
    F[6][9]  = -wz * 0.5f;
    F[7][6]  = wx * 0.5f;
    F[7][7]  = 0;
    F[7][8]  = wz * 0.5f;
    F[7][9]  = -wy * 0.5f;
    F[8][6]  = wy * 0.5f;
    F[8][7]  = -wz * 0.5f;
    F[8][8]  = 0;
    F[8][9]  = wx * 0.5f;
    F[9][6]  = wz * 0.5f;
    F[9][7]  = wy * 0.5f;
    F[9][8]  = -wx * 0.5f;
    F[9][9]  = 0;

    // dqdot/dwbias
    F[6][10] = q1 * 0.5f;
    F[6][11] = q2 * 0.5f;
    F[6][12] = q3 * 0.5f;
    F[7][10] = -q0 * 0.5f;
    F[7][11] = q3 * 0.5f;
    F[7][12] = -q2 * 0.5f;
    F[8][10] = -q3 * 0.5f;
    F[8][11] = -q0 * 0.5f;
    F[8][12] = q1 * 0.5f;
    F[9][10] = q2 * 0.5f;
    F[9][11] = -q1 * 0.5f;
    F[9][12] = -q0 * 0.5f;

    // dVdot/dna  - NO BIAS STATES ON ACCELS - S0 REPEAT FOR G HERE
    G[3][3]  = -q0 * q0 - q1 * q1 + q2 * q2 + q3 * q3;
    G[3][4]  = 2.0f * (-q1 * q2 + q0 * q3);
    G[3][5]  = -2.0f * (q1 * q3 + q0 * q2);
    G[4][3]  = -2.0f * (q1 * q2 + q0 * q3);
    G[4][4]  = -q0 * q0 + q1 * q1 - q2 * q2 + q3 * q3;
    G[4][5]  = 2.0f * (-q2 * q3 + q0 * q1);
    G[5][3]  = 2.0f * (-q1 * q3 + q0 * q2);
    G[5][4]  = -2.0f * (q2 * q3 + q0 * q1);
    G[5][5]  = -q0 * q0 + q1 * q1 + q2 * q2 - q3 * q3;

    // dqdot/dnw
    G[6][0]  = q1 * 0.5f;
    G[6][1]  = q2 * 0.5f;
    G[6][2]  = q3 * 0.5f;
    G[7][0]  = -q0 * 0.5f;
    G[7][1]  = q3 * 0.5f;
    G[7][2]  = -q2 * 0.5f;
    G[8][0]  = -q3 * 0.5f;
    G[8][1]  = -q0 * 0.5f;
    G[8][2]  = q1 * 0.5f;
    G[9][0]  = q2 * 0.5f;
    G[9][1]  = -q1 * 0.5f;
    G[9][2]  = -q0 * 0.5f;

    // dwbias = random walk noise
    G[10][6] = G[11][7] = G[12][8] = 1.0f;
    // dabias = random walk noise
    // G[13][9]=G[14][10]=G[15][11]=1;  // NO BIAS STATES ON ACCELS
}

// Predicts the measurements from the current state. Note
// that this is very similar to @ref LinearizeH except this
// directly computes the outputs instead of a matrix that
// you transform the state by
void EKFState::MeasurementEq(const float X[NUMX], const float Be[3], const EKFMagFusionOption option, float Y[NUMV])
{
    const float q0 = X[6];
    const float q1 = X[7];
    const float q2 = X[8];
    const float q3 = X[9];

    // first six outputs are P and V
    Y[0] = X[0];
    Y[1] = X[1];
    Y[2] = X[2];
    Y[3] = X[3];
    Y[4] = X[4];
    Y[5] = X[5];

    switch (option) {
    case MAG_FUSION_NONE:
    case MAG_FUSION_3D:
    {
        float q0q0 = q0 * q0;
        float q1q1 = q1 * q1;
        float q2q2 = q2 * q2;
        float q3q3 = q3 * q3;
        float q1q3 = q1 * q3;

        // Bb=Rbe*Be
        Y[6] = (q0q0 + q1q1 - q2q2 - q3q3) * Be[0] + 2.0f * (q1 * q2 + q0 * q3) * Be[1] + 2.0f * (q1q3 - q0 * q2) * Be[2];
        Y[7] = 2.0f * (q1 * q2 - q0 * q3) * Be[0] + (q0q0 - q1q1 + q2q2 - q3q3) * Be[1] + 2.0f * (q2 * q3 + q0 * q1) * Be[2];
        Y[8] = 2.0f * (q1q3 + q0 * q2) * Be[0] + 2.0f * (q2 * q3 - q0 * q1) * Be[1] + (q0q0 - q1q1 - q2q2 + q3q3) * Be[2];
    }
    break;
    case MAG_FUSION_2D:
    {
        // Rotate Be by only the yaw heading
        float a1 = 2 * q0 * q3 + 2 * q1 * q2;
        float a2 = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3;
        float r  = sqrtf(a1 * a1 + a2 * a2);
        float cP = a2 / r;
        float sP = a1 / r;
        Y[6] = Be[0] * cP + Be[1] * sP;
        Y[7] = -Be[0] * sP + Be[1] * cP;
        Y[8] = 0; // don't care
    }
    break;
    }

    // Alt = -Pz
    Y[9] = -1.0f * X[2];
}

// Linearize the measurement around the current state prediction
// so the predicted measurements are
// Z = H * X
void EKFState::LinearizeH(const float X[NUMX], const float Be[3], const EKFMagFusionOption option, float H[NUMV][NUMX])
{
    const float q0 = X[6];
    const float q1 = X[7];
    const float q2 = X[8];
    const float q3 = X[9];

    // dP/dP=I; (expect position to measure the position)
    H[0][0] = H[1][1] = H[2][2] = 1.0f;
    // dV/dV=I; (expect velocity to measure the velocity)
    H[3][3] = H[4][4] = H[5][5] = 1.0f;

    switch (option) {
    case MAG_FUSION_NONE:
    case MAG_FUSION_3D:
    {
#ifdef NON_OPTIMISED

        // dBb/dq
        H[6][6] = 2.0f * (q0 * Be[0] + q3 * Be[1] - q2 * Be[2]);
        H[6][7] = 2.0f * (q1 * Be[0] + q2 * Be[1] + q3 * Be[2]);
        H[6][8] = 2.0f * (-q2 * Be[0] + q1 * Be[1] - q0 * Be[2]);
        H[6][9] = 2.0f * (-q3 * Be[0] + q0 * Be[1] + q1 * Be[2]);
        H[7][6] = 2.0f * (-q3 * Be[0] + q0 * Be[1] + q1 * Be[2]);
        H[7][7] = 2.0f * (q2 * Be[0] - q1 * Be[1] + q0 * Be[2]);
        H[7][8] = 2.0f * (q1 * Be[0] + q2 * Be[1] + q3 * Be[2]);
        H[7][9] = 2.0f * (-q0 * Be[0] - q3 * Be[1] + q2 * Be[2]);
        H[8][6] = 2.0f * (q2 * Be[0] - q1 * Be[1] + q0 * Be[2]);
        H[8][7] = 2.0f * (q3 * Be[0] - q0 * Be[1] - q1 * Be[2]);
        H[8][8] = 2.0f * (q0 * Be[0] + q3 * Be[1] - q2 * Be[2]);
        H[8][9] = 2.0f * (q1 * Be[0] + q2 * Be[1] + q3 * Be[2]);

#else
        float q0Be0 = q0 * Be[0];
        float q1Be0 = q1 * Be[0];
        float q2Be0 = q2 * Be[0];
        float q3Be0 = q3 * Be[0];
        float q0Be1 = q0 * Be[1];
        float q1Be1 = q1 * Be[1];
        float q2Be1 = q2 * Be[1];
        float q3Be1 = q3 * Be[1];
        float q0Be2 = q0 * Be[2];
        float q1Be2 = q1 * Be[2];
        float q2Be2 = q2 * Be[2];
        float q3Be2 = q3 * Be[2];
        // dBb/dq
        H[6][6] = 2.0f * (q0Be0 + q3Be1 - q2Be2);
        H[6][7] = 2.0f * (q1Be0 + q2Be1 + q3Be2);
        H[6][8] = 2.0f * (-q2Be0 + q1Be1 - q0Be2);
        H[6][9] = 2.0f * (-q3Be0 + q0Be1 + q1Be2);

        H[7][6] = H[6][9]; // 2.0f * (-q3Be0 + q0Be1 + q1Be2);
        H[7][7] = 2.0f * (q2Be0 - q1Be1 + q0Be2);
        H[7][8] = H[6][7]; // 2.0f * (q1Be0 + q2Be1 + q3Be2);
        H[7][9] = 2.0f * (-q0Be0 - q3Be1 + q2Be2);

        H[8][6] = H[7][7]; // 2.0f * (q2Be0 - q1Be1 + q0Be2);
        H[8][7] = 2.0f * (q3Be0 - q0Be1 - q1Be2);
        H[8][8] = H[6][6]; // 2.0f * (q0Be0 + q3Be1 - q2Be2);
        H[8][9] = H[7][8]; // 2.0f * (q1Be0 + q2Be1 + q3Be2);
#endif // ifdef NON_OPTIMISED
    }
    break;
    case MAG_FUSION_2D:
    {
        // dBb/dq    (expected magnetometer readings in the horizontal plane)
        // these equations were generated by Rhb(q)*Be which is the matrix that
        // rotates the earth magnetic field into the horizontal plane, and then
        // taking the partial derivative wrt each term in q.
        float Be_0 = Be[0];
        float Be_1 = Be[1];
        float a1   = q0 * q3 * 2.0f + q1 * q2 * 2.0f;
        float a1s  = a1 * a1;
        float a2   = q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3;
        float a2s  = a2 * a2;
        float a3   = 1.0f / powf(a1s + a2s, 3.0f / 2.0f) * (1.0f / 2.0f);

        float k1   = 1.0f / sqrtf(a1s + a2s);
        float k3   = a3 * a2;
        float k4   = a2 * 4.0f;
        float k5   = a1 * 4.0f;
        float k6   = a3 * a1;

        H[6][6] = Be_0 * q0 * k1 * 2.0f + Be_1 * q3 * k1 * 2.0f - Be_0 * (q0 * k4 + q3 * k5) * k3 - Be_1 * (q0 * k4 + q3 * k5) * k6;
        H[6][7] = Be_0 * q1 * k1 * 2.0f + Be_1 * q2 * k1 * 2.0f - Be_0 * (q1 * k4 + q2 * k5) * k3 - Be_1 * (q1 * k4 + q2 * k5) * k6;
        H[6][8] = Be_0 * q2 * k1 * -2.0f + Be_1 * q1 * k1 * 2.0f + Be_0 * (q2 * k4 - q1 * k5) * k3 + Be_1 * (q2 * k4 - q1 * k5) * k6;
        H[6][9] = Be_1 * q0 * k1 * 2.0f - Be_0 * q3 * k1 * 2.0f + Be_0 * (q3 * k4 - q0 * k5) * k3 + Be_1 * (q3 * k4 - q0 * k5) * k6;

        H[7][6] = Be_1 * q0 * k1 * 2.0f - Be_0 * q3 * k1 * 2.0f - Be_1 * (q0 * k4 + q3 * k5) * k3 + Be_0 * (q0 * k4 + q3 * k5) * k6;
        H[7][7] = Be_0 * q2 * k1 * -2.0f + Be_1 * q1 * k1 * 2.0f - Be_1 * (q1 * k4 + q2 * k5) * k3 + Be_0 * (q1 * k4 + q2 * k5) * k6;
        H[7][8] = Be_0 * q1 * k1 * -2.0f - Be_1 * q2 * k1 * 2.0f + Be_1 * (q2 * k4 - q1 * k5) * k3 - Be_0 * (q2 * k4 - q1 * k5) * k6;
        H[7][9] = Be_0 * q0 * k1 * -2.0f - Be_1 * q3 * k1 * 2.0f + Be_1 * (q3 * k4 - q0 * k5) * k3 - Be_0 * (q3 * k4 - q0 * k5) * k6;

        H[8][6] = 0.0f;
        H[8][7] = 0.0f;
        H[8][8] = 0.0f;
        H[8][9] = 0.0f;
    }
    break;
    }


    // dAlt/dPz = -1
    H[9][2] = -1.0f;
}

/**
 * @}
 * @}
 */
