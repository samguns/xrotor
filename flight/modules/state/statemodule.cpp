/**
 ******************************************************************************
 * @file       statemodule.cpp
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      module integration
 *
 ******************************************************************************/
#include <project_xflight.h>
#include <callbackinfo.h>

#include <gyrosensor.h>
#include <accelsensor.h>
#include <magsensor.h>
#include <barosensor.h>
#include <airspeedsensor.h>
#include <gpspositionsensor.h>
#include <gpsvelocitysensor.h>
#include <auxmagsensor.h>

#include <ekfbias.h>
#include <gyrostate.h>
#include <accelstate.h>
#include <magstate.h>
#include <airspeedstate.h>
#include <attitudestate.h>
#include <positionstate.h>
#include <velocitystate.h>

#include "flightstatus.h"

#include "CoordinateConversions.h"
#include "libuavoutils/settingutils.h"
#include <mpugyroaccelsettings.h>
#include <altitudefiltersettings.h>

#include <calibrationmags.h>
#include <revosettings.h>
#include <homelocation.h>
#include <ekfstatevariance.h>
#include <gpssettings.h>
#include "sanitycheck.h"
#include "ekfconfiguration.h"

#include "sensorstate.h"

// Private constants
#define STACK_SIZE_BYTES  2048
#define CALLBACK_PRIORITY CALLBACK_PRIORITY_REGULAR
#define TASK_PRIORITY     CALLBACK_TASK_FLIGHTCONTROL
#define TIMEOUT_MS        10

// Private variables
static DelayedCallbackInfo *stateEstimationCallback;
static SensorState *sensorState;

// Private functions
static uint8_t gyro_divider = 1;

static SystemAlarmsExtendedAlarmStatusOptions StateModuleConfigHook();
static void EKFConfigurationUpdatedCb(UAVObjEvent *objEv);
static void CalibrationMagsUpdatedCb(UAVObjEvent *objEv);
static void RevoSettingsUpdatedCb(UAVObjEvent *objEv);
static void MPUGyroAccelSettingsUpdatedCb(UAVObjEvent *objEv);
static void HomeLocationUpdatedCb(UAVObjEvent *objEv);
static void StateEstimationCb(void);
static void accelSensorUpdatedCb(UAVObjEvent *ev);
static void magSensorUpdatedCb(UAVObjEvent *ev);
static void auxMagSensorUpdatedCb(UAVObjEvent *ev);
static void gpsPositionSensorUpdatedCb(UAVObjEvent *ev);
static void baroSensorUpdatedCb(UAVObjEvent *ev);
static void airspeedSensorUpdatedCb(UAVObjEvent *ev);
static void gyroSensorUpdatedCb(UAVObjEvent *ev);
static void gpsVelocitySensorUpdatedCb(__attribute__((unused))  UAVObjEvent * ev);
static void altitudeFilterSettingsUpdatedCb(__attribute__((unused))  UAVObjEvent * ev);
static void flightStatusUpdatedCb(__attribute__((unused))  UAVObjEvent * ev);
static void gpsSettingsCb(__attribute__((unused))  UAVObjEvent * ev);

extern "C" {
int32_t stateInitialize(void);
int32_t stateStart(void);
}


/**
 * Initialise the module.  Called before the start function
 * \returns 0 on success or -1 if initialisation failed
 */
int32_t stateInitialize(void)
{
    RevoSettingsInitialize();
    GyroSensorInitialize();
    MagSensorInitialize();
    AuxMagSensorInitialize();
    BaroSensorInitialize();
    AirspeedSensorInitialize();
    GPSVelocitySensorInitialize();
    GPSPositionSensorInitialize();
    HomeLocationInitialize();
    EKFBiasInitialize();
    GyroStateInitialize();
    AccelStateInitialize();
    MagStateInitialize();
    AirspeedStateInitialize();
    PositionStateInitialize();
    VelocityStateInitialize();
    AttitudeStateInitialize();
    HomeLocationInitialize();
    AttitudeStateInitialize();
    AltitudeFilterSettingsInitialize();
    FlightStatusInitialize();
    HomeLocationInitialize();
    CalibrationMagsInitialize();
    EKFConfigurationInitialize();
    EKFStateVarianceInitialize();
    GPSSettingsInitialize();

    sensorState = new SensorState();
    PIOS_Assert(sensorState);

    EKFConfigurationConnectCallback(&EKFConfigurationUpdatedCb);
    CalibrationMagsConnectCallback(&CalibrationMagsUpdatedCb);
    RevoSettingsConnectCallback(&RevoSettingsUpdatedCb);
    MPUGyroAccelSettingsConnectCallback(&MPUGyroAccelSettingsUpdatedCb);
    HomeLocationConnectCallback(&HomeLocationUpdatedCb);
    GyroSensorConnectCallback(&gyroSensorUpdatedCb);
    AccelSensorConnectCallback(&accelSensorUpdatedCb);
    MagSensorConnectCallback(&magSensorUpdatedCb);
    BaroSensorConnectCallback(&baroSensorUpdatedCb);
    AirspeedSensorConnectCallback(&airspeedSensorUpdatedCb);
    AuxMagSensorConnectCallback(&auxMagSensorUpdatedCb);
    GPSVelocitySensorConnectCallback(&gpsVelocitySensorUpdatedCb);
    GPSPositionSensorConnectCallback(&gpsPositionSensorUpdatedCb);
    AltitudeFilterSettingsConnectCallback(&altitudeFilterSettingsUpdatedCb);
    FlightStatusConnectCallback(&flightStatusUpdatedCb);
    GPSSettingsConnectCallback(&gpsSettingsCb);

    SANITYCHECK_AttachHook(&StateModuleConfigHook);

    stateEstimationCallback = PIOS_CALLBACKSCHEDULER_Create(&StateEstimationCb, CALLBACK_PRIORITY, TASK_PRIORITY, CALLBACKINFO_RUNNING_STATEESTIMATION, STACK_SIZE_BYTES);

    return STATUS_OK;
}

/**
 * Start the task.  Expects all objects to be initialized by this point.
 * \returns 0 on success or -1 if initialisation failed
 */
int32_t stateStart(void)
{
    sensorState->init();
    return STATUS_OK;
}

MODULE_INITCALL(stateInitialize, stateStart);


/**
 * Module callback
 */
static void StateEstimationCb(void)
{
    sensorState->updateState();
}


static SystemAlarmsExtendedAlarmStatusOptions StateModuleConfigHook()
{
    return sensorState->ekfState.configCheck();
}


/**
 * Callbacks
 */
static void EKFConfigurationUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    sensorState->settingsUpdated();
}
static void CalibrationMagsUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    sensorState->magHandler.settingsUpdated();
}
static void gpsSettingsCb(__attribute__((unused)) UAVObjEvent *ev)
{
    sensorState->gpsPosHandler.settingsUpdated();
}
static void altitudeFilterSettingsUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    sensorState->altitudeState.settingsUpdated();
}
static void RevoSettingsUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    sensorState->magHandler.settingsUpdated();
    sensorState->baroHandler.settingsUpdated();
    sensorState->settingsUpdated();
}
static void MPUGyroAccelSettingsUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    uint32_t stateEstimationRate;

    gyro_divider = getStateEstimationRate(&stateEstimationRate);
}
static void HomeLocationUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    sensorState->gpsPosHandler.settingsUpdated();
    sensorState->altitudeState.settingsUpdated();
    sensorState->settingsUpdated();
}
static void flightStatusUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    sensorState->cfState.settingsUpdated();
    sensorState->ekfState.settingsUpdated();
}
static void accelSensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->accelHandler.sensorUpdated();
}
static void magSensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->magHandler.sensorBoardUpdated();
}
static void auxMagSensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->magHandler.sensorAuxUpdated();
}
static void gpsPositionSensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->gpsPosHandler.sensorUpdated();
}
static void baroSensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->baroHandler.sensorUpdated();
}
static void airspeedSensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->airSpeedHandler.sensorUpdated();
}
static void gyroSensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->gyroHandler.sensorUpdated();
    static uint8_t gyro_counter = 0;

    if ((gyro_counter++ % gyro_divider) == 0) {
        PIOS_CALLBACKSCHEDULER_Dispatch(stateEstimationCallback);
    }
}
static void gpsVelocitySensorUpdatedCb(__attribute__((unused))  UAVObjEvent *ev)
{
    sensorState->gpsVelHandler.sensorUpdated();
}

/**
 * @}
 * @}
 */
