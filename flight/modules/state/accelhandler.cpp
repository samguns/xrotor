/**
 ******************************************************************************
 * @file       accelhandler.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#include "project_xflight.h"
#include <statetypes.h>
#include "accelsensor.h"
#include "accelstate.h"
#include "accelhandler.h"

AccelHandler::AccelHandler()
    : updated(false)
{
    accel[0] = 0.0f;
    accel[1] = 0.0f;
    accel[2] = 0.0f;
}

AccelHandler::~AccelHandler()
{}
