/**
 ******************************************************************************
 * @file       airspeedhandler.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#include <project_xflight.h>
#include "pios_math.h"
#include "airspeedsensor.h"
#include "airspeedstate.h"
#include "airspeedhandler.h"
#include "barohandler.h"

AirSpeedHandler::AirSpeedHandler()
    : baroHandler(0)
{}

AirSpeedHandler::~AirSpeedHandler()
{}

void AirSpeedHandler::init(BaroHandler *baro_handler)
{
    baroHandler = baro_handler;
    altitude    = 0.0f;
}

// simple IAS to TAS aproximation - 2% increase per 1000ft
// since we do not have flowing air temperature information
#define IAS2TAS(alt) (1.0f + (0.02f * (alt) / 304.8f))


void AirSpeedHandler::sensorUpdated()
{
    AirspeedSensorGet(&sensor);
    if ((!isnan(sensor.CalibratedAirspeed) && !isinf(sensor.CalibratedAirspeed)) &&
        (!isnan(sensor.TrueAirspeed) && !isinf(sensor.TrueAirspeed)) && sensor.SensorConnected == AIRSPEEDSENSOR_SENSORCONNECTED_TRUE) {
        // airspeed[0] = s.CalibratedAirspeed;
        // airspeed[1] = s.TrueAirspeed;
        updated = true;

        // take static pressure altitude estimation for
        if (baroHandler->isSet()) {
            altitude = baroHandler->altitude();
        }

        // calculate true airspeed estimation
        if (sensor.TrueAirspeed < 0.f) {
            sensor.TrueAirspeed = sensor.CalibratedAirspeed * IAS2TAS(altitude);
        }
    }
}

void AirSpeedHandler::exportUAVO()
{
    if (updated) {
        AirspeedStateData s;
        AirspeedStateGet(&s);
        s.CalibratedAirspeed = sensor.CalibratedAirspeed;
        s.TrueAirspeed = sensor.TrueAirspeed;
        AirspeedStateSet(&s);
        updated = false;
    }
}
