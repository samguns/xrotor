/**
 ******************************************************************************
 * @file       maghandler.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Mag sensor handler implementation
 *
 *****************************************************************************/
#include "project_xflight.h"
#include "maghandler.h"

#include "magsensor.h"
#include "magstate.h"
#include "auxmagsensor.h"
#include "homelocation.h"
#include "calibrationmags.h"
#include "revosettings.h"

MagHandler::MagHandler()
    : magBe(0.0f), invMagBe(0.0f), Rxy(0.0f), sensorUpdated(0), magIsSet(false), warningcount(0), errorcount(0), magStatus(MAGSTATUS_INVALID)
{}

MagHandler::~MagHandler()
{}

void MagHandler::initialise()
{
    warningcount = errorcount = 0;
    magIsSet     = false;
    magStatus    = MAGSTATUS_INVALID;
    settingsUpdated();
}


void MagHandler::settingsUpdated(void)
{
    HomeLocationBeGet(homeLocationBe);
    CalibrationMagsGet(&calibrationMags);
    RevoSettingsGet(&revoSettings);

    // magBe holds the magnetic vector length (expected)
    magBe    = vector_lengthf(this->homeLocationBe, 3);
    invMagBe = 1.0f / magBe;
    Rxy = sqrtf(homeLocationBe[0] * homeLocationBe[0] + homeLocationBe[1] * homeLocationBe[1]);
}

/**
 * check validity of magnetometers
 */
inline bool MagHandler::checkMagValidity(float error, bool setAlarms)
{
    #define ALARM_THRESHOLD 5

    // set errors
    if (error < revoSettings.MagnetometerMaxDeviation.Warning) { // TODO Review these deviation values
        warningcount = 0;
        errorcount   = 0;
        AlarmsClear(SYSTEMALARMS_ALARM_MAGNETOMETER);
        return true;
    }

    if (error < revoSettings.MagnetometerMaxDeviation.Error) {
        errorcount = 0;
        if (warningcount > ALARM_THRESHOLD) {
            if (setAlarms) {
                AlarmsSet(SYSTEMALARMS_ALARM_MAGNETOMETER, SYSTEMALARMS_ALARM_WARNING);
            }
            return false;
        } else {
            warningcount++;
            return true;
        }
    }

    if (errorcount > ALARM_THRESHOLD) {
        if (setAlarms) {
            AlarmsSet(SYSTEMALARMS_ALARM_MAGNETOMETER, SYSTEMALARMS_ALARM_CRITICAL);
        }
        return false;
    } else {
        errorcount++;
    }
    // still in "grace period"
    return true;
}


filterResult MagHandler::filter()
{
    if (sensorUpdated != 0) {
        PIOS_DEBUG_PinHigh(2);

        // Uses the external mag when available
        if (IS_SET(sensorUpdated, SENSORUPDATES_auxMag)) {
            auxMagStatus = checkMagValidity(auxMagError, true);
        }

        if (IS_SET(sensorUpdated, SENSORUPDATES_boardMag)) {
            boardMagStatus = checkMagValidity(boardMagError, true);
        }


        switch (calibrationMags.Usage) {
        case CALIBRATIONMAGS_USAGE_EXTERNALONLY:
            if (IS_SET(sensorUpdated, SENSORUPDATES_auxMag)) {
                mag[0]   = auxMag[0];
                mag[1]   = auxMag[1];
                mag[2]   = auxMag[2];
                magIsSet = true;
                if (auxMagStatus) {
                    magStatus = MAGSTATUS_AUX;
                } else {
                    magStatus = MAGSTATUS_INVALID;
                }
            }
            break;

        case CALIBRATIONMAGS_USAGE_ONBOARDONLY:
            if (IS_SET(sensorUpdated, SENSORUPDATES_boardMag)) {
                mag[0]   = boardMag[0];
                mag[1]   = boardMag[1];
                mag[2]   = boardMag[2];
                magIsSet = true;
                if (boardMagStatus) {
                    magStatus = MAGSTATUS_OK;
                } else {
                    magStatus = MAGSTATUS_INVALID;
                }
            }
            break;

        case CALIBRATIONMAGS_USAGE_BOTH:
            // prefer aux mag if its valid
            if (IS_SET(sensorUpdated, SENSORUPDATES_auxMag) && auxMagStatus) {
                mag[0]    = auxMag[0];
                mag[1]    = auxMag[1];
                mag[2]    = auxMag[2];
                magStatus = MAGSTATUS_AUX;
                magIsSet  = true;

                // otherwise fallback to board if valid
            } else if (IS_SET(sensorUpdated, SENSORUPDATES_boardMag) && boardMagStatus) {
                mag[0]    = boardMag[0];
                mag[1]    = boardMag[1];
                mag[2]    = boardMag[2];
                magStatus = MAGSTATUS_OK;
                magIsSet  = true;

                // otherwise nothing valid but report an invalid result
            } else if (IS_SET(sensorUpdated, SENSORUPDATES_auxMag)) {
                mag[0]    = auxMag[0];
                mag[1]    = auxMag[1];
                mag[2]    = auxMag[2];
                magIsSet  = true;
                magStatus = MAGSTATUS_INVALID;
            } else if (IS_SET(sensorUpdated, SENSORUPDATES_boardMag)) {
                mag[0]    = boardMag[0];
                mag[1]    = boardMag[1];
                mag[2]    = boardMag[2];
                magIsSet  = true;
                magStatus = MAGSTATUS_INVALID;
            }
            break;
        }

        exportUAVO();

        PIOS_DEBUG_PinLow(2);

        // clear now we have processed
        sensorUpdated = 0;
    }

    return FILTERRESULT_OK;
}
