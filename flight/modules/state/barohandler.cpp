/**
 ******************************************************************************
 * @file       barohandler.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#include "project_xflight.h"
#include <statetypes.h>
#include <revosettings.h>
#include "barohandler.h"
#include "gpsposhandler.h"


BaroHandler::BaroHandler()
    : baroOffset(0.0f), baroGPSOffsetCorrectionAlpha(0.0f), baroAlt(0.0f), gpsAlt(0.0f), first_run(INIT_CYCLES), useGPS(false), updated(false), gpsPositionHandler(0)
{}

BaroHandler::~BaroHandler()
{}


void BaroHandler::enableGPS()
{
    useGPS = 1;
}

void BaroHandler::disableGPS()
{
    useGPS = 0;
}

void BaroHandler::init(GPSPosHandler *gps_pos_handler)
{
    gpsPositionHandler = gps_pos_handler;
    useGPS = 0;
    if (gpsPositionHandler) {
        useGPS = 1;
    }

    baroOffset = 0.0f;
    gpsAlt     = 0.0f;
    first_run  = INIT_CYCLES;

    settingsUpdated();
}

void BaroHandler::settingsUpdated()
{
    RevoSettingsBaroGPSOffsetCorrectionAlphaGet(&baroGPSOffsetCorrectionAlpha);
}
