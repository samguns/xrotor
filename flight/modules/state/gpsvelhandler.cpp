/**
 ******************************************************************************
 * @file       gpsvelhandler.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Interface class for sensor state
 *
 *****************************************************************************/
#include "project_xflight.h"
#include "gpsvelhandler.h"

GPSVelocityHandler::GPSVelocityHandler()
    : updated(false)
{}

GPSVelocityHandler::~GPSVelocityHandler()
{}
