/**
 ******************************************************************************
 *
 * @file       unittests.cpp
 * @author     Alex Beck  Copyright (C) 2015.
 * @brief      UnitTests module
 *
 *****************************************************************************/

#include "project_xflight.h"
#include "ut_control.h"
#include "ut_status.h"
#include "ut_libmatrix.h"

static UT_StatusData utStatus;

static void UT_ControlUpdated(__attribute__((unused)) UAVObjEvent * ev);
static void utRunSingle(UT_ControlRunSingleOptions selection);

int32_t UnitTestsInitialize(void);
int32_t UnitTestsStart(void);

int32_t UnitTestsStart(void)
{
    // PIOS_CALLBACKSCHEDULER_Dispatch(unittestsTimerTaskCBInfo);

    UT_ControlData utControl;

    utControl.Operation = UT_CONTROL_OPERATION_RUNSINGLE;
    utControl.RunSingle = UT_CONTROL_RUNSINGLE_LIBMATRIX;
    utRunSingle(utControl.RunSingle);

    return STATUS_OK;
}

int32_t UnitTestsInitialize(void)
{
    UT_ControlInitialize();
    UT_StatusInitialize();

    UT_ControlConnectCallback(&UT_ControlUpdated);

    return STATUS_OK;
}

MODULE_INITCALL(UnitTestsInitialize, UnitTestsStart);

static void utRunSingle(UT_ControlRunSingleOptions selection)
{
    switch (selection) {
    case UT_CONTROL_RUNSINGLE_NONE:
        break;
    case UT_CONTROL_RUNSINGLE_LIBMATRIX:
        // call a framework that
        ut_libmatrix(utStatus);
        break;
    }
}

static void UT_ControlUpdated(__attribute__((unused)) UAVObjEvent *ev)
{
    memset(&utStatus, 0, sizeof(UT_StatusData));

    UT_ControlData utControl;
    UT_ControlGet(&utControl);
    switch (utControl.Operation) {
    case UT_CONTROL_OPERATION_NONE:
        break;

    case UT_CONTROL_OPERATION_RUNSINGLE:
        utStatus.Operation = UT_STATUS_OPERATION_RUNSINGLE;
        utStatus.OperationState = UT_STATUS_OPERATIONSTATE_IDLE;
        utRunSingle(utControl.RunSingle);

        break;
    default:
        break;
    }
}
