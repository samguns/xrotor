/**
 ******************************************************************************
 *
 * @file       ut_libmatrix.cpp
 * @author     Alex Beck  Copyright (C) 2015.
 * @brief      UnitTests module for libmatrix
 *
 *****************************************************************************/

#include "project_xflight.h"
#include "ut_libmatrix.h"
#include "unittests_helper.h"

// library methods
#include <libmatrix/orientation.h>
#include <libmatrix/matrix3.h>
#include <libmatrix/quaternion.h>
// Data Objects
#include "ut_status.h"

#define SNR_THRESHOLD 90


static bool utlm1()
{
    float R[3][3] = {
        { 0 }
    };
    Matrix3f rotationMatrix;

    // board rotation in RPY coordinates
    const float rpy[3] = { 0.0f, // roll board rotation
                           0.0f, // pitch
                           -90.0f }; // yaw

    // rotation quaternion
    float rotationQuat[4];

    // convert RPT to a quaternion
    RPY2Quaternion(rpy, rotationQuat);
    Quaternion boardRotation;
    boardRotation.from_euler(rpy[0], rpy[1], rpy[2]);

    // trim and summation quaternion
    float trimQuat[4];
    float sumQuat[4];
    Quaternion levelTrim;
    Quaternion sumBoardAndTrim;

    // trim in RPY
    const float trimRpy[3] = { -3.0f, 3.0f, 0.0f };

    // convert a RPY to a quaternion
    RPY2Quaternion(trimRpy, trimQuat);
    levelTrim.from_euler(trimRpy[0], trimRpy[1], trimRpy[2]);

    // multiply two quaternions together
    quat_mult(rotationQuat, trimQuat, sumQuat);
    sumBoardAndTrim = boardRotation * levelTrim;

    // convert a quaternion to a rotation matrix
    Quaternion2R(sumQuat, R);
    sumBoardAndTrim.rotation_matrix(rotationMatrix);

    // example mag transform
    float cal_mag_transform_as_array[3][3] = { // [row] [col]
        { 1.0f, 0.1f, 0.0f }, // row 0
        { 0.2f, 1.0f, 0.0f,}, // row 1
        { 0.0f, 0.0f, 1.0f } // row 2
    };


    Vector3f va(1.0f, 0.2f, 0.0f); // col 0
    Vector3f vb(0.1f, 1.0f, 0.0f); // col 1
    Vector3f vc(0.0f, 0.0f, 1.0f); // col 2
    Matrix3f mag_transform_new(va, vb, vc);

    float mag_transform[3][3];

    // rotate the mag transform....hmm do we want to do this??
    // R * mag_transform
    matrix_mult_3x3f(cal_mag_transform_as_array, R, mag_transform);
    mag_transform_new *= rotationMatrix;


    float vec_in[3] = { 1.0f, 1.0f, 1.0f };
    Vector3f vectorIn(1.0f, 1.0f, 1.0f);
    Vector3f vectorOut;
    float vec_out[3];

    rot_mult(R, vec_in, vec_out);
    vectorOut = vectorIn;
    vectorOut.matrix_rotate(rotationMatrix);

    if (is_equal(vec_out[0], vectorOut[0]) &&
        is_equal(vec_out[1], vectorOut[1]) &&
        is_equal(vec_out[2], vectorOut[2])) {
        return true;
    } else {
        return false;
    }
}


void ut_libmatrix(UT_StatusData &utStatus)
{
    utStatus.OperationState = UT_STATUS_OPERATIONSTATE_ACTIVE;
    UT_StatusSet(&utStatus);


    // execute unit tests
    bool status = utlm1();


    // on completion
    utStatus.OperationState = UT_STATUS_OPERATIONSTATE_COMPLETED;

    // on error
    if (!status) {
        utStatus.Error = UT_STATUS_ERROR_ERROR;
    } else {
        utStatus.Error = UT_STATUS_ERROR_NOERROR;
    }
    UT_StatusSet(&utStatus);
}
