/*
 *    File: ut_libmatrix.h unit testing of matrix methods
 *
 *    Project XFlight
 *
 *    Copyright (C) 2016 NextGenRotors.
 *
 */
#ifndef UT_LIBMATRIX_H
#define UT_LIBMATRIX_H

#include "project_xflight.h"
#include "ut_status.h"

void ut_libmatrix(UT_StatusData &utStatus);

#endif
