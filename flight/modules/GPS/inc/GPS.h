/**
 ******************************************************************************
 * @addtogroup OpenPilotModules OpenPilot Modules
 * @{
 * @addtogroup GSPModule GPS Module
 * @brief Process GPS information
 * @{
 *
 * @file       GPS.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      Include file of the GPS module.
 *             As with all modules only the initialize function is exposed all other
 *             interactions with the module take place through the event queue and
 *             objects.
 *
 *****************************************************************************/
#ifndef GPS_H
#define GPS_H

#include "gpsvelocitysensor.h"
#include "gpssatellites.h"
#include "gpspositionsensor.h"
#include "gpstime.h"
#include "calibrationmags.h"

#define NO_PARSER         -3 // no parser available
#define PARSER_OVERRUN    -2 // message buffer overrun before completing the message
#define PARSER_ERROR      -1 // message unparsable by this parser
#define PARSER_INCOMPLETE 0 // parser needs more data to complete the message
#define PARSER_COMPLETE   1 // parser has received a complete message and finished processing

struct GPS_RX_STATS {
    uint16_t gpsRxReceived;
    uint16_t gpsRxChkSumError;
    uint16_t gpsRxOverflow;
    uint16_t gpsRxParserError;
};

int32_t GPSInitialize(void);
void gps_set_fc_baud_from_arg(uint8_t baud);
uint32_t hwsettings_gpsspeed_enum_to_baud(uint8_t baud);

#endif // GPS_H

/**
 * @}
 * @}
 */
