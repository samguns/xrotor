/**
 ******************************************************************************
 *
 * @file       soft_uart_demo.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @brief      Software UART demonstration module.
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "openpilot.h"
#include "softwareuartsettings.h"
#include "blhelicommand.h"
#include "taskinfo.h"
#include "pios_soft_uart.h"
#include "blheli_comm.h"

#define STACK_SIZE         (2 * configMINIMAL_STACK_SIZE)
#define TASK_PRIORITY      (tskIDLE_PRIORITY + 1)
#define TASK_LOOP_DELAY_MS 1000
#define COMMAND_DELAY      10
#define FLAHING_DELAY      50

#define MAX_RETRY          3
#define NO_RESPONSE        0xff

#define MCU_BYTE           3
#define MCU_SILAB          0x32 /* 2 */

typedef enum {
    NOT_CONNECTED = 0,
    IDLE = 1,
    PROGRESSING   = 2
} BLHeliLinkerState;

struct BLHeliLinkerHandler {
    BLHeliLinkerState state;
};

static xTaskHandle BLHeliLinkerTaskHandle;
static bool isBLHeliLinkerEnabled = false;

static struct BLHeliLinkerHandler esc_handler;

static void BLHeliLinkerTask(void *parameters);
// static void BLHeliCommandUpdateCb(UAVObjEvent *ev);
static void SoftwareUARTSettingsUpdateCb(UAVObjEvent *ev);
static bool BLHeliLinker_PingESC(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_ConnectESC(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_ReadESCConfiguration(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_ReadSiLabESCConfiguration(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_ReadAtmelESCConfiguration(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_WriteESCConfiguration(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_WriteSiLabESCConfiguration(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_WriteAtmelESCConfiguration(BLHeliCommandData *pCmdData);
static bool BLHeliLinker_FlashESCFirmware(BLHeliCommandData *pCmdData, uint16_t address,
                                          uint8_t *fw, uint8_t size);
static bool BLHeliLinker_FlashSiLabESCFirmware(BLHeliCommandData *pCmdData, uint16_t address,
                                               uint8_t *fw, uint8_t size);
static bool BLHeliLinker_FlashAtmelESCFirmware(BLHeliCommandData *pCmdData, uint16_t address,
                                               uint8_t *fw, uint8_t size);
static bool BLHeliLinker_EraseESCFlash(BLHeliCommandData *pCmdData, uint16_t address);
static bool BLHeliLinker_EraseSiLabESCFlash(BLHeliCommandData *pCmdData, uint16_t address);
static void BLHeliLinker_SendRequest(uint8_t *request, uint16_t request_len,
                                     uint8_t *response, uint16_t response_len, BLHeliCommandData *pCmdData);

int32_t BLHeliLinkerStart(void)
{
    // Start main task
    if ((true == isBLHeliLinkerEnabled) && (0 != PIOS_COM_SOFT_UART)) {
        xTaskCreate(BLHeliLinkerTask, "BLHeliLinker", STACK_SIZE, NULL, TASK_PRIORITY, &BLHeliLinkerTaskHandle);
        PIOS_TASK_MONITOR_RegisterTask(TASKINFO_RUNNING_BLHELILINKER, BLHeliLinkerTaskHandle);
    }
    return STATUS_OK;
}

int32_t BLHeliLinkerInitialize(void)
{
    SoftwareUARTSettingsInitialize();
    BLHeliCommandInitialize();

    SoftwareUARTSettingsApplicationOptions softUART_application;
    SoftwareUARTSettingsApplicationGet(&softUART_application);
    if (SOFTWAREUARTSETTINGS_APPLICATION_BLHELILINKER == softUART_application) {
        isBLHeliLinkerEnabled = true;
    }

    SoftwareUARTSettingsConnectCallback(SoftwareUARTSettingsUpdateCb);

    esc_handler.state = NOT_CONNECTED;

    return STATUS_OK;
}

MODULE_INITCALL(BLHeliLinkerInitialize, BLHeliLinkerStart);


static void BLHeliLinkerTask(__attribute__((unused)) void *parameters)
{
    BLHeliCommandData cmdData;
    bool keepAlive;

    while (1) {
        keepAlive = false;
        BLHeliCommandGet(&cmdData);

        switch (cmdData.Operations) {
        case BLHELICOMMAND_OPERATIONS_CONNECT:
            if (esc_handler.state == NOT_CONNECTED) {
                BLHeliLinker_ConnectESC(&cmdData);
            }

            break;

        case BLHELICOMMAND_OPERATIONS_READ:
            if (esc_handler.state != IDLE) {
                break;
            }

            BLHeliLinker_ReadESCConfiguration(&cmdData);
            break;

        case BLHELICOMMAND_OPERATIONS_WRITE:
            if (esc_handler.state != IDLE) {
                break;
            }

            BLHeliLinker_WriteESCConfiguration(&cmdData);

            /* Read back */
            vTaskDelay(COMMAND_DELAY);
            BLHeliLinker_ReadESCConfiguration(&cmdData);
            break;

        case BLHELICOMMAND_OPERATIONS_FLASH:
            if ((esc_handler.state != IDLE) &&
                (esc_handler.state != PROGRESSING)) {
                break;
            }

            BLHeliLinker_FlashESCFirmware(&cmdData,
                                          cmdData.MemAddress,
                                          cmdData.MemBuffer,
                                          cmdData.MemBufferSize);

            esc_handler.state = PROGRESSING;
            break;

        case BLHELICOMMAND_OPERATIONS_ERASE:
            if ((esc_handler.state != IDLE) &&
                (esc_handler.state != PROGRESSING)) {
                break;
            }

            BLHeliLinker_EraseESCFlash(&cmdData,
                                       cmdData.MemAddress);

            esc_handler.state = PROGRESSING;
            break;

        case BLHELICOMMAND_OPERATIONS_STOP:
            esc_handler.state = IDLE;
            break;

        case BLHELICOMMAND_OPERATIONS_NONE:
        default:
            keepAlive = true;
            break;
        }

        if ((esc_handler.state == IDLE) && (true == keepAlive)) {
            vTaskDelay(TASK_LOOP_DELAY_MS);
            if (true == BLHeliLinker_PingESC(&cmdData)) {
                BLHeliCommandSetDefaults(BLHeliCommandBase(), 0);
            } else {
                vTaskDelay(COMMAND_DELAY);
            }
        } else if (esc_handler.state == PROGRESSING) {
            vTaskDelay(FLAHING_DELAY);
        } else {
            vTaskDelay(TASK_LOOP_DELAY_MS);
        }

        if (BLHELICOMMAND_OPERATIONS_NONE != cmdData.Operations) {
            cmdData.Operations = BLHELICOMMAND_OPERATIONS_NONE;
            BLHeliCommandSet(&cmdData);
        }
    }
}

static void SoftwareUARTSettingsUpdateCb(__attribute__((unused)) UAVObjEvent *ev)
{
    BLHeliCommandSetDefaults(BLHeliCommandBase(), 0);
}

static bool BLHeliLinker_PingESC(BLHeliCommandData *pCmdData)
{
    uint8_t buffer[BLHELI_CMD_KEEPALIVE_LENGTH];

    BLHeli_COMM_GenericCmdKeepAlive(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_KEEPALIVE_LENGTH,
                             buffer, BLHELI_ACK_KEEPALIVE_LENGTH,
                             pCmdData);

    if ((buffer[BLHELI_ACK_KEEPALIVE_LENGTH - 1] == NO_RESPONSE) ||
        (buffer[BLHELI_ACK_KEEPALIVE_LENGTH - 1] == 0)) {
        esc_handler.state    = NOT_CONNECTED;
        pCmdData->Connection = BLHELICOMMAND_CONNECTION_DISCONNECTED;
        return true;
    }

    return false;
}

static bool BLHeliLinker_ConnectESC(BLHeliCommandData *pCmdData)
{
    uint8_t buffer[BLHELI_CMD_BOOTSIGN_LENGTH];

    BLHeli_COMM_GenericCmdPreBootSign(buffer);
    BLHeliLinker_SendRequest(buffer, 2,
                             NULL, 0, NULL);

    vTaskDelay(COMMAND_DELAY);

    BLHeli_COMM_GenericCmdBootSign(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_BOOTSIGN_LENGTH,
                             buffer, BLHELI_ACK_BOOTSIGN_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_BOOTSIGN_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        esc_handler.state    = NOT_CONNECTED;
        pCmdData->Connection = BLHELICOMMAND_CONNECTION_DISCONNECTED;
    } else {
        if (buffer[MCU_BYTE] == MCU_SILAB) {
            pCmdData->MCUType = BLHELICOMMAND_MCUTYPE_SILAB;
        } else {
            pCmdData->MCUType = BLHELICOMMAND_MCUTYPE_ATMEL;
        }

        esc_handler.state    = IDLE;
        pCmdData->Connection = BLHELICOMMAND_CONNECTION_CONNECTED;
    }

    return true;
}

static bool BLHeliLinker_ReadESCConfiguration(BLHeliCommandData *pCmdData)
{
    bool retval = false;

    if (BLHELICOMMAND_MCUTYPE_SILAB == pCmdData->MCUType) {
        retval = BLHeliLinker_ReadSiLabESCConfiguration(pCmdData);
    } else {
        retval = BLHeliLinker_ReadAtmelESCConfiguration(pCmdData);
    }

    return retval;
}

static bool BLHeliLinker_ReadSiLabESCConfiguration(BLHeliCommandData *pCmdData)
{
    uint8_t buffer[BLHELI_PARAMETERS_DATA_LENGTH];

    /* Set Address to 0x1A00 */
    BLHeli_COMM_GenericCmdSetAddress(buffer, BLHELI_SILAB_PARAMS_ADDRESS);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETADDR_LENGTH,
                             buffer, BLHELI_ACK_SETADDR_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETADDR_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETADDRFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Read 112 bytes from 0x1A00 */
    BLHeli_COMM_SiLabCmdReadFlash(buffer, BLHELI_PARAMETERS_LENGTH);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_READPARAMS_LENGTH,
                             buffer, BLHELI_PARAMETERS_DATA_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_PARAMETERS_DATA_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_READFLASHFAIL;
        return true;
    }

    BLHeli_COMM_ParseReadBuffer(buffer, pCmdData);
    return true;
}

static bool BLHeliLinker_ReadAtmelESCConfiguration(BLHeliCommandData *pCmdData)
{
    uint8_t buffer[BLHELI_PARAMETERS_DATA_LENGTH];

    /* Set Address to 0x0000 */
    BLHeli_COMM_GenericCmdSetAddress(buffer, BLHELI_ATMEL_PARAMS_ADDRESS);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETADDR_LENGTH,
                             buffer, BLHELI_ACK_SETADDR_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETADDR_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETADDRFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Read 112 bytes from 0x0000 */
    BLHeli_COMM_AtmelCmdReadEEPROM(buffer, BLHELI_PARAMETERS_LENGTH);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_READPARAMS_LENGTH,
                             buffer, BLHELI_PARAMETERS_DATA_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_PARAMETERS_DATA_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_READFLASHFAIL;
        return true;
    }

    BLHeli_COMM_ParseReadBuffer(buffer, pCmdData);
    return true;
}

static bool BLHeliLinker_WriteESCConfiguration(BLHeliCommandData *pCmdData)
{
    bool retval = false;

    if (BLHELICOMMAND_MCUTYPE_SILAB == pCmdData->MCUType) {
        retval = BLHeliLinker_WriteSiLabESCConfiguration(pCmdData);
    } else {
        retval = BLHeliLinker_WriteAtmelESCConfiguration(pCmdData);
    }

    return retval;
}

static bool BLHeliLinker_WriteSiLabESCConfiguration(BLHeliCommandData *pCmdData)
{
    uint8_t buffer[BLHELI_PARAM_CRC16_LENGTH];

    /* Set Address to 0x1A00 */
    BLHeli_COMM_GenericCmdSetAddress(buffer, BLHELI_SILAB_PARAMS_ADDRESS);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETADDR_LENGTH,
                             buffer, BLHELI_ACK_SETADDR_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETADDR_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETADDRFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Erase Flash */
    BLHeli_COMM_SiLabCmdEraseFlash(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_ERASEFLASH_LENGTH,
                             buffer, BLHELI_ACK_ERASEFLASH_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_ERASEFLASH_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_ERASEFLASHFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer Size */
    BLHeli_COMM_GenericCmdSetBufferSize(buffer, BLHELI_EEPROM_WRITABLE_SIZE);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETBUFFERSIZE_LENGTH,
                             NULL, 0,
                             pCmdData);

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer */
    BLHeli_COMM_FormatConfigBuffer(buffer, pCmdData);
    BLHeliLinker_SendRequest(buffer, BLHELI_PARAM_CRC16_LENGTH,
                             buffer, BLHELI_ACK_SETBUFFER_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETBUFFER_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETBUFFERFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Program Flash */
    BLHeli_COMM_SiLabCmdProgramFlash(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_PROGRAM_LENGTH,
                             buffer, BLHELI_ACK_PROGRAM_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_PROGRAM_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_PROGRAMFLASHFAIL;
        return true;
    }

    return true;
}

static bool BLHeliLinker_WriteAtmelESCConfiguration(BLHeliCommandData *pCmdData)
{
    uint8_t buffer[BLHELI_PARAM_CRC16_LENGTH];

    /* Set Address to 0x0000 */
    BLHeli_COMM_GenericCmdSetAddress(buffer, BLHELI_ATMEL_PARAMS_ADDRESS);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETADDR_LENGTH,
                             buffer, BLHELI_ACK_SETADDR_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETADDR_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETADDRFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer Size */
    BLHeli_COMM_GenericCmdSetBufferSize(buffer, BLHELI_EEPROM_WRITABLE_SIZE);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETBUFFERSIZE_LENGTH,
                             NULL, 0,
                             pCmdData);

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer */
    BLHeli_COMM_FormatConfigBuffer(buffer, pCmdData);
    BLHeliLinker_SendRequest(buffer, BLHELI_PARAM_CRC16_LENGTH,
                             buffer, BLHELI_ACK_SETBUFFER_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETBUFFER_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETBUFFERFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Program EEPROM */
    BLHeli_COMM_AtmelCmdProgramEEPROM(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_PROGRAM_LENGTH,
                             buffer, BLHELI_ACK_PROGRAM_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_PROGRAM_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_PROGRAMFLASHFAIL;
        return true;
    }

    return true;
}

static bool BLHeliLinker_FlashESCFirmware(BLHeliCommandData *pCmdData, uint16_t address,
                                          uint8_t *fw, uint8_t size)
{
    bool retval = false;

    if (BLHELICOMMAND_MCUTYPE_SILAB == pCmdData->MCUType) {
        retval = BLHeliLinker_FlashSiLabESCFirmware(pCmdData, address, fw, size);
    } else {
        retval = BLHeliLinker_FlashAtmelESCFirmware(pCmdData, address, fw, size);
    }

    return retval;
}

static bool BLHeliLinker_FlashSiLabESCFirmware(BLHeliCommandData *pCmdData, uint16_t address,
                                               uint8_t *fw, uint8_t size)
{
    uint8_t buffer[size + BLHELI_CRC16_LENGTH];

    /* Set Address */
    BLHeli_COMM_GenericCmdSetAddress(buffer, address);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETADDR_LENGTH,
                             buffer, BLHELI_ACK_SETADDR_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETADDR_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETADDRFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer Size */
    BLHeli_COMM_GenericCmdSetBufferSize(buffer, size);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETBUFFERSIZE_LENGTH,
                             NULL, 0,
                             pCmdData);

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer */
    BLHeli_COMM_FormatFirmwareBuffer(buffer, fw, size);
    BLHeliLinker_SendRequest(buffer, size + BLHELI_CRC16_LENGTH,
                             buffer, BLHELI_ACK_SETBUFFER_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETBUFFER_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETBUFFERFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Program Flash */
    BLHeli_COMM_SiLabCmdProgramFlash(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_PROGRAM_LENGTH,
                             buffer, BLHELI_ACK_PROGRAM_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_PROGRAM_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_PROGRAMFLASHFAIL;
        return true;
    }

    return true;
}

static bool BLHeliLinker_FlashAtmelESCFirmware(BLHeliCommandData *pCmdData, uint16_t address,
                                               uint8_t *fw, uint8_t size)
{
    uint8_t buffer[size + BLHELI_CRC16_LENGTH];

    /* Set Address */
    BLHeli_COMM_GenericCmdSetAddress(buffer, address);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETADDR_LENGTH,
                             buffer, BLHELI_ACK_SETADDR_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETADDR_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETADDRFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer Size */
    BLHeli_COMM_GenericCmdSetBufferSize(buffer, size);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETBUFFERSIZE_LENGTH,
                             NULL, 0,
                             pCmdData);

    vTaskDelay(COMMAND_DELAY);

    /* Set Buffer */
    BLHeli_COMM_FormatFirmwareBuffer(buffer, fw, size);
    BLHeliLinker_SendRequest(buffer, size + BLHELI_CRC16_LENGTH,
                             buffer, BLHELI_ACK_SETBUFFER_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETBUFFER_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETBUFFERFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Program Flash */
    BLHeli_COMM_AtmelCmdProgramFlash(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_PROGRAM_LENGTH,
                             buffer, BLHELI_ACK_PROGRAM_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_PROGRAM_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_PROGRAMFLASHFAIL;
        return true;
    }

    return true;
}

static bool BLHeliLinker_EraseESCFlash(BLHeliCommandData *pCmdData, uint16_t address)
{
    bool retval = false;

    if (BLHELICOMMAND_MCUTYPE_SILAB == pCmdData->MCUType) {
        retval = BLHeliLinker_EraseSiLabESCFlash(pCmdData, address);
    }

    return retval;
}

static bool BLHeliLinker_EraseSiLabESCFlash(BLHeliCommandData *pCmdData, uint16_t address)
{
    uint8_t buffer[BLHELI_ERASE_CRC16_LENGTH];

    /* Set Address */
    BLHeli_COMM_GenericCmdSetAddress(buffer, address);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_SETADDR_LENGTH,
                             buffer, BLHELI_ACK_SETADDR_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_SETADDR_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_SETADDRFAIL;
        return true;
    }

    vTaskDelay(COMMAND_DELAY);

    /* Erase Flash */
    BLHeli_COMM_SiLabCmdEraseFlash(buffer);
    BLHeliLinker_SendRequest(buffer, BLHELI_CMD_ERASEFLASH_LENGTH,
                             buffer, BLHELI_ACK_ERASEFLASH_LENGTH,
                             pCmdData);

    if (buffer[BLHELI_ACK_ERASEFLASH_LENGTH - 1] != BLHELI_COMM_ACK_OK) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_ERASEFLASHFAIL;
        return true;
    }

    return true;
}

static void BLHeliLinker_SendRequest(uint8_t *request, uint16_t request_len,
                                     uint8_t *response, uint16_t response_len, BLHeliCommandData *pCmdData)
{
    int retry = 0;
    uint16_t rxBytes = 0;

    PIOS_COM_SendBufferNonBlocking(PIOS_COM_SOFT_UART, request, request_len);

    if (NULL == response) {
        return;
    }

    do {
        rxBytes = PIOS_COM_ReceiveFixedSizeBuffer(PIOS_COM_SOFT_UART, response,
                                                  response_len,
                                                  TASK_LOOP_DELAY_MS);
        retry++;
    } while ((rxBytes == 0) && (retry < MAX_RETRY));

    pCmdData->DebugFlag  = rxBytes; // PIOS_SOFT_UART_getState
    pCmdData->DebugState = retry; // PIOS_SOFT_UART_getError();

    if (retry >= MAX_RETRY) {
        response[0] = NO_RESPONSE;
    }
}
