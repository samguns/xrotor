/**
 ******************************************************************************
 *
 * @file       blheli_comm.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @brief      BLHeli Bootloader communication protocol module.
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "openpilot.h"
#include "blhelicommand.h"
#include "blheli_comm.h"

#define BLHELI_SILAB_COMMAND_PROGRAM_FLASH  0x01
#define BLHELI_ATMEL_COMMAND_PROGRAM_FLASH  BLHELI_SILAB_COMMAND_PROGRAM_FLASH
#define BLHELI_SILAB_COMMAND_ERASE_FLASH    0x02
#define BLHELI_SILAB_COMMAND_READ_FLASH     0x03
#define BLHELI_ATMEL_COMMAND_READ_EEPROM    0x04
#define BLHELI_ATMEL_COMMAND_PROGRAM_EEPROM 0x05
#define BLHELI_ATMEL_COMMAND_READ_FLASH     0x07
#define BLHELI_COMMAND_SET_ADDRESS          0xFF
#define BLHELI_COMMAND_SET_BUFFER           0xFE
#define BLHELI_COMMAND_KEEPALIVE            0xFD

#define BLHELI_CRC_SETADDR_LENGTH           (BLHELI_CMD_SETADDR_LENGTH - 2)
#define BLHELI_CRC_READPARAMS_LENGTH        (BLHELI_CMD_READPARAMS_LENGTH - 2)
#define BLHELI_CRC_SETBUFFERSIZE_LENGTH     (BLHELI_CMD_SETBUFFERSIZE_LENGTH - 2)

#define BLHELI_ESC_TAG_OFFSET               0x40
#define BLHELI_ESC_TAG_LENGTH               16
#define BLHELI_ESC_MCU_OFFSET               0x50
#define BLHELI_ESC_MCU_LENGTH               16
#define BLHELI_DUMMY_OFFSET                 0x60
#define BLHELI_DUMMY_LENGTH                 16

#define BLHELI_VALUE_OFFSET                 1


#define P_16                                0xa001

static const uint16_t crc_tab16[] = { 0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301,
                                      0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0,0x0780,  0xC741, 0x0500, 0xC5C1,
                                      0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80,0xCD41,  0x0F00, 0xCFC1, 0xCE81,
                                      0x0E40, 0x0A00, 0xCAC1, 0xCB81, 0x0B40,0xC901,  0x09C0, 0x0880, 0xC841,
                                      0xD801, 0x18C0, 0x1980, 0xD941, 0x1B00,0xDBC1,  0xDA81, 0x1A40, 0x1E00,
                                      0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0,0x1C80,  0xDC41, 0x1400, 0xD4C1,
                                      0xD581, 0x1540, 0xD701, 0x17C0, 0x1680,0xD641,  0xD201, 0x12C0, 0x1380,
                                      0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040,0xF001,  0x30C0, 0x3180, 0xF141,
                                      0x3300, 0xF3C1, 0xF281, 0x3240, 0x3600,0xF6C1,  0xF781, 0x3740, 0xF501,
                                      0x35C0, 0x3480, 0xF441, 0x3C00, 0xFCC1,0xFD81,  0x3D40, 0xFF01, 0x3FC0,
                                      0x3E80, 0xFE41, 0xFA01, 0x3AC0, 0x3B80,0xFB41,  0x3900, 0xF9C1, 0xF881,
                                      0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940,0xEB01,  0x2BC0, 0x2A80, 0xEA41,
                                      0xEE01, 0x2EC0, 0x2F80, 0xEF41, 0x2D00,0xEDC1,  0xEC81, 0x2C40, 0xE401,
                                      0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1,0xE681,  0x2640, 0x2200, 0xE2C1,
                                      0xE381, 0x2340, 0xE101, 0x21C0, 0x2080,0xE041,  0xA001, 0x60C0, 0x6180,
                                      0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240,0x6600,  0xA6C1, 0xA781, 0x6740,
                                      0xA501, 0x65C0, 0x6480, 0xA441, 0x6C00,0xACC1,  0xAD81, 0x6D40, 0xAF01,
                                      0x6FC0, 0x6E80, 0xAE41, 0xAA01, 0x6AC0,0x6B80,  0xAB41, 0x6900, 0xA9C1,
                                      0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981,0x7940,  0xBB01, 0x7BC0, 0x7A80,
                                      0xBA41, 0xBE01, 0x7EC0, 0x7F80, 0xBF41,0x7D00,  0xBDC1, 0xBC81, 0x7C40,
                                      0xB401, 0x74C0, 0x7580, 0xB541, 0x7700,0xB7C1,  0xB681, 0x7640, 0x7200,
                                      0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0,0x7080,  0xB041, 0x5000, 0x90C1,
                                      0x9181, 0x5140, 0x9301, 0x53C0, 0x5280,0x9241,  0x9601, 0x56C0, 0x5780,
                                      0x9741, 0x5500, 0x95C1, 0x9481, 0x5440,0x9C01,  0x5CC0, 0x5D80, 0x9D41,
                                      0x5F00, 0x9FC1, 0x9E81, 0x5E40, 0x5A00,0x9AC1,  0x9B81, 0x5B40, 0x9901,
                                      0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0,0x4980,  0x8941, 0x4B00, 0x8BC1,
                                      0x8A81, 0x4A40, 0x4E00, 0x8EC1, 0x8F81,0x4F40,  0x8D01, 0x4DC0, 0x4C80,
                                      0x8C41, 0x4400, 0x84C1, 0x8581, 0x4540,0x8701,  0x47C0, 0x4680, 0x8641,
                                      0x8201, 0x42C0, 0x4380, 0x8341, 0x4100,0x81C1,  0x8081, 0x4040 };

void BLHeli_COMM_GenericCmdPreBootSign(uint8_t *data)
{
    data[0] = 0xff;
    data[1] = 0xff;
}

void BLHeli_COMM_GenericCmdBootSign(uint8_t *data)
{
    data[0]  = 0x00;
    data[1]  = 0x00;
    data[2]  = 0x00;
    data[3]  = 0x00;
    data[4]  = 0x00;
    data[5]  = 0x00;
    data[6]  = 0x00;
    data[7]  = 0x00;
    data[8]  = 0x0d;

    data[9]  = 0x42; // B
    data[10] = 0x4c; // L
    data[11] = 0x48; // H
    data[12] = 0x65; // e
    data[13] = 0x6c; // l
    data[14] = 0x69; // i

    /* Hard code CRC, no need to calculate it every time */
    data[15] = 0xf4;
    data[16] = 0x7d;
}

void BLHeli_COMM_GenericCmdKeepAlive(uint8_t *data)
{
    data[0] = BLHELI_COMMAND_KEEPALIVE;
    data[1] = 0x00;

    /* Hard code CRC, no need to calculate it every time */
    data[2] = 0x40;
    data[3] = 0x90;
}

void BLHeli_COMM_GenericCmdSetAddress(uint8_t *data, uint16_t addr)
{
    uint16_t crc16 = 0;
    unsigned char *crc16_byte = (unsigned char *)&crc16;
    unsigned char *addr_byte  = (unsigned char *)&addr;
    int i;

    data[0] = BLHELI_COMMAND_SET_ADDRESS;
    data[1] = 0x00;

    data[2] = *(addr_byte + 1);
    data[3] = *addr_byte;

    for (i = 0; i < BLHELI_CRC_SETADDR_LENGTH; i++) {
        crc16 = BLHeli_COMM_UpdateCRC16(crc16, data[i]);
    }

    data[4] = *crc16_byte;
    data[5] = *(crc16_byte + 1);
}

void BLHeli_COMM_SiLabCmdReadFlash(uint8_t *data, uint8_t size)
{
    uint16_t crc16 = 0;
    unsigned char *crc16_byte = (unsigned char *)&crc16;
    int i;

    data[0] = BLHELI_SILAB_COMMAND_READ_FLASH;
    data[1] = size;

    for (i = 0; i < BLHELI_CRC_READPARAMS_LENGTH; i++) {
        crc16 = BLHeli_COMM_UpdateCRC16(crc16, data[i]);
    }

    data[2] = *crc16_byte;
    data[3] = *(crc16_byte + 1);
}

void BLHeli_COMM_AtmelCmdReadEEPROM(uint8_t *data, uint8_t size)
{
    uint16_t crc16 = 0;
    unsigned char *crc16_byte = (unsigned char *)&crc16;
    int i;

    data[0] = BLHELI_ATMEL_COMMAND_READ_EEPROM;
    data[1] = size;

    for (i = 0; i < BLHELI_CRC_READPARAMS_LENGTH; i++) {
        crc16 = BLHeli_COMM_UpdateCRC16(crc16, data[i]);
    }

    data[2] = *crc16_byte;
    data[3] = *(crc16_byte + 1);
}

void BLHeli_COMM_SiLabCmdEraseFlash(uint8_t *data)
{
    data[0] = BLHELI_SILAB_COMMAND_ERASE_FLASH;
    data[1] = 0x01;

    /* Hard code CRC, no need to calculate it every time */
    data[2] = 0xc0;
    data[3] = 0xa0;
}

void BLHeli_COMM_GenericCmdSetBufferSize(uint8_t *data, uint16_t size)
{
    uint16_t crc16 = 0;
    unsigned char *crc16_byte = (unsigned char *)&crc16;
    unsigned char *size_byte  = (unsigned char *)&size;
    int i;

    data[0] = BLHELI_COMMAND_SET_BUFFER;
    data[1] = 0x00;

    data[2] = *(size_byte + 1);
    data[3] = *size_byte;

    for (i = 0; i < BLHELI_CRC_SETBUFFERSIZE_LENGTH; i++) {
        crc16 = BLHeli_COMM_UpdateCRC16(crc16, data[i]);
    }

    data[4] = *crc16_byte;
    data[5] = *(crc16_byte + 1);
}

void BLHeli_COMM_SiLabCmdProgramFlash(uint8_t *data)
{
    data[0] = BLHELI_SILAB_COMMAND_PROGRAM_FLASH;
    data[1] = 0x01;

    /* Hard code CRC, no need to calculate it every time */
    data[2] = 0xc0;
    data[3] = 0x50;
}

void BLHeli_COMM_AtmelCmdProgramFlash(uint8_t *data)
{
    data[0] = BLHELI_ATMEL_COMMAND_PROGRAM_FLASH;
    data[1] = 0x01;

    /* Hard code CRC, no need to calculate it every time */
    data[2] = 0xc0;
    data[3] = 0x50;
}

void BLHeli_COMM_AtmelCmdProgramEEPROM(uint8_t *data)
{
    data[0] = BLHELI_ATMEL_COMMAND_PROGRAM_EEPROM;
    data[1] = 0x01;

    /* Hard code CRC, no need to calculate it every time */
    data[2] = 0xc2;
    data[3] = 0x90;
}

void BLHeli_COMM_FormatConfigBuffer(uint8_t *data, BLHeliCommandData *pCmdData)
{
    uint16_t crc16 = 0;
    unsigned char *crc16_byte = (unsigned char *)&crc16;
    int i;

    memset(data, 0xff, BLHELI_PARAM_CRC16_LENGTH);

    data[0]  = pCmdData->Main_Rev;
    data[1]  = pCmdData->Sub_Rev;
    data[2]  = pCmdData->Layout_Rev;
    data[3]  = pCmdData->GovernorPGain + BLHELI_VALUE_OFFSET;
    data[4]  = pCmdData->GovernorIGain + BLHELI_VALUE_OFFSET;
    data[5]  = pCmdData->GovernorMode + BLHELI_VALUE_OFFSET;
    data[6]  = pCmdData->LowVoltageLimiter + BLHELI_VALUE_OFFSET;
    data[7]  = pCmdData->Motor_Gain + BLHELI_VALUE_OFFSET;
    data[8]  = pCmdData->Motor_IdleSpeed + BLHELI_VALUE_OFFSET;
    data[9]  = pCmdData->Startup_Power + BLHELI_VALUE_OFFSET;
    data[10] = pCmdData->PWM_Frequency + BLHELI_VALUE_OFFSET;
    data[11] = pCmdData->Motor_Rotation_Direction + BLHELI_VALUE_OFFSET;
    data[12] = pCmdData->Input_Polarity + BLHELI_VALUE_OFFSET;
    data[13] = pCmdData->Initialized_Low_Byte;
    data[14] = pCmdData->Initialized_High_Byte;
    data[15] = pCmdData->TX_Programming_Enabled;
    data[16] = pCmdData->ReArming_Every_Start;
    data[17] = pCmdData->Governor_Setup_Target;
    data[18] = pCmdData->Startup_RPM;
    data[19] = pCmdData->Startup_Accel;
    data[20] = pCmdData->Voltage_Comp;
    data[21] = pCmdData->Comm_Timing + BLHELI_VALUE_OFFSET;
    data[22] = pCmdData->Damping_Force;
    data[23] = pCmdData->Governor_Range + BLHELI_VALUE_OFFSET;
    data[24] = pCmdData->Startup_Method;
    data[25] = pCmdData->Minimum_Throttle;
    data[26] = pCmdData->Maximum_Throttle;
    data[27] = pCmdData->Beep_Strength;
    data[28] = pCmdData->Beacon_Strength;
    data[29] = pCmdData->Beacon_Delay + BLHELI_VALUE_OFFSET;
    data[30] = pCmdData->Throttle_Rate;
    data[31] = pCmdData->Demag_Compensation + BLHELI_VALUE_OFFSET;
    data[32] = pCmdData->BEC_Voltage_High;
    data[33] = pCmdData->Center_Throttle;
    data[34] = pCmdData->Spoolup_Time;
    data[35] = pCmdData->Temperature_Protection_Enabled;

    memcpy(&data[BLHELI_ESC_TAG_OFFSET], pCmdData->ESCTag, BLHELI_ESC_TAG_LENGTH);
    memcpy(&data[BLHELI_ESC_MCU_OFFSET], pCmdData->MCU, BLHELI_ESC_MCU_LENGTH);
    memset(&data[BLHELI_DUMMY_OFFSET], 0x20, BLHELI_DUMMY_LENGTH);

    for (i = 0; i < BLHELI_PARAMETERS_LENGTH; i++) {
        crc16 = BLHeli_COMM_UpdateCRC16(crc16, data[i]);
    }

    data[BLHELI_PARAM_CRC16_LENGTH - 2] = *crc16_byte;
    data[BLHELI_PARAM_CRC16_LENGTH - 1] = *(crc16_byte + 1);
}

void BLHeli_COMM_FormatFirmwareBuffer(uint8_t *data, uint8_t *fw, uint16_t size)
{
    uint16_t crc16 = 0;
    unsigned char *crc16_byte = (unsigned char *)&crc16;
    int i;

    memcpy(data, fw, size);

    for (i = 0; i < size; i++) {
        crc16 = BLHeli_COMM_UpdateCRC16(crc16, data[i]);
    }

    data[size]     = *crc16_byte;
    data[size + 1] = *(crc16_byte + 1);
}

void BLHeli_COMM_ParseReadBuffer(uint8_t *data, BLHeliCommandData *pCmdData)
{
    /* CRC16 verification */
    uint16_t crc16 = 0;
    unsigned char *crc16_byte = (unsigned char *)&crc16;
    int i = 0;

    for (i = 0; i < BLHELI_PARAMETERS_LENGTH; i++) {
        crc16 = BLHeli_COMM_UpdateCRC16(crc16, data[i]);
    }
    if ((data[BLHELI_PARAMETERS_DATA_LENGTH - 3] != *crc16_byte) ||
        (data[BLHELI_PARAMETERS_DATA_LENGTH - 2] != *(crc16_byte + 1))) {
        pCmdData->ErrorCode = BLHELICOMMAND_ERRORCODE_BADDATA;
        pCmdData->DebugFlag = crc16;
        return;
    }

    pCmdData->Main_Rev                 = data[0];
    pCmdData->Sub_Rev                  = data[1];
    pCmdData->Layout_Rev               = data[2];
    pCmdData->GovernorPGain            = data[3] - BLHELI_VALUE_OFFSET;
    pCmdData->GovernorIGain            = data[4] - BLHELI_VALUE_OFFSET;
    pCmdData->GovernorMode             = data[5] - BLHELI_VALUE_OFFSET;
    pCmdData->LowVoltageLimiter        = data[6] - BLHELI_VALUE_OFFSET;
    pCmdData->Motor_Gain               = data[7] - BLHELI_VALUE_OFFSET;
    pCmdData->Motor_IdleSpeed          = data[8] - BLHELI_VALUE_OFFSET;
    pCmdData->Startup_Power            = data[9] - BLHELI_VALUE_OFFSET;
    pCmdData->PWM_Frequency            = data[10] - BLHELI_VALUE_OFFSET;
    pCmdData->Motor_Rotation_Direction = data[11] - BLHELI_VALUE_OFFSET;
    pCmdData->Input_Polarity           = data[12] - BLHELI_VALUE_OFFSET;
    pCmdData->Initialized_Low_Byte     = data[13];
    pCmdData->Initialized_High_Byte    = data[14];
    pCmdData->TX_Programming_Enabled   = data[15];
    pCmdData->ReArming_Every_Start     = data[16];
    pCmdData->Governor_Setup_Target    = data[17];
    pCmdData->Startup_RPM              = data[18];
    pCmdData->Startup_Accel            = data[19];
    pCmdData->Voltage_Comp             = data[20];
    pCmdData->Comm_Timing              = data[21] - BLHELI_VALUE_OFFSET;
    pCmdData->Damping_Force            = data[22];
    pCmdData->Governor_Range           = data[23] - BLHELI_VALUE_OFFSET;
    pCmdData->Startup_Method           = data[24];
    pCmdData->Minimum_Throttle         = data[25];
    pCmdData->Maximum_Throttle         = data[26];
    pCmdData->Beep_Strength            = data[27];
    pCmdData->Beacon_Strength          = data[28];
    pCmdData->Beacon_Delay             = data[29] - BLHELI_VALUE_OFFSET;
    pCmdData->Throttle_Rate            = data[30];
    pCmdData->Demag_Compensation       = data[31] - BLHELI_VALUE_OFFSET;
    pCmdData->BEC_Voltage_High         = data[32];
    pCmdData->Center_Throttle          = data[33];
    pCmdData->Spoolup_Time             = data[34];
    pCmdData->Temperature_Protection_Enabled = data[35];

    memcpy(pCmdData->ESCTag, &data[BLHELI_ESC_TAG_OFFSET], BLHELI_ESC_TAG_LENGTH);
    memcpy(pCmdData->MCU, &data[BLHELI_ESC_MCU_OFFSET], BLHELI_ESC_MCU_LENGTH);
    memcpy(pCmdData->MemBuffer, data, BLHELI_PARAMETERS_DATA_LENGTH);
}

uint16_t BLHeli_COMM_UpdateCRC16(uint16_t crc, uint8_t c)
{
    uint16_t tmp, short_c;

    short_c = 0x00ff & (uint16_t)c;

    tmp     = crc ^ short_c;
    crc     = (crc >> 8) ^ crc_tab16[tmp & 0xff];

    return crc;
}
