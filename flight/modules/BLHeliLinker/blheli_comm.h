/**
 ******************************************************************************
 *
 * @file       blheli_comm.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2015.
 * @brief      A software UART implementation
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#ifndef BLHELI_COMM_H
#define BLHELI_COMM_H

#define BLHELI_ACK_LENGTH               1
#define BLHELI_CRC16_LENGTH             2
#define BLHELI_PARAMETERS_LENGTH        (16 * 7)
#define BLHELI_PARAM_CRC16_LENGTH       (BLHELI_PARAMETERS_LENGTH + BLHELI_CRC16_LENGTH)
#define BLHELI_PARAMETERS_DATA_LENGTH   (BLHELI_PARAM_CRC16_LENGTH + BLHELI_ACK_LENGTH)
#define BLHELI_ERASE_CRC16_LENGTH       (4 + BLHELI_CRC16_LENGTH)

#define BLHELI_CMD_BOOTSIGN_LENGTH      17
#define BLHELI_ACK_BOOTSIGN_LENGTH      9
#define BLHELI_CMD_KEEPALIVE_LENGTH     4
#define BLHELI_ACK_KEEPALIVE_LENGTH     1
#define BLHELI_CMD_SETADDR_LENGTH       6
#define BLHELI_ACK_SETADDR_LENGTH       1
#define BLHELI_CMD_READPARAMS_LENGTH    4
#define BLHELI_CMD_ERASEFLASH_LENGTH    4
#define BLHELI_ACK_ERASEFLASH_LENGTH    1
#define BLHELI_CMD_SETBUFFERSIZE_LENGTH 6
#define BLHELI_ACK_SETBUFFER_LENGTH     1
#define BLHELI_CMD_PROGRAM_LENGTH       4
#define BLHELI_ACK_PROGRAM_LENGTH       1

#define BLHELI_COMM_ACK_OK              0x30
#define BLHELI_COMM_ACK_VERIFY_ERROR    0xc0
#define BLHELI_COMM_ACK_COMMAND_ERROR   0xc1
#define BLHELI_COMM_ACK_CRC_ERROR       0xc2
#define BLHELI_COMM_ACK_PROGRAM_ERROR   0xc5
#define BLHELI_COMM_ACK_KEEPALIVE       BLHELI_COMM_ACK_COMMAND_ERROR
#define BLHELI_COMM_ACK_KEEPALIVE_2     0x82

#define BLHELI_SILAB_PARAMS_ADDRESS     0x1a00
#define BLHELI_ATMEL_PARAMS_ADDRESS     0x0
#define BLHELI_EEPROM_WRITABLE_SIZE     0x70

void BLHeli_COMM_GenericCmdPreBootSign(uint8_t *);
void BLHeli_COMM_GenericCmdBootSign(uint8_t *);
void BLHeli_COMM_GenericCmdKeepAlive(uint8_t *);
void BLHeli_COMM_GenericCmdSetAddress(uint8_t *, uint16_t);
void BLHeli_COMM_SiLabCmdReadFlash(uint8_t *, uint8_t);
void BLHeli_COMM_AtmelCmdReadEEPROM(uint8_t *, uint8_t);
void BLHeli_COMM_SiLabCmdEraseFlash(uint8_t *);
void BLHeli_COMM_GenericCmdSetBufferSize(uint8_t *, uint16_t);
void BLHeli_COMM_SiLabCmdProgramFlash(uint8_t *);
void BLHeli_COMM_AtmelCmdProgramFlash(uint8_t *);
void BLHeli_COMM_AtmelCmdProgramEEPROM(uint8_t *);
void BLHeli_COMM_FormatConfigBuffer(uint8_t *, BLHeliCommandData *);
void BLHeli_COMM_FormatFirmwareBuffer(uint8_t *, uint8_t *, uint16_t);
void BLHeli_COMM_ParseReadBuffer(uint8_t *, BLHeliCommandData *);
uint16_t BLHeli_COMM_UpdateCRC16(uint16_t, uint8_t);

#endif /* BLHELI_COMM_H */
