/**
 ******************************************************************************
 * @addtogroup OpenPilotModules OpenPilot Modules
 * @{
 * @addtogroup ActuatorModule Actuator Module
 * @brief Compute servo/motor settings based on @ref ActuatorDesired "desired actuator positions" and aircraft type.
 * This is where all the mixing of channels is computed.
 * @{
 *
 * @file       actuator.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      Actuator module. Drives the actuators (servos, motors etc).
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#ifndef ACTUATOR_H
#define ACTUATOR_H

#include "actuatorif.h"
#include <stdbool.h>

extern "C" {
#include <openpilot.h>
#include <actuatorsettings.h>
#include <actuatorcommand.h>
#include <actuatordesired.h>
#include <mixersettings.h>
#include <systemsettings.h>
#include <sanitycheck.h>
}

typedef enum { BUZZ_BUZZER = 0, BUZZ_ARMING = 1, BUZZ_INFO = 2, BUZZ_MAX = 3 } buzzertype;
#define MAX_MIX_ACTUATORS ACTUATORCOMMAND_CHANNEL_NUMELEM

class ActuatorImpl : public ActuatorIF {
private:
    static ActuatorImpl *p_inst;
    ActuatorImpl();
public:
    static ActuatorImpl *instance()
    {
        if (!p_inst) {
            p_inst = new ActuatorImpl();
        }
        return p_inst;
    }

    void Update(void);
    int32_t Initialize(void);
    int32_t Start(void);
    void ActuatorSettingsUpdated();
    void MixerSettingsUpdated();
    void SettingsUpdated();
protected:
    float ProcessMixer(const int index, const float curve1, const float curve2,
                       ActuatorDesiredData *desired, const float period, bool multirotor);
    float MixerCurveFullRangeProportional(const float input, const float *curve, uint8_t elements, bool multirotor);
    float MixerCurveFullRangeAbsolute(const float input, const float *curve, uint8_t elements, bool multirotor);
    int16_t scaleChannel(float value, int16_t max, int16_t min, int16_t neutral);
    int16_t scaleMotor(float value, int16_t max, int16_t min, int16_t neutral, float maxMotor, float minMotor, bool armed, bool AlwaysStabilizeWhenArmed, float throttleDesired);
    void setFailsafe();
    bool buzzerState(buzzertype type);
    bool set_channel(uint8_t mixer_channel, uint16_t value);
    void actuator_update_rate_if_changed(bool force_update);


#ifdef TRACE_ACTUATOR
    traceLabel actuator_start;
    traceLabel actuator_duration;
    traceLabel actuator_latency;
    traceLabel actuator_uavos_queried;
    traceLabel actuator_alarm_cleared;
    traceLabel actuator_curve2;
    traceLabel actuator_mixing;
    traceLabel actuator_cmd_calculated;
#endif /* TRACE_ACTUATOR */

    FrameType_t frameType;
    SystemSettingsThrustControlOptions thrustType;

    float lastResult[MAX_MIX_ACTUATORS];
    float filterAccumulator[MAX_MIX_ACTUATORS];
    uint8_t pinsMode[MAX_MIX_ACTUATORS];
    // used to inform the actuator thread that actuator update rate is changed
    ActuatorSettingsData actuatorSettings;
    bool spinWhileArmed;

    // used to inform the actuator thread that mixer settings are changed
    MixerSettingsData mixerSettings;
    int mixer_settings_count;
    portTickType lastSysTime;
};

#endif // ACTUATOR_H

/**
 * @}
 * @}
 */
