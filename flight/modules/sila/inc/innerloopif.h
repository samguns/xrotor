/**
 ******************************************************************************
 * @file       innerloopif.h
 * @author     Alex Beck, OPNG Copyright (C) 2015.
 * @brief      Interface class for Inner Loop
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#ifndef INNERLOOPIF_H
#define INNERLOOPIF_H

class InnerLoopIF {
public:
    virtual void Update(void) {}
    // virtual ~PathFollowerFSM() = 0;
};

#endif
