/**
 ******************************************************************************
 * @addtogroup OpenPilotModules OpenPilot Modules
 * @{
 * @addtogroup StabilizationModule Stabilization Module
 * @brief innerloop mode
 * @note This file implements the logic for a innerloop
 * @{
 *
 * @file       innerloop.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2014.
 * @brief      Attitude stabilization module.
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef INNERLOOP_H
#define INNERLOOP_H

extern "C" {
#include <openpilot.h>
#include <pid.h>
#include <stabilizationsettings.h>
#include <stabilizationbank.h>
#include <ratedesired.h>
}
#include "innerloopif.h"

typedef struct pid_curve_scaler {
    float  x;
    pointf points[5];
} pid_curve_scaler;

#define AXES 4

typedef struct {
    StabilizationSettingsData settings;
    StabilizationBankData     stabBank;
    RateDesiredData rateDesired;
    float gyro_alpha;
    struct {
        float min_thrust;
        float max_thrust;
    }    cruiseControl;
    struct {
        int8_t rateupdates;
    }    monitor;
    struct pid innerPids[3];
    bool thrust_pid_scaling_enabled[3][3];
} StabilizationData;

class ActuatorIF;

class InnerLoopImpl : public InnerLoopIF {
private:
    static InnerLoopImpl *p_inst;
    InnerLoopImpl();
public:
    static InnerLoopImpl *instance()
    {
        if (!p_inst) {
            p_inst = new InnerLoopImpl();
        }
        return p_inst;
    }

    void Update(void);
    void Initialize(void);
    void AirSpeedUpdated(void);
    void SettingsUpdated(void);
    void SettingsBankUpdated(void);
    void RateDesiredUpdated(void);
    void RegisterActuator(ActuatorIF *actuator)
    {
        actuator_ptr = actuator;
    }

protected:
    float get_pid_scale_source_value();
    float pid_curve_value(const pid_curve_scaler *scaler);
    pid_scaler create_pid_scaler(int axis);
    bool use_tps_for_roll();
    bool use_tps_for_pitch();
    bool use_tps_for_yaw();
    bool use_tps_for_p();
    bool use_tps_for_i();
    bool use_tps_for_d();
    float cruisecontrol_apply_factor(float raw);

#ifdef TRACE_INNERLOOP
    traceLabel innerloop_start;
    traceLabel innerloop_duration;
    traceLabel innerloop_end_alarms;
    traceLabel innerloop_start_axes;
    traceLabel innerloop_end_axes;
    traceLabel innerloop_end_actuator;
    traceLabel innerloop_end_piro;
#endif /* TRACE_INNERLOOP */


    StabilizationData stabSettings;
    PiOSDeltatimeConfig timeval;
    float speedScaleFactor;
    float gyro_filtered[3];
    float axis_lock_accum[3];
    uint8_t previous_mode[AXES];
    bool frame_is_multirotor;
    class ActuatorIF *actuator_ptr;
};


#endif /* INNERLOOP_H */
