/**
 ******************************************************************************
 *
 * @file       sila.cpp
 * @author     Alex Beck  Copyright (C) 2015.
 * @brief      Sensor + InnerLoop + Actuator super module
 *
 *****************************************************************************/
#include "project_xflight.h"
#include <callbackinfo.h>
#include "sensors.h"
#include "actuator.h"
#include "innerloop.h"

int32_t SilaInitialize(void);
int32_t SilaStart(void);

int32_t SilaStart(void)
{
    SensorsStart();
    ActuatorImpl::instance()->Start();
    return STATUS_OK;
}

int32_t SilaInitialize(void)
{
    SensorsInitialize();
    InnerLoopImpl::instance()->Initialize();
    ActuatorImpl::instance()->Initialize();

    // Register the innerloop interface with the sensor object
    SensorsRegisterInnerLoop(InnerLoopImpl::instance());
    InnerLoopImpl::instance()->RegisterActuator(ActuatorImpl::instance());

    return STATUS_OK;
}

MODULE_INITCALL(SilaInitialize, SilaStart);
