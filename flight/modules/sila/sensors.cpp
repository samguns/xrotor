/**
 * Input objects: None, takes sensor data via pios
 * Output objects: @ref GyroSensor @ref AccelSensor @ref MagSensor
 *
 * The module executes in its own thread.
 *
 */
#include <project_xflight.h>
#include <alarms.h>
#include <pios_sensors.h>
#include <homelocation.h>

#include <magsensor.h>
#include <accelsensor.h>
#include <gyrosensor.h>
#include <gyrostate.h>
#include <ekfbias.h>
#include <barosensor.h>
#include <flightstatus.h>

#include <attitudesettings.h>
#include <calibrationmags.h>
#include <accelgyrosettings.h>
#include <revosettings.h>
#include <mpugyroaccelsettings.h>
#include <calibrationfactory.h>

#include <mathmisc.h>
#include <taskinfo.h>
#include <pios_math.h>
#include <pios_constants.h>
#include <CoordinateConversions.h>
#include <pios_board_info.h>

#include "sensors.h"
#include "libuavoutils/settingutils.h"

#include "libmatrix/matrix3.h"
#include "libmatrix/orientation.h"
#include "libmatrix/quaternion.h"

#ifdef TRACE_GYRO_TIMING
traceLabel sensor_latency;
#endif /* TRACE_GYRO_TIMING */

// Private constants
#define STACK_SIZE_BYTES         0x600
#define TASK_PRIORITY            (tskIDLE_PRIORITY + 4)

#define MAX_SENSORS_PER_INSTANCE 2
#ifdef PIOS_INCLUDE_WDG
#define RELOAD_WDG()   PIOS_WDG_UpdateFlag(PIOS_WDG_SENSORS)
#define REGISTER_WDG() PIOS_WDG_RegisterFlag(PIOS_WDG_SENSORS)
#else
#define RELOAD_WDG()
#define REGISTER_WDG()
#endif

static TickType_t sensor_period_ticks = ((uint32_t)1000.0f / PIOS_SILA_RATE) / portTICK_RATE_MS;

// Interval in number of sample to recalculate temp bias
#define TEMP_CALIB_INTERVAL      30

// LPF
#define TEMP_DT_GYRO_ACCEL       (1.0f / PIOS_SILA_RATE)
#define TEMP_LPF_FC_GYRO_ACCEL   5.0f
static const float temp_alpha_gyro_accel = LPF_ALPHA(TEMP_DT_GYRO_ACCEL, TEMP_LPF_FC_GYRO_ACCEL);

// Interval in number of sample to recalculate temp bias
#define BARO_TEMP_CALIB_INTERVAL 10

// LPF
#define TEMP_DT_BARO             (1.0f / 120.0f)
#define TEMP_LPF_FC_BARO         5.0f
static const float temp_alpha_baro = TEMP_DT_BARO / (TEMP_DT_BARO + 1.0f / (2.0f * M_PI_F * TEMP_LPF_FC_BARO));


#define ZERO_ROT_ANGLE           0.00001f
// Private types
typedef struct {
    // used to accumulate all samples in a task iteration
    Vector3i32_s accum[2];
    int32_t temperature;
    uint32_t     count;
    // Timestamp of last sample
    uint32_t     timestamp;
} sensor_fetch_context;

#define MAX_SENSOR_DATA_SIZE (sizeof(PIOS_SENSORS_3Axis_SensorsWithTemp) + MAX_SENSORS_PER_INSTANCE * sizeof(Vector3i16_s))
typedef union {
    PIOS_SENSORS_3Axis_SensorsWithTemp sensorSample3Axis;
    PIOS_SENSORS_1Axis_SensorsWithTemp sensorSample1Axis;
} sensor_data;

#define PIOS_INSTRUMENT_MODULE
extern "C" {
#include <pios_instrumentation_helper.h>
}

PERF_DEFINE_COUNTER(counterAccelSamples);
PERF_DEFINE_COUNTER(counterAccelPeriod);
PERF_DEFINE_COUNTER(counterMagPeriod);
PERF_DEFINE_COUNTER(counterBaroPeriod);
PERF_DEFINE_COUNTER(counterSensorPeriod);
PERF_DEFINE_COUNTER(counterSensorResets);

// Private functions
static void SensorsTask(void *parameters);
static void settingsUpdatedCb(UAVObjEvent *objEv);
static void MPUGyroAccelSettingsUpdatedCb(UAVObjEvent *objEv);

static void accumulateSamples(sensor_fetch_context *sensor_context, sensor_data *sample);
static void processSamples3d(sensor_fetch_context *sensor_context, const PIOS_SENSORS_Instance *sensor);
static void processSamples1d(PIOS_SENSORS_1Axis_SensorsWithTemp *sample, const PIOS_SENSORS_Instance *sensor);

static void clearContext(sensor_fetch_context *sensor_context);

static void handleAccel(float *samples, float temperature, uint32_t timestamp);
static void handleGyro(float *samples, float temperature, uint32_t timestamp);
static void handleMag(float *samples, float temperature, uint32_t timestamp);
static void handleBaro(float sample, float temperature, uint32_t timestamp);

static void updateAccelTempBias(float temperature);
static void updateGyroTempBias(float temperature);
static void updateBaroTempBias(float temperature);

// Private variables
static sensor_data *source_data;
static xTaskHandle sensorsTaskHandle;
CalibrationMagsData cal;
AccelGyroSettingsData agcal;

// These values are initialized by settings but can be updated by the attitude algorithm

static float mag_bias[3] = { 0, 0, 0 };
static Matrix3f mag_transform_new;

static Matrix3f accel_transform;
static Vector3f accel_bias;
static Vector3f accel_temp_bias;

// Variables used to handle accel/gyro temperature bias
static volatile bool gyro_temp_calibrated  = false;
static volatile bool accel_temp_calibrated = false;

static float accel_temperature = NAN;
static float gyro_temperature  = NAN;
static float gyro_temp_bias[3] = { 0 };
static uint8_t accel_temp_calibration_count = 0;
static uint8_t gyro_temp_calibration_count  = 0;
static Matrix3f rotationMatrix;

// Variables used to handle baro temperature bias
static RevoSettingsBaroTempCorrectionPolynomialData baroCorrection;
static RevoSettingsBaroTempCorrectionExtentData baroCorrectionExtent;
static volatile bool baro_temp_correction_enabled;
static float baro_temp_bias   = 0;
static float baro_temperature = NAN;
static uint8_t baro_temp_calibration_count = 0;

static int8_t rotate = 0;

static float homeLocationBe[3];
static float magBe;
static float invMagBe;

/**
 * Initialise the module.  Called before the start function
 * \returns STATUS_OK on success
 */
int32_t SensorsInitialize(void)
{
#ifdef TRACE_GYRO_TIMING
    sensor_latency = xTraceOpenLabel("Sensor Latency");
#endif /* TRACE_GYRO_TIMING */
    source_data    = (sensor_data *)pios_malloc(MAX_SENSOR_DATA_SIZE);
    GyroSensorInitialize();
    GyroStateInitialize();
    EKFBiasInitialize();
    AccelSensorInitialize();
    MagSensorInitialize();
    BaroSensorInitialize();
    CalibrationMagsInitialize();
    RevoSettingsInitialize();
    AttitudeSettingsInitialize();
    AccelGyroSettingsInitialize();
    MPUGyroAccelSettingsInitialize();
    CalibrationFactoryInitialize();

    rotate = 0;

    RevoSettingsConnectCallback(&settingsUpdatedCb);
    CalibrationMagsConnectCallback(&settingsUpdatedCb);
    AttitudeSettingsConnectCallback(&settingsUpdatedCb);
    AccelGyroSettingsConnectCallback(&settingsUpdatedCb);
    MPUGyroAccelSettingsConnectCallback(&MPUGyroAccelSettingsUpdatedCb);

    return STATUS_OK;
}

/**
 * Start the task.  Expects all objects to be initialized by this point.
 * \returns STATUS_OK on success
 */
int32_t SensorsStart(void)
{
    // Start main task
    xTaskCreate(SensorsTask, "SILA", STACK_SIZE_BYTES / 4, NULL, TASK_PRIORITY, &sensorsTaskHandle);
    PIOS_TASK_MONITOR_RegisterTask(TASKINFO_RUNNING_SILA, sensorsTaskHandle);
    REGISTER_WDG();
    return STATUS_OK;
}

static InnerLoopIF *mInnerloopIF = NULL;

void SensorsRegisterInnerLoop(InnerLoopIF *innerloop)
{
    mInnerloopIF = innerloop;
}

/**
 * The sensor task.  This polls the gyros at 500 Hz and pumps that data to
 * stabilization and to the attitude loop
 *
 */

uint32_t sensor_dt_us;
static void SensorsTask(__attribute__((unused)) void *parameters)
{
    portTickType lastSysTime;
    sensor_fetch_context sensor_context;
    bool error = false;
    const PIOS_SENSORS_Instance *sensors_list = PIOS_SENSORS_GetList();
    PIOS_SENSORS_Instance *sensor;

    AlarmsClear(SYSTEMALARMS_ALARM_SENSORS);
    settingsUpdatedCb(NULL);

    // Performance counters
    PERF_INIT_COUNTER(counterAccelSamples, 0x53000001);
    PERF_INIT_COUNTER(counterAccelPeriod, 0x53000002);
    PERF_INIT_COUNTER(counterMagPeriod, 0x53000003);
    PERF_INIT_COUNTER(counterBaroPeriod, 0x53000004);
    PERF_INIT_COUNTER(counterSensorPeriod, 0x53000005);
    PERF_INIT_COUNTER(counterSensorResets, 0x53000006);

    // Test sensors
    bool sensors_test = true;
    uint8_t count     = 0;
    float factoryVectorPrimary[3]   = { 0 };
    float factoryVectorSecondary[3] = { 0 };
    LL_FOREACH((PIOS_SENSORS_Instance *)sensors_list, sensor) {
        if (sensor->type == PIOS_SENSORS_TYPE_3AXIS_GYRO_ACCEL) {
            sensors_test &= (PIOS_SENSORS_Test(sensor, factoryVectorPrimary, factoryVectorSecondary) == STATUS_OK);
        } else {
            sensors_test &= (PIOS_SENSORS_Test(sensor, NULL, NULL) == STATUS_OK);
        }
        count++;
    }


    PIOS_Assert(count);
    RELOAD_WDG();
    if (!sensors_test) {
        AlarmsSet(SYSTEMALARMS_ALARM_SENSORS, SYSTEMALARMS_ALARM_CRITICAL);
        while (1) {
            vTaskDelay(10);
        }
    }

    CalibrationFactoryData factoryCal;
    memset(&factoryCal, 0, sizeof(CalibrationFactoryData));
    factoryCal.Gyro.x  = factoryVectorPrimary[0];
    factoryCal.Gyro.y  = factoryVectorPrimary[1];
    factoryCal.Gyro.z  = factoryVectorPrimary[2];
    factoryCal.Accel.x = factoryVectorSecondary[0];
    factoryCal.Accel.y = factoryVectorSecondary[1];
    factoryCal.Accel.z = factoryVectorSecondary[2];
    CalibrationFactorySet(&factoryCal);


    // Main task loop
    lastSysTime = xTaskGetTickCount();
    uint32_t reset_counter = 0;

    while (1) {
        // TODO: add timeouts to the sensor reads and set an error if the fail
        if (error) {
            RELOAD_WDG();
            lastSysTime = xTaskGetTickCount();
            vTaskDelayUntil(&lastSysTime, sensor_period_ticks);
            AlarmsSet(SYSTEMALARMS_ALARM_SENSORS, SYSTEMALARMS_ALARM_CRITICAL);
            error = false;
        } else {
            AlarmsClear(SYSTEMALARMS_ALARM_SENSORS);
        }


        // reset the fetch context
        clearContext(&sensor_context);
        LL_FOREACH((PIOS_SENSORS_Instance *)sensors_list, sensor) {
            // we will wait on the sensor that's marked as primary( that means the sensor with higher sample rate)
            bool is_primary = (sensor->type & PIOS_SENSORS_TYPE_3AXIS_ACCEL);

            if (!sensor->driver->is_polled) {
                if (is_primary) {
                    PIOS_SENSORS_Wait(sensor);
#if defined(PIOS_ENABLE_DEBUG_PINS) && defined(DEBUG_GYRO_TIMING)
                    PIOS_DEBUG_PinLow(0);
#endif
#ifdef TRACE_GYRO_TIMING
                    vTraceTaskInstanceFinishDirect();
#endif /* TRACE_GYRO_TIMING */
                }
                const QueueHandle_t queue = PIOS_SENSORS_GetQueue(sensor);
                while (xQueueReceive(queue,
                                     (void *)source_data,
                                     (is_primary && !sensor_context.count) ? sensor_period_ticks : 0) == pdTRUE) {
                    accumulateSamples(&sensor_context, source_data);
                }
                if (sensor_context.count) {
                    processSamples3d(&sensor_context, sensor);
                    clearContext(&sensor_context);
                } else if (is_primary) {
                    PIOS_SENSOR_Reset(sensor);
                    reset_counter++;
                    PERF_TRACK_VALUE(counterSensorResets, reset_counter);
                    error = true;
                }
            } else {
                if (PIOS_SENSORS_Poll(sensor)) {
                    PIOS_SENSOR_Fetch(sensor, (void *)source_data, MAX_SENSORS_PER_INSTANCE);
                    if (sensor->type & PIOS_SENSORS_TYPE_3D) {
                        accumulateSamples(&sensor_context, source_data);
                        processSamples3d(&sensor_context, sensor);
                    } else {
                        processSamples1d(&source_data->sensorSample1Axis, sensor);
                    }
                    clearContext(&sensor_context);
                }
            }
        }
        PERF_MEASURE_PERIOD(counterSensorPeriod);
        RELOAD_WDG();
    }
}

static void clearContext(sensor_fetch_context *sensor_context)
{
    // clear the context once it has finished
    for (uint32_t i = 0; i < MAX_SENSORS_PER_INSTANCE; i++) {
        sensor_context->accum[i].x = 0;
        sensor_context->accum[i].y = 0;
        sensor_context->accum[i].z = 0;
    }
    sensor_context->temperature = 0;
    sensor_context->count = 0;
}

static void accumulateSamples(sensor_fetch_context *sensor_context, sensor_data *sample)
{
    for (uint32_t i = 0; (i < MAX_SENSORS_PER_INSTANCE) && (i < sample->sensorSample3Axis.count); i++) {
        sensor_context->accum[i].x += sample->sensorSample3Axis.sample[i].x;
        sensor_context->accum[i].y += sample->sensorSample3Axis.sample[i].y;
        sensor_context->accum[i].z += sample->sensorSample3Axis.sample[i].z;
    }
    sensor_context->temperature += sample->sensorSample3Axis.temperature;
    sensor_context->timestamp    = sample->sensorSample3Axis.timestamp;
    sensor_context->count++;
}

static void processSamples3d(sensor_fetch_context *sensor_context, const PIOS_SENSORS_Instance *sensor)
{
    float samples[3];
    float temperature;
    float scales[MAX_SENSORS_PER_INSTANCE];

    PIOS_SENSORS_GetScales(sensor, scales, MAX_SENSORS_PER_INSTANCE);
    float inv_count = 1.0f / (float)sensor_context->count;

    if ((sensor->type & PIOS_SENSORS_TYPE_3AXIS_ACCEL) ||
        (sensor->type == PIOS_SENSORS_TYPE_3AXIS_MAG)) {
        float t = inv_count * scales[0];
        samples[0]  = ((float)sensor_context->accum[0].x * t);
        samples[1]  = ((float)sensor_context->accum[0].y * t);
        samples[2]  = ((float)sensor_context->accum[0].z * t);
        temperature = (float)sensor_context->temperature * inv_count * 0.01f;
        if (sensor->type == PIOS_SENSORS_TYPE_3AXIS_MAG) {
            handleMag(samples, temperature, sensor_context->timestamp);
            PERF_MEASURE_PERIOD(counterMagPeriod);
            return;
        } else {
            PERF_TRACK_VALUE(counterAccelSamples, sensor_context->count);
            PERF_MEASURE_PERIOD(counterAccelPeriod);
            handleAccel(samples, temperature, sensor_context->timestamp);
        }
    }

    if (sensor->type & PIOS_SENSORS_TYPE_3AXIS_GYRO) {
        uint8_t index = 0;
        if (sensor->type == PIOS_SENSORS_TYPE_3AXIS_GYRO_ACCEL) {
            index = 1;
        }
        float t = inv_count * scales[index];
        samples[0]  = ((float)sensor_context->accum[index].x * t);
        samples[1]  = ((float)sensor_context->accum[index].y * t);
        samples[2]  = ((float)sensor_context->accum[index].z * t);
        temperature = (float)sensor_context->temperature * inv_count * 0.01f;
        handleGyro(samples, temperature, sensor_context->timestamp);
        return;
    }
}

static void processSamples1d(PIOS_SENSORS_1Axis_SensorsWithTemp *sample, const PIOS_SENSORS_Instance *sensor)
{
    switch (sensor->type) {
    case PIOS_SENSORS_TYPE_1AXIS_BARO:
        PERF_MEASURE_PERIOD(counterBaroPeriod);
        handleBaro(sample->sample,
                   sample->temperature,
                   sample->timestamp);
        return;

    default:
        PIOS_Assert(0);
    }
}

static void handleAccel(float *samples, float temperature, uint32_t timestamp)
{
    AccelSensorData accelSensorData;

    updateAccelTempBias(temperature);
    Vector3f corrected_accels;
    Vector3f sampled_accels(samples[0], samples[1], samples[2]);
    corrected_accels = accel_transform * (sampled_accels - accel_bias) - accel_temp_bias;

    corrected_accels.matrix_rotate(rotationMatrix);

    accelSensorData.x = corrected_accels.x;
    accelSensorData.y = corrected_accels.y;
    accelSensorData.z = corrected_accels.z;
    accelSensorData.temperature = temperature;
    accelSensorData.timestamp   = timestamp;
    AccelSensorSet(&accelSensorData);
}

static void handleGyro(float *samples, float temperature, uint32_t timestamp)
{
    GyroSensorData gyroSensorData;

    updateGyroTempBias(temperature);
    Vector3f gyros_out(samples[0] * agcal.gyro_scale.X - agcal.gyro_bias.X - gyro_temp_bias[0],
                       samples[1] * agcal.gyro_scale.Y - agcal.gyro_bias.Y - gyro_temp_bias[1],
                       samples[2] * agcal.gyro_scale.Z - agcal.gyro_bias.Z - gyro_temp_bias[2]);

    gyros_out.matrix_rotate(rotationMatrix);
    gyroSensorData.temperature = temperature;
    gyroSensorData.timestamp   = timestamp;
    gyroSensorData.x = gyros_out.x;
    gyroSensorData.y = gyros_out.y;
    gyroSensorData.z = gyros_out.z;

#ifdef TRACE_GYRO_TIMING
    vTracePrintF(sensor_latency, "%d", PIOS_DELAY_GetuS() - timestamp);
#endif /* TRACE_GYRO_TIMING */
    GyroSensorSet(&gyroSensorData);

    EKFBiasData EKFBias;
    EKFBiasGet(&EKFBias);

    GyroStateData t;
    t.x = gyroSensorData.x + EKFBias.gyro_bias.X;
    t.y = gyroSensorData.y + EKFBias.gyro_bias.Y;
    t.z = gyroSensorData.z + EKFBias.gyro_bias.Z;
    GyroStateSet(&t);

    if (mInnerloopIF) {
        mInnerloopIF->Update();
    }
}

static void handleMag(float *samples, float temperature, uint32_t timestamp)
{
    Vector3f mags((float)samples[0] - mag_bias[0],
                  (float)samples[1] - mag_bias[1],
                  (float)samples[2] - mag_bias[2]);
    Vector3f raw(samples[0],
                 samples[1],
                 samples[2]);

    mags.matrix_rotate(mag_transform_new);
    raw.matrix_rotate(rotationMatrix);

    MagSensorData mag;
    mag.raw.x = raw.x;
    mag.raw.y = raw.y;
    mag.raw.z = raw.z;
    mag.calibrated.x         = mags.x;
    mag.calibrated.y         = mags.y;
    mag.calibrated.z         = mags.z;
    mag.temperature          = temperature;
    mag.timestamp            = timestamp;
    mag.calibrated.magnitude = mags.length();
    mag.calibrated.error     = 100.0f * (mag.calibrated.magnitude - magBe) * invMagBe;
    mag.raw.magnitude        = raw.length();
    mag.raw.error            = 100.0f * (mag.raw.magnitude - magBe) * invMagBe;

    MagSensorSet(&mag);
}

static void handleBaro(float sample, float temperature, uint32_t timestamp)
{
    updateBaroTempBias(temperature);
    sample -= baro_temp_bias;

    float altitude = 44330.0f * (1.0f - powf((sample) / PIOS_CONST_MKS_STD_ATMOSPHERE_F, (1.0f / 5.255f)));

    if (!isnan(altitude)) {
        BaroSensorData data;
        data.Altitude    = altitude;
        data.Temperature = temperature;
        data.Pressure    = sample;
        data.timestamp   = timestamp;
        // Update the BasoSensor UAVObject
        BaroSensorSet(&data);
    }
}

static void updateAccelTempBias(float temperature)
{
    if (isnan(accel_temperature)) {
        accel_temperature = temperature;
    }
    accel_temperature = temp_alpha_gyro_accel * (temperature - accel_temperature) + accel_temperature;

    if ((accel_temp_calibrated) && !accel_temp_calibration_count) {
        accel_temp_calibration_count = TEMP_CALIB_INTERVAL;
        if (accel_temp_calibrated) {
            float ctemp = boundf(accel_temperature,
                                 agcal.temp_calibrated_extent.max,
                                 agcal.temp_calibrated_extent.min);

            accel_temp_bias.x = agcal.accel_temp_coeff.X * ctemp;
            accel_temp_bias.y = agcal.accel_temp_coeff.Y * ctemp;
            accel_temp_bias.z = agcal.accel_temp_coeff.Z * ctemp;
        }
    }
    accel_temp_calibration_count--;
}

static void updateGyroTempBias(float temperature)
{
    if (isnan(gyro_temperature)) {
        gyro_temperature = temperature;
    }

    gyro_temperature = temp_alpha_gyro_accel * (temperature - gyro_temperature) + gyro_temperature;

    if (gyro_temp_calibrated && !gyro_temp_calibration_count) {
        gyro_temp_calibration_count = TEMP_CALIB_INTERVAL;

        if (gyro_temp_calibrated) {
            float ctemp = boundf(gyro_temperature, agcal.temp_calibrated_extent.max, agcal.temp_calibrated_extent.min);
            gyro_temp_bias[0] = (agcal.gyro_temp_coeff.X + agcal.gyro_temp_coeff.X2 * ctemp) * ctemp;
            gyro_temp_bias[1] = (agcal.gyro_temp_coeff.Y + agcal.gyro_temp_coeff.Y2 * ctemp) * ctemp;
            gyro_temp_bias[2] = (agcal.gyro_temp_coeff.Z + agcal.gyro_temp_coeff.Z2 * ctemp) * ctemp;
        }
    }
    gyro_temp_calibration_count--;
}

static void updateBaroTempBias(float temperature)
{
    if (isnan(baro_temperature)) {
        baro_temperature = temperature;
    }

    baro_temperature = temp_alpha_baro * (temperature - baro_temperature) + baro_temperature;

    if (baro_temp_correction_enabled && !baro_temp_calibration_count) {
        baro_temp_calibration_count = BARO_TEMP_CALIB_INTERVAL;
        // pressure bias = A + B*t + C*t^2 + D * t^3
        // in case the temperature is outside of the calibrated range, uses the nearest extremes
        float ctemp = boundf(baro_temperature, baroCorrectionExtent.max, baroCorrectionExtent.min);
        baro_temp_bias = baroCorrection.a + ((baroCorrection.d * ctemp + baroCorrection.c) * ctemp + baroCorrection.b) * ctemp;
    }
    baro_temp_calibration_count--;
}


static void MPUGyroAccelSettingsUpdatedCb(__attribute__((unused)) UAVObjEvent *objEv)
{
    /* Configure pios sensor frequency */
    _pios_sila_rate     = getInnerLoopRate();

    sensor_period_ticks = ((uint32_t)1000.0f / PIOS_SILA_RATE) / portTICK_RATE_MS;

    const PIOS_SENSORS_Instance *sensors_list = PIOS_SENSORS_GetList();
    PIOS_SENSORS_Instance *sensor;

    LL_FOREACH((PIOS_SENSORS_Instance *)sensors_list, sensor) {
        if (sensor->driver->reconfigure && sensor->type == PIOS_SENSORS_TYPE_3AXIS_GYRO_ACCEL) {
            sensor->driver->reconfigure(sensor->context);
        }
    }
}

static void settingsUpdatedCb(__attribute__((unused)) UAVObjEvent *objEv)
{
    CalibrationMagsGet(&cal);
    AccelGyroSettingsGet(&agcal);
    mag_bias[0] = cal.OnboardBias.X;
    mag_bias[1] = cal.OnboardBias.Y;
    mag_bias[2] = cal.OnboardBias.Z;

    accel_temp_calibrated = (agcal.temp_calibrated_extent.max - agcal.temp_calibrated_extent.min > .1f) &&
                            (fabsf(agcal.accel_temp_coeff.X) > 1e-9f || fabsf(agcal.accel_temp_coeff.Y) > 1e-9f || fabsf(agcal.accel_temp_coeff.Z) > 1e-9f);

    gyro_temp_calibrated  = (agcal.temp_calibrated_extent.max - agcal.temp_calibrated_extent.min > .1f) &&
                            (fabsf(agcal.gyro_temp_coeff.X) > 1e-9f || fabsf(agcal.gyro_temp_coeff.Y) > 1e-9f ||
                            fabsf(agcal.gyro_temp_coeff.Z) > 1e-9f || fabsf(agcal.gyro_temp_coeff.Z2) > 1e-9f);

    Vector3f va(agcal.accel_scale.X, agcal.accel_crossaxis_scale.XY, agcal.accel_crossaxis_scale.XZ);
    Vector3f vb(agcal.accel_crossaxis_scale.YX, agcal.accel_scale.Y, agcal.accel_crossaxis_scale.YZ);
    Vector3f vc(agcal.accel_crossaxis_scale.ZX, agcal.accel_crossaxis_scale.ZY, agcal.accel_scale.Z);
    Matrix3f accel_transform_new(va, vb, vc);
    accel_transform = accel_transform_new;
    accel_bias.x    = agcal.accel_bias.X;
    accel_bias.y    = agcal.accel_bias.Y;
    accel_bias.z    = agcal.accel_bias.Z;

    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);

    // Indicates not to expend cycles on rotation
    if (fabsf(attitudeSettings.BoardRotation.Roll) < ZERO_ROT_ANGLE
        && fabsf(attitudeSettings.BoardRotation.Pitch) < ZERO_ROT_ANGLE &&
        fabsf(attitudeSettings.BoardRotation.Yaw) < ZERO_ROT_ANGLE) {
        rotate = 0;
    } else {
        rotate = 1;
    }

    Quaternion boardRotation;
    boardRotation.from_euler(attitudeSettings.BoardRotation.Roll,
                             attitudeSettings.BoardRotation.Pitch,
                             attitudeSettings.BoardRotation.Yaw);

    if (fabsf(attitudeSettings.BoardLevelTrim.Roll) > ZERO_ROT_ANGLE ||
        fabsf(attitudeSettings.BoardLevelTrim.Pitch) > ZERO_ROT_ANGLE) {
        Quaternion levelTrim;
        levelTrim.from_euler(attitudeSettings.BoardLevelTrim.Roll, attitudeSettings.BoardLevelTrim.Pitch, 0.0f);
        Quaternion sumBoardAndTrim = boardRotation * levelTrim;
        sumBoardAndTrim.rotation_matrix(rotationMatrix);
        rotate = 1;
    } else {
        boardRotation.rotation_matrix(rotationMatrix);
    }

    Vector3f ma(cal.OnboardTransform.r0c0, cal.OnboardTransform.r1c0, cal.OnboardTransform.r2c0);
    Vector3f mb(cal.OnboardTransform.r0c1, cal.OnboardTransform.r1c1, cal.OnboardTransform.r2c1);
    Vector3f mc(cal.OnboardTransform.r0c2, cal.OnboardTransform.r1c2, cal.OnboardTransform.r2c2);
    mag_transform_new  = Matrix3f(ma, mb, mc);
    mag_transform_new *= rotationMatrix;

    RevoSettingsBaroTempCorrectionPolynomialGet(&baroCorrection);
    RevoSettingsBaroTempCorrectionExtentGet(&baroCorrectionExtent);
    baro_temp_correction_enabled =
        (baroCorrectionExtent.max - baroCorrectionExtent.min > 0.1f &&
         (fabsf(baroCorrection.a) > 1e-9f ||
          fabsf(baroCorrection.b) > 1e-9f ||
          fabsf(baroCorrection.c) > 1e-9f ||
          fabsf(baroCorrection.d) > 1e-9f));

    HomeLocationBeGet(homeLocationBe);
    magBe    = vector_lengthf(homeLocationBe, 3);
    invMagBe = 1.0f / magBe;
}
/**
 * @}
 * @}
 */
