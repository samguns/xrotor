/**
 ******************************************************************************
 * @addtogroup OpenPilotModules OpenPilot Modules
 * @{
 * @addtogroup StabilizationModule Stabilization Module
 * @brief Stabilization PID loops in an airframe type independent manner
 * @note This object updates the @ref ActuatorDesired "Actuator Desired" based on the
 * PID loops on the @ref AttitudeDesired "Attitude Desired" and @ref AttitudeState "Attitude State"
 * @{
 *
 * @file       innerloop.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2014.
 * @brief      Attitude stabilization module.
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
extern "C" {
#include <project_xflight.h>
#include <alarms.h>
#include <pid.h>
#include <sin_lookup.h>
#include <callbackinfo.h>
#include <ratedesired.h>
#include <actuatordesired.h>
#include <gyrostate.h>
#include <airspeedstate.h>
#include <stabilizationstatus.h>
#include <flightstatus.h>
#include <manualcontrolcommand.h>
#include <stabilizationbank.h>
#include <stabilizationdesired.h>
#include <actuatordesired.h>
#include <systemsettings.h>
#include <flightmodesettings.h>
#include <stabilizationsettings.h>
#include <stabilizationdesired.h>
#include <stabilizationstatus.h>
#include <stabilizationbank.h>
#include <stabilizationsettingsbank1.h>
#include <stabilizationsettingsbank2.h>
#include <stabilizationsettingsbank3.h>
#include <cruisecontrolfactor.h>

#include <virtualflybar.h>
#include <sanitycheck.h>
}

#include "innerloop.h"
#include "actuatorif.h"

// Private constants
// pointer to a singleton instance
InnerLoopImpl *InnerLoopImpl::p_inst = 0;

#define CALLBACK_PRIORITY   CALLBACK_PRIORITY_CRITICAL

#define UPDATE_EXPECTED     (1.0f / PIOS_SILA_RATE)
#define UPDATE_MIN          1.0e-6f
#define UPDATE_MAX          1.0f
#define UPDATE_ALPHA        1.0e-2f
#define OUTERLOOP_SKIPCOUNT 4

// Private variables
// static DelayedCallbackInfo *callbackHandle;

// Private functions
static void AirSpeedUpdatedCb(__attribute__((unused)) UAVObjEvent * ev);
static void SettingsUpdatedCb(UAVObjEvent *ev);
static void SettingsBankUpdatedCb(UAVObjEvent *ev);
static void RateDesiredUpdatedCb(UAVObjEvent *ev);

InnerLoopImpl::InnerLoopImpl()
    : speedScaleFactor(1.0f), gyro_filtered
{
    0, 0, 0
}, axis_lock_accum { 0, 0, 0 }, previous_mode { 255, 255, 255, 255 }, frame_is_multirotor(true), actuator_ptr(NULL)
{}
void InnerLoopImpl::Initialize()
{
#ifdef TRACE_INNERLOOP
    innerloop_start        = xTraceOpenLabel("Inner-loop start");
    innerloop_duration     = xTraceOpenLabel("Inner-loop duration");
    innerloop_end_alarms   = xTraceOpenLabel("Inner-loop end of alarm check");
    innerloop_start_axes   = xTraceOpenLabel("Inner-loop start of axes loop");
    innerloop_end_axes     = xTraceOpenLabel("Inner-loop end of axes loop");
    innerloop_end_actuator = xTraceOpenLabel("Inner-loop end of actuator set");
    innerloop_end_piro     = xTraceOpenLabel("Inner-loop end of piro comp");
#endif /* TRACE_INNERLOOP */
    RateDesiredInitialize();
    ActuatorDesiredInitialize();
    GyroStateInitialize();
    StabilizationStatusInitialize();
    FlightStatusInitialize();
    ManualControlCommandInitialize();
    StabilizationDesiredInitialize();
    ActuatorDesiredInitialize();
    SystemSettingsInitialize();
    AirspeedStateInitialize();
    CruiseControlFactorInitialize();
    StabilizationBankInitialize();
    StabilizationDesiredInitialize();
    StabilizationSettingsInitialize();
    StabilizationStatusInitialize();
    StabilizationBankInitialize();
    StabilizationSettingsBank1Initialize();
    StabilizationSettingsBank2Initialize();
    StabilizationSettingsBank3Initialize();
    PIOS_DELTATIME_Init(&timeval, UPDATE_EXPECTED, UPDATE_MIN, UPDATE_MAX, UPDATE_ALPHA);

    frame_is_multirotor = (GetCurrentFrameType() == FRAME_TYPE_MULTIROTOR);

    StabilizationBankConnectCallback(SettingsBankUpdatedCb);
    AirspeedStateConnectCallback(AirSpeedUpdatedCb);
    RateDesiredConnectCallback(RateDesiredUpdatedCb);
    StabilizationSettingsConnectCallback(SettingsUpdatedCb);

    SettingsUpdated();
    SettingsBankUpdated();

    pid_zero(&stabSettings.innerPids[0]);
    pid_zero(&stabSettings.innerPids[1]);
    pid_zero(&stabSettings.innerPids[2]);
}

float InnerLoopImpl::get_pid_scale_source_value()
{
    float value;

    switch (stabSettings.stabBank.ThrustPIDScaleSource) {
    case STABILIZATIONBANK_THRUSTPIDSCALESOURCE_MANUALCONTROLTHROTTLE:
        ManualControlCommandThrottleGet(&value);
        break;
    case STABILIZATIONBANK_THRUSTPIDSCALESOURCE_STABILIZATIONDESIREDTHRUST:
        StabilizationDesiredThrustGet(&value);
        break;
    case STABILIZATIONBANK_THRUSTPIDSCALESOURCE_ACTUATORDESIREDTHRUST:
        ActuatorDesiredThrustGet(&value);
        break;
    default:
        ActuatorDesiredThrustGet(&value);
        break;
    }

    if (value < 0) {
        value = 0.0f;
    }

    return value;
}

float InnerLoopImpl::pid_curve_value(const pid_curve_scaler *scaler)
{
    float y = y_on_curve(scaler->x, scaler->points, sizeof(scaler->points) / sizeof(scaler->points[0]));

    return 1.0f + (IS_REAL(y) ? y : 0.0f);
}

pid_scaler InnerLoopImpl::create_pid_scaler(int axis)
{
    pid_scaler scaler;

    // Always scaled with the this.
    scaler.p = scaler.i = scaler.d = speedScaleFactor;

    if (stabSettings.thrust_pid_scaling_enabled[axis][0]
        || stabSettings.thrust_pid_scaling_enabled[axis][1]
        || stabSettings.thrust_pid_scaling_enabled[axis][2]) {
        const pid_curve_scaler curve_scaler = {
            .x      = get_pid_scale_source_value(),
            .points = {
                { 0.00f, stabSettings.stabBank.ThrustPIDScaleCurve[0] },
                { 0.25f, stabSettings.stabBank.ThrustPIDScaleCurve[1] },
                { 0.50f, stabSettings.stabBank.ThrustPIDScaleCurve[2] },
                { 0.75f, stabSettings.stabBank.ThrustPIDScaleCurve[3] },
                { 1.00f, stabSettings.stabBank.ThrustPIDScaleCurve[4] }
            }
        };

        float curve_value = pid_curve_value(&curve_scaler);

        if (stabSettings.thrust_pid_scaling_enabled[axis][0]) {
            scaler.p *= curve_value;
        }
        if (stabSettings.thrust_pid_scaling_enabled[axis][1]) {
            scaler.i *= curve_value;
        }
        if (stabSettings.thrust_pid_scaling_enabled[axis][2]) {
            scaler.d *= curve_value;
        }
    }

    return scaler;
}

/**
 * WARNING! This callback executes with critical flight control priority every
 * time a gyroscope update happens do NOT put any time consuming calculations
 * in this loop unless they really have to execute with every gyro update
 */
void InnerLoopImpl::Update()
{
#ifdef TRACE_INNERLOOP
    uint32_t start_time = PIOS_DELAY_GetuS();
    vTraceUserEvent(innerloop_start);
#endif /* TRACE_INNERLOOP */

    GyroStateData gyroState;
    GyroStateGet(&gyroState);
    gyro_filtered[0] = gyro_filtered[0] * stabSettings.gyro_alpha + gyroState.x * (1 - stabSettings.gyro_alpha);
    gyro_filtered[1] = gyro_filtered[1] * stabSettings.gyro_alpha + gyroState.y * (1 - stabSettings.gyro_alpha);
    gyro_filtered[2] = gyro_filtered[2] * stabSettings.gyro_alpha + gyroState.z * (1 - stabSettings.gyro_alpha);

    {
        bool warn  = false;
        bool error = false;
        bool crit  = false;

        // check if outer loop keeps executing
        if (stabSettings.monitor.rateupdates > -64) {
            stabSettings.monitor.rateupdates--;
        }
        if (stabSettings.monitor.rateupdates < -(2 * OUTERLOOP_SKIPCOUNT)) {
            // warning if rate loop skipped more than 2 execution
            warn = true;
        }
        if (stabSettings.monitor.rateupdates < -(4 * OUTERLOOP_SKIPCOUNT)) {
            // critical if rate loop skipped more than 4 executions
            crit = true;
        }

        if (crit) {
            AlarmsSet(SYSTEMALARMS_ALARM_STABILIZATION, SYSTEMALARMS_ALARM_CRITICAL);
        } else if (error) {
            AlarmsSet(SYSTEMALARMS_ALARM_STABILIZATION, SYSTEMALARMS_ALARM_ERROR);
        } else if (warn) {
            AlarmsSet(SYSTEMALARMS_ALARM_STABILIZATION, SYSTEMALARMS_ALARM_WARNING);
        } else {
            AlarmsClear(SYSTEMALARMS_ALARM_STABILIZATION);
        }
#ifdef TRACE_INNERLOOP
        vTracePrintF(innerloop_end_alarms, "%d", PIOS_DELAY_GetuS() - start_time);
#endif /* TRACE_INNERLOOP */
    }

    ActuatorDesiredData actuator;
    StabilizationStatusInnerLoopData enabled;
    FlightStatusControlChainData cchain;

    ActuatorDesiredGet(&actuator);
    StabilizationStatusInnerLoopGet(&enabled);
    FlightStatusControlChainGet(&cchain);
    float *rate = &stabSettings.rateDesired.Roll;
    float *actuatorDesiredAxis = &actuator.Roll;
    int t;
    float dT;
    bool multirotor = (GetCurrentFrameType() == FRAME_TYPE_MULTIROTOR); // check if frame is a multirotor
    dT = PIOS_DELTATIME_GetAverageSeconds(&timeval);

    StabilizationStatusOuterLoopData outerLoop;
    StabilizationStatusOuterLoopGet(&outerLoop);
    bool allowPiroComp = true;

#ifdef TRACE_INNERLOOP
    vTracePrintF(innerloop_start_axes, "%d", PIOS_DELAY_GetuS() - start_time);
#endif /* TRACE_INNERLOOP */
    for (t = 0; t < AXES; t++) {
        bool reinit = (StabilizationStatusInnerLoopToArray(enabled)[t] != previous_mode[t]);
        previous_mode[t] = StabilizationStatusInnerLoopToArray(enabled)[t];

        if (t < STABILIZATIONSTATUS_INNERLOOP_THRUST) {
            if (reinit) {
                stabSettings.innerPids[t].iAccumulator = 0;
                if (frame_is_multirotor) {
                    // Multirotors should dump axis lock accumulators when unarmed or throttle is low.
                    // Fixed wing or ground vehicles can fly/drive with low throttle.
                    axis_lock_accum[t] = 0;
                }
            }
            // Any self leveling on roll or pitch must prevent pirouette compensation
            if (t < STABILIZATIONSTATUS_INNERLOOP_YAW && StabilizationStatusOuterLoopToArray(outerLoop)[t] != STABILIZATIONSTATUS_OUTERLOOP_DIRECT) {
                allowPiroComp = false;
            }
            switch (StabilizationStatusInnerLoopToArray(enabled)[t]) {
            case STABILIZATIONSTATUS_INNERLOOP_VIRTUALFLYBAR:
                stabilization_virtual_flybar(gyro_filtered[t], rate[t], &actuatorDesiredAxis[t], dT, reinit, t, &stabSettings.settings);
                break;
            case STABILIZATIONSTATUS_INNERLOOP_AXISLOCK:
                if (fabsf(rate[t]) > stabSettings.settings.MaxAxisLockRate) {
                    // While getting strong commands act like rate mode
                    axis_lock_accum[t] = 0;
                } else {
                    // For weaker commands or no command simply attitude lock (almost) on no gyro change
                    axis_lock_accum[t] += (rate[t] - gyro_filtered[t]) * dT;
                    axis_lock_accum[t]  = boundf(axis_lock_accum[t], -stabSettings.settings.MaxAxisLock, stabSettings.settings.MaxAxisLock);
                    rate[t] = axis_lock_accum[t] * stabSettings.settings.AxisLockKp;
                }
            // IMPORTANT: deliberately no "break;" here, execution continues with regular RATE control loop to avoid code duplication!
            // keep order as it is, RATE must follow!
            case STABILIZATIONSTATUS_INNERLOOP_RATE:
            {
                // limit rate to maximum configured limits (once here instead of 5 times in outer loop)
                rate[t] = boundf(rate[t],
                                 -StabilizationBankMaximumRateToArray(stabSettings.stabBank.MaximumRate)[t],
                                 StabilizationBankMaximumRateToArray(stabSettings.stabBank.MaximumRate)[t]
                                 );
                pid_scaler scaler = create_pid_scaler(t);
                actuatorDesiredAxis[t] = pid_apply_setpoint(&stabSettings.innerPids[t], &scaler, rate[t], gyro_filtered[t], dT);
            }
            break;
            case STABILIZATIONSTATUS_INNERLOOP_ACRO:
            {
                float stickinput[3];
                stickinput[0] = boundf(rate[0] / stabSettings.stabBank.ManualRate.Roll, -1.0f, 1.0f);
                stickinput[1] = boundf(rate[1] / stabSettings.stabBank.ManualRate.Pitch, -1.0f, 1.0f);
                stickinput[2] = boundf(rate[2] / stabSettings.stabBank.ManualRate.Yaw, -1.0f, 1.0f);
                rate[t] = boundf(rate[t],
                                 -StabilizationBankMaximumRateToArray(stabSettings.stabBank.MaximumRate)[t],
                                 StabilizationBankMaximumRateToArray(stabSettings.stabBank.MaximumRate)[t]
                                 );
                pid_scaler ascaler = create_pid_scaler(t);
                ascaler.i *= boundf(1.0f - (1.5f * fabsf(stickinput[t])), 0.0f, 1.0f); // this prevents Integral from getting too high while controlled manually
                float arate  = pid_apply_setpoint(&stabSettings.innerPids[t], &ascaler, rate[t], gyro_filtered[t], dT);
                float factor = fabsf(stickinput[t]) * stabSettings.stabBank.AcroInsanityFactor;
                actuatorDesiredAxis[t] = factor * stickinput[t] + (1.0f - factor) * arate;
            }
            break;
            case STABILIZATIONSTATUS_INNERLOOP_DIRECT:
            default:
                actuatorDesiredAxis[t] = rate[t];
                break;
            }
        } else {
            switch (StabilizationStatusInnerLoopToArray(enabled)[t]) {
            case STABILIZATIONSTATUS_INNERLOOP_CRUISECONTROL:
                actuatorDesiredAxis[t] = cruisecontrol_apply_factor(rate[t]);
                break;
            case STABILIZATIONSTATUS_INNERLOOP_DIRECT:
            default:
                actuatorDesiredAxis[t] = rate[t];
                break;
            }
        }

        if (!multirotor) {
            // we only need to clamp the desired axis to a sane range if the frame is not a multirotor type
            // we don't want to do any clamping until after the motors are calculated and scaled.
            // need to figure out what to do with a tricopter tail servo.
            actuatorDesiredAxis[t] = boundf(actuatorDesiredAxis[t], -1.0f, 1.0f);
        }
    }
#ifdef TRACE_INNERLOOP
    vTracePrintF(innerloop_end_axes, "%d", PIOS_DELAY_GetuS() - start_time);
#endif /* TRACE_INNERLOOP */

    actuator.UpdateTime = dT * 1000;

    if (cchain.Stabilization == FLIGHTSTATUS_CONTROLCHAIN_TRUE) {
        ActuatorDesiredSet(&actuator);
        if (actuator_ptr) {
            actuator_ptr->Update();
        }
    } else {
        // Force all axes to reinitialize when engaged
        for (t = 0; t < AXES; t++) {
            previous_mode[t] = 255;
        }
    }
#ifdef TRACE_INNERLOOP
    vTracePrintF(innerloop_end_actuator, "%d", PIOS_DELAY_GetuS() - start_time);
#endif /* TRACE_INNERLOOP */

    if (allowPiroComp && stabSettings.stabBank.EnablePiroComp == STABILIZATIONBANK_ENABLEPIROCOMP_TRUE && stabSettings.innerPids[0].iLim > 1e-3f && stabSettings.innerPids[1].iLim > 1e-3f) {
        // attempted piro compensation - rotate pitch and yaw integrals (experimental)
        float angleYaw = DEG2RAD(gyro_filtered[2] * dT);
        float sinYaw   = sinf(angleYaw);
        float cosYaw   = cosf(angleYaw);
        float rollAcc  = stabSettings.innerPids[0].iAccumulator / stabSettings.innerPids[0].iLim;
        float pitchAcc = stabSettings.innerPids[1].iAccumulator / stabSettings.innerPids[1].iLim;
        stabSettings.innerPids[0].iAccumulator = stabSettings.innerPids[0].iLim * (cosYaw * rollAcc + sinYaw * pitchAcc);
        stabSettings.innerPids[1].iAccumulator = stabSettings.innerPids[1].iLim * (cosYaw * pitchAcc - sinYaw * rollAcc);
    }
#ifdef TRACE_INNERLOOP
    vTracePrintF(innerloop_end_piro, "%d", PIOS_DELAY_GetuS() - start_time);
#endif /* TRACE_INNERLOOP */

    {
        FlightStatusArmedOptions armed;
        FlightStatusArmedGet(&armed);
        float throttleDesired;
        ManualControlCommandThrottleGet(&throttleDesired);
        if (armed != FLIGHTSTATUS_ARMED_ARMED ||
            ((stabSettings.settings.LowThrottleZeroIntegral == STABILIZATIONSETTINGS_LOWTHROTTLEZEROINTEGRAL_TRUE) && throttleDesired < 0)) {
            // Force all axes to reinitialize when engaged
            for (t = 0; t < AXES; t++) {
                previous_mode[t] = 255;
            }
        }
    }
#ifdef TRACE_INNERLOOP
    vTracePrintF(innerloop_duration, "%d", PIOS_DELAY_GetuS() - start_time);
#endif /* TRACE_INNERLOOP */
}

void InnerLoopImpl::AirSpeedUpdated(void)
{
    // Scale PID coefficients based on current airspeed estimation - needed for fixed wing planes
    AirspeedStateData airspeedState;

    AirspeedStateGet(&airspeedState);
    if (stabSettings.settings.ScaleToAirspeed < 0.1f || airspeedState.CalibratedAirspeed < 0.1f) {
        // feature has been turned off
        speedScaleFactor = 1.0f;
    } else {
        // scale the factor to be 1.0 at the specified airspeed (for example 10m/s) but scaled by 1/speed^2
        speedScaleFactor = boundf((stabSettings.settings.ScaleToAirspeed * stabSettings.settings.ScaleToAirspeed) / (airspeedState.CalibratedAirspeed * airspeedState.CalibratedAirspeed),
                                  stabSettings.settings.ScaleToAirspeedLimits.Min,
                                  stabSettings.settings.ScaleToAirspeedLimits.Max);
    }
}

static void AirSpeedUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    InnerLoopImpl::instance()->AirSpeedUpdated();
}

static void SettingsBankUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    InnerLoopImpl::instance()->SettingsBankUpdated();
}

bool InnerLoopImpl::use_tps_for_roll()
{
    uint8_t axes = stabSettings.stabBank.ThrustPIDScaleAxes;

    return axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLLPITCHYAW ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLLPITCH ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLLYAW ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLL;
}

bool InnerLoopImpl::use_tps_for_pitch()
{
    uint8_t axes = stabSettings.stabBank.ThrustPIDScaleAxes;

    return axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLLPITCHYAW ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLLPITCH ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_PITCHYAW ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_PITCH;
}

bool InnerLoopImpl::use_tps_for_yaw()
{
    uint8_t axes = stabSettings.stabBank.ThrustPIDScaleAxes;

    return axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLLPITCHYAW ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_ROLLYAW ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_PITCHYAW ||
           axes == STABILIZATIONBANK_THRUSTPIDSCALEAXES_YAW;
}

bool InnerLoopImpl::use_tps_for_p()
{
    uint8_t target = stabSettings.stabBank.ThrustPIDScaleTarget;

    return target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_PID ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_PI ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_PD ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_P;
}

bool InnerLoopImpl::use_tps_for_i()
{
    uint8_t target = stabSettings.stabBank.ThrustPIDScaleTarget;

    return target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_PID ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_PI ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_ID ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_I;
}

bool InnerLoopImpl::use_tps_for_d()
{
    uint8_t target = stabSettings.stabBank.ThrustPIDScaleTarget;

    return target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_PID ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_PD ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_ID ||
           target == STABILIZATIONBANK_THRUSTPIDSCALETARGET_D;
}

void InnerLoopImpl::SettingsBankUpdated(void)
{
    StabilizationBankGet(&stabSettings.stabBank);

    // Set the roll rate PID constants
    pid_configure(&stabSettings.innerPids[0], stabSettings.stabBank.RollRatePID.Kp,
                  stabSettings.stabBank.RollRatePID.Ki,
                  stabSettings.stabBank.RollRatePID.Kd,
                  stabSettings.stabBank.RollRatePID.ILimit);

    // Set the pitch rate PID constants
    pid_configure(&stabSettings.innerPids[1], stabSettings.stabBank.PitchRatePID.Kp,
                  stabSettings.stabBank.PitchRatePID.Ki,
                  stabSettings.stabBank.PitchRatePID.Kd,
                  stabSettings.stabBank.PitchRatePID.ILimit);

    // Set the yaw rate PID constants
    pid_configure(&stabSettings.innerPids[2], stabSettings.stabBank.YawRatePID.Kp,
                  stabSettings.stabBank.YawRatePID.Ki,
                  stabSettings.stabBank.YawRatePID.Kd,
                  stabSettings.stabBank.YawRatePID.ILimit);

    bool tps_for_axis[3] = {
        use_tps_for_roll(),
        use_tps_for_pitch(),
        use_tps_for_yaw()
    };
    bool tps_for_pid[3] = {
        use_tps_for_p(),
        use_tps_for_i(),
        use_tps_for_d()
    };
    for (int axis = 0; axis < 3; axis++) {
        for (int pid = 0; pid < 3; pid++) {
            stabSettings.thrust_pid_scaling_enabled[axis][pid] = stabSettings.stabBank.EnableThrustPIDScaling
                                                                 && tps_for_axis[axis]
                                                                 && tps_for_pid[pid];
        }
    }
}

void InnerLoopImpl::SettingsUpdated(void)
{
    // needs no mutex, as long as eventdispatcher and Stabilization are both TASK_PRIORITY_CRITICAL
    StabilizationSettingsGet(&stabSettings.settings);

    // Set up the derivative term
    pid_configure_derivative(stabSettings.settings.DerivativeCutoff, stabSettings.settings.DerivativeGamma);

    // The dT has some jitter iteration to iteration that we don't want to
    // make thie result unpredictable.  Still, it's nicer to specify the constant
    // based on a time (in ms) rather than a fixed multiplier.  The error between
    // update rates on OP (~300 Hz) and CC (~475 Hz) is negligible for this
    // calculation
    const float fakeDt = 0.0025f;
    if (stabSettings.settings.GyroTau < 0.0001f) {
        stabSettings.gyro_alpha = 0; // not trusting this to resolve to 0
    } else {
        stabSettings.gyro_alpha = expf(-fakeDt / stabSettings.settings.GyroTau);
    }

    stabSettings.cruiseControl.min_thrust = (float)stabSettings.settings.CruiseControlMinThrust / 100.0f;
    stabSettings.cruiseControl.max_thrust = (float)stabSettings.settings.CruiseControlMaxThrust / 100.0f;
}

static void SettingsUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    InnerLoopImpl::instance()->SettingsUpdated();
}

void InnerLoopImpl::RateDesiredUpdated(void)
{
    RateDesiredGet(&stabSettings.rateDesired);
    stabSettings.monitor.rateupdates = 0;
}

static void RateDesiredUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    InnerLoopImpl::instance()->RateDesiredUpdated();
}


// assumes 1.0 <= factor <= 100.0
// a factor of less than 1.0 could make it return a value less than stabSettings.cruiseControl.min_thrust
// CP helis need to have min_thrust=-1
//
// multicopters need to have min_thrust=0.05 or so
// values below that will not be subject to max / min limiting
// that means thrust can be less than min
// that means multicopter motors stop spinning at low stick
float InnerLoopImpl::cruisecontrol_apply_factor(float thrust)
{
    if (stabSettings.settings.CruiseControlMaxPowerFactor > 0.0001f) {
        // don't touch thrust if it's less than min_thrust
        // without that	 test, quadcopter props will spin up
        // to min thrust even at zero throttle stick
        // if Cruise Control is enabled on this flight switch position
        if (thrust > stabSettings.cruiseControl.min_thrust) {
            CruiseControlFactorData cruiseControlFactor;
            CruiseControlFactorGet(&cruiseControlFactor);
            thrust = boundf(thrust * cruiseControlFactor.factor, stabSettings.cruiseControl.min_thrust, stabSettings.cruiseControl.max_thrust);
        }
    }
    return thrust;
}


/**
 * @}
 * @}
 */
