/**
 ******************************************************************************
 *
 * @file       calibration.cpp
 * @author     Alex Beck  Copyright (C) 2015.
 * @brief      Calibration module
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#include "project_xflight.h"
#include "alarms.h"
#include <systemalarms.h>

#include <callbackinfo.h>

// Data objects
#include "calibrationcontrol.h"
#include "calibrationstatus.h"
#include "homelocation.h"
#include "flightstatus.h"
#include <gyrosensor.h>
#include <accelsensor.h>
#include <magsensor.h>
#include <barosensor.h>
#include <airspeedsensor.h>
#include <gpspositionsensor.h>
#include <gpsvelocitysensor.h>
#include <auxmagsensor.h>
#include <calibrationmags.h>
#include <attitudestate.h>
#include <attitudesettings.h>
#include <objectpersistence.h>
#include "accelgyrosettings.h"
#include "calibrationtrigger.h"
#include "mpugyroaccelsettings.h"
#include "calibrationfactory.h"
#include "receiveractivity.h"
#include "maintenancemodesettings.h"
#include "maintenancemodeinfo.h"
#include "flightmodesettings.h"
#include "manualcontrolsettings.h"
#include "manualcontrolcommand.h"
#include "levelcalibrationfsm.h"
#include "gyrobiascalibrationfsm.h"
#include "magcalibrationfsm.h"
#include "accelcalibrationfsm.h"
#include "txcalibrationfsm.h"

#define CALLBACK_PRIORITY       CALLBACK_PRIORITY_LOW
#define CBTASK_PRIORITY         CALLBACK_TASK_FLIGHTCONTROL

#define IDLE_UPDATE_RATE_MS     1000
#define MAGACCEL_UPDATE_RATE_MS 100
#define STACK_SIZE_BYTES        2 * 2048


static DelayedCallbackInfo *calibrationTimerTaskCBInfo;
static DelayedCallbackInfo *calibrationActivateTaskCBInfo;
static void calibrationTimerTask(void);
static void calibrationActivateTask(void);
static void CalibrationControlUpdated(__attribute__((unused)) UAVObjEvent * ev);
static void CalibrationTriggerUpdated(__attribute__((unused)) UAVObjEvent * ev);
int32_t CalibrationInitialize(void);
int32_t CalibrationStart(void);
static int32_t CalibrationActivate(void);
static CalibrationFSM *activeCalibrationFSM = 0;
static uint32_t updatePeriod = IDLE_UPDATE_RATE_MS;

static void clearHomeLocation(void);
static void setCaliStatusError(CalibrationStatusErrorOptions errorOption);
static bool isDisarmed();
static void sensorUpdatedCb(UAVObjEvent *ev);
static void attitudeStateUpdatedCb(UAVObjEvent *ev);
static void homeLocationUpdatedCb(UAVObjEvent *ev);
static void receiverUpdatedCb(UAVObjEvent *ev);

static void clearHomeLocation(void)
{
    HomeLocationData home;

    HomeLocationGet(&home);
    home.Set = HOMELOCATION_SET_FALSE;
    home.Latitude  = 0;
    home.Longitude = 0;
    home.Altitude  = 0;
    home.Be[0]     = 0;
    home.Be[1]     = 0;
    home.Be[2]     = 0;
    home.g_e = 9.81f;
    HomeLocationSet(&home);
}

static void setCaliStatusError(CalibrationStatusErrorOptions errorOption)
{
    CalibrationStatusData caliStatus;

    memset(&caliStatus, 0, sizeof(CalibrationStatusData));

    caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_NONE;
    caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    caliStatus.Error     = errorOption;
    CalibrationStatusSet(&caliStatus);
}

static bool isDisarmed()
{
    FlightStatusData flightStatus;

    FlightStatusGet(&flightStatus);
    return flightStatus.Armed == FLIGHTSTATUS_ARMED_DISARMED;
}

int32_t CalibrationStart(void)
{
    PIOS_CALLBACKSCHEDULER_Dispatch(calibrationTimerTaskCBInfo);
    return STATUS_OK;
}

static int32_t CalibrationActivate(void)
{
    PIOS_CALLBACKSCHEDULER_Dispatch(calibrationActivateTaskCBInfo);
    return STATUS_OK;
}

int32_t CalibrationInitialize(void)
{
    CalibrationControlInitialize();
    CalibrationFactoryInitialize();
    CalibrationStatusInitialize();
    CalibrationTriggerInitialize();
    HomeLocationInitialize();
    GyroSensorInitialize();
    AccelSensorInitialize();
    MagSensorInitialize();
    AuxMagSensorInitialize();
    BaroSensorInitialize();
    AirspeedSensorInitialize();
    GPSVelocitySensorInitialize();
    GPSPositionSensorInitialize();
    CalibrationMagsInitialize();
    AttitudeStateInitialize();
    AttitudeSettingsInitialize();
    ObjectPersistenceInitialize();
    AccelGyroSettingsInitialize();
    MPUGyroAccelSettingsInitialize();
    SystemAlarmsInitialize();
    ReceiverActivityInitialize();
    MaintenanceModeSettingsInitialize();
    MaintenanceModeInfoInitialize();
    FlightModeSettingsInitialize();
    ManualControlSettingsInitialize();
    ManualControlCommandInitialize();

    // AuxMagSettingsConnectCallback(&criticalConfigUpdatedCb);
    HomeLocationConnectCallback(&homeLocationUpdatedCb);

    calibrationTimerTaskCBInfo    = PIOS_CALLBACKSCHEDULER_Create(&calibrationTimerTask, CALLBACK_PRIORITY, CBTASK_PRIORITY, CALLBACKINFO_RUNNING_CALIBRATION, STACK_SIZE_BYTES);
    calibrationActivateTaskCBInfo = PIOS_CALLBACKSCHEDULER_Create(&calibrationActivateTask, CALLBACK_PRIORITY, CBTASK_PRIORITY, CALLBACKINFO_RUNNING_CALIBRATION, STACK_SIZE_BYTES);
    GyroSensorConnectCallback(&sensorUpdatedCb);
    AccelSensorConnectCallback(&sensorUpdatedCb);
    MagSensorConnectCallback(&sensorUpdatedCb);
    // BaroSensorConnectCallback(&sensorUpdatedCb);
    // AirspeedSensorConnectCallback(&sensorUpdatedCb);
    AuxMagSensorConnectCallback(&sensorUpdatedCb);
    // GPSVelocitySensorConnectCallback(&sensorUpdatedCb);
    // GPSPositionSensorConnectCallback(&sensorUpdatedCb);
    CalibrationControlConnectCallback(&CalibrationControlUpdated);
    CalibrationTriggerConnectCallback(&CalibrationTriggerUpdated);
    AttitudeStateConnectCallback(&attitudeStateUpdatedCb);
    ReceiverActivityConnectCallback(&receiverUpdatedCb);
    ManualControlCommandConnectCallback(&receiverUpdatedCb);

    LevelCalibrationFSM::instance()->Initialize();
    GyroBiasCalibrationFSM::instance()->Initialize();
    MagCalibrationFSM::instance()->Initialize();
    AccelCalibrationFSM::instance()->Initialize();
    TxCalibrationFSM::instance()->Initialize();

    return STATUS_OK;
}

MODULE_INITCALL(CalibrationInitialize, CalibrationStart);


static void calibrationTimerTask(void)
{
    // Check with FSM if sensor data required?
    if (activeCalibrationFSM) {
        if (activeCalibrationFSM->GetCurrentState() == CALFSM_STATE_ACTIVE) {
            activeCalibrationFSM->Update(CALFSM_DATA_TIMER);
        }
    }
    PIOS_CALLBACKSCHEDULER_Schedule(calibrationTimerTaskCBInfo, updatePeriod, CALLBACK_UPDATEMODE_SOONER);
}

static void calibrationActivateTask(void)
{
    // Check with FSM if sensor data required?
    if (activeCalibrationFSM) {
        activeCalibrationFSM->Activate();
    }
}


static void CalibrationTriggerUpdated(__attribute__((unused)) UAVObjEvent *ev)
{
    if (activeCalibrationFSM) {
        activeCalibrationFSM->calibrationTrigger();
    }
}
static void CalibrationControlUpdated(__attribute__((unused)) UAVObjEvent *ev)
{
    // TODO Keep track of overall calibration state - have we completed it all???
    // For example clearing home location should clear mag calibration.
    // If a cal error'd, reflect an uncalibrated state and block arming
    // Level cal should be redone after other cals
    // If board rotation, force redo cals.
    // if accel/gyro cal, force redo level cal.

    HomeLocationData home;
    CalibrationStatusData caliStatus;

    memset(&caliStatus, 0, sizeof(CalibrationStatusData));

    updatePeriod = IDLE_UPDATE_RATE_MS;

    // check if armed
    if (!isDisarmed()) {
        setCaliStatusError(CALIBRATIONSTATUS_ERROR_DISARMREQUIREDFIRST);
    }

    if (activeCalibrationFSM) {
        activeCalibrationFSM->Inactive();
        activeCalibrationFSM = 0;
    }

    CalibrationControlData caliControl;
    CalibrationControlGet(&caliControl);
    switch (caliControl.Operation) {
    case CALIBRATIONCONTROL_OPERATION_NONE:
        activeCalibrationFSM = 0;
        caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_NONE;
        CalibrationStatusSet(&caliStatus);
        break;

    // TODO Add Temp calibration

    case CALIBRATIONCONTROL_OPERATION_TEMPERATURECALIBRATION:
    case CALIBRATIONCONTROL_OPERATION_LEVELCALIBRATION:
        activeCalibrationFSM = LevelCalibrationFSM::instance();
        CalibrationActivate();
        updatePeriod = activeCalibrationFSM->timerPeriod();
        break;

    case CALIBRATIONCONTROL_OPERATION_GYROBIASCALIBRATION:
    case CALIBRATIONCONTROL_OPERATION_GYROSELFTEST:
        activeCalibrationFSM = GyroBiasCalibrationFSM::instance();
        CalibrationActivate();
        break;

    case CALIBRATIONCONTROL_OPERATION_SETHOMELOCATION:
        // sanity check we have a functioning gps first otherwise don't action
        if (SYSTEMALARMS_ALARM_OK == AlarmsGet(SYSTEMALARMS_ALARM_GPS)) {
            clearHomeLocation();
            caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_SETHOMELOCATION;
            caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
            caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_IDLE;
            caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;
            CalibrationStatusSet(&caliStatus);
        } else {
            setCaliStatusError(CALIBRATIONSTATUS_ERROR_GPSALARMPRESENT);
        }
        break;

    case CALIBRATIONCONTROL_OPERATION_COMPASSCALIBRATION:
    case CALIBRATIONCONTROL_OPERATION_COMPASSRECALIBRATION:
        HomeLocationGet(&home);
        if (home.Set == HOMELOCATION_SET_TRUE) {
            activeCalibrationFSM = MagCalibrationFSM::instance();
            CalibrationActivate();
            updatePeriod = activeCalibrationFSM->timerPeriod();
        } else {
            setCaliStatusError(CALIBRATIONSTATUS_ERROR_HOMELOCATIONNOTSET);
        }
        break;

    case CALIBRATIONCONTROL_OPERATION_ACCELSELFTEST:
    case CALIBRATIONCONTROL_OPERATION_ACCELCALIBRATION:
        HomeLocationGet(&home);
        if (home.Set == HOMELOCATION_SET_TRUE) {
            activeCalibrationFSM = AccelCalibrationFSM::instance();
            CalibrationActivate();
            updatePeriod = activeCalibrationFSM->timerPeriod();
        } else {
            setCaliStatusError(CALIBRATIONSTATUS_ERROR_HOMELOCATIONNOTSET);
        }
        break;

    case CALIBRATIONCONTROL_OPERATION_TXCALIBRATION:
        activeCalibrationFSM = TxCalibrationFSM::instance();
        CalibrationActivate();
        updatePeriod = activeCalibrationFSM->timerPeriod();
        break;

    case CALIBRATIONCONTROL_OPERATION_SAVECALIBRATIONRESULTS:
        LevelCalibrationFSM::instance()->SaveToFlash();
        GyroBiasCalibrationFSM::instance()->SaveToFlash();
        MagCalibrationFSM::instance()->SaveToFlash();
        AccelCalibrationFSM::instance()->SaveToFlash();
        TxCalibrationFSM::instance()->SaveToFlash();
        // TODO Does home location need saving?
        caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_SAVECALIBRATIONRESULTS;
        caliStatus.OperationState           = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
        caliStatus.LevelCalibrationState    = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_IDLE;
        caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_IDLE;
        caliStatus.TxCalibrationState       = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDLE;
        caliStatus.Error = CALIBRATIONSTATUS_ERROR_NOERROR;
        CalibrationStatusSet(&caliStatus);
        break;
    }
}

static void homeLocationUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    HomeLocationData home;

    HomeLocationGet(&home);
    if (home.Set == HOMELOCATION_SET_TRUE) {
        CalibrationStatusData caliStatus;
        memset(&caliStatus, 0, sizeof(CalibrationStatusData));
        caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_SETHOMELOCATION;
        caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
        caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_IDLE;
        caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;
        CalibrationStatusSet(&caliStatus);
    }
}

static void attitudeStateUpdatedCb(__attribute__((unused)) UAVObjEvent *ev)
{
    if (activeCalibrationFSM && activeCalibrationFSM->GetCurrentState() == CALFSM_STATE_ACTIVE) {
        activeCalibrationFSM->Update(CALFSM_DATA_ATTITUDESTATE);
    }
}


static void sensorUpdatedCb(UAVObjEvent *ev)
{
    if (!ev) {
        return;
    }

    // Check with FSM if sensor data required?
    if (!activeCalibrationFSM || activeCalibrationFSM->GetCurrentState() != CALFSM_STATE_ACTIVE) {
        return;
    }

    if (ev->base == GyroSensorBase()) {
        activeCalibrationFSM->Update(CALFSM_DATA_GYROSENSOR);
    }

    if (ev->base == MagSensorBase()) {
        activeCalibrationFSM->Update(CALFSM_DATA_MAGSENSOR);
    }

    if (ev->base == AuxMagSensorBase()) {
        activeCalibrationFSM->Update(CALFSM_DATA_AUXMAGSENSOR);
    }

    if (ev->base == AccelSensorBase()) {
        activeCalibrationFSM->Update(CALFSM_DATA_ACCELSENSOR);
    }
}

static void receiverUpdatedCb(UAVObjEvent *ev)
{
    if (!ev) {
        return;
    }

    if (activeCalibrationFSM && activeCalibrationFSM->GetCurrentState() == CALFSM_STATE_ACTIVE) {
        if (ev->base == ReceiverActivityBase()) {
            activeCalibrationFSM->Update(CALFSM_DATA_RECEIVERACTIVITY);
        } else if (ev->base == ManualControlCommandBase()) {
            activeCalibrationFSM->Update(CALFSM_DATA_RECEIVERCHANNEL);
        }
    }
}
