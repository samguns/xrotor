/*
 ******************************************************************************
 *
 * @file       accelcalibrationfsm.cpp
 * @author     Copyright (C) 2015 Alex Beck
 * @brief      Accel calibration state machine
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#include <project_xflight.h>
#include <callbackinfo.h>
#include <pios_errno.h>

#include <mathmisc.h>
#include <alarms.h>
#include "attitudesettings.h"
#include "attitudestate.h"
#include "calibrationstatus.h"
#include "calibrationtrigger.h"
#include "objectpersistence.h"
#include "accelgyrosettings.h"
#include "accelsensor.h"
#include "calibrationmags.h"
#include "homelocation.h"
#include "calibrationfactory.h"
#include "calibrationcontrol.h"

// C++ includes
#include <math.h>
#include <accelcalibrationfsm.h>

// Include Files
#include "libmarquardt/rt_nonfinite.h"
#include "libmarquardt/marquardt.h"
#include "libmarquardt/marquardt_initialize.h"


static DelayedCallbackInfo *calibrationCalcTaskCBInfo;

// Private constants
#define TIMER_COUNT_PER_SECOND 10
#define SKIP_SAMPLE_SIZE       200

#define COMPLETED_DISPLAY_TIME (4 * timerTicksPerSecond())

AccelCalibrationFSM::AccelCalibrationFSM_StateHandler_T AccelCalibrationFSM::sAccelStateTable[ACCELCAL_STATE_SIZE] = {
    [ACCELCAL_STATE_INACTIVE]     =       { .setup = &AccelCalibrationFSM::setup_inactive,           .run = 0                                     },
    [ACCELCAL_STATE_START]        =       { .setup = &AccelCalibrationFSM::setup_start,              .run = 0                                     },
    [ACCELCAL_STATE_PRESELFTEST]  =       { .setup = &AccelCalibrationFSM::setup_preselftest,        .run = &AccelCalibrationFSM::run_preselftest },
    [ACCELCAL_STATE_SELFTEST]     =       { .setup = &AccelCalibrationFSM::setup_selftest,           .run = &AccelCalibrationFSM::run_selftest    },
    [ACCELCAL_STATE_POSTSELFTEST] =       { .setup = &AccelCalibrationFSM::setup_postselftest,       .run = 0                                     },
    [ACCELCAL_STATE_SELFTEST_COMPLETED] = { .setup = &AccelCalibrationFSM::setup_selftest_completed, .run = 0                                     },
    [ACCELCAL_STATE_SAMPLE_1]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_2]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_3]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_4]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_5]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_6]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_7]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_8]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_9]     =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_10]    =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_11]    =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_12]    =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_13]    =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },
    [ACCELCAL_STATE_SAMPLE_14]    =       { .setup = &AccelCalibrationFSM::setup_sample,             .run = &AccelCalibrationFSM::run_sample      },

    [ACCELCAL_STATE_CALC] =               { .setup = &AccelCalibrationFSM::setup_calc,               .run = &AccelCalibrationFSM::run_calc        },
    [ACCELCAL_STATE_COMPLETED]    =       { .setup = &AccelCalibrationFSM::setup_completed,          .run = &AccelCalibrationFSM::run_completed   },
    [ACCELCAL_STATE_ERROR]        =       { .setup = &AccelCalibrationFSM::setup_error,              .run = &AccelCalibrationFSM::run_completed   }
};


AccelCalibrationFSM_SamplingSequence_T AccelCalibrationFSM::sAccelSamplingTable[ACCEL_SAMPLE_COUNT] = {
    [0]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PLACENOSEDOWN,          ACCELCAL_STATE_SAMPLE_2,  ACCELCAL_STATE_SAMPLE_1  },  // SAMPLE_1
    [1]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PLACERIGHTSIDEDOWN,     ACCELCAL_STATE_SAMPLE_3,  ACCELCAL_STATE_SAMPLE_1  },  // SAMPLE_2
    [2]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PLACENOSEUP,            ACCELCAL_STATE_SAMPLE_4,  ACCELCAL_STATE_SAMPLE_2  },  // SAMPLE_3
    [3]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PLACELEFTSIDEDOWN,      ACCELCAL_STATE_SAMPLE_5,  ACCELCAL_STATE_SAMPLE_3  },  // SAMPLE_4
    [4]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PITCHFORWARD45,         ACCELCAL_STATE_SAMPLE_6,  ACCELCAL_STATE_SAMPLE_4  },  // SAMPLE_5
    [5]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_ROLLRIGHT45,            ACCELCAL_STATE_SAMPLE_7,  ACCELCAL_STATE_SAMPLE_5  },  // SAMPLE_6
    [6]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PITCHBACK45,            ACCELCAL_STATE_SAMPLE_8,  ACCELCAL_STATE_SAMPLE_6  },  // SAMPLE_7
    [7]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_ROLLLEFT45,             ACCELCAL_STATE_SAMPLE_9,  ACCELCAL_STATE_SAMPLE_7  },  // SAMPLE_8
    [8]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_INVERTEDPITCHFORWARD45, ACCELCAL_STATE_SAMPLE_10, ACCELCAL_STATE_SAMPLE_8  },  // SAMPLE_9
    [9]  = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_INVERTEDROLLRIGHT45,    ACCELCAL_STATE_SAMPLE_11, ACCELCAL_STATE_SAMPLE_9  },  // SAMPLE_10
    [10] = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_INVERTEDPITCHBACK45,    ACCELCAL_STATE_SAMPLE_12, ACCELCAL_STATE_SAMPLE_10 }, // SAMPLE_11
    [11] = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_INVERTEDROLLLEFT45,     ACCELCAL_STATE_SAMPLE_13, ACCELCAL_STATE_SAMPLE_11 }, // SAMPLE_12
    [12] = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PLACEUPSIDEDOWN,        ACCELCAL_STATE_SAMPLE_14, ACCELCAL_STATE_SAMPLE_12 }, // SAMPLE_13
    [13] = { CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PLACEHORIZONTAL,        ACCELCAL_STATE_CALC,      ACCELCAL_STATE_SAMPLE_13 } // SAMPLE_14
};

// pointer to a singleton instance
AccelCalibrationFSM *AccelCalibrationFSM::p_inst = 0;


// FSM Setup and Run method implementation

void AccelCalibrationFSM::setup_inactive(void)
{
    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    CalibrationStatusSet(&(mAccelData->caliStatus));
}

void AccelCalibrationFSM::setup_start(void)
{
    memset(&mAccelData->caliStatus, 0, sizeof(CalibrationStatusData));
    mAccelData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_ACCELCALIBRATION;
    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_IDLE;
    mAccelData->caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;
    CalibrationStatusSet(&(mAccelData->caliStatus));

    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);

    // save board rotation
    mAccelData->boardRotationYaw          = attitudeSettings.BoardRotation.Yaw;
    mAccelData->boardRotationRoll         = attitudeSettings.BoardRotation.Roll;
    mAccelData->boardRotationPitch        = attitudeSettings.BoardRotation.Pitch;
    mAccelData->boardLevelTrimRoll        = attitudeSettings.BoardLevelTrim.Roll;
    mAccelData->boardLevelTrimPitch       = attitudeSettings.BoardLevelTrim.Pitch;

    // Zero board rotation during calibration
    attitudeSettings.BoardRotation.Yaw    = 0.0f;
    attitudeSettings.BoardRotation.Roll   = 0.0f;
    attitudeSettings.BoardRotation.Pitch  = 0.0f;
    attitudeSettings.BoardLevelTrim.Pitch = 0.0f;
    attitudeSettings.BoardLevelTrim.Roll  = 0.0f;
    AttitudeSettingsSet(&attitudeSettings);


    // Calibration accel
    AccelGyroSettingsGet(&mAccelData->saveAccelGyro);

    AccelGyroSettingsData clearedAccelValues = mAccelData->saveAccelGyro;
    clearedAccelValues.accel_scale.X = 1;
    clearedAccelValues.accel_scale.Y = 1;
    clearedAccelValues.accel_scale.Z = 1;
    clearedAccelValues.accel_crossaxis_scale.XY = 0;
    clearedAccelValues.accel_crossaxis_scale.XZ = 0;
    clearedAccelValues.accel_crossaxis_scale.YX = 0;
    clearedAccelValues.accel_crossaxis_scale.YZ = 0;
    clearedAccelValues.accel_crossaxis_scale.ZX = 0;
    clearedAccelValues.accel_crossaxis_scale.ZY = 0;
    clearedAccelValues.accel_bias.X = 0;
    clearedAccelValues.accel_bias.Y = 0;
    clearedAccelValues.accel_bias.Z = 0;
    AccelGyroSettingsSet(&clearedAccelValues);

    mAccelData->startSampling = false;

    for (int idx = 0; idx < ACCEL_SAMPLE_COUNT; idx++) {
        mAccelData->accel[idx].zero();
        mAccelData->variance[idx] = 0.0f;
    }

    CalibrationControlData caliControl;
    CalibrationControlGet(&caliControl);
    if (caliControl.Operation == CALIBRATIONCONTROL_OPERATION_ACCELSELFTEST) {
#ifdef PIOS_INCLUDE_MPU9250
        mAccelData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_ACCELSELFTEST;
        setState(ACCELCAL_STATE_PRESELFTEST);
        return;

#endif
    }

    if (testCalibrationFitAsymmetric() != STATUS_OK) {
        mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_IDLE;
        mAccelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_ACCELCALUNITTESTFAIL;
        setState(ACCELCAL_STATE_ERROR);
    } else {
        setState(ACCELCAL_STATE_SAMPLE_1);
    }
}


void AccelCalibrationFSM::calibrationTrigger(void)
{
    CalibrationTriggerData calibration_trigger;

    CalibrationTriggerGet(&calibration_trigger);
    if (calibration_trigger.AccelCal == CALIBRATIONTRIGGER_ACCELCAL_NEWORIENTATIONREADY) {
        if (mAccelData->startSampling == false) {
            mAccelData->startSampling = true;
            mAccelData->samplingInitialised = false;
        }
    } else if (calibration_trigger.AccelCal == CALIBRATIONTRIGGER_ACCELCAL_BACK) {
        mAccelData->startSampling = false;
        mAccelData->goBack = true;
        mAccelData->samplingInitialised = false;
    }
}

void AccelCalibrationFSM::setup_helper(CalibrationStatusAccelCalibrationStateOptions setup_status_state)
{
    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    mAccelData->caliStatus.AccelCalibrationState = setup_status_state;
    mAccelData->caliStatus.Error  = CALIBRATIONSTATUS_ERROR_NOERROR;
    CalibrationStatusSet(&(mAccelData->caliStatus));
    mAccelData->startSampling     = false;
    mAccelData->accel_sum.zero();
    mAccelData->observationCount  = 0;
    mAccelData->observation2Count = 0;
}

void AccelCalibrationFSM::run_helper(uint8_t idx, AccelCalibrationFSM_State_T next_state, AccelCalibrationFSM_State_T back_state)
{
    AccelSensorData accelSensor;

    // prevent immediate sampling. used only for self tests which does not require a trigger.
    if (mAccelData->observation2Count++ < SKIP_SAMPLE_SIZE) {
        return;
    }

    // Wait until we get a signal from the user that we should start samping.
    if (mAccelData->startSampling) {
        if (!mAccelData->samplingInitialised) {
            mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
            mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_SAMPLE;
            mAccelData->caliStatus.Error    = CALIBRATIONSTATUS_ERROR_NOERROR;
            CalibrationStatusSet(&(mAccelData->caliStatus));
            mAccelData->samplingInitialised = true;
            AccelSensorGet(&accelSensor);
            mAccelData->accelBeginning(accelSensor.x, accelSensor.y, accelSensor.z);
            if (is_zero(mAccelData->accelBeginning.length())) {
                mAccelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_ACCELSENSORFAIL;
                setState(ACCELCAL_STATE_ERROR);
                return;
            }
        }


        AccelSensorGet(&accelSensor);
        Vector3f sensor(accelSensor.x, accelSensor.y, accelSensor.z);

        Vector3f accelDiff = sensor - mAccelData->accelBeginning;

        if (accelDiff.length() > 1.0f) {
            // we have detected a change in orientation. skip this sample
            // restart the current state
            setState(mAccelData->currentState);
            mAccelData->startSampling = true;
            mAccelData->samplingInitialised = false;
            return;
        }


        if (mAccelData->observationCount >= POINT_SAMPLE_SIZE) {
            // calc the average
            mAccelData->accel[idx] = mAccelData->accel_sum / (float)mAccelData->observationCount;

            Vector3f delta_sum;
            delta_sum.zero();
            for (int jdum = 0; jdum < POINT_SAMPLE_SIZE; jdum++) {
                Vector3f squared_delta = mAccelData->accel_sample[jdum] - mAccelData->accel[idx];
                squared_delta.norm2();
                delta_sum += squared_delta;
            }
            delta_sum /= POINT_SAMPLE_SIZE;

            mAccelData->variance[idx] = delta_sum.squarednorm();

            if (accept_sample(mAccelData->accel[idx], idx)) {
                // Requires addition of this to CalbrationStatus (if not too large!)
                // <field name="SampleDataX" units="" type="float" elements="14" defaultvalue="0,0,0,0,0,0,0,0,0,0,0,0,0,0"/>
                // <field name="SampleDataY" units="" type="float" elements="14" defaultvalue="0,0,0,0,0,0,0,0,0,0,0,0,0,0"/>
                // <field name="SampleDataZ" units="" type="float" elements="14" defaultvalue="0,0,0,0,0,0,0,0,0,0,0,0,0,0"/>
                // <field name="SampleDataVariance" units="" type="float" elements="14" defaultvalue="0,0,0,0,0,0,0,0,0,0,0,0,0,0"/>
                // mAccelData->caliStatus.SampleDataX[idx] = mAccelData->accel[idx].x;
                // mAccelData->caliStatus.SampleDataY[idx] = mAccelData->accel[idx].y;
                // mAccelData->caliStatus.SampleDataZ[idx] = mAccelData->accel[idx].z;
                // mAccelData->caliStatus.SampleDataVariance[idx] = mAccelData->variance[idx];


                setState(next_state);
            } else {
                mAccelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_TOOCLOSETOPREVIOUSPOSITION;
                CalibrationStatusSet(&(mAccelData->caliStatus));
                setState(mAccelData->currentState);
            }
        } else {
            // cumulate the sum for the average
            mAccelData->accel_sum += sensor;
            mAccelData->accel_sample[mAccelData->observationCount] = sensor;
            mAccelData->observationCount++;
        }
    } else if (mAccelData->goBack) {
        mAccelData->goBack = false;
        mAccelData->samplingInitialised = false;
        setState(back_state);
    }
}

bool AccelCalibrationFSM::accept_sample(const Vector3f & sample, uint8_t samples_collected)
{
    uint8_t faces = 2 * ACCEL_SAMPLE_COUNT - 4;
    float theta   = acosf(cosf((4.0f * M_PI_F / (3.0f * faces)) + M_PI_F / 3.0f) / (1.0f - cosf((4.0f * M_PI_F / (3.0f * faces)) + M_PI_F / 3.0f)));

    theta *= 0.25f;
    HomeLocationData homeLocation;
    HomeLocationGet(&homeLocation);
    float min_distance = homeLocation.g_e * 2.0f * sinf(theta);

    for (uint8_t idum = 0; idum < samples_collected; idum++) {
        if ((mAccelData->accel[idum] - sample).length() < min_distance) {
            return false;
        }
    }
    return true;
}


void AccelCalibrationFSM::setup_preselftest(void)
{
    MPUGyroAccelSettingsData mpuSettings;

    MPUGyroAccelSettingsGet(&mpuSettings);
    mpuSettings.AccelFilterSetting = MPUGYROACCELSETTINGS_ACCELFILTERSETTING_LOWPASS_92_HZ;
    mpuSettings.AccelScale = MPUGYROACCELSETTINGS_ACCELSCALE_SCALE_2G;
    mpuSettings.MaxInnerLoopRate   = MPUGYROACCELSETTINGS_MAXINNERLOOPRATE_1000;
    MPUGyroAccelSettingsSet(&mpuSettings);
    setup_helper(CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_PRESELFTEST);
    mAccelData->startSampling = true;
    mAccelData->samplingInitialised = false;
}
void AccelCalibrationFSM::run_preselftest(uint8_t)
{
    run_helper(0, ACCELCAL_STATE_SELFTEST, ACCELCAL_STATE_PRESELFTEST);
}


void AccelCalibrationFSM::setup_selftest(void)
{
    MPUGyroAccelSettingsData mpuSettings;

    MPUGyroAccelSettingsGet(&mpuSettings);
    mpuSettings.AccelFilterSetting = MPUGYROACCELSETTINGS_ACCELFILTERSETTING_LOWPASS_92_HZ;
    mpuSettings.AccelScale = MPUGYROACCELSETTINGS_ACCELSCALE_SCALE_2G_TEST;
    mpuSettings.MaxInnerLoopRate   = MPUGYROACCELSETTINGS_MAXINNERLOOPRATE_1000;
    MPUGyroAccelSettingsSet(&mpuSettings);
    setup_helper(CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_SELFTEST);
    mAccelData->startSampling = true;
    mAccelData->samplingInitialised = false;
}


void AccelCalibrationFSM::run_selftest(uint8_t)
{
    run_helper(1, ACCELCAL_STATE_POSTSELFTEST, ACCELCAL_STATE_PRESELFTEST);
}

#define STR_PERCENTAGE_DELTA_FROM_FACTORY 15.0f
void AccelCalibrationFSM::setup_postselftest(void)
{
    mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_POSTSELFTEST;
    CalibrationStatusSet(&(mAccelData->caliStatus));

    // restore settings // todo do we need to do this for error conditions also????
    MPUGyroAccelSettingsSet(&mAccelData->mpuSettings);

    // calculate factory vs self test ratios
    CalibrationFactoryData factoryCal;
    CalibrationFactoryGet(&factoryCal);
    factoryCal.AccelBase.x     = mAccelData->accel[0].x;
    factoryCal.AccelBase.y     = mAccelData->accel[0].y;
    factoryCal.AccelBase.z     = mAccelData->accel[0].z;
    factoryCal.AccelSelfTest.x = mAccelData->accel[1].x;
    factoryCal.AccelSelfTest.y = mAccelData->accel[1].y;
    factoryCal.AccelSelfTest.z = mAccelData->accel[1].z;
    factoryCal.AccelSTR.x = fabsf(100.0f * (factoryCal.AccelSelfTest.x - factoryCal.AccelBase.x - factoryCal.Accel.x) / factoryCal.Accel.x);
    factoryCal.AccelSTR.y = fabsf(100.0f * (factoryCal.AccelSelfTest.y - factoryCal.AccelBase.y - factoryCal.Accel.y) / factoryCal.Accel.y);
    factoryCal.AccelSTR.z = fabsf(100.0f * (factoryCal.AccelSelfTest.z - factoryCal.AccelBase.z - factoryCal.Accel.z) / factoryCal.Accel.z);

    if (fabsf(factoryCal.AccelSTR.x) < STR_PERCENTAGE_DELTA_FROM_FACTORY &&
        fabsf(factoryCal.AccelSTR.y) < STR_PERCENTAGE_DELTA_FROM_FACTORY &&
        fabsf(factoryCal.AccelSTR.z) < STR_PERCENTAGE_DELTA_FROM_FACTORY) {
        factoryCal.AccelSelfTestStatus = CALIBRATIONFACTORY_ACCELSELFTESTSTATUS_GOOD;
    } else {
        factoryCal.AccelSelfTestStatus = CALIBRATIONFACTORY_ACCELSELFTESTSTATUS_BAD;
    }
    CalibrationFactorySet(&factoryCal);

    if (factoryCal.AccelSelfTestStatus == CALIBRATIONFACTORY_ACCELSELFTESTSTATUS_BAD) {
        mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_ERROR;
        mAccelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_ACCELSELFTESTFAIL;
        setState(ACCELCAL_STATE_ERROR);
    } else {
        setState(ACCELCAL_STATE_SELFTEST_COMPLETED);
    }
}


void AccelCalibrationFSM::setup_sample(void)
{
    uint8_t idx = mAccelData->currentState - ACCELCAL_STATE_SAMPLE_1;

    setup_helper(sAccelSamplingTable[idx].setup_status_state);
}
void AccelCalibrationFSM::run_sample(uint8_t)
{
    uint8_t idx = mAccelData->currentState - ACCELCAL_STATE_SAMPLE_1;

    run_helper(idx, sAccelSamplingTable[idx].next_state, sAccelSamplingTable[idx].back_state);
}

#define CALC_TIME (30 * TIMER_COUNT_PER_SECOND)
void AccelCalibrationFSM::setup_calc(void)
{
    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
    mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_CALC;
    CalibrationStatusSet(&(mAccelData->caliStatus));
    setStateTimeout(CALC_TIME);
    PIOS_CALLBACKSCHEDULER_Schedule(calibrationCalcTaskCBInfo, 500, CALLBACK_UPDATEMODE_SOONER);
}

void AccelCalibrationFSM::run_calc(__attribute__((unused)) uint8_t flTimeout)
{
    // report on status
}

void AccelCalibrationFSM::calibrationCalcTask(void)
{
    AccelCalibrationFSM::instance()->calcTask();
}


void AccelCalibrationFSM::setup_completed(void)
{
    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_COMPLETE;
    CalibrationStatusSet(&(mAccelData->caliStatus));
    mSaveToFlashPending = true;

    // Restore
    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);
    attitudeSettings.BoardRotation.Yaw    = mAccelData->boardRotationYaw;
    attitudeSettings.BoardRotation.Roll   = mAccelData->boardRotationRoll;
    attitudeSettings.BoardRotation.Pitch  = mAccelData->boardRotationPitch;
    attitudeSettings.BoardLevelTrim.Roll  = mAccelData->boardLevelTrimRoll;
    attitudeSettings.BoardLevelTrim.Pitch = mAccelData->boardLevelTrimPitch;
    AttitudeSettingsSet(&attitudeSettings);

    AccelGyroSettingsSet(&accelGyroSettings);
    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void AccelCalibrationFSM::setup_selftest_completed(void)
{
    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_SELFTESTCOMPLETE;
    CalibrationStatusSet(&(mAccelData->caliStatus));

    // Restore
    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);
    attitudeSettings.BoardRotation.Yaw    = mAccelData->boardRotationYaw;
    attitudeSettings.BoardRotation.Roll   = mAccelData->boardRotationRoll;
    attitudeSettings.BoardRotation.Pitch  = mAccelData->boardRotationPitch;
    attitudeSettings.BoardLevelTrim.Roll  = mAccelData->boardLevelTrimRoll;
    attitudeSettings.BoardLevelTrim.Pitch = mAccelData->boardLevelTrimPitch;
    AttitudeSettingsSet(&attitudeSettings);

    AccelGyroSettingsSet(&mAccelData->saveAccelGyro);
    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void AccelCalibrationFSM::setup_error(void)
{
    // Restore
    AttitudeSettingsData attitudeSettings;

    AttitudeSettingsGet(&attitudeSettings);
    // attitudeSettings.BiasCorrectGyro     = ATTITUDESETTINGS_BIASCORRECTGYRO_TRUE;
    attitudeSettings.BoardRotation.Yaw    = mAccelData->boardRotationYaw;
    attitudeSettings.BoardRotation.Roll   = mAccelData->boardRotationRoll;
    attitudeSettings.BoardRotation.Pitch  = mAccelData->boardRotationPitch;
    attitudeSettings.BoardLevelTrim.Roll  = mAccelData->boardLevelTrimRoll;
    attitudeSettings.BoardLevelTrim.Pitch = mAccelData->boardLevelTrimPitch;
    AttitudeSettingsSet(&attitudeSettings);

    AccelGyroSettingsSet(&mAccelData->saveAccelGyro);

    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_ERROR;
    CalibrationStatusSet(&(mAccelData->caliStatus));

    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void AccelCalibrationFSM::run_completed(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(ACCELCAL_STATE_INACTIVE);
    }
}

void AccelCalibrationFSM::SaveToFlash(void)
{
    if (mSaveToFlashPending) {
        mSaveToFlashPending = false;
        ObjectPersistenceData objper;
        objper.InstanceID   = 0;
        objper.ObjectID     = ACCELGYROSETTINGS_OBJID;
        objper.Operation    = OBJECTPERSISTENCE_OPERATION_SAVE;
        objper.Selection    = OBJECTPERSISTENCE_SELECTION_SINGLEOBJECT;
        ObjectPersistenceSet(&objper);
    }
}


void AccelCalibrationFSM::calcTask(void)
{
    HomeLocationData homeLocation;

    HomeLocationGet(&homeLocation);
    float S[MODEL_UNKNOWNS], b[3];

    for (int idx = 0; idx < ACCEL_SAMPLE_COUNT; idx++) {
        AccelSamples[idx] = mAccelData->accel[idx].x;
        AccelSamples[idx + 1 * ACCEL_SAMPLE_COUNT] = mAccelData->accel[idx].y;
        AccelSamples[idx + 2 * ACCEL_SAMPLE_COUNT] = mAccelData->accel[idx].z;
    }

    calibrationFit(homeLocation.g_e, mAccelData->accel, mAccelData->variance, S, b);

    AccelGyroSettingsGet(&accelGyroSettings);

    accelGyroSettings.accel_scale.X = fabsf(S[0]);
    accelGyroSettings.accel_scale.Y = fabsf(S[4]);
    accelGyroSettings.accel_scale.Z = fabsf(S[8]);
    accelGyroSettings.accel_crossaxis_scale.XY = S[1];
    accelGyroSettings.accel_crossaxis_scale.XZ = S[2];
    accelGyroSettings.accel_crossaxis_scale.YX = S[3];
    accelGyroSettings.accel_crossaxis_scale.YZ = S[5];
    accelGyroSettings.accel_crossaxis_scale.ZX = S[6];
    accelGyroSettings.accel_crossaxis_scale.ZY = S[7];
    accelGyroSettings.accel_bias.X = b[0];
    accelGyroSettings.accel_bias.Y = b[1];
    accelGyroSettings.accel_bias.Z = b[2];

    if (isnan(accelGyroSettings.accel_scale.X) || isnan(accelGyroSettings.accel_scale.Y) || isnan(accelGyroSettings.accel_scale.Z) ||
        isnan(accelGyroSettings.accel_bias.X) || isnan(accelGyroSettings.accel_bias.Y) || isnan(accelGyroSettings.accel_bias.Z)) {
        mAccelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_ACCELCALFAIL;
        setState(ACCELCAL_STATE_ERROR);
        return;
    }


    setState(ACCELCAL_STATE_COMPLETED);
}

int AccelCalibrationFSM::calibrationFit(float ConstMag, Vector3f *samples, float *variance, float S[9], float b[3])
{
    float Vx[ACCEL_SAMPLE_COUNT], Vy[ACCEL_SAMPLE_COUNT], Vz[ACCEL_SAMPLE_COUNT], Vvariance[ACCEL_SAMPLE_COUNT], B2[3];
    float info[7];

    // Loop over the array to initialize each element.
    for (int idx0 = 0; idx0 < ACCEL_SAMPLE_COUNT; idx0++) {
        Vx[idx0] = samples[idx0].x / ConstMag;
        Vy[idx0] = samples[idx0].y / ConstMag;
        Vz[idx0] = samples[idx0].z / ConstMag;
        Vvariance[idx0] = variance[idx0] / ConstMag;
    }
    marquardt_initialize();
    calibrationFitAsymmetric(Vx, Vy, Vz, Vvariance, S, B2, info);

    b[0] = B2[0] * ConstMag;
    b[1] = B2[1] * ConstMag;
    b[2] = B2[2] * ConstMag;

    return 1;
}


// Function Definitions

//
// Arguments    : const float x[12]
// const float Vx[10]
// const float Vy[10]
// const float Vz[10]
// float r[10]
// float J[120]
// Return Type  : void
//
void c_accelCalAsymmetricGetRandJaco(const float x[12],
                                     int32_T sampleCount,
                                     const float Vx[ACCEL_SAMPLE_COUNT],
                                     const float Vy[ACCEL_SAMPLE_COUNT],
                                     const float Vz[ACCEL_SAMPLE_COUNT],
                                     const float Vvariance[ACCEL_SAMPLE_COUNT],
                                     float r[ACCEL_SAMPLE_COUNT],
                                     float J[ACCEL_SAMPLE_COUNT * ACCEL_SAMPLE_COUNT])
{
    float Sx;
    float Sy;
    float Sz;
    float Mxy;
    float Mxz;
    float Myx;
    float Myz;
    float Mzx;
    float Mzy;
    int idum;
    float BVx_colvec;
    float BVy_colvec;
    float BVz_colvec;
    float c;
    float b_c;
    float c_c;
    float d_c;
    float e_c;
    float f_c;
    float g_c;
    float h_c;
    float i_c;

    Sx  = x[0];
    Sy  = x[1];
    Sz  = x[2];
    Mxy = x[3];
    Mxz = x[4];
    Myx = x[5];
    Myz = x[6];
    Mzx = x[7];
    Mzy = x[8];

    // 'autocalibrationAsymmetric:94' for idum=1:numSamples
    for (idum = 0; idum < sampleCount; idum++) {
        BVx_colvec = x[9] - Vx[idum];
        BVy_colvec = x[10] - Vy[idum];
        BVz_colvec = x[11] - Vz[idum];

        // 'autocalibrationAsymmetric:100' Ex = (Sx*BVx)^2 + (Sy*Mxy*BVy)^2 + (Sz*Mxz*BVz)^2 + 2*(Sx*BVx*Sy*Mxy*BVy) + 2*(Sx*BVx*Sz*Mxz*BVz) + 2*(Sy*Mxy*BVy*Sz*Mxz*BVz);
        c       = Sx * BVx_colvec;
        b_c     = Sy * Mxy * BVy_colvec;
        c_c     = Sz * Mxz * BVz_colvec;

        // 'autocalibrationAsymmetric:101' Ey = (Sx*Myx*BVx)^2 + (Sy*BVy)^2 + (Sz*Myz*BVz)^2 + 2*(Sx*Myx*BVx*Sy*BVy) + 2*(Sx*Myx*BVx*Sz*Myz*BVz) + 2*(Sy*BVy*Sz*Myz*BVz);
        d_c     = Sx * Myx * BVx_colvec;
        e_c     = Sy * BVy_colvec;
        f_c     = Sz * Myz * BVz_colvec;

        // 'autocalibrationAsymmetric:102' Ez = (Sx*Mzx*BVx)^2 + (Sy*Mzy*BVy)^2 + (Sz*BVz)^2 + 2*(Sx*Mzx*BVx*Sy*Mzy*BVy) + 2*(Sx*Mzx*BVx*Sz*BVz) + 2*(Sy*Mzy*BVy*Sz*BVz);
        g_c     = Sx * Mzx * BVx_colvec;
        h_c     = Sy * Mzy * BVy_colvec;
        i_c     = Sz * BVz_colvec;

        // 'autocalibrationAsymmetric:104' r(idum)  =  Ex + Ey + Ez - 1;
        r[idum] = (((((((c * c + b_c * b_c) + c_c * c_c) + 2.0F * (Sx * BVx_colvec *
                                                                   Sy * Mxy * BVy_colvec)) + 2.0F * (Sx * BVx_colvec * Sz * Mxz * BVz_colvec))
                     + 2.0F * (Sy * Mxy * BVy_colvec * Sz * Mxz * BVz_colvec)) +
                    (((((d_c * d_c + e_c * e_c) + f_c * f_c) + 2.0F * (Sx * Myx *
                                                                       BVx_colvec * Sy * BVy_colvec)) + 2.0F * (Sx * Myx * BVx_colvec * Sz * Myz *
                                                                                                                BVz_colvec)) + 2.0F * (Sy * BVy_colvec * Sz * Myz * BVz_colvec))) +
                   (((((g_c * g_c + h_c * h_c) + i_c * i_c) + 2.0F * (Sx * Mzx *
                                                                      BVx_colvec * Sy * Mzy * BVy_colvec)) + 2.0F * (Sx * Mzx * BVx_colvec * Sz *
                                                                                                                     BVz_colvec)) + 2.0F * (Sy * Mzy * BVy_colvec * Sz * BVz_colvec))) - 1.0F - 3.0F * Vvariance[idum];

        // per sample i, calculate the Jacobian row vector for the 12 unknowns
        // dE/dSx
        // 'autocalibrationAsymmetric:109' J(idum,1) = 2*Sx*BVx^2 + 2*(BVx*Sy*Mxy*BVy) + 2*(BVx*Sz*Mxz*BVz) + 2*(Sy*Mxy*BVy*Sz*Mxz*BVz) + 2*Sx*(Myx*BVx)^2 + 2*(Myx*BVx*Sy*BVy) + 2*(Myx*BVx*Sz*Myz*BVz) + 2*Sx*(Mzx*BVx)^2 + 2*(Mzx*BVx*Sy*Mzy*BVy) + 2*(Mzx*BVx*Sz*BVz);
        c       = Myx * BVx_colvec;
        b_c     = Mzx * BVx_colvec;
        J[idum] = ((((((((2.0F * Sx * (BVx_colvec * BVx_colvec) + 2.0F * (BVx_colvec
                                                                          * Sy * Mxy * BVy_colvec)) + 2.0F * (BVx_colvec * Sz * Mxz * BVz_colvec)) +
                        2.0F * (Sy * Mxy * BVy_colvec * Sz * Mxz * BVz_colvec)) +
                       2.0F * Sx * (c * c)) + 2.0F * (Myx * BVx_colvec * Sy *
                                                      BVy_colvec)) + 2.0F * (Myx * BVx_colvec * Sz * Myz * BVz_colvec)) + 2.0F *
                    Sx * (b_c * b_c)) + 2.0F * (Mzx * BVx_colvec * Sy * Mzy *
                                                BVy_colvec)) + 2.0F * (Mzx * BVx_colvec * Sz * BVz_colvec);

        // dE/dSy
        // 'autocalibrationAsymmetric:112' J(idum, 2) = 2*Sy*(Mxy*BVy)^2 + 2*(Sx*BVx*Mxy*BVy) + 2*(Mxy*BVy*Sz*Mxz*BVz) + 2*Sy*BVy^2 + 2*(Sx*Myx*BVx*BVy) + 2*(BVy*Sz*Myz*BVz) + 2*Sy*(Mzy*BVy)^2 + 2*(Sx*Mzx*BVx*Mzy*BVy) + 2*(Mzy*BVy*Sz*BVz);
        c   = Mxy * BVy_colvec;
        b_c = Mzy * BVy_colvec;
        J[ACCEL_SAMPLE_COUNT + idum] = (((((((2.0F * Sy * (c * c) + 2.0F * (Sx * BVx_colvec * Mxy *
                                                                            BVy_colvec)) + 2.0F * (Mxy * BVy_colvec * Sz * Mxz * BVz_colvec)) + 2.0F *
                                            Sy * (BVy_colvec * BVy_colvec)) + 2.0F * (Sx * Myx *
                                                                                      BVx_colvec * BVy_colvec)) + 2.0F * (BVy_colvec * Sz * Myz * BVz_colvec)) +
                                         2.0F * Sy * (b_c * b_c)) + 2.0F * (Sx * Mzx * BVx_colvec *
                                                                            Mzy * BVy_colvec)) + 2.0F * (Mzy * BVy_colvec * Sz * BVz_colvec);

        // dE/dSz
        // 'autocalibrationAsymmetric:115' J(idum, 3) = 2*Sz*(Mxz*BVz)^2 + 2*(Sx*BVx*Mxz*BVz) + 2*(Sy*Mxy*BVy*Mxz*BVz) + 2*Sz*(Myz*BVz)^2 + 2*(Sx*Myx*BVx*Myz*BVz) + 2*(Sy*BVy*Myz*BVz) + 2*Sz*BVz^2 + 2*(Sx*Mzx*BVx*BVz) + 2*(Sy*Mzy*BVy*BVz);
        c   = Mxz * BVz_colvec;
        b_c = Myz * BVz_colvec;
        J[2 * ACCEL_SAMPLE_COUNT + idum] = (((((((2.0F * Sz * (c * c) + 2.0F * (Sx * BVx_colvec * Mxz *
                                                                                BVz_colvec)) + 2.0F * (Sy * Mxy * BVy_colvec * Mxz * BVz_colvec)) + 2.0F *
                                                Sz * (b_c * b_c)) + 2.0F * (Sx * Myx * BVx_colvec * Myz *
                                                                            BVz_colvec)) + 2.0F * (Sy * BVy_colvec * Myz * BVz_colvec)) + 2.0F * Sz *
                                             (BVz_colvec * BVz_colvec)) + 2.0F * (Sx * Mzx * BVx_colvec *
                                                                                  BVz_colvec)) + 2.0F * (Sy * Mzy * BVy_colvec * BVz_colvec);

        // dE/dMxy
        // 'autocalibrationAsymmetric:118' J(idum, 4) = 2*Mxy*(Sy*BVy)^2 + 2*(Sx*BVx*Sy*BVy) + 2*(Sy*BVy*Sz*Mxz*BVz);
        c = Sy * BVy_colvec;
        J[3 * ACCEL_SAMPLE_COUNT + idum] = (2.0F * Mxy * (c * c) + 2.0F * (Sx * BVx_colvec * Sy *
                                                                           BVy_colvec)) + 2.0F * (Sy * BVy_colvec * Sz * Mxz * BVz_colvec);

        // dE/dMxz
        // 'autocalibrationAsymmetric:121' J(idum, 5) = 2*Mxz*(Sz*BVz)^2 + 2*(Sx*BVx*Sz*BVz) + 2*(Sy*Mxy*BVy*Sz*BVz);
        c = Sz * BVz_colvec;
        J[4 * ACCEL_SAMPLE_COUNT + idum] = (2.0F * Mxz * (c * c) + 2.0F * (Sx * BVx_colvec * Sz *
                                                                           BVz_colvec)) + 2.0F * (Sy * Mxy * BVy_colvec * Sz * BVz_colvec);

        // dE/dMyx
        // 'autocalibrationAsymmetric:124' J(idum, 6) = 2*Myx*(Sx*BVx)^2 + 2*(Sx*BVx*Sy*BVy) + 2*(Sx*BVx*Sz*Myz*BVz);
        c = Sx * BVx_colvec;
        J[5 * ACCEL_SAMPLE_COUNT + idum] = (2.0F * Myx * (c * c) + 2.0F * (Sx * BVx_colvec * Sy *
                                                                           BVy_colvec)) + 2.0F * (Sx * BVx_colvec * Sz * Myz * BVz_colvec);

        // dE/dMyz
        // 'autocalibrationAsymmetric:127' J(idum, 7) = 2*Myz*(Sz*BVz)^2 + 2*(Sx*Myx*BVx*Sz*BVz) + 2*(Sy*BVy*Sz*BVz);
        c = Sz * BVz_colvec;
        J[6 * ACCEL_SAMPLE_COUNT + idum] = (2.0F * Myz * (c * c) + 2.0F * (Sx * Myx * BVx_colvec * Sz *
                                                                           BVz_colvec)) + 2.0F * (Sy * BVy_colvec * Sz * BVz_colvec);

        // dE/dMzx
        // 'autocalibrationAsymmetric:130' J(idum, 8) = 2*Mzx*(Sx*BVx)^2 + 2*(Sx*BVx*Sy*Mzy*BVy) + 2*(Sx*BVx*Sz*BVz);
        c = Sx * BVx_colvec;
        J[7 * ACCEL_SAMPLE_COUNT + idum] = (2.0F * Mzx * (c * c) + 2.0F * (Sx * BVx_colvec * Sy * Mzy *
                                                                           BVy_colvec)) + 2.0F * (Sx * BVx_colvec * Sz * BVz_colvec);

        // dE/dMzy
        // 'autocalibrationAsymmetric:133' J(idum, 9) = 2*Mzy*(Sy*BVy)^2 + 2*(Sx*Mzx*BVx*Sy*BVy) + 2*(Sy*Mzy*BVy*Sz*BVz) ;
        c = Sy * BVy_colvec;
        J[8 * ACCEL_SAMPLE_COUNT + idum] = (2.0F * Mzy * (c * c) + 2.0F * (Sx * Mzx * BVx_colvec * Sy *
                                                                           BVy_colvec)) + 2.0F * (Sy * Mzy * BVy_colvec * Sz * BVz_colvec);

        // dE/dBVx
        // 'autocalibrationAsymmetric:136' J(idum, 10) = 2*BVx*Sx^2 + 2*(Sx*Sy*Mxy*BVy) + 2*(Sx*Sz*Mxz*BVz) + 2*BVx*(Sx*Myx)^2 + 2*(Sx*Myx*Sy*BVy) + 2*(Sx*Myx*Sz*Myz*BVz) + 2*BVx*(Sx*Mzx)^2 + 2*(Sx*Mzx*Sy*Mzy*BVy) + 2*(Sx*Mzx*Sz*BVz);
        c   = Sx * Myx;
        b_c = Sx * Mzx;
        J[9 * ACCEL_SAMPLE_COUNT + idum] = (((((((2.0F * BVx_colvec * (Sx * Sx) + 2.0F * (Sx * Sy * Mxy *
                                                                                          BVy_colvec)) + 2.0F * (Sx * Sz * Mxz * BVz_colvec)) + 2.0F * BVx_colvec *
                                                (c * c)) + 2.0F * (Sx * Myx * Sy * BVy_colvec)) + 2.0F *
                                              (Sx * Myx * Sz * Myz * BVz_colvec)) + 2.0F * BVx_colvec *
                                             (b_c * b_c)) + 2.0F * (Sx * Mzx * Sy * Mzy * BVy_colvec)) +
                                           2.0F * (Sx * Mzx * Sz * BVz_colvec);

        // dE/dBVy
        // 'autocalibrationAsymmetric:139' J(idum, 11) = 2*BVy*(Sy*Mxy)^2 + 2*(Sx*BVx*Sy*Mxy) + 2*(Sy*Mxy*Sz*Mxz*BVz) + 2*BVy*Sy^2 + 2*(Sx*Myx*BVx*Sy) + 2*(Sy*Sz*Myz*BVz) + 2*BVy*(Sy*Mzy)^2 + 2*(Sx*Mzx*BVx*Sy*Mzy) + 2*(Sy*Mzy*Sz*BVz);
        c   = Sy * Mxy;
        b_c = Sy * Mzy;
        J[10 * ACCEL_SAMPLE_COUNT + idum] = (((((((2.0F * BVy_colvec * (c * c) + 2.0F * (Sx * BVx_colvec
                                                                                         * Sy * Mxy)) + 2.0F * (Sy * Mxy * Sz * Mxz * BVz_colvec)) + 2.0F *
                                                 BVy_colvec * (Sy * Sy)) + 2.0F * (Sx * Myx * BVx_colvec
                                                                                   * Sy)) + 2.0F * (Sy * Sz * Myz * BVz_colvec)) + 2.0F * BVy_colvec * (b_c *
                                                                                                                                                        b_c)) + 2.0F * (Sx * Mzx * BVx_colvec * Sy * Mzy)) + 2.0F * (Sy * Mzy * Sz
                                                                                                                                                                                                                     * BVz_colvec);

        // dE/dBVz
        // 'autocalibrationAsymmetric:142' J(idum, 12) = 2*BVz*(Sz*Mxz)^2 + 2*(Sx*BVx*Sz*Mxz) + 2*(Sy*Mxy*BVy*Sz*Mxz) + 2*(Sx*Myx*BVx*Sz*Myz) + 2*(Sy*BVy*Sz*Myz) + 2*BVz*Sz^2 + 2*(Sx*Mzx*BVx*Sz) + 2*(Sy*Mzy*BVy*Sz) ;
        c = Sz * Mxz;
        J[11 * ACCEL_SAMPLE_COUNT + idum] = ((((((2.0F * BVz_colvec * (c * c) + 2.0F * (Sx * BVx_colvec *
                                                                                        Sz * Mxz)) + 2.0F * (Sy * Mxy * BVy_colvec * Sz * Mxz)) + 2.0F * (Sx * Myx
                                                                                                                                                          * BVx_colvec * Sz * Myz)) + 2.0F * (Sy * BVy_colvec * Sz * Myz)) + 2.0F *
                                              BVz_colvec * (Sz * Sz)) + 2.0F * (Sx * Mzx * BVx_colvec *
                                                                                Sz)) + 2.0F * (Sy * Mzy * BVy_colvec * Sz);
    }
}


//
// Arguments    : const float V[30]
// float M2[9]
// float B2[3]
// float info[7]
// Return Type  : void
//
void AccelCalibrationFSM::calibrationFitAsymmetric(const float *Vx, const float *Vy, const float *Vz, const float *Vvariance, float M2[9], float B2[3], float info[7])
{
    float X[MODEL_UNKNOWNS];
    float x0[MODEL_UNKNOWNS] = { 1.5f, 1.5f, 1.5f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.1f, 0.5f, 0.5f, 0.5f };

    marquardt(c_accelCalAsymmetricGetRandJaco, x0, MODEL_UNKNOWNS, ACCEL_SAMPLE_COUNT, Vx, Vy, Vz, Vvariance, X, info);

    // Save Outputs
    // 'autocalibrationAsymmetric:56' M2 = [X(1) X(4) X(5); X(6) X(2) X(7); X(8) X(9) X(3)];
    M2[0] = X[0];
    M2[3] = X[3];
    M2[6] = X[4];
    M2[1] = X[5];
    M2[4] = X[1];
    M2[7] = X[6];
    M2[2] = X[7];
    M2[5] = X[8];
    M2[8] = X[2];

    // 'autocalibrationAsymmetric:57' B2 = [X(10);X(11);X(12)];
    B2[0] = X[9];
    B2[1] = X[10];
    B2[2] = X[11];
}


// Test calibration routine against expected on change to the implementation
int AccelCalibrationFSM::testCalibrationFitAsymmetric()
{
    float M2[9], B2[3], info[7];

    const float Vx[14] = { -0.9853F, 0.0222F, 1.0016F, -0.0830F, -0.6008F, -0.4595F, 0.5499F, 0.5156F, 0.6500F, -0.0787F, -0.7706F, -0.7534F, 0.1190F, 0.0010F };
    const float Vy[14] = { 0.0919F, -0.9797F, 0.0885F, 1.0172F, 0.0547F, -0.5045F, -0.3154F, -0.8421F, 0.7107F, 0.6375F, 0.6505F, -0.6300F, 0.0346F, 0.0091F };
    const float Vz[14] = { -0.0628F, 0.0156F, -0.0333F, 0.0231F, -0.7211F, -0.6371F, -0.6935F, 0.0357F, -0.2654F, -0.7126F, -0.0004F, 0.0281F, 1.0775F, -0.9257F };
    const float Vvariance[14] = { 0.0008F, 0.0014F, 0.0018F, 0.0015F, 0.0021F, 0.0020F, 0.0018F, 0.0039F, 0.0021F, 0.0029F, 0.0028F, 0.0075F, 0.0052F, 0.0001F };

    marquardt_initialize();
    calibrationFitAsymmetric(Vx, Vy, Vz, Vvariance, M2, B2, info);

    float m[9]     = { 1.00065327F, 0.0037253273F, -0.00103992806F, -0.000366205F, 1.00172365F, 0.02697104F, -0.00293104F, -0.025972F, 0.99994576F };

    float b[3]     = { 0.0083303F, 0.0225799214F, 0.0757183954F };


    int compare_ok = true;
    for (int idx = 0; idx < 9 && compare_ok == true; idx++) {
        if (fabsf(M2[idx] - m[idx]) > 0.0001F) {
            compare_ok = false;
        }
    }
    for (int idx = 0; idx < 3 && compare_ok == true; idx++) {
        if (fabsf(B2[idx] - b[idx]) > 0.0001F) {
            compare_ok = false;
        }
    }

    if (compare_ok) {
        return STATUS_OK;
    }
    return STATUS_ERROR;
}


AccelCalibrationFSM::AccelCalibrationFSM()
    : mAccelData(0), mSaveToFlashPending(false)
{}

// Private types

// Private functions
// Public API methods
/**
 * Initialise the module, called on startup
 * \returns STATUS_OK on success
 */
#define CALLBACK_PRIORITY CALLBACK_PRIORITY_LOW
#define CBTASK_PRIORITY   CALLBACK_TASK_FLIGHTCONTROL
#define STACK_SIZE_BYTES  2 * 2048 // TODO REDUCE

int32_t AccelCalibrationFSM::Initialize()
{
    if (mAccelData == 0) {
        mAccelData = (AccelCalibrationFSMData_T *)pios_malloc(sizeof(AccelCalibrationFSMData_T));
        PIOS_Assert(mAccelData);
    }
    memset(mAccelData, 0, sizeof(AccelCalibrationFSMData_T));
    calibrationCalcTaskCBInfo = PIOS_CALLBACKSCHEDULER_Create(&AccelCalibrationFSM::calibrationCalcTask, CALLBACK_PRIORITY, CBTASK_PRIORITY, CALLBACKINFO_RUNNING_CALIBRATION, STACK_SIZE_BYTES);
    initFSM();


    return STATUS_OK;
}

void AccelCalibrationFSM::Inactive(void)
{
    memset(mAccelData, 0, sizeof(AccelCalibrationFSMData_T));
    initFSM();

    mAccelData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_ACCELCALIBRATION;
    mAccelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    mAccelData->caliStatus.AccelCalibrationState = CALIBRATIONSTATUS_ACCELCALIBRATIONSTATE_IDLE;
    mAccelData->caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;
    CalibrationStatusSet(&(mAccelData->caliStatus));
}

// Initialise the FSM
void AccelCalibrationFSM::initFSM(void)
{
    mAccelData->currentState = ACCELCAL_STATE_INACTIVE;
}

void AccelCalibrationFSM::Activate()
{
    memset(mAccelData, 0, sizeof(AccelCalibrationFSMData_T));
    mAccelData->currentState = ACCELCAL_STATE_INACTIVE;

    setState(ACCELCAL_STATE_START);
}

CalibrationFSMState_T AccelCalibrationFSM::GetCurrentState(void)
{
    if (mAccelData->currentState == ACCELCAL_STATE_INACTIVE) {
        return CALFSM_STATE_INACTIVE;
    } else {
        return CALFSM_STATE_ACTIVE;
    }
}

void AccelCalibrationFSM::Update(CalibrationFSMDataTypes_T dataUpdateAvail)
{
    // check state and return if completed, inactive or error
    if (mAccelData->currentState == ACCELCAL_STATE_INACTIVE) {
        return;
    }

    mAccelData->updateReason = CALFSM_DATA_NONE;
    if ((mAccelData->currentState > ACCELCAL_STATE_START && mAccelData->currentState < ACCELCAL_STATE_CALC)) {
        if ((dataUpdateAvail == CALFSM_DATA_ACCELSENSOR)) {
            mAccelData->updateReason = dataUpdateAvail;
            runState();
        }
    } else {
        if (dataUpdateAvail == CALFSM_DATA_TIMER) {
            mAccelData->updateReason = dataUpdateAvail;
            runState();
        }
    }
}

int32_t AccelCalibrationFSM::runState(void)
{
    uint8_t flTimeout = false;


#ifdef ACCELCAL_DEBUG
    PIOS_DEBUG_PinHigh(PIN0_stateupdate);
#endif

    mAccelData->stateRunCount++;

    if (mAccelData->updateReason == CALFSM_DATA_TIMER) {
        mAccelData->stateRunCount++;
        if (mAccelData->stateTimeoutCount > 0 && mAccelData->stateRunCount > mAccelData->stateTimeoutCount) {
            flTimeout = true;
        }
    }

    // If the current state has a static function, call it
    if (sAccelStateTable[mAccelData->currentState].run) {
        (this->*sAccelStateTable[mAccelData->currentState].run)(flTimeout);
    }
#ifdef ACCELCAL_DEBUG
    PIOS_DEBUG_PinLow(PIN0_stateupdate);
#endif
    return STATUS_OK;
}

// Set the new state and perform setup for subsequent state run calls
// This is called by state run functions on event detection that drive
// state transitions.
void AccelCalibrationFSM::setState(AccelCalibrationFSM_State_T newState)
{
    mAccelData->currentState      = newState;

    // Restart state timer counter
    mAccelData->stateRunCount     = 0;

    // Reset state timeout to disabled/zero
    mAccelData->stateTimeoutCount = 0;

    if (sAccelStateTable[mAccelData->currentState].setup) {
        (this->*sAccelStateTable[mAccelData->currentState].setup)();
    }
}


// Timeout utility function for use by state init implementations
void AccelCalibrationFSM::setStateTimeout(int32_t count)
{
    mAccelData->stateTimeoutCount = count;
}
