/*
 ******************************************************************************
 *
 * @file       magcalibration.cpp
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Mag calibration implementation
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#include <project_xflight.h>
#include "magcalibration.h"

#include <math.h>
#include <mathmisc.h>
#include <alarms.h>
#include <CoordinateConversions.h>
#include <sin_lookup.h>
#include "attitudesettings.h"
#include "attitudestate.h"
#include "calibrationcontrol.h"
#include "calibrationstatus.h"
#include "objectpersistence.h"
#include "calibrationmags.h"
#include "auxmagsensor.h"
#include "magsensor.h"
#include "accelsensor.h"
#include "accelgyrosettings.h"
#include "homelocation.h"

#include <libmatrix/matrix.h>


// C++ includes
#include "libmatrix/vector3.h"

MagCalibration::MagCalibration()
    : buffer(0), progress(0.0f), allow4INVSolver(true)
{
    buffer = (MagneticBuffer *)pios_malloc(sizeof(MagneticBuffer));
}

MagCalibration::~MagCalibration()
{
    pios_free(buffer);
    buffer = 0;
}

void MagCalibration::initialise()
{
    sample.zero();
    progress = 0.0f;

    // initialize the calibration hard and soft iron estimate to null
    f3x3matrixAeqI(calData.finvW);
    calData.fV[Xidx]     = calData.fV[Yidx] = calData.fV[Zidx] = 0.0F;
    calData.fB = 0.5;
    calData.fFourBsq     = 4.0F * calData.fB * calData.fB;
    calData.fFitErrorpc  = 50.0F;
    calData.iValidMagCal = MAGCAL_STAGE_NOCALC;
    calData.solver = MAGCAL_STAGE_NOCALC;


    // set magnetic buffer->index to invalid value -1 to denote invalid
    buffer->iMagBufferCount = 0;
    for (int j = 0; j < MAGBUFFSIZEX; j++) {
        for (int k = 0; k < MAGBUFFSIZEY; k++) {
            buffer->index[j][k] = -1;
            (buffer->sampleData[j][k]).zero();
        }
    }

    // initialize the array of (MAGBUFFSIZEX - 1) elements of 100 * tangents used for buffer->indexing
    // entries cover the range 100 * tan(-PI/2 + PI/MAGBUFFSIZEX), 100 * tan(-PI/2 + 2*PI/MAGBUFFSIZEX) to
    // 100 * tan(-PI/2 + (MAGBUFFSIZEX - 1) * PI/MAGBUFFSIZEX).
    // for MAGBUFFSIZEX=12, the entries range in value from -373 to +373
    for (int j = 0; j < (MAGBUFFSIZEX - 1); j++) {
        buffer->tanarray[j] = (int16_t)(100.0F * tanf(PI * (-0.5F + (float)(j + 1) / (float)MAGBUFFSIZEX)));
    }
}


void MagCalibration::processSample(uint32_t loopcounter, float mx, float my, float mz, float ax, float ay, float az)
{
    sample.x = mx;
    sample.y = my;
    sample.z = mz;

    // local variables
    int32_t i; // counter
    int16_t itanj, itank; // indexing accelerometer ratios
    int8_t j, k, l, m; // counters

    // calculate the magnetometer buffer->bins from the accelerometer tangent ratios
    if (is_zero(az) || is_zero(ax) || is_zero(az) || loopcounter == 0) {
        return;
    }
    itanj = (100 * (int32_t)ax) / ((int32_t)az);
    itank = (100 * (int32_t)ay) / ((int32_t)az);

    // map tangent ratios to bins j and k using equal angle bins: C guarantees left to right execution of the test
    // and add an offset of MAGBUFFSIZEX bins to k to mimic atan2 on this ratio
    // j will vary from 0 to MAGBUFFSIZEX - 1 and k from 0 to 2 * MAGBUFFSIZEX - 1
    j = k = 0;
    while ((j < (MAGBUFFSIZEX - 1) && (itanj >= buffer->tanarray[j]))) {
        j++;
    }
    while ((k < (MAGBUFFSIZEX - 1) && (itank >= buffer->tanarray[k]))) {
        k++;
    }
    if (ax < 0) {
        k += MAGBUFFSIZEX;
    }


    // case 1: buffer->is full and this bin has a measurement: over-write without increasing number of measurements
    // this is the most common option at run time
    if ((buffer->iMagBufferCount == MAXMEASUREMENTS) && (buffer->index[j][k] != -1)) {
        // store the fast (unaveraged at typically 200Hz) integer magnetometer reading into the buffer->bin j, k
        buffer->sampleData[j][k] = sample;
        buffer->index[j][k] = loopcounter;
        return;
    } // end case 1

    // case 2: the buffer->is full and this bin does not have a measurement: store and retire the oldest
    // this is the second most common option at run time
    if ((buffer->iMagBufferCount == MAXMEASUREMENTS) && (buffer->index[j][k] == -1)) {
        // store the fast (unaveraged at typically 200Hz) integer magnetometer reading into the buffer->bin j, k
        buffer->sampleData[j][k] = sample;
        buffer->index[j][k] = loopcounter;

        // set l and m to the oldest active entry and disable it
        i = loopcounter;
        l = m = 0; // to avoid compiler complaint
        for (j = 0; j < MAGBUFFSIZEX; j++) {
            for (k = 0; k < MAGBUFFSIZEY; k++) {
                // check if the time stamp is older than the oldest found so far (normally fails this test)
                if (buffer->index[j][k] < i) {
                    // check if this bin is active (normally passes this test)
                    if (buffer->index[j][k] != -1) {
                        // set l and m to the indices of the oldest entry found so far
                        l = j;
                        m = k;
                        // set i to the time stamp of the oldest entry found so far
                        i = buffer->index[l][m];
                    } // end of test for active
                } // end of test for older
            } // end of loop over k
        } // end of loop over j

        // deactivate the oldest measurement (no need to zero the measurement data)
        buffer->index[l][m] = -1;
        return;
    } // end case 2

    // case 3: buffer->is not full and this bin is empty: store and increment number of measurements
    if ((buffer->iMagBufferCount < MAXMEASUREMENTS) && (buffer->index[j][k] == -1)) {
        // store the fast (unaveraged at typically 200Hz) integer magnetometer reading into the buffer->bin j, k
        buffer->sampleData[j][k] = sample;
        buffer->index[j][k] = loopcounter;
        (buffer->iMagBufferCount)++;
        return;
    } // end case 3

    // case 4: buffer->is not full and this bin has a measurement: over-write
    if ((buffer->iMagBufferCount < MAXMEASUREMENTS) && (buffer->index[j][k] != -1)) {
        buffer->sampleData[j][k] = sample;
        buffer->index[j][k] = loopcounter;
        return;
    } // end case 4

    sample.zero();
    // this line should be unreachable
}


bool MagCalibration::calcCalibration(float Be_length, float calibrationMatrix[], float bias_arg[])
{
    switch (getCalStage()) {
    case MAGCAL_STAGE_NOCALC:
        return false; // more sampling required

        break;
    case MAGCAL_STAGE_4CAL:
        if (!allow4INVSolver) {
            return false; // more sampling required
        }
        calData.solver = MAGCAL_STAGE_4CAL;
        fUpdateCalibration4INV(Be_length);
        break;
    case MAGCAL_STAGE_7CAL:
        calData.solver = MAGCAL_STAGE_7CAL;
        fUpdateCalibration7EIG(Be_length);
        break;
    case MAGCAL_STAGE_10CAL:
        calData.solver = MAGCAL_STAGE_10CAL;
        fUpdateCalibration10EIG(Be_length);
        break;
    }


    // always accept the calibration if i) no previous calibration exists ii) the calibration fit is reduced or
    // an improved solver was used giving a good trial calibration (4% or under)

    calData.trialAccepted = false;
    if ((calData.iValidMagCal == MAGCAL_STAGE_NOCALC) ||
        (calData.ftrFitErrorpc <= calData.fFitErrorpc) ||
        ((calData.solver > calData.iValidMagCal) && (calData.ftrFitErrorpc <= 4.0F))) {
        // accept the new calibration solution
        calData.trialAccepted = true;
        calData.iValidMagCal  = calData.solver;
        calData.fFitErrorpc   = calData.ftrFitErrorpc;
        calData.fB = calData.ftrB;
        for (int i = CHX; i <= CHZ; i++) {
            calData.fV[i] = calData.ftrV[i];
            for (int j = CHX; j <= CHZ; j++) {
                calData.finvW[i][j] = calData.ftrinvW[i][j];
            }
        }
        calData.calibrationRejectedCount = 0;
    } // end of test to accept the new calibration
    else {
        calData.calibrationRejectedCount++;
        return false;
    }


    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R0C0] = calData.finvW[Xidx][Xidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R0C1] = calData.finvW[Xidx][Yidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R0C2] = calData.finvW[Xidx][Zidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R1C0] = calData.finvW[Yidx][Xidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R1C1] = calData.finvW[Yidx][Yidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R1C2] = calData.finvW[Yidx][Zidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R2C0] = calData.finvW[Zidx][Xidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R2C1] = calData.finvW[Zidx][Yidx] / calData.fB;
    calibrationMatrix[CALIBRATIONMAGS_ONBOARDTRANSFORM_R2C2] = calData.finvW[Zidx][Zidx] / calData.fB;

    bias_arg[CALIBRATIONMAGS_ONBOARDBIAS_X] = calData.fV[Xidx];
    bias_arg[CALIBRATIONMAGS_ONBOARDBIAS_Y] = calData.fV[Yidx];
    bias_arg[CALIBRATIONMAGS_ONBOARDBIAS_Z] = calData.fV[Zidx];


    return true;
}


float MagCalibration::percentSampled()
{
    if (buffer->iMagBufferCount < MINMEASUREMENTS4CAL) {
        // stage 1 sampling up to run 4cal
        progress = (float)buffer->iMagBufferCount / (float)MINMEASUREMENTS4CAL * 25.0F;
    } else if (buffer->iMagBufferCount < MINMEASUREMENTS7CAL) {
        // stage 2 samlping up to run 7cal
        progress = 25.0f + 25.0f * ((float)buffer->iMagBufferCount - (float)MINMEASUREMENTS4CAL) / (float)(MINMEASUREMENTS7CAL - MINMEASUREMENTS4CAL);
    } else if (buffer->iMagBufferCount < MINMEASUREMENTS10CAL) {
        // stage 3 sampling up to run 10cal
        progress = 50.0f + 25.0f * ((float)buffer->iMagBufferCount - (float)MINMEASUREMENTS7CAL) / (float)(MINMEASUREMENTS10CAL - MINMEASUREMENTS7CAL);
    } else {
        progress += 0.0001f;
    }
    return progress;
}

MagCalibrationStage_T MagCalibration::getCalStage()
{
    if (buffer->iMagBufferCount < MINMEASUREMENTS4CAL) {
        return MAGCAL_STAGE_NOCALC;
    } else if (allow4INVSolver && buffer->iMagBufferCount < MINMEASUREMENTS7CAL) {
        return MAGCAL_STAGE_4CAL;
    } else if (buffer->iMagBufferCount < MINMEASUREMENTS10CAL) {
        return MAGCAL_STAGE_7CAL;
    } else {
        return MAGCAL_STAGE_10CAL;
    }
}

int16_t MagCalibration::sampleCount()
{
    return buffer->iMagBufferCount;
}


// Solvers

// useful multiplicative conversion constants
#define FDEGTORAD    0.01745329251994F             // degrees to radians conversion = pi / 180
#define FRADTODEG    57.2957795130823F             // radians to degrees conversion = 180 / pi
#define FRECIP180    0.0055555555555F              // multiplicative factor 1/180
#define ONETHIRD     0.33333333F                    // one third
#define ONESIXTH     0.166666667F                   // one sixth
#define ONETWELFTH   0.0833333333F                // one twelfth
#define ONEOVER48    0.02083333333F                // 1 / 48
#define ONEOVER120   0.0083333333F                // 1 / 120
#define ONEOVER3840  0.0002604166667F    // 1 / 3840
#define ONEOVERROOT2 0.707106781F // 1/sqrt(2)
#define ROOT3OVER2   0.866025403784F              // sqrt(3)/2

// 4 element calibration using 4x4 matrix inverse
void MagCalibration::fUpdateCalibration4INV(const float Be_length)
{
    // local variables
    float fBs2; // fBs[CHX]^2+fBs[CHY]^2+fBs[CHZ]^2
    float fSumBs4; // sum of fBs2
    float fscaling; // set to FUTPERCOUNT * FMATRIXSCALING
    float fE; // error function = r^T.r
    Vector3f iOffset; // offset to remove large DC hard iron bias in matrix

    int16_t iCount; // number of measurements counted
    int8_t ierror; // matrix inversion error flag
    int8_t i, j, k, l; // loop counters

    // working arrays for 4x4 matrix inversion
    float *pfRows[4];
    int8_t iColInd[4];
    int8_t iRowInd[4];
    int8_t iPivot[4];

    // compute fscaling to reduce multiplications later
    // fscaling = pthisMag->fuTPerCount / DEFAULTB;
    // fs code was using counts instead of uT
    fscaling = 1.0F / Be_length;


    // the trial inverse soft iron matrix invW always equals the identity matrix for 4 element calibration
    f3x3matrixAeqI(calData.ftrinvW);

    // zero fSumBs4=Y^T.Y, fvecB=X^T.Y (4x1) and on and above diagonal elements of fmatA=X^T*X (4x4)
    fSumBs4 = 0.0F;
    for (i = 0; i < 4; i++) {
        calData.fvecB[i] = 0.0F;
        for (j = i; j < 4; j++) {
            calData.fmatA[i][j] = 0.0F;
        }
    }

    // the offsets are guaranteed to be set from the first element but to avoid compiler error
    iOffset[CHX] = iOffset[CHY] = iOffset[CHZ] = 0;

    // use entries from magnetic buffer to compute matrices
    iCount = 0;
    for (j = 0; j < MAGBUFFSIZEX; j++) {
        for (k = 0; k < MAGBUFFSIZEY; k++) {
            if (buffer->index[j][k] != -1) {
                // use first valid magnetic buffer entry as estimate (in counts) for offset
                if (iCount == 0) {
                    iOffset = buffer->sampleData[j][k];
                }

                // store scaled and offset fBs[XYZ] in fvecA[0-2] and fBs[XYZ]^2 in fvecA[3-5]
                for (l = CHX; l <= CHZ; l++) {
                    calData.fvecA[l]     = ((buffer->sampleData[j][k])[l] - iOffset[l]) * fscaling;
                    calData.fvecA[l + 3] = calData.fvecA[l] * calData.fvecA[l];
                }

                // calculate fBs2 = fBs[CHX]^2 + fBs[CHY]^2 + fBs[CHZ]^2 (scaled uT^2)
                fBs2     = calData.fvecA[3] + calData.fvecA[4] + calData.fvecA[5];

                // accumulate fBs^4 over all measurements into fSumBs4=Y^T.Y
                fSumBs4 += fBs2 * fBs2;

                // now we have fBs2, accumulate fvecB[0-2] = X^T.Y =sum(fBs2.fBs[XYZ])
                for (l = CHX; l <= CHZ; l++) {
                    calData.fvecB[l] += calData.fvecA[l] * fBs2;
                }

                // accumulate fvecB[3] = X^T.Y =sum(fBs2)
                calData.fvecB[3]    += fBs2;

                // accumulate on and above-diagonal terms of fmatA = X^T.X ignoring fmatA[3][3]
                calData.fmatA[0][0] += calData.fvecA[CHX + 3];
                calData.fmatA[0][1] += calData.fvecA[CHX] * calData.fvecA[CHY];
                calData.fmatA[0][2] += calData.fvecA[CHX] * calData.fvecA[CHZ];
                calData.fmatA[0][3] += calData.fvecA[CHX];
                calData.fmatA[1][1] += calData.fvecA[CHY + 3];
                calData.fmatA[1][2] += calData.fvecA[CHY] * calData.fvecA[CHZ];
                calData.fmatA[1][3] += calData.fvecA[CHY];
                calData.fmatA[2][2] += calData.fvecA[CHZ + 3];
                calData.fmatA[2][3] += calData.fvecA[CHZ];

                // increment the counter for next iteration
                iCount++;
            }
        }
    }

    // set the last element of the measurement matrix to the number of buffer elements used
    calData.fmatA[3][3]     = (float)iCount;

    // store the number of measurements accumulated (defensive programming, should never be needed)
    buffer->iMagBufferCount = iCount;

    // use above diagonal elements of symmetric fmatA to set both fmatB and fmatA to X^T.X
    for (i = 0; i < 4; i++) {
        for (j = i; j < 4; j++) {
            calData.fmatB[i][j] = calData.fmatB[j][i] = calData.fmatA[j][i] = calData.fmatA[i][j];
        }
    }

    // calculate in situ inverse of fmatB = inv(X^T.X) (4x4) while fmatA still holds X^T.X
    for (i = 0; i < 4; i++) {
        pfRows[i] = calData.fmatB[i];
    }
    fmatrixAeqInvA(pfRows, iColInd, iRowInd, iPivot, 4, &ierror);

    // calculate fvecA = solution beta (4x1) = inv(X^T.X).X^T.Y = fmatB * fvecB
    for (i = 0; i < 4; i++) {
        calData.fvecA[i] = 0.0F;
        for (k = 0; k < 4; k++) {
            calData.fvecA[i] += calData.fmatB[i][k] * calData.fvecB[k];
        }
    }

    // calculate P = r^T.r = Y^T.Y - 2 * beta^T.(X^T.Y) + beta^T.(X^T.X).beta
    // = fSumBs4 - 2 * fvecA^T.fvecB + fvecA^T.fmatA.fvecA
    // first set P = Y^T.Y - 2 * beta^T.(X^T.Y) = fSumBs4 - 2 * fvecA^T.fvecB
    fE = 0.0F;
    for (i = 0; i < 4; i++) {
        fE += calData.fvecA[i] * calData.fvecB[i];
    }
    fE = fSumBs4 - 2.0F * fE;

    // set fvecB = (X^T.X).beta = fmatA.fvecA
    for (i = 0; i < 4; i++) {
        calData.fvecB[i] = 0.0F;
        for (k = 0; k < 4; k++) {
            calData.fvecB[i] += calData.fmatA[i][k] * calData.fvecA[k];
        }
    }

    // complete calculation of P by adding beta^T.(X^T.X).beta = fvecA^T * fvecB
    for (i = 0; i < 4; i++) {
        fE += calData.fvecB[i] * calData.fvecA[i];
    }

    // compute the hard iron vector (in uT but offset and scaled by FMATRIXSCALING)
    for (l = CHX; l <= CHZ; l++) {
        calData.ftrV[l] = 0.5F * calData.fvecA[l];
    }

    // compute the scaled geomagnetic field strength B (in uT but scaled by FMATRIXSCALING)
    calData.ftrB = sqrtf(calData.fvecA[3] + calData.ftrV[CHX] * calData.ftrV[CHX] +
                         calData.ftrV[CHY] * calData.ftrV[CHY] + calData.ftrV[CHZ] * calData.ftrV[CHZ]);

    // calculate the trial fit error (percent) normalized to number of measurements and scaled geomagnetic field strength
    calData.ftrFitErrorpc = sqrtf(fE / (float)buffer->iMagBufferCount) * 100.0F /
                            (2.0F * calData.ftrB * calData.ftrB);

    // correct the hard iron estimate for FMATRIXSCALING and the offsets applied (result in uT)
    for (l = CHX; l <= CHZ; l++) {
        calData.ftrV[l] = calData.ftrV[l] * Be_length + (float)iOffset[l];
    }
}

// 7 element calibration using direct eigen-decomposition
void MagCalibration::fUpdateCalibration7EIG(const float Be_length)
{
    // local variables
    float det; // matrix determinant
    float fscaling; // set to FUTPERCOUNT * FMATRIXSCALING
    float ftmp; // scratch variable
    Vector3f iOffset; // offset to remove large DC hard iron bias in matrix
    int16_t iCount; // number of measurements counted
    int8_t i, j, k, l, m, n; // loop counters

    // compute fscaling to reduce multiplications later
    fscaling = 1.0F / Be_length;


    // the offsets are guaranteed to be set from the first element but to avoid compiler error
    iOffset[CHX] = iOffset[CHY] = iOffset[CHZ] = 0;

    // zero the on and above diagonal elements of the 7x7 symmetric measurement matrix fmatA
    for (m = 0; m < 7; m++) {
        for (n = m; n < 7; n++) {
            calData.fmatA[m][n] = 0.0F;
        }
    }

    // add megnetic buffer entries into product matrix fmatA
    iCount = 0;
    for (j = 0; j < MAGBUFFSIZEX; j++) {
        for (k = 0; k < MAGBUFFSIZEY; k++) {
            if (buffer->index[j][k] != -1) {
                // use first valid magnetic buffer entry as offset estimate (bit counts)
                if (iCount == 0) {
                    iOffset = buffer->sampleData[j][k];
                }

                // apply the offset and scaling and store in fvecA
                for (l = CHX; l <= CHZ; l++) {
                    calData.fvecA[l + 3] = ((buffer->sampleData[j][k])[l] - iOffset[l]) * fscaling;
                    calData.fvecA[l]     = calData.fvecA[l + 3] * calData.fvecA[l + 3];
                }

                // accumulate the on-and above-diagonal terms of calData.fmatA=Sigma{fvecA^T * fvecA}
                // with the exception of fmatA[6][6] which will sum to the number of measurements
                // and remembering that fvecA[6] equals 1.0F
                // update the right hand column [6] of fmatA except for fmatA[6][6]
                for (m = 0; m < 6; m++) {
                    calData.fmatA[m][6] += calData.fvecA[m];
                }
                // update the on and above diagonal terms except for right hand column 6
                for (m = 0; m < 6; m++) {
                    for (n = m; n < 6; n++) {
                        calData.fmatA[m][n] += calData.fvecA[m] * calData.fvecA[n];
                    }
                }

                // increment the measurement counter for the next iteration
                iCount++;
            }
        }
    }

    // finally set the last element fmatA[6][6] to the number of measurements
    calData.fmatA[6][6]     = (float)iCount;

    // store the number of measurements accumulated (defensive programming, should never be needed)
    buffer->iMagBufferCount = iCount;

    // copy the above diagonal elements of fmatA to below the diagonal
    for (m = 1; m < 7; m++) {
        for (n = 0; n < m; n++) {
            calData.fmatA[m][n] = calData.fmatA[n][m];
        }
    }

    // set tmpA7x1 to the unsorted eigenvalues and fmatB to the unsorted eigenvectors of fmatA
    eigencompute(calData.fmatA, calData.fvecA, calData.fmatB, 7);

    // find the smallest eigenvalue
    j = 0;
    for (i = 1; i < 7; i++) {
        if (calData.fvecA[i] < calData.fvecA[j]) {
            j = i;
        }
    }

    // set ellipsoid matrix A to the solution vector with smallest eigenvalue, compute its determinant
    // and the hard iron offset (scaled and offset)
    f3x3matrixAeqScalar(calData.fA, 0.0F);
    det = 1.0F;
    for (l = CHX; l <= CHZ; l++) {
        calData.fA[l][l] = calData.fmatB[l][j];
        det *= calData.fA[l][l];
        calData.ftrV[l]  = -0.5F * calData.fmatB[l + 3][j] / calData.fA[l][l];
    }

    // negate A if it has negative determinant
    if (det < 0.0F) {
        f3x3matrixAeqMinusA(calData.fA);
        calData.fmatB[6][j] = -calData.fmatB[6][j];
        det = -det;
    }

    // set ftmp to the square of the trial geomagnetic field strength B (counts times FMATRIXSCALING)
    ftmp = -calData.fmatB[6][j];
    for (l = CHX; l <= CHZ; l++) {
        ftmp += calData.fA[l][l] * calData.ftrV[l] * calData.ftrV[l];
    }

    // calculate the trial normalized fit error as a percentage
    calData.ftrFitErrorpc = 50.0f * sqrtf(fabsf(calData.fvecA[j]) / (float)buffer->iMagBufferCount) / fabsf(ftmp);

    // normalize the ellipsoid matrix A to unit determinant
    f3x3matrixAeqAxScalar(calData.fA, powf(det, -(ONETHIRD)));

    // convert the geomagnetic field strength B into uT for normalized soft iron matrix A and normalize
    calData.ftrB = sqrtf(fabsf(ftmp)) * powf(det, -(ONESIXTH));

    // compute trial invW from the square root of A also with normalized determinant and hard iron offset in uT
    f3x3matrixAeqI(calData.ftrinvW);
    for (l = CHX; l <= CHZ; l++) {
        calData.ftrinvW[l][l] = sqrtf(fabsf(calData.fA[l][l]));
        calData.ftrV[l] = calData.ftrV[l] * Be_length + (float)iOffset[l];
    }
}

// 10 element calibration using direct eigen-decomposition
void MagCalibration::fUpdateCalibration10EIG(const float Be_length)
{
    // local variables
    float det; // matrix determinant
    float fscaling; // set to FUTPERCOUNT * FMATRIXSCALING
    float ftmp; // scratch variable
    Vector3f iOffset; // offset to remove large DC hard iron bias in matrix
    int16_t iCount; // number of measurements counted
    int8_t i, j, k, l, m, n; // loop counters

    // compute fscaling to reduce multiplications later
    fscaling     = 1.0F / Be_length;

    // the offsets are guaranteed to be set from the first element but to avoid compiler error
    iOffset[CHX] = iOffset[CHY] = iOffset[CHZ] = 0;

    // zero the on and above diagonal elements of the 10x10 symmetric measurement matrix fmatA
    for (m = 0; m < 10; m++) {
        for (n = m; n < 10; n++) {
            calData.fmatA[m][n] = 0.0F;
        }
    }

    // add magnetic buffer entries into the 10x10 product matrix fmatA
    iCount = 0;
    for (j = 0; j < MAGBUFFSIZEX; j++) {
        for (k = 0; k < MAGBUFFSIZEY; k++) {
            if (buffer->index[j][k] != -1) {
                // use first valid magnetic buffer entry as estimate for offset to help solution (bit counts)
                if (iCount == 0) {
                    iOffset = buffer->sampleData[j][k];
                }

                // apply the fixed offset and scaling and enter into fvecA[6-8]
                for (l = CHX; l <= CHZ; l++) {
                    calData.fvecA[l + 6] = ((buffer->sampleData[j][k])[l] - iOffset[l]) * fscaling;
                }

                // compute measurement vector elements fvecA[0-5] from fvecA[6-8]
                calData.fvecA[0] = calData.fvecA[6] * calData.fvecA[6];
                calData.fvecA[1] = 2.0F * calData.fvecA[6] * calData.fvecA[7];
                calData.fvecA[2] = 2.0F * calData.fvecA[6] * calData.fvecA[8];
                calData.fvecA[3] = calData.fvecA[7] * calData.fvecA[7];
                calData.fvecA[4] = 2.0F * calData.fvecA[7] * calData.fvecA[8];
                calData.fvecA[5] = calData.fvecA[8] * calData.fvecA[8];

                // accumulate the on-and above-diagonal terms of fmatA=Sigma{fvecA^T * fvecA}
                // with the exception of fmatA[9][9] which equals the number of measurements
                // update the right hand column [9] of fmatA[0-8][9] ignoring fmatA[9][9]
                for (m = 0; m < 9; m++) {
                    calData.fmatA[m][9] += calData.fvecA[m];
                }
                // update the on and above diagonal terms of fmatA ignoring right hand column 9
                for (m = 0; m < 9; m++) {
                    for (n = m; n < 9; n++) {
                        calData.fmatA[m][n] += calData.fvecA[m] * calData.fvecA[n];
                    }
                }

                // increment the measurement counter for the next iteration
                iCount++;
            }
        }
    }

    // set the last element fmatA[9][9] to the number of measurements
    calData.fmatA[9][9]     = (float)iCount;

    // store the number of measurements accumulated (defensive programming, should never be needed)
    buffer->iMagBufferCount = iCount;

    // copy the above diagonal elements of symmetric product matrix fmatA to below the diagonal
    for (m = 1; m < 10; m++) {
        for (n = 0; n < m; n++) {
            calData.fmatA[m][n] = calData.fmatA[n][m];
        }
    }

    // set calData.gfvecA to the unsorted eigenvalues and fmatB to the unsorted normalized eigenvectors of fmatA
    eigencompute(calData.fmatA, calData.fvecA, calData.fmatB, 10);

    // set ellipsoid matrix A from elements of the solution vector column j with smallest eigenvalue
    j = 0;
    for (i = 1; i < 10; i++) {
        if (calData.fvecA[i] < calData.fvecA[j]) {
            j = i;
        }
    }
    calData.fA[0][0] = calData.fmatB[0][j];
    calData.fA[0][1] = calData.fA[1][0] = calData.fmatB[1][j];
    calData.fA[0][2] = calData.fA[2][0] = calData.fmatB[2][j];
    calData.fA[1][1] = calData.fmatB[3][j];
    calData.fA[1][2] = calData.fA[2][1] = calData.fmatB[4][j];
    calData.fA[2][2] = calData.fmatB[5][j];

    // negate entire solution if A has negative determinant
    det = f3x3matrixDetA(calData.fA);
    if (det < 0.0F) {
        f3x3matrixAeqMinusA(calData.fA);
        calData.fmatB[6][j] = -calData.fmatB[6][j];
        calData.fmatB[7][j] = -calData.fmatB[7][j];
        calData.fmatB[8][j] = -calData.fmatB[8][j];
        calData.fmatB[9][j] = -calData.fmatB[9][j];
        det = -det;
    }

    // compute the inverse of the ellipsoid matrix
    f3x3matrixAeqInvSymB(calData.finvA, calData.fA);

    // compute the trial hard iron vector in offset bit counts times FMATRIXSCALING
    for (l = CHX; l <= CHZ; l++) {
        calData.ftrV[l] = 0.0F;
        for (m = CHX; m <= CHZ; m++) {
            calData.ftrV[l] += calData.finvA[l][m] * calData.fmatB[m + 6][j];
        }
        calData.ftrV[l] *= -0.5F;
    }

    // compute the trial geomagnetic field strength B in bit counts times FMATRIXSCALING
    calData.ftrB = sqrtf(fabsf(calData.fA[0][0] * calData.ftrV[CHX] * calData.ftrV[CHX] +
                               2.0F * calData.fA[0][1] * calData.ftrV[CHX] * calData.ftrV[CHY] +
                               2.0F * calData.fA[0][2] * calData.ftrV[CHX] * calData.ftrV[CHZ] +
                               calData.fA[1][1] * calData.ftrV[CHY] * calData.ftrV[CHY] +
                               2.0F * calData.fA[1][2] * calData.ftrV[CHY] * calData.ftrV[CHZ] +
                               calData.fA[2][2] * calData.ftrV[CHZ] * calData.ftrV[CHZ] - calData.fmatB[9][j]));

    // calculate the trial normalized fit error as a percentage
    calData.ftrFitErrorpc = 50.0f * sqrtf(fabsf(calData.fvecA[j]) / (float)buffer->iMagBufferCount) /
                            (calData.ftrB * calData.ftrB);

    // correct for the measurement matrix offset and scaling and get the computed hard iron offset in uT
    for (l = CHX; l <= CHZ; l++) {
        calData.ftrV[l] = calData.ftrV[l] * Be_length + (float)iOffset[l];
    }

    // normalize the ellipsoid matrix A to unit determinant and correct B by root of this multiplicative factor
    f3x3matrixAeqAxScalar(calData.fA, powf(det, -(ONETHIRD)));
    calData.ftrB *= powf(det, -(ONESIXTH));

    // compute trial invW from the square root of fA (both with normalized determinant)
    // set fvecA to the unsorted eigenvalues and fmatB to the unsorted eigenvectors of fmatA
    // where fmatA holds the 3x3 matrix fA in its top left elements
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            calData.fmatA[i][j] = calData.fA[i][j];
        }
    }
    eigencompute(calData.fmatA, calData.fvecA, calData.fmatB, 3);

    // set calData.gfmatB to be eigenvectors . diag(sqrt(sqrt(eigenvalues))) = fmatB . diag(sqrt(sqrt(fvecA))
    for (j = 0; j < 3; j++) { // loop over columns j
        ftmp = sqrtf(sqrtf(fabsf(calData.fvecA[j])));
        for (i = 0; i < 3; i++) { // loop over rows i
            calData.fmatB[i][j] *= ftmp;
        }
    }

    // set ftrinvW to eigenvectors * diag(sqrt(eigenvalues)) * eigenvectors^T
    // = fmatB * fmatB^T = sqrt(fA) (guaranteed symmetric)
    // loop over rows
    for (i = 0; i < 3; i++) {
        // loop over on and above diagonal columns
        for (j = i; j < 3; j++) {
            calData.ftrinvW[i][j] = 0.0F;
            // accumulate the matrix product
            for (k = 0; k < 3; k++) {
                calData.ftrinvW[i][j] += calData.fmatB[i][k] * calData.fmatB[j][k];
            }
            // copy to below diagonal element
            calData.ftrinvW[j][i] = calData.ftrinvW[i][j];
        }
    }
}
