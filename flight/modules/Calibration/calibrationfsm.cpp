/*
 ******************************************************************************
 *
 * @file       gyrobiascalibrationfsm.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      GyroBias calibration state machine
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/

#include <project_xflight.h>

// C++ includes
#include <calibrationfsm.h>
