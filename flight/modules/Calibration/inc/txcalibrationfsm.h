/**
 ******************************************************************************
 * @file       txcalibrationfsm.h
 * @author     Rodney Grainger Copyright (C) 2015.
 * @brief      Tx calibration implementation class
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#ifndef TXCALIBRATIONFSM_H
#define TXCALIBRATIONFSM_H

#include "calibrationfsm.h"

typedef enum {
    TXCAL_STATE_INACTIVE = 0, // Inactive state is the initialised state on startup
    TXCAL_STATE_START,
    TXCAL_STATE_IDENTIFY,
    TXCAL_STATE_VERIFY,
    TXCAL_STATE_SAVE,
    TXCAL_STATE_COMPLETE,
    TXCAL_STATE_ERROR,
    TXCAL_STATE_SIZE
} TxCalibrationFSM_State_T;

#define NUMBER_OF_ACRO_CHANNELS 8
#define NUMBER_OF_HELI_CHANNELS 9
#define NUMBER_OF_GRND_CHANNELS 6

class TxCalibrationFSM : public CalibrationFSM {
private:
    static TxCalibrationFSM *p_inst;
    TxCalibrationFSM();

public:
    static TxCalibrationFSM *instance()
    {
        if (!p_inst) {
            p_inst = new TxCalibrationFSM();
        }
        return p_inst;
    }

public:
    void Inactive(void);
    void Activate(void);
    void Update(CalibrationFSMDataTypes_T dataTypeUpdated);
    void SettingsUpdated(void) {}
    CalibrationFSMState_T GetCurrentState(void);
    void Abort(void) {}
    int32_t Initialize(void);
    void SaveToFlash(void);
    void calibrationTrigger(void);
    uint32_t timerPeriod(void)
    {
        return CALTIMER_1Hz;
    }


protected:

    typedef struct {
        CalibrationStatusTxCalibrationStateOptions state;
        uint32_t index;
        bool canSkip;
    } TxCalibrationFSM_ChannelOrder_T;

    // FSM instance data type
    typedef struct {
        TxCalibrationFSM_State_T  currentState;
        uint32_t stateRunCount;
        uint32_t stateTimeoutCount;
        CalibrationStatusData     caliStatus;
        CalibrationFSMDataTypes_T updateReason;
        ManualControlSettingsData tempManualControlSettings;
        ManualControlSettingsData savedManualControlSettings;
        FlightModeSettingsArmingOptions savedArming;
        uint16_t savedStickTimeHoldTime;
        uint16_t savedManualControlCommandRate;
        bool     isReversed[NUMBER_OF_HELI_CHANNELS];
        TxCalibrationFSM_ChannelOrder_T *channelOrder;
        uint8_t  numberOfChannels;
        uint32_t channelIndex;
        uint32_t debounceCount;
        uint8_t  previousChannel;
        uint32_t iterationCount;
    } TxCalibrationFSMData_T;

    // FSM state structure
    typedef struct {
        void(TxCalibrationFSM::*setup) (void); // Called to initialise the state
        void(TxCalibrationFSM::*run) (uint8_t); // Run the event detection code for a state
    } TxCalibrationFSM_StateHandler_T;

    // Private variables
    TxCalibrationFSMData_T *mTxData;
    bool mSaveToFlashPending;

    void setup_inactive(void);
    void setup_start(void);
    void run_start(uint8_t);
    void setup_identify(void);
    void run_identify(uint8_t);
    void setup_verify(void);
    void run_verify(uint8_t);
    void run_completed(uint8_t);
    void setup_completed(void);
    void setup_save(void);
    void setup_error(void);


    void initFSM(void);
    void setState(TxCalibrationFSM_State_T newState);
    int32_t runState();
    void setStateTimeout(int32_t count);

    void setup_sample_helper(bool flagReset);
    void command_next_helper(void);
    void command_back_helper(void);
    void clear_channel(uint8_t channel);
    void reverse_channel(uint8_t channel);
    void AdjustSpecialCaseNeutrals(void);
    void disable_arming_setting(void);
    void restore_arming_setting(void);
    void increase_manualcontrolcommanddata_rate(void);
    void restore_manualcontrolcommanddata_rate(void);
    void disable_maintenancemode(void);
    void restore_maintenancemode(void);

    static TxCalibrationFSM_StateHandler_T sTxStateTable[TXCAL_STATE_SIZE];
    static TxCalibrationFSM_ChannelOrder_T sAcroChannelOrder[NUMBER_OF_ACRO_CHANNELS];
    static TxCalibrationFSM_ChannelOrder_T sHeliChannelOrder[NUMBER_OF_HELI_CHANNELS];
    static TxCalibrationFSM_ChannelOrder_T sGrndChannelOrder[NUMBER_OF_GRND_CHANNELS];
};

#endif // ifndef TXCALIBRATIONFSM_H
