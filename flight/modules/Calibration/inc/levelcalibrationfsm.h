/**
 ******************************************************************************
 * @file       levelcalibrationfsm.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Level calibration implementation class
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#ifndef LEVELCALIBRATIONFSM_H
#define LEVELCALIBRATIONFSM_H

#include "calibrationfsm.h"
#include "libmatrix/vector2.h"
#include "libmatrix/vector3.h"
#include "pios_debug.h"

typedef enum {
    LEVELCAL_STATE_INACTIVE = 0, // Inactive state is the initialised state on startup
    LEVELCAL_STATE_START,
    LEVELCAL_STATE_POSITION1,
    LEVELCAL_STATE_POSITION2,
    LEVELCAL_STATE_SAVE,
    LEVELCAL_STATE_COMPLETED,
    LEVELCAL_STATE_ERROR,
    LEVELCAL_STATE_SIZE
} LevelCalibrationFSM_State_T;

class LevelCalibrationFSM : public CalibrationFSM {
private:
    static LevelCalibrationFSM *p_inst;
    LevelCalibrationFSM();

public:
    static LevelCalibrationFSM *instance()
    {
        if (!p_inst) {
            p_inst = new LevelCalibrationFSM();
        }
        return p_inst;
    }

public:
    void Inactive(void);
    void Activate(void);
    void Update(CalibrationFSMDataTypes_T dataTypeUpdated);
    void SettingsUpdated(void) {}
    CalibrationFSMState_T GetCurrentState(void);
    void Abort(void) {}
    int32_t Initialize(void);
    void SaveToFlash(void);
    void calibrationTrigger(void);
    uint32_t timerPeriod(void)
    {
        return CALTIMER_1Hz;
    }


protected:

    // FSM instance data type
    typedef struct {
        LevelCalibrationFSM_State_T currentState;
        uint32_t stateRunCount;
        uint32_t stateTimeoutCount;
        uint32_t observationCount;
        uint32_t observation2Count;
        Vector3f accelBeginning;
        Vector2f trimPosition1;
        Vector2f trimPosition2;
        Vector2f stateSum;
        Vector2f stateAveragePrevious;
        CalibrationStatusData caliStatus;
        uint32_t iterationCount;
        bool     trim1OK;
        bool     trim2OK;
        bool     startSampling;
        bool     samplingInitialised;
    } LevelCalibrationFSMData_T;

    // FSM state structure
    typedef struct {
        void(LevelCalibrationFSM::*setup) (void); // Called to initialise the state
        void(LevelCalibrationFSM::*run) (uint8_t); // Run the event detection code for a state
    } LevelCalibrationFSM_StateHandler_T;

    // Private variables
    LevelCalibrationFSMData_T *mLevelData;
    bool mSaveToFlashPending;

    void setup_inactive(void);
    void setup_start(void);
    void setup_position1(void);
    void setup_position2(void);
    void setup_save(void);
    void setup_completed(void);
    void setup_error(void);
    // void run_init_althold(uint8_t);
    void run_position1(uint8_t);
    void run_position2(uint8_t);
    void run_completed(uint8_t);

    void initFSM(void);
    void setState(LevelCalibrationFSM_State_T newState);
    int32_t runState();

    void setStateTimeout(int32_t count);

    void setup_sample_helper(bool flagReset);
    void sample_helper(uint8_t flTimeout, Vector2f &trimResult, bool & resultOK, LevelCalibrationFSM_State_T cur_state, LevelCalibrationFSM_State_T next_state);

    static LevelCalibrationFSM_StateHandler_T sLevelStateTable[LEVELCAL_STATE_SIZE];
};

#endif // ifndef LEVELCALIBRATIONFSM_H
