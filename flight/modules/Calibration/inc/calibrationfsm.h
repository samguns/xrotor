/**
 ******************************************************************************
 * @file       calibrationfsm.h
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Interface class for calibration finite state machine
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#ifndef CALIBRATIONFSM_H
#define CALIBRATIONFSM_H

#define CALTIMER_1Hz  1000
#define CALTIMER_10Hz 100

typedef enum {
    CALFSM_STATE_INACTIVE = 0, // Inactive state is the initialised state on startup
    CALFSM_STATE_ACTIVE,
    CALFSM_STATE_COMPLETED,
    CALFSM_STATE_ERROR
} CalibrationFSMState_T;

typedef enum {
    CALFSM_DATA_NONE = 0x00000002,
    CALFSM_DATA_ATTITUDESTATE    = 0x00000004,
    CALFSM_DATA_GYROSENSOR       = 0x00000008,
    CALFSM_DATA_ACCELSENSOR      = 0x00000010,
    CALFSM_DATA_MAGSENSOR        = 0x00000020,
    CALFSM_DATA_AUXMAGSENSOR     = 0x00000040,
    CALFSM_DATA_TIMER = 0x00000080,
    CALFSM_DATA_RECEIVERACTIVITY = 0x00000100,
    CALFSM_DATA_RECEIVERCHANNEL  = 0x00000200,
} CalibrationFSMDataTypes_T;

class CalibrationFSM {
public:
    virtual void Inactive(void) = 0;
    virtual void Activate(void) = 0;
    virtual void Update(CalibrationFSMDataTypes_T dataTypesUpdated) = 0;
    virtual void SettingsUpdated(void)    = 0;
    virtual CalibrationFSMState_T GetCurrentState(void) = 0;
    virtual void Abort(void) = 0;
    virtual void calibrationTrigger(void) = 0;
    virtual uint32_t timerPeriod(void)    = 0;
    uint32_t timerTicksPerSecond(void)
    {
        return CALTIMER_1Hz / timerPeriod();
    }
};

#endif // CALIBRATIONFSM_H
