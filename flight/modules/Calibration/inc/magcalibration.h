/**
 ******************************************************************************
 * @file       magcalibration.h
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Mag calibration implementation class
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#ifndef MAGCALIBRATION_H
#define MAGCALIBRATION_H

#include "project_xflight.h"
#include "libmatrix/vector3.h"

// magnetic calibration constants
#define MAGBUFFSIZEX         14                 // x dimension in magnetometer buffer (12x24 equals 288 elements)
#define MAGBUFFSIZEY         (2 * MAGBUFFSIZEX) // y dimension in magnetometer buffer (12x24 equals 288 elements)
#define MINMEASUREMENTS4CAL  100                // minimum number of measurements for 4 element calibration
#define MINMEASUREMENTS7CAL  150                // minimum number of measurements for 7 element calibration
#define MINMEASUREMENTS10CAL 250 // minimum number of measurements for 10 element calibration
#define MAXMEASUREMENTS      340                // maximum number of measurements used for calibration


#define Xidx                 0
#define Yidx                 1
#define Zidx                 2
#define CHX                  0
#define CHY                  1
#define CHZ                  2

typedef enum {
    MAGCAL_STAGE_NOCALC = 0,
    MAGCAL_STAGE_4CAL,
    MAGCAL_STAGE_7CAL,
    MAGCAL_STAGE_10CAL
} MagCalibrationStage_T;

class MagCalibration {
public:
    MagCalibration();
    ~MagCalibration();
    void initialise();
    void processSample(uint32_t timestamp, float x, float y, float z, float ax, float ay, float az);
    float percentSampled();
    int16_t sampleCount();
    bool calcCalibration(float Be_length, float calibrationMatrix[], float bias[]);

    // Get the solver stage allowed based on sampling
    MagCalibrationStage_T getCalStage();

    // Get the trial solver stage actually used
    MagCalibrationStage_T getSolverStage()
    {
        return calData.solver;
    }

    // Get solver that last returned a valid calibration result that was accepted
    MagCalibrationStage_T getValidCalibrationSolverStage()
    {
        return calData.iValidMagCal;
    }

    uint8_t getCalibrationFailCount()
    {
        return calData.calibrationRejectedCount;
    }

    float getSolverErrorPcnt()
    {
        return calData.fFitErrorpc;
    }
    float getUnscaledBe()
    {
        return calData.fB;
    }

    bool getTrialCalcStatus()
    {
        return calData.trialAccepted;
    }

    void setupRecalibrationSequence()
    {
        allow4INVSolver = false;
    }
    void setupCalibrationSequence()
    {
        allow4INVSolver = true;
    }

private:
    void fUpdateCalibration10EIG(const float Be_length);
    void fUpdateCalibration7EIG(const float Be_length);
    void fUpdateCalibration4INV(const float Be_length);

    Vector3f sample;

    struct MagneticBuffer {
        Vector3f sampleData[MAGBUFFSIZEX + 4][MAGBUFFSIZEY + 4]; // uncalibrated magnetometer readings
        int32_t  index[MAGBUFFSIZEX + 4][MAGBUFFSIZEY + 4]; // array of time indices
        int16_t  tanarray[MAGBUFFSIZEX - 1 + 4]; // array of tangents of (100 * angle)
        int16_t  iMagBufferCount; // number of magnetometer readings
    };

    MagneticBuffer *buffer;

    // magnetic calibration structure
    struct CalData {
        float   fV[3]; // current hard iron offset x, y, z, (uT)
        float   finvW[3][3]; // current inverse soft iron matrix
        float   fB; // current geomagnetic field magnitude (uT)
        float   fFourBsq; // current 4*B*B (uT^2)
        float   fFitErrorpc; // current fit error %
        float   ftrV[3]; // trial value of hard iron offset z, y, z (uT)
        float   ftrinvW[3][3]; // trial inverse soft iron matrix size
        float   ftrB; // trial value of geomagnetic field magnitude in uT
        float   ftrFitErrorpc; // trial value of fit error %
        float   fA[3][3]; // ellipsoid matrix A
        float   finvA[3][3]; // inverse of ellipsoid matrix A
        float   fmatA[10][10]; // scratch 10x10 matrix used by calibration algorithms
        float   fmatB[10][10]; // scratch 10x10 matrix used by calibration algorithms
        float   fvecA[10]; // scratch 10x1 vector used by calibration algorithms
        float   fvecB[4]; // scratch 4x1 vector used by calibration algorithms
        MagCalibrationStage_T iValidMagCal; // integer value 0, 4, 7, 10 denoting both valid calibration and solver used
        MagCalibrationStage_T solver;
        uint8_t calibrationRejectedCount;
        bool    trialAccepted;
    };

    CalData calData;
    float progress;
    bool allow4INVSolver;
};


#endif // ifndef MAGCALIBRATION_H
