/**
 ******************************************************************************
 * @file       gyrobiascalibrationfsm.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      GyroBias calibration implementation class
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#ifndef GYROBIASCALIBRATIONFSM_H
#define GYROBIASCALIBRATIONFSM_H

#include "mpugyroaccelsettings.h"

#include "libmatrix/vector3.h"

#include "calibrationfsm.h"
typedef enum {
    GYROBIASCAL_STATE_INACTIVE = 0, // Inactive state is the initialised state on startup
    GYROBIASCAL_STATE_START,
    GYROBIASCAL_STATE_PRESELFTEST,
    GYROBIASCAL_STATE_SELFTEST,
    GYROBIASCAL_STATE_POSTSELFTEST,
    GYROBIASCAL_STATE_SELFTEST_COMPLETED,
    GYROBIASCAL_STATE_SAMPLE,
    GYROBIASCAL_STATE_SAVE,
    GYROBIASCAL_STATE_COMPLETED,
    GYROBIASCAL_STATE_ERROR,
    GYROBIASCAL_STATE_SIZE
} GyroBiasCalibrationFSM_State_T;

class GyroBiasCalibrationFSM : public CalibrationFSM {
private:
    static GyroBiasCalibrationFSM *p_inst;
    GyroBiasCalibrationFSM();

public:
    static GyroBiasCalibrationFSM *instance()
    {
        if (!p_inst) {
            p_inst = new GyroBiasCalibrationFSM();
        }
        return p_inst;
    }

public:
    void Inactive(void);
    void Activate(void);
    void Update(CalibrationFSMDataTypes_T dataTypeUpdated);
    void SettingsUpdated(void) {}
    CalibrationFSMState_T GetCurrentState(void);
    void Abort(void) {}
    int32_t Initialize(void);
    void SaveToFlash(void);
    void calibrationTrigger(void) {}
    uint32_t timerPeriod(void)
    {
        return CALTIMER_1Hz;
    }


protected:

    // FSM instance data type
    typedef struct {
        GyroBiasCalibrationFSM_State_T currentState;
        uint32_t stateRunCount;
        uint32_t stateTimeoutCount;
        Vector3f accelBeginning;
        Vector3f gyroAveragePrevious;
        Vector3f gyroSum;
        Vector3f gyroPreSelfTest; // pre-self-test bias
        Vector3f gyroSelfTest; // self-test observed
        Vector3f gyroBias; // observed bias at flight config values
        bool     gyroPreSelfTestOK;
        bool     gyroSelfTestOK;
        bool     gyroBiasOK;
        uint32_t observationCount;
        uint32_t observation2Count;
        uint32_t iterationCount;
        CalibrationStatusData caliStatus;
        float    boardRotationYaw;
        float    boardRotationRoll;
        float    boardRotationPitch;
        float    boardLevelTrimRoll;
        float    boardLevelTrimPitch;
        MPUGyroAccelSettingsData mpuSettings;
    } GyroBiasCalibrationFSMData_T;

    // FSM state structure
    typedef struct {
        void(GyroBiasCalibrationFSM::*setup) (void); // Called to initialise the state
        void(GyroBiasCalibrationFSM::*run) (uint8_t); // Run the event detection code for a state
    } GyroBiasCalibrationFSM_StateHandler_T;

    // Private variables
    GyroBiasCalibrationFSMData_T *mGyroBiasData;
    bool mSaveToFlashPending;

    void setup_inactive(void);
    void setup_preselftest(void);
    void setup_selftest(void);
    void setup_postselftest(void);
    void setup_selftest_completed(void);
    void setup_start(void);
    void setup_sample(void);
    void setup_save(void);
    void setup_completed(void);
    void setup_error(void);
    void run_preselftest(uint8_t);
    void run_selftest(uint8_t);
    void run_sample(uint8_t);
    void run_completed(uint8_t);

    void initFSM(void);
    void setState(GyroBiasCalibrationFSM_State_T newState);
    int32_t runState();

    void setStateTimeout(int32_t count);

    void setup_sample_helper(bool flagReset);
    void sample_helper(uint8_t flTimeout, Vector3f &gyroResult, bool &gyroResultOK,
                       GyroBiasCalibrationFSM_State_T cur_state,
                       GyroBiasCalibrationFSM_State_T next_state);

    static GyroBiasCalibrationFSM_StateHandler_T sGyroBiasStateTable[GYROBIASCAL_STATE_SIZE];
};

#endif // ifndef GYROBIASCALIBRATIONFSM_H
