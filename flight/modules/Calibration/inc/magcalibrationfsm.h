/**
 ******************************************************************************
 * @file       magcalibrationfsm.h
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Mag calibration implementation class
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#ifndef MAGCALIBRATIONFSM_H
#define MAGCALIBRATIONFSM_H

#include "magcalibration.h"
#include "calibrationfsm.h"

typedef enum {
    MAGCAL_STATE_INACTIVE = 0, // Inactive state is the initialised state on startup
    MAGCAL_STATE_START,
    MAGCAL_STATE_SAMPLE,
    MAGCAL_STATE_CALC,
    MAGCAL_STATE_SAMPLE_STAGE2,
    MAGCAL_STATE_COMPLETED,
    MAGCAL_STATE_ERROR,
    MAGCAL_STATE_SIZE
} MagCalibrationFSM_State_T;


class MagCalibrationFSM : public CalibrationFSM {
private:
    static MagCalibrationFSM *p_inst;
    MagCalibrationFSM();

public:
    static MagCalibrationFSM *instance()
    {
        if (!p_inst) {
            p_inst = new MagCalibrationFSM();
        }
        return p_inst;
    }

public:
    void Inactive(void);
    void Activate(void);
    void Update(CalibrationFSMDataTypes_T dataTypeUpdated);
    void SettingsUpdated(void) {}
    CalibrationFSMState_T GetCurrentState(void);
    void Abort(void) {}
    int32_t Initialize(void);
    void SaveToFlash(void);
    void calibrationTrigger(void) {}
    uint32_t timerPeriod(void)
    {
        return CALTIMER_1Hz;
    }


protected:

    // FSM instance data type
    typedef struct {
        MagCalibrationFSM_State_T currentState;
        uint32_t stateRunCount;
        uint32_t stateTimeoutCount;
        CalibrationStatusData     caliStatus;
        float boardRotationYaw;
        float boardRotationRoll;
        float boardRotationPitch;
        float boardLevelTrimRoll;
        float boardLevelTrimPitch;
        CalibrationMagsData saveMag;
        CalibrationFSMDataTypes_T updateReason;
    } MagCalibrationFSMData_T;
    MagCalibration mag1; // primary
    MagCalibration mag2; // optional aux offboard mag

    // sample data
    AccelSensorData sampleAccelSensor;
    MagSensorData sampleMagSensor;
    AuxMagSensorData sampleAuxMagSensor;
    bool flHaveMagSensorSample;
    bool flHaveAuxMagSensorSample;

    // FSM state structure
    typedef struct {
        void(MagCalibrationFSM::*setup) (void); // Called to initialise the state
        void(MagCalibrationFSM::*run) (uint8_t); // Run the event detection code for a state
    } MagCalibrationFSM_StateHandler_T;

    // Private variables
    MagCalibrationFSMData_T *mMagData;
    bool mSaveToFlashPending;
    bool mAuxMagOkToBeSaved;
    bool mMagOkToBeSaved;

    void setup_inactive(void);
    void setup_start(void);
    void setup_sample(void);
    void setup_sample_stage2(void);
    void setup_calc(void);
    void setup_completed(void);
    void setup_error(void);
    void run_sample(uint8_t);
    void run_calc(uint8_t);
    void run_completed(uint8_t);

    void initFSM(void);
    void setState(MagCalibrationFSM_State_T newState);
    int32_t runState();

    void setStateTimeout(int32_t count);
    void calcTask(void);
    void sampleTask(void);
    static void calibrationSampleTask(void);
    static void calibrationCalcTask(void);

    static MagCalibrationFSM_StateHandler_T sMagStateTable[MAGCAL_STATE_SIZE];
};

#endif // ifndef MAGCALIBRATIONFSM_H
