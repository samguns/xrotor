/**
 ******************************************************************************
 * @file       accelcalibrationfsm.h
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      Accel calibration implementation class
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *
 *****************************************************************************/
#ifndef ACCELCALIBRATIONFSM_H
#define ACCELCALIBRATIONFSM_H

#include "calibrationfsm.h"
#include "libmatrix/vector3.h"
#include "mpugyroaccelsettings.h"
#include "accelgyrosettings.h"

typedef enum {
    ACCELCAL_STATE_INACTIVE = 0, // Inactive state is the initialised state on startup
    ACCELCAL_STATE_START,
    ACCELCAL_STATE_PRESELFTEST,
    ACCELCAL_STATE_SELFTEST,
    ACCELCAL_STATE_POSTSELFTEST,
    ACCELCAL_STATE_SELFTEST_COMPLETED,
    ACCELCAL_STATE_SAMPLE_1,
    ACCELCAL_STATE_SAMPLE_2,
    ACCELCAL_STATE_SAMPLE_3,
    ACCELCAL_STATE_SAMPLE_4,
    ACCELCAL_STATE_SAMPLE_5,
    ACCELCAL_STATE_SAMPLE_6,
    ACCELCAL_STATE_SAMPLE_7,
    ACCELCAL_STATE_SAMPLE_8,
    ACCELCAL_STATE_SAMPLE_9,
    ACCELCAL_STATE_SAMPLE_10,
    ACCELCAL_STATE_SAMPLE_11,
    ACCELCAL_STATE_SAMPLE_12,
    ACCELCAL_STATE_SAMPLE_13,
    ACCELCAL_STATE_SAMPLE_14,
    ACCELCAL_STATE_CALC,
    ACCELCAL_STATE_COMPLETED,
    ACCELCAL_STATE_ERROR,
    ACCELCAL_STATE_SIZE
} AccelCalibrationFSM_State_T;

// sampling structure
typedef struct {
    CalibrationStatusAccelCalibrationStateOptions setup_status_state;
    AccelCalibrationFSM_State_T next_state;
    AccelCalibrationFSM_State_T back_state;
} AccelCalibrationFSM_SamplingSequence_T;


// Debug pin out enable/disable and pin definitions
// #define ACCELCAL_DEBUG
#define PIN0_stateupdate    0


#define ACCEL_SAMPLE_COUNT  14
#define POINT_SAMPLE_SIZE   1000
#define MODEL_UNKNOWNS      12
#define ACCEL_SAMPLESIZE_1D (ACCEL_SAMPLE_COUNT * 3)

class AccelCalibrationFSM : public CalibrationFSM {
private:
    static AccelCalibrationFSM *p_inst;
    AccelCalibrationFSM();

public:
    static AccelCalibrationFSM *instance()
    {
        if (!p_inst) {
            p_inst = new AccelCalibrationFSM();
        }
        return p_inst;
    }

public:
    void Inactive(void);
    void Activate(void);
    void Update(CalibrationFSMDataTypes_T dataTypeUpdated);
    void SettingsUpdated(void) {}
    CalibrationFSMState_T GetCurrentState(void);
    void Abort(void) {}
    int32_t Initialize(void);
    void SaveToFlash(void);
    void calibrationTrigger(void);
    uint32_t timerPeriod(void)
    {
        return CALTIMER_1Hz;
    }


protected:

    // FSM instance data type
    typedef struct {
        AccelCalibrationFSM_State_T currentState;
        uint32_t stateRunCount;
        uint32_t stateTimeoutCount;
        Vector3f accelBeginning;
        Vector3f accel_sample[POINT_SAMPLE_SIZE];
        Vector3f accel_sum;
        Vector3f accel[ACCEL_SAMPLE_COUNT];
        float    variance[ACCEL_SAMPLE_COUNT];
        uint32_t observationCount;
        uint32_t observation2Count;
        CalibrationStatusData caliStatus;
        float    boardRotationYaw;
        float    boardRotationRoll;
        float    boardRotationPitch;
        float    boardLevelTrimRoll;
        float    boardLevelTrimPitch;
        AccelGyroSettingsData     saveAccelGyro;
        MPUGyroAccelSettingsData  mpuSettings;
        CalibrationFSMDataTypes_T updateReason;
        bool startSampling;
        bool samplingInitialised;
        bool goBack;
    } AccelCalibrationFSMData_T;

    // FSM state structure
    typedef struct {
        void(AccelCalibrationFSM::*setup) (void); // Called to initialise the state
        void(AccelCalibrationFSM::*run) (uint8_t); // Run the event detection code for a state
    } AccelCalibrationFSM_StateHandler_T;

    // Private variables
    AccelCalibrationFSMData_T *mAccelData;
    AccelGyroSettingsData accelGyroSettings;
    float AccelSamples[ACCEL_SAMPLESIZE_1D];
    bool mSaveToFlashPending;

    void setup_inactive(void);
    void setup_preselftest(void);
    void setup_selftest(void);
    void setup_postselftest(void);
    void setup_selftest_completed(void);
    void setup_start(void);
    void setup_calc(void);
    void setup_completed(void);
    void setup_error(void);
    void run_calc(uint8_t);
    void run_preselftest(uint8_t);
    void run_selftest(uint8_t);

    void setup_helper(CalibrationStatusAccelCalibrationStateOptions setup_status_state);
    void run_helper(uint8_t idx, AccelCalibrationFSM_State_T next_state, AccelCalibrationFSM_State_T back_state);
    void setup_sample(void);
    void run_sample(uint8_t);
    void run_completed(uint8_t);

    void initFSM(void);
    void setState(AccelCalibrationFSM_State_T newState);
    int32_t runState();

    void setStateTimeout(int32_t count);
    void calcTask(void);
    static void calibrationCalcTask(void);
    void calcCalibration(uint32_t sample_count, float * *samples, double Be_length, float calibrationMatrix[], float bias[]);


    static int calibrationFit(float ConstMag, Vector3f * samples, float *variance, float S[9], float b[3]);
    static int testCalibrationFitAsymmetric();
    static void calibrationFitAsymmetric(const float *Vx, const float *Vy, const float *Vz, const float *Vvariance, float M2[9], float B2[3], float info[7]);

    bool accept_sample(const Vector3f & sample, uint8_t samples_collected);

    static AccelCalibrationFSM_StateHandler_T sAccelStateTable[ACCELCAL_STATE_SIZE];
    static AccelCalibrationFSM_SamplingSequence_T sAccelSamplingTable[ACCEL_SAMPLE_COUNT];
};

#endif // ifndef ACCELCALIBRATIONFSM_H
