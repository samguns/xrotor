/*
 ******************************************************************************
 *
 * @file       gyrobiascalibrationfsm.cpp
 * @author     Alex Beck Copyright (C) 2015.
 * @brief      GyroBias calibration state machine
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/

#include <project_xflight.h>
#include <callbackinfo.h>

#include <math.h>
#include <alarms.h>
#include <CoordinateConversions.h>
#include <sin_lookup.h>
#include "attitudesettings.h"
#include "attitudestate.h"
#include "calibrationstatus.h"
#include "objectpersistence.h"
#include "gyrosensor.h"
#include "accelsensor.h"
#include "accelgyrosettings.h"
#include "mpugyroaccelsettings.h"
#include "calibrationfactory.h"
#include "calibrationcontrol.h"

// C++ includes
#include <gyrobiascalibrationfsm.h>


// Private constants
#define TIMER_COUNT_PER_SECOND (PIOS_SILA_RATE)

GyroBiasCalibrationFSM::GyroBiasCalibrationFSM_StateHandler_T GyroBiasCalibrationFSM::sGyroBiasStateTable[GYROBIASCAL_STATE_SIZE] = {
    [GYROBIASCAL_STATE_INACTIVE]     =       { .setup = &GyroBiasCalibrationFSM::setup_inactive,           .run = 0                                        },
    [GYROBIASCAL_STATE_START]        =       { .setup = &GyroBiasCalibrationFSM::setup_start,              .run = 0                                        },
    [GYROBIASCAL_STATE_PRESELFTEST]  =       { .setup = &GyroBiasCalibrationFSM::setup_preselftest,        .run = &GyroBiasCalibrationFSM::run_preselftest },
    [GYROBIASCAL_STATE_SELFTEST]     =       { .setup = &GyroBiasCalibrationFSM::setup_selftest,           .run = &GyroBiasCalibrationFSM::run_selftest    },
    [GYROBIASCAL_STATE_POSTSELFTEST] =       { .setup = &GyroBiasCalibrationFSM::setup_postselftest,       .run = 0                                        },
    [GYROBIASCAL_STATE_SELFTEST_COMPLETED] = { .setup = &GyroBiasCalibrationFSM::setup_selftest_completed, .run = &GyroBiasCalibrationFSM::run_completed   },
    [GYROBIASCAL_STATE_SAMPLE]       =       { .setup = &GyroBiasCalibrationFSM::setup_sample,             .run = &GyroBiasCalibrationFSM::run_sample      },
    [GYROBIASCAL_STATE_SAVE] =               { .setup = &GyroBiasCalibrationFSM::setup_save,               .run = 0                                        },
    [GYROBIASCAL_STATE_COMPLETED]    =       { .setup = &GyroBiasCalibrationFSM::setup_completed,          .run = &GyroBiasCalibrationFSM::run_completed   },
    [GYROBIASCAL_STATE_ERROR]        =       { .setup = &GyroBiasCalibrationFSM::setup_error,              .run = &GyroBiasCalibrationFSM::run_completed   },
};

// pointer to a singleton instance
GyroBiasCalibrationFSM *GyroBiasCalibrationFSM::p_inst = 0;

#define SAMPLE_SKIP            50
#define SAMPLE_TIME            (2 * TIMER_COUNT_PER_SECOND + SAMPLE_SKIP)
#define COMPLETED_DISPLAY_TIME (4 * timerTicksPerSecond())


// FSM Setup and Run method implementation

void GyroBiasCalibrationFSM::setup_start(void)
{
    memset(&mGyroBiasData->caliStatus, 0, sizeof(CalibrationStatusData));
    mGyroBiasData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_GYROBIASCALIBRATION;
    mGyroBiasData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
    mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_IDLE;
    mGyroBiasData->caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));

    // configure board for calibration
    // TODO This is not actually used. But we do need to keep track of a state of calibration.
    // RevoCalibrationData revoCal;
    // RevoCalibrationGet(&revoCal);
    // revoCal.BiasCorrectedRaw = REVOCALIBRATION_BIASCORRECTEDRAW_FALSE;
    // RevoCalibrationSet(&revoCal);

    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);

    // The following is unused. TODO Remove it from UAVO definition.
    // Disable gyro bias correction while calibrating
    // attitudeSettings.BiasCorrectGyro     = ATTITUDESETTINGS_BIASCORRECTGYRO_FALSE;

    // save board rotation
    mGyroBiasData->boardRotationYaw       = attitudeSettings.BoardRotation.Yaw;
    mGyroBiasData->boardRotationRoll      = attitudeSettings.BoardRotation.Roll;
    mGyroBiasData->boardRotationPitch     = attitudeSettings.BoardRotation.Pitch;
    mGyroBiasData->boardLevelTrimRoll     = attitudeSettings.BoardLevelTrim.Roll;
    mGyroBiasData->boardLevelTrimPitch    = attitudeSettings.BoardLevelTrim.Pitch;

    // Zero board rotation during calibration
    attitudeSettings.BoardRotation.Yaw    = 0.0f;
    attitudeSettings.BoardRotation.Roll   = 0.0f;
    attitudeSettings.BoardRotation.Pitch  = 0.0f;
    attitudeSettings.BoardLevelTrim.Pitch = 0.0f;
    attitudeSettings.BoardLevelTrim.Roll  = 0.0f;
    AttitudeSettingsSet(&attitudeSettings);

    // zero out the bias
    AccelGyroSettingsData accelGyroSettings;
    AccelGyroSettingsGet(&accelGyroSettings);
    accelGyroSettings.gyro_bias.X = 0.0f;
    accelGyroSettings.gyro_bias.Y = 0.0f;
    accelGyroSettings.gyro_bias.Z = 0.0f;
    AccelGyroSettingsSet(&accelGyroSettings);

    // store the mpu settings to revert to.
    MPUGyroAccelSettingsGet(&mGyroBiasData->mpuSettings);

    setup_sample_helper(true);

    CalibrationControlData caliControl;
    CalibrationControlGet(&caliControl);
    if (caliControl.Operation == CALIBRATIONCONTROL_OPERATION_GYROSELFTEST) {
#ifdef PIOS_INCLUDE_MPU9250
        setState(GYROBIASCAL_STATE_PRESELFTEST);
#else
        setState(GYROBIASCAL_STATE_SAMPLE);
#endif
    } else {
        setState(GYROBIASCAL_STATE_SAMPLE);
    }
}

void GyroBiasCalibrationFSM::setup_preselftest(void)
{
    mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_PRESELFTEST;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));

    MPUGyroAccelSettingsData mpuSettings;
    MPUGyroAccelSettingsGet(&mpuSettings);
    mpuSettings.FilterSetting = MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_98_HZ;
    mpuSettings.GyroScale     = MPUGYROACCELSETTINGS_GYROSCALE_SCALE_250;
    MPUGyroAccelSettingsSet(&mpuSettings);
    setup_sample_helper(false);

    setStateTimeout(SAMPLE_TIME);
}
void GyroBiasCalibrationFSM::run_preselftest(uint8_t flTimeout)
{
    sample_helper(flTimeout, mGyroBiasData->gyroPreSelfTest, mGyroBiasData->gyroPreSelfTestOK, GYROBIASCAL_STATE_PRESELFTEST, GYROBIASCAL_STATE_SELFTEST);
}


void GyroBiasCalibrationFSM::setup_selftest(void)
{
    mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_SELFTEST;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));

    MPUGyroAccelSettingsData mpuSettings;
    MPUGyroAccelSettingsGet(&mpuSettings);
    mpuSettings.FilterSetting = MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_98_HZ;
    mpuSettings.GyroScale     = MPUGYROACCELSETTINGS_GYROSCALE_SCALE_250_TEST;
    MPUGyroAccelSettingsSet(&mpuSettings);
    setup_sample_helper(false);
    setStateTimeout(SAMPLE_TIME);
}


void GyroBiasCalibrationFSM::run_selftest(uint8_t flTimeout)
{
    sample_helper(flTimeout, mGyroBiasData->gyroSelfTest, mGyroBiasData->gyroSelfTestOK, GYROBIASCAL_STATE_SELFTEST, GYROBIASCAL_STATE_POSTSELFTEST);
}

#define STR_PERCENTAGE_DELTA_FROM_FACTORY 15.0f
void GyroBiasCalibrationFSM::setup_postselftest(void)
{
    // restore settings
    MPUGyroAccelSettingsSet(&mGyroBiasData->mpuSettings);

    setup_sample_helper(true);


    // calculate factory vs self test ratios
    CalibrationFactoryData factoryCal;
    CalibrationFactoryGet(&factoryCal);
    factoryCal.GyroBias.x     = mGyroBiasData->gyroPreSelfTest.x;
    factoryCal.GyroBias.y     = mGyroBiasData->gyroPreSelfTest.y;
    factoryCal.GyroBias.z     = mGyroBiasData->gyroPreSelfTest.z;
    factoryCal.GyroSelfTestStatus.bias = ((mGyroBiasData->gyroPreSelfTestOK == true) ? CALIBRATIONFACTORY_GYROSELFTESTSTATUS_GOOD : CALIBRATIONFACTORY_GYROSELFTESTSTATUS_BAD);
    factoryCal.GyroSelfTest.x = mGyroBiasData->gyroSelfTest.x;
    factoryCal.GyroSelfTest.y = mGyroBiasData->gyroSelfTest.y;
    factoryCal.GyroSelfTest.z = mGyroBiasData->gyroSelfTest.z;
    factoryCal.GyroSelfTestStatus.selftest = ((mGyroBiasData->gyroSelfTestOK == true) ? CALIBRATIONFACTORY_GYROSELFTESTSTATUS_GOOD : CALIBRATIONFACTORY_GYROSELFTESTSTATUS_BAD);
    factoryCal.GyroSTR.x = 100.0f * (factoryCal.GyroSelfTest.x - factoryCal.GyroBias.x - factoryCal.Gyro.x) / factoryCal.Gyro.x;
    factoryCal.GyroSTR.y = 100.0f * (factoryCal.GyroSelfTest.y - factoryCal.GyroBias.y - factoryCal.Gyro.y) / factoryCal.Gyro.y;
    factoryCal.GyroSTR.z = 100.0f * (factoryCal.GyroSelfTest.z - factoryCal.GyroBias.z - factoryCal.Gyro.z) / factoryCal.Gyro.z;

    if (fabsf(factoryCal.GyroSTR.x) < STR_PERCENTAGE_DELTA_FROM_FACTORY &&
        fabsf(factoryCal.GyroSTR.y) < STR_PERCENTAGE_DELTA_FROM_FACTORY &&
        fabsf(factoryCal.GyroSTR.z) < STR_PERCENTAGE_DELTA_FROM_FACTORY) {
        factoryCal.GyroSelfTestStatus.str = CALIBRATIONFACTORY_GYROSELFTESTSTATUS_GOOD;
    } else {
        factoryCal.GyroSelfTestStatus.str = CALIBRATIONFACTORY_GYROSELFTESTSTATUS_BAD;
    }
    CalibrationFactorySet(&factoryCal);

    if (factoryCal.GyroSelfTestStatus.str == CALIBRATIONFACTORY_GYROSELFTESTSTATUS_BAD ||
        factoryCal.GyroSelfTestStatus.bias == CALIBRATIONFACTORY_GYROSELFTESTSTATUS_BAD ||
        factoryCal.GyroSelfTestStatus.selftest == CALIBRATIONFACTORY_GYROSELFTESTSTATUS_BAD) {
        mGyroBiasData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_ERROR;
        mGyroBiasData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_GYROSELFTESTERROR;
        CalibrationStatusSet(&(mGyroBiasData->caliStatus));
        setState(GYROBIASCAL_STATE_ERROR);
    } else {
        setState(GYROBIASCAL_STATE_SELFTEST_COMPLETED);
    }
}


void GyroBiasCalibrationFSM::setup_sample(void)
{
    mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_SAMPLE;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));
    setup_sample_helper(false);
    setStateTimeout(SAMPLE_TIME);
}
void GyroBiasCalibrationFSM::run_sample(uint8_t flTimeout)
{
    sample_helper(flTimeout, mGyroBiasData->gyroBias, mGyroBiasData->gyroBiasOK, GYROBIASCAL_STATE_SAMPLE, GYROBIASCAL_STATE_SAVE);
}

void GyroBiasCalibrationFSM::setup_save(void)
{
    // check OK flag and move to error state
    if (!mGyroBiasData->gyroBiasOK) {
        mGyroBiasData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_ERROR;
        mGyroBiasData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_GYROBIASERROR;
        CalibrationStatusSet(&(mGyroBiasData->caliStatus));
        setState(GYROBIASCAL_STATE_ERROR);
        return;
    }

    mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_SAVE;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));

    // Update biases based on collected data
    AccelGyroSettingsData accelGyroSettings;
    AccelGyroSettingsGet(&accelGyroSettings);
    accelGyroSettings.gyro_bias.X = mGyroBiasData->gyroBias.x;
    accelGyroSettings.gyro_bias.Y = mGyroBiasData->gyroBias.y;
    accelGyroSettings.gyro_bias.Z = mGyroBiasData->gyroBias.z;
    AccelGyroSettingsSet(&accelGyroSettings);

    setState(GYROBIASCAL_STATE_COMPLETED);
}


void GyroBiasCalibrationFSM::setup_completed(void)
{
    mGyroBiasData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_COMPLETE;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));


    // Restore
    // RevoCalibrationData revoCal;
    // RevoCalibrationGet(&revoCal);
    // revoCal.BiasCorrectedRaw = REVOCALIBRATION_BIASCORRECTEDRAW_TRUE;
    // RevoCalibrationSet(&revoCal);

    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);
    // attitudeSettings.BiasCorrectGyro     = ATTITUDESETTINGS_BIASCORRECTGYRO_TRUE;
    attitudeSettings.BoardRotation.Yaw    = mGyroBiasData->boardRotationYaw;
    attitudeSettings.BoardRotation.Roll   = mGyroBiasData->boardRotationRoll;
    attitudeSettings.BoardRotation.Pitch  = mGyroBiasData->boardRotationPitch;
    attitudeSettings.BoardLevelTrim.Roll  = mGyroBiasData->boardLevelTrimRoll;
    attitudeSettings.BoardLevelTrim.Pitch = mGyroBiasData->boardLevelTrimPitch;
    AttitudeSettingsSet(&attitudeSettings);

    mSaveToFlashPending = true;

    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void GyroBiasCalibrationFSM::setup_inactive(void)
{
    mGyroBiasData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));
}

void GyroBiasCalibrationFSM::setup_selftest_completed(void)
{
    mGyroBiasData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mGyroBiasData->caliStatus.GyroBiasCalibrationState = CALIBRATIONSTATUS_GYROBIASCALIBRATIONSTATE_SELFTESTCOMPLETED;
    CalibrationStatusSet(&(mGyroBiasData->caliStatus));


    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);
    // attitudeSettings.BiasCorrectGyro     = ATTITUDESETTINGS_BIASCORRECTGYRO_TRUE;
    attitudeSettings.BoardRotation.Yaw    = mGyroBiasData->boardRotationYaw;
    attitudeSettings.BoardRotation.Roll   = mGyroBiasData->boardRotationRoll;
    attitudeSettings.BoardRotation.Pitch  = mGyroBiasData->boardRotationPitch;
    attitudeSettings.BoardLevelTrim.Roll  = mGyroBiasData->boardLevelTrimRoll;
    attitudeSettings.BoardLevelTrim.Pitch = mGyroBiasData->boardLevelTrimPitch;
    AttitudeSettingsSet(&attitudeSettings);

    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void GyroBiasCalibrationFSM::run_completed(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(GYROBIASCAL_STATE_INACTIVE);
    }
}

void GyroBiasCalibrationFSM::setup_error(void)
{
    // restore
    AttitudeSettingsData attitudeSettings;

    AttitudeSettingsGet(&attitudeSettings);
    // attitudeSettings.BiasCorrectGyro     = ATTITUDESETTINGS_BIASCORRECTGYRO_TRUE;
    attitudeSettings.BoardRotation.Yaw    = mGyroBiasData->boardRotationYaw;
    attitudeSettings.BoardRotation.Roll   = mGyroBiasData->boardRotationRoll;
    attitudeSettings.BoardRotation.Pitch  = mGyroBiasData->boardRotationPitch;
    attitudeSettings.BoardLevelTrim.Roll  = mGyroBiasData->boardLevelTrimRoll;
    attitudeSettings.BoardLevelTrim.Pitch = mGyroBiasData->boardLevelTrimPitch;
    AttitudeSettingsSet(&attitudeSettings);

    // note that gyro bias in accelgyrosettings will still be 0.0f and can be used
    // to check if a calibration has been done.

    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void GyroBiasCalibrationFSM::setup_sample_helper(bool flagReset)
{
    if (flagReset) {
        // Restart new sampling sequence from scratch
        mGyroBiasData->gyroAveragePrevious.zero();
        mGyroBiasData->iterationCount = 0;
    }
    // Same sampling sequence with a new iteration
    mGyroBiasData->gyroSum.zero();
    mGyroBiasData->observationCount  = 0;
    mGyroBiasData->observation2Count = 0;
}

#define MAX_ITERATIONS       15
#define MIN_ITERATIONS       2
#define CONVERGENCE_LIMIT    0.006f
#define CONVERGENCE_OK_LIMIT 0.04f
void GyroBiasCalibrationFSM::sample_helper(uint8_t flTimeout,
                                           Vector3f &gyroResult,
                                           bool & gyroResultOK,
                                           GyroBiasCalibrationFSM_State_T cur_state,
                                           GyroBiasCalibrationFSM_State_T next_state)
{
    // ignore the first 50 samples
    mGyroBiasData->observation2Count++;
    if (mGyroBiasData->observation2Count < SAMPLE_SKIP) {
        return;
    }

    if (mGyroBiasData->observationCount == 0) {
        AccelSensorData accelSensor;
        AccelSensorGet(&accelSensor);
        mGyroBiasData->accelBeginning(accelSensor.x, accelSensor.y, accelSensor.z);
    }

    GyroSensorData gyroSensor;
    GyroSensorGet(&gyroSensor);
    mGyroBiasData->observationCount++;
    Vector3f sensor(gyroSensor.x, gyroSensor.y, gyroSensor.z);
    mGyroBiasData->gyroSum += sensor;

    if (flTimeout) {
        // calculate the average gyro vector observed during the sampling period
        Vector3f gyroAverage = mGyroBiasData->gyroSum / (float)mGyroBiasData->observationCount;
        if (mGyroBiasData->iterationCount == 0) {
            // first iteration, save the current average for comparison on the next iteration
            mGyroBiasData->gyroAveragePrevious = gyroAverage;

            // increment the iteration count
            mGyroBiasData->iterationCount++;

            // initialise result to false
            gyroResultOK = false;
        } else {
            // increment the iteration count
            mGyroBiasData->iterationCount++;

            // check accels. Note we don't for the first sample as we look for convergence anyway
            AccelSensorData accelSensor;
            AccelSensorGet(&accelSensor);
            Vector3f accelEnd(accelSensor.x, accelSensor.y, accelSensor.z);
            Vector3f accelDiff = accelEnd - mGyroBiasData->accelBeginning;

            // reject samples up to a iteration count after which we give up rejecting.
            if (accelDiff.length() > 0.2f && mGyroBiasData->iterationCount < 15) {
                // we have detected a change in orientation. skip this sample
                // restart an interation in the current state
                setState(cur_state);
                return;
            }

            // calculate the deviation across the iterations
            Vector3f gyroDiff = mGyroBiasData->gyroAveragePrevious - gyroAverage;

            // update the moving average for the previous average
            mGyroBiasData->gyroAveragePrevious = (gyroAverage * 0.5f) + (mGyroBiasData->gyroAveragePrevious * 0.5f);


            // check for convergence or we give up
            if ((mGyroBiasData->iterationCount > MIN_ITERATIONS && gyroDiff.length() < CONVERGENCE_LIMIT) || mGyroBiasData->iterationCount >= MAX_ITERATIONS) {
                gyroResult   = gyroAverage;
                gyroResultOK = (gyroDiff.length() < CONVERGENCE_OK_LIMIT);
                // reinitialise for a future sampling phase
                setup_sample_helper(true);
                // jump to the next state
                setState(next_state);
            } else {
                // restart an iteration in the current state
                setState(cur_state);
            }
        }
    }
}


void GyroBiasCalibrationFSM::SaveToFlash(void)
{
    if (mSaveToFlashPending) {
        mSaveToFlashPending = false;
        ObjectPersistenceData objper;
        objper.InstanceID   = 0;
        objper.ObjectID     = ACCELGYROSETTINGS_OBJID;
        objper.Operation    = OBJECTPERSISTENCE_OPERATION_SAVE;
        objper.Selection    = OBJECTPERSISTENCE_SELECTION_SINGLEOBJECT;
        ObjectPersistenceSet(&objper);
    }
}

GyroBiasCalibrationFSM::GyroBiasCalibrationFSM()
    : mGyroBiasData(0), mSaveToFlashPending(false)
{}

// Private types

// Private functions
// Public API methods
/**
 * Initialise the module, called on startup
 * \returns STATUS_OK on success
 */
int32_t GyroBiasCalibrationFSM::Initialize()
{
    if (mGyroBiasData == 0) {
        mGyroBiasData = (GyroBiasCalibrationFSMData_T *)pios_malloc(sizeof(GyroBiasCalibrationFSMData_T));
        PIOS_Assert(mGyroBiasData);
    }
    memset(mGyroBiasData, 0, sizeof(GyroBiasCalibrationFSMData_T));
    initFSM();

    return STATUS_OK;
}

void GyroBiasCalibrationFSM::Inactive(void)
{
    memset(mGyroBiasData, 0, sizeof(GyroBiasCalibrationFSMData_T));
    initFSM();
}

// Initialise the FSM
void GyroBiasCalibrationFSM::initFSM(void)
{
    mGyroBiasData->currentState = GYROBIASCAL_STATE_INACTIVE;
}

void GyroBiasCalibrationFSM::Activate()
{
    if (mGyroBiasData->currentState == GYROBIASCAL_STATE_INACTIVE || mGyroBiasData->currentState >= GYROBIASCAL_STATE_COMPLETED) {
        memset(mGyroBiasData, 0, sizeof(GyroBiasCalibrationFSMData_T));
        mGyroBiasData->currentState = GYROBIASCAL_STATE_INACTIVE;
        setState(GYROBIASCAL_STATE_START);
    }
}

CalibrationFSMState_T GyroBiasCalibrationFSM::GetCurrentState(void)
{
    if (mGyroBiasData->currentState == GYROBIASCAL_STATE_INACTIVE) {
        return CALFSM_STATE_INACTIVE;
    } else {
        return CALFSM_STATE_ACTIVE;
    }
}

void GyroBiasCalibrationFSM::Update(CalibrationFSMDataTypes_T dataUpdateAvail)
{
    // check state and return if completed, inactive or error
    if (mGyroBiasData->currentState == GYROBIASCAL_STATE_INACTIVE) {
        return;
    }

    // attitudestate is updated at the sensor rate
    if (dataUpdateAvail == CALFSM_DATA_GYROSENSOR) {
        // update rate is sensor rate / 4 which is 500/4=125Hz
        runState();
    }
}

int32_t GyroBiasCalibrationFSM::runState(void)
{
    uint8_t flTimeout = false;

    mGyroBiasData->stateRunCount++;

    if (mGyroBiasData->stateTimeoutCount > 0 && mGyroBiasData->stateRunCount > mGyroBiasData->stateTimeoutCount) {
        flTimeout = true;
    }

    // If the current state has a static function, call it
    if (sGyroBiasStateTable[mGyroBiasData->currentState].run) {
        (this->*sGyroBiasStateTable[mGyroBiasData->currentState].run)(flTimeout);
    }
    return STATUS_OK;
}

// Set the new state and perform setup for subsequent state run calls
// This is called by state run functions on event detection that drive
// state transitions.
void GyroBiasCalibrationFSM::setState(GyroBiasCalibrationFSM_State_T newState)
{
    mGyroBiasData->currentState      = newState;

    // Restart state timer counter
    mGyroBiasData->stateRunCount     = 0;

    // Reset state timeout to disabled/zero
    mGyroBiasData->stateTimeoutCount = 0;

    if (sGyroBiasStateTable[mGyroBiasData->currentState].setup) {
        (this->*sGyroBiasStateTable[mGyroBiasData->currentState].setup)();
    }
}


// Timeout utility function for use by state init implementations
void GyroBiasCalibrationFSM::setStateTimeout(int32_t count)
{
    mGyroBiasData->stateTimeoutCount = count;
}
