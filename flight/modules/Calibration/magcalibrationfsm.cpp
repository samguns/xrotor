/*
 ******************************************************************************
 *
 * @file       magcalibrationfsm.cpp
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Mag calibration state machine
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#include <project_xflight.h>
#include <callbackinfo.h>

#include <math.h>
#include <mathmisc.h>
#include <alarms.h>
#include <CoordinateConversions.h>
#include <sin_lookup.h>
#include "attitudesettings.h"
#include "attitudestate.h"
#include "calibrationcontrol.h"
#include "calibrationstatus.h"
#include "objectpersistence.h"
#include "calibrationmags.h"
#include "auxmagsensor.h"
#include "magsensor.h"
#include "accelsensor.h"
#include "accelgyrosettings.h"
#include "homelocation.h"

#include <libmatrix/matrix.h>


// C++ includes
#include "libmatrix/vector3.h"
#include <magcalibrationfsm.h>

static DelayedCallbackInfo *calibrationCalcTaskCBInfo;
static DelayedCallbackInfo *calibrationSampleTaskCBInfo;
#define CALLBACK_PRIORITY      CALLBACK_PRIORITY_LOW
#define CBTASK_PRIORITY        CALLBACK_TASK_FLIGHTCONTROL
#define STACK_SIZE_BYTES       2 * 2048 // REDUCE

// Private constants
#define COMPLETED_DISPLAY_TIME (3 * timerTicksPerSecond())

MagCalibrationFSM::MagCalibrationFSM_StateHandler_T MagCalibrationFSM::sMagStateTable[MAGCAL_STATE_SIZE] = {
    [MAGCAL_STATE_INACTIVE]      = { .setup = &MagCalibrationFSM::setup_inactive,      .run = 0                                 },
    [MAGCAL_STATE_START]         = { .setup = &MagCalibrationFSM::setup_start,         .run = 0                                 },
    [MAGCAL_STATE_SAMPLE]        = { .setup = &MagCalibrationFSM::setup_sample,        .run = &MagCalibrationFSM::run_sample    },
    [MAGCAL_STATE_CALC]          = { .setup = &MagCalibrationFSM::setup_calc,          .run = &MagCalibrationFSM::run_calc      },
    [MAGCAL_STATE_SAMPLE_STAGE2] = { .setup = &MagCalibrationFSM::setup_sample_stage2, .run = &MagCalibrationFSM::run_sample    },
    [MAGCAL_STATE_COMPLETED]     = { .setup = &MagCalibrationFSM::setup_completed,     .run = &MagCalibrationFSM::run_completed },
    [MAGCAL_STATE_ERROR]         = { .setup = &MagCalibrationFSM::setup_error,         .run = &MagCalibrationFSM::run_completed },
};

// pointer to a singleton instance
MagCalibrationFSM *MagCalibrationFSM::p_inst = 0;

MagCalibrationFSM::MagCalibrationFSM()
    : mMagData(0), mSaveToFlashPending(false), mAuxMagOkToBeSaved(false), mMagOkToBeSaved(false)
{
    calibrationCalcTaskCBInfo   = PIOS_CALLBACKSCHEDULER_Create(&MagCalibrationFSM::calibrationCalcTask, CALLBACK_PRIORITY, CBTASK_PRIORITY, CALLBACKINFO_RUNNING_CALIBRATION, STACK_SIZE_BYTES);
    calibrationSampleTaskCBInfo = PIOS_CALLBACKSCHEDULER_Create(&MagCalibrationFSM::calibrationSampleTask, CALLBACK_PRIORITY, CBTASK_PRIORITY, CALLBACKINFO_RUNNING_CALIBRATION, STACK_SIZE_BYTES);
}

// Private types

// Private functions
// Public API methods
/**
 * Initialise the module, called on startup
 * \returns STATUS_OK on success
 */
int32_t MagCalibrationFSM::Initialize()
{
    if (mMagData == 0) {
        mMagData = (MagCalibrationFSMData_T *)pios_malloc(sizeof(MagCalibrationFSMData_T));
        PIOS_Assert(mMagData);
    }
    memset(mMagData, 0, sizeof(MagCalibrationFSMData_T));
    initFSM();

    return STATUS_OK;
}

void MagCalibrationFSM::Inactive(void)
{
    memset(mMagData, 0, sizeof(MagCalibrationFSMData_T));
    initFSM();
}


void MagCalibrationFSM::setup_inactive(void)
{
    mMagData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    CalibrationStatusSet(&(mMagData->caliStatus));
}


// FSM Setup and Run method implementation

void MagCalibrationFSM::setup_start(void)
{
    memset(&mMagData->caliStatus, 0, sizeof(CalibrationStatusData));
    mMagData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_IDLE;
    mMagData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_NOERROR;

    CalibrationMagsData magData;
    CalibrationMagsGet(&magData);
    mMagData->saveMag = magData;

    CalibrationControlData caliControl;
    CalibrationControlGet(&caliControl);
    if (caliControl.Operation == CALIBRATIONCONTROL_OPERATION_COMPASSCALIBRATION) {
        mMagData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_COMPASSCALIBRATION;
        // Reset the transformation matrix to identity
        magData.OnboardTransform.r0c0  = 1;
        magData.OnboardTransform.r0c1  = 0;
        magData.OnboardTransform.r0c2  = 0;
        magData.OnboardTransform.r1c0  = 0;
        magData.OnboardTransform.r1c1  = 1;
        magData.OnboardTransform.r1c2  = 0;
        magData.OnboardTransform.r2c0  = 0;
        magData.OnboardTransform.r2c1  = 0;
        magData.OnboardTransform.r2c2  = 1;
        magData.OnboardBias.X = 0;
        magData.OnboardBias.Y = 0;
        magData.OnboardBias.Z = 0;

        // Reset the transformation matrix to identity
        magData.ExternalTransform.r0c0 = 1;
        magData.ExternalTransform.r0c1 = 0;
        magData.ExternalTransform.r0c2 = 0;
        magData.ExternalTransform.r1c0 = 0;
        magData.ExternalTransform.r1c1 = 1;
        magData.ExternalTransform.r1c2 = 0;
        magData.ExternalTransform.r2c0 = 0;
        magData.ExternalTransform.r2c1 = 0;
        magData.ExternalTransform.r2c2 = 1;
        magData.ExternalBias.X = 0;
        magData.ExternalBias.Y = 0;
        magData.ExternalBias.Z = 0;
        CalibrationMagsSet(&magData);


        mag1.setupCalibrationSequence();
        mag2.setupCalibrationSequence();
    } else {
        mMagData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_COMPASSRECALIBRATION;
        // otherwise a recalibration doesn't initially clear our the calibration data
        mag1.setupRecalibrationSequence();
        mag2.setupRecalibrationSequence();
    }

    mAuxMagOkToBeSaved  = false;
    mMagOkToBeSaved     = false;
    mSaveToFlashPending = false;

    setState(MAGCAL_STATE_SAMPLE);
}

#define SAMPLETIMEOUT4CAL  (30 * timerTicksPerSecond())
#define SAMPLETIMEOUT7CAL  (30 * timerTicksPerSecond())
#define SAMPLETIMEOUT10CAL (15 * timerTicksPerSecond())
void MagCalibrationFSM::setup_sample(void)
{
    mag1.initialise();
    mag2.initialise();

    setStateTimeout(SAMPLETIMEOUT4CAL);

    mMagData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
    mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_SAMPLE;
    CalibrationStatusSet(&(mMagData->caliStatus));
    flHaveMagSensorSample    = false;
    flHaveAuxMagSensorSample = false;
}

void MagCalibrationFSM::setup_sample_stage2(void)
{
    uint32_t timeout = SAMPLETIMEOUT4CAL;
    MagCalibrationStage_T stage = mag1.getSolverStage();

    if (mag2.getSolverStage() > mag1.getSolverStage()) {
        stage = mag2.getSolverStage();
    }

    switch (stage) {
    case MAGCAL_STAGE_NOCALC:
    case MAGCAL_STAGE_4CAL:
        timeout = SAMPLETIMEOUT4CAL;
        break;
    case MAGCAL_STAGE_7CAL:
        timeout = SAMPLETIMEOUT7CAL;
        break;
    case MAGCAL_STAGE_10CAL:
        // if the trial solver actually used is 10CAL, it was rejected, allowing sampling for a reduced
        // period and retry the calculation
        timeout = SAMPLETIMEOUT10CAL;
        break;
    }
    setStateTimeout(timeout);


    mMagData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
    mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_SAMPLE;
    CalibrationStatusSet(&(mMagData->caliStatus));
    flHaveMagSensorSample    = false;
    flHaveAuxMagSensorSample = false;
}

// only sample when not running a calibration calculation
void MagCalibrationFSM::run_sample(uint8_t flTimeout)
{
    switch (mMagData->updateReason) {
    case CALFSM_DATA_TIMER: // 1 Hz
        mMagData->caliStatus.Mag1CalibrationProgress    = mag1.percentSampled();
        mMagData->caliStatus.Mag1CalibrationSampleCount = mag1.sampleCount();
        mMagData->caliStatus.Mag2CalibrationProgress    = mag2.percentSampled();
        mMagData->caliStatus.Mag2CalibrationSampleCount = mag2.sampleCount();
        CalibrationStatusSet(&(mMagData->caliStatus));
        break;
    case CALFSM_DATA_MAGSENSOR: // onboard sensor typically updates at 100Hz
        if (!flHaveMagSensorSample) {
            AccelSensorGet(&sampleAccelSensor);
            MagSensorGet(&sampleMagSensor);
            flHaveMagSensorSample = true;
            // mag1.processSample(magSensor.timestamp, magSensor.x, magSensor.y, magSensor.z, accelSensor.x, accelSensor.y, accelSensor.z);
            PIOS_CALLBACKSCHEDULER_Schedule(calibrationSampleTaskCBInfo, 0, CALLBACK_UPDATEMODE_SOONER);
        }
        break;
    case CALFSM_DATA_AUXMAGSENSOR: // a gps v9 sensor updates at around 1000/40 Hz.
        if (!flHaveAuxMagSensorSample) {
            AccelSensorGet(&sampleAccelSensor);
            AuxMagSensorGet(&sampleAuxMagSensor);
            flHaveAuxMagSensorSample = true;
            // mag2.processSample(auxMagSensor.timestamp, auxMagSensor.x, auxMagSensor.y, auxMagSensor.z, accelSensor.x, accelSensor.y, accelSensor.z);
            PIOS_CALLBACKSCHEDULER_Schedule(calibrationSampleTaskCBInfo, 0, CALLBACK_UPDATEMODE_SOONER);
        }
        break;
    default:
        break;
    }

    if (flTimeout) {
        setState(MAGCAL_STATE_CALC);
    }
}

void MagCalibrationFSM::calibrationSampleTask(void)
{
    MagCalibrationFSM::instance()->sampleTask();
}

void MagCalibrationFSM::sampleTask(void)
{
    if (flHaveMagSensorSample) {
        mag1.processSample(sampleMagSensor.timestamp, sampleMagSensor.raw.x, sampleMagSensor.raw.y, sampleMagSensor.raw.z,
                           sampleMagSensor.calibrated.x, sampleMagSensor.calibrated.y, sampleMagSensor.calibrated.z);
        flHaveMagSensorSample = false;
    }

    if (flHaveAuxMagSensorSample) {
        mag2.processSample(sampleAuxMagSensor.timestamp, sampleAuxMagSensor.raw.x, sampleAuxMagSensor.raw.y, sampleAuxMagSensor.raw.z,
                           sampleMagSensor.calibrated.x, sampleMagSensor.calibrated.y, sampleMagSensor.calibrated.z);
        flHaveAuxMagSensorSample = false;
    }


    // Assess when enough sampling has completed to move from prior solver level to the next solver level.
    bool sampleComplete = false;

    switch (mMagData->saveMag.Usage) {
    case CALIBRATIONMAGS_USAGE_ONBOARDONLY:
        if (mag1.getCalStage() > mag1.getSolverStage()) {
            // we have enough samples to take is to the next cal stage which is a higher stage than current solver stage
            sampleComplete = true;
        }
        // else if (mag1.getCalStage() == mag1.getSolverStage())
        // rely on sample timeout
        break;
    case CALIBRATIONMAGS_USAGE_EXTERNALONLY:
        if (mag2.getCalStage() > mag2.getSolverStage()) {
            // we have enough samples to take is to the next cal stage which is a higher stage than current solver stage
            sampleComplete = true;
        }
        // else if (mag2.getCalStage() == mag2.getSolverStage())
        // rely on sample timeout
        break;
    default: // using both mags simultaneously
        if (mag1.getCalStage() == mag2.getCalStage() && mag1.getCalStage() > mag1.getSolverStage() && mag2.getCalStage() > mag2.getSolverStage()) {
            // we have enough samples to take is to the next cal stage which is a higher stage than current solver stage
            sampleComplete = true;
        }
        // else if (mag1.getCalStage() == mag1.getSolverStage() && mag2.getCalStage() == mag2.getSolverStage() )
        // rely on sample timeout
        break;
    }


    if (sampleComplete) {
        setState(MAGCAL_STATE_CALC);
    }
}


#define CALC_TIME (15 * timerTicksPerSecond())
void MagCalibrationFSM::setup_calc(void)
{
    mMagData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;

    MagCalibrationStage_T stage = mag1.getCalStage();
    if (mag2.getCalStage() > mag1.getCalStage()) {
        stage = mag2.getCalStage();
    }

    switch (stage) {
    case MAGCAL_STAGE_NOCALC:
    case MAGCAL_STAGE_4CAL:
        mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_CALC4;
        break;
    case MAGCAL_STAGE_7CAL:
        mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_CALC7;
        break;
    case MAGCAL_STAGE_10CAL:
        mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_CALC10;
        break;
    }


    CalibrationStatusSet(&(mMagData->caliStatus));
    setStateTimeout(CALC_TIME);
    // we might need to run this as a seperte thread if its long running and going to cause issues with callback
    PIOS_CALLBACKSCHEDULER_Schedule(calibrationCalcTaskCBInfo, 2000, CALLBACK_UPDATEMODE_SOONER);
}

void MagCalibrationFSM::run_calc(__attribute__((unused)) uint8_t flTimeout)
{
    // report on status
    if (flTimeout) {
        mMagData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_MAGCALCTIMEOUT;
        setState(MAGCAL_STATE_ERROR);
    }
}

void MagCalibrationFSM::calibrationCalcTask(void)
{
    MagCalibrationFSM::instance()->calcTask();
}


#define SOLVER_RETRY_LIMIT 2

void MagCalibrationFSM::calcTask(void)
{
    CalibrationMagsData calibrationMags;

    CalibrationMagsGet(&calibrationMags);

    HomeLocationData homeLocation;
    HomeLocationGet(&homeLocation);

    float Be_length;
    Be_length = sqrtf(pow(homeLocation.Be[0], 2.0f) + pow(homeLocation.Be[1], 2.0f) + pow(homeLocation.Be[2], 2.0f));

    bool mag1_calc_was_run = false;
    bool mag2_calc_was_run = false;

    switch (mMagData->saveMag.Usage) {
    case CALIBRATIONMAGS_USAGE_ONBOARDONLY:
        if (mag1.calcCalibration(Be_length, (float *)&calibrationMags.OnboardTransform, (float *)&calibrationMags.OnboardBias)) {
            CalibrationMagsSet(&calibrationMags);
            mSaveToFlashPending = true;
            mMagOkToBeSaved     = true;
        }
        mag1_calc_was_run = true;
        break;
    case CALIBRATIONMAGS_USAGE_EXTERNALONLY:
        if (mag2.calcCalibration(Be_length, (float *)&calibrationMags.ExternalTransform, (float *)&calibrationMags.ExternalBias)) {
            CalibrationMagsSet(&calibrationMags);
            mSaveToFlashPending = true;
            mAuxMagOkToBeSaved  = true;
        }
        mag2_calc_was_run = true;
        break;
    default:
        if (mag1.calcCalibration(Be_length, (float *)&calibrationMags.OnboardTransform, (float *)&calibrationMags.OnboardBias)) {
            CalibrationMagsSet(&calibrationMags);
            mSaveToFlashPending = true;
            mMagOkToBeSaved     = true;
        }
        mag1_calc_was_run = true;

        if (mag2.calcCalibration(Be_length, (float *)&calibrationMags.ExternalTransform, (float *)&calibrationMags.ExternalBias)) {
            CalibrationMagsSet(&calibrationMags);
            mSaveToFlashPending = true;
            mAuxMagOkToBeSaved  = true;
        }
        mag2_calc_was_run = true;
        break;
    }

    if (mag1_calc_was_run) {
        mMagData->caliStatus.Mag1FitErrorPcnt = mag1.getSolverErrorPcnt();
        mMagData->caliStatus.Mag1UnscaledBe   = mag1.getUnscaledBe();
        mMagData->caliStatus.Mag1CalcOK = (mag1.getTrialCalcStatus() == true ? CALIBRATIONSTATUS_MAG1CALCOK_TRUE : CALIBRATIONSTATUS_MAG1CALCOK_FALSE);
    }
    if (mag2_calc_was_run) {
        mMagData->caliStatus.Mag2FitErrorPcnt = mag2.getSolverErrorPcnt();
        mMagData->caliStatus.Mag2UnscaledBe   = mag2.getUnscaledBe();
        mMagData->caliStatus.Mag2CalcOK = (mag2.getTrialCalcStatus() == true ? CALIBRATIONSTATUS_MAG2CALCOK_TRUE : CALIBRATIONSTATUS_MAG2CALCOK_FALSE);
    }
    CalibrationStatusSet(&(mMagData->caliStatus));

    switch (mMagData->saveMag.Usage) {
    case CALIBRATIONMAGS_USAGE_ONBOARDONLY:
        if (mag1.getValidCalibrationSolverStage() < MAGCAL_STAGE_10CAL) {
            if (mag1.getSolverStage() > MAGCAL_STAGE_4CAL && mag1.getCalibrationFailCount() > SOLVER_RETRY_LIMIT) {
                setState(MAGCAL_STATE_COMPLETED);
            } else {
                setState(MAGCAL_STATE_SAMPLE_STAGE2);
            }
        } else {
            setState(MAGCAL_STATE_COMPLETED);
        }
        return;

        break;
    case CALIBRATIONMAGS_USAGE_EXTERNALONLY:
        if (mag2.getValidCalibrationSolverStage() < MAGCAL_STAGE_10CAL) {
            if (mag2.getSolverStage() > MAGCAL_STAGE_4CAL && mag2.getCalibrationFailCount() > SOLVER_RETRY_LIMIT) {
                setState(MAGCAL_STATE_COMPLETED);
            } else {
                setState(MAGCAL_STATE_SAMPLE_STAGE2);
            }
        } else {
            setState(MAGCAL_STATE_COMPLETED);
        }
        return;

        break;
    default: // using both mags simultaneously
        if (mag1.getValidCalibrationSolverStage() < MAGCAL_STAGE_10CAL || mag2.getValidCalibrationSolverStage() < MAGCAL_STAGE_10CAL) {
            if (mag1.getSolverStage() > MAGCAL_STAGE_4CAL && mag2.getSolverStage() > MAGCAL_STAGE_4CAL &&
                mag1.getCalibrationFailCount() > SOLVER_RETRY_LIMIT &&
                mag2.getCalibrationFailCount() > SOLVER_RETRY_LIMIT) {
                setState(MAGCAL_STATE_COMPLETED);
            } else {
                setState(MAGCAL_STATE_SAMPLE_STAGE2);
            }
        } else {
            setState(MAGCAL_STATE_COMPLETED);
        }
        return;

        break;
    }

    // should never get here
    setState(MAGCAL_STATE_SAMPLE_STAGE2);
}


void MagCalibrationFSM::setup_completed(void)
{
    mMagData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_COMPLETE;
    CalibrationStatusSet(&(mMagData->caliStatus));
    mSaveToFlashPending = true;

    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void MagCalibrationFSM::run_completed(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(MAGCAL_STATE_INACTIVE);
    }
}

void MagCalibrationFSM::setup_error(void)
{
    mMagData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mMagData->caliStatus.MagCalibrationState = CALIBRATIONSTATUS_MAGCALIBRATIONSTATE_ERROR;
    mMagData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_MAGSAMPLINGTIMEOUT;
    CalibrationStatusSet(&(mMagData->caliStatus));

    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void MagCalibrationFSM::SaveToFlash(void)
{
    if (mSaveToFlashPending) {
        mSaveToFlashPending = false;

        if (mMagOkToBeSaved || mAuxMagOkToBeSaved) {
            mMagOkToBeSaved    = false;
            mAuxMagOkToBeSaved = false;
            ObjectPersistenceData objper;
            objper.InstanceID  = 0;
            objper.ObjectID    = CALIBRATIONMAGS_OBJID;
            objper.Operation   = OBJECTPERSISTENCE_OPERATION_SAVE;
            objper.Selection   = OBJECTPERSISTENCE_SELECTION_SINGLEOBJECT;
            ObjectPersistenceSet(&objper);
        }
    }
}


// Initialise the FSM
void MagCalibrationFSM::initFSM(void)
{
    mMagData->currentState = MAGCAL_STATE_INACTIVE;
}

void MagCalibrationFSM::Activate()
{
    memset(mMagData, 0, sizeof(MagCalibrationFSMData_T));
    mMagData->currentState = MAGCAL_STATE_INACTIVE;

    setState(MAGCAL_STATE_START);
}

CalibrationFSMState_T MagCalibrationFSM::GetCurrentState(void)
{
    if (mMagData->currentState == MAGCAL_STATE_INACTIVE) {
        return CALFSM_STATE_INACTIVE;
    } else {
        return CALFSM_STATE_ACTIVE;
    }
}

void MagCalibrationFSM::Update(CalibrationFSMDataTypes_T dataUpdateAvail)
{
    // check state and return if completed, inactive or error
    if (mMagData->currentState == MAGCAL_STATE_INACTIVE) {
        return;
    }
    mMagData->updateReason = CALFSM_DATA_NONE;
    if (mMagData->currentState == MAGCAL_STATE_SAMPLE || mMagData->currentState == MAGCAL_STATE_SAMPLE_STAGE2) {
        if ((dataUpdateAvail == CALFSM_DATA_MAGSENSOR) ||
            (dataUpdateAvail == CALFSM_DATA_AUXMAGSENSOR) ||
            (dataUpdateAvail == CALFSM_DATA_TIMER)) {
            mMagData->updateReason = dataUpdateAvail;
            runState();
        }
    } else {
        if (dataUpdateAvail == CALFSM_DATA_TIMER) {
            mMagData->updateReason = dataUpdateAvail;
            runState();
        }
    }
}

int32_t MagCalibrationFSM::runState(void)
{
    uint8_t flTimeout = false;

    if (mMagData->updateReason == CALFSM_DATA_TIMER) {
        mMagData->stateRunCount++;
        if (mMagData->stateTimeoutCount > 0 && mMagData->stateRunCount > mMagData->stateTimeoutCount) {
            flTimeout = true;
        }
    }

    // If the current state has a static function, call it
    if (sMagStateTable[mMagData->currentState].run) {
        (this->*sMagStateTable[mMagData->currentState].run)(flTimeout);
    }
    return STATUS_OK;
}

// Set the new state and perform setup for subsequent state run calls
// This is called by state run functions on event detection that drive
// state transitions.
void MagCalibrationFSM::setState(MagCalibrationFSM_State_T newState)
{
    mMagData->currentState      = newState;

    // Restart state timer counter
    mMagData->stateRunCount     = 0;

    // Reset state timeout to disabled/zero
    mMagData->stateTimeoutCount = 0;

    if (sMagStateTable[mMagData->currentState].setup) {
        (this->*sMagStateTable[mMagData->currentState].setup)();
    }
}


// Timeout utility function for use by state init implementations
void MagCalibrationFSM::setStateTimeout(int32_t count)
{
    mMagData->stateTimeoutCount = count;
}
