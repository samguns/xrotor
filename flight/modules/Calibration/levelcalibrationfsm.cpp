/*
 ******************************************************************************
 *
 * @file       levelcalibrationfsm.cpp
 * @author     Alex Beck. Copyright (C) 2015.
 * @brief      Level calibration state machine
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/
#include <project_xflight.h>
#include <callbackinfo.h>

#include <math.h>
#include <alarms.h>
#include <CoordinateConversions.h>
#include <sin_lookup.h>
#include "attitudesettings.h"
#include "attitudestate.h"
#include "calibrationstatus.h"
#include "accelsensor.h"
#include "objectpersistence.h"
#include "calibrationtrigger.h"

// C++ includes
#include <levelcalibrationfsm.h>


// Private constants
#define TIMER_COUNT_PER_SECOND (PIOS_SILA_RATE) // attitude state double fires every 2ms
#define POSITION1_SAMPLE_TIME  (2 * TIMER_COUNT_PER_SECOND)
#define POSITION2_SAMPLE_TIME  (2 * TIMER_COUNT_PER_SECOND)
#define COMPLETED_DISPLAY_TIME (4 * timerTicksPerSecond())

LevelCalibrationFSM::LevelCalibrationFSM_StateHandler_T LevelCalibrationFSM::sLevelStateTable[LEVELCAL_STATE_SIZE] = {
    [LEVELCAL_STATE_INACTIVE]  = { .setup = &LevelCalibrationFSM::setup_inactive,  .run = 0                                   },
    [LEVELCAL_STATE_START]     = { .setup = &LevelCalibrationFSM::setup_start,     .run = 0                                   },
    [LEVELCAL_STATE_POSITION1] = { .setup = &LevelCalibrationFSM::setup_position1, .run = &LevelCalibrationFSM::run_position1 },
    [LEVELCAL_STATE_POSITION2] = { .setup = &LevelCalibrationFSM::setup_position2, .run = &LevelCalibrationFSM::run_position2 },
    [LEVELCAL_STATE_SAVE] =      { .setup = &LevelCalibrationFSM::setup_save,      .run = 0                                   },
    [LEVELCAL_STATE_COMPLETED] = { .setup = &LevelCalibrationFSM::setup_completed, .run = &LevelCalibrationFSM::run_completed },
    [LEVELCAL_STATE_ERROR]     = { .setup = &LevelCalibrationFSM::setup_error,     .run = &LevelCalibrationFSM::run_completed },
};

// pointer to a singleton instance
LevelCalibrationFSM *LevelCalibrationFSM::p_inst = 0;

void LevelCalibrationFSM::calibrationTrigger(void)
{
    CalibrationTriggerData calibration_trigger;

    CalibrationTriggerGet(&calibration_trigger);
    if (calibration_trigger.AccelCal == CALIBRATIONTRIGGER_ACCELCAL_NEWORIENTATIONREADY) {
        if (mLevelData->startSampling == false) {
            mLevelData->startSampling = true;
            mLevelData->samplingInitialised = false;
        }
    }
}


// FSM Setup and Run method implementation


void LevelCalibrationFSM::setup_inactive(void)
{
    mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    CalibrationStatusSet(&(mLevelData->caliStatus));
}

void LevelCalibrationFSM::run_completed(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(LEVELCAL_STATE_INACTIVE);
    }
}

void LevelCalibrationFSM::setup_start(void)
{
    memset(&mLevelData->caliStatus, 0, sizeof(CalibrationStatusData));
    mLevelData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_LEVELCALIBRATION;
    mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_IDLE;
    mLevelData->caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;

    SystemAlarmsAlarmData alarms;
    SystemAlarmsAlarmGet(&alarms);
    if (alarms.Attitude != SYSTEMALARMS_ALARM_OK) {
        mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_ERROR;
        mLevelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_ATTITUDEALARMPREVENTSLEVELCAL;
        CalibrationStatusSet(&(mLevelData->caliStatus));
    } else {
        AttitudeSettingsData attitudeSettings;
        AttitudeSettingsGet(&attitudeSettings);

        attitudeSettings.BoardLevelTrim.Pitch = 0.0f;
        attitudeSettings.BoardLevelTrim.Roll  = 0.0f;
        AttitudeSettingsSet(&attitudeSettings);
        setup_sample_helper(true);

        setState(LEVELCAL_STATE_POSITION1);
    }
}

void LevelCalibrationFSM::setup_position1(void)
{
    mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
    mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_POSITION1;
    CalibrationStatusSet(&(mLevelData->caliStatus));
    setup_sample_helper(false);
    setStateTimeout(POSITION1_SAMPLE_TIME);
}

void LevelCalibrationFSM::setup_sample_helper(bool flagReset)
{
    if (flagReset) {
        // Restart new sampling sequence from scratch
        mLevelData->stateAveragePrevious.zero();
        mLevelData->iterationCount = 0;
        mLevelData->startSampling  = false;
    }
    // Same sampling sequence with a new iteration
    mLevelData->stateSum.zero();
    mLevelData->observationCount  = 0;
    mLevelData->observation2Count = 0;
}


#define SKIP_SAMPLE_TIME     (TIMER_COUNT_PER_SECOND / 2)
#define MAX_ITERATIONS       15
#define MIN_ITERATIONS       2
#define CONVERGENCE_LIMIT    0.006f
#define CONVERGENCE_OK_LIMIT 0.04f
void LevelCalibrationFSM::sample_helper(uint8_t flTimeout, Vector2f &trimResult, bool & resultOK, LevelCalibrationFSM_State_T cur_state, LevelCalibrationFSM_State_T next_state)
{
    // ignore the first SKIP_SAMPLE_TIME samples to put a delay between subsequent retries/iterations
    mLevelData->observation2Count++;
    if (mLevelData->observation2Count < SKIP_SAMPLE_TIME) {
        return;
    }

    if (mLevelData->observationCount == 0) {
        AccelSensorData accelSensor;
        AccelSensorGet(&accelSensor);
        mLevelData->accelBeginning(accelSensor.x, accelSensor.y, accelSensor.z);
    }


    AttitudeStateData attitudeState;
    AttitudeStateGet(&attitudeState);
    mLevelData->observationCount++;
    Vector2f currentAttitude(attitudeState.Pitch, attitudeState.Roll);
    mLevelData->stateSum += currentAttitude;

    if (flTimeout) {
        // calculate the average state vector observed during the sampling period
        Vector2f stateAverage = mLevelData->stateSum / (float)mLevelData->observationCount;
        if (mLevelData->iterationCount == 0) {
            // first iteration, save the current average for comparison on the next iteration
            mLevelData->stateAveragePrevious = stateAverage;

            // increment the iteration count
            mLevelData->iterationCount++;

            // initialise result to false
            resultOK = false;
        } else {
            // increment the iteration count
            mLevelData->iterationCount++;

            // check accels. Note we don't for the first sample as we look for convergence anyway
            AccelSensorData accelSensor;
            AccelSensorGet(&accelSensor);
            Vector3f accelEnd(accelSensor.x, accelSensor.y, accelSensor.z);
            Vector3f accelDiff = accelEnd - mLevelData->accelBeginning;

            // reject samples up to a iteration count after which we give up rejecting.
            if (accelDiff.length() > 0.2f && mLevelData->iterationCount < 15) {
                // we have detected a change in orientation. skip this sample
                // restart an interation in the current state
                setState(cur_state);
                return;
            }

            // calculate the deviation across the iterations
            Vector2f stateDiff = mLevelData->stateAveragePrevious - stateAverage;

            // update the moving average for the previous average
            mLevelData->stateAveragePrevious = (stateAverage * 0.5f) + (mLevelData->stateAveragePrevious * 0.5f);


            // check for convergence or we give up
            if ((mLevelData->iterationCount > MIN_ITERATIONS && stateDiff.length() < CONVERGENCE_LIMIT) || mLevelData->iterationCount >= MAX_ITERATIONS) {
                trimResult = stateAverage;
                resultOK   = (stateDiff.length() < CONVERGENCE_OK_LIMIT);
                // reinitialise for a future sampling phase
                setup_sample_helper(true);
                // jump to the next state
                setState(next_state);
            } else {
                // restart an iteration in the current state
                setState(cur_state);
            }
        }
    }
}

void LevelCalibrationFSM::run_position1(uint8_t flTimeout)
{
    // Wait until we get a signal from the user that we should start samping.
    if (mLevelData->startSampling) {
        if (!mLevelData->samplingInitialised) {
            mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_SAMPLE;
            CalibrationStatusSet(&(mLevelData->caliStatus));
            mLevelData->samplingInitialised = true;
        }

        sample_helper(flTimeout, mLevelData->trimPosition1, mLevelData->trim1OK, LEVELCAL_STATE_POSITION1, LEVELCAL_STATE_POSITION2);
    } else {
        mLevelData->stateRunCount = 0; // keep resetting to avoid timeout until we get the trigger
    }
}

void LevelCalibrationFSM::setup_position2(void)
{
    if (!mLevelData->trim1OK) {
        mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_ERROR;
        mLevelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_LEVELCALERROR;
        CalibrationStatusSet(&(mLevelData->caliStatus));
        setState(LEVELCAL_STATE_ERROR);
    } else {
        mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
        mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_POSITION2;
        CalibrationStatusSet(&(mLevelData->caliStatus));
        setup_sample_helper(false);
        setStateTimeout(POSITION2_SAMPLE_TIME);
    }
}
void LevelCalibrationFSM::run_position2(uint8_t flTimeout)
{
    // Wait until we get a signal from the user that we should start samping.
    if (mLevelData->startSampling) {
        if (!mLevelData->samplingInitialised) {
            mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_SAMPLE;
            CalibrationStatusSet(&(mLevelData->caliStatus));
            mLevelData->samplingInitialised = true;
        }
        sample_helper(flTimeout, mLevelData->trimPosition2, mLevelData->trim2OK, LEVELCAL_STATE_POSITION2, LEVELCAL_STATE_SAVE);
    } else {
        mLevelData->stateRunCount = 0; // keep resetting to avoid timeout until we get the trigger
    }
}
void LevelCalibrationFSM::setup_save(void)
{
    // check OK flag and move to error state
    if (!mLevelData->trim2OK) {
        mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_ERROR;
        mLevelData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_LEVELCALERROR;
        CalibrationStatusSet(&(mLevelData->caliStatus));
        setState(LEVELCAL_STATE_ERROR);
        return;
    }

    Vector2f averageState = (mLevelData->trimPosition1 + mLevelData->trimPosition2) / 2.0f;

    AttitudeSettingsData attitudeSettings;
    AttitudeSettingsGet(&attitudeSettings);

    // Update the biases based on collected data
    // "rotate" the board in the opposite direction as the calculated offset
    attitudeSettings.BoardLevelTrim.Pitch = -averageState.x;
    attitudeSettings.BoardLevelTrim.Roll  = -averageState.y;
    AttitudeSettingsSet(&attitudeSettings);

    // TODO If large pitch roll change, consider automatically re-running the sequence.

    // TODO reject large trim values which require board rotation

    // TODO If a large pitch roll change, consider rejecting it and going into an error state.
    // For example perhaps the board roation needs to be set?  Look for angles greater than 45 degrees.

    mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
    mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_SAVE;
    CalibrationStatusSet(&(mLevelData->caliStatus));
    setState(LEVELCAL_STATE_COMPLETED);
}
void LevelCalibrationFSM::setup_completed(void)
{
    mLevelData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mLevelData->caliStatus.LevelCalibrationState = CALIBRATIONSTATUS_LEVELCALIBRATIONSTATE_COMPLETE;
    CalibrationStatusSet(&(mLevelData->caliStatus));
    mSaveToFlashPending = true;
    setStateTimeout(COMPLETED_DISPLAY_TIME);
}
void LevelCalibrationFSM::setup_error(void)
{
    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void LevelCalibrationFSM::SaveToFlash(void)
{
    if (mSaveToFlashPending) {
        mSaveToFlashPending = false;
        ObjectPersistenceData objper;
        objper.InstanceID   = 0;
        objper.ObjectID     = ATTITUDESETTINGS_OBJID;
        objper.Operation    = OBJECTPERSISTENCE_OPERATION_SAVE;
        objper.Selection    = OBJECTPERSISTENCE_SELECTION_SINGLEOBJECT;
        ObjectPersistenceSet(&objper);
    }
}

LevelCalibrationFSM::LevelCalibrationFSM()
    : mLevelData(0), mSaveToFlashPending(false)
{}

// Private types

// Private functions
// Public API methods
/**
 * Initialise the module, called on startup
 * \returns STATUS_OK on success
 */
int32_t LevelCalibrationFSM::Initialize()
{
    if (mLevelData == 0) {
        mLevelData = (LevelCalibrationFSMData_T *)pios_malloc(sizeof(LevelCalibrationFSMData_T));
        PIOS_Assert(mLevelData);
    }
    memset(mLevelData, 0, sizeof(LevelCalibrationFSMData_T));
    initFSM();

    return STATUS_OK;
}

void LevelCalibrationFSM::Inactive(void)
{
    memset(mLevelData, 0, sizeof(LevelCalibrationFSMData_T));
    initFSM();
}

// Initialise the FSM
void LevelCalibrationFSM::initFSM(void)
{
    mLevelData->currentState = LEVELCAL_STATE_INACTIVE;
}

void LevelCalibrationFSM::Activate()
{
    memset(mLevelData, 0, sizeof(LevelCalibrationFSMData_T));
    mLevelData->currentState = LEVELCAL_STATE_INACTIVE;

    setState(LEVELCAL_STATE_START);
}

CalibrationFSMState_T LevelCalibrationFSM::GetCurrentState(void)
{
    if (mLevelData->currentState == LEVELCAL_STATE_INACTIVE) {
        return CALFSM_STATE_INACTIVE;
    } else {
        return CALFSM_STATE_ACTIVE;
    }
}


void LevelCalibrationFSM::Update(CalibrationFSMDataTypes_T dataUpdateAvail)
{
    // check state and return if completed, inactive or error
    if (mLevelData->currentState == LEVELCAL_STATE_INACTIVE) {
        return;
    }

    // attitudestate is updated at the sensor rate
    if (dataUpdateAvail == CALFSM_DATA_ATTITUDESTATE) {
        // update rate is sensor rate / 4 which is 500/4=125Hz
        runState();
    }
}

int32_t LevelCalibrationFSM::runState(void)
{
    uint8_t flTimeout = false;

    mLevelData->stateRunCount++;

    if (mLevelData->stateTimeoutCount > 0 && mLevelData->stateRunCount > mLevelData->stateTimeoutCount) {
        flTimeout = true;
    }

    // If the current state has a static function, call it
    if (sLevelStateTable[mLevelData->currentState].run) {
        (this->*sLevelStateTable[mLevelData->currentState].run)(flTimeout);
    }
    return STATUS_OK;
}

// Set the new state and perform setup for subsequent state run calls
// This is called by state run functions on event detection that drive
// state transitions.
void LevelCalibrationFSM::setState(LevelCalibrationFSM_State_T newState)
{
    if (mLevelData->currentState == newState) {
        return;
    }
    mLevelData->currentState      = newState;

    // Restart state timer counter
    mLevelData->stateRunCount     = 0;

    // Reset state timeout to disabled/zero
    mLevelData->stateTimeoutCount = 0;

    if (sLevelStateTable[mLevelData->currentState].setup) {
        (this->*sLevelStateTable[mLevelData->currentState].setup)();
    }
}


// Timeout utility function for use by state init implementations
void LevelCalibrationFSM::setStateTimeout(int32_t count)
{
    mLevelData->stateTimeoutCount = count;
}
