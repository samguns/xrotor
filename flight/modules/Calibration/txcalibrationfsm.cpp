/*
 ******************************************************************************
 *
 * @file       txcalibrationfsm.cpp
 * @author     Rodney Grainger Copyright (C) 2015.
 * @brief      TX calibration state machine
 *
 * Terms of Use
 * This software is the intellectual property of author and may not be
 * sold or in other ways commercially redistributed without explicit written
 * permission by the author.
 *****************************************************************************/

#include <project_xflight.h>
#include <callbackinfo.h>

#include <math.h>
#include <alarms.h>
#include "calibrationstatus.h"
#include "objectpersistence.h"
#include "calibrationtrigger.h"
#include "maintenancemodesettings.h"
#include "maintenancemodeinfo.h"
#include "flightmodesettings.h"
#include "manualcontrolsettings.h"
#include "manualcontrolcommand.h"
#include "receiveractivity.h"

// C++ includes
#include <txcalibrationfsm.h>

/* How it works
 *
 * Calibration of the inputs is performed as follows:
 *  1) GCS sets CalibrationControl->Operation = TxCalibration upon which CalibrationStatus->TxCalibrationState changes
 *     to TxType.  At this point the user should choose the transmitter type (Acro, Heli, Ground) and the GCS should set
 *     CalibrationTrigger->TxCal->Operation = Acro/Heli/Ground.
 *  2) After step 1, the GCS should monitor CalibrationStatus->TxCalibrationState to aid in prompting the user to move
 *     each of the control inputs, one by one in the specified order, such that they may be associated with specific functions.
 *     As each channel is assigned, CalibrationStatus->TxCalibrationState is automatically updated to request
 *     the next channel until they have all been discovered.
 *          To support simple transmitters with few control inputs, certain channels may be skipped by setting
 *          CalibrationTrigger->TxCal->Operation = Next.
 *          Setting CalibrationTrigger->TxCal->Operation = Back will allow the user to revisit the previous channel.
 *     Once all channels have been discovered, the state machine will progress to the Verify state.
 *  3) Verify state measures the extent of the channel ranges and determines their neutral position.
 *     If need be, the GCS can set CalibrationTrigger->TxCal->ReverseThrottle,Roll,Pitch,&Yaw to allow the user to reverse
 *     a channels Min/Max values according to system convention.
 *     Once verified, the user should move all the transmitter controls to their neutral positions.
 *     To complete the process set CalibrationTrigger->TxCal->Next upon which channel neutral positions are stored.
 *
 *  In standard fashion, the UAVOs will only be updated when CalibratioControl->Operation = SaveCalibrationResults.
 *  Setting CalibrationTrigger->TxCal = None at any stage prior to saving the results, will restore system to original state.
 *
 */

// Macros

// Private constants
#define TIMER_COUNT_PER_SECOND (1) // The same rate at which receiver activity is updated
#define CHANNEL_IDENTIFY_TIME  (0) // Disabled for now (50 * TIMER_COUNT_PER_SECOND)
#define CHANNEL_VERIFY_TIME    (0) // Disabled for now (50 * TIMER_COUNT_PER_SECOND)
#define HIGH_REPORTING_RATE    (500) // higher reporting rate for some UAVOs for better user interaction

#define DEBOUNCE_COUNT         4
#define UPDATE_COUNT           30
#define COMPLETED_DISPLAY_TIME (4 * timerTicksPerSecond())

TxCalibrationFSM::TxCalibrationFSM_StateHandler_T TxCalibrationFSM::sTxStateTable[TXCAL_STATE_SIZE] = {
    [TXCAL_STATE_INACTIVE] = { .setup = &TxCalibrationFSM::setup_inactive,  .run = 0                                },
    [TXCAL_STATE_START]    = { .setup = &TxCalibrationFSM::setup_start,     .run = &TxCalibrationFSM::run_start     },
    [TXCAL_STATE_IDENTIFY] = { .setup = &TxCalibrationFSM::setup_identify,  .run = &TxCalibrationFSM::run_identify  },
    [TXCAL_STATE_VERIFY]   = { .setup = &TxCalibrationFSM::setup_verify,    .run = &TxCalibrationFSM::run_verify    },
    [TXCAL_STATE_SAVE]     = { .setup = &TxCalibrationFSM::setup_save,      .run = 0                                },
    [TXCAL_STATE_COMPLETE] = { .setup = &TxCalibrationFSM::setup_completed, .run = &TxCalibrationFSM::run_completed },
    [TXCAL_STATE_ERROR]    = { .setup = &TxCalibrationFSM::setup_error,     .run = &TxCalibrationFSM::run_completed },
};


// Data structures to support the various transmitter types
TxCalibrationFSM::TxCalibrationFSM_ChannelOrder_T TxCalibrationFSM::sAcroChannelOrder[NUMBER_OF_ACRO_CHANNELS] = {
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYTHROTTLE,   .index = 0, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYROLL,       .index = 1, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYPITCH,      .index = 2, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYYAW,        .index = 3, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYFLIGHTMODE, .index = 4, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC1,       .index = 6, .canSkip = true  },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC2,       .index = 7, .canSkip = true  },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC3,       .index = 8, .canSkip = true  },
};

TxCalibrationFSM::TxCalibrationFSM_ChannelOrder_T TxCalibrationFSM::sHeliChannelOrder[NUMBER_OF_HELI_CHANNELS] = {
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYCOLLECTIVE, .index = 5, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYTHROTTLE,   .index = 0, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYROLL,       .index = 1, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYPITCH,      .index = 2, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYYAW,        .index = 3, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYFLIGHTMODE, .index = 4, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC1,       .index = 6, .canSkip = true  },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC2,       .index = 7, .canSkip = true  },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC3,       .index = 8, .canSkip = true  }
};

TxCalibrationFSM::TxCalibrationFSM_ChannelOrder_T TxCalibrationFSM::sGrndChannelOrder[NUMBER_OF_GRND_CHANNELS] = {
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYTHROTTLE,   .index = 0, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYYAW,        .index = 3, .canSkip = false },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYFLIGHTMODE, .index = 4, .canSkip = true  },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC1,       .index = 6, .canSkip = true  },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC2,       .index = 7, .canSkip = true  },
    { .state = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYACC3,       .index = 8, .canSkip = true  }
};


// pointer to a singleton instance
TxCalibrationFSM *TxCalibrationFSM::p_inst = 0;


// FSM Setup and Run method implementation

void TxCalibrationFSM::setup_inactive(void)
{
    mTxData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    CalibrationStatusSet(&(mTxData->caliStatus));
}

void TxCalibrationFSM::run_completed(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(TXCAL_STATE_INACTIVE);
    }
}

void TxCalibrationFSM::setup_start(void)
{
    memset(&mTxData->caliStatus, 0, sizeof(CalibrationStatusData));
    mTxData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_TXCALIBRATION;
    mTxData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
    mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDLE;
    mTxData->caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;

    // Do not proceed if we are in maintenance mode.
    MaintenanceModeInfoData mmInfo;
    MaintenanceModeInfoGet(&mmInfo);
    if (mmInfo.MaintenanceModeActive != MAINTENANCEMODEINFO_MAINTENANCEMODEACTIVE_FALSE) {
        mTxData->caliStatus.OperationState     = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
        mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_ERROR;
        mTxData->caliStatus.Error = CALIBRATIONSTATUS_ERROR_MAINTENANCEPREVENTSTXCAL;
        CalibrationStatusSet(&(mTxData->caliStatus));
    } else {
        // Set system into Always disarmed to prevent accidental arming during calibration
        disable_arming_setting();

        // Clear channel groups and numbers, but remember their reversals for later use
        ManualControlSettingsGet(&mTxData->savedManualControlSettings);
        mTxData->tempManualControlSettings = mTxData->savedManualControlSettings;
        for (int x = 0; x < MANUALCONTROLSETTINGS_CHANNELGROUPS_NUMELEM; x++) {
            mTxData->isReversed[x] = ManualControlSettingsChannelMinToArray(mTxData->savedManualControlSettings.ChannelMin)[x] >
                                     ManualControlSettingsChannelMaxToArray(mTxData->savedManualControlSettings.ChannelMax)[x];
            ManualControlSettingsChannelGroupsToArray(mTxData->tempManualControlSettings.ChannelGroups)[x] = MANUALCONTROLSETTINGS_CHANNELGROUPS_NONE;
            ManualControlSettingsChannelNumberToArray(mTxData->tempManualControlSettings.ChannelNumber)[x] = 0;
        }
        ManualControlSettingsSet(&mTxData->tempManualControlSettings);

        // Increase telemetry rate on ManualControlCommand to allow responsive visualisation of controls in GCS
        increase_manualcontrolcommanddata_rate();

        // Prevent any attempts at recognising stick commands.
        disable_maintenancemode();

        // Wait for GCS to specify Acro, Heli, or Ground style Tx.
        mTxData->caliStatus.OperationState     = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
        mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_TXTYPE;
        CalibrationStatusSet(&(mTxData->caliStatus));

        // Got to start somewhere
        mTxData->channelIndex = 0;
    }
}

void TxCalibrationFSM::run_start(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(TXCAL_STATE_ERROR);
    }
}

void TxCalibrationFSM::setup_identify()
{
    // Ask for control movement on a channel
    mTxData->caliStatus.OperationState     = CALIBRATIONSTATUS_OPERATIONSTATE_ACTIVE;
    mTxData->caliStatus.TxCalibrationState = mTxData->channelOrder[mTxData->channelIndex].state;
    CalibrationStatusSet(&(mTxData->caliStatus));

    mTxData->previousChannel = 0;
    mTxData->debounceCount   = 0;

    setStateTimeout(CHANNEL_IDENTIFY_TIME);
}

void TxCalibrationFSM::run_identify(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(TXCAL_STATE_ERROR);
    }

    if (mTxData->updateReason == CALFSM_DATA_RECEIVERACTIVITY) {
        ReceiverActivityData raData;
        ReceiverActivityGet(&raData);

        if (raData.ActiveChannel == 255 || raData.ActiveGroup >= RECEIVERACTIVITY_ACTIVEGROUP_NONE) {
            return;
        }

        if (mTxData->debounceCount == 0) {
            mTxData->debounceCount++;
            mTxData->previousChannel = raData.ActiveChannel;
            return;
        } else if (raData.ActiveChannel != mTxData->previousChannel) {
            mTxData->debounceCount--;
            return;
        } else if (mTxData->debounceCount < DEBOUNCE_COUNT) {
            mTxData->debounceCount++;
            return;
        }

        // A control input has moved, try and assign it if it has not been done

        ManualControlSettingsGet(&mTxData->tempManualControlSettings);

        uint32_t index = mTxData->channelOrder[mTxData->channelIndex].index;
        if (ManualControlSettingsChannelGroupsToArray(mTxData->tempManualControlSettings.ChannelGroups)[index] >= MANUALCONTROLSETTINGS_CHANNELGROUPS_NONE) {
            bool alreadyUsed = false;
            for (int x = 0; x < MANUALCONTROLSETTINGS_CHANNELGROUPS_NUMELEM; x++) {
                if (ManualControlSettingsChannelNumberToArray(mTxData->tempManualControlSettings.ChannelNumber)[x] == raData.ActiveChannel) {
                    alreadyUsed = true;
                    break;
                }
            }

            if (!alreadyUsed) {
                ManualControlSettingsChannelGroupsOptions group;
                switch (raData.ActiveGroup) {
                case RECEIVERACTIVITY_ACTIVEGROUP_PWM:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_PWM;
                    break;
                case RECEIVERACTIVITY_ACTIVEGROUP_PPM:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_PPM;
                    break;
                case RECEIVERACTIVITY_ACTIVEGROUP_DSMMAINPORT:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_DSMMAINPORT;
                    break;
                case RECEIVERACTIVITY_ACTIVEGROUP_DSMFLEXIPORT:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_DSMFLEXIPORT;
                    break;
                case RECEIVERACTIVITY_ACTIVEGROUP_SBUS:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_SBUS;
                    break;
                case RECEIVERACTIVITY_ACTIVEGROUP_SRXL:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_SRXL;
                    break;
                case RECEIVERACTIVITY_ACTIVEGROUP_GCS:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_GCS;
                    break;
                case RECEIVERACTIVITY_ACTIVEGROUP_OPLINK:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_OPLINK;
                    break;
                default:
                    group = MANUALCONTROLSETTINGS_CHANNELGROUPS_NONE;
                    break;
                }

                if (mTxData->channelOrder[mTxData->channelIndex].state == CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYFLIGHTMODE) {
                    // If this is the flight mode channel, then assign 3 flight modes as a default
                    mTxData->tempManualControlSettings.FlightModeNumber = 3;
                }

                ManualControlSettingsChannelGroupsToArray(mTxData->tempManualControlSettings.ChannelGroups)[index] = group;
                ManualControlSettingsChannelNumberToArray(mTxData->tempManualControlSettings.ChannelNumber)[index] = raData.ActiveChannel;

                ManualControlSettingsSet(&mTxData->tempManualControlSettings);

                // Are we done identifying channels yet?
                mTxData->channelIndex++;
                if (mTxData->channelIndex >= mTxData->numberOfChannels) {
                    setState(TXCAL_STATE_VERIFY); // Yes
                } else {
                    setState(mTxData->currentState); // No, ask for next channel
                }
                setStateTimeout(CHANNEL_IDENTIFY_TIME);
            }
        }
    }
}

void TxCalibrationFSM::setup_verify()
{
    mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_VERIFY;
    CalibrationStatusSet(&(mTxData->caliStatus));

    mTxData->iterationCount = 0;

    ManualControlSettingsGet(&mTxData->tempManualControlSettings);

    ManualControlCommandData mcData;
    ManualControlCommandGet(&mcData);

    for (int x = 0; x < MANUALCONTROLSETTINGS_CHANNELNUMBER_NUMELEM; x++) {
        // Set initial neutral and range.
        if (ManualControlSettingsChannelGroupsToArray(mTxData->tempManualControlSettings.ChannelGroups)[x] != MANUALCONTROLSETTINGS_CHANNELGROUPS_NONE) {
            ManualControlSettingsChannelNeutralToArray(mTxData->tempManualControlSettings.ChannelNeutral)[x] =
                ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[x]     =
                    ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[x] = mcData.Channel[x];
        } else {
            // Channel not assigned, set to zero
            ManualControlSettingsChannelNeutralToArray(mTxData->tempManualControlSettings.ChannelNeutral)[x] = 0;
        }
    }

    setStateTimeout(CHANNEL_VERIFY_TIME);
}

void TxCalibrationFSM::run_verify(uint8_t flTimeout)
{
    if (flTimeout) {
        setState(TXCAL_STATE_ERROR);
    }

    // Determine channel ranges, and set neutral values.
    if (mTxData->updateReason == CALFSM_DATA_RECEIVERCHANNEL) {
        ManualControlCommandData mcData;
        ManualControlCommandGet(&mcData);

        for (int x = 0; x < MANUALCONTROLSETTINGS_CHANNELNUMBER_NUMELEM; x++) {
            // Take current position as the neutral
            if (mcData.Channel[x] < (uint16_t)PIOS_RCVR_NODRIVER) {
                ManualControlSettingsChannelNeutralToArray(mTxData->tempManualControlSettings.ChannelNeutral)[x] = mcData.Channel[x];

                // Adjust the Min and Max
                if (mTxData->isReversed[x]) {
                    // Channel is reversed
                    if (ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[x] < mcData.Channel[x]) {
                        ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[x] = mcData.Channel[x];
                    }
                    if (ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[x] > mcData.Channel[x]) {
                        ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[x] = mcData.Channel[x];
                    }
                } else {
                    if (ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[x] > mcData.Channel[x]) {
                        ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[x] = mcData.Channel[x];
                    }
                    if (ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[x] < mcData.Channel[x]) {
                        ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[x] = mcData.Channel[x];
                    }
                }
            } else {
                continue;
            }
        }
        // Some channels are special
        AdjustSpecialCaseNeutrals();

        // Prevent swamping of telemetry channel and only periodically update ManualControlSettings
        if (++mTxData->iterationCount >= UPDATE_COUNT) {
            mTxData->iterationCount = 0;
            ManualControlSettingsSet(&mTxData->tempManualControlSettings);
        }
    }
}

void TxCalibrationFSM::AdjustSpecialCaseNeutrals(void)
{
    // Flight mode neutral must be in the middle
    ManualControlSettingsChannelNeutralToArray(mTxData->tempManualControlSettings.ChannelNeutral)[MANUALCONTROLSETTINGS_CHANNELNUMBER_FLIGHTMODE] =
        (ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[MANUALCONTROLSETTINGS_CHANNELNUMBER_FLIGHTMODE] +
         ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[MANUALCONTROLSETTINGS_CHANNELNUMBER_FLIGHTMODE]) / 2;

    // Ground vehicle has reversible motors, so throttle neutral is the centre.
    // So unless we are using a ground Tx, set neutral to 4% of range
    if (mTxData->channelOrder != sGrndChannelOrder) {
        ManualControlSettingsChannelNeutralToArray(mTxData->tempManualControlSettings.ChannelNeutral)[MANUALCONTROLSETTINGS_CHANNELNUMBER_THROTTLE] =
            ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[MANUALCONTROLSETTINGS_CHANNELNUMBER_THROTTLE] +
            ((ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[MANUALCONTROLSETTINGS_CHANNELNUMBER_THROTTLE] -
              ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[MANUALCONTROLSETTINGS_CHANNELNUMBER_THROTTLE]) * 0.04f);
    }
}


void TxCalibrationFSM::setup_save(void)
{
    // TODO Check some ranges to make sure none of them look bad.
    // Maybe set BadThrottleOrCollectiveInputRange, even though sanity check does same thin?
    // (channelMax.Throttle - channelMin.Throttle) > 300.0f
    //
    // Don't restore settings so that we can see what is wrong
    // mSaveToFlashPending = false;
    /// else if all good, then go to complete.

    setStateTimeout(0);

    // Restore
    restore_arming_setting();

    restore_maintenancemode();

    restore_manualcontrolcommanddata_rate();

    mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_SAVE;
    CalibrationStatusSet(&(mTxData->caliStatus));
    setState(TXCAL_STATE_COMPLETE);
}

void TxCalibrationFSM::setup_completed(void)
{
    mTxData->caliStatus.OperationState     = CALIBRATIONSTATUS_OPERATIONSTATE_COMPLETED;
    mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_COMPLETE;
    CalibrationStatusSet(&(mTxData->caliStatus));
    mSaveToFlashPending = true;
    setStateTimeout(COMPLETED_DISPLAY_TIME);
}

void TxCalibrationFSM::setup_error(void)
{
    // Restore settings to what they were at the start, and report that there was an error

    ManualControlSettingsSet(&mTxData->savedManualControlSettings);

    restore_arming_setting();

    restore_maintenancemode();

    restore_manualcontrolcommanddata_rate();

    // Update status
    memset(&mTxData->caliStatus, 0, sizeof(CalibrationStatusData));
    mTxData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_NONE;
    mTxData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_ERROR;
    mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_ERROR;
    mTxData->caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;
    CalibrationStatusSet(&(mTxData->caliStatus));

    setStateTimeout(COMPLETED_DISPLAY_TIME);
}


void TxCalibrationFSM::SaveToFlash(void)
{
    if (mSaveToFlashPending) {
        mSaveToFlashPending = false;

        // Save ManualConrolSettings
        ObjectPersistenceData objper;
        objper.InstanceID = 0;
        objper.ObjectID   = MANUALCONTROLSETTINGS_OBJID;
        objper.Operation  = OBJECTPERSISTENCE_OPERATION_SAVE;
        objper.Selection  = OBJECTPERSISTENCE_SELECTION_SINGLEOBJECT;
        ObjectPersistenceSet(&objper);

        // Save MaintenanceModeSettings
        objper.ObjectID = MAINTENANCEMODESETTINGS_OBJID;
        ObjectPersistenceSet(&objper);
    }
}

void TxCalibrationFSM::calibrationTrigger(void)
{
    CalibrationTriggerData calibration_trigger;

    CalibrationTriggerGet(&calibration_trigger);

    // Issuing a none will cancel calibration, and restore settings to their states
    if (calibration_trigger.TxCal == CALIBRATIONTRIGGER_TXCAL_NONE) {
        ManualControlSettingsSet(&mTxData->savedManualControlSettings);

        restore_arming_setting();

        restore_maintenancemode();

        restore_manualcontrolcommanddata_rate();

        // Update status
        memset(&mTxData->caliStatus, 0, sizeof(CalibrationStatusData));
        mTxData->caliStatus.Operation = CALIBRATIONSTATUS_OPERATION_NONE;
        mTxData->caliStatus.OperationState = CALIBRATIONSTATUS_OPERATIONSTATE_IDLE;
        mTxData->caliStatus.TxCalibrationState = CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDLE;
        mTxData->caliStatus.Error     = CALIBRATIONSTATUS_ERROR_NOERROR;
        CalibrationStatusSet(&(mTxData->caliStatus));

        TxCalibrationFSM::Inactive();

        return;
    }

    switch (mTxData->currentState) {
    case TXCAL_STATE_START:
        switch (calibration_trigger.TxCal) {
        case CALIBRATIONTRIGGER_TXCAL_ACRO:
            mTxData->channelOrder     = sAcroChannelOrder;
            mTxData->numberOfChannels = NUMBER_OF_ACRO_CHANNELS;
            setState(TXCAL_STATE_IDENTIFY);
            break;
        case CALIBRATIONTRIGGER_TXCAL_HELI:
            mTxData->channelOrder     = sHeliChannelOrder;
            mTxData->numberOfChannels = NUMBER_OF_HELI_CHANNELS;
            setState(TXCAL_STATE_IDENTIFY);
            break;
        case CALIBRATIONTRIGGER_TXCAL_GROUND:
            mTxData->channelOrder     = sGrndChannelOrder;
            mTxData->numberOfChannels = NUMBER_OF_GRND_CHANNELS;
            setState(TXCAL_STATE_IDENTIFY);
            break;
        default:
            break;
        }
        break;

    case TXCAL_STATE_VERIFY:
        switch (calibration_trigger.TxCal) {
        case CALIBRATIONTRIGGER_TXCAL_REVERSETHROTTLE:
            reverse_channel(MANUALCONTROLSETTINGS_CHANNELNUMBER_THROTTLE);
            break;

        case CALIBRATIONTRIGGER_TXCAL_REVERSEROLL:
            reverse_channel(MANUALCONTROLSETTINGS_CHANNELNUMBER_ROLL);
            break;

        case CALIBRATIONTRIGGER_TXCAL_REVERSEPITCH:
            reverse_channel(MANUALCONTROLSETTINGS_CHANNELNUMBER_PITCH);
            break;

        case CALIBRATIONTRIGGER_TXCAL_REVERSEYAW:
            reverse_channel(MANUALCONTROLSETTINGS_CHANNELNUMBER_YAW);
            break;
        default:
            break;
        }
    // Fall through...
    /* no break */
    case TXCAL_STATE_IDENTIFY:
        if (calibration_trigger.TxCal == CALIBRATIONTRIGGER_TXCAL_BACK) {
            command_back_helper();
        } else if (calibration_trigger.TxCal == CALIBRATIONTRIGGER_TXCAL_NEXT) {
            command_next_helper();
        }
        break;
    default:
        break;
    }
}

void TxCalibrationFSM::command_next_helper(void)
{
    switch (mTxData->currentState) {
    case TXCAL_STATE_IDENTIFY:
        if (mTxData->channelOrder[mTxData->channelIndex].canSkip) {
            // Force 1 flight mode if user skipped the flight mode switch on a ground style Tx.
            if ((mTxData->channelOrder == sGrndChannelOrder) &&
                (mTxData->channelOrder[mTxData->channelIndex].state == CALIBRATIONSTATUS_TXCALIBRATIONSTATE_IDENTIFYFLIGHTMODE)) {
                ManualControlSettingsGet(&mTxData->tempManualControlSettings);
                mTxData->tempManualControlSettings.FlightModeNumber = 1;
                ManualControlSettingsSet(&mTxData->tempManualControlSettings);
            }
            mTxData->channelIndex++;
        }
        if (mTxData->channelIndex >= mTxData->numberOfChannels) {
            setState(TXCAL_STATE_VERIFY);
        } else {
            setState(mTxData->currentState);
        }
        setStateTimeout(CHANNEL_IDENTIFY_TIME);
        break;
    case TXCAL_STATE_VERIFY:
        AdjustSpecialCaseNeutrals();
        ManualControlSettingsSet(&mTxData->tempManualControlSettings);
        setState(TXCAL_STATE_SAVE);
        setStateTimeout(0);
        break;
    default:
        break;
    }
}


void TxCalibrationFSM::command_back_helper(void)
{
    switch (mTxData->currentState) {
    case TXCAL_STATE_IDENTIFY:
        if (mTxData->channelIndex > 0) {
            mTxData->channelIndex--;
        }
        clear_channel(mTxData->channelIndex);
        setState(TXCAL_STATE_IDENTIFY);
        setStateTimeout(CHANNEL_IDENTIFY_TIME);
        break;

    case TXCAL_STATE_VERIFY:
        mTxData->channelIndex = mTxData->numberOfChannels - 1;
        clear_channel(mTxData->channelIndex);
        setState(TXCAL_STATE_IDENTIFY);
        setStateTimeout(CHANNEL_IDENTIFY_TIME);
        break;

    default:
        break;
    }
}

void TxCalibrationFSM::reverse_channel(uint8_t channel)
{
    uint16_t tmp;

    tmp = ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[channel];

    ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[channel] =
        ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[channel];

    ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[channel] = tmp;

    mTxData->isReversed[channel] = ManualControlSettingsChannelMinToArray(mTxData->tempManualControlSettings.ChannelMin)[channel] >
                                   ManualControlSettingsChannelMaxToArray(mTxData->tempManualControlSettings.ChannelMax)[channel];

    ManualControlSettingsSet(&mTxData->tempManualControlSettings);
}

void TxCalibrationFSM::clear_channel(uint8_t channel)
{
    ManualControlSettingsChannelGroupsToArray(mTxData->tempManualControlSettings.ChannelGroups)[mTxData->channelOrder[channel].index] = MANUALCONTROLSETTINGS_CHANNELGROUPS_NONE;
    ManualControlSettingsChannelNumberToArray(mTxData->tempManualControlSettings.ChannelNumber)[mTxData->channelOrder[channel].index] = 0;

    ManualControlSettingsSet(&mTxData->tempManualControlSettings);
}

void TxCalibrationFSM::disable_arming_setting(void)
{
    FlightModeSettingsData fmSettings;

    FlightModeSettingsGet(&fmSettings);
    mTxData->savedArming = fmSettings.Arming;
    fmSettings.Arming    = FLIGHTMODESETTINGS_ARMING_ALWAYSDISARMED;
    FlightModeSettingsSet(&fmSettings);

    // TODO Not sure if I should make this sticky and store in flash in case something goes awry...?
    // ObjectPersistenceData objper;
    // objper.InstanceID = 0;
    // objper.ObjectID   = FLIGHTMODESETTINGS_OBJID;
    // objper.Operation  = OBJECTPERSISTENCE_OPERATION_SAVE;
    // objper.Selection  = OBJECTPERSISTENCE_SELECTION_SINGLEOBJECT;
    // ObjectPersistenceSet(&objper);
}

void TxCalibrationFSM::restore_arming_setting(void)
{
    FlightModeSettingsData fmSettings;

    FlightModeSettingsGet(&fmSettings);
    fmSettings.Arming = mTxData->savedArming;
    FlightModeSettingsSet(&fmSettings);

    // TODO Corresponding change with whatever was done in disable_arming_settings
}

void TxCalibrationFSM::increase_manualcontrolcommanddata_rate(void)
{
    UAVObjMetadata metadata;

    ManualControlCommandGetMetadata((UAVTalkConnection)NULL, &metadata);
    mTxData->savedManualControlCommandRate = metadata.telemetryUpdatePeriod;
    metadata.telemetryUpdatePeriod = HIGH_REPORTING_RATE;
    ManualControlCommandSetMetadata((UAVTalkConnection)NULL, &metadata);
}

void TxCalibrationFSM::restore_manualcontrolcommanddata_rate(void)
{
    UAVObjMetadata metadata;

    ManualControlCommandGetMetadata((UAVTalkConnection)NULL, &metadata);
    metadata.telemetryUpdatePeriod = mTxData->savedManualControlCommandRate;
    ManualControlCommandSetMetadata((UAVTalkConnection)NULL, &metadata);
}

void TxCalibrationFSM::disable_maintenancemode(void)
{
    MaintenanceModeSettingsData maintData;

    MaintenanceModeSettingsGet(&maintData);
    mTxData->savedStickTimeHoldTime = maintData.StickHoldTime;
    maintData.StickHoldTime = 0;
    MaintenanceModeSettingsSet(&maintData);
}

void TxCalibrationFSM::restore_maintenancemode(void)
{
    MaintenanceModeSettingsData maintData;

    MaintenanceModeSettingsGet(&maintData);
    maintData.StickHoldTime = mTxData->savedStickTimeHoldTime;
    MaintenanceModeSettingsSet(&maintData);
}


// Private types

// Private functions
TxCalibrationFSM::TxCalibrationFSM()
    : mTxData(0), mSaveToFlashPending(false)
{}


// Public API methods
/**
 * Initialise the module, called on startup
 * \returns STATUS_OK on success
 */
int32_t TxCalibrationFSM::Initialize()
{
    if (mTxData == 0) {
        mTxData = (TxCalibrationFSMData_T *)pios_malloc(sizeof(TxCalibrationFSMData_T));
        PIOS_Assert(mTxData);
    }
    memset(mTxData, 0, sizeof(TxCalibrationFSMData_T));
    initFSM();

    return STATUS_OK;
}

void TxCalibrationFSM::Inactive(void)
{
    memset(mTxData, 0, sizeof(TxCalibrationFSMData_T));
    initFSM();
}

// Initialise the FSM
void TxCalibrationFSM::initFSM(void)
{
    mTxData->currentState = TXCAL_STATE_INACTIVE;
}

void TxCalibrationFSM::Activate()
{
    memset(mTxData, 0, sizeof(TxCalibrationFSMData_T));
    mTxData->currentState = TXCAL_STATE_INACTIVE;

    setState(TXCAL_STATE_START);
}

CalibrationFSMState_T TxCalibrationFSM::GetCurrentState(void)
{
    if (mTxData->currentState == TXCAL_STATE_INACTIVE) {
        return CALFSM_STATE_INACTIVE;
    } else {
        return CALFSM_STATE_ACTIVE;
    }
}


void TxCalibrationFSM::Update(CalibrationFSMDataTypes_T dataUpdateAvail)
{
    // check state and return if completed, inactive or error
    if (mTxData->currentState == TXCAL_STATE_INACTIVE) {
        return;
    }

    mTxData->updateReason = dataUpdateAvail;
    if ((dataUpdateAvail == CALFSM_DATA_RECEIVERACTIVITY) || (dataUpdateAvail == CALFSM_DATA_RECEIVERCHANNEL)) {
        runState();
    } else if (dataUpdateAvail == CALFSM_DATA_TIMER) {
        runState();
    }
}

int32_t TxCalibrationFSM::runState(void)
{
    uint8_t flTimeout = false;

    if (mTxData->updateReason == CALFSM_DATA_TIMER) {
        // This will be at the normal 1000ms timer rate set from calibrationTask
        mTxData->stateRunCount++;
        if (mTxData->stateTimeoutCount > 0 && mTxData->stateRunCount > mTxData->stateTimeoutCount) {
            flTimeout = true;
        }
    }

    // If the current state has a static function, call it
    if (sTxStateTable[mTxData->currentState].run) {
        (this->*sTxStateTable[mTxData->currentState].run)(flTimeout);
    }
    return STATUS_OK;
}

// Set the new state and perform setup for subsequent state run calls
// This is called by state run functions on event detection that drive
// state transitions.
void TxCalibrationFSM::setState(TxCalibrationFSM_State_T newState)
{
    mTxData->currentState      = newState;

    // Restart state timer counter
    mTxData->stateRunCount     = 0;

    // Reset state timeout to disabled/zero
    mTxData->stateTimeoutCount = 0;

    if (sTxStateTable[mTxData->currentState].setup) {
        (this->*sTxStateTable[mTxData->currentState].setup)();
    }
}


// Timeout utility function for use by state init implementations
void TxCalibrationFSM::setStateTimeout(int32_t count)
{
    mTxData->stateTimeoutCount = count;
}
