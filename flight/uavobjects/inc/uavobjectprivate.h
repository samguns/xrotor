/**
 ******************************************************************************
 *
 * @file       uavobjectprivate.h
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2014.
 * @brief      Private declarations for uavobject manager and persistence handler.
 *             --
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#ifndef UAVOBJECTPRIVATE_H_
#define UAVOBJECTPRIVATE_H_


#if (defined(__MACH__) && defined(__APPLE__))
#include <mach-o/getsect.h>
#endif

// Constants

// Private types

// Macros
#define SET_BITS(var, shift, value, mask) var = (var & ~(mask << shift)) | (value << shift);

// Mach-o: dummy segment to calculate ASLR offset in sim_osx
#if (defined(__MACH__) && defined(__APPLE__))
static long _aslr_offset __attribute__((section("__DATA,_aslr")));
#endif

/* Table of UAVO handles */
#if (defined(__MACH__) && defined(__APPLE__))
/* Mach-o format */
static UAVOData * *__start__uavo_handles;
static UAVOData * *__stop__uavo_handles;
#else
/* ELF format: automagically defined at compile time */
extern UAVOData *__start__uavo_handles[] __attribute__((weak));
extern UAVOData *__stop__uavo_handles[] __attribute__((weak));
#endif

#define UAVO_LIST_ITERATE(_item) \
    for (UAVOData * *_uavo_slot = __start__uavo_handles; \
         _uavo_slot && _uavo_slot < __stop__uavo_handles; \
         _uavo_slot++) { \
        UAVOData *_item = *_uavo_slot; \
        if (_item == NULL) { continue; }

/**
 * List of event queues and the eventmask associated with the queue.
 */

/** opaque type for instances **/
typedef void *InstanceHandle;

struct ObjectEventEntry {
    struct ObjectEventEntry *next;
    xQueueHandle queue;
    xSemaphoreHandle sem;
    UAVObjEventCallback     cb;
    uint8_t eventMask;
};

/*
   MetaInstance   == [UAVOBase [UAVObjMetadata]]
   SingleInstance == [UAVOBase [UAVOData [InstanceData]]]
   MultiInstance  == [UAVOBase [UAVOData [NumInstances [InstanceData0 [next]]]]
                                                  ____________________/
   \-->[InstanceData1 [next]]
                                                  _________...________/
   \-->[InstanceDataN [next]]
 */

/** all information about a metaobject are hardcoded constants **/
#define MetaNumBytes sizeof(UAVObjMetadata)
#define MetaBaseObjectPtr(obj)           ((UAVOData *)((void *)(obj) - offsetof(UAVOData, defMetaObj)))
#define MetaDataPtr(obj)                 (&((UAVOMeta *)obj)->metadata)
#define LinkedMetaDataPtr(obj)           ((UAVObjMetadata *)&((obj)->defMetaObj.metadata))

/** all information about instances are dependant on object type **/
#define ObjSingleInstanceDataOffset(obj) ((void *)(&(((UAVOSingle *)obj)->instance0)))
#define InstanceDataOffset(inst)         ((void *)&(((UAVOMultiInst *)inst)->instance))
#define InstanceData(instance)           ((void *)instance)

// Private functions
int32_t sendEvent(UAVOBase *obj, uint16_t instId, UAVObjEventType event);
InstanceHandle getInstance(UAVOData *obj, uint16_t instId);

#endif /* UAVOBJECTPRIVATE_H_ */
