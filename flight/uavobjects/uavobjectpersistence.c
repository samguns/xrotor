/**
 ******************************************************************************
 *
 * @file       uavobjectpersistence.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2014.
 * @brief      handles uavo persistence
 *             --
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "openpilot.h"
#include "pios_struct_helper.h"
#include "inc/uavobjectprivate.h"

extern uintptr_t pios_uavo_settings_fs_id;

/**
 * Save the data of the specified object to the file system (SD card).
 * If the object contains multiple instances, all of them will be saved.
 * A new file with the name of the object will be created.
 * The object data can be restored using the UAVObjLoad function.
 * @param[in] obj The object handle.
 * @param[in] instId The instance ID
 * @return STATUS_OK if success or STATUS_ERROR if failure
 */
int32_t UAVObjSave(__attribute__((unused)) UAVTalkConnection connection, UAVOBase *base, uint16_t instId)
{
    int32_t status;

    PIOS_Assert(base);

    if (UAVObjIsMetaobject(base)) {
        if (instId != 0) {
            return STATUS_INVAL;
        }

        // Update the default metadata. This will then be used if default metadata is restored.
        memcpy((void *)MetaDataPtr(base), MetaDataPtr(MetaObjectPtr(MetaBaseObjectPtr(base), connection)), UAVObjGetNumBytes(base));
        if ((status = PIOS_FLASHFS_ObjSave(pios_uavo_settings_fs_id, UAVObjGetID(base), instId, (uint8_t *)MetaDataPtr(base), UAVObjGetNumBytes(base)))) {
            return status;
        }
    } else {
        InstanceHandle instEntry = getInstance((UAVOData *)base, instId);

        if (instEntry == NULL) {
            return STATUS_INVAL;
        }

        if (InstanceData(instEntry) == NULL) {
            return STATUS_INVAL;
        }

        if ((status = PIOS_FLASHFS_ObjSave(pios_uavo_settings_fs_id, UAVObjGetID(base), instId, InstanceData(instEntry), UAVObjGetNumBytes(base)))) {
            return status;
        }
    }
    return STATUS_OK;
}


/**
 * Load an object from the file system (SD card).
 * A file with the name of the object will be opened.
 * The object data can be saved using the UAVObjSave function.
 * @param[in] obj The object handle.
 * @param[in] instId The object instance
 * @return STATUS_OK if success or STATUS_ERROR if failure
 */
int32_t UAVObjLoad(__attribute__((unused)) UAVTalkConnection connection, UAVOBase *base, uint16_t instId)
{
    int32_t status;

    PIOS_Assert(base);

    if (UAVObjIsMetaobject(base)) {
        if (instId != 0) {
            return STATUS_INVAL;
        }

        // Fire event on success
        // Update the default metadata and then the connection specific metadata
        if ((status = PIOS_FLASHFS_ObjLoad(pios_uavo_settings_fs_id, UAVObjGetID(base), instId, (uint8_t *)MetaDataPtr(base), UAVObjGetNumBytes(base)))) {
            return status;
        } else {
            memcpy(MetaDataPtr(MetaObjectPtr(MetaBaseObjectPtr(base), connection)), (void *)MetaDataPtr(base), UAVObjGetNumBytes(base));
            sendEvent(base, instId, EV_UNPACKED);
        }
    } else {
        InstanceHandle instEntry = getInstance((UAVOData *)base, instId);

        if (instEntry == NULL) {
            return STATUS_INVAL;
        }

        // Fire event on success
        if ((status = PIOS_FLASHFS_ObjLoad(pios_uavo_settings_fs_id, UAVObjGetID(base), instId, InstanceData(instEntry), UAVObjGetNumBytes(base)))) {
            return status;
        } else {
            sendEvent(base, instId, EV_UNPACKED);
        }
    }

    return STATUS_OK;
}

/**
 * Delete an object from the file system (SD card).
 * @param[in] obj The object handle.
 * @param[in] instId The object instance
 * @return STATUS_OK if success or STATUS_ERROR if failure
 */
int32_t UAVObjDelete(UAVOBase *base, uint16_t instId)
{
    PIOS_Assert(base);
    return PIOS_FLASHFS_ObjDelete(pios_uavo_settings_fs_id, UAVObjGetID(base), instId);
}
