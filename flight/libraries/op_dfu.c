/**
 ******************************************************************************
 * @addtogroup CopterControlBL CopterControl BootLoader
 * @brief These files contain the code to the CopterControl Bootloader.
 *
 * @{
 * @file       op_dfu.c
 * @author     The OPNG Team, http://www.opng.org Copyright (C) 2015.
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      This file contains the DFU commands handling code
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

/* Includes ------------------------------------------------------------------*/
#include "pios.h"
#include <stdbool.h>
#include <stdarg.h>
#include "op_dfu.h"
#include "pios_bl_helper.h"
#include <pios_board_info.h>


// Jump to the application
extern void jump_to_app();

// programmable devices
Device devicesTable[10];
uint8_t numberOfDevices = 0;

DFUProgType currentProgrammingDestination; // flash, flash_trough spi
uint8_t currentDeviceCanRead;
uint8_t currentDeviceCanWrite;
Device currentDevice;

#define BUF_SIZE 32
uint8_t SendBuffer[BUF_SIZE];
// Argument to be passed back with status request
uint32_t statusArg = 0;

// Download vars
uint32_t downSizeOfLastPacket = 0;
uint32_t downPacketTotal = 0;
uint32_t downPacketCurrent    = 0;
DFUTransfer downType = 0;

/* Extern variables ----------------------------------------------------------*/
extern DFUStates blState;
extern uint8_t JumpToApp;
extern int32_t platformSendData(const uint8_t *msg, uint16_t msg_len);
extern void platformSendDebugMsg(const uint8_t *msg, uint16_t msg_len);
/* Private function prototypes -----------------------------------------------*/
static uint32_t baseOfAdressType(uint8_t type);
static uint8_t isBiggerThanAvailable(uint8_t type, uint32_t size);
static void OPDfuIni(uint8_t discover);
static void BooBDebug(__attribute__((unused)) const char *format, ...);
static void BooBAck(uint32_t count, uint32_t expectCount, DFUStates state);
bool flashRead(uint8_t *buffer, uint32_t adr, DFUProgType type);
/* Private functions ---------------------------------------------------------*/
void sendData(uint8_t *buf, uint16_t size);
uint32_t CalcFirmCRC(void);

// Omit this to same space if not required
#ifdef INCLUDE_DATA_DOWNLOAD
void dataDownload(__attribute__((unused)) DownloadAction action)
{
    if ((blState == downloading)) {
        uint8_t packetSize;
        uint32_t offset;
        uint32_t partoffset;
        SendBuffer[0] = 0x01;
        SendBuffer[1] = downPacketCurrent >> 24;
        SendBuffer[2] = downPacketCurrent >> 16;
        SendBuffer[3] = downPacketCurrent >> 8;
        SendBuffer[4] = downPacketCurrent;
        if (downPacketCurrent == downPacketTotal - 1) {
            packetSize = downSizeOfLastPacket;
        } else {
            packetSize = 14;
        }
        for (uint8_t x = 0; x < packetSize; ++x) {
            partoffset = (downPacketCurrent * 14 * 4) + (x * 4);
            offset     = baseOfAdressType(downType) + partoffset;
            if (!flashRead(SendBuffer + (5 + x * 4), offset,
                           currentProgrammingDestination)) {
                blState = Last_operation_failed;
            }
        }
        downPacketCurrent = downPacketCurrent + 1;
        if (downPacketCurrent > downPacketTotal - 1) {
            blState   = Last_operation_Success;
            statusArg = DOWNLOADING;
        }
        sendData(SendBuffer, 63);
    }
}
#endif /* ifdef INCLUDE_DATA_DOWNLOAD */

void processComand(uint8_t *buf)
{
    // Downloader state
    static uint32_t blockCount      = 0;
    static uint32_t expectedCRC     = 0;
    static uint8_t sizeOfLastPacket = 0;
    static uint8_t transferType;
    static uint32_t curPacket = 0;

    bootloader_cmd_t *cmd     = (bootloader_cmd_t *)buf;

    swap32(cmd->command);

    switch (cmd->command) {
    case ENTER_IAP:
        if (((blState == BLidle) && (cmd->deviceNum < numberOfDevices)) ||
            (blState == IAPidle)) {
            if (cmd->deviceNum) {
                OPDfuIni(true);
            }

            BooBDebug("ENTER_IAP\r\n");

            blState = IAPidle;
            currentDevice = devicesTable[cmd->deviceNum];
            currentProgrammingDestination = currentDevice.programmingType;
            currentDeviceCanRead  = currentDevice.readWriteFlags & 0x01;
            currentDeviceCanWrite = (currentDevice.readWriteFlags >> 1) & 0x01;
            uint8_t result = 0;
            switch (currentProgrammingDestination) {
            case Self_flash:
                result    = PIOS_BL_HELPER_FLASH_Ini();
                break;
            case Remote_flash_via_spi:
                result    = true;
                break;
            default:
                blState   = Last_operation_failed;
                statusArg = cmd->command;
            }
            if (result != 1) {
                blState   = Last_operation_failed;
                statusArg = cmd->command;
            }
        }
        break;

    case UPLOAD_START:
    {
        bootloader_cmd_UPLOAD_START_t *args = (bootloader_cmd_UPLOAD_START_t *)buf;
        // command is already swapped
        swap32(args->blockCount);
        swap32(args->crc);

        if ((blState == IAPidle) || (blState == uploading)) {
            if (curPacket == 0) {
                BooBDebug("UPLOAD_START\r\n");

                transferType     = args->transferType;
                blockCount       = args->blockCount;
                curPacket        = 1;
                expectedCRC      = args->crc;
                sizeOfLastPacket = args->sizeOfLastPacket;

                if (isBiggerThanAvailable(transferType, (blockCount - 1) * UPLOADING_BLOCK +
                                          sizeOfLastPacket * 4) == true) {
                    blState   = outsideDevCapabilities;
                    statusArg = args->command;
                } else {
                    uint8_t result = 1;
                    if (transferType == FW) {
                        switch (currentProgrammingDestination) {
                        case Self_flash:
                            // This operation is slow, so set the LEDs to a known state
                            PIOS_LED_On(PIOS_LED_HEARTBEAT);
                            result = PIOS_BL_HELPER_FLASH_Start();
                            break;
                        case Remote_flash_via_spi:
                            result = false;
                            break;
                        default:
                            break;
                        }
                    }
                    if (result != 1) {
                        blState   = Last_operation_failed;
                        statusArg = args->command;
                    } else {
                        blState = uploading;
                    }
                }
            } else {
                blState   = Last_operation_failed;
                statusArg = args->command;

                BooBDebug("UPLOAD_START failed\r\n");
            }
        }
        break;
    }

    case UPLOADING:
    {
        bootloader_cmd_UPLOADING_t *args = (bootloader_cmd_UPLOADING_t *)buf;
        // command is already swapped
        swap32(args->blockCount);

        if ((blState == IAPidle) || (blState == uploading)) {
            if (curPacket != 0) {
                if (args->blockCount > blockCount) {
                    blState   = too_many_packets;
                    statusArg = args->blockCount;

                    BooBDebug("UPLOADING too_many_packets\r\n");
                } else if (args->blockCount == curPacket - 1) {
                    uint8_t numberOfWords = 14;
                    if (args->blockCount == blockCount - 1) { // is this the last packet?
                        numberOfWords = sizeOfLastPacket;
                    }
                    uint8_t result = 0;
                    uint32_t aux;;
                    switch (currentProgrammingDestination) {
                    case Self_flash:
                        for (uint8_t x = 0; x < numberOfWords; ++x) {
                            swap32(args->data[x]);
                            aux    = baseOfAdressType(transferType) + (uint32_t)(
                                args->blockCount * UPLOADING_BLOCK + x * 4);
                            result = 0;
                            for (int retry = 0; retry < MAX_WRI_RETRYS; ++retry) {
                                if (result == 0) {
                                    PIOS_BL_HELPER_FLASH_Clear_ErrorBits();

                                    result = (FLASH_ProgramWord(aux, args->data[x])
                                              == FLASH_COMPLETE) ? 1 : 0;
                                } else {
                                    break;
                                }
                            }
                        }
                        break;
                    case Remote_flash_via_spi:
                        result = false; // No support for this for the OPLink Mini
                        break;
                    default:
                        result = 0;
                        break;
                    }
                    if (result != 1) {
                        statusArg = args->command;

                        BooBAck(args->blockCount, curPacket, Last_operation_failed);
                        BooBDebug("UPLOADING Last_operation_failed\r\n");
                    } else {
                        BooBAck(args->blockCount, curPacket, blState);

                        ++curPacket;
                    }
                } else {
                    statusArg = args->blockCount;
                    BooBAck(args->blockCount, curPacket, wrong_packet_received);
                    BooBDebug("wrong_packet_received %d %d\r\n", args->blockCount, curPacket);
                }
            } else {
                blState   = Last_operation_failed;
                statusArg = args->command;

                BooBDebug("UPLOADING not in IAPidle\r\n");
            }
        }
        break;
    }

    case REQ_CAPABILITY:
    {
        bootloader_capability_t *caps = (bootloader_capability_t *)SendBuffer;

        BooBDebug("REQ_CAPABILITY\r\n");

        OPDfuIni(true);

        caps->type = 0x01;
        caps->deviceNum = cmd->deviceNum;

        if (cmd->deviceNum == 0) {
            caps->sizeOfCode = 0;
            caps->u.perms.numberOfDevices = numberOfDevices;
            uint16_t WRFlags = 0;
            for (int x = 0; x < numberOfDevices; ++x) {
                WRFlags = ((devicesTable[x].readWriteFlags << (x * 2))
                           | WRFlags);
            }
            caps->u.perms.WRFlags = WRFlags;
            swap16(caps->u.perms.WRFlags);
        } else {
            caps->sizeOfCode = devicesTable[cmd->deviceNum - 1].sizeOfCode;
            swap32(caps->sizeOfCode);
            caps->u.identity.BL_Version = devicesTable[cmd->deviceNum - 1].BL_Version;
            caps->u.identity.sizeOfDescription = devicesTable[cmd->deviceNum - 1].sizeOfDescription;
            caps->u.identity.FW_Crc     = devicesTable[cmd->deviceNum - 1].FW_Crc;
            swap32(caps->u.identity.FW_Crc);
            caps->u.identity.devID = devicesTable[cmd->deviceNum - 1].devID;
            swap16(caps->u.identity.devID);
        }

        sendData((uint8_t *)caps, 63);
        break;
    }

    case JUMP_TO_FW:
    {
        bootloader_cmd_JUMP_TO_FW_t *args = (bootloader_cmd_JUMP_TO_FW_t *)buf;
        // command is already swapped
        swap32(args->mode);
        swap32(args->args[0]);
        swap32(args->args[1]);
        swap32(args->args[2]);

        if (args->mode == 0x5AFE) {
            /* Force board into safe mode */
            PIOS_IAP_WriteBootCount(0xFFFF);
        }
        // pass any Opt value to the firmware
        PIOS_IAP_WriteBootCmd(0, args->args[0]);
        PIOS_IAP_WriteBootCmd(1, args->args[1]);
        PIOS_IAP_WriteBootCmd(2, args->args[2]);

        FLASH_Lock();
        // Jump straight into the application
        jump_to_app();
        break;
    }

    case RESET:
        PIOS_SYS_Reset();
        break;
    case ABORT_OPERATION:
        curPacket = 0;
        blState   = IAPidle;
        break;

    case OP_END:
        if (blState == uploading) {
            if (curPacket - 1 == blockCount) {
                curPacket = 0;
                if ((transferType != FW) || (expectedCRC == CalcFirmCRC())) {
                    blState = Last_operation_Success;
                } else {
                    blState = CRC_Fail;
                }
            }
            if (curPacket - 1 < blockCount) {
                curPacket = 0;
                blState   = too_few_packets;
            }
            BooBDebug("expectedCRC: 0x%X Actual: 0x%X\r\n", expectedCRC, CalcFirmCRC());
        }
        break;

    case DOWNLOAD_REQ:
    {
        bootloader_cmd_UPLOAD_START_t *args = (bootloader_cmd_UPLOAD_START_t *)buf;

        if (blState == IAPidle) {
            downType = args->transferType;
            downPacketTotal = args->blockCount;
            downSizeOfLastPacket = args->sizeOfLastPacket;
            if (isBiggerThanAvailable(downType, (downPacketTotal - 1) * 14
                                      + downSizeOfLastPacket) == 1) {
                blState   = outsideDevCapabilities;
                statusArg = args->command;
            } else {
                downPacketCurrent = 0;
                blState = downloading;
            }
        } else {
            blState   = Last_operation_failed;
            statusArg = args->command;
        }
        break;
    }

    case REQ_STATUS:
    {
        bootloader_status_t *status = (bootloader_status_t *)SendBuffer;

        BooBDebug("Status Request\r\n");

        status->flag    = 0x01;
        status->blState = blState;
        status->arg     = statusArg;
        swap32(status->arg);
        if (blState == wrong_packet_received) {
            BooBDebug("wrong_packet_received\r\n");
        }

        sendData((uint8_t *)status, 63);
        if (blState == Last_operation_Success) {
            blState = IAPidle;
        }
        break;
    }

    default:
        break;
    }
}

void OPDfuIni(uint8_t discover)
{
    const struct pios_board_info *bdinfo = &pios_board_info_blob;
    Device dev;

    dev.programmingType   = Self_flash;
    dev.readWriteFlags    = (BOARD_READABLE | (BOARD_WRITABLE << 1));
    dev.startOfUserCode   = bdinfo->fw_base;
    dev.sizeOfCode = bdinfo->fw_size;
    dev.sizeOfDescription = bdinfo->desc_size;
    dev.BL_Version  = bdinfo->bl_rev;
    dev.FW_Crc      = CalcFirmCRC();
    dev.devID = (bdinfo->board_type << 8) | (bdinfo->board_rev);
    dev.devType     = bdinfo->hw_type;
    numberOfDevices = 1;
    devicesTable[0] = dev;
    if (discover) {
        // TODO check other devices trough spi or whatever
    }
}
uint32_t baseOfAdressType(DFUTransfer type)
{
    switch (type) {
    case FW:
        return currentDevice.startOfUserCode;

        break;
    case Descript:
        return currentDevice.startOfUserCode + currentDevice.sizeOfCode;

        break;
    default:

        return 0;
    }
}
uint8_t isBiggerThanAvailable(DFUTransfer type, uint32_t size)
{
    switch (type) {
    case FW:
        return (size > currentDevice.sizeOfCode) ? 1 : 0;

        break;
    case Descript:
        return (size > currentDevice.sizeOfDescription) ? 1 : 0;

        break;
    default:
        return true;
    }
}

uint32_t CalcFirmCRC()
{
    switch (currentProgrammingDestination) {
    case Self_flash:
        return PIOS_BL_HELPER_CRC_Memory_Calc();

        break;
    case Remote_flash_via_spi:
        return 0;

        break;
    default:
        return 0;

        break;
    }
}

static uint8_t *last_buf;
static uint16_t last_size;

void sendData(uint8_t *buf, uint16_t size)
{
    last_buf  = buf;
    last_size = size;

    while (platformSendData(buf, size) < STATUS_OK) {
        ;
    }
}

void resendData()
{
    platformSendData(last_buf, last_size);
}

bool flashRead(uint8_t *buffer, uint32_t adr, DFUProgType type)
{
    switch (type) {
    case Remote_flash_via_spi:
        return false; // We should not get this for the OPLink Mini

        break;
    case Self_flash:
        for (uint8_t x = 0; x < 4; ++x) {
            buffer[x] = *PIOS_BL_HELPER_FLASH_If_Read(adr + x);
        }
        return true;

        break;
    default:
        return false;
    }
}

static void BooBDebug(__attribute__((unused)) const char *format, ...)
{
#ifdef PIOS_INCLUDE_BOOB_DEBUG_OUTPUT
    va_list args;
    char buffer[63];

    memset(buffer, 0, sizeof(buffer));
    va_start(args, format);
    vsnprintf(buffer, sizeof(buffer), format, args);
    va_end(args);

    platform_sendDebugData((uint8_t *)buffer, sizeof(buffer));
#endif /* PIOS_INCLUDE_BOOB_DEBUG_OUTPUT */
}

static void BooBAck(uint32_t count, uint32_t expectCount, DFUStates state)
{
    // This is static in case it needs to be resent
    typedef struct {
        uint32_t count;
        uint32_t expectCount;
        uint8_t  state;
    } __attribute__((__packed__)) ack_t;
    ack_t *ack = (ack_t *)SendBuffer;

    ack->count = count;
    ack->expectCount = expectCount;
    ack->state = state;

    swap32(ack->count);
    swap32(ack->expectCount);

    sendData((uint8_t *)ack, sizeof(ack_t));
}
