/**
 ******************************************************************************
 * @addtogroup OpenPilotModules OpenPilot Modules
 * @{
 * @addtogroup PID Library
 * @brief Thrust control callback pure virtual
 * @{
 *
 * @file       pidcontroldowncallback.h
 * @author     Alex Beck Copyright 2015
 * @brief      Interface class for PathFollower FSMs
 *
 *****************************************************************************/
#ifndef PIDCONTROLDOWNCALLBACK_H
#define PIDCONTROLDOWNCALLBACK_H

class PIDControlDownCallback {
public:
    // PIDControlDownCalback() {};
    virtual void BoundThrust(__attribute__((unused)) float &ulow, __attribute__((unused)) float &uhigh) = 0;
    virtual float BoundVelocityDown(float velocity) = 0;
    // virtual ~PIDControlDownCalback() = 0;
};

#endif // PIDCONTROLDOWNCALLBACK_H
