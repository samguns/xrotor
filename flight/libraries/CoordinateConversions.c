/**
 ******************************************************************************
 *
 * @file       CoordinateConversions.c
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      General conversions with different coordinate systems.
 *             - all angles in deg
 *             - distances in meters
 *             - altitude above WGS-84 elipsoid
 *
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <math.h>
#include <stdint.h>
#include <pios_math.h>
#include "CoordinateConversions.h"


// ****** convert Lat,Lon,Alt to ECEF  ************
void LLA2ECEF(int32_t LLAi[3], double ECEF[3])
{
    static const double a = 6378137.0d; // Equatorial Radius
    static const double e = 8.1819190842622e-2d; // Eccentricity
    const double e2 = e * e; // Eccentricity squared
    double sinLat, sinLon, cosLat, cosLon;
    double N;
    double LLA[3]   = {
        (double)LLAi[0] * 1e-7d,
        (double)LLAi[1] * 1e-7d,
        (double)LLAi[2] * 1e-4d
    };

    sinLat = sin(DEG2RAD_D(LLA[0]));
    sinLon = sin(DEG2RAD_D(LLA[1]));
    cosLat = cos(DEG2RAD_D(LLA[0]));
    cosLon = cos(DEG2RAD_D(LLA[1]));

    N = a / sqrt(1.0d - e2 * sinLat * sinLat); // prime vertical radius of curvature

    ECEF[0] = (N + LLA[2]) * cosLat * cosLon;
    ECEF[1] = (N + LLA[2]) * cosLat * sinLon;
    ECEF[2] = ((1.0d - e2) * N + LLA[2]) * sinLat;
}

// ****** convert ECEF to Lat,Lon,Alt (ITERATIVE!) *********
uint16_t ECEF2LLA(double ECEF[3], float LLA[3])
{
    /**
     * LLA parameter is used to prime the iteration.
     * A position within 1 meter of the specified LLA
     * will be calculated within at most 3 iterations.
     * If unknown: Call with any valid LLA coordinate
     * will compute within at most 5 iterations.
     * Suggestion: [0,0,0]
     **/

    static const double a = 6378137.0f; // Equatorial Radius
    static const double e = 8.1819190842622e-2f; // Eccentricity
    double x = ECEF[0], y = ECEF[1], z = ECEF[2];
    double Lat, N, NplusH, delta, esLat;
    uint16_t iter;

#define MAX_ITER 10 // should not take more than 5 for valid coordinates
#define ACCURACY 1.0e-11d // used to be e-14, but we don't need sub micrometer exact calculations

    LLA[1] = (float)RAD2DEG_D(atan2(y, x));
    Lat    = DEG2RAD_D((double)LLA[0]);
    esLat  = e * sin(Lat);
    N = a / sqrt(1 - esLat * esLat);
    NplusH = N + (double)LLA[2];
    delta  = 1;
    iter   = 0;

    while (((delta > ACCURACY) || (delta < -ACCURACY))
           && (iter < MAX_ITER)) {
        delta  = Lat - atan(z / (sqrt(x * x + y * y) * (1 - (N * e * e / NplusH))));
        Lat    = Lat - delta;
        esLat  = e * sin(Lat);
        N      = a / sqrt(1 - esLat * esLat);
        NplusH = sqrt(x * x + y * y) / cos(Lat);
        iter  += 1;
    }

    LLA[0] = RAD2DEG_D(Lat);
    LLA[2] = NplusH - N;

    return iter < MAX_ITER;
}

// ****** find ECEF to NED rotation matrix ********
void RneFromLLA(int32_t LLAi[3], float Rne[3][3])
{
    float sinLat, sinLon, cosLat, cosLon;

    sinLat    = sinf(DEG2RAD((float)LLAi[0] * 1e-7f));
    sinLon    = sinf(DEG2RAD((float)LLAi[1] * 1e-7f));
    cosLat    = cosf(DEG2RAD((float)LLAi[0] * 1e-7f));
    cosLon    = cosf(DEG2RAD((float)LLAi[1] * 1e-7f));

    Rne[0][0] = -sinLat * cosLon;
    Rne[0][1] = -sinLat * sinLon;
    Rne[0][2] = cosLat;
    Rne[1][0] = -sinLon;
    Rne[1][1] = cosLon;
    Rne[1][2] = 0;
    Rne[2][0] = -cosLat * cosLon;
    Rne[2][1] = -cosLat * sinLon;
    Rne[2][2] = -sinLat;
}


// ****** Express LLA in a local NED Base Frame ********
void LLA2Base(int32_t LLAi[3], double BaseECEF[3], float Rne[3][3], float NED[3])
{
    double ECEF[3];
    float diff[3];

    LLA2ECEF(LLAi, ECEF);

    diff[0] = (float)(ECEF[0] - BaseECEF[0]);
    diff[1] = (float)(ECEF[1] - BaseECEF[1]);
    diff[2] = (float)(ECEF[2] - BaseECEF[2]);

    NED[0]  = Rne[0][0] * diff[0] + Rne[0][1] * diff[1] + Rne[0][2] * diff[2];
    NED[1]  = Rne[1][0] * diff[0] + Rne[1][1] * diff[1] + Rne[1][2] * diff[2];
    NED[2]  = Rne[2][0] * diff[0] + Rne[2][1] * diff[1] + Rne[2][2] * diff[2];
}

// ****** Express ECEF in a local NED Base Frame ********
void ECEF2Base(double ECEF[3], double BaseECEF[3], float Rne[3][3], float NED[3])
{
    float diff[3];

    diff[0] = (float)(ECEF[0] - BaseECEF[0]);
    diff[1] = (float)(ECEF[1] - BaseECEF[1]);
    diff[2] = (float)(ECEF[2] - BaseECEF[2]);

    NED[0]  = Rne[0][0] * diff[0] + Rne[0][1] * diff[1] + Rne[0][2] * diff[2];
    NED[1]  = Rne[1][0] * diff[0] + Rne[1][1] * diff[1] + Rne[1][2] * diff[2];
    NED[2]  = Rne[2][0] * diff[0] + Rne[2][1] * diff[1] + Rne[2][2] * diff[2];
}
