/**
 ******************************************************************************
 * @addtogroup CopterControlBL CopterControl BootLoader
 * @brief These files contain the code to the CopterControl Bootloader.
 *
 * @{
 * @file       common.c
 * @author     The OPNG Team, http://www.opng.org Copyright (C) 2015.
 * @author     The OpenPilot Team, http://www.openpilot.org Copyright (C) 2010.
 * @brief      This file contains various common defines for the BootLoader
 * @see        The GNU Public License (GPL) Version 3
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
#ifndef BL_COMMON_H_
#define BL_COMMON_H_

// #include "board.h"

// Note that these structures contain little endian values, and this code is big endian
#define swap16(x) x = (((x) >> 8) | ((x) << 8))
#define swap32(x) x = (((x) >> 24) | (((x) & 0x00ff0000) >> 8) | (((x) & 0x0000ff00) << 8) | ((x) << 24))

#pragma pack(push)
#pragma pack(1)
typedef struct {
    uint32_t command;
    uint32_t blockCount;
    uint8_t  deviceNum;
} bootloader_cmd_t;

typedef struct {
    uint32_t command;
    uint32_t blockCount;
    uint8_t  transferType;
    uint8_t  sizeOfLastPacket;
    uint32_t crc;
} bootloader_cmd_UPLOAD_START_t;

#define UPLOADING_BLOCK 56
typedef struct {
    uint32_t command;
    uint32_t blockCount;
    uint32_t data[UPLOADING_BLOCK];
} bootloader_cmd_UPLOADING_t;

typedef struct {
    uint32_t command;
    uint32_t blockCount;
    uint32_t mode;
    uint32_t args[3];
} bootloader_cmd_JUMP_TO_FW_t;

typedef struct {
    uint8_t  type;
    uint32_t sizeOfCode;
    uint8_t  deviceNum;
    union {
        struct {
            uint8_t  numberOfDevices;
            uint16_t WRFlags;
        } perms;
        struct {
            uint8_t  BL_Version;
            uint8_t  sizeOfDescription;
            uint8_t  unused;
            uint32_t FW_Crc;
            uint16_t devID;
        } identity;
    } u;
} bootloader_capability_t;

typedef struct {
    uint8_t  flag;
    uint32_t arg;
    uint8_t  blState;
} bootloader_status_t;
#pragma pack(pop)

typedef enum {
    start, keepgoing,
} DownloadAction;

/**************************************************/
/* OP_DFU states                       */
/**************************************************/

typedef enum {
    IAPidle, // 0
    uploading, // 1
    wrong_packet_received, // 2
    too_many_packets, // 3
    too_few_packets, // 4
    Last_operation_Success, // 5
    downloading, // 6
    BLidle, // 7
    Last_operation_failed, // 8
    uploadingStarting, // 9
    outsideDevCapabilities, // 10
    CRC_Fail, // 11
    failed_jump,
// 12
} DFUStates;

typedef enum {
    High_Density, Medium_Density
} DeviceType;
/**************************************************/
/* OP_DFU transfer types                       */
/**************************************************/
typedef enum {
    FW, // 0
    Descript
// 2
} DFUTransfer;
/**************************************************/
/* OP_DFU transfer port                           */
/**************************************************/
typedef enum {
    Usb, // 0
    Serial
// 2
} DFUPort;
/**************************************************/
/* OP_DFU programable programable HW types        */
/**************************************************/
typedef enum {
    Self_flash, // 0
    Remote_flash_via_spi
// 1
} DFUProgType;
/**************************************************/
/* OP_DFU programable sources			          */
/**************************************************/
#define USB             0
#define SPI             1

#define DownloadDelay   100000

#define MAX_DEL_RETRYS  3
#define MAX_WRI_RETRYS  3

/**************************************************/
/* OPNG BooB commands                             */
/**************************************************/
#define BOOB_START      0xB00B0000
#define REQ_CAPABILITY  0xB00B0001
#define RES_CAPABILITY  0xB00B0002
#define ENTER_IAP       0xB00B0003
#define JUMP_TO_FW      0xB00B0004
#define RESET           0xB00B0005
#define ABORT_OPERATION 0xB00B0006
#define UPLOAD_START    0xB00B0107
#define UPLOADING       0xB00B0007
#define OP_END          0xB00B0008
#define DOWNLOAD_REQ    0xB00B0009
#define DOWNLOADING     0xB00B000a
#define REQ_STATUS      0xB00B000b

#endif /* BL_COMMON_H_ */
