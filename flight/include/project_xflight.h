/*
 *    Project XFlight project header file
 *
 *    Copyright (C) 2016 NextGenRotors.
 *
 */

#ifndef PROJECT_XFLIGHT_H
#define PROJECT_XFLIGHT_H

// Include common system files as needed
#include <stdint.h>

// This is the code instrumentation plugin
// #include "maestra.h"

// Include firmware target specific header
// #include "target_xflight.h"

// Include common project files as needed
#if defined(__cplusplus)
extern "C" {
#endif
// operating system abstraction
#include "pios.h"

// common utilities
#include "mathmisc.h"

// uavo interface
#include "uavobjectmanager.h"
#include "eventdispatcher.h"
#include "alarms.h"

#if defined(__cplusplus)
}
#endif

#endif // PROJECT_XFLIGHT_H
