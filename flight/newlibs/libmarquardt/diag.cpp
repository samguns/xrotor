//
// File: diag.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "diag.h"

// Function Definitions

//
// Arguments    : const float v_data[]
// const int v_size[2]
// float d_data[]
// int d_size[1]
// Return Type  : void
//
void diag(const float v_data[], const int v_size[2], float d_data[], int d_size
          [1])
{
    int dlen;
    int j;

    if (v_size[0] <= v_size[1]) {
        dlen = v_size[0];
    } else {
        dlen = v_size[1];
    }

    d_size[0] = (signed char)dlen;
    for (j = 0; j + 1 <= dlen; j++) {
        d_data[j] = v_data[j * (v_size[0] + 1)];
    }
}

//
// File trailer for diag.cpp
//
// [EOF]
//
