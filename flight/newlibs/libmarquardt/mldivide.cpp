//
// File: mldivide.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "libmarquardt.h"
#include "rt_nonfinite.h"
#include "mldivide.h"
#include "xzgetrf.h"
#include "qrsolve.h"
#include "xgeqp3.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : const float A_data[]
// const int A_size[2]
// const float B_data[]
// const int B_size[2]
// float Y_data[]
// int Y_size[2]
// Return Type  : void
//
void b_mldivide(const float A_data[], const int A_size[2],
                const float B_data[], const int B_size[2],
                float Y_data[], int Y_size[2])
{
    float *b_A_data = 0;
    float *b_B_data = 0;
    int jBcol;
    int mn;
    int n;
    int b_A_size[2];
    int jpvt_size[2];
    int jpvt_data[MARQUARDT_MAX_NUMOBS];
    int rankR;
    int m;
    float wj;
    int j;
    int k;
    int i;
    int tau_size[1];
    float tau_data[MARQUARDT_MAX_NUMOBS];

    if ((A_size[0] == 0) || (A_size[1] == 0) || (B_size[0] == 0)) {
        Y_size[0] = (signed char)A_size[1];
        Y_size[1] = (signed char)B_size[1];
        jBcol     = (signed char)A_size[1] * (signed char)B_size[1];
        for (mn = 0; mn < jBcol; mn++) {
            Y_data[mn] = 0.0F;
        }
    } else if (A_size[0] == A_size[1]) {
        n           = A_size[1];
        b_A_size[0] = A_size[0];
        b_A_size[1] = A_size[1];
        jBcol       = A_size[0] * A_size[1];
        b_A_data    = (float *)pios_malloc(jBcol * sizeof(float));
        for (mn = 0; mn < jBcol; mn++) {
            b_A_data[mn] = A_data[mn];
        }

        xzgetrf(A_size[1], A_size[1], b_A_data, b_A_size, A_size[1], jpvt_data, jpvt_size, &jBcol);
        Y_size[0] = B_size[0];
        Y_size[1] = B_size[1];
        jBcol     = B_size[0] * B_size[1];
        for (mn = 0; mn < jBcol; mn++) {
            Y_data[mn] = B_data[mn];
        }

        rankR = B_size[1];
        for (jBcol = 0; jBcol + 1 < n; jBcol++) {
            if (jpvt_data[jBcol] != jBcol + 1) {
                mn = jpvt_data[jBcol] - 1;
                for (m = 0; m + 1 <= rankR; m++) {
                    wj = Y_data[jBcol + Y_size[0] * m];
                    Y_data[jBcol + Y_size[0] * m] = Y_data[mn + Y_size[0] * m];
                    Y_data[mn + Y_size[0] * m] = wj;
                }
            }
        }

        for (j = 1; j <= rankR; j++) {
            jBcol = n * (j - 1);
            for (k = 0; k + 1 <= n; k++) {
                mn = n * k;
                if (!is_zero(Y_data[k + jBcol])) {
                    for (i = k + 1; i + 1 <= n; i++) {
                        Y_data[i + jBcol] -= Y_data[k + jBcol] * b_A_data[i + mn];
                    }
                }
            }
        }

        for (j = 1; j <= rankR; j++) {
            jBcol = n * (j - 1);
            for (k = n - 1; k + 1 > 0; k--) {
                mn = n * k;
                if (!is_zero(Y_data[k + jBcol])) {
                    Y_data[k + jBcol] /= b_A_data[k + mn];
                    for (i = 0; i + 1 <= k; i++) {
                        Y_data[i + jBcol] -= Y_data[k + jBcol] * b_A_data[i + mn];
                    }
                }
            }
        }
    } else {
        b_A_size[0] = A_size[0];
        b_A_size[1] = A_size[1];
        jBcol = A_size[0] * A_size[1];
        b_A_data    = (float *)pios_malloc(jBcol * sizeof(float));
        for (mn = 0; mn < jBcol; mn++) {
            b_A_data[mn] = A_data[mn];
        }

        xgeqp3(b_A_data, b_A_size, tau_data, tau_size, jpvt_data, jpvt_size);
        rankR     = rankFromQR(b_A_data, b_A_size);
        Y_size[0] = (signed char)b_A_size[1];
        Y_size[1] = (signed char)B_size[1];
        jBcol     = (signed char)b_A_size[1] * (signed char)B_size[1];
        for (mn = 0; mn < jBcol; mn++) {
            Y_data[mn] = 0.0F;
        }

        n        = B_size[0];
        jBcol    = B_size[0] * B_size[1];
        b_B_data = (float *)pios_malloc(jBcol * sizeof(float));
        for (mn = 0; mn < jBcol; mn++) {
            b_B_data[mn] = B_data[mn];
        }

        m     = b_A_size[0];
        jBcol = b_A_size[0];
        mn    = b_A_size[1];
        if (jBcol <= mn) {
            mn = jBcol;
        }

        for (j = 0; j + 1 <= mn; j++) {
            if (!is_zero(tau_data[j])) {
                for (k = 0; k + 1 <= B_size[1]; k++) {
                    wj = b_B_data[j + n * k];
                    for (i = j + 1; i + 1 <= m; i++) {
                        wj += b_A_data[i + b_A_size[0] * j] * b_B_data[i + n * k];
                    }

                    wj *= tau_data[j];
                    if (!is_zero(wj)) {
                        b_B_data[j + n * k] -= wj;
                        for (i = j + 1; i + 1 <= m; i++) {
                            b_B_data[i + n * k] -= b_A_data[i + b_A_size[0] * j] * wj;
                        }
                    }
                }
            }
        }

        for (k = 0; k + 1 <= B_size[1]; k++) {
            for (i = 0; i + 1 <= rankR; i++) {
                Y_data[(jpvt_data[i] + Y_size[0] * k) - 1] = b_B_data[i + n * k];
            }

            for (j = rankR - 1; j + 1 > 0; j--) {
                Y_data[(jpvt_data[j] + Y_size[0] * k) - 1] /= b_A_data[j + b_A_size[0] *
                                                                       j];
                for (i = 0; i + 1 <= j; i++) {
                    Y_data[(jpvt_data[i] + Y_size[0] * k) - 1] -= Y_data[(jpvt_data[j] +
                                                                          Y_size[0] * k) - 1] * b_A_data[i + b_A_size[0] * j];
                }
            }
        }
    }
    pios_free(b_A_data); b_A_data = 0;
    pios_free(b_B_data); b_B_data = 0;
}

//
// Arguments    : const float A_data[]
// const int A_size[2]
// const float B_data[]
// const int B_size[2]
// float Y_data[]
// int Y_size[2]
// Return Type  : void
//
void mldivide(const float A_data[], const int A_size[2],
              const float B_data[], const int B_size[2],
              float Y_data[], int Y_size[2])
{
    float *b_A_data = 0;
    float *b_B_data = 0;
    int b_A_size[2];
    int mn;
    int jBcol;
    int jpvt_size[2];
    int jpvt_data[MARQUARDT_MAX_NUMOBS];
    // int nb;
    int rankR;
    float wj;
    int j;
    int k;
    int i;
    int tau_size[1];
    float tau_data[MARQUARDT_MAX_NUMOBS];

    if (A_size[1] == 0) {
        Y_size[0] = 0;
        Y_size[1] = (signed char)B_size[1];
    }
#if 0
    else if (12 == A_size[1]) {
        b_A_size[0] = A_size[0];
        b_A_size[1] = 12;
        mn = A_size[0] * 12;
        for (jBcol = 0; jBcol < mn; jBcol++) {
            b_A_data[jBcol] = A_data[jBcol];
        }

        xzgetrf(12, 12, b_A_data, b_A_size, 12, jpvt_data, jpvt_size, &mn);
        mn = B_size[0] * B_size[1];
        for (jBcol = 0; jBcol < mn; jBcol++) {
            b_B_data[jBcol] = B_data[jBcol];
        }

        nb = B_size[1];
        for (mn = 0; mn + 1 < 12; mn++) {
            if (jpvt_data[mn] != mn + 1) {
                jBcol = jpvt_data[mn] - 1;
                for (rankR = 0; rankR + 1 <= nb; rankR++) {
                    wj = b_B_data[mn + 12 * rankR];
                    b_B_data[mn + 12 * rankR] = b_B_data[jBcol + 12 * rankR];
                    b_B_data[jBcol + 12 * rankR] = wj;
                }
            }
        }

        for (j = 1; j <= nb; j++) {
            jBcol = 12 * (j - 1);
            for (k = 0; k + 1 < 13; k++) {
                rankR = 12 * k;
                if (!is_zero(b_B_data[k + jBcol])) {
                    for (i = k + 1; i + 1 < 13; i++) {
                        b_B_data[i + jBcol] -= b_B_data[k + jBcol] * b_A_data[i + rankR];
                    }
                }
            }
        }

        for (j = 1; j <= nb; j++) {
            jBcol = 12 * (j - 1);
            for (k = 11; k + 1 > 0; k--) {
                rankR = 12 * k;
                if (!is_zero(b_B_data[k + jBcol])) {
                    b_B_data[k + jBcol] /= b_A_data[k + rankR];
                    for (i = 0; i + 1 <= k; i++) {
                        b_B_data[i + jBcol] -= b_B_data[k + jBcol] * b_A_data[i + rankR];
                    }
                }
            }
        }

        Y_size[0] = 12;
        Y_size[1] = B_size[1];
        mn = 12 * B_size[1];
        for (jBcol = 0; jBcol < mn; jBcol++) {
            Y_data[jBcol] = b_B_data[jBcol];
        }
    }
#endif // if 0
    else {
        b_A_size[0] = A_size[0];
        b_A_size[1] = A_size[1];
        mn = A_size[0] * A_size[1];
        b_A_data    = (float *)pios_malloc(mn * sizeof(float));
        for (jBcol = 0; jBcol < mn; jBcol++) {
            b_A_data[jBcol] = A_data[jBcol];
        }

        xgeqp3(b_A_data, b_A_size, tau_data, tau_size, jpvt_data, jpvt_size);
        rankR     = rankFromQR(b_A_data, b_A_size);
        Y_size[0] = (signed char)b_A_size[1];
        Y_size[1] = (signed char)B_size[1];
        mn = (signed char)b_A_size[1] * (signed char)B_size[1];
        for (jBcol = 0; jBcol < mn; jBcol++) {
            Y_data[jBcol] = 0.0F;
        }

        mn = B_size[0] * B_size[1];
        b_B_data = (float *)pios_malloc(mn * sizeof(float));
        for (jBcol = 0; jBcol < mn; jBcol++) {
            b_B_data[jBcol] = B_data[jBcol];
        }

        mn = b_A_size[1];
        if (MARQUARDT_MAX_UNKNOWNS <= mn) {
            mn = MARQUARDT_MAX_UNKNOWNS;
        }

        for (j = 0; j + 1 <= mn; j++) {
            if (!is_zero(tau_data[j])) {
                for (k = 0; k + 1 <= B_size[1]; k++) {
                    wj = b_B_data[j + MARQUARDT_MAX_UNKNOWNS * k];
                    for (i = j + 1; i < MARQUARDT_MAX_UNKNOWNS; i++) {
                        wj += b_A_data[i + b_A_size[0] * j] * b_B_data[i + MARQUARDT_MAX_UNKNOWNS * k];
                    }

                    wj *= tau_data[j];
                    if (!is_zero(wj)) {
                        b_B_data[j + MARQUARDT_MAX_UNKNOWNS * k] -= wj;
                        for (i = j + 1; i < MARQUARDT_MAX_UNKNOWNS; i++) {
                            b_B_data[i + MARQUARDT_MAX_UNKNOWNS * k] -= b_A_data[i + b_A_size[0] * j] * wj;
                        }
                    }
                }
            }
        }


        for (k = 0; k + 1 <= B_size[1]; k++) {
            for (i = 0; i + 1 <= rankR; i++) {
                Y_data[(jpvt_data[i] + Y_size[0] * k) - 1] = b_B_data[i + MARQUARDT_MAX_UNKNOWNS * k];
            }

            for (j = rankR - 1; j + 1 > 0; j--) {
                Y_data[(jpvt_data[j] + Y_size[0] * k) - 1] /= b_A_data[j + b_A_size[0] * j];
                for (i = 0; i + 1 <= j; i++) {
                    Y_data[(jpvt_data[i] + Y_size[0] * k) - 1] -= Y_data[(jpvt_data[j] + Y_size[0] * k) - 1] * b_A_data[i + b_A_size[0] * j];
                }
            }
        }


        pios_free(b_A_data); b_A_data = 0;
        pios_free(b_B_data); b_B_data = 0;
    }
}

//
// File trailer for mldivide.cpp
//
// [EOF]
//
