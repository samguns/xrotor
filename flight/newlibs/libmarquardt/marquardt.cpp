//
// File: autocalibrationAsymmetric.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "norm.h"
#include "xzgetrf.h"
#include "all.h"
#include "rcond.h"
#include "mldivide.h"
#include "diag.h"
#include "mpower.h"
#include "marquardt.h"
#include "mathmisc.h"


//
// Arguments    : const float x[12]
// const float varargin_2[10]
// const float varargin_3[10]
// const float varargin_4[10]
// float *err
// float *f
// float r[10]
// float J[120]
// Return Type  : void
//
static void checkrJ(MarquartCallback_T callback,
                    const float *x,
                    int32_T numUnknowns,
                    int32_T numObservations,
                    const float *varargin_2, // X
                    const float *varargin_3, // Y
                    const float *varargin_4, // Z
                    const float *Vvariance,
                    int8_T *err,
                    float *f,
                    float *r, // error function
                    float *J) // jacobian
{
    boolean_T b[MARQUARDT_MAX_NUMOBS]; // 14
    int k;
    boolean_T y;
    boolean_T exitg4;
    boolean_T guard1 = false;
    boolean_T guard2 = false;
    boolean_T exitg3;
    float f0;

    int jacobian_size = numUnknowns * numObservations; // 168

    // 'autocalibrationAsymmetric:305' coder.inline('never');
    // CHECKRJ  Check Matlab function which is called by a
    // nonlinear least squares solver.
    // Version 10.09.20.  hbn(a)imm.dtu.dk
    // 'autocalibrationAsymmetric:311' err = 0;
    *err = 0.0F;

    // 'autocalibrationAsymmetric:311' f = NaN;
    *f   = ((real32_T)rtNaNF);

    // 'autocalibrationAsymmetric:311' n = length(x);
    // 'autocalibrationAsymmetric:312' if  nargout > 3
    // Check  r  and  J
    // [r, J] = feval(fun,x,varargin{:});
    // 'autocalibrationAsymmetric:315' [r, J] = accelCalAsymmetricGetRandJacobian(x,varargin{:});
    (*callback)(x, numObservations, varargin_2, varargin_3, varargin_4, Vvariance, r, J);

    // 'autocalibrationAsymmetric:317' sr = size(r);
    // 'autocalibrationAsymmetric:317' sJ = size(J);
    // 'autocalibrationAsymmetric:318' if  sr(2) ~= 1 || ~isreal(r) || any(isnan(r(:))) || any(isinf(r(:)))
    for (k = 0; k < numObservations; k++) { // 14
        b[k] = rtIsNaNF(r[k]) | rtIsInfF(r[k]);
    }

    y = false;
    k = 0;
    exitg4 = false;
    while ((!exitg4) && (k < numObservations)) { // 14
        if (b[k]) {
            y = true;
            exitg4 = true;
        } else {
            k++;
        }
    }

    guard1 = false;
    guard2 = false;
    if (y) {
        guard2 = true;
    } else {
        for (k = 0; k < numObservations; k++) { // 14
            b[k] = rtIsInfF(r[k]);
        }

        y = false;
        k = 0;
        exitg3 = false;
        while ((!exitg3) && (k < numObservations)) { // 14
            if (b[k]) {
                y = true;
                exitg3 = true;
            } else {
                k++;
            }
        }

        if (y) {
            guard2 = true;
        } else {
            // 'autocalibrationAsymmetric:320' if  ~isreal(J) || any(isnan(J(:))) || any(isinf(J(:)))
            for (k = 0; k < jacobian_size; k++) {
                if (rtIsNaNF(J[k]) || rtIsInfF(J[k])) {
                    y = true;
                    break;
                }
            }

            if (y) {
                guard1 = true;
            } else {
                // 'autocalibrationAsymmetric:322' if  sJ(1) ~= sr(1) || sJ(2) ~= n
                // Objective function
                // 'autocalibrationAsymmetric:336' f = (r' * r)/2;
                f0 = 0.0F;
                for (k = 0; k < numObservations; k++) { // 14
                    f0 += r[k] * r[k];
                }

                *f = f0 / 2.0F;

                // 'autocalibrationAsymmetric:337' if  isinf(f)
                if (rtIsInfF(*f)) {
                    // 'autocalibrationAsymmetric:337' err = -5;
                    *err = -5.0F;
                }
            }
        }
    }

    if (guard2) {
        // 'autocalibrationAsymmetric:319' err = -2;
        *err = -2.0F;
    }

    if (guard1) {
        // 'autocalibrationAsymmetric:321' err = -3;
        *err = -3.0F;
    }
}


//
// Arguments    : const float A_data[]
// const int A_size[2]
// const float g_data[]
// const int g_size[2]
// float *mu
// float h_data[]
// int h_size[2]
// Return Type  : void
//
static void geth(const float A_data[], const int A_size[2],
                 float *working_mem[4],
                 const float g_data[], const int g_size[2],
                 float *mu,
                 float h_data[], int h_size[2])
{
    float *y_data = working_mem[0];
    float *R_data = working_mem[1];
    float *I_data = working_mem[2];
    float *c_data = working_mem[3];

    float b_g_data[MARQUARDT_MAX_UNKNOWNS]; // size is same as g_data and g_size

    unsigned char A_idx_0;
    int jmax;
    int ixstart;
    float mtmp;
    int ix;
    boolean_T exitg2;
    float ajj;
    int ia;
    int i2;
    int fcnOutput;
    int chp;
    boolean_T near_singularity;
    signed char sz[2];
    int R_size[2];
    int n;
    int b_n;
    int info;
    int colj;
    int j;
    boolean_T exitg1;
    int iy;
    int nmj;
    int coljp1;
    float c;
    int i3;
    int b_R_size[2];
    int b_g_size[2];
    int c_size[2];

    int iteration = 0;

    // 'autocalibrationAsymmetric:363' coder.inline('never');
    // Solve  (A + mu*I)h = -g  with possible adjustment of  mu
    // Version 10.02.17.  hbn@imm.dtu.dk
    // 'autocalibrationAsymmetric:368' mA = max(abs(A(:)));
    A_idx_0 = (unsigned char)(A_size[0] * A_size[1]);
    for (jmax = 0; jmax + 1 <= A_size[0] * A_size[1]; jmax++) {
        y_data[jmax] = fabsf(A_data[jmax]);
    }

    ixstart = 1;
    mtmp    = y_data[0];
    if (rtIsNaNF(y_data[0])) {
        ix     = 2;
        exitg2 = false;
        while ((!exitg2) && (ix <= A_idx_0)) {
            ixstart = ix;
            if (!rtIsNaNF(y_data[ix - 1])) {
                mtmp   = y_data[ix - 1];
                exitg2 = true;
            } else {
                ix++;
            }
        }
    }

    if (ixstart < A_idx_0) {
        while (ixstart + 1 <= A_idx_0) {
            if (y_data[ixstart] > mtmp) {
                mtmp = y_data[ixstart];
            }

            ixstart++;
        }
    }

    // 'autocalibrationAsymmetric:369' if  mA == 0
    if (is_zero(mtmp)) {
        // zero matrix
        // 'autocalibrationAsymmetric:370' h = g / (-mu);
        h_size[0] = g_size[0];
        h_size[1] = g_size[1];
        ajj = -*mu;
        ia  = g_size[0] * g_size[1];
        for (i2 = 0; i2 < ia; i2++) {
            h_data[i2] = g_data[i2] / ajj;
        }
    } else {
        // 'autocalibrationAsymmetric:373' h = zeros(size(g));
        // Factorize with check of pos. def.
        // 'autocalibrationAsymmetric:375' n = size(A,1);
        fcnOutput = A_size[0];

        // 'autocalibrationAsymmetric:375' chp = 1;
        chp = 1;

        // 'autocalibrationAsymmetric:375' near_singularity = false;
        near_singularity = false;

        // 'autocalibrationAsymmetric:376' sz = size(A);
        for (i2 = 0; i2 < 2; i2++) {
            sz[i2] = (signed char)A_size[i2];
        }

        // 'autocalibrationAsymmetric:377' R = zeros(sz);
        R_size[0] = sz[0];
        R_size[1] = sz[1];
        ia = sz[0] * sz[1];
        for (i2 = 0; i2 < ia; i2++) {
            R_data[i2] = 0.0F;
        }

        // 'autocalibrationAsymmetric:379' while  chp
        while (chp != 0 && iteration++ < 100) {
            // 'autocalibrationAsymmetric:380' [R, chp] = chol(A + mu*eye(n));
            ia = fcnOutput * fcnOutput;
            for (i2 = 0; i2 < ia; i2++) {
                I_data[i2] = 0.0;
            }

            for (jmax = 0; jmax + 1 <= A_size[0]; jmax++) {
                I_data[jmax + fcnOutput * jmax] = 1.0;
            }

            ia = A_size[0] * A_size[1];
            for (i2 = 0; i2 < ia; i2++) {
                c_data[i2] = A_data[i2] + *mu * I_data[i2];
            }

            n  = A_size[1];
            R_size[0] = A_size[0];
            ia = A_size[0] * A_size[1];
            for (i2 = 0; i2 < ia; i2++) {
                R_data[i2] = c_data[i2];
            }

            b_n    = A_size[0];
            info   = 0;
            colj   = 1;
            j      = 0;
            exitg1 = false;
            while ((!exitg1) && (j + 1 <= b_n)) {
                ixstart = (colj + j) - 1;
                ajj     = 0.0F;
                if (j < 1) {} else {
                    ix = colj;
                    iy = colj;
                    for (jmax = 1; jmax <= j; jmax++) {
                        ajj += R_data[ix - 1] * R_data[iy - 1];
                        ix++;
                        iy++;
                    }
                }

                ajj = R_data[ixstart] - ajj;
                if (ajj > 0.0F) {
                    ajj = sqrtf(ajj);
                    R_data[ixstart] = ajj;
                    if (j + 1 < b_n) {
                        nmj      = (b_n - j) - 2;
                        ixstart += b_n;
                        coljp1   = colj + b_n;
                        if ((j == 0) || (nmj + 1 == 0)) {} else {
                            iy = ixstart;
                            i2 = coljp1 + n * nmj;
                            for (jmax = coljp1; jmax <= i2; jmax += n) {
                                ix = colj;
                                c  = 0.0F;
                                i3 = (jmax + j) - 1;
                                for (ia = jmax; ia <= i3; ia++) {
                                    c += R_data[ia - 1] * R_data[ix - 1];
                                    ix++;
                                }

                                R_data[iy] += -c;
                                iy += n;
                            }
                        }

                        ajj = 1.0F / ajj;
                        i2  = (ixstart + b_n * nmj) + 1;
                        for (jmax = ixstart + 1; jmax <= i2; jmax += b_n) {
                            R_data[jmax - 1] *= ajj;
                        }

                        colj = coljp1;
                    }

                    j++;
                } else {
                    R_data[ixstart] = ajj;
                    info = j + 1;
                    exitg1 = true;
                }
            }

            if (info == 0) {
                jmax = A_size[1];
            } else {
                jmax = info - 1;
            }

            for (j = 0; j + 1 <= jmax; j++) {
                for (ixstart = j + 1; ixstart + 1 <= jmax; ixstart++) {
                    R_data[ixstart + R_size[0] * j] = 0.0F;
                }
            }

            if (1 > jmax) {
                ia = 0;
            } else {
                ia = jmax;
            }

            if (1 > jmax) {
                ixstart = 0;
            } else {
                ixstart = jmax;
            }

            for (i2 = 0; i2 < ixstart; i2++) {
                for (i3 = 0; i3 < ia; i3++) {
                    y_data[i3 + ia * i2] = R_data[i3 + R_size[0] * i2];
                }
            }

            R_size[0] = ia;
            R_size[1] = ixstart;
            for (i2 = 0; i2 < ixstart; i2++) {
                for (i3 = 0; i3 < ia; i3++) {
                    R_data[i3 + ia * i2] = y_data[i3 + ia * i2];
                }
            }

            chp = info;

            // 'autocalibrationAsymmetric:381' if  chp == 0
            if (info == 0) {
                // check for near singularity
                // 'autocalibrationAsymmetric:382' near_singularity = rcond(R) < 1e-15;
                ajj = rcond(R_data, R_size);
                near_singularity = (ajj < 1.0E-15F);
            }

            // 'autocalibrationAsymmetric:384' if  near_singularity
            if (near_singularity) {
                // 'autocalibrationAsymmetric:385' mu = max(10*mu, eps*mA);
                *mu = fmaxf(10.0F * *mu, 2.22044605E-16F * mtmp);

                // 'autocalibrationAsymmetric:386' chp = 1;
                chp = 1;
            }
        }

        // 'autocalibrationAsymmetric:390' if chp == 0
        // Solve  (R'*R)h = -g
        // 'autocalibrationAsymmetric:392' h = R \ (R' \ (-g));
        b_R_size[0] = R_size[1];
        b_R_size[1] = R_size[0];
        ia = R_size[0];
        for (i2 = 0; i2 < ia; i2++) {
            ixstart = R_size[1];
            for (i3 = 0; i3 < ixstart; i3++) {
                y_data[i3 + b_R_size[0] * i2] = R_data[i2 + R_size[0] * i3];
            }
        }

        b_g_size[0] = g_size[0];
        b_g_size[1] = g_size[1];
        ia = g_size[0] * g_size[1];
        for (i2 = 0; i2 < ia; i2++) {
            b_g_data[i2] = -g_data[i2];
        }

        mldivide(y_data, b_R_size, b_g_data, b_g_size, c_data, c_size);
        b_mldivide(R_data, R_size, c_data, c_size, h_data, h_size);
    }
}


//
// Arguments    : const float varargin_2[14]
// const float varargin_3[14]
// const float varargin_4[14]
// const float varargin_5[14]
// float X[12]
// float info[7]
// Return Type  : void
//
extern void marquardt(MarquartCallback_T callback,
                      const float *x0,
                      int32_T numUnknowns,
                      int32_T numObservations,
                      const float *Vx,
                      const float *Vy,
                      const float *Vz,
                      const float *Vvariance,
                      float *X,
                      float info[7])
{
    int ixstart;

    float *A_data = 0;
    float *J = 0;
    float *h_data;
    float *a1     = 0;
    float *working_mem[4];

    float r[MARQUARDT_MAX_NUMOBS];
    float f;
    int8_T stop;
    int A_size[2];
    int g_size[2];
    int n;
    float g_data[MARQUARDT_MAX_UNKNOWNS];
    float b_a[MARQUARDT_MAX_UNKNOWNS];
    float c_a[MARQUARDT_MAX_UNKNOWNS];
    float a_data[MARQUARDT_MAX_UNKNOWNS];
    float c_data[MARQUARDT_MAX_UNKNOWNS];
    float mtmp;
    float ng;
    int ix;
    boolean_T guard1 = false;
    int a_size[1];
    float fcnOutput_data[MARQUARDT_MAX_NUMOBS];
    boolean_T exitg3;
    float mu;
    float nu;
    float nh;
    float kit;
    int kiteration;
    int exitg2;
    int h_size[2];
    float xnew[MARQUARDT_MAX_UNKNOWNS];
    float b_xnew;
    float c_a_data[MARQUARDT_MAX_UNKNOWNS];
    float dL_data[1];
    float b_A_data[1];
    float rn[MARQUARDT_MAX_NUMOBS];
    float fn;
    float df;
    boolean_T b_fcnOutput_data[1];
    boolean_T fcnOutput;
    int b_A_size[2];
    int ipiv_data[MARQUARDT_MAX_NUMOBS];
    float X_data[1];
    boolean_T b_guard1 = false;
    int A[1];
    boolean_T exitg1;

    const int maxeval = 100;
    const float tolg  = 1.0E-9F;
    const float tolx  = 1.0E-8F;

    // generic marquardt code below this line
    // -------------------------------------------------
    // 'autocalibrationAsymmetric:154' coder.inline('never');
    // MARQUARDT  Levenberg-Marquardt's method for least squares.
    // Find  xm = argmin{f(x)} , where  x  is an n-vector and
    // f(x) = 0.5 * sum(r_i(x)^2) .
    // The functions  r_i(x) (i=1,...,m) and the Jacobian matrix  J(x)
    // (with elements  J(i,j) = Dr_i/Dx_j ) must be given by a MATLAB
    // function with declaration
    // function  [r, J] = fun(x,p1,p2,...)
    // p1,p2,... are parameters of the function.  In connection with
    // nonlinear data fitting they may be arrays with coordinates of
    // the data points.
    //
    // Call    [X, info] = marquardt(fun, x0)
    // [X, info] = marquardt(fun, x0, opts, p1,p2,...)
    // [X, info, perf] = marquardt(.....)
    //
    // Input parameters
    // fun  :  Handle to the function.
    // x0   :  Starting guess for  xm .
    // opts :  Either a struct with fields 'tau', 'tolg', 'tolx' and 'maxeval',
    // or a vector with the values of these options,
    // opts = [tau  tolg  tolx  maxeval].
    // tau    used in starting value for Marquardt parameter:
    // mu = tau * max( [J(x0)'*J(x0)]_(ii) )  .
    // The other options are used in stopping criteria:
    // ||g||_inf <= tolg                            or
    // ||dx||_2  <= tolx*(tolx + ||x||_2)           or
    // no. of function evaluations exceeds  maxeval .
    // Default  tau = 1e-3,  tolg = 1e-4,  tolx = 1e-8, maxeval = 100.
    // If the input opts has less than 4 elements, it is augmented by
    // the default values.  Also, zeros and negative elements are
    // replaced by the default values.
    // p1,p2,..  are passed directly to the function FUN .
    //
    // Output parameters
    // X    :  If  perf  is present, then array, holding the iterates
    // columnwise.  Otherwise, computed solution vector.
    // info :  Performance information, vector with 7 elements:
    // info(1:4) = final values of
    // [f(x)  ||g(x)||inf  ||dx||2  mu/max( [J(x)'*J(x)]_(ii) )] .
    // info(5:6) = no. of iteration steps and function evaluations.
    // info(7) = 1 :  Stopped by small gradient
    // 2 :  Stopped by small x-step
    // 3 :  No. of function evaluations exceeded
    // -1 :  x is not a real valued vector
    // -2 :  r is not a real valued column vector
    // -3 :  J is not a real valued matrix
    // -4 :  Dimension mismatch in x, r, J
    // -5 :  Overflow during computation
    // perf :  Struct with fields
    // f :  values of  f(xk) ,
    // ng :  values of  || g(xk) ||_inf ,
    // mu :  values of Marquardt damping parameter.
    // Version 10.11.03.  hbn(a)imm.dtu.dk
    // Check parameters and function call
    // 'autocalibrationAsymmetric:213' [stop, x, ~] = checkx(x0);
    for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
        X[ixstart] = x0[ixstart];
    }

    J = (float *)pios_malloc(numUnknowns * numObservations * sizeof(float));

    // 'autocalibrationAsymmetric:213' ~
    // 'autocalibrationAsymmetric:214' if  ~stop
    // 'autocalibrationAsymmetric:214' [stop, f, r, J] = checkrJ(fun,x0,varargin{:});
    checkrJ(callback, x0, numUnknowns, numObservations, Vx, Vy, Vz, Vvariance, &stop, &f, r, J);

    // 'autocalibrationAsymmetric:214' k = 1;
    // 'autocalibrationAsymmetric:216' A = zeros(size(J,1));
    A_size[0] = numObservations;
    A_size[1] = numObservations;
    int A_total_mem_size = numObservations * numObservations * sizeof(float);
    A_data    = (float *)pios_malloc(A_total_mem_size);
    memset(A_data, 0, A_total_mem_size);
    h_data    = (float *)pios_malloc(A_total_mem_size);
    working_mem[0] = (float *)pios_malloc(A_total_mem_size);
    working_mem[1] = (float *)pios_malloc(A_total_mem_size);
    working_mem[2] = (float *)pios_malloc(A_total_mem_size);
    working_mem[3] = (float *)pios_malloc(A_total_mem_size);

    // 'autocalibrationAsymmetric:217' g = zeros(size(J'));
    g_size[0] = numUnknowns;
    g_size[1] = 1;
    memset(&g_data[0], 0, numUnknowns * sizeof(float));

    a1 = (float *)pios_malloc(numUnknowns * numObservations * sizeof(float));

    // 'autocalibrationAsymmetric:219' if  ~stop
    if (!(stop != 0)) {
        // 'autocalibrationAsymmetric:220' g = J'*r;
        for (ixstart = 0; ixstart < numObservations; ixstart++) {
            for (n = 0; n < numUnknowns; n++) {
                a1[n + numUnknowns * ixstart] = J[ixstart + numObservations * n];
            }
        }

        g_size[0] = numUnknowns;
        g_size[1] = 1;

        // 'autocalibrationAsymmetric:220' ng = norm(g,inf);
        for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
            b_a[ixstart] = 0.0F;
            for (n = 0; n < numObservations; n++) {
                b_a[ixstart] += a1[ixstart + numUnknowns * n] * r[n];
            }

            g_data[ixstart] = b_a[ixstart];
            c_a[ixstart]    = 0.0F;
            for (n = 0; n < numObservations; n++) {
                c_a[ixstart] += a1[ixstart + numUnknowns * n] * r[n];
            }

            a_data[ixstart] = c_a[ixstart];
        }

        mtmp = b_norm(a_data, numUnknowns);
        ng   = mtmp;

        // 'autocalibrationAsymmetric:220' A = J'*J;
        for (ixstart = 0; ixstart < numObservations; ixstart++) {
            for (n = 0; n < numUnknowns; n++) {
                a1[n + numUnknowns * ixstart] = J[ixstart + numObservations * n];
            }
        }

        for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
            for (n = 0; n < numUnknowns; n++) {}
        }

        A_size[0] = numUnknowns;
        A_size[1] = numUnknowns;
        for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
            for (n = 0; n < numUnknowns; n++) {
                A_data[ixstart + numUnknowns * n] = 0.0F;
                for (ix = 0; ix < numObservations; ix++) {
                    A_data[ixstart + numUnknowns * n] += a1[ixstart + numUnknowns * ix] * J[ix + numObservations * n];
                }
            }
        }

        // 'autocalibrationAsymmetric:221' if  isinf(ng) || isinf(norm(A(:),inf))
        guard1 = false;
        if (rtIsInfF(mtmp)) {
            guard1 = true;
        } else {
            float ymax = 0.0;
            for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
                for (n = 0; n < numUnknowns; n++) {
                    float e_a = 0.0F;
                    for (ix = 0; ix < numObservations; ix++) {
                        e_a += a1[ixstart + numUnknowns * ix] * J[ix + numObservations * n];
                    }
                    float absx = fabsf(e_a);
                    if (absx > ymax) {
                        ymax = absx;
                    }
                }
            }
            mtmp = ymax;

            if (rtIsInfF(mtmp)) {
                guard1 = true;
            }
        }

        if (guard1) {
            // 'autocalibrationAsymmetric:221' stop = -5;
            stop = -5.0F;
        }
    } else {
        // 'autocalibrationAsymmetric:222' else
        // 'autocalibrationAsymmetric:223' f = NaN;
        f  = ((real32_T)rtNaNF);

        // 'autocalibrationAsymmetric:223' ng = NaN;
        ng = ((real32_T)rtNaNF);
    }

    // 'autocalibrationAsymmetric:226' if  stop
    if (stop != 0) {
        // 'autocalibrationAsymmetric:227' X = x0;
        // 'autocalibrationAsymmetric:227' perf = [];
        // 'autocalibrationAsymmetric:227' info = [f  ng  0  opts(1)  0  1 stop];
        info[0] = f;
        info[1] = ng;
        info[2] = 0.0F;
        info[3] = 0.001F;
        info[4] = 0.0F;
        info[5] = 1.0F;
        info[6] = stop;
    } else {
        // if  nargin < 3,  opts = []; end
        // opts  = checkopts('marquardt', opts);  % use default options where required
        // 'autocalibrationAsymmetric:233' tau = opts(1);
        // 'autocalibrationAsymmetric:233' tolg = opts(2);
        // 'autocalibrationAsymmetric:233' tolx = opts(3);
        // 'autocalibrationAsymmetric:233' maxeval = opts(4);
        // Finish initialization
        // 'autocalibrationAsymmetric:236' mu = tau * max(diag(A));
        diag(A_data, A_size, fcnOutput_data, a_size);
        ixstart = 1;
        n = a_size[0];
        mtmp    = fcnOutput_data[0];
        if (rtIsNaNF(fcnOutput_data[0])) {
            ix     = 2;
            exitg3 = false;
            while ((!exitg3) && (ix <= n)) {
                ixstart = ix;
                if (!rtIsNaNF(fcnOutput_data[ix - 1])) {
                    mtmp   = fcnOutput_data[ix - 1];
                    exitg3 = true;
                } else {
                    ix++;
                }
            }
        }

        if (ixstart < a_size[0]) {
            while (ixstart + 1 <= n) {
                if (fcnOutput_data[ixstart] > mtmp) {
                    mtmp = fcnOutput_data[ixstart];
                }

                ixstart++;
            }
        }

        mu = 0.001F * mtmp;

        // 'autocalibrationAsymmetric:237' Trace = nargout > 2;
        // 'autocalibrationAsymmetric:238' if  Trace
        // 'autocalibrationAsymmetric:242' nu = 2;
        nu   = 2.0F;

        // 'autocalibrationAsymmetric:242' nh = 0;
        nh   = 0.0F;

        // 'autocalibrationAsymmetric:242' stop = 0;
        stop = 0.0F;

        // 'autocalibrationAsymmetric:242' kit = 0;
        kit  = 0.0F;

        // 'autocalibrationAsymmetric:242' dL = zeros;
        // 'autocalibrationAsymmetric:242' df = zeros;
        // 'autocalibrationAsymmetric:242' h = zeros(size(J'));
        memset(&h_data[0], 0, MARQUARDT_MAX_UNKNOWNS * sizeof(float));

        // 'autocalibrationAsymmetric:242' kiteration = 0;
        kiteration = 0.0F;

        // Iterate
        // 'autocalibrationAsymmetric:245' while   ~stop
        do {
            exitg2 = 0;
            if (!(stop != 0)) {
                // 'autocalibrationAsymmetric:246' if  ng <= tolg
                if (ng <= tolg) {
                    // 'autocalibrationAsymmetric:246' stop = 1;
                    stop = 1.0F;
                } else {
                    // 'autocalibrationAsymmetric:247' else
                    // 'autocalibrationAsymmetric:248' [h, mu] = geth(A,g,mu);
                    geth(A_data, A_size, working_mem, g_data, g_size, &mu, h_data, h_size);

                    // 'autocalibrationAsymmetric:249' nh = norm(h);
                    mtmp = e_norm(h_data, h_size);
                    nh   = mtmp;

                    // 'autocalibrationAsymmetric:249' nx = tolx + norm(x);
                    // 'autocalibrationAsymmetric:250' if  nh <= tolx*nx
                    if (mtmp <= tolx * (tolx + norm(X, numUnknowns))) {
                        // 'autocalibrationAsymmetric:250' stop = 2;
                        stop = 2.0F;
                    }
                }

                // 'autocalibrationAsymmetric:252' if  ~stop
                if (!(stop != 0)) {
                    // 'autocalibrationAsymmetric:253' xnew = x + h;
                    // 'autocalibrationAsymmetric:254' h = xnew - x;
                    // 'autocalibrationAsymmetric:255' dL = (h'*(mu*h - g))/2;
                    for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
                        b_xnew = X[ixstart] + h_data[ixstart];
                        mtmp   = b_xnew - X[ixstart];
                        b_a[ixstart]    = mu * mtmp - g_data[ixstart];
                        c_data[ixstart] = mtmp;
                        h_data[ixstart] = b_xnew - X[ixstart];
                        xnew[ixstart]   = b_xnew;
                    }

                    for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
                        c_a_data[ixstart] = c_data[ixstart];
                    }

                    mtmp    = 0.0F;
                    ixstart = -1;
                    for (n = 0; n < numUnknowns; n++) {
                        if (!is_zero(b_a[n])) {
                            mtmp += b_a[n] * c_a_data[ixstart + 1];
                        }

                        ixstart++;
                    }

                    dL_data[0] = mtmp / 2.0F;

                    // 'autocalibrationAsymmetric:256' [stop, fn, rn, Jn] = checkrJ(fun, xnew, varargin{:});
                    checkrJ(callback, xnew, numUnknowns, numObservations, Vx, Vy, Vz, Vvariance, &stop, &fn, rn, J);

                    // 'autocalibrationAsymmetric:257' kiteration = kiteration + 1;
                    kiteration++;

                    // 'autocalibrationAsymmetric:258' if  ~stop
                    if (!(stop != 0)) {
                        // 'autocalibrationAsymmetric:259' if  length(rn) ~= length(r)
                        // 'autocalibrationAsymmetric:261' else
                        // more accurate
                        // 'autocalibrationAsymmetric:262' df = ( (r - rn)' * (r + rn) )/2;
                        mtmp = 0.0F;
                        for (ixstart = 0; ixstart < numObservations; ixstart++) {
                            mtmp += (r[ixstart] - rn[ixstart]) * (r[ixstart] + rn[ixstart]);
                        }

                        df = mtmp / 2.0F;

                        // 'autocalibrationAsymmetric:265' if  (all(all(dL)) > 0 && all(all(df)) > 0)
                        all(dL_data, b_fcnOutput_data, h_size);
                        fcnOutput = b_all(b_fcnOutput_data);
                        if (((int)fcnOutput > 0) && (!is_zero(df))) {
                            // Update x and modify mu
                            // 'autocalibrationAsymmetric:266' kit = kit + 1;
                            kit++;

                            // 'autocalibrationAsymmetric:267' x = xnew;
                            for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
                                X[ixstart] = xnew[ixstart];
                            }

                            // 'autocalibrationAsymmetric:268' f = fn;
                            f = fn;

                            // 'autocalibrationAsymmetric:269' J = Jn;
                            // 'autocalibrationAsymmetric:270' r = rn;
                            for (ixstart = 0; ixstart < numObservations; ixstart++) {
                                r[ixstart] = rn[ixstart];
                            }

                            // 'autocalibrationAsymmetric:271' A = J'*J;
                            A_size[0] = numUnknowns;
                            A_size[1] = numUnknowns;
                            for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
                                for (n = 0; n < numUnknowns; n++) {
                                    A_data[ixstart + numUnknowns * n] = 0.0F;
                                    for (ix = 0; ix < numObservations; ix++) {
                                        A_data[ixstart + numUnknowns * n] += J[ix + numObservations * ixstart] * J[ix + numObservations * n];
                                    }
                                }
                            }

                            // 'autocalibrationAsymmetric:271' g = J'*r;
                            for (ixstart = 0; ixstart < numObservations; ixstart++) {
                                for (n = 0; n < numUnknowns; n++) {
                                    a1[n + numUnknowns * ixstart] = J[ixstart + numObservations * n];
                                }
                            }

                            g_size[0] = numUnknowns;
                            g_size[1] = 1;

                            // 'autocalibrationAsymmetric:271' ng = norm(g,inf);
                            for (ixstart = 0; ixstart < numUnknowns; ixstart++) {
                                b_a[ixstart] = 0.0F;
                                for (n = 0; n < numObservations; n++) {
                                    b_a[ixstart] += a1[ixstart + numUnknowns * n] * rn[n];
                                }

                                g_data[ixstart] = b_a[ixstart];
                                c_a[ixstart]    = 0.0F;
                                for (n = 0; n < numObservations; n++) {
                                    c_a[ixstart] += a1[ixstart + numUnknowns * n] * rn[n];
                                }

                                a_data[ixstart] = c_a[ixstart];
                            }

                            ng = b_norm(a_data, numUnknowns);

                            // 'autocalibrationAsymmetric:272' mu = mu * max(max(1/3, 1 - (2*df/dL - 1)^3));
                            b_A_size[0] = 1;
                            b_A_size[1] = 1;
                            b_A_data[0] = dL_data[0];
                            xzgetrf(1, 1, b_A_data, b_A_size, 1, ipiv_data, h_size, &ixstart);
                            X_data[0]   = 1.0F / b_A_data[0] * (2.0F * df) - 1.0F;
                            mpower(X_data, dL_data, h_size);
                            dL_data[0]  = 1.0F - dL_data[0];
                            mu *= fmaxf(0.333333343F, dL_data[0]);

                            // 'autocalibrationAsymmetric:272' nu = 2;
                            nu  = 2.0F;

                            // 'autocalibrationAsymmetric:273' if  Trace
                        } else {
                            // 'autocalibrationAsymmetric:277' else
                            // Same  x, increase  mu
                            // 'autocalibrationAsymmetric:278' mu = mu*nu;
                            mu *= nu;

                            // 'autocalibrationAsymmetric:278' nu = 2*nu;
                            nu *= 2.0F;
                        }

                        // 'autocalibrationAsymmetric:280' if      kiteration > maxeval
                        if (kiteration > maxeval) {
                            // 'autocalibrationAsymmetric:281' stop = 3;
                            stop = 3.0F;
                        } else {
                            b_guard1 = false;
                            if (rtIsInfF(ng)) {
                                b_guard1 = true;
                            } else {
                                A[0] = A_size[0] * A_size[1];
                                mtmp = c_norm(A_data, A);
                                if (rtIsInfF(mtmp)) {
                                    b_guard1 = true;
                                }
                            }

                            if (b_guard1) {
                                // 'autocalibrationAsymmetric:282' elseif isinf(ng) || isinf(norm(A(:),inf))
                                // 'autocalibrationAsymmetric:283' stop = -5;
                                stop = -5.0F;
                            }
                        }
                    }
                }
            } else {
                exitg2 = 1;
            }
        } while (exitg2 == 0);

        // Set return values
        // 'autocalibrationAsymmetric:290' if  Trace
        // 'autocalibrationAsymmetric:293' else
        // 'autocalibrationAsymmetric:294' X = x;
        // 'autocalibrationAsymmetric:296' if  stop < 0
        if (stop < 0.0F) {
            // 'autocalibrationAsymmetric:297' f = NaN;
            f  = ((real32_T)rtNaNF);

            // 'autocalibrationAsymmetric:297' ng = NaN;
            ng = ((real32_T)rtNaNF);
        }

        // 'autocalibrationAsymmetric:300' info = [f  ng  nh  mu/max(diag(A))  kit k  stop];
        diag(A_data, A_size, fcnOutput_data, a_size);
        ixstart = 1;
        n = a_size[0];
        mtmp    = fcnOutput_data[0];
        if (rtIsNaNF(fcnOutput_data[0])) {
            ix     = 2;
            exitg1 = false;
            while ((!exitg1) && (ix <= n)) {
                ixstart = ix;
                if (!rtIsNaNF(fcnOutput_data[ix - 1])) {
                    mtmp   = fcnOutput_data[ix - 1];
                    exitg1 = true;
                } else {
                    ix++;
                }
            }
        }

        if (ixstart < a_size[0]) {
            while (ixstart + 1 <= n) {
                if (fcnOutput_data[ixstart] > mtmp) {
                    mtmp = fcnOutput_data[ixstart];
                }

                ixstart++;
            }
        }

        info[0] = f;
        info[1] = ng;
        info[2] = nh;
        info[3] = mu / mtmp;
        info[4] = kit;
        info[5] = kiteration;
        info[6] = stop;
    }

    pios_free(A_data); A_data = 0;
    pios_free(h_data); h_data = 0;
    pios_free(a1); a1 = 0;
    pios_free(J); J   = 0;
    pios_free(working_mem[0]); working_mem[0] = 0;
    pios_free(working_mem[1]); working_mem[1] = 0;
    pios_free(working_mem[2]); working_mem[2] = 0;
    pios_free(working_mem[3]); working_mem[3] = 0;
}
