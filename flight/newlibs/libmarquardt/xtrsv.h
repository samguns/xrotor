//
// File: xtrsv.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __XTRSV_H__
#define __XTRSV_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void xtrsv(int n, const float A_data[], const int A_size[2], int lda,
                  float x_data[]);

#endif

//
// File trailer for xtrsv.h
//
// [EOF]
//
