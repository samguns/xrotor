//
// File: svd.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __SVD_H__
#define __SVD_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void svd(const float A_data[], const int A_size[2], float U_data[], int
                U_size[1]);

#endif

//
// File trailer for svd.h
//
// [EOF]
//
