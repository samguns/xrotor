/*
 * File: rt_nonfinite.cpp
 *
 * MATLAB Coder version            : 3.0
 * C/C++ source code generated on  : 03-Jan-2016 09:45:08
 */

/*
 * Abstract:
 *      MATLAB for code generation function to initialize non-finites,
 *      (Inf, NaN and -Inf).
 */
#include "rt_nonfinite.h"
#include "rtGetNaN.h"
#include "rtGetInf.h"
#include "mathmisc.h"

// real_T rtInf;
// real_T rtMinusInf;
// real_T rtNaN;
real32_T rtInfF;
real32_T rtMinusInfF;
real32_T rtNaNF;

/* Function: rt_InitInfAndNaN ==================================================
 * Abstract:
 * Initialize the rtInf, rtMinusInf, and rtNaN needed by the
 * generated code. NaN is initialized as non-signaling. Assumes IEEE.
 */
void rt_InitInfAndNaN(size_t realSize)
{
    (void)(realSize);
    // rtNaN       = rtGetNaN();
    rtNaNF = rtGetNaNF();
    // rtInf       = rtGetInf();
    rtInfF = rtGetInfF();
    // rtMinusInf  = rtGetMinusInf();
    rtMinusInfF = rtGetMinusInfF();
}

#if 0
/* Function: rtIsInf ==================================================
 * Abstract:
 * Test if value is infinite
 */
boolean_T rtIsInf(real_T value)
{
    return (is_equal(value, rtInf) || is_equal(value, rtMinusInf)) ? true : false;
}

#endif

/* Function: rtIsInfF =================================================
 * Abstract:
 * Test if single-precision value is infinite
 */
boolean_T rtIsInfF(real32_T value)
{
    return (is_equal(value, rtInfF) || is_equal(value, rtMinusInfF)) ? true : false;
}

#if 0
/* Function: rtIsNaN ==================================================
 * Abstract:
 * Test if value is not a number
 */
boolean_T rtIsNaN(real_T value)
{
    return isnan(value) ? true : false;
}

#endif

/* Function: rtIsNaNF =================================================
 * Abstract:
 * Test if single-precision value is not a number
 */
boolean_T rtIsNaNF(real32_T value)
{
    return isnan(value) ? true : false;
}

/*
 * File trailer for rt_nonfinite.cpp
 *
 * [EOF]
 */
