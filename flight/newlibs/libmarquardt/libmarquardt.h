#ifndef __LIBMARQUARDT_H__
#define __LIBMARQUARDT_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "pios_mem.h"

#define MARQUARDT_MAX_UNKNOWNS                        12
#define MARQUARDT_MAX_NUMOBS                          14
#define MARQUARDT_MAX_UNKNOWNS_BY_UNKNOWNS_1DSIZE     144
#define MARQUARDT_MAX_OBS_BY_OBS_1DSIZE               196
#define MARQUARDT_MAX_UNKNOWNS_BY_OBSERVATIONS_1DSIZE 168
#endif

//
// File trailer for autocalibrationAsymmetric.h
//
// [EOF]
//
