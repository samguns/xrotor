//
// File: mldivide.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __MLDIVIDE_H__
#define __MLDIVIDE_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void b_mldivide(const float A_data[], const int A_size[2], const float
                       B_data[], const int B_size[2], float Y_data[], int Y_size[2]);
extern void mldivide(const float A_data[], const int A_size[2], const float
                     B_data[], const int B_size[2], float Y_data[], int Y_size[2]);

#endif

//
// File trailer for mldivide.h
//
// [EOF]
//
