//
// File: xaxpy.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "xaxpy.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : int n
// float a
// const float x_data[]
// int ix0
// float y_data[]
// int iy0
// Return Type  : void
//
void b_xaxpy(int n, float a, const float x_data[], int ix0, float y_data[], int
             iy0)
{
    int ix;
    int iy;
    int k;

    if ((n < 1) || (is_zero(a))) {} else {
        ix = ix0 - 1;
        iy = iy0 - 1;
        for (k = 0; k < n; k++) {
            y_data[iy] += a * x_data[ix];
            ix++;
            iy++;
        }
    }
}

//
// Arguments    : int n
// float a
// const float x_data[]
// int ix0
// float y_data[]
// int iy0
// Return Type  : void
//
void c_xaxpy(int n, float a, const float x_data[], int ix0, float y_data[], int
             iy0)
{
    int ix;
    int iy;
    int k;

    if ((n < 1) || (is_zero(a))) {} else {
        ix = ix0 - 1;
        iy = iy0 - 1;
        for (k = 0; k < n; k++) {
            y_data[iy] += a * x_data[ix];
            ix++;
            iy++;
        }
    }
}

//
// Arguments    : int n
// float a
// int ix0
// float y_data[]
// int iy0
// Return Type  : void
//
void xaxpy(int n, float a, int ix0, float y_data[], int iy0)
{
    int ix;
    int iy;
    int k;

    if ((n < 1) || (is_zero(a))) {} else {
        ix = ix0 - 1;
        iy = iy0 - 1;
        for (k = 0; k < n; k++) {
            y_data[iy] += a * y_data[ix];
            ix++;
            iy++;
        }
    }
}

//
// File trailer for xaxpy.cpp
//
// [EOF]
//
