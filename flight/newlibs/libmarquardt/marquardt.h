//
// File: autocalibrationAsymmetric.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __AUTOCALIBRATIONASYMMETRIC_H__
#define __AUTOCALIBRATIONASYMMETRIC_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"
#include "libmarquardt.h"

typedef void (*MarquartCallback_T)(const float *X, int32_T sampleCount, const float *Vx, const float *Vy, const float *Vz, const float *Vvariance, float *r, float *J);

// Function Declarations
extern void marquardt(MarquartCallback_T callback,
                      const float x0[MARQUARDT_MAX_UNKNOWNS],
                      int32_T numUnknowns,
                      int32_T numObservations,
                      const float Vx[MARQUARDT_MAX_NUMOBS],
                      const float Vy[MARQUARDT_MAX_NUMOBS],
                      const float Vz[MARQUARDT_MAX_NUMOBS],
                      const float Vvariance[MARQUARDT_MAX_NUMOBS],
                      float X[MARQUARDT_MAX_UNKNOWNS],
                      float info[7]);

#endif // ifndef __AUTOCALIBRATIONASYMMETRIC_H__

//
// File trailer for autocalibrationAsymmetric.h
//
// [EOF]
//
