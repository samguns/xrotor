//
// File: qrsolve.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "qrsolve.h"

// Function Definitions

//
// Arguments    : const float A_data[]
// const int A_size[2]
// Return Type  : int
//
int rankFromQR(const float A_data[], const int A_size[2])
{
    int r;
    int minmn;
    int maxmn;
    float tol;

    r = 0;
    if (A_size[0] < A_size[1]) {
        minmn = A_size[0];
        maxmn = A_size[1];
    } else {
        minmn = A_size[1];
        maxmn = A_size[0];
    }

    if (minmn > 0) {
        tol = (float)maxmn * fabsf(A_data[0]) * 1.1920929E-7F;
        while ((r < minmn) && (fabsf(A_data[r + A_size[0] * r]) >= tol)) {
            r++;
        }
    }

    return r;
}

//
// File trailer for qrsolve.cpp
//
// [EOF]
//
