//
// File: norm.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __NORM_H__
#define __NORM_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern float b_norm(const float x_data[], int size);
extern float c_norm(const float x_data[], const int x_size[1]);
extern float d_norm(const float x_data[], const int x_size[2]);
extern float e_norm(const float x_data[], const int x_size[2]);
extern float norm(const float x[], int norm);

#endif

//
// File trailer for norm.h
//
// [EOF]
//
