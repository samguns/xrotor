//
// File: xdotc.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __XDOTC_H__
#define __XDOTC_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern float xdotc(int n, const float x_data[], int ix0, const float y_data[],
                   int iy0);

#endif

//
// File trailer for xdotc.h
//
// [EOF]
//
