//
// File: autocalibrationAsymmetric_initialize.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __AUTOCALIBRATIONASYMMETRIC_INITIALIZE_H__
#define __AUTOCALIBRATIONASYMMETRIC_INITIALIZE_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void marquardt_initialize();

#endif
