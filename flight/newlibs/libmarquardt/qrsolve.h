//
// File: qrsolve.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __QRSOLVE_H__
#define __QRSOLVE_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern int rankFromQR(const float A_data[], const int A_size[2]);

#endif

//
// File trailer for qrsolve.h
//
// [EOF]
//
