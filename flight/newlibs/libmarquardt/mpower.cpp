//
// File: mpower.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 04-Jan-2016 21:30:06
//

// Include Files
#include "rt_nonfinite.h"
#include "mpower.h"

// Function Definitions

//
// Arguments    : const float a_data[]
// float c_data[]
// int c_size[2]
// Return Type  : void
//
void mpower(const float a_data[], float c_data[], int c_size[2])
{
    c_size[0] = 1;
    c_size[1] = 1;
    c_data[0] = a_data[0] * (a_data[0] * a_data[0]);
}

//
// File trailer for mpower.cpp
//
// [EOF]
//
