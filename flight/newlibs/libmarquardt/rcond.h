//
// File: rcond.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __RCOND_H__
#define __RCOND_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern float rcond(const float A_data[], const int A_size[2]);

#endif

//
// File trailer for rcond.h
//
// [EOF]
//
