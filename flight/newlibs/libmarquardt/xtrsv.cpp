//
// File: xtrsv.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "xtrsv.h"

// Function Definitions

//
// Arguments    : int n
// const float A_data[]
// const int A_size[2]
// int lda
// float x_data[]
// Return Type  : void
//
void xtrsv(int n, const float A_data[], const int A_size[2], int lda, float
           x_data[])
{
    int j;
    int jjA;
    int i;
    int ix;

    if ((A_size[0] == 0) || (A_size[1] == 0)) {} else {
        for (j = n - 1; j + 1 > 0; j--) {
            jjA = j + j * lda;
            x_data[j] /= A_data[jjA];
            for (i = 1; i <= j; i++) {
                ix = j - i;
                x_data[ix] -= x_data[j] * A_data[jjA - i];
            }
        }
    }
}

//
// File trailer for xtrsv.cpp
//
// [EOF]
//
