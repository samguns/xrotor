//
// File: colon.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __COLON_H__
#define __COLON_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void eml_signed_integer_colon(int b, int y_data[], int y_size[2]);

#endif

//
// File trailer for colon.h
//
// [EOF]
//
