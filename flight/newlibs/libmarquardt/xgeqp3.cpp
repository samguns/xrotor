//
// File: xgeqp3.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "xgeqp3.h"
#include "xnrm2.h"
#include "colon.h"
#include "libmarquardt.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : float A_data[]
// int A_size[2]
// float tau_data[]
// int tau_size[1]
// int jpvt_data[]
// int jpvt_size[2]
// Return Type  : void
//
void xgeqp3(float A_data[], int A_size[2], float tau_data[], int tau_size[1], int jpvt_data[], int jpvt_size[2])
{
    int m;
    int n;
    int mn;
    int itemp;
    int i6;
    float work_data[MARQUARDT_MAX_UNKNOWNS];
    float vn1_data[MARQUARDT_MAX_UNKNOWNS];
    float vn2_data[MARQUARDT_MAX_UNKNOWNS];
    int k;
    int nmi;
    int i;
    int i_i;
    int mmi;
    int ix;
    float smax;
    float s;
    int pvt;
    int iy;
    float atmp;
    int i_ip1;
    int lastv;
    int lastc;
    boolean_T exitg2;
    int exitg1;

    m = A_size[0];
    n = A_size[1];
    if (A_size[0] <= A_size[1]) {
        mn = A_size[0];
    } else {
        mn = A_size[1];
    }

    tau_size[0] = (signed char)mn;
    eml_signed_integer_colon(A_size[1], jpvt_data, jpvt_size);
    if ((A_size[0] == 0) || (A_size[1] == 0)) {} else {
        itemp = (signed char)A_size[1];
        for (i6 = 0; i6 < itemp; i6++) {
            work_data[i6] = 0.0F;
        }

        k = 1;
        for (nmi = 0; nmi + 1 <= n; nmi++) {
            vn1_data[nmi] = xnrm2(m, A_data, k);
            vn2_data[nmi] = vn1_data[nmi];
            k += m;
        }

        for (i = 0; i + 1 <= mn; i++) {
            i_i = i + i * m;
            nmi = n - i;
            mmi = (m - i) - 1;
            if (nmi < 1) {
                itemp = -1;
            } else {
                itemp = 0;
                if (nmi > 1) {
                    ix   = i;
                    smax = fabsf(vn1_data[i]);
                    for (k = 0; k + 2 <= nmi; k++) {
                        ix++;
                        s = fabsf(vn1_data[ix]);
                        if (s > smax) {
                            itemp = k + 1;
                            smax  = s;
                        }
                    }
                }
            }

            pvt = i + itemp;
            if (pvt + 1 != i + 1) {
                ix = m * pvt;
                iy = m * i;
                for (k = 1; k <= m; k++) {
                    smax = A_data[ix];
                    A_data[ix] = A_data[iy];
                    A_data[iy] = smax;
                    ix++;
                    iy++;
                }

                itemp = jpvt_data[pvt];
                jpvt_data[pvt] = jpvt_data[i];
                jpvt_data[i]   = itemp;
                vn1_data[pvt]  = vn1_data[i];
                vn2_data[pvt]  = vn2_data[i];
            }

            if (i + 1 < m) {
                atmp = A_data[i_i];
                s    = 0.0F;
                if (1 + mmi <= 0) {} else {
                    smax = b_xnrm2(mmi, A_data, i_i + 2);
                    if (!is_zero(smax)) {
                        smax = hypotf(A_data[i_i], smax);
                        if (A_data[i_i] >= 0.0F) {
                            smax = -smax;
                        }

                        if (fabsf(smax) < 9.86076132E-32F) {
                            itemp = 0;
                            do {
                                itemp++;
                                i6 = i_i + mmi;
                                for (k = i_i + 1; k + 1 <= i6 + 1; k++) {
                                    A_data[k] *= 1.01412048E+31F;
                                }

                                smax *= 1.01412048E+31F;
                                atmp *= 1.01412048E+31F;
                            } while (!(fabsf(smax) >= 9.86076132E-32F));

                            smax = b_xnrm2(mmi, A_data, i_i + 2);
                            smax = hypotf(atmp, smax);
                            if (atmp >= 0.0F) {
                                smax = -smax;
                            }

                            s    = (smax - atmp) / smax;
                            atmp = 1.0F / (atmp - smax);
                            i6   = i_i + mmi;
                            for (k = i_i + 1; k + 1 <= i6 + 1; k++) {
                                A_data[k] *= atmp;
                            }

                            for (k = 1; k <= itemp; k++) {
                                smax *= 9.86076132E-32F;
                            }

                            atmp = smax;
                        } else {
                            s    = (smax - A_data[i_i]) / smax;
                            atmp = 1.0F / (A_data[i_i] - smax);
                            i6   = i_i + mmi;
                            for (k = i_i + 1; k + 1 <= i6 + 1; k++) {
                                A_data[k] *= atmp;
                            }

                            atmp = smax;
                        }
                    }
                }

                tau_data[i] = s;
                A_data[i_i] = atmp;
            } else {
                tau_data[i] = 0.0F;
            }

            if (i + 1 < n) {
                atmp  = A_data[i_i];
                A_data[i_i] = 1.0F;
                i_ip1 = (i + (i + 1) * m) + 1;
                if (!is_zero(tau_data[i])) {
                    lastv = mmi;
                    itemp = i_i + mmi;
                    while ((lastv + 1 > 0) && (is_zero(A_data[itemp]))) {
                        lastv--;
                        itemp--;
                    }

                    lastc  = nmi - 1;
                    exitg2 = false;
                    while ((!exitg2) && (lastc > 0)) {
                        itemp = i_ip1 + (lastc - 1) * m;
                        k     = itemp;
                        do {
                            exitg1 = 0;
                            if (k <= itemp + lastv) {
                                if (!is_zero(A_data[k - 1])) {
                                    exitg1 = 1;
                                } else {
                                    k++;
                                }
                            } else {
                                lastc--;
                                exitg1 = 2;
                            }
                        } while (exitg1 == 0);

                        if (exitg1 == 1) {
                            exitg2 = true;
                        }
                    }
                } else {
                    lastv = -1;
                    lastc = 0;
                }

                if (lastv + 1 > 0) {
                    if (lastc == 0) {} else {
                        for (iy = 1; iy <= lastc; iy++) {
                            work_data[iy - 1] = 0.0F;
                        }

                        iy    = 0;
                        i6    = i_ip1 + m * (lastc - 1);
                        itemp = i_ip1;
                        while ((m > 0) && (itemp <= i6)) {
                            ix   = i_i;
                            smax = 0.0F;
                            pvt  = itemp + lastv;
                            for (k = itemp; k <= pvt; k++) {
                                smax += A_data[k - 1] * A_data[ix];
                                ix++;
                            }

                            work_data[iy] += smax;
                            iy++;
                            itemp += m;
                        }
                    }

                    if (is_zero(-tau_data[i])) {} else {
                        itemp = 0;
                        for (nmi = 1; nmi <= lastc; nmi++) {
                            if (!is_zero(work_data[itemp])) {
                                smax = work_data[itemp] * -tau_data[i];
                                ix   = i_i;
                                i6   = lastv + i_ip1;
                                for (pvt = i_ip1; pvt <= i6; pvt++) {
                                    A_data[pvt - 1] += A_data[ix] * smax;
                                    ix++;
                                }
                            }

                            itemp++;
                            i_ip1 += m;
                        }
                    }
                }

                A_data[i_i] = atmp;
            }

            for (nmi = i + 1; nmi + 1 <= n; nmi++) {
                if (!is_zero(vn1_data[nmi])) {
                    smax = fabsf(A_data[i + A_size[0] * nmi]) / vn1_data[nmi];
                    smax = 1.0F - smax * smax;
                    if (smax < 0.0F) {
                        smax = 0.0F;
                    }

                    s = vn1_data[nmi] / vn2_data[nmi];
                    s = smax * (s * s);
                    if (s <= 0.000345266977F) {
                        if (i + 1 < m) {
                            vn1_data[nmi] = c_xnrm2(mmi, A_data, (i + m * nmi) + 2);
                            vn2_data[nmi] = vn1_data[nmi];
                        } else {
                            vn1_data[nmi] = 0.0F;
                            vn2_data[nmi] = 0.0F;
                        }
                    } else {
                        vn1_data[nmi] *= sqrtf(smax);
                    }
                }
            }
        }
    }
}

//
// File trailer for xgeqp3.cpp
//
// [EOF]
//
