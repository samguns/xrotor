//
// File: xnrm2.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __XNRM2_H__
#define __XNRM2_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern float b_xnrm2(int n, const float x_data[], int ix0);
extern float c_xnrm2(int n, const float x_data[], int ix0);
extern float d_xnrm2(int n, const float x_data[], int ix0);
extern float xnrm2(int n, const float x_data[], int ix0);

#endif

//
// File trailer for xnrm2.h
//
// [EOF]
//
