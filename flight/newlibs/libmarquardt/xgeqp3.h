//
// File: xgeqp3.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __XGEQP3_H__
#define __XGEQP3_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void xgeqp3(float A_data[], int A_size[2], float tau_data[], int
                   tau_size[1], int jpvt_data[], int jpvt_size[2]);

#endif

//
// File trailer for xgeqp3.h
//
// [EOF]
//
