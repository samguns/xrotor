//
// File: xrotg.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __XROTG_H__
#define __XROTG_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void xrotg(float *a, float *b, float *c, float *s);

#endif

//
// File trailer for xrotg.h
//
// [EOF]
//
