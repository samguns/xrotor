//
// File: all.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __ALL_H__
#define __ALL_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void all(const float x_data[], boolean_T y_data[], int y_size[2]);
extern boolean_T b_all(const boolean_T x_data[]);

#endif

//
// File trailer for all.h
//
// [EOF]
//
