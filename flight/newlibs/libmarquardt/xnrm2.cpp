//
// File: xnrm2.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "xnrm2.h"

// Function Definitions

//
// Arguments    : int n
// const float x_data[]
// int ix0
// Return Type  : float
//
float b_xnrm2(int n, const float x_data[], int ix0)
{
    float y;
    float scale;
    int kend;
    int k;
    float absxk;
    float t;

    y = 0.0F;
    if (n < 1) {} else if (n == 1) {
        y = fabsf(x_data[ix0 - 1]);
    } else {
        scale = 1.17549435E-38F;
        kend  = (ix0 + n) - 1;
        for (k = ix0; k <= kend; k++) {
            absxk = fabsf(x_data[k - 1]);
            if (absxk > scale) {
                t     = scale / absxk;
                y     = 1.0F + y * t * t;
                scale = absxk;
            } else {
                t  = absxk / scale;
                y += t * t;
            }
        }

        y = scale * sqrtf(y);
    }

    return y;
}

//
// Arguments    : int n
// const float x_data[]
// int ix0
// Return Type  : float
//
float c_xnrm2(int n, const float x_data[], int ix0)
{
    float y;
    float scale;
    int kend;
    int k;
    float absxk;
    float t;

    y = 0.0F;
    if (n < 1) {} else if (n == 1) {
        y = fabsf(x_data[ix0 - 1]);
    } else {
        scale = 1.17549435E-38F;
        kend  = (ix0 + n) - 1;
        for (k = ix0; k <= kend; k++) {
            absxk = fabsf(x_data[k - 1]);
            if (absxk > scale) {
                t     = scale / absxk;
                y     = 1.0F + y * t * t;
                scale = absxk;
            } else {
                t  = absxk / scale;
                y += t * t;
            }
        }

        y = scale * sqrtf(y);
    }

    return y;
}

//
// Arguments    : int n
// const float x_data[]
// int ix0
// Return Type  : float
//
float d_xnrm2(int n, const float x_data[], int ix0)
{
    float y;
    float scale;
    int kend;
    int k;
    float absxk;
    float t;

    y = 0.0F;
    if (n < 1) {} else if (n == 1) {
        y = fabsf(x_data[ix0 - 1]);
    } else {
        scale = 1.17549435E-38F;
        kend  = (ix0 + n) - 1;
        for (k = ix0; k <= kend; k++) {
            absxk = fabsf(x_data[k - 1]);
            if (absxk > scale) {
                t     = scale / absxk;
                y     = 1.0F + y * t * t;
                scale = absxk;
            } else {
                t  = absxk / scale;
                y += t * t;
            }
        }

        y = scale * sqrtf(y);
    }

    return y;
}

//
// Arguments    : int n
// const float x_data[]
// int ix0
// Return Type  : float
//
float xnrm2(int n, const float x_data[], int ix0)
{
    float y;
    float scale;
    int kend;
    int k;
    float absxk;
    float t;

    y = 0.0F;
    if (n < 1) {} else if (n == 1) {
        y = fabsf(x_data[ix0 - 1]);
    } else {
        scale = 1.17549435E-38F;
        kend  = (ix0 + n) - 1;
        for (k = ix0; k <= kend; k++) {
            absxk = fabsf(x_data[k - 1]);
            if (absxk > scale) {
                t     = scale / absxk;
                y     = 1.0F + y * t * t;
                scale = absxk;
            } else {
                t  = absxk / scale;
                y += t * t;
            }
        }

        y = scale * sqrtf(y);
    }

    return y;
}

//
// File trailer for xnrm2.cpp
//
// [EOF]
//
