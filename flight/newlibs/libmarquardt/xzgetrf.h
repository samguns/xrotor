//
// File: xzgetrf.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __XZGETRF_H__
#define __XZGETRF_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void xzgetrf(int m, int n, float A_data[], int A_size[2], int lda, int
                    ipiv_data[], int ipiv_size[2], int *info);

#endif

//
// File trailer for xzgetrf.h
//
// [EOF]
//
