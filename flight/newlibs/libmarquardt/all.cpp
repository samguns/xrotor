//
// File: all.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "all.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : const float x_data[]
// boolean_T y_data[]
// int y_size[2]
// Return Type  : void
//
void all(const float x_data[], boolean_T y_data[], int y_size[2])
{
    int ix;
    boolean_T exitg1;

    y_size[0] = 1;
    y_size[1] = 1;
    y_data[0] = true;
    ix = 1;
    exitg1    = false;
    while ((!exitg1) && (ix <= 1)) {
        if (is_zero(x_data[0])) {
            y_data[0] = false;
            exitg1    = true;
        } else {
            ix = 2;
        }
    }
}

//
// Arguments    : const boolean_T x_data[]
// Return Type  : boolean_T
//
boolean_T b_all(const boolean_T x_data[])
{
    boolean_T y;
    int ix;
    boolean_T exitg1;

    y      = true;
    ix     = 1;
    exitg1 = false;
    while ((!exitg1) && (ix <= 1)) {
        if (!x_data[0]) {
            y = false;
            exitg1 = true;
        } else {
            ix = 2;
        }
    }

    return y;
}

//
// File trailer for all.cpp
//
// [EOF]
//
