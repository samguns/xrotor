//
// File: norm.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "libmarquardt.h"
#include "rt_nonfinite.h"
#include "norm.h"
#include "svd.h"

// Function Definitions

//
// Arguments    : const float x_data[]
// Return Type  : float
//
float b_norm(const float x_data[], int size)
{
    float y;
    int k;
    boolean_T exitg1;
    float absx;

    y = 0.0F;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= (size - 1))) {
        absx = fabsf(x_data[k]);
        if (rtIsNaNF(absx)) {
            y = ((real32_T)rtNaNF);
            exitg1 = true;
        } else {
            if (absx > y) {
                y = absx;
            }

            k++;
        }
    }

    return y;
}

//
// Arguments    : const float x_data[]
// const int x_size[1]
// Return Type  : float
//
float c_norm(const float x_data[], const int x_size[1])
{
    float y;
    int k;
    boolean_T exitg1;
    float absx;

    y = 0.0F;
    k = 0;
    exitg1 = false;
    while ((!exitg1) && (k <= x_size[0] - 1)) {
        absx = fabsf(x_data[k]);
        if (rtIsNaNF(absx)) {
            y = ((real32_T)rtNaNF);
            exitg1 = true;
        } else {
            if (absx > y) {
                y = absx;
            }

            k++;
        }
    }

    return y;
}

//
// Arguments    : const float x_data[]
// const int x_size[2]
// Return Type  : float
//
float d_norm(const float x_data[], const int x_size[2])
{
    float y;
    int j;
    int i;
    boolean_T exitg1;
    float s;

    if ((x_size[0] == 0) || (x_size[1] == 0)) {
        y = 0.0F;
    } else if ((x_size[0] == 1) || (x_size[1] == 1)) {
        y = 0.0F;
        j = x_size[0] * x_size[1];
        for (i = 0; i < j; i++) {
            y += fabsf(x_data[i]);
        }
    } else {
        y = 0.0F;
        j = 0;
        exitg1 = false;
        while ((!exitg1) && (j <= x_size[1] - 1)) {
            s = 0.0F;
            for (i = 0; i < x_size[0]; i++) {
                s += fabsf(x_data[i + x_size[0] * j]);
            }

            if (rtIsNaNF(s)) {
                y = ((real32_T)rtNaNF);
                exitg1 = true;
            } else {
                if (s > y) {
                    y = s;
                }

                j++;
            }
        }
    }

    return y;
}

//
// Arguments    : const float x_data[]
// const int x_size[2]
// Return Type  : float
//
float e_norm(const float x_data[], const int x_size[2])
{
    float y;
    int n;
    float absx;
    int k;
    float absxk;
    float t;
    boolean_T exitg1;
    int s_size[1];
    float s_data[MARQUARDT_MAX_NUMOBS];

    if (x_size[0] == 0) {
        y = 0.0F;
    } else if ((x_size[0] == 1) || (x_size[1] == 1)) {
        n = x_size[0] * x_size[1];
        y = 0.0F;
        if (n < 1) {} else if (n == 1) {
            y = fabsf(x_data[0]);
        } else {
            absx = 1.17549435E-38F;
            for (k = 1; k <= n; k++) {
                absxk = fabsf(x_data[k - 1]);
                if (absxk > absx) {
                    t    = absx / absxk;
                    y    = 1.0F + y * t * t;
                    absx = absxk;
                } else {
                    t  = absxk / absx;
                    y += t * t;
                }
            }

            y = absx * sqrtf(y);
        }
    } else {
        y = 0.0F;
        n = x_size[0] * x_size[1];
        k = 0;
        exitg1 = false;
        while ((!exitg1) && (k <= n - 1)) {
            absx = fabsf(x_data[k]);
            if (rtIsNaNF(absx)) {
                y = ((real32_T)rtNaNF);
                exitg1 = true;
            } else {
                if (absx > y) {
                    y = absx;
                }

                k++;
            }
        }

        if ((!rtIsInfF(y)) && (!rtIsNaNF(y))) {
            svd(x_data, x_size, s_data, s_size);
            y = s_data[0];
        }
    }

    return y;
}

//
// Arguments    : const float x[12]
// Return Type  : float
//
float norm(const float x[], int size)
{
    float y;
    float scale;
    int k;
    float absxk;
    float t;

    y     = 0.0F;
    scale = 1.17549435E-38F;
    for (k = 0; k < size; k++) {
        absxk = fabsf(x[k]);
        if (absxk > scale) {
            t     = scale / absxk;
            y     = 1.0F + y * t * t;
            scale = absxk;
        } else {
            t  = absxk / scale;
            y += t * t;
        }
    }

    return scale * sqrtf(y);
}

//
// File trailer for norm.cpp
//
// [EOF]
//
