//
// File: rcond1.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "libmarquardt.h"
#include "rt_nonfinite.h"
#include "rcond1.h"
#include "xtrsv.h"
#include "xzgetrf.h"
#include "norm.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : float A_data[]
// int A_size[2]
// Return Type  : float
//
float b_rcond(float A_data[], int A_size[2])
{
    float result;
    int n;
    float temp;
    float normA;
    int jjA;
    int ipiv_size[2];
    int ipiv_data[14];
    int ix;
    int exitg2;
    float ainvnm;
    int iter;
    int kase;
    int jump;
    int j;
    int x_size_idx_0;
    int i1;
    float x_data[MARQUARDT_MAX_NUMOBS];
    int exitg1;
    int b_j;
    int i;
    float absrexk;

    n = A_size[0];
    x_size_idx_0 = (signed char)n;
    result = 0.0F;
    if (A_size[0] == 0) {
        result = ((real32_T)rtInfF);
    } else if (A_size[0] == 1) {
        temp = fabsf(A_data[0]);
        if (rtIsInfF(temp)) {} else if (!((!rtIsInfF(temp)) && (!rtIsNaNF(temp)))) {
            result = ((real32_T)rtNaNF);
        } else {
            if (!is_zero(temp)) {
                result = 1.0F;
            }
        }
    } else {
        normA = d_norm(A_data, A_size);
        if (!is_zero(normA)) {
            xzgetrf(A_size[0], A_size[0], A_data, A_size, A_size[0], ipiv_data, ipiv_size, &jjA);
            ix = n;
            do {
                exitg2 = 0;
                if (ix > 0) {
                    if (is_zero(A_data[(ix + A_size[0] * (ix - 1)) - 1])) {
                        exitg2 = 1;
                    } else {
                        ix--;
                    }
                } else {
                    ainvnm = 0.0F;
                    iter   = 2;
                    kase   = 1;
                    jump   = 1;
                    j = 0;
                    x_size_idx_0 = (signed char)n;
                    jjA    = (signed char)n;
                    for (i1 = 0; i1 < jjA; i1++) {
                        x_data[i1] = (float)(1.0F / (float)n);
                    }

                    exitg2 = 2;
                }
            } while (exitg2 == 0);

            if (exitg2 == 1) {} else {
                do {
                    exitg1 = 0;
                    if (kase != 0) {
                        if (kase == 1) {
                            if ((A_size[0] == 0) || (A_size[1] == 0)) {} else {
                                for (b_j = 0; b_j + 1 <= n; b_j++) {
                                    jjA = (b_j + b_j * n) + 1;
                                    i1  = (n - b_j) - 1;
                                    for (i = 1; i <= i1; i++) {
                                        ix = b_j + i;
                                        x_data[ix] -= x_data[b_j] * A_data[(jjA + i) - 1];
                                    }
                                }
                            }

                            xtrsv(n, A_data, A_size, n, x_data);
                        } else {
                            if ((A_size[0] == 0) || (A_size[1] == 0)) {} else {
                                for (b_j = 0; b_j + 1 <= n; b_j++) {
                                    jjA  = b_j * n;
                                    temp = x_data[b_j];
                                    for (i = 1; i <= b_j; i++) {
                                        temp -= A_data[(jjA + i) - 1] * x_data[i - 1];
                                    }

                                    temp /= A_data[jjA + b_j];
                                    x_data[b_j] = temp;
                                }
                            }

                            if ((A_size[0] == 0) || (A_size[1] == 0)) {} else {
                                for (b_j = n - 1; b_j + 1 > 0; b_j--) {
                                    jjA  = b_j * n;
                                    temp = x_data[b_j];
                                    for (i = n; i >= b_j + 2; i--) {
                                        temp -= A_data[(jjA + i) - 1] * x_data[i - 1];
                                    }

                                    x_data[b_j] = temp;
                                }
                            }
                        }

                        if (jump == 1) {
                            ainvnm = 0.0F;
                            for (ix = 0; ix < x_size_idx_0; ix++) {
                                ainvnm += fabsf(x_data[ix]);
                            }

                            if (!((!rtIsInfF(ainvnm)) && (!rtIsNaNF(ainvnm)))) {
                                result = ainvnm;
                                exitg1 = 1;
                            } else {
                                for (ix = 0; ix < x_size_idx_0; ix++) {
                                    temp = fabsf(x_data[ix]);
                                    if (temp > 1.17549435E-38F) {
                                        x_data[ix] /= temp;
                                    } else {
                                        x_data[ix] = 1.0F;
                                    }
                                }

                                kase = 2;
                                jump = 2;
                            }
                        } else if (jump == 2) {
                            j    = 0;
                            temp = fabsf(x_data[0]);
                            for (ix = 1; ix - 1 <= x_size_idx_0 - 2; ix++) {
                                absrexk = fabsf(x_data[ix]);
                                if (absrexk <= temp) {} else {
                                    j    = ix;
                                    temp = absrexk;
                                }
                            }

                            iter = 2;
                            for (i1 = 0; i1 < x_size_idx_0; i1++) {
                                x_data[i1] = 0.0F;
                            }

                            x_data[j] = 1.0F;
                            kase = 1;
                            jump = 3;
                        } else if (jump == 3) {
                            ainvnm = 0.0F;
                            for (ix = 0; ix < x_size_idx_0; ix++) {
                                ainvnm += fabsf(x_data[ix]);
                            }

                            if (ainvnm <= x_data[0]) {
                                temp = 1.0F;
                                for (ix = 0; ix < x_size_idx_0; ix++) {
                                    x_data[ix] = temp * (1.0F + ((1.0F + (float)ix) - 1.0F) /
                                                         ((float)x_size_idx_0 - 1.0F));
                                    temp = -temp;
                                }

                                kase = 1;
                                jump = 5;
                            } else {
                                for (ix = 0; ix < x_size_idx_0; ix++) {
                                    temp = fabsf(x_data[ix]);
                                    if (temp > 1.17549435E-38F) {
                                        x_data[ix] /= temp;
                                    } else {
                                        x_data[ix] = 1.0F;
                                    }
                                }

                                kase = 2;
                                jump = 4;
                            }
                        } else if (jump == 4) {
                            jjA  = j;
                            j    = 0;
                            temp = fabsf(x_data[0]);
                            for (ix = 1; ix - 1 <= x_size_idx_0 - 2; ix++) {
                                absrexk = fabsf(x_data[ix]);
                                if (absrexk <= temp) {} else {
                                    j    = ix;
                                    temp = absrexk;
                                }
                            }

                            if ((!is_equal(fabsf(x_data[jjA]), fabsf(x_data[j]))) && (iter <= 5)) {
                                iter++;
                                for (i1 = 0; i1 < x_size_idx_0; i1++) {
                                    x_data[i1] = 0.0F;
                                }

                                x_data[j] = 1.0F;
                                kase = 1;
                                jump = 3;
                            } else {
                                temp = 1.0F;
                                for (ix = 0; ix < x_size_idx_0; ix++) {
                                    x_data[ix] = temp * (1.0F + ((1.0F + (float)ix) - 1.0F) /
                                                         ((float)x_size_idx_0 - 1.0F));
                                    temp = -temp;
                                }

                                kase = 1;
                                jump = 5;
                            }
                        } else {
                            if (jump == 5) {
                                temp = 0.0F;
                                for (ix = 0; ix < x_size_idx_0; ix++) {
                                    temp += fabsf(x_data[ix]);
                                }

                                temp = 2.0F * temp / 3.0F / (float)n;
                                if (temp > ainvnm) {
                                    ainvnm = temp;
                                }

                                kase = 0;
                            }
                        }
                    } else {
                        if (!is_zero(ainvnm)) {
                            result = 1.0F / ainvnm / normA;
                        }

                        exitg1 = 1;
                    }
                } while (exitg1 == 0);
            }
        }
    }

    return result;
}

//
// File trailer for rcond1.cpp
//
// [EOF]
//
