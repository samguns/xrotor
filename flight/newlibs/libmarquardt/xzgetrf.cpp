//
// File: xzgetrf.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "xzgetrf.h"
#include "colon.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : int m
// int n
// float A_data[]
// int A_size[2]
// int lda
// int ipiv_data[]
// int ipiv_size[2]
// int *info
// Return Type  : void
//
void xzgetrf(int m, int n, float A_data[], int A_size[2], int lda, int
             ipiv_data[], int ipiv_size[2], int *info)
{
    int b_m;
    int i4;
    int j;
    int mmj;
    int c;
    int iy;
    int ix;
    float smax;
    int jA;
    float s;
    int i5;
    int jy;
    int b_j;
    int ijA;

    if (m <= n) {
        b_m = m;
    } else {
        b_m = n;
    }

    eml_signed_integer_colon(b_m, ipiv_data, ipiv_size);
    *info = 0;
    if ((m < 1) || (n < 1)) {} else {
        if (m - 1 <= n) {
            i4 = m - 1;
        } else {
            i4 = n;
        }

        for (j = 0; j + 1 <= i4; j++) {
            mmj = m - j;
            c   = j * (lda + 1);
            if (mmj < 1) {
                iy = -1;
            } else {
                iy = 0;
                if (mmj > 1) {
                    ix   = c;
                    smax = fabsf(A_data[c]);
                    for (jA = 1; jA + 1 <= mmj; jA++) {
                        ix++;
                        s = fabsf(A_data[ix]);
                        if (s > smax) {
                            iy   = jA;
                            smax = s;
                        }
                    }
                }
            }

            if (!is_zero(A_data[c + iy])) {
                if (iy != 0) {
                    ipiv_data[j] = (j + iy) + 1;
                    ix  = j;
                    iy += j;
                    for (jA = 1; jA <= n; jA++) {
                        smax = A_data[ix];
                        A_data[ix] = A_data[iy];
                        A_data[iy] = smax;
                        ix  += lda;
                        iy  += lda;
                    }
                }

                i5 = c + mmj;
                for (iy = c + 1; iy + 1 <= i5; iy++) {
                    A_data[iy] /= A_data[c];
                }
            } else {
                *info = j + 1;
            }

            iy = (n - j) - 1;
            jA = c + lda;
            jy = c + lda;
            for (b_j = 1; b_j <= iy; b_j++) {
                smax = A_data[jy];
                if (!is_zero(A_data[jy])) {
                    ix = c + 1;
                    i5 = mmj + jA;
                    for (ijA = 1 + jA; ijA + 1 <= i5; ijA++) {
                        A_data[ijA] += A_data[ix] * -smax;
                        ix++;
                    }
                }

                jy += lda;
                jA += lda;
            }
        }

        if ((*info == 0) && (m <= n) && (!(!is_zero(A_data[(m + A_size[0] * (m - 1)) - 1])))) {
            *info = m;
        }
    }
}

//
// File trailer for xzgetrf.cpp
//
// [EOF]
//
