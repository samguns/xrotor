//
// File: xrotg.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "xrotg.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : float *a
// float *b
// float *c
// float *s
// Return Type  : void
//
void xrotg(float *a, float *b, float *c, float *s)
{
    float roe;
    float absa;
    float absb;
    float scale;
    float ads;
    float bds;

    roe  = *b;
    absa = fabsf(*a);
    absb = fabsf(*b);
    if (absa > absb) {
        roe = *a;
    }

    scale = absa + absb;
    if (is_zero(scale)) {
        *s    = 0.0F;
        *c    = 1.0F;
        scale = 0.0F;
        *b    = 0.0F;
    } else {
        ads    = absa / scale;
        bds    = absb / scale;
        scale *= sqrtf(ads * ads + bds * bds);
        if (roe < 0.0F) {
            scale = -scale;
        }

        *c = *a / scale;
        *s = *b / scale;
        if (absa > absb) {
            *b = *s;
        } else if (!is_zero(*c)) {
            *b = 1.0F / *c;
        } else {
            *b = 1.0F;
        }
    }

    *a = scale;
}

//
// File trailer for xrotg.cpp
//
// [EOF]
//
