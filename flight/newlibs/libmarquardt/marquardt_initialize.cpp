//
// File: autocalibrationAsymmetric_initialize.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "marquardt_initialize.h"

// Function Definitions

//
// Arguments    : void
// Return Type  : void
//
void marquardt_initialize()
{
    rt_InitInfAndNaN(8U);
}

//
// File trailer for autocalibrationAsymmetric_initialize.cpp
//
// [EOF]
//
