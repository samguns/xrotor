//
// File: xaxpy.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __XAXPY_H__
#define __XAXPY_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void b_xaxpy(int n, float a, const float x_data[], int ix0, float y_data[],
                    int iy0);
extern void c_xaxpy(int n, float a, const float x_data[], int ix0, float y_data[],
                    int iy0);
extern void xaxpy(int n, float a, int ix0, float y_data[], int iy0);

#endif

//
// File trailer for xaxpy.h
//
// [EOF]
//
