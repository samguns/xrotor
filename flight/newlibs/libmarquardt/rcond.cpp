//
// File: rcond.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 04-Jan-2016 21:30:06
//

// Include Files
#include "rt_nonfinite.h"
#include "rcond.h"
#include "rcond1.h"

// Function Definitions

//
// Arguments    : const float A_data[]
// const int A_size[2]
// Return Type  : float
//
float rcond(const float A_data[], const int A_size[2])
{
    float b_A_data[196];
    int b_A_size[2];
    int loop_ub;
    int i0;

    b_A_size[0] = A_size[0];
    b_A_size[1] = A_size[1];
    loop_ub     = A_size[0] * A_size[1];
    for (i0 = 0; i0 < loop_ub; i0++) {
        b_A_data[i0] = A_data[i0];
    }

    return b_rcond(b_A_data, b_A_size);
}

//
// File trailer for rcond.cpp
//
// [EOF]
//
