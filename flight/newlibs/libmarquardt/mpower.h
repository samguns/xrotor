//
// File: mpower.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 04-Jan-2016 21:30:06
//
#ifndef __MPOWER_H__
#define __MPOWER_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void mpower(const float a_data[], float c_data[], int c_size[2]);

#endif

//
// File trailer for mpower.h
//
// [EOF]
//
