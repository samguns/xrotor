//
// File: xdotc.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//

// Include Files
#include "rt_nonfinite.h"
#include "xdotc.h"

// Function Definitions

//
// Arguments    : int n
// const float x_data[]
// int ix0
// const float y_data[]
// int iy0
// Return Type  : float
//
float xdotc(int n, const float x_data[], int ix0, const float y_data[], int iy0)
{
    float d;
    int ix;
    int iy;
    int k;

    d = 0.0F;
    if (n < 1) {} else {
        ix = ix0;
        iy = iy0;
        for (k = 1; k <= n; k++) {
            d += x_data[ix - 1] * y_data[iy - 1];
            ix++;
            iy++;
        }
    }

    return d;
}

//
// File trailer for xdotc.cpp
//
// [EOF]
//
