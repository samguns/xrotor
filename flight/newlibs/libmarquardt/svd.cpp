//
// File: svd.cpp
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 04-Jan-2016 21:30:06
//

// Include Files
#include "libmarquardt.h"
#include "rt_nonfinite.h"
#include "svd.h"
#include "xaxpy.h"
#include "xdotc.h"
#include "xnrm2.h"
#include "xrotg.h"
#include "mathmisc.h"

// Function Definitions

//
// Arguments    : const float A_data[]
// const int A_size[2]
// float U_data[]
// int U_size[1]
// Return Type  : void
//
void svd(const float A_data[], const int A_size[2], float U_data[], int U_size[1])
{
    int kase;
    int m;
    float *b_A_data = 0;
    int n;
    int p;
    int minnp;
    float s_data[MARQUARDT_MAX_NUMOBS];
    float e_data[MARQUARDT_MAX_NUMOBS];
    float work_data[MARQUARDT_MAX_NUMOBS];
    int nrt;
    int nct;
    int q;
    int iter;
    int mm;
    boolean_T apply_transform;
    float ztest0;
    int qs;
    float ztest;
    float snorm;
    boolean_T exitg3;
    boolean_T exitg2;
    float f;
    float b;
    float varargin_1[5];
    float mtmp;
    boolean_T exitg1;
    float sqds;

    kase     = A_size[0] * A_size[1];
    b_A_data = (float *)pios_malloc(sizeof(float) * kase);
    for (m = 0; m < kase; m++) {
        b_A_data[m] = A_data[m];
    }

    n = A_size[0];
    p = A_size[1];
    if (A_size[0] <= A_size[1]) {
        minnp = A_size[0];
    } else {
        minnp = A_size[1];
    }

    if (A_size[0] + 1 <= A_size[1]) {
        kase = (signed char)(A_size[0] + 1);
    } else {
        kase = (signed char)A_size[1];
    }

    for (m = 0; m < kase; m++) {
        s_data[m] = 0.0F;
    }

    kase = (signed char)A_size[1];
    for (m = 0; m < kase; m++) {
        e_data[m] = 0.0F;
    }

    kase = (signed char)A_size[0];
    for (m = 0; m < kase; m++) {
        work_data[m] = 0.0F;
    }

    if (A_size[0] == 0) {} else {
        if (A_size[1] < 2) {
            kase = 0;
        } else {
            kase = A_size[1] - 2;
        }

        if (kase <= A_size[0]) {
            nrt = kase;
        } else {
            nrt = A_size[0];
        }

        if (A_size[0] - 1 <= A_size[1]) {
            nct = A_size[0] - 1;
        } else {
            nct = A_size[1];
        }

        if (nct >= nrt) {
            m = nct;
        } else {
            m = nrt;
        }

        for (q = 1; q <= m; q++) {
            iter = q + n * (q - 1);
            mm   = n - q;
            apply_transform = false;
            if (q <= nct) {
                ztest0 = c_xnrm2(mm + 1, b_A_data, iter);
                if (ztest0 > 0.0F) {
                    apply_transform = true;
                    if (b_A_data[iter - 1] < 0.0F) {
                        s_data[q - 1] = -ztest0;
                    } else {
                        s_data[q - 1] = ztest0;
                    }

                    if (fabsf(s_data[q - 1]) >= 9.86076132E-32F) {
                        ztest0 = 1.0F / s_data[q - 1];
                        kase   = iter + mm;
                        for (qs = iter; qs <= kase; qs++) {
                            b_A_data[qs - 1] *= ztest0;
                        }
                    } else {
                        kase = iter + mm;
                        for (qs = iter; qs <= kase; qs++) {
                            b_A_data[qs - 1] /= s_data[q - 1];
                        }
                    }

                    b_A_data[iter - 1]++;
                    s_data[q - 1] = -s_data[q - 1];
                } else {
                    s_data[q - 1] = 0.0F;
                }
            }

            for (kase = q; kase + 1 <= p; kase++) {
                qs = q + n * kase;
                if (apply_transform) {
                    ztest0 = xdotc(mm + 1, b_A_data, iter, b_A_data, qs);
                    xaxpy(mm + 1, -(ztest0 / b_A_data[(q + A_size[0] * (q - 1)) - 1]),
                          iter, b_A_data, qs);
                }

                e_data[kase] = b_A_data[qs - 1];
            }

            if (q <= nrt) {
                kase   = p - q;
                ztest0 = d_xnrm2(kase, e_data, q + 1);
                if (is_zero(ztest0)) {
                    e_data[q - 1] = 0.0F;
                } else {
                    if (e_data[q] < 0.0F) {
                        e_data[q - 1] = -ztest0;
                    } else {
                        e_data[q - 1] = ztest0;
                    }

                    ztest0 = e_data[q - 1];
                    if (fabsf(e_data[q - 1]) >= 9.86076132E-32F) {
                        ztest0 = 1.0F / e_data[q - 1];
                        kase  += q;
                        for (qs = q; qs + 1 <= kase; qs++) {
                            e_data[qs] *= ztest0;
                        }
                    } else {
                        kase += q;
                        for (qs = q; qs + 1 <= kase; qs++) {
                            e_data[qs] /= ztest0;
                        }
                    }

                    e_data[q]++;
                    e_data[q - 1] = -e_data[q - 1];
                    if (q + 1 <= n) {
                        for (kase = q; kase + 1 <= n; kase++) {
                            work_data[kase] = 0.0F;
                        }

                        for (kase = q; kase + 1 <= p; kase++) {
                            b_xaxpy(mm, e_data[kase], b_A_data, (q + n * kase) + 1, work_data,
                                    q + 1);
                        }

                        for (kase = q; kase + 1 <= p; kase++) {
                            c_xaxpy(mm, -e_data[kase] / e_data[q], work_data, q + 1, b_A_data,
                                    (q + n * kase) + 1);
                        }
                    }
                }
            }
        }

        if (A_size[1] <= A_size[0] + 1) {
            m = A_size[1];
        } else {
            m = A_size[0] + 1;
        }

        if (nct < A_size[1]) {
            s_data[nct] = b_A_data[nct + A_size[0] * nct];
        }

        if (A_size[0] < m) {
            s_data[m - 1] = 0.0F;
        }

        if (nrt + 1 < m) {
            e_data[nrt] = b_A_data[nrt + A_size[0] * (m - 1)];
        }

        e_data[m - 1] = 0.0F;
        for (q = 0; q + 1 <= m; q++) {
            if (!is_zero(s_data[q])) {
                ztest     = fabsf(s_data[q]);
                ztest0    = s_data[q] / ztest;
                s_data[q] = ztest;
                if (q + 1 < m) {
                    e_data[q] /= ztest0;
                }
            }

            if ((q + 1 < m) && (!is_zero(e_data[q]))) {
                ztest          = fabsf(e_data[q]);
                ztest0         = e_data[q];
                e_data[q]      = ztest;
                s_data[q + 1] *= ztest / ztest0;
            }
        }

        mm    = m;
        iter  = 0;
        snorm = 0.0F;
        for (kase = 0; kase + 1 <= m; kase++) {
            snorm = fmaxf(snorm, fmaxf(fabsf(s_data[kase]), fabsf(e_data[kase])));
        }

        while ((m > 0) && (!(iter >= 75))) {
            q = m - 1;
            exitg3 = false;
            while (!(exitg3 || (q == 0))) {
                ztest0 = fabsf(e_data[q - 1]);
                if ((ztest0 <= 1.1920929E-7F * (fabsf(s_data[q - 1]) + fabsf(s_data[q])))
                    || (ztest0 <= 9.86076132E-32F) || ((iter > 20) && (ztest0 <=
                                                                       1.1920929E-7F * snorm))) {
                    e_data[q - 1] = 0.0F;
                    exitg3 = true;
                } else {
                    q--;
                }
            }

            if (q == m - 1) {
                kase = 4;
            } else {
                qs     = m;
                kase   = m;
                exitg2 = false;
                while ((!exitg2) && (kase >= q)) {
                    qs = kase;
                    if (kase == q) {
                        exitg2 = true;
                    } else {
                        ztest0 = 0.0F;
                        if (kase < m) {
                            ztest0 = fabsf(e_data[kase - 1]);
                        }

                        if (kase > q + 1) {
                            ztest0 += fabsf(e_data[kase - 2]);
                        }

                        ztest = fabsf(s_data[kase - 1]);
                        if ((ztest <= 1.1920929E-7F * ztest0) || (ztest <= 9.86076132E-32F)) {
                            s_data[kase - 1] = 0.0F;
                            exitg2 = true;
                        } else {
                            kase--;
                        }
                    }
                }

                if (qs == q) {
                    kase = 3;
                } else if (qs == m) {
                    kase = 1;
                } else {
                    kase = 2;
                    q    = qs;
                }
            }

            switch (kase) {
            case 1:
                f = e_data[m - 2];
                e_data[m - 2] = 0.0F;
                for (qs = m - 3; qs + 2 >= q + 1; qs--) {
                    ztest0 = s_data[qs + 1];
                    xrotg(&ztest0, &f, &ztest, &b);
                    s_data[qs + 1] = ztest0;
                    if (qs + 2 > q + 1) {
                        f = -b * e_data[qs];
                        e_data[qs] *= ztest;
                    }
                }
                break;

            case 2:
                f = e_data[q - 1];
                e_data[q - 1] = 0.0F;
                while (q + 1 <= m) {
                    xrotg(&s_data[q], &f, &ztest, &b);
                    f = -b * e_data[q];
                    e_data[q] *= ztest;
                    q++;
                }
                break;

            case 3:
                varargin_1[0] = fabsf(s_data[m - 1]);
                varargin_1[1] = fabsf(s_data[m - 2]);
                varargin_1[2] = fabsf(e_data[m - 2]);
                varargin_1[3] = fabsf(s_data[q]);
                varargin_1[4] = fabsf(e_data[q]);
                kase = 1;
                mtmp = varargin_1[0];
                if (rtIsNaNF(varargin_1[0])) {
                    qs     = 2;
                    exitg1 = false;
                    while ((!exitg1) && (qs < 6)) {
                        kase = qs;
                        if (!rtIsNaNF(varargin_1[qs - 1])) {
                            mtmp   = varargin_1[qs - 1];
                            exitg1 = true;
                        } else {
                            qs++;
                        }
                    }
                }

                if (kase < 5) {
                    while (kase + 1 < 6) {
                        if (varargin_1[kase] > mtmp) {
                            mtmp = varargin_1[kase];
                        }

                        kase++;
                    }
                }

                f       = s_data[m - 1] / mtmp;
                ztest0  = s_data[m - 2] / mtmp;
                ztest   = e_data[m - 2] / mtmp;
                sqds    = s_data[q] / mtmp;
                b       = ((ztest0 + f) * (ztest0 - f) + ztest * ztest) / 2.0F;
                ztest0  = f * ztest;
                ztest0 *= ztest0;
                if ((!is_zero(b)) || (!is_zero(ztest0))) {
                    ztest = sqrtf(b * b + ztest0);
                    if (b < 0.0F) {
                        ztest = -ztest;
                    }

                    ztest = ztest0 / (b + ztest);
                } else {
                    ztest = 0.0F;
                }

                f = (sqds + f) * (sqds - f) + ztest;
                ztest0 = sqds * (e_data[q] / mtmp);
                for (qs = q + 1; qs < m; qs++) {
                    xrotg(&f, &ztest0, &ztest, &b);
                    if (qs > q + 1) {
                        e_data[qs - 2] = f;
                    }

                    f = ztest * s_data[qs - 1] + b * e_data[qs - 1];
                    e_data[qs - 1] = ztest * e_data[qs - 1] - b * s_data[qs - 1];
                    ztest0 = b * s_data[qs];
                    s_data[qs]    *= ztest;
                    s_data[qs - 1] = f;
                    xrotg(&s_data[qs - 1], &ztest0, &ztest, &b);
                    f = ztest * e_data[qs - 1] + b * s_data[qs];
                    s_data[qs]     = -b * e_data[qs - 1] + ztest * s_data[qs];
                    ztest0 = b * e_data[qs];
                    e_data[qs]    *= ztest;
                }

                e_data[m - 2] = f;
                iter++;
                break;

            default:
                if (s_data[q] < 0.0F) {
                    s_data[q] = -s_data[q];
                }

                kase = q + 1;
                while ((q + 1 < mm) && (s_data[q] < s_data[kase])) {
                    ztest        = s_data[q];
                    s_data[q]    = s_data[kase];
                    s_data[kase] = ztest;
                    q = kase;
                    kase++;
                }

                iter = 0;
                m--;
                break;
            }
        }
    }

    U_size[0] = minnp;
    for (qs = 0; qs + 1 <= minnp; qs++) {
        U_data[qs] = s_data[qs];
    }

    pios_free(b_A_data);
    b_A_data = 0;
}

//
// File trailer for svd.cpp
//
// [EOF]
//
