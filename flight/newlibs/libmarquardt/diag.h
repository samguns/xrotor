//
// File: diag.h
//
// MATLAB Coder version            : 3.0
// C/C++ source code generated on  : 03-Jan-2016 09:45:08
//
#ifndef __DIAG_H__
#define __DIAG_H__

// Include Files
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rt_nonfinite.h"
#include "rtwtypes.h"

// Function Declarations
extern void diag(const float v_data[], const int v_size[2], float d_data[], int d_size[1]);

#endif

//
// File trailer for diag.h
//
// [EOF]
//
