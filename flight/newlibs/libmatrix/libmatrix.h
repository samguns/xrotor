/*
 *    Project XFlight libmatrix header file
 *
 *    Copyright (C) 2016 NextGenRotors.
 *
 */

#ifndef LIBMATRIX_H
#define LIBMATRIX_H

#include "vector2.h"
#include "vector3.h"
#include "matrix3.h"
#include "matrix.h"
#include "quaternion.h"

#endif // LIBMATRIX
