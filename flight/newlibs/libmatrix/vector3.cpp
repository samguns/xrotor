#include "project_xflight.h"
#include "vector3.h"


#define HALF_SQRT_2 0.70710678118654757f


// vector cross product
template <typename T>
Vector3<T> Vector3<T>::operator %(const Vector3<T> &v) const
{
    Vector3<T> temp(y * v.z - z * v.y, z * v.x - x * v.z, x * v.y - y * v.x);
    return temp;
}

// dot product
template <typename T>
T Vector3<T>::operator *(const Vector3<T> &v) const
{
    return x * v.x + y * v.y + z * v.z;
}

template <typename T>
float Vector3<T>::length(void) const
{
    return sqrtf(x * x + y * y + z * z);
}

template <typename T>
float Vector3<T>::squarednorm(void) const
{
    return x * x + y * y + z * z;
}

template <typename T>
T Vector3<T>::difference(const Vector3<T> &v) const
{
    return fabs(x - v.x) + fabs(y - v.y) + fabs(z - v.z);
}

template <typename T>
void Vector3<T>::squareroot(void)
{
    x = sqrtf(x);
    y = sqrtf(y);
    z = sqrtf(z);
}

template <typename T>
void Vector3<T>::norm2(void)
{
    x = x * x;
    y = y * y;
    z = z * z;
}

template <typename T>
Vector3<T> &Vector3<T>::operator *=(const T num)
{
    x *= num; y *= num; z *= num;
    return *this;
}

template <typename T>
Vector3<T> &Vector3<T>::operator /=(const T num)
{
    x /= num; y /= num; z /= num;
    return *this;
}

template <typename T>
Vector3<T> &Vector3<T>::operator -=(const Vector3<T> &v)
{
    x -= v.x; y -= v.y; z -= v.z;
    return *this;
}

template <typename T>
bool Vector3<T>::is_nan(void) const
{
    return isnan(x) || isnan(y) || isnan(z);
}

template <typename T>
bool Vector3<T>::is_inf(void) const
{
    return isinf(x) || isinf(y) || isinf(z);
}

template <typename T>
Vector3<T> &Vector3<T>::operator +=(const Vector3<T> &v)
{
    x += v.x; y += v.y; z += v.z;
    return *this;
}

template <typename T>
Vector3<T> Vector3<T>::operator /(const T num) const
{
    return Vector3<T>(x / num, y / num, z / num);
}

template <typename T>
Vector3<T> Vector3<T>::operator *(const T num) const
{
    return Vector3<T>(x * num, y * num, z * num);
}

template <typename T>
Vector3<T> Vector3<T>::operator -(const Vector3<T> &v) const
{
    return Vector3<T>(x - v.x, y - v.y, z - v.z);
}

template <typename T>
Vector3<T> Vector3<T>::operator +(const Vector3<T> &v) const
{
    return Vector3<T>(x + v.x, y + v.y, z + v.z);
}

template <typename T>
Vector3<T> Vector3<T>::operator -(void) const
{
    return Vector3<T>(-x, -y, -z);
}

template <typename T>
bool Vector3<T>::operator ==(const Vector3<T> &v) const
{
    return is_equal(x, v.x) && is_equal(y, v.y) && is_equal(z, v.z);
}

template <typename T>
bool Vector3<T>::operator !=(const Vector3<T> &v) const
{
    return !is_equal(x, v.x) || !is_equal(y, v.y) || !is_equal(z, v.z);
}

template <typename T>
float Vector3<T>::angle(const Vector3<T> &v2) const
{
    return acosf((*this) * v2) / (float)((this->length() * v2.length()));
}


// only define for float
template float Vector3<float>::difference(const Vector3<float> &v) const;
template float Vector3<float>::length(void) const;
template float Vector3<float>::squarednorm(void) const;
template void Vector3<float>::squareroot(void);
template void Vector3<float>::norm2(void);
template Vector3<float> Vector3<float>::operator %(const Vector3<float> &v) const;
template float Vector3<float>::operator *(const Vector3<float> &v) const;
template Vector3<float> &Vector3<float>::operator *=(const float num);
template Vector3<float> &Vector3<float>::operator /=(const float num);
template Vector3<float> &Vector3<float>::operator -=(const Vector3<float> &v);
template Vector3<float> &Vector3<float>::operator +=(const Vector3<float> &v);
template Vector3<float> Vector3<float>::operator /(const float num) const;
template Vector3<float> Vector3<float>::operator *(const float num) const;
template Vector3<float> Vector3<float>::operator +(const Vector3<float> &v) const;
template Vector3<float> Vector3<float>::operator -(const Vector3<float> &v) const;
template Vector3<float> Vector3<float>::operator -(void) const;
template bool Vector3<float>::operator ==(const Vector3<float> &v) const;
template bool Vector3<float>::operator !=(const Vector3<float> &v) const;
template bool Vector3<float>::is_nan(void) const;
template bool Vector3<float>::is_inf(void) const;
template float Vector3<float>::angle(const Vector3<float> &v) const;
