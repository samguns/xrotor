// Copyright (c) 2014, 2015, Freescale Semiconductor, Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of Freescale Semiconductor, Inc. nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL FREESCALE SEMICONDUCTOR, INC. BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
#ifndef ORIENTATION_H
#define ORIENTATION_H

#if defined(__cplusplus)
extern "C" {
#endif

// quaternion structure definition
struct fquaternion {
    float q0; // scalar component
    float q1; // x vector component
    float q2; // y vector component
    float q3; // z vector component
};

// vector components
#define CHX 0
#define CHY 1
#define CHZ 2

// function prototypes
void f3DOFTiltNED(float fR[][3], float fGs[]);
void f3DOFMagnetometerMatrixNED(float fR[][3], float fBc[]);
void feCompassNED(float fR[][3], float *pfDelta, float fBc[], float fGs[]);

void fLeastSquareseCompassNED(struct fquaternion *pfq, float fB, float fsinDelta, float fcosDelta,
                              float *pfDelta6DOF, float fBc[], float fGs[], float *pfQvB, float *pfQvG);

void fNEDAnglesDegFromRotationMatrix(float R[][3], float *pfPhiDeg, float *pfTheDeg, float *pfPsiDeg, float *pfRhoDeg, float *pfChiDeg);
void fQuaternionFromRotationMatrix(float R[][3], struct fquaternion *pq);
void fRotationMatrixFromQuaternion(float R[][3], const struct fquaternion *pq);
void qAeqBxC(struct fquaternion *pqA, const struct fquaternion *pqB, const struct fquaternion *pqC);
void qAeqAxB(struct fquaternion *pqA, const struct fquaternion *pqB);
struct fquaternion qconjgAxB(const struct fquaternion *pqA, const struct fquaternion *pqB);
void fqAeqNormqA(struct fquaternion *pqA);
void fqAeq1(struct fquaternion *pqA);
void fQuaternionFromRotationVectorDeg(struct fquaternion *pq, const float rvecdeg[], float fscaling);
void fRotationVectorDegFromQuaternion(struct fquaternion *pq, float rvecdeg[]);
void fLPFOrientationQuaternion(struct fquaternion *pq, struct fquaternion *pLPq, float flpf, float fdeltat, float fOmega[]);
void fveqconjgquq(struct fquaternion *pfq, float fu[], float fv[]);


// ****** find rotation matrix from rotation vector
void Rv2Rot(float Rv[3], float R[3][3]);

// ****** find roll, pitch, yaw from quaternion ********
void Quaternion2RPY(const float q[4], float rpy[3]);

// ****** find quaternion from roll, pitch, yaw ********
void RPY2Quaternion(const float rpy[3], float q[4]);

// ** Find Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
void Quaternion2R(float q[4], float Rbe[3][3]);

// ** Find first row of Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
// ** This vector corresponds to the fuselage/roll vector xB **
void QuaternionC2xB(const float q0, const float q1, const float q2, const float q3, float x[3]);
void Quaternion2xB(const float q[4], float x[3]);

// ** Find second row of Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
// ** This vector corresponds to the spanwise/pitch vector yB **
void QuaternionC2yB(const float q0, const float q1, const float q2, const float q3, float y[3]);
void Quaternion2yB(const float q[4], float y[3]);

// ** Find third row of Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
// ** This vector corresponds to the vertical/yaw vector zB **
void QuaternionC2zB(const float q0, const float q1, const float q2, const float q3, float z[3]);
void Quaternion2zB(const float q[4], float z[3]);


// ****** convert Rotation Matrix to Quaternion ********
// ****** if R converts from e to b, q is rotation from e to b ****
void R2Quaternion(float R[3][3], float q[4]);

// ****** Rotation Matrix from Two Vector Directions ********
// ****** given two vector directions (v1 and v2) known in two frames (b and e) find Rbe ***
// ****** solution is approximate if can't be exact ***
uint8_t RotFrom2Vectors(const float v1b[3], const float v1e[3], const float v2b[3], const float v2e[3], float Rbe[3][3]);

// ****** Vector Cross Product ********
void CrossProduct(const float v1[3], const float v2[3], float result[3]);

// ****** Vector Magnitude ********
float VectorMagnitude(const float v[3]);

void quat_inverse(float q[4]);
void quat_copy(const float q[4], float qnew[4]);
void quat_mult(const float q1[4], const float q2[4], float qout[4]);
void rot_mult(float R[3][3], const float vec[3], float vec_out[3]);
/**
 * matrix_mult_3x3f - perform a multiplication between two 3x3 float matrices
 * result = a*b
 * @param a
 * @param b
 * @param result
 */
static inline void matrix_mult_3x3f(float a[3][3], float b[3][3], float result[3][3])
{
    result[0][0] = a[0][0] * b[0][0] + a[1][0] * b[0][1] + a[2][0] * b[0][2];
    result[0][1] = a[0][1] * b[0][0] + a[1][1] * b[0][1] + a[2][1] * b[0][2];
    result[0][2] = a[0][2] * b[0][0] + a[1][2] * b[0][1] + a[2][2] * b[0][2];

    result[1][0] = a[0][0] * b[1][0] + a[1][0] * b[1][1] + a[2][0] * b[1][2];
    result[1][1] = a[0][1] * b[1][0] + a[1][1] * b[1][1] + a[2][1] * b[1][2];
    result[1][2] = a[0][2] * b[1][0] + a[1][2] * b[1][1] + a[2][2] * b[1][2];

    result[2][0] = a[0][0] * b[2][0] + a[1][0] * b[2][1] + a[2][0] * b[2][2];
    result[2][1] = a[0][1] * b[2][0] + a[1][1] * b[2][1] + a[2][1] * b[2][2];
    result[2][2] = a[0][2] * b[2][0] + a[1][2] * b[2][1] + a[2][2] * b[2][2];
}

static inline void matrix_inline_scale_3f(float a[3][3], float scale)
{
    a[0][0] *= scale;
    a[0][1] *= scale;
    a[0][2] *= scale;

    a[1][0] *= scale;
    a[1][1] *= scale;
    a[1][2] *= scale;

    a[2][0] *= scale;
    a[2][1] *= scale;
    a[2][2] *= scale;
}

static inline void rot_about_axis_x(const float rotation, float R[3][3])
{
    float s = sinf(rotation);
    float c = cosf(rotation);

    R[0][0] = 1;
    R[0][1] = 0;
    R[0][2] = 0;

    R[1][0] = 0;
    R[1][1] = c;
    R[1][2] = -s;

    R[2][0] = 0;
    R[2][1] = s;
    R[2][2] = c;
}

static inline void rot_about_axis_y(const float rotation, float R[3][3])
{
    float s = sinf(rotation);
    float c = cosf(rotation);

    R[0][0] = c;
    R[0][1] = 0;
    R[0][2] = s;

    R[1][0] = 0;
    R[1][1] = 1;
    R[1][2] = 0;

    R[2][0] = -s;
    R[2][1] = 0;
    R[2][2] = c;
}

static inline void rot_about_axis_z(const float rotation, float R[3][3])
{
    float s = sinf(rotation);
    float c = cosf(rotation);

    R[0][0] = c;
    R[0][1] = -s;
    R[0][2] = 0;

    R[1][0] = s;
    R[1][1] = c;
    R[1][2] = 0;

    R[2][0] = 0;
    R[2][1] = 0;
    R[2][2] = 1;
}

#if defined(__cplusplus)
}
#endif

#endif // #ifndef ORIENTATION_H
