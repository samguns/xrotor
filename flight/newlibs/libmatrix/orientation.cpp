// Copyright (c) 2014, 2015, Freescale Semiconductor, Inc.
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// * Redistributions of source code must retain the above copyright
// notice, this list of conditions and the following disclaimer.
// * Redistributions in binary form must reproduce the above copyright
// notice, this list of conditions and the following disclaimer in the
// documentation and/or other materials provided with the distribution.
// * Neither the name of Freescale Semiconductor, Inc. nor the
// names of its contributors may be used to endorse or promote products
// derived from this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL FREESCALE SEMICONDUCTOR, INC. BE LIABLE FOR ANY
// DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// This file contains functions designed to operate on, or compute, orientations.
// These may be in rotation matrix form, quaternion form, or Euler angles.
// It also includes functions designed to operate with specify reference frames
// (Android, Windows 8, NED).
//
#include "mathmisc.h"
#include "orientation.h"
#include "matrix.h"
#include "approximations.h"

// compile time constants that are private to this file
#define SMALLQ0      0.01F           // limit of quaternion scalar component requiring special algorithm
#define CORRUPTQUAT  0.001F      // threshold for deciding rotation quaternion is corrupt
#define SMALLMODULUS 0.01F // limit where rounding errors may appear

// useful multiplicative conversion constants
#define FPIOVER180   0.01745329251994F    // degrees to radians conversion = pi / 180
#define F180OVERPI   57.2957795130823F    // radians to degrees conversion = 180 / pi
#define F180OVERPISQ 3282.8063500117F // square of F180OVERPI
#define ONETHIRD     0.33333333F                    // one third
#define ONESIXTH     0.166666667F                   // one sixth
#define ONEOVER48    0.02083333333F                // 1 / 48
#define ONEOVER120   0.0083333333F                // 1 / 120
#define ONEOVER3840  0.0002604166667F    // 1 / 3840
#define ONEOVERSQRT2 0.707106781F // 1/sqrt(2)
#define GTOMSEC2     9.80665                                // standard gravity in m/s2

// Aerospace NED accelerometer 3DOF tilt function computing rotation matrix fR
void f3DOFTiltNED(float fR[][3], float fGs[])
{
    // the NED self-consistency twist occurs at 90 deg pitch

    // local variables
    int16_t i; // counter
    float fmodGxyz; // modulus of the x, y, z accelerometer readings
    float fmodGyz; // modulus of the y, z accelerometer readings
    float frecipmodGxyz; // reciprocal of modulus
    float ftmp; // scratch variable

    // compute the accelerometer squared magnitudes
    fmodGyz  = fGs[CHY] * fGs[CHY] + fGs[CHZ] * fGs[CHZ];
    fmodGxyz = fmodGyz + fGs[CHX] * fGs[CHX];

    // check for freefall special case where no solution is possible
    if (is_zero(fmodGxyz)) {
        f3x3matrixAeqI(fR);
        return;
    }

    // check for vertical up or down gimbal lock case
    if (is_zero(fmodGyz)) {
        f3x3matrixAeqScalar(fR, 0.0F);
        fR[CHY][CHY] = 1.0F;
        if (fGs[CHX] >= 0.0F) {
            fR[CHX][CHZ] = 1.0F;
            fR[CHZ][CHX] = -1.0F;
        } else {
            fR[CHX][CHZ] = -1.0F;
            fR[CHZ][CHX] = 1.0F;
        }
        return;
    }

    // compute moduli for the general case
    fmodGyz  = sqrtf(fmodGyz);
    fmodGxyz = sqrtf(fmodGxyz);
    frecipmodGxyz = 1.0F / fmodGxyz;
    ftmp     = fmodGxyz / fmodGyz;

    // normalize the accelerometer reading into the z column
    for (i = CHX; i <= CHZ; i++) {
        fR[i][CHZ] = fGs[i] * frecipmodGxyz;
    }

    // construct x column of orientation matrix
    fR[CHX][CHX] = fmodGyz * frecipmodGxyz;
    fR[CHY][CHX] = -fR[CHX][CHZ] * fR[CHY][CHZ] * ftmp;
    fR[CHZ][CHX] = -fR[CHX][CHZ] * fR[CHZ][CHZ] * ftmp;

    // construct y column of orientation matrix
    fR[CHX][CHY] = 0.0F;
    fR[CHY][CHY] = fR[CHZ][CHZ] * ftmp;
    fR[CHZ][CHY] = -fR[CHY][CHZ] * ftmp;
}

// Aerospace NED magnetometer 3DOF flat eCompass function computing rotation matrix fR
void f3DOFMagnetometerMatrixNED(float fR[][3], float fBc[])
{
    // local variables
    float fmodBxy; // modulus of the x, y magnetometer readings

    // compute the magnitude of the horizontal (x and y) magnetometer reading
    fmodBxy = sqrtf(fBc[CHX] * fBc[CHX] + fBc[CHY] * fBc[CHY]);

    // check for zero field special case where no solution is possible
    if (is_zero(fmodBxy)) {
        f3x3matrixAeqI(fR);
        return;
    }

    // define the fixed entries in the z row and column
    fR[CHZ][CHX] = fR[CHZ][CHY] = fR[CHX][CHZ] = fR[CHY][CHZ] = 0.0F;
    fR[CHZ][CHZ] = 1.0F;

    // define the remaining entries
    fR[CHX][CHX] = fR[CHY][CHY] = fBc[CHX] / fmodBxy;
    fR[CHY][CHX] = fBc[CHY] / fmodBxy;
    fR[CHX][CHY] = -fR[CHY][CHX];
}

// NED: basic 6DOF e-Compass function computing rotation matrix fR and magnetic inclination angle fDelta
void feCompassNED(float fR[][3], float *pfDelta, float fBc[], float fGs[])
{
    // local variables
    float fmod[3]; // column moduli
    float fmodBc; // modulus of Bc
    float fGsdotBc; // dot product of vectors G.Bc
    float ftmp; // scratch variable
    int8_t i, j; // loop counters

    // set the inclination angle to zero in case it is not computed later
    *pfDelta = 0.0F;

    // place the un-normalized gravity and geomagnetic vectors into the rotation matrix z and x axes
    for (i = CHX; i <= CHZ; i++) {
        fR[i][CHZ] = fGs[i];
        fR[i][CHX] = fBc[i];
    }

    // set y vector to vector product of z and x vectors
    fR[CHX][CHY] = fR[CHY][CHZ] * fR[CHZ][CHX] - fR[CHZ][CHZ] * fR[CHY][CHX];
    fR[CHY][CHY] = fR[CHZ][CHZ] * fR[CHX][CHX] - fR[CHX][CHZ] * fR[CHZ][CHX];
    fR[CHZ][CHY] = fR[CHX][CHZ] * fR[CHY][CHX] - fR[CHY][CHZ] * fR[CHX][CHX];

    // set x vector to vector product of y and z vectors
    fR[CHX][CHX] = fR[CHY][CHY] * fR[CHZ][CHZ] - fR[CHZ][CHY] * fR[CHY][CHZ];
    fR[CHY][CHX] = fR[CHZ][CHY] * fR[CHX][CHZ] - fR[CHX][CHY] * fR[CHZ][CHZ];
    fR[CHZ][CHX] = fR[CHX][CHY] * fR[CHY][CHZ] - fR[CHY][CHY] * fR[CHX][CHZ];

    // calculate the rotation matrix column moduli
    fmod[CHX]    = sqrtf(fR[CHX][CHX] * fR[CHX][CHX] + fR[CHY][CHX] * fR[CHY][CHX] + fR[CHZ][CHX] * fR[CHZ][CHX]);
    fmod[CHY]    = sqrtf(fR[CHX][CHY] * fR[CHX][CHY] + fR[CHY][CHY] * fR[CHY][CHY] + fR[CHZ][CHY] * fR[CHZ][CHY]);
    fmod[CHZ]    = sqrtf(fR[CHX][CHZ] * fR[CHX][CHZ] + fR[CHY][CHZ] * fR[CHY][CHZ] + fR[CHZ][CHZ] * fR[CHZ][CHZ]);

    // normalize the rotation matrix columns
    if (!((is_zero(fmod[CHX])) || (is_zero(fmod[CHY])) || (is_zero(fmod[CHZ])))) {
        // loop over columns j
        for (j = CHX; j <= CHZ; j++) {
            ftmp = 1.0F / fmod[j];
            // loop over rows i
            for (i = CHX; i <= CHZ; i++) {
                // normalize by the column modulus
                fR[i][j] *= ftmp;
            }
        }
    } else {
        // no solution is possible so set rotation to identity matrix
        f3x3matrixAeqI(fR);
        return;
    }

    // compute the geomagnetic inclination angle (deg)
    fmodBc   = sqrtf(fBc[CHX] * fBc[CHX] + fBc[CHY] * fBc[CHY] + fBc[CHZ] * fBc[CHZ]);
    fGsdotBc = fGs[CHX] * fBc[CHX] + fGs[CHY] * fBc[CHY] + fGs[CHZ] * fBc[CHZ];
    if (!((is_zero(fmod[CHZ])) || (is_zero(fmodBc)))) {
        *pfDelta = fasin_deg(fGsdotBc / (fmod[CHZ] * fmodBc));
    }
}

// NED: 6DOF e-Compass function computing least squares fit to orientation quaternion fq
// on the assumption that the geomagnetic field fB and magnetic inclination angle fDelta are known
void fLeastSquareseCompassNED(struct fquaternion *pfq, float fB, float fsinDelta, float fcosDelta,
                              float *pfDelta6DOF, float fBc[], float fGs[], float *pfQvBQd, float *pfQvGQa)
{
    // local variables
    float fK[4][4]; // K measurement matrix
    float eigvec[4][4]; // matrix of eigenvectors of K
    float eigval[4]; // vector of eigenvalues of K
    float fmodGsSq; // modulus of fGs[] squared
    float fmodBcSq; // modulus of fBc[] squared
    float fmodGs; // modulus of fGs[]
    float fmodBc; // modulus of fBc[]
    float fGsdotBc; // scalar product of Gs and Bc
    float fag, fam; // relative weightings
    float fagOvermodGs; // a0 / |Gs|
    float famOvermodBc; // a1 / |Bc|
    float famOvermodBccosDelta; // a1 / |Bc| * cos(Delta)
    float famOvermodBcsinDelta; // a1 / |Bc| * sin(Delta)
    float ftmp; // scratch
    int8_t i; // loop counter

    // calculate the measurement vector moduli and return with identity quaternion if either is null.
    fmodGsSq = fGs[CHX] * fGs[CHX] + fGs[CHY] * fGs[CHY] + fGs[CHZ] * fGs[CHZ];
    fmodBcSq = fBc[CHX] * fBc[CHX] + fBc[CHY] * fBc[CHY] + fBc[CHZ] * fBc[CHZ];
    fmodGs   = sqrtf(fmodGsSq);
    fmodBc   = sqrtf(fmodBcSq);
    if ((is_zero(fmodGs)) || (is_zero(fmodBc)) || (is_zero(fB))) {
        pfq->q0 = 1.0F;
        pfq->q1 = pfq->q2 = pfq->q3 = 0.0F;
        return;
    }

    // calculate the accelerometer and magnetometer noise covariances (units rad^2) and least squares weightings
    *pfQvGQa = fabsf(fmodGsSq - 1.0F);
    *pfQvBQd = fabsf(fmodBcSq - fB * fB);
    fag = *pfQvBQd / (fB * fB * *pfQvGQa + *pfQvBQd);
    fam = 1.0F - fag;

    // compute useful ratios to reduce computation
    fagOvermodGs = fag / fmodGs;
    famOvermodBc = fam / fmodBc;
    famOvermodBccosDelta = famOvermodBc * fcosDelta;
    famOvermodBcsinDelta = famOvermodBc * fsinDelta;

    // compute the scalar product Gs.Bc and 6DOF accelerometer plus magnetometer geomagnetic inclination angle (deg)
    fGsdotBc     = fGs[CHX] * fBc[CHX] + fGs[CHY] * fBc[CHY] + fGs[CHZ] * fBc[CHZ];
    *pfDelta6DOF = fasin_deg((fGsdotBc) / (fmodGs * fmodBc));

    // set the K matrix to the non-zero accelerometer components
    fK[0][0]     = fK[3][3] = fagOvermodGs * fGs[CHZ];
    fK[1][1]     = fK[2][2] = -fK[0][0];
    fK[0][1]     = fK[2][3] = fagOvermodGs * fGs[CHY];
    fK[1][3]     = fagOvermodGs * fGs[CHX];
    fK[0][2]     = -fK[1][3];

    // update the K matrix with the magnetometer component
    ftmp      = famOvermodBcsinDelta * fBc[CHY];
    fK[0][1] += ftmp;
    fK[2][3] += ftmp;
    fK[1][2]  = famOvermodBccosDelta * fBc[CHY];
    fK[0][3]  = -fK[1][2];
    ftmp      = famOvermodBccosDelta * fBc[CHX];
    fK[0][0] += ftmp;
    fK[1][1] += ftmp;
    fK[2][2] -= ftmp;
    fK[3][3] -= ftmp;
    ftmp      = famOvermodBcsinDelta * fBc[CHZ];
    fK[0][0] += ftmp;
    fK[1][1] -= ftmp;
    fK[2][2] -= ftmp;
    fK[3][3] += ftmp;
    ftmp      = famOvermodBccosDelta * fBc[CHZ];
    fK[0][2] += ftmp;
    fK[1][3] += ftmp;
    ftmp      = famOvermodBcsinDelta * fBc[CHX];
    fK[0][2] -= ftmp;
    fK[1][3] += ftmp;

    // copy above diagonal elements to below diagonal
    fK[1][0]  = fK[0][1];
    fK[2][0]  = fK[0][2];
    fK[2][1]  = fK[1][2];
    fK[3][0]  = fK[0][3];
    fK[3][1]  = fK[1][3];
    fK[3][2]  = fK[2][3];

    // set eigval to the unsorted eigenvalues and eigvec to the unsorted normalized eigenvectors of fK
    eigencompute4(fK, eigval, eigvec, 4);

    // copy the largest eigenvector into the orientation quaternion fq
    i = 0;
    if (eigval[1] > eigval[i]) {
        i = 1;
    }
    if (eigval[2] > eigval[i]) {
        i = 2;
    }
    if (eigval[3] > eigval[i]) {
        i = 3;
    }
    pfq->q0 = eigvec[0][i];
    pfq->q1 = eigvec[1][i];
    pfq->q2 = eigvec[2][i];
    pfq->q3 = eigvec[3][i];

    // force q0 to be non-negative
    if (pfq->q0 < 0.0F) {
        pfq->q0 = -pfq->q0;
        pfq->q1 = -pfq->q1;
        pfq->q2 = -pfq->q2;
        pfq->q3 = -pfq->q3;
    }
}

// extract the NED angles in degrees from the NED rotation matrix
void fNEDAnglesDegFromRotationMatrix(float R[][3], float *pfPhiDeg, float *pfTheDeg, float *pfPsiDeg, float *pfRhoDeg, float *pfChiDeg)
{
    // calculate the pitch angle -90.0 <= Theta <= 90.0 deg
    *pfTheDeg = fasin_deg(-R[CHX][CHZ]);

    // calculate the roll angle range -180.0 <= Phi < 180.0 deg
    *pfPhiDeg = fatan2_deg(R[CHY][CHZ], R[CHZ][CHZ]);

    // map +180 roll onto the functionally equivalent -180 deg roll
    if (is_equal(*pfPhiDeg, 180.0F)) {
        *pfPhiDeg = -180.0F;
    }

    // calculate the yaw (compass) angle 0.0 <= Psi < 360.0 deg
    if (is_equal(*pfTheDeg, 90.0F)) {
        // vertical upwards gimbal lock case
        *pfPsiDeg = fatan2_deg(R[CHZ][CHY], R[CHY][CHY]) + *pfPhiDeg;
    } else if (is_equal(*pfTheDeg, -90.0F)) {
        // vertical downwards gimbal lock case
        *pfPsiDeg = fatan2_deg(-R[CHZ][CHY], R[CHY][CHY]) - *pfPhiDeg;
    } else {
        // general case
        *pfPsiDeg = fatan2_deg(R[CHX][CHY], R[CHX][CHX]);
    }

    // map yaw angle Psi onto range 0.0 <= Psi < 360.0 deg
    if (*pfPsiDeg < 0.0F) {
        *pfPsiDeg += 360.0F;
    }

    // check for rounding errors mapping small negative angle to 360 deg
    if (*pfPsiDeg >= 360.0F) {
        *pfPsiDeg = 0.0F;
    }

    // for NED, the compass heading Rho equals the yaw angle Psi
    *pfRhoDeg = *pfPsiDeg;

    // calculate the tilt angle from vertical Chi (0 <= Chi <= 180 deg)
    *pfChiDeg = facos_deg(R[CHZ][CHZ]);
}

// computes normalized rotation quaternion from a rotation vector (deg)
void fQuaternionFromRotationVectorDeg(struct fquaternion *pq, const float rvecdeg[], float fscaling)
{
    float fetadeg; // rotation angle (deg)
    float fetarad; // rotation angle (rad)
    float fetarad2; // eta (rad)^2
    float fetarad4; // eta (rad)^4
    float sinhalfeta; // sin(eta/2)
    float fvecsq; // q1^2+q2^2+q3^2
    float ftmp; // scratch variable

    // compute the scaled rotation angle eta (deg) which can be both positve or negative
    fetadeg  = fscaling * sqrtf(rvecdeg[CHX] * rvecdeg[CHX] + rvecdeg[CHY] * rvecdeg[CHY] + rvecdeg[CHZ] * rvecdeg[CHZ]);
    fetarad  = fetadeg * FPIOVER180;
    fetarad2 = fetarad * fetarad;

    // calculate the sine and cosine using small angle approximations or exact
    // angles under sqrt(0.02)=0.141 rad is 8.1 deg and 1620 deg/s (=936deg/s in 3 axes) at 200Hz and 405 deg/s at 50Hz
    if (fetarad2 <= 0.02F) {
        // use MacLaurin series up to and including third order
        sinhalfeta = fetarad * (0.5F - ONEOVER48 * fetarad2);
    } else if (fetarad2 <= 0.06F) {
        // use MacLaurin series up to and including fifth order
        // angles under sqrt(0.06)=0.245 rad is 14.0 deg and 2807 deg/s (=1623deg/s in 3 axes) at 200Hz and 703 deg/s at 50Hz
        fetarad4   = fetarad2 * fetarad2;
        sinhalfeta = fetarad * (0.5F - ONEOVER48 * fetarad2 + ONEOVER3840 * fetarad4);
    } else {
        // use exact calculation
        sinhalfeta = (float)sinf(0.5F * fetarad);
    }

    // compute the vector quaternion components q1, q2, q3
    if (!is_zero(fetadeg)) {
        // general case with non-zero rotation angle
        ftmp   = fscaling * sinhalfeta / fetadeg;
        pq->q1 = rvecdeg[CHX] * ftmp; // q1 = nx * sin(eta/2)
        pq->q2 = rvecdeg[CHY] * ftmp; // q2 = ny * sin(eta/2)
        pq->q3 = rvecdeg[CHZ] * ftmp; // q3 = nz * sin(eta/2)
    } else {
        // zero rotation angle giving zero vector component
        pq->q1 = pq->q2 = pq->q3 = 0.0F;
    }

    // compute the scalar quaternion component q0 by explicit normalization
    // taking care to avoid rounding errors giving negative operand to sqrt
    fvecsq = pq->q1 * pq->q1 + pq->q2 * pq->q2 + pq->q3 * pq->q3;
    if (fvecsq <= 1.0F) {
        // normal case
        pq->q0 = sqrtf(1.0F - fvecsq);
    } else {
        // rounding errors are present
        pq->q0 = 0.0F;
    }
}

// compute the orientation quaternion from a 3x3 rotation matrix
void fQuaternionFromRotationMatrix(float R[][3], struct fquaternion *pq)
{
    float fq0sq; // q0^2
    float recip4q0; // 1/4q0

    // the quaternion is not explicitly normalized in this function on the assumption that it
    // is supplied with a normalized rotation matrix. if the rotation matrix is normalized then
    // the quaternion will also be normalized even if the case of small q0

    // get q0^2 and q0
    fq0sq  = 0.25F * (1.0F + R[CHX][CHX] + R[CHY][CHY] + R[CHZ][CHZ]);
    pq->q0 = sqrtf(fabsf(fq0sq));

    // normal case when q0 is not small meaning rotation angle not near 180 deg
    if (pq->q0 > SMALLQ0) {
        // calculate q1 to q3
        recip4q0 = 0.25F / pq->q0;
        pq->q1   = recip4q0 * (R[CHY][CHZ] - R[CHZ][CHY]);
        pq->q2   = recip4q0 * (R[CHZ][CHX] - R[CHX][CHZ]);
        pq->q3   = recip4q0 * (R[CHX][CHY] - R[CHY][CHX]);
    } // end of general case
    else {
        // special case of near 180 deg corresponds to nearly symmetric matrix
        // which is not numerically well conditioned for division by small q0
        // instead get absolute values of q1 to q3 from leading diagonal
        pq->q1 = sqrtf(fabsf(0.5F * (1.0F + R[CHX][CHX]) - fq0sq));
        pq->q2 = sqrtf(fabsf(0.5F * (1.0F + R[CHY][CHY]) - fq0sq));
        pq->q3 = sqrtf(fabsf(0.5F * (1.0F + R[CHZ][CHZ]) - fq0sq));

        // correct the signs of q1 to q3 by examining the signs of differenced off-diagonal terms
        if ((R[CHY][CHZ] - R[CHZ][CHY]) < 0.0F) {
            pq->q1 = -pq->q1;
        }
        if ((R[CHZ][CHX] - R[CHX][CHZ]) < 0.0F) {
            pq->q2 = -pq->q2;
        }
        if ((R[CHX][CHY] - R[CHY][CHX]) < 0.0F) {
            pq->q3 = -pq->q3;
        }
    } // end of special case
}

// compute the rotation matrix from an orientation quaternion
void fRotationMatrixFromQuaternion(float R[][3], const struct fquaternion *pq)
{
    float f2q;
    float f2q0q0, f2q0q1, f2q0q2, f2q0q3;
    float f2q1q1, f2q1q2, f2q1q3;
    float f2q2q2, f2q2q3;
    float f2q3q3;

    // set f2q to 2*q0 and calculate products
    f2q    = 2.0F * pq->q0;
    f2q0q0 = f2q * pq->q0;
    f2q0q1 = f2q * pq->q1;
    f2q0q2 = f2q * pq->q2;
    f2q0q3 = f2q * pq->q3;
    // set f2q to 2*q1 and calculate products
    f2q    = 2.0F * pq->q1;
    f2q1q1 = f2q * pq->q1;
    f2q1q2 = f2q * pq->q2;
    f2q1q3 = f2q * pq->q3;
    // set f2q to 2*q2 and calculate products
    f2q    = 2.0F * pq->q2;
    f2q2q2 = f2q * pq->q2;
    f2q2q3 = f2q * pq->q3;
    f2q3q3 = 2.0F * pq->q3 * pq->q3;

    // calculate the rotation matrix assuming the quaternion is normalized
    R[CHX][CHX] = f2q0q0 + f2q1q1 - 1.0F;
    R[CHX][CHY] = f2q1q2 + f2q0q3;
    R[CHX][CHZ] = f2q1q3 - f2q0q2;
    R[CHY][CHX] = f2q1q2 - f2q0q3;
    R[CHY][CHY] = f2q0q0 + f2q2q2 - 1.0F;
    R[CHY][CHZ] = f2q2q3 + f2q0q1;
    R[CHZ][CHX] = f2q1q3 + f2q0q2;
    R[CHZ][CHY] = f2q2q3 - f2q0q1;
    R[CHZ][CHZ] = f2q0q0 + f2q3q3 - 1.0F;
}

// computes rotation vector (deg) from rotation quaternion
void fRotationVectorDegFromQuaternion(struct fquaternion *pq, float rvecdeg[])
{
    float fetarad; // rotation angle (rad)
    float fetadeg; // rotation angle (deg)
    float sinhalfeta; // sin(eta/2)
    float ftmp; // scratch variable

    // calculate the rotation angle in the range 0 <= eta < 360 deg
    if ((pq->q0 >= 1.0F) || (pq->q0 <= -1.0F)) {
        // rotation angle is 0 deg or 2*180 deg = 360 deg = 0 deg
        fetarad = 0.0F;
        fetadeg = 0.0F;
    } else {
        // general case returning 0 < eta < 360 deg
        fetarad = 2.0F * acosf(pq->q0);
        fetadeg = fetarad * F180OVERPI;
    }

    // map the rotation angle onto the range -180 deg <= eta < 180 deg
    if (fetadeg >= 180.0F) {
        fetadeg -= 360.0F;
        fetarad  = fetadeg * FPIOVER180;
    }

    // calculate sin(eta/2) which will be in the range -1 to +1
    sinhalfeta = (float)sinf(0.5F * fetarad);

    // calculate the rotation vector (deg)
    if (is_zero(sinhalfeta)) {
        // the rotation angle eta is zero and the axis is irrelevant
        rvecdeg[CHX] = rvecdeg[CHY] = rvecdeg[CHZ] = 0.0F;
    } else {
        // general case with non-zero rotation angle
        ftmp = fetadeg / sinhalfeta;
        rvecdeg[CHX] = pq->q1 * ftmp;
        rvecdeg[CHY] = pq->q2 * ftmp;
        rvecdeg[CHZ] = pq->q3 * ftmp;
    }
}

// function low pass filters an orientation quaternion and computes virtual gyro rotation rate
void fLPFOrientationQuaternion(struct fquaternion *pq, struct fquaternion *pLPq, float flpf, float fdeltat, float fOmega[])
{
    // local variables
    struct fquaternion fdeltaq; // delta rotation quaternion
    float rvecdeg[3]; // rotation vector (deg)
    float ftmp; // scratch variable

    // set fdeltaqn to the delta rotation quaternion conjg(fLPq[n-1) . fqn
    fdeltaq = qconjgAxB(pLPq, pq);
    if (fdeltaq.q0 < 0.0F) {
        fdeltaq.q0 = -fdeltaq.q0;
        fdeltaq.q1 = -fdeltaq.q1;
        fdeltaq.q2 = -fdeltaq.q2;
        fdeltaq.q3 = -fdeltaq.q3;
    }

    // set ftmp to a scaled lpf value which equals flpf in the limit of small rotations (q0=1)
    // but which rises to 1 (all pass) as the delta rotation angle increases (q0 tends to zero)
    ftmp = flpf + (1.0F - flpf) * (1.0F - fdeltaq.q0);

    // scale the delta rotation by the corrected lpf value
    fdeltaq.q1 *= ftmp;
    fdeltaq.q2 *= ftmp;
    fdeltaq.q3 *= ftmp;

    // compute the scalar component q0
    ftmp = fdeltaq.q1 * fdeltaq.q1 + fdeltaq.q2 * fdeltaq.q2 + fdeltaq.q3 * fdeltaq.q3;
    if (ftmp <= 1.0F) {
        // normal case
        fdeltaq.q0 = sqrtf(1.0F - ftmp);
    } else {
        // rounding errors present so simply set scalar component to 0
        fdeltaq.q0 = 0.0F;
    }

    // calculate the delta rotation vector from fdeltaqn and the virtual gyro angular velocity (deg/s)
    fRotationVectorDegFromQuaternion(&fdeltaq, rvecdeg);
    ftmp = 1.0F / fdeltat;
    fOmega[CHX] = rvecdeg[CHX] * ftmp;
    fOmega[CHY] = rvecdeg[CHY] * ftmp;
    fOmega[CHZ] = rvecdeg[CHZ] * ftmp;

    // set LPq[n] = LPq[n-1] . deltaq[n]
    qAeqAxB(pLPq, &fdeltaq);

    // renormalize the low pass filtered quaternion to prevent error accumulation
    // the renormalization function ensures that q0 is non-negative
    fqAeqNormqA(pLPq);
}

// function compute the quaternion product qA * qB
void qAeqBxC(struct fquaternion *pqA, const struct fquaternion *pqB, const struct fquaternion *pqC)
{
    pqA->q0 = pqB->q0 * pqC->q0 - pqB->q1 * pqC->q1 - pqB->q2 * pqC->q2 - pqB->q3 * pqC->q3;
    pqA->q1 = pqB->q0 * pqC->q1 + pqB->q1 * pqC->q0 + pqB->q2 * pqC->q3 - pqB->q3 * pqC->q2;
    pqA->q2 = pqB->q0 * pqC->q2 - pqB->q1 * pqC->q3 + pqB->q2 * pqC->q0 + pqB->q3 * pqC->q1;
    pqA->q3 = pqB->q0 * pqC->q3 + pqB->q1 * pqC->q2 - pqB->q2 * pqC->q1 + pqB->q3 * pqC->q0;
}

// function compute the quaternion product qA = qA * qB
void qAeqAxB(struct fquaternion *pqA, const struct fquaternion *pqB)
{
    struct fquaternion qProd;

    // perform the quaternion product
    qProd.q0 = pqA->q0 * pqB->q0 - pqA->q1 * pqB->q1 - pqA->q2 * pqB->q2 - pqA->q3 * pqB->q3;
    qProd.q1 = pqA->q0 * pqB->q1 + pqA->q1 * pqB->q0 + pqA->q2 * pqB->q3 - pqA->q3 * pqB->q2;
    qProd.q2 = pqA->q0 * pqB->q2 - pqA->q1 * pqB->q3 + pqA->q2 * pqB->q0 + pqA->q3 * pqB->q1;
    qProd.q3 = pqA->q0 * pqB->q3 + pqA->q1 * pqB->q2 - pqA->q2 * pqB->q1 + pqA->q3 * pqB->q0;

    // copy the result back into qA
    *pqA     = qProd;
}

// function compute the quaternion product conjg(qA) * qB
struct fquaternion qconjgAxB(const struct fquaternion *pqA, const struct fquaternion *pqB)
{
    struct fquaternion qProd;

    qProd.q0 = pqA->q0 * pqB->q0 + pqA->q1 * pqB->q1 + pqA->q2 * pqB->q2 + pqA->q3 * pqB->q3;
    qProd.q1 = pqA->q0 * pqB->q1 - pqA->q1 * pqB->q0 - pqA->q2 * pqB->q3 + pqA->q3 * pqB->q2;
    qProd.q2 = pqA->q0 * pqB->q2 + pqA->q1 * pqB->q3 - pqA->q2 * pqB->q0 - pqA->q3 * pqB->q1;
    qProd.q3 = pqA->q0 * pqB->q3 - pqA->q1 * pqB->q2 + pqA->q2 * pqB->q1 - pqA->q3 * pqB->q0;

    return qProd;
}

// function normalizes a rotation quaternion and ensures q0 is non-negative
void fqAeqNormqA(struct fquaternion *pqA)
{
    float fNorm; // quaternion Norm

    // calculate the quaternion Norm
    fNorm = sqrtf(pqA->q0 * pqA->q0 + pqA->q1 * pqA->q1 + pqA->q2 * pqA->q2 + pqA->q3 * pqA->q3);
    if (fNorm > CORRUPTQUAT) {
        // general case
        fNorm    = 1.0F / fNorm;
        pqA->q0 *= fNorm;
        pqA->q1 *= fNorm;
        pqA->q2 *= fNorm;
        pqA->q3 *= fNorm;
    } else {
        // return with identity quaternion since the quaternion is corrupted
        pqA->q0 = 1.0F;
        pqA->q1 = pqA->q2 = pqA->q3 = 0.0F;
    }

    // correct a negative scalar component if the function was called with negative q0
    if (pqA->q0 < 0.0F) {
        pqA->q0 = -pqA->q0;
        pqA->q1 = -pqA->q1;
        pqA->q2 = -pqA->q2;
        pqA->q3 = -pqA->q3;
    }
}

// set a quaternion to the unit quaternion
void fqAeq1(struct fquaternion *pqA)
{
    pqA->q0 = 1.0F;
    pqA->q1 = pqA->q2 = pqA->q3 = 0.0F;
}

// function computes the rotation quaternion that rotates unit vector u onto unit vector v as v=q*.u.q
// using q = 1/sqrt(2) * {sqrt(1 + u.v) - u x v / sqrt(1 + u.v)}
void fveqconjgquq(struct fquaternion *pfq, float fu[], float fv[])
{
    float fuxv[3]; // vector product u x v
    float fsqrt1plusudotv; // sqrt(1 + u.v)
    float ftmp; // scratch

    // compute sqrt(1 + u.v) and scalar quaternion component q0 (valid for all angles including 180 deg)
    fsqrt1plusudotv = sqrtf(fabsf(1.0F + fu[CHX] * fv[CHX] + fu[CHY] * fv[CHY] + fu[CHZ] * fv[CHZ]));
    pfq->q0   = ONEOVERSQRT2 * fsqrt1plusudotv;

    // calculate the vector product uxv
    fuxv[CHX] = fu[CHY] * fv[CHZ] - fu[CHZ] * fv[CHY];
    fuxv[CHY] = fu[CHZ] * fv[CHX] - fu[CHX] * fv[CHZ];
    fuxv[CHZ] = fu[CHX] * fv[CHY] - fu[CHY] * fv[CHX];

    // compute the vector component of the quaternion
    if (!is_zero(fsqrt1plusudotv)) {
        // general case where u and v are not anti-parallel where u.v=-1
        ftmp    = ONEOVERSQRT2 / fsqrt1plusudotv;
        pfq->q1 = -fuxv[CHX] * ftmp;
        pfq->q2 = -fuxv[CHY] * ftmp;
        pfq->q3 = -fuxv[CHZ] * ftmp;
    } else {
        // degenerate case where u and v are anti-aligned and the 180 deg rotation quaternion is not uniquely defined.
        // first calculate the un-normalized vector component (the scalar component q0 is already set to zero)
        pfq->q1 = fu[CHY] - fu[CHZ];
        pfq->q2 = fu[CHZ] - fu[CHX];
        pfq->q3 = fu[CHX] - fu[CHY];
        // and normalize the quaternion for this case checking for fu[CHX]=fu[CHY]=fuCHZ] where q1=q2=q3=0.
        ftmp    = sqrtf(fabsf(pfq->q1 * pfq->q1 + pfq->q2 * pfq->q2 + pfq->q3 * pfq->q3));
        if (!is_zero(ftmp)) {
            // normal case where all three components of fu (and fv=-fu) are not equal
            ftmp     = 1.0F / ftmp;
            pfq->q1 *= ftmp;
            pfq->q2 *= ftmp;
            pfq->q3 *= ftmp;
        } else {
            // final case where the three entries are equal but since the vectors are known to be normalized
            // the vector u must be 1/root(3)*{1, 1, 1} or -1/root(3)*{1, 1, 1} so simply set the
            // rotation vector to 1/root(2)*{1, -1, 0} to cover both cases
            pfq->q1 = ONEOVERSQRT2;
            pfq->q2 = -ONEOVERSQRT2;
            pfq->q3 = 0.0F;
        }
    }
}

// Conversion macro
#define RAD2DEG(rad) ((rad) * (180.0f / M_PI_F))
#define DEG2RAD(deg) ((deg) * (M_PI_F / 180.0f))


// ****** find roll, pitch, yaw from quaternion ********
void Quaternion2RPY(const float q[4], float rpy[3])
{
    float R13, R11, R12, R23, R33;
    float q0s = q[0] * q[0];
    float q1s = q[1] * q[1];
    float q2s = q[2] * q[2];
    float q3s = q[3] * q[3];

    R13    = 2.0f * (q[1] * q[3] - q[0] * q[2]);
    R11    = q0s + q1s - q2s - q3s;
    R12    = 2.0f * (q[1] * q[2] + q[0] * q[3]);
    R23    = 2.0f * (q[2] * q[3] + q[0] * q[1]);
    R33    = q0s - q1s - q2s + q3s;

    rpy[1] = RAD2DEG(asinf(-R13)); // pitch always between -pi/2 to pi/2
    rpy[2] = RAD2DEG(atan2f(R12, R11));
    rpy[0] = RAD2DEG(atan2f(R23, R33));

    // TODO: consider the cases where |R13| ~= 1, |pitch| ~= pi/2
}

// ****** find quaternion from roll, pitch, yaw ********
void RPY2Quaternion(const float rpy[3], float q[4])
{
    float phi, theta, psi;
    float cphi, sphi, ctheta, stheta, cpsi, spsi;

    phi    = DEG2RAD(rpy[0] / 2);
    theta  = DEG2RAD(rpy[1] / 2);
    psi    = DEG2RAD(rpy[2] / 2);
    cphi   = cosf(phi);
    sphi   = sinf(phi);
    ctheta = cosf(theta);
    stheta = sinf(theta);
    cpsi   = cosf(psi);
    spsi   = sinf(psi);

    q[0]   = cphi * ctheta * cpsi + sphi * stheta * spsi;
    q[1]   = sphi * ctheta * cpsi - cphi * stheta * spsi;
    q[2]   = cphi * stheta * cpsi + sphi * ctheta * spsi;
    q[3]   = cphi * ctheta * spsi - sphi * stheta * cpsi;

    if (q[0] < 0) { // q0 always positive for uniqueness
        q[0] = -q[0];
        q[1] = -q[1];
        q[2] = -q[2];
        q[3] = -q[3];
    }
}

// ** Find Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
void Quaternion2R(float q[4], float Rbe[3][3])
{
    const float q0s = q[0] * q[0], q1s = q[1] * q[1], q2s = q[2] * q[2], q3s = q[3] * q[3];

    Rbe[0][0] = q0s + q1s - q2s - q3s;
    Rbe[0][1] = 2 * (q[1] * q[2] + q[0] * q[3]);
    Rbe[0][2] = 2 * (q[1] * q[3] - q[0] * q[2]);
    Rbe[1][0] = 2 * (q[1] * q[2] - q[0] * q[3]);
    Rbe[1][1] = q0s - q1s + q2s - q3s;
    Rbe[1][2] = 2 * (q[2] * q[3] + q[0] * q[1]);
    Rbe[2][0] = 2 * (q[1] * q[3] + q[0] * q[2]);
    Rbe[2][1] = 2 * (q[2] * q[3] - q[0] * q[1]);
    Rbe[2][2] = q0s - q1s - q2s + q3s;
}


// ** Find first row of Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
// ** This vector corresponds to the fuselage/roll vector xB **
void QuaternionC2xB(const float q0, const float q1, const float q2, const float q3, float x[3])
{
    const float q0s = q0 * q0, q1s = q1 * q1, q2s = q2 * q2, q3s = q3 * q3;

    x[0] = q0s + q1s - q2s - q3s;
    x[1] = 2 * (q1 * q2 + q0 * q3);
    x[2] = 2 * (q1 * q3 - q0 * q2);
}


void Quaternion2xB(const float q[4], float x[3])
{
    QuaternionC2xB(q[0], q[1], q[2], q[3], x);
}


// ** Find second row of Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
// ** This vector corresponds to the spanwise/pitch vector yB **
void QuaternionC2yB(const float q0, const float q1, const float q2, const float q3, float y[3])
{
    const float q0s = q0 * q0, q1s = q1 * q1, q2s = q2 * q2, q3s = q3 * q3;

    y[0] = 2 * (q1 * q2 - q0 * q3);
    y[1] = q0s - q1s + q2s - q3s;
    y[2] = 2 * (q2 * q3 + q0 * q1);
}


void Quaternion2yB(const float q[4], float y[3])
{
    QuaternionC2yB(q[0], q[1], q[2], q[3], y);
}


// ** Find third row of Rbe, that rotates a vector from earth fixed to body frame, from quaternion **
// ** This vector corresponds to the vertical/yaw vector zB **
void QuaternionC2zB(const float q0, const float q1, const float q2, const float q3, float z[3])
{
    const float q0s = q0 * q0, q1s = q1 * q1, q2s = q2 * q2, q3s = q3 * q3;

    z[0] = 2 * (q1 * q3 + q0 * q2);
    z[1] = 2 * (q2 * q3 - q0 * q1);
    z[2] = q0s - q1s - q2s + q3s;
}


void Quaternion2zB(const float q[4], float z[3])
{
    QuaternionC2zB(q[0], q[1], q[2], q[3], z);
}


// ****** convert Rotation Matrix to Quaternion ********
// ****** if R converts from e to b, q is rotation from e to b ****
void R2Quaternion(float R[3][3], float q[4])
{
    float m[4], mag;
    uint8_t index, i;

    m[0]  = 1 + R[0][0] + R[1][1] + R[2][2];
    m[1]  = 1 + R[0][0] - R[1][1] - R[2][2];
    m[2]  = 1 - R[0][0] + R[1][1] - R[2][2];
    m[3]  = 1 - R[0][0] - R[1][1] + R[2][2];

    // find maximum divisor
    index = 0;
    mag   = m[0];
    for (i = 1; i < 4; i++) {
        if (m[i] > mag) {
            mag   = m[i];
            index = i;
        }
    }
    mag = 2 * sqrtf(mag);

    if (index == 0) {
        q[0] = mag / 4;
        q[1] = (R[1][2] - R[2][1]) / mag;
        q[2] = (R[2][0] - R[0][2]) / mag;
        q[3] = (R[0][1] - R[1][0]) / mag;
    } else if (index == 1) {
        q[1] = mag / 4;
        q[0] = (R[1][2] - R[2][1]) / mag;
        q[2] = (R[0][1] + R[1][0]) / mag;
        q[3] = (R[0][2] + R[2][0]) / mag;
    } else if (index == 2) {
        q[2] = mag / 4;
        q[0] = (R[2][0] - R[0][2]) / mag;
        q[1] = (R[0][1] + R[1][0]) / mag;
        q[3] = (R[1][2] + R[2][1]) / mag;
    } else {
        q[3] = mag / 4;
        q[0] = (R[0][1] - R[1][0]) / mag;
        q[1] = (R[0][2] + R[2][0]) / mag;
        q[2] = (R[1][2] + R[2][1]) / mag;
    }

    // q0 positive, i.e. angle between pi and -pi
    if (q[0] < 0) {
        q[0] = -q[0];
        q[1] = -q[1];
        q[2] = -q[2];
        q[3] = -q[3];
    }
}

#define MIN_ALLOWABLE_MAGNITUDE 1e-30f

// ****** Rotation Matrix from Two Vector Directions ********
// ****** given two vector directions (v1 and v2) known in two frames (b and e) find Rbe ***
// ****** solution is approximate if can't be exact ***
uint8_t RotFrom2Vectors(const float v1b[3], const float v1e[3], const float v2b[3], const float v2e[3], float Rbe[3][3])
{
    float Rib[3][3], Rie[3][3];
    float mag;
    uint8_t i, j, k;

    // identity rotation in case of error
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            Rbe[i][j] = 0;
        }
        Rbe[i][i] = 1;
    }

    // The first rows of rot matrices chosen in direction of v1
    mag = VectorMagnitude(v1b);
    if (fabsf(mag) < MIN_ALLOWABLE_MAGNITUDE) {
        return -1;
    }
    for (i = 0; i < 3; i++) {
        Rib[0][i] = v1b[i] / mag;
    }

    mag = VectorMagnitude(v1e);
    if (fabsf(mag) < MIN_ALLOWABLE_MAGNITUDE) {
        return -1;
    }
    for (i = 0; i < 3; i++) {
        Rie[0][i] = v1e[i] / mag;
    }

    // The second rows of rot matrices chosen in direction of v1xv2
    CrossProduct(v1b, v2b, &Rib[1][0]);
    mag = VectorMagnitude(&Rib[1][0]);
    if (fabsf(mag) < MIN_ALLOWABLE_MAGNITUDE) {
        return -1;
    }
    for (i = 0; i < 3; i++) {
        Rib[1][i] = Rib[1][i] / mag;
    }

    CrossProduct(v1e, v2e, &Rie[1][0]);
    mag = VectorMagnitude(&Rie[1][0]);
    if (fabsf(mag) < MIN_ALLOWABLE_MAGNITUDE) {
        return -1;
    }
    for (i = 0; i < 3; i++) {
        Rie[1][i] = Rie[1][i] / mag;
    }

    // The third rows of rot matrices are XxY (Row1xRow2)
    CrossProduct(&Rib[0][0], &Rib[1][0], &Rib[2][0]);
    CrossProduct(&Rie[0][0], &Rie[1][0], &Rie[2][0]);

    // Rbe = Rbi*Rie = Rib'*Rie
    for (i = 0; i < 3; i++) {
        for (j = 0; j < 3; j++) {
            Rbe[i][j] = 0;
            for (k = 0; k < 3; k++) {
                Rbe[i][j] += Rib[k][i] * Rie[k][j];
            }
        }
    }

    return 1;
}

void Rv2Rot(float Rv[3], float R[3][3])
{
    // Compute rotation matrix from a rotation vector
    // To save .text space, uses Quaternion2R()
    float q[4];

    float angle = VectorMagnitude(Rv);

    if (angle <= 0.00048828125f) {
        // angle < sqrt(2*machine_epsilon(float)), so flush cos(x) to 1.0f
        q[0] = 1.0f;

        // and flush sin(x/2)/x to 0.5
        q[1] = 0.5f * Rv[0];
        q[2] = 0.5f * Rv[1];
        q[3] = 0.5f * Rv[2];
        // This prevents division by zero, while retaining full accuracy
    } else {
        q[0] = cosf(angle * 0.5f);
        float scale = sinf(angle * 0.5f) / angle;
        q[1] = scale * Rv[0];
        q[2] = scale * Rv[1];
        q[3] = scale * Rv[2];
    }

    Quaternion2R(q, R);
}

// ****** Vector Cross Product ********
void CrossProduct(const float v1[3], const float v2[3], float result[3])
{
    result[0] = v1[1] * v2[2] - v2[1] * v1[2];
    result[1] = v2[0] * v1[2] - v1[0] * v2[2];
    result[2] = v1[0] * v2[1] - v2[0] * v1[1];
}

// ****** Vector Magnitude ********
float VectorMagnitude(const float v[3])
{
    return sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
}

/**
 * @brief Compute the inverse of a quaternion
 * @param [in][out] q The matrix to invert
 */
void quat_inverse(float q[4])
{
    q[1] = -q[1];
    q[2] = -q[2];
    q[3] = -q[3];
}

/**
 * @brief Duplicate a quaternion
 * @param[in] q quaternion in
 * @param[out] qnew quaternion to copy to
 */
void quat_copy(const float q[4], float qnew[4])
{
    qnew[0] = q[0];
    qnew[1] = q[1];
    qnew[2] = q[2];
    qnew[3] = q[3];
}

/**
 * @brief Multiply two quaternions into a third
 * @param[in] q1 First quaternion
 * @param[in] q2 Second quaternion
 * @param[out] qout Output quaternion
 */
void quat_mult(const float q1[4], const float q2[4], float qout[4])
{
    qout[0] = q1[0] * q2[0] - q1[1] * q2[1] - q1[2] * q2[2] - q1[3] * q2[3];
    qout[1] = q1[0] * q2[1] + q1[1] * q2[0] + q1[2] * q2[3] - q1[3] * q2[2];
    qout[2] = q1[0] * q2[2] - q1[1] * q2[3] + q1[2] * q2[0] + q1[3] * q2[1];
    qout[3] = q1[0] * q2[3] + q1[1] * q2[2] - q1[2] * q2[1] + q1[3] * q2[0];
}

/**
 * @brief Rotate a vector by a rotation matrix
 * @param[in] R a three by three rotation matrix (first index is row)
 * @param[in] vec the source vector
 * @param[out] vec_out the output vector
 */
void rot_mult(float R[3][3], const float vec[3], float vec_out[3])
{
    vec_out[0] = R[0][0] * vec[0] + R[0][1] * vec[1] + R[0][2] * vec[2];
    vec_out[1] = R[1][0] * vec[0] + R[1][1] * vec[1] + R[1][2] * vec[2];
    vec_out[2] = R[2][0] * vec[0] + R[2][1] * vec[1] + R[2][2] * vec[2];
}
