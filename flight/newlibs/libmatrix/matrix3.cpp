#include "mathmisc.h"
#include "matrix3.h"
#include "vector3.h"


// Conversion macro
#define RAD2DEG(rad) ((rad) * (180.0f / M_PI_F))
#define DEG2RAD(deg) ((deg) * (M_PI_F / 180.0f))


// create a rotation matrix given some euler angles
// this is based on http://gentlenav.googlecode.com/files/EulerAngles.pdf
template <typename T>
void Matrix3<T>::from_euler(float roll, float pitch, float yaw)
{
    float phi, theta, psi;

    phi   = DEG2RAD(0.5f * roll);
    theta = DEG2RAD(0.5f * pitch);
    psi   = DEG2RAD(0.5f * yaw);
    float cp = cosf(theta);
    float sp = sinf(theta);
    float sr = sinf(phi);
    float cr = cosf(phi);
    float sy = sinf(psi);
    float cy = cosf(psi);

    a.x = cp * cy;
    a.y = (sr * sp * cy) - (cr * sy);
    a.z = (cr * sp * cy) + (sr * sy);
    b.x = cp * sy;
    b.y = (sr * sp * sy) + (cr * cy);
    b.z = (cr * sp * sy) - (sr * cy);
    c.x = -sp;
    c.y = sr * cp;
    c.z = cr * cp;
}

// calculate euler angles from a rotation matrix
// this is based on http://gentlenav.googlecode.com/files/EulerAngles.pdf
// still in radians
template <typename T>
void Matrix3<T>::to_euler(float *roll, float *pitch, float *yaw) const
{
    if (pitch != 0) {
        *pitch = -asinf(c.x);
        // *pitch = -safe_asin(c.x);
    }
    if (roll != 0) {
        *roll = atan2f(c.y, c.z);
    }
    if (yaw != 0) {
        *yaw = atan2f(b.x, a.x);
    }
}

/*
   re-normalise a rotation matrix
 */
template <typename T>
void Matrix3<T>::normalize(void)
{
    float error   = a * b;

    Vector3<T> t0 = a - (b * (0.5f * error));
    Vector3<T> t1 = b - (a * (0.5f * error));
    Vector3<T> t2 = t0 % t1;
    a = t0 * (1.0f / t0.length());
    b = t1 * (1.0f / t1.length());
    c = t2 * (1.0f / t2.length());
}

// multiplication by a vector
template <typename T>
Vector3<T> Matrix3<T>::operator *(const Vector3<T> &v) const
{
    return Vector3<T>(a.x * v.x + a.y * v.y + a.z * v.z,
                      b.x * v.x + b.y * v.y + b.z * v.z,
                      c.x * v.x + c.y * v.y + c.z * v.z);
}

// multiplication by a vector, extracting only the xy components
template <typename T>
Vector2<T> Matrix3<T>::mulXY(const Vector3<T> &v) const
{
    return Vector2<T>(a.x * v.x + a.y * v.y + a.z * v.z,
                      b.x * v.x + b.y * v.y + b.z * v.z);
}

// multiplication of transpose by a vector
template <typename T>
Vector3<T> Matrix3<T>::mul_transpose(const Vector3<T> &v) const
{
    return Vector3<T>(a.x * v.x + b.x * v.y + c.x * v.z,
                      a.y * v.x + b.y * v.y + c.y * v.z,
                      a.z * v.x + b.z * v.y + c.z * v.z);
}

// multiplication by another Matrix3<T>
// m * this
template <typename T>
Matrix3<T> Matrix3<T>::operator *(const Matrix3<T> &m) const
{
    Matrix3<T> temp(Vector3<T>(a.x * m.a.x + a.y * m.b.x + a.z * m.c.x,
                               a.x * m.a.y + a.y * m.b.y + a.z * m.c.y,
                               a.x * m.a.z + a.y * m.b.z + a.z * m.c.z),
                    Vector3<T>(b.x * m.a.x + b.y * m.b.x + b.z * m.c.x,
                               b.x * m.a.y + b.y * m.b.y + b.z * m.c.y,
                               b.x * m.a.z + b.y * m.b.z + b.z * m.c.z),
                    Vector3<T>(c.x * m.a.x + c.y * m.b.x + c.z * m.c.x,
                               c.x * m.a.y + c.y * m.b.y + c.z * m.c.y,
                               c.x * m.a.z + c.y * m.b.z + c.z * m.c.z));


    return temp;
}


template <typename T>
Matrix3<T> Matrix3<T>::transposed(void) const
{
    return Matrix3<T>(Vector3<T>(a.x, b.x, c.x),
                      Vector3<T>(a.y, b.y, c.y),
                      Vector3<T>(a.z, b.z, c.z));
}

template <typename T>
void Matrix3<T>::zero(void)
{
    a.x = a.y = a.z = 0;
    b.x = b.y = b.z = 0;
    c.x = c.y = c.z = 0;
}

// multiplication of transpose by a vector
template <typename T>
Vector3<T> Vector3<T>::operator *(const Matrix3<T> &m) const
{
    return Vector3<T>(*this * m.colx(),
                      *this * m.coly(),
                      *this * m.colz());
}

// multiply a column vector by a row vector, returning a 3x3 matrix
template <typename T>
Matrix3<T> Vector3<T>::mul_rowcol(const Vector3<T> &v2) const
{
    const Vector3<T> v1 = *this;

    return Matrix3<T>(v1.x * v2.x, v1.x * v2.y, v1.x * v2.z,
                      v1.y * v2.x, v1.y * v2.y, v1.y * v2.z,
                      v1.z * v2.x, v1.z * v2.y, v1.z * v2.z);
}


template <typename T>
void Vector3<T>::matrix_rotate(const Matrix3<T> &rm)
{
    const Vector3<T> v1 = *this;

    x = rm.a.x * v1.x + rm.b.x * v1.y + rm.c.x * v1.z;
    y = rm.a.y * v1.x + rm.b.y * v1.y + rm.c.y * v1.z;
    z = rm.a.z * v1.x + rm.b.z * v1.y + rm.c.z * v1.z;
}

template <typename T>
void Matrix3<T>::matrix_scale(float scale)
{
    a.x *= scale;
    a.y *= scale;
    a.z *= scale;
    b.x *= scale;
    b.y *= scale;
    b.z *= scale;
    c.x *= scale;
    c.y *= scale;
    c.z *= scale;
}

template <typename T>
void Matrix3<T>::x_rotate(float rads)
{
    float s    = sinf(rads);
    float cval = cosf(rads);

    a.x = 1;
    a.y = 0;
    a.z = 0;

    b.x = 0;
    b.y = cval;
    b.z = s;

    c.x = 0;
    c.y = -s;
    c.z = cval;
}


template <typename T>
void Matrix3<T>::y_rotate(float rads)
{
    float s    = sinf(rads);
    float cval = cosf(rads);

    a.x = cval;
    a.y = 0;
    a.z = -s;

    b.x = 0;
    b.y = 1;
    b.z = 0;

    c.x = s;
    c.y = 0;
    c.z = cval;
}


template <typename T>
void Matrix3<T>::z_rotate(float rads)
{
    float s    = sinf(rads);
    float cval = cosf(rads);

    a.x = cval;
    a.y = s;
    a.z = 0;

    b.x = -s;
    b.y = cval;
    b.z = 0;

    c.x = 0;
    c.y = 0;
    c.z = 1;
}


// only define for float
template void Matrix3<float>::z_rotate(float rads);
template Vector3<float> Vector3<float>::operator *(const Matrix3<float> &m) const;
template Matrix3<float> Vector3<float>::mul_rowcol(const Vector3<float> &v) const;
template void Vector3<float>::matrix_rotate(const Matrix3<float> &rm);
template void Matrix3<float>::zero(void);
template void Matrix3<float>::normalize(void);
template void Matrix3<float>::from_euler(float roll, float pitch, float yaw);
template void Matrix3<float>::to_euler(float *roll, float *pitch, float *yaw) const;
template Vector3<float> Matrix3<float>::operator *(const Vector3<float> &v) const;
template Vector3<float> Matrix3<float>::mul_transpose(const Vector3<float> &v) const;
template Matrix3<float> Matrix3<float>::operator *(const Matrix3<float> &m) const;
template Matrix3<float> Matrix3<float>::transposed(void) const;
template Vector2<float> Matrix3<float>::mulXY(const Vector3<float> &v) const;
