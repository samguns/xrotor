/**
 ******************************************************************************
 *
 * @file       SettingUtils.cpp
 * @author     Alex Beck
 *
 * @brief      Utilities for settings
 * @see        The GNU Public License (GPL) Version 3
 * @defgroup
 * @{
 *
 *****************************************************************************/
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
 * for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
extern "C" {
#include "openpilot.h"
#include "pios_config.h"
#include "mpugyroaccelsettings.h"
}
#include "settingutils.h"
#include "mathmisc.h"


#ifdef PIOS_INCLUDE_MPU9250
#define MPU_SAMPLE_FREQ
const uint32_t mpu_sample_freq[] = {
#ifdef PIOS_MPU9250_MAG
    4000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_256_HZ
#else /* PIOS_MPU9250_MAG */
    8000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_256_HZ
#endif /* PIOS_MPU9250_MAG */
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_188_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_98_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_42_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_20_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_10_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_5_HZ
#ifdef PIOS_MPU9250_MAG
    4000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_3600_HZ
#else /* PIOS_MPU9250_MAG */
    8000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_3600_HZ
#endif /* PIOS_MPU9250_MAG */
};
#endif

#ifdef PIOS_INCLUDE_MPU6000
#define MPU_SAMPLE_FREQ
const uint32_t mpu_sample_freq[] = {
    8000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_256_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_188_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_98_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_42_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_20_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_10_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_5_HZ
    1000, // MPUGYROACCELSETTINGS_FILTERSETTING_LOWPASS_3600_HZ // this is really 256Hz for revo
};
#endif

uint32_t getInnerLoopRate()
{
    uint32_t innerloopRate = 500;

    MPUGyroAccelSettingsData mpuSettings;

    MPUGyroAccelSettingsGet(&mpuSettings);

    switch (mpuSettings.MaxInnerLoopRate) {
    case MPUGYROACCELSETTINGS_MAXINNERLOOPRATE_500:
        innerloopRate = 500;
        break;
    case MPUGYROACCELSETTINGS_MAXINNERLOOPRATE_1000:
        innerloopRate = 1000;
        break;
    case MPUGYROACCELSETTINGS_MAXINNERLOOPRATE_1333:
        innerloopRate = 1333;
        break;
    case MPUGYROACCELSETTINGS_MAXINNERLOOPRATE_2000:
        innerloopRate = 2000;
        break;
    }

#ifdef MPU_SAMPLE_FREQ
    uint32_t gyro_sample_rate = mpu_sample_freq[mpuSettings.FilterSetting];
#else
    uint32_t gyro_sample_rate = 500;
#endif

    if (innerloopRate > gyro_sample_rate) {
        innerloopRate = gyro_sample_rate;
    }

    return innerloopRate;
}

// the state estimation rate is derived from the gyro rate.
// Options are:
// 500: 500
// 1000: 1000, 500
// 1333: 1333, 666
// 2000: 2000, 1000, 500
uint8_t getStateEstimationRate(uint32_t *stateEstimationRate)
{
    uint8_t divider = 1;
    uint32_t innerloopRate = getInnerLoopRate();

    MPUGyroAccelSettingsData mpuSettings;

    MPUGyroAccelSettingsGet(&mpuSettings);

    if (innerloopRate == 500) {
        *stateEstimationRate = 500;
        divider = 1;
    } else if (innerloopRate == 1000) {
        if (mpuSettings.StateEstimationRate == MPUGYROACCELSETTINGS_STATEESTIMATIONRATE_LOW) {
            *stateEstimationRate = 500;
            divider = 2;
        } else {
            *stateEstimationRate = 1000;
            divider = 1;
        }
    } else if (innerloopRate == 1333) {
        if (mpuSettings.StateEstimationRate == MPUGYROACCELSETTINGS_STATEESTIMATIONRATE_LOW) {
            *stateEstimationRate = 666;
            divider = 2;
        } else {
            *stateEstimationRate = 1333;
            divider = 1;
        }
    } else if (innerloopRate == 2000) {
        if (mpuSettings.StateEstimationRate == MPUGYROACCELSETTINGS_STATEESTIMATIONRATE_LOW) {
            *stateEstimationRate = 500;
            divider = 4;
        } else if (mpuSettings.StateEstimationRate == MPUGYROACCELSETTINGS_STATEESTIMATIONRATE_MID) {
            *stateEstimationRate = 1000;
            divider = 2;
        } else {
            *stateEstimationRate = 2000;
            divider = 1;
        }
    }
    return divider;
}
