package org.opng.uavtalk.uavobjectgenerator.parser;


import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.opng.uavtalk.uavobjectgenerator.common.Utilities;
import org.opng.uavtalk.uavobjectgenerator.parser.FieldType.Elementnames;
import org.opng.uavtalk.uavobjectgenerator.parser.FieldType.Options;
import org.opng.uavtalk.uavobjectgenerator.parser.ObjectType.Access;

public class Parser {
    // Types
    public enum FieldType {
        FIELDTYPE_INT8(0), FIELDTYPE_INT16(1), FIELDTYPE_INT32(2), FIELDTYPE_UINT8(
                3), FIELDTYPE_UINT16(4), FIELDTYPE_UINT32(5), FIELDTYPE_FLOAT32(
                6), FIELDTYPE_ENUM(7);

        int _value;

        private FieldType(int value) {
            _value = value;
        }

        public int getValue() {
            return _value;
        }
    }

    public class FieldInfo implements Cloneable{
        public String name;
        public String units;
        public FieldType type;
        public int numElements;
        int numBytes;
        public List<String> elementNames = new ArrayList<String>();
        public List<String> options = new ArrayList<String>(); // for enums only
        public boolean defaultElementNames;
        public List<String> defaultValues = new ArrayList<String>();
        public String limitValues;

        @Override
        public Object clone() {
            FieldInfo _FiledInfo = null;
            try {
                _FiledInfo = (FieldInfo)super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return _FiledInfo;
        }
    }

    /**
     * Object update mode
     */
    public enum UpdateMode {
        UPDATEMODE_MANUAL(0), /**
         * Manually update object, by calling the
         * updated() function
         */
        UPDATEMODE_PERIODIC(1), /**
         * Automatically update object at periodic
         * intervals
         */
        UPDATEMODE_ONCHANGE(2), /** Only update object when its data changes */
        UPDATEMODE_THROTTLED(3);
        /**
         * Object is updated on change, but not more often than the interval
         * time
         */

        int _value;

        private UpdateMode(int value) {
            _value = value;
        }

        public int getValue() {
            return _value;
        }
    }

    public enum AccessMode {
        ACCESS_READWRITE(0), ACCESS_READONLY(1);

        int _value;

        private AccessMode(int value) {
            _value = value;
        }

        public int getValue() {
            return _value;
        }
    }

    public class ObjectInfo {
        public String name;
        public String namelc;
        /** name in lowercase */
        public String filename;
        public int id;
        public boolean isSingleInst;
        public boolean isSettings;
        public boolean isPriority;
        public AccessMode gcsAccess;
        public AccessMode flightAccess;
        public boolean flightTelemetryAcked;
        public UpdateMode flightTelemetryUpdateMode;
        /** Update mode used by the autopilot (UpdateMode) */
        public int flightTelemetryUpdatePeriod;
        /**
         * Update period used by the autopilot (only if telemetry mode is
         * PERIODIC)
         */
        public boolean gcsTelemetryAcked;
        public UpdateMode gcsTelemetryUpdateMode;
        /** Update mode used by the GCS (UpdateMode) */
        public int gcsTelemetryUpdatePeriod;
        /** Update period used by the GCS (only if telemetry mode is PERIODIC) */
        public UpdateMode loggingUpdateMode;
        /** Update mode used by the logging module (UpdateMode) */
        public int loggingUpdatePeriod;
        /**
         * Update period used by the logging module (only if logging mode is
         * PERIODIC)
         */
        public List<FieldInfo> fields = new ArrayList<FieldInfo>();
        /** The data fields for the object **/
        public String description;
        /** Description used for Doxygen **/
        public String category;
        /** Description used for Doxygen **/
    }

    private List<ObjectInfo> _objInfo = new ArrayList<ObjectInfo>();
    private Set<String> _all_units = new HashSet<String>();

    /**
     * Get number of objects
     */
    public int getNumObjects() {
        return _objInfo.size();
    }

    /**
     * Get the detailed object information
     */
    public List<ObjectInfo> getObjectInfo() {
        return _objInfo;
    }

    public ObjectInfo getObjectByIndex(int objIndex) {
        return _objInfo.get(objIndex);
    }

    /**
     * Get the name of the object
     */
    String getObjectName(int objIndex) {
        ObjectInfo info = _objInfo.get(objIndex);

        if (info == null) {
            return "";
        }

        return info.name;
    }

    /**
     * Get the ID of the object
     */
    public int getObjectID(int objIndex) {
        ObjectInfo info = _objInfo.get(objIndex);

        if (info == null) {
            return 0;
        }
        return info.id;
    }

    /**
     * Get the number of bytes in the data fields of this object
     */
    public int getNumBytes(int objIndex) {
        ObjectInfo info = _objInfo.get(objIndex);

        if (info == null) {
            return 0;
        } else {
            int numBytes = 0;
            for (int n = 0; n < info.fields.size(); ++n) {
                FieldInfo fieldInfo = info.fields.get(n);
                numBytes += fieldInfo.numBytes * fieldInfo.numElements;
            }
            return numBytes;
        }
    }

    /**
     * Parse supplied XML file
     *
     * @param filename
     *            The xml filename
     * @throws JAXBException
     * @returns Null QString() on success, error message on failure
     */
    public String parseXML(String filename) throws JAXBException {
        String status = null;
        JAXBContext context = JAXBContext.newInstance(XmlType.class);
        Unmarshaller unMarshaller = context.createUnmarshaller();
        XmlType docContent = (XmlType) unMarshaller
                .unmarshal(new File(filename));

        // Read all objects contained in the XML file, creating an new
        // ObjectInfo for each
        for (ObjectType curObj : docContent.getObject()) {
            // Create new object entry
            ObjectInfo info = new ObjectInfo();

            Path p = Paths.get(filename);
            info.filename=p.getFileName().toString();
            // Process object attributes
            status = processObjectAttributes(curObj, info);
            if (status != null) {
                return status;
            }

            // Process child elements (fields and metadata)
            for (org.opng.uavtalk.uavobjectgenerator.parser.FieldType curField : curObj
                    .getField()) {

                status = processObjectFields(curField, info);
                if (status != null) {
                    return status;
                }
            }

            processObjectAccess(curObj.getAccess(), info);
            
            TelemetryType telemetrygcs = curObj.getTelemetrygcs();
            info.gcsTelemetryUpdateMode = xmlUpdateModeToUpdateMode(telemetrygcs.getUpdatemode());
            info.gcsTelemetryUpdatePeriod = telemetrygcs.getPeriod();
            info.gcsTelemetryAcked = telemetrygcs.isAcked();
            
            TelemetryType telemetryflight = curObj.getTelemetryflight();
            info.flightTelemetryUpdateMode = xmlUpdateModeToUpdateMode(telemetryflight.getUpdatemode());
            info.flightTelemetryUpdatePeriod = telemetryflight.getPeriod();
            info.flightTelemetryAcked = telemetryflight.isAcked();
            
            LoggingType logging = curObj.getLogging();
            info.loggingUpdateMode = xmlUpdateModeToUpdateMode(logging.getUpdatemode());
            info.loggingUpdatePeriod = logging.getPeriod();

            processObjectDescription(curObj.getDescription(), info);

            // Sort all fields according to size
            Collections.sort(info.fields, new Comparator<FieldInfo>() {

                @Override
                public int compare(FieldInfo o1, FieldInfo o2) {
                    return Integer.valueOf(o2.numBytes).compareTo(
                            Integer.valueOf(o1.numBytes));
                }
            });

            // Calculate ID
            calculateID(info);

            // Add object
            _objInfo.add(info);
        }

        // Done, return null string
        return status;
    }

    private void processObjectDescription(String description, ObjectInfo info) {
        info.description = description;
    }

    private void processObjectAccess(Access access, ObjectInfo info) {
        info.gcsAccess = xmlAccessTypeToAccessMode(access.gcs);
        info.flightAccess = xmlAccessTypeToAccessMode(access.flight);
    }

    private String processObjectFields(
            org.opng.uavtalk.uavobjectgenerator.parser.FieldType xmlField,
            ObjectInfo info) {

        // Create field
        FieldInfo field = new FieldInfo();

        // Get name attribute
        String name = xmlField.getName();

        // Check to see is this field is a clone of another
        // field that has already been declared
        String parentName = xmlField.getCloneof();
        if (!Utilities.isNullOrEmpty(parentName)) {
            for (FieldInfo parent : info.fields) {
                if (parent.name.equals(parentName)) {
                    // clone from this parent
                    field = (FieldInfo)parent.clone(); // safe shallow copy, no ptrs in struct

                    field.name = name; // set our name
                    // Add field to object
                    info.fields.add(field);
                    return null;
                }
            }
            return "Object:field::cloneof parent unknown";
        } else {
            // this field is not a clone, so remember its name
            field.name = name;
        }

        // Get units attribute
        field.units = xmlField.getUnits();
        _all_units.add(field.units);

        // Get type attribute
        FieldTypeType xmlFieldType = xmlField.getType();
        field.type = xmlFieldTypeToFieldType(xmlFieldType);
        field.numBytes = fieldTypeToNumBytes(xmlFieldType);

        // Get numelements or elementnames attribute
        field.numElements = 0;
        // Look for element names as an attribute first
        String elementNames = xmlField.getElementnames();
        if (!Utilities.isNullOrEmpty(elementNames)) {
            // Get element names
            List<String> names = new ArrayList<String>();
            for (String curName : elementNames.split(",")) {
                names.add(curName.trim());
            }

            field.elementNames = names;
            field.numElements = names.size();
            field.defaultElementNames = false;
        } else {
            // Look for a list of child elementname nodes
            Elementnames elementNamesList = xmlField.getElementnames2();
            if (elementNamesList != null) {
                for (String curName : elementNamesList.getElementname()) {
                    field.elementNames.add(curName);
                }
                field.numElements = field.elementNames.size();
                field.defaultElementNames = false;
            }
        }
        // If no element names were found, then fall back to looking
        // for the number of elements in the 'elements' attribute
        if (field.numElements == 0) {
            field.numElements = xmlField.getElements();
            for (int n = 0; n < field.numElements; ++n) {
                field.elementNames.add(Integer.toString(n));
            }

            field.defaultElementNames = true;
        }

        // Get options attribute or child elements (only if an enum type)
        if (field.type == FieldType.FIELDTYPE_ENUM) {
            // Look for options attribute
            String optionsString = xmlField.getOptions();
            if (!Utilities.isNullOrEmpty(optionsString)) {
                List<String> options = new ArrayList<String>();
                for (String curOption : optionsString.split(",")) {
                    options.add(curOption.trim());
                }
                field.options = options;
            } else {
                // Look for a list of child 'option' nodes
                Options optionsList = xmlField.getOptions2();
                if (optionsList != null) {
                    for (String curOption : optionsList.getOption()) {
                        field.options.add(curOption);
                    }
                }
            }
            if (field.options.size() == 0) {
                return "Object:field:options attribute/element is missing";
            }
        }

        // Get the default value attribute (required for settings objects,
        // optional for the rest)
        String defaultValue = xmlField.getDefaultvalue();
        if (defaultValue == null) {
            if (info.isSettings) {
                return "Object:field:defaultvalue attribute is missing (required for settings objects)";
            }
        } else {
            List<String> defaults = new ArrayList<String>();
            for (String curDefault : xmlField.getDefaultvalue().split(",")) {
                defaults.add(curDefault.trim());
            }

            if (defaults.size() != field.numElements) {
                if (defaults.size() != 1) {
                    return "Object:field:incorrect number of default values";
                }

                /*
                 * support legacy single default for multiple elements We should
                 * really issue a warning
                 */
                for (int ct = 1; ct < field.numElements; ct++) {
                    defaults.add(defaults.get(0));
                }
            }
            field.defaultValues = defaults;
        }

        // Limits attribute
        String limits = xmlField.getLimits();
        if (limits == null) {
            field.limitValues = "";
        } else {
            field.limitValues = limits;
        }
        // Add field to object
        info.fields.add(field);
        return null;
    }

    private int fieldTypeToNumBytes(FieldTypeType xmlFieldType) {
        int retVal = 0;
        
        switch(xmlFieldType){
        case ENUM:
            retVal = 1;
            break;
        case FLOAT:
            retVal = 4;
            break;
        case INT_8:
            retVal = 1;
            break;
        case INT_16:
            retVal = 2;
            break;
        case INT_32:
            retVal = 4;
            break;
        case UINT_8:
            retVal = 1;
            break;
        case UINT_16:
            retVal = 2;
            break;
        case UINT_32:
            retVal = 4;
            break;
        }
        
        return retVal;
    }

    private String processObjectAttributes(ObjectType curObj, ObjectInfo info) {
        String error = null;

        // Get name attribute
        info.name = curObj.getName();
        info.namelc = curObj.getName().toLowerCase();

        // Get category attribute if present
        String attr = curObj.getCategory();
        if (attr != null) {
            info.category = attr;
        }

        // Get singleinstance attribute
        info.isSingleInst = curObj.isSingleinstance();

        // Get settings attribute
        info.isSettings = curObj.isSettings();

        // Get priority attribute
        info.isPriority = curObj.isPriority();

        // Settings objects can only have a single instance
        if (info.isSettings && !info.isSingleInst) {
            error = "Object: Settings objects can not have multiple instances";
        }

        // Done
        return error;
    }

    private UpdateMode xmlUpdateModeToUpdateMode(UpdateModeType updatemode) {
        UpdateMode retVal = null;
        
        switch(updatemode){
        case MANUAL:
            retVal = UpdateMode.UPDATEMODE_MANUAL;
            break;
        case ONCHANGE:
            retVal = UpdateMode.UPDATEMODE_ONCHANGE;
            break;
        case PERIODIC:
            retVal = UpdateMode.UPDATEMODE_PERIODIC;
            break;
        case THROTTLED:
            retVal = UpdateMode.UPDATEMODE_THROTTLED;
            break;
        }
        
        return retVal;
    }

    private FieldType xmlFieldTypeToFieldType(FieldTypeType xmlFieldType) {
        FieldType retVal = null;
        
        switch(xmlFieldType){
        case ENUM:
            retVal = FieldType.FIELDTYPE_ENUM;
            break;
        case FLOAT:
            retVal = FieldType.FIELDTYPE_FLOAT32;
            break;
        case INT_8:
            retVal = FieldType.FIELDTYPE_INT8;
            break;
        case INT_16:
            retVal = FieldType.FIELDTYPE_INT16;
            break;
        case INT_32:
            retVal = FieldType.FIELDTYPE_INT32;
            break;
        case UINT_8:
            retVal = FieldType.FIELDTYPE_UINT8;
            break;
        case UINT_16:
            retVal = FieldType.FIELDTYPE_UINT16;
            break;
        case UINT_32:
            retVal = FieldType.FIELDTYPE_UINT32;
            break;
        }
        
        return retVal;
    }

    private AccessMode xmlAccessTypeToAccessMode(AccessType accessType) {
        AccessMode retVal = AccessMode.ACCESS_READONLY;

        switch (accessType) {
        case READWRITE:
            retVal = AccessMode.ACCESS_READWRITE;
            break;
        default:
            break;
        }
        return retVal;
    }

    /**
     * Calculate the unique object ID based on the object information. The ID
     * will change if the object definition changes, this is intentional and is
     * used to avoid connecting objects with incompatible configurations. The
     * LSB is set to zero and is reserved for metadata
     */
    void calculateID(ObjectInfo info) {
        // Hash object name
        int hash = updateHash(info.name, 0);

        // Hash object attributes
        hash = updateHash(info.isSettings ? 1 : 0, hash);
        hash = updateHash(info.isSingleInst ? 1 : 0, hash);
        // Hash field information
        for (int n = 0; n < info.fields.size(); ++n) {
            FieldInfo fieldInfo = info.fields.get(n);
            hash = updateHash(fieldInfo.name, hash);
            hash = updateHash(fieldInfo.numElements, hash);
            hash = updateHash(fieldInfo.type.getValue(), hash);
            if (fieldInfo.type == FieldType.FIELDTYPE_ENUM) {
                List<String> options = fieldInfo.options;
                for (int m = 0; m < options.size(); m++) {
                    hash = updateHash(options.get(m), hash);
                }
            }
        }
        // Done
        info.id = hash & 0xFFFFFFFE;
    }

    /**
     * Shift-Add-XOR hash implementation. LSB is set to zero, it is reserved for
     * the ID of the metaobject.
     * 
     * http://eternallyconfuzzled.com/tuts/algorithms/jsw_tut_hashing.aspx
     */
    int updateHash(int value, int hash) {
        return hash ^ ((hash << 5) + (hash >>> 2) + value);
    }

    /**
     * Update the hash given a string
     */
    int updateHash(String value, int hash) {
        byte[] bytes = value.getBytes();
        int hashout = hash;

        for (int n = 0; n < bytes.length; ++n) {
            hashout = updateHash(bytes[n], hashout);
        }

        return hashout;
    }
}
