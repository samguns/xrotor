package org.opng.uavtalk.uavobjectgenerator;

import org.opng.uavtalk.uavobjectgenerator.generator.FlightGenerator;
import org.opng.uavtalk.uavobjectgenerator.generator.JavaGenerator;
import org.opng.uavtalk.uavobjectgenerator.generator.JsGenerator;
import org.opng.uavtalk.uavobjectgenerator.generator.JsonGenerator;
import org.opng.uavtalk.uavobjectgenerator.parser.Parser;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class appMain {
    public static void main(String[] args) {
        System.out.println("- OPNG UAVObject Generator -");

        List<String> arguments_stringlist = new ArrayList<String>();
        for (String arg : args) {
            arguments_stringlist.add(arg);
        }

        boolean help = arguments_stringlist.remove("-h");
        if (help) {
            usage();
            return;
        }

        boolean do_flight = arguments_stringlist.remove("-flight");
        boolean do_java = arguments_stringlist.remove("-java");
        boolean do_json = arguments_stringlist.remove("-json");
        boolean do_js = arguments_stringlist.remove("-js");

        String input_path;
        String template_path;
        if (arguments_stringlist.size() == 2) {
            input_path = arguments_stringlist.get(0);
            template_path = arguments_stringlist.get(1);
        } else {
            usage_err();
            return;
        }

        if (!input_path.endsWith("/")) {
            input_path += "/";
        }
        if (!template_path.endsWith("/")) {
            template_path += "/";
        }

        File xmlPath = new File(input_path);
        File[] xmlList = xmlPath.listFiles();
        Parser parser = new Parser();
        for (File fileinfo : xmlList) {
            if (fileinfo.isFile() && fileinfo.getName().endsWith(".xml")) {
                try {
                    String res = parser.parseXML(fileinfo.getAbsolutePath());
                    if (res != null) {
                        System.out.format("Fail to parse %s: %s\n", fileinfo.getName(), res);
                    }
                } catch (JAXBException e) {
                    e.printStackTrace();
                }
            }
        }

        String output_path = "./";

        if (do_java) {
            System.out.println("generating java code");
            JavaGenerator generator = new JavaGenerator();
            generator.generate(parser, template_path, output_path);
        }

        if (do_flight) {
            System.out.println("generating flight code");
            FlightGenerator generator = new FlightGenerator();
            generator.generate(parser, template_path, output_path);
        }

        if (do_json) {
            System.out.println("generating json code");
            JsonGenerator generator = new JsonGenerator();
            generator.generate(parser, template_path, output_path);
        }

        if (do_js) {
            System.out.println("generating javascript code");
            JsGenerator generator = new JsGenerator();
            generator.generate(parser, template_path, output_path);
        }
    }

    private static void usage_err() {
        System.out.println("Invalid usage!");
        usage();
    }

    private static void usage() {
        System.out.println("Usage: uavobjectgenerator [language] xml_path template_base");
        System.out.println("Languages: ");
        System.out.println("\t-flight        build flight code");
        System.out.println("\t-java          build java code");
        System.out.println("\t-json          build json code");
        System.out.println("\t-js            build javascript code");
        System.out.println("\tIf no language is specified none are built - just parse xmls.");
        System.out.println("Misc: ");
        System.out.println("\t-h             this help");
        System.out.println("\tinput_path     path to UAVObject definition (.xml) files.");
        System.out.println("\ttemplate_path  path to the root of the template source tree.");
    }
}
