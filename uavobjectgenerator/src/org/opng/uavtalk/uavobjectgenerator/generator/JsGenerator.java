package org.opng.uavtalk.uavobjectgenerator.generator;

import org.opng.uavtalk.uavobjectgenerator.common.Utilities;
import org.opng.uavtalk.uavobjectgenerator.parser.Parser;

import java.io.File;
import java.util.Locale;

public class JsGenerator {
    private String jsCodeTemplate = "";
    private File jsCodePath;
    private File jsOutputPath;
    private String[] fieldTypeStrC;

    public boolean generate(Parser parser, String templatepath,
                            String outputpath) {
        fieldTypeStrC = new String[] {"int8", "int16", "int32", "uint8",
                "uint16", "uint32", "float", "enum"};
        jsCodePath = new File(templatepath + "js");
        jsOutputPath = new File(outputpath);
        jsOutputPath.mkdir();

        jsCodeTemplate = Utilities.readFile(new File(jsCodePath,
                "uavobject.js.template"));

        String jsInitTemplate = Utilities.readFile(new File(jsCodePath,
                "uavobjectsinit.js.template"));

        if ((Utilities.isNullOrEmpty(jsCodeTemplate))  || Utilities.isNullOrEmpty(jsInitTemplate)) {
            System.out.println("Error: Could not open javascript template files.");
        }

        StringBuilder jsObjInit = new StringBuilder();

        for (Parser.ObjectInfo info : parser.getObjectInfo()) {
            processObject(info);

            jsObjInit.append(String.format("    objMngr.registerObject(%s.createObj());\n", info.name));
        }

        // Write the gcs object inialization files
        jsInitTemplate = jsInitTemplate.replace("$(OBJINIT)", jsObjInit);
        boolean res = Utilities.writeFileIfDiffrent(new File(jsOutputPath,
                "uavobjectsinit.js"), jsInitTemplate);
        if (!res) {
            System.out.println("Error: Could not write the BIG json output files");
            return false;
        }

        return true;
    }

    /**
     * Generate the json object files
     */
    private boolean processObject(Parser.ObjectInfo info) {
        if (info == null) {
            return false;
        }

        // Prepare output strings
        String outCode = jsCodeTemplate;

        // Replace common tags
        outCode = Utilities.replaceCommonTags(outCode, info);

        // Replace the $(DATAFIELDS) tag
        String type;
        int numberOfFields = 0;
        StringBuilder fields = new StringBuilder();
        boolean first_field = true;
        for (Parser.FieldInfo curInfo : info.fields) {
            numberOfFields++;
            if (first_field) {
                fields.append("{\n");
                first_field = false;
            } else {
                fields.append("        {\n");
            }

            fields.append(String.format("                \"name\": \"%s\",\n", curInfo.name));

            // Determine type
            type = fieldTypeStrC[curInfo.type.getValue()];
            fields.append(String.format("                \"type\": \"%s\",\n", type));

            // Determine unit
            fields.append(String.format("                \"units\": \"%s\",\n", curInfo.units));

            if ((curInfo.numElements > 1) ||
                    ((curInfo.numElements == 1) && (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM))) {
                fields.append(String.format("                \"numElements\": \"%d\",\n", curInfo.numElements));
            } else {
                fields.append(String.format("                \"numElements\": \"%d\"\n", curInfo.numElements));
            }

            // Only for enum types
            if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                fields.append("                \"options\": [");
                for (int idx = 0; idx < curInfo.options.size(); ++idx) {
                    String option = curInfo.options.get(idx);
                    fields.append(String.format("\"%s\"", option));
                    if ((idx + 1) < curInfo.options.size()) {
                        fields.append(", ");
                    }
                }

                if (curInfo.numElements > 1) {
                    fields.append("],\n");
                } else {
                    fields.append("]\n");
                }
            }

            if (curInfo.numElements > 1) {
                fields.append("                \"elements\": [\n");
                for (int idx = 0; idx < curInfo.numElements; ++idx) {
                    String curName = curInfo.elementNames.get(idx);
                    fields.append(String.format("                    \"%s\"", curName));

                    if ((idx + 1) >= curInfo.numElements) {
                        fields.append("\n");
                    } else {
                        fields.append(",\n");
                    }
                }
                fields.append("                ]\n");
            }

            if (numberOfFields >= info.fields.size()) {
                fields.append("            }\n");
            } else {
                fields.append("            },\n");
            }
        }
        outCode = outCode.replace("$(DATAFIELDS)", fields);

        // Replace the $(INITFIELDS) tag
        StringBuilder initfields = new StringBuilder();
        for (Parser.FieldInfo curInfo : info.fields) {
            if (curInfo.defaultValues.isEmpty()) {
                continue;
            }

            // For non-array fields
            if (curInfo.numElements == 1) {
                if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                    initfields.append(String.format(
                            "    obj.getField(\"%s\").setValue(\"%s\");\n",
                            curInfo.name, curInfo.defaultValues.get(0)));
                } else if (curInfo.type == Parser.FieldType.FIELDTYPE_FLOAT32) {
                    initfields.append(String.format(
                            Locale.ENGLISH,
                            "    obj.getField(\"%s\").setValue(%f);\n",
                            curInfo.name,
                            Float.valueOf(curInfo.defaultValues.get(0))
                                    .floatValue()));
                } else {
                    initfields.append(String.format(
                            "    obj.getField(\"%s\").setValue(%d);\n",
                            curInfo.name,
                            Integer.valueOf(curInfo.defaultValues.get(0))
                                    .intValue()));
                }
            } else {
                // Initialize all fields in the array
                for (int idx = 0; idx < curInfo.numElements; ++idx) {
					String thisName = curInfo.elementNames.get(idx);
                    if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                        initfields
                                .append(String
                                        .format("    obj.getField(\"%s\").setValue(\"%s\",\"" +thisName+ "\");\n",
                                                curInfo.name,
                                                curInfo.defaultValues
                                                        .get(idx)));
                    } else if (curInfo.type == Parser.FieldType.FIELDTYPE_FLOAT32) {
                        initfields.append(String.format(
                                Locale.ENGLISH,
                                "    obj.getField(\"%s\").setValue(%f,\"" +thisName+ "\");\n",
                                curInfo.name,
                                Float.valueOf(
                                        curInfo.defaultValues.get(idx))
                                        .floatValue()));
                    } else {
                        initfields.append(String.format(
                                "    obj.getField(\"%s\").setValue(%d,\"" +thisName+ "\");\n",
                                curInfo.name,
                                Integer.valueOf(
                                        curInfo.defaultValues.get(idx))
                                        .intValue()));
                    }
                }
            }
        }

        outCode = outCode.replace("$(INITFIELDS)", initfields);

        // Write the json code
        boolean res = Utilities.writeFileIfDiffrent(new File(jsOutputPath, info.name
                + ".js"), outCode);
        if (!res) {
            System.out.println("Error: Could not write json output files");
            return false;
        }

        return true;
    }
}
