package org.opng.uavtalk.uavobjectgenerator.generator;

import org.opng.uavtalk.uavobjectgenerator.common.Utilities;
import org.opng.uavtalk.uavobjectgenerator.parser.Parser;

import java.io.File;

public class JsonGenerator {
    private String jsonCodeTemplate = "";
    private File jsonCodePath;
    private File jsonOutputPath;
    private String[] fieldTypeStrC;

    public boolean generate(Parser parser, String templatepath,
                            String outputpath) {
        fieldTypeStrC = new String[] {"int8", "int16", "int32", "uint8",
                "uint16", "uint32", "float", "enum"};
        jsonCodePath = new File(templatepath + "json");
        jsonOutputPath = new File(outputpath);
        jsonOutputPath.mkdir();

        jsonCodeTemplate = Utilities.readFile(new File(jsonCodePath,
                "uavobject.json.template"));

        if (Utilities.isNullOrEmpty(jsonCodeTemplate)) {
            System.out.println("Error: Could not open json template files.");
        }

        for (Parser.ObjectInfo info : parser.getObjectInfo()) {
            processObject(info);

        }

        return true;
    }

    /**
     * Generate the json object files
     */
    private boolean processObject(Parser.ObjectInfo info) {
        if (info == null) {
            return false;
        }

        // Prepare output strings
        String outCode = jsonCodeTemplate;

        // Replace common tags
        outCode = Utilities.replaceCommonTags(outCode, info);

        // Replace the $(DATAFIELDS) tag
        String type;
        int numberOfFields = 0;
        StringBuilder fields = new StringBuilder();
        boolean first_field = true;
        for (Parser.FieldInfo curInfo : info.fields) {
            numberOfFields++;
            if (first_field) {
                fields.append("{\n");
                first_field = false;
            } else {
                fields.append("        {\n");
            }

            fields.append(String.format("            \"name\": \"%s\",\n", curInfo.name));

            // Determine type
            type = fieldTypeStrC[curInfo.type.getValue()];
            fields.append(String.format("            \"type\": \"%s\",\n", type));

            // Determine unit
            fields.append(String.format("            \"units\": \"%s\",\n", curInfo.units));

            if ((curInfo.numElements > 1) ||
                    ((curInfo.numElements == 1) && (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM))) {
                fields.append(String.format("            \"numElements\": \"%d\",\n", curInfo.numElements));
            } else {
                fields.append(String.format("            \"numElements\": \"%d\"\n", curInfo.numElements));
            }

            // Only for enum types
            if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                fields.append("            \"options\": [");
                for (int idx = 0; idx < curInfo.options.size(); ++idx) {
                    String option = curInfo.options.get(idx);
                    fields.append(String.format("\"%s\"", option));
                    if ((idx + 1) < curInfo.options.size()) {
                        fields.append(", ");
                    }
                }

                if (curInfo.numElements > 1) {
                    fields.append("],\n");
                } else {
                    fields.append("]\n");
                }
            }

            if (curInfo.numElements > 1) {
                fields.append("            \"elements\": [\n");
                for (int idx = 0; idx < curInfo.numElements; ++idx) {
                    String curName = curInfo.elementNames.get(idx);
                    fields.append(String.format("                \"%s\"", curName));

                    if ((idx + 1) >= curInfo.numElements) {
                        fields.append("\n");
                    } else {
                        fields.append(",\n");
                    }
                }
                fields.append("            ]\n");
            }

            if (numberOfFields >= info.fields.size()) {
                fields.append("        }\n");
            } else {
                fields.append("        },\n");
            }
        }
        outCode = outCode.replace("$(DATAFIELDS)", fields);

        // Write the json code
        boolean res = Utilities.writeFileIfDiffrent(new File(jsonOutputPath, info.name
                + ".json"), outCode);
        if (!res) {
            System.out.println("Error: Could not write json output files");
            return false;
        }

        return true;
    }
}
