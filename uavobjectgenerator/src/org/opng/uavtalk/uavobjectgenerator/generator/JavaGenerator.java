package org.opng.uavtalk.uavobjectgenerator.generator;

import org.opng.uavtalk.uavobjectgenerator.common.Utilities;
import org.opng.uavtalk.uavobjectgenerator.parser.Parser;

import java.io.*;
import java.util.List;
import java.util.Locale;

public class JavaGenerator {
    // These special chars (regexp) will be removed from C/java identifiers
    private static final String ENUM_SPECIAL_CHARS = "[\\.\\-\\s\\+/\\(\\)]";

    private String javaCodeTemplate = "";
    private String javaIncludeTemplate = "";
    private String[] fieldTypeStrCPP;
    private String[] fieldTypeStrCPPClass;
    private File javaCodePath;
    private File javaOutputPath;

    public boolean generate(Parser parser, String templatepath,
                            String outputpath) {
        fieldTypeStrCPP = new String[] { "Byte", "Short", "Int", "Short",
                "Int", "Long", "Float", "Byte" };

        fieldTypeStrCPPClass = new String[] { "INT8", "INT16", "INT32",
                "UINT8", "UINT16", "UINT32", "FLOAT32", "ENUM" };

        javaCodePath = new File(templatepath + "java");
        javaOutputPath = new File(outputpath);
        javaOutputPath.mkdir();

        javaCodeTemplate = Utilities.readFile(new File(javaCodePath,
                "uavobject.java.template"));
        String javaInitTemplate = Utilities.readFile(new File(javaCodePath,
                "uavobjectsinit.java.template"));

        if (Utilities.isNullOrEmpty(javaCodeTemplate) || Utilities.isNullOrEmpty(javaInitTemplate)) {
            System.out.println("Error: Could not open java template files.");
            return false;
        }

        StringBuilder objInc = new StringBuilder();
        StringBuilder javaObjInit = new StringBuilder();

        for (Parser.ObjectInfo info : parser.getObjectInfo()) {
            processObject(info);

            javaObjInit.append("\t\t\tobjMngr.registerObject( new " + info.name
                    + "() );\n");
            objInc.append("#include \"" + info.namelc + ".h\"\n");
        }

        // Write the gcs object inialization files
        javaInitTemplate = javaInitTemplate.replace("$(OBJINC)", objInc);
        javaInitTemplate = javaInitTemplate.replace("$(OBJINIT)", javaObjInit);
        boolean res = Utilities.writeFileIfDiffrent(new File(javaOutputPath,
                "UAVObjectsInitialize.java"), javaInitTemplate);
        if (!res) {
            System.out.println("Error: Could not write output files");
            return false;
        }

        return true; // if we come here everything should be fine
    }

    /**
     * Generate the java object files
     */
    private boolean processObject(Parser.ObjectInfo info) {
        if (info == null) {
            return false;
        }

        // Prepare output strings
        String outInclude = javaIncludeTemplate;
        String outCode = javaCodeTemplate;
		StringBuilder fieldConstValues = new StringBuilder();

        // Replace common tags
        outInclude = Utilities.replaceCommonTags(outInclude, info);
        outCode = Utilities.replaceCommonTags(outCode, info);

        // Replace the $(DATAFIELDS) tag
        String type;
        StringBuilder fields = new StringBuilder();
        for (Parser.FieldInfo curInfo : info.fields) {
            // Determine type
            type = fieldTypeStrCPP[curInfo.type.getValue()];
            // Append field
            if (curInfo.numElements > 1) {
                fields.append(String.format("        %s %s[%d];\n", type,
                        curInfo.name, curInfo.numElements));
            } else {
                fields.append(String.format("        %s %s;\n", type,
                        curInfo.name));
            }
			
			fieldConstValues.append(String.format("\tpublic static final String FIELD_%s = \"%s\";\n",
						curInfo.name.toUpperCase().replaceAll("\\W", "_").replace("__", "_"), curInfo.name));
        }
        outInclude = outInclude.replace("$(DATAFIELDS)", fields);

        // Replace the $(FIELDSINIT) tag
        StringBuilder finit = new StringBuilder();
        for (Parser.FieldInfo curInfo : info.fields) {
            finit.append("\n");
			String varOptionNameConst = curInfo.name.toUpperCase();

            // Setup element names
            String varElemName = curInfo.name + "ElemNames";
            finit.append(String.format(
                    "\t\tList<String> %s = new ArrayList<>();\n",
                    varElemName));
            List<String> elemNames = curInfo.elementNames;
            for (String curName : elemNames) {
                finit.append(String.format("\t\t%s.add(\"%s\");\n",
                        varElemName, curName));
            }
			
			if (elemNames.size() > 1 && !elemNames.get(0).equals("0"))
				for (String curName : elemNames) {
					fieldConstValues.append(String.format("\tpublic static final String %s_%s = \"%s\";\n",
							varOptionNameConst, curName.toUpperCase().replaceAll("\\W", "_").replace("__", "_"), curName));
				}

            // Only for enum types
            if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                String varOptionName = curInfo.name + "EnumOptions";
				
                finit.append(String.format(
                        "\t\tList<String> %s = new ArrayList<>();\n",
                        varOptionName));
                List<String> options = curInfo.options;
                for (String curOption : options) {
                    finit.append(String.format("\t\t%s.add(\"%s\");\n",
                            varOptionName, curOption));
							
					fieldConstValues.append(String.format("\tpublic static final String %s_%s = \"%s\";\n",
						varOptionNameConst, curOption.toUpperCase().replaceAll("\\W", "_").replace("__", "_"), curOption));
                }
                finit.append(String
                        .format("\t\tfields.add( new UAVObjectField(\"%s\", \"%s\", UAVObjectField.FieldType.ENUM, %s, %s, \"%s\") );\n",
                                curInfo.name, curInfo.units, varElemName,
                                varOptionName, curInfo.limitValues));
            }
            // For all other types
            else {
                finit.append(String
                        .format("\t\tfields.add( new UAVObjectField(\"%s\", \"%s\", UAVObjectField.FieldType.%s, %s, null, \"%s\") );\n",
                                curInfo.name, curInfo.units,
                                fieldTypeStrCPPClass[curInfo.type.getValue()],
                                varElemName, curInfo.limitValues));
            }
        }
        outCode = outCode.replace("$(FIELDSVALUES)", fieldConstValues);
		outCode = outCode.replace("$(FIELDSINIT)", finit);

        // Replace the $(DATAFIELDINFO) tag
        StringBuilder enums = new StringBuilder();
        for (Parser.FieldInfo curInfo : info.fields) {
            enums.append(String.format("    // Field %s information\n",
                    curInfo.name));
            // Only for enum types
            if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                enums.append(String.format(
                        "    /* Enumeration options for field %s */\n",
                        curInfo.name));
                enums.append("    typedef enum { ");
                // Go through each option
                List<String> options = curInfo.options;
                for (int m = 0; m < options.size(); ++m) {
                    String s = (m != (options.size() - 1)) ? "%s_%s=%d, "
                            : "%s_%s=%d";
                    enums.append(String.format(
                            s,
                            curInfo.name.toUpperCase(),
                            options.get(m).toUpperCase()
                                    .replaceAll(ENUM_SPECIAL_CHARS, ""), m));
                }
                enums.append(String.format(" } %sOptions;\n", curInfo.name));
            }
            // Generate element names (only if field has more than one element)
            if (curInfo.numElements > 1 && !curInfo.defaultElementNames) {
                enums.append(String.format(
                        "    /* Array element names for field %s */\n",
                        curInfo.name));
                enums.append("    typedef enum { ");
                // Go through the element names
                List<String> elemNames = curInfo.elementNames;
                for (int m = 0; m < elemNames.size(); ++m) {
                    String s = (m != (elemNames.size() - 1)) ? "%s_%s=%d, "
                            : "%s_%s=%d";
                    enums.append(String.format(s, curInfo.name.toUpperCase(),
                            elemNames.get(m).toUpperCase(), m));
                }
                enums.append(String.format(" } %sElem;\n", curInfo.name));
            }
            // Generate array information
            if (curInfo.numElements > 1) {
                enums.append(String.format(
                        "    /* Number of elements for field %s */\n",
                        curInfo.name));
                enums.append(String.format(
                        "    static const quint32 %s_NUMELEM = %d;\n",
                        curInfo.name.toUpperCase(), curInfo.numElements));
            }
        }
        outInclude = outInclude.replace("$(DATAFIELDINFO)", enums);

        // Replace the $(INITFIELDS) tag
        StringBuilder initfields = new StringBuilder();
        for (Parser.FieldInfo curInfo : info.fields) {
            if (!curInfo.defaultValues.isEmpty()) {
                // For non-array fields
                if (curInfo.numElements == 1) {
                    if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                        initfields.append(String.format(
                                "\t\tgetField(\"%s\").setValue(\"%s\");\n",
                                curInfo.name, curInfo.defaultValues.get(0)));
                    } else if (curInfo.type == Parser.FieldType.FIELDTYPE_FLOAT32) {
                        initfields.append(String.format(
                        		Locale.ENGLISH,
                                "\t\tgetField(\"%s\").setValue(%f);\n",
                                curInfo.name,
                                Float.valueOf(curInfo.defaultValues.get(0))
                                        .floatValue()));
                    } else {
                        initfields.append(String.format(
                                "\t\tgetField(\"%s\").setValue(%d);\n",
                                curInfo.name,
                                Integer.valueOf(curInfo.defaultValues.get(0))
                                        .intValue()));
                    }
                } else {
                    // Initialize all fields in the array
                    for (int idx = 0; idx < curInfo.numElements; ++idx) {
                        if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                            initfields
                                    .append(String
                                            .format("\t\tgetField(\"%s\").setValue(\"%s\",%d);\n",
                                                    curInfo.name,
                                                    curInfo.defaultValues
                                                            .get(idx), idx));
                        } else if (curInfo.type == Parser.FieldType.FIELDTYPE_FLOAT32) {
                            initfields.append(String.format(
                            		Locale.ENGLISH,
                                    "\t\tgetField(\"%s\").setValue(%f,%d);\n",
                                    curInfo.name,
                                    Float.valueOf(
                                            curInfo.defaultValues.get(idx))
                                            .floatValue(), idx));
                        } else {
                            initfields.append(String.format(
                                    "\t\tgetField(\"%s\").setValue(%d,%d);\n",
                                    curInfo.name,
                                    Integer.valueOf(
                                            curInfo.defaultValues.get(idx))
                                            .intValue(), idx));
                        }
                    }
                }
            }
        }

        outCode = outCode.replace("$(INITFIELDS)", initfields);

        // Write the java code
        boolean res = Utilities.writeFileIfDiffrent(new File(javaOutputPath, info.name
                + ".java"), outCode);
        if (!res) {
            System.out.println("Error: Could not write java output files");
            return false;
        }

        return true;
    }
}
