package org.opng.uavtalk.uavobjectgenerator.generator;

import org.opng.uavtalk.uavobjectgenerator.common.Utilities;
import org.opng.uavtalk.uavobjectgenerator.parser.Parser;

import java.io.File;
import java.util.*;

public class FlightGenerator {
    // These special chars (regexp) will be removed from C/java identifiers
    private static final String ENUM_SPECIAL_CHARS = "[\\.\\-\\s\\+/\\(\\)]";

    private String flightCodeTemplate = "";
    private String flightIncludeTemplate = "";
    private String flightInitTemplate = "";
    private String flightInitIncludeTemplate = "";
    private String flightMakeTemplate = "";
    private String[] fieldTypeStrC;
    private File flightCodePath;
    private File flightOutputPath;

    public boolean generate(Parser parser, String templatepath,
                            String outputpath) {
        fieldTypeStrC = new String[] {"int8_t", "int16_t", "int32_t", "uint8_t",
                                    "uint16_t", "uint32_t", "float", "uint8_t"};
        flightCodePath = new File(templatepath + "flight");
        flightOutputPath = new File(outputpath);
        flightOutputPath.mkdir();

        flightCodeTemplate = Utilities.readFile(new File(flightCodePath,
                "uavobject.c.template"));
        flightIncludeTemplate = Utilities.readFile(new File(flightCodePath,
                "inc/uavobject.h.template"));
        flightInitTemplate = Utilities.readFile(new File(flightCodePath,
                "uavobjectsinit.c.template"));
        flightInitIncludeTemplate = Utilities.readFile(new File(flightCodePath,
                "inc/uavobjectsinit.h.template"));
        flightMakeTemplate = Utilities.readFile(new File(flightCodePath,
                "Makefile.inc.template"));

        if (Utilities.isNullOrEmpty(flightCodeTemplate) ||
                Utilities.isNullOrEmpty(flightIncludeTemplate) ||
                Utilities.isNullOrEmpty(flightInitTemplate)) {
            System.out.println("Error: Could not open flight template files.");
            return false;
        }

        int sizeCalc = 0;
        StringBuilder flightObjInit = new StringBuilder();
        StringBuilder objInc = new StringBuilder();
        StringBuilder objFileNames = new StringBuilder();
        StringBuilder objNames = new StringBuilder();
        for (int objidx = 0; objidx < parser.getNumObjects(); ++objidx) {
            Parser.ObjectInfo info = parser.getObjectByIndex(objidx);
            processObject(info);
            flightObjInit.append("#ifdef UAVOBJ_INIT_" + info.namelc + "\n");
            flightObjInit.append("    " + info.name + "Initialize();\n");
            flightObjInit.append("#endif\n");
            objInc.append("#include \"" + info.namelc + ".h\"\n");
            objFileNames.append(" " + info.namelc);
            objNames.append(" " + info.name);
            if (parser.getNumBytes(objidx) > sizeCalc) {
                sizeCalc = parser.getNumBytes(objidx);
            }
        }

        // Write the flight object initialization files
        flightInitTemplate = flightInitTemplate.replace("$(OBJINC)", objInc);
        flightInitTemplate = flightInitTemplate.replace("$(OBJINIT)", flightObjInit);
        boolean res = Utilities.writeFileIfDiffrent(new File(flightOutputPath,
                "uavobjectsinit.c"), flightInitTemplate);
        if (!res) {
            System.out.println("Error: Could not write output files");
            return false;
        }

        // Write the flight object initialization header
        flightInitIncludeTemplate = flightInitIncludeTemplate.replace("$(SIZECALCULATION)",
                Integer.toString(sizeCalc));
        res = Utilities.writeFileIfDiffrent(new File(flightOutputPath,
                "uavobjectsinit.h"), flightInitIncludeTemplate);
        if (!res) {
            System.out.println("Error: Could not write output files");
            return false;
        }

        // Write the flight object Makefile
        flightMakeTemplate = flightMakeTemplate.replace("$(UAVOBJFILENAMES)", objFileNames);
        flightMakeTemplate = flightMakeTemplate.replace("$(UAVOBJNAMES)", objNames);
        res = Utilities.writeFileIfDiffrent(new File(flightOutputPath,
                "Makefile.inc"), flightMakeTemplate);
        if (!res) {
            System.out.println("Error: Could not write output files");
            return false;
        }

        return true;
    }

    private boolean processObject(Parser.ObjectInfo info) {
        if (info == null) {
            return false;
        }

        // Prepare output strings
        String outInclude = flightIncludeTemplate;
        String outCode = flightCodeTemplate;

        // Replace common tags
        outInclude = Utilities.replaceCommonTags(outInclude, info);
        outCode = Utilities.replaceCommonTags(outCode, info);

        // Use the appropriate typedef for enums where we find them. Set up
        // that StringList here for use below.
        //
        List<String> typeStrList = new ArrayList<String>();
        for (Parser.FieldInfo field : info.fields) {
            if (field.type == Parser.FieldType.FIELDTYPE_ENUM) {
                typeStrList.add(String.format("%s%sOptions", info.name, field.name));
            } else {
                typeStrList.add(fieldTypeStrC[field.type.getValue()]);
            }
        }
        String typeList[] = new String[typeStrList.size()];
        typeStrList.toArray(typeList);

        // Replace the $(DATAFIELDS) tag
        String type;
        StringBuilder fields = new StringBuilder();
        StringBuilder dataStructures = new StringBuilder();

        for (int n = 0; n < info.fields.size(); ++n) {
            Parser.FieldInfo curInfo = info.fields.get(n);

            // Determine type
            type = typeList[n];
            // Append field
            // Check if it's a named set and creates structures accordingly
            if (curInfo.numElements > 1) {
                String tempElementNamesArray[] = new String[curInfo.elementNames.size()];
                curInfo.elementNames.toArray(tempElementNamesArray);
                if (tempElementNamesArray[0].compareTo("0") != 0) {
                    String structTypeName = String.format("%s%sData", info.name, curInfo.name);
                    String structType = "typedef struct __attribute__ ((__packed__)) {\n";

                    for (String eleName : curInfo.elementNames) {
                        structType += String.format("    %s %s;\n", type, eleName);
                    }

                    structType += String.format("}  %s ;\n", structTypeName);
                    structType += "typedef struct __attribute__ ((__packed__)) {\n";
                    structType += String.format("    %s array[%s];\n", type, curInfo.elementNames.size());
                    structType += String.format("} __attribute__((__may_alias__)) %sArray ;\n", structTypeName);
                    structType += String.format("#define %s%sToArray( var ) UAVObjectFieldToArray( %s, var )\n\n",
                            info.name, curInfo.name, structTypeName);

                    dataStructures.append(structType);
                    fields.append(String.format("    %s %s;\n", structTypeName, curInfo.name));
                } else {
                    fields.append(String.format("    %s %s[%s];\n", type, curInfo.name, curInfo.numElements));
                }
            } else {
                fields.append(String.format("    %s %s;\n", type, curInfo.name));
            }
        }

        outInclude = outInclude.replace("$(DATAFIELDS)", fields);
        outInclude = outInclude.replace("$(DATASTRUCTURES)", dataStructures);

        // Replace the $(DATAFIELDINFO) tag
        StringBuilder enums = new StringBuilder();
        for (Parser.FieldInfo curInfo : info.fields) {
            enums.append("/* Field " + curInfo.name +" information */\n");
            // Only for enum types
            if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                enums.append("\n// Enumeration options for field " + curInfo.name + "\n");
                enums.append("typedef enum __attribute__ ((__packed__)) {\n");
                // Go through each option
                List<String> options = curInfo.options;
                for (int m = 0; m < options.size(); ++m) {
                    String s = (m != (options.size() - 1)) ? "    %s_%s_%s=%d,\n"
                            : "    %s_%s_%s=%d\n";
                    enums.append(String.format(
                            s,
                            info.name.toUpperCase(),
                            curInfo.name.toUpperCase(),
                            options.get(m).toUpperCase()
                                    .replaceAll(ENUM_SPECIAL_CHARS, ""), m));
                }
                enums.append(String.format("} %s%sOptions;\n", info.name, curInfo.name));
            }
            // Generate element names (only if field has more than one element)
            if (curInfo.numElements > 1 && !curInfo.defaultElementNames) {
                enums.append(String.format(
                        "\n// Array element names for field %s\n",
                        curInfo.name));
                enums.append("typedef enum {\n");
                // Go through the element names
                List<String> elemNames = curInfo.elementNames;
                for (int m = 0; m < elemNames.size(); ++m) {
                    String s = (m != (elemNames.size() - 1)) ? "    %s_%s_%s=%d,\n"
                            : "    %s_%s_%s=%d\n";
                    enums.append(String.format(s,
                            info.name.toUpperCase(),
                            curInfo.name.toUpperCase(),
                            elemNames.get(m).toUpperCase(), m));
                }
                enums.append(String.format("} %s%sElem;\n",
                        info.name, curInfo.name));
            }
            // Generate array information
            if (curInfo.numElements > 1) {
                enums.append(String.format(
                        "\n// Number of elements for field %s\n",
                        curInfo.name));
                enums.append(String.format(
                        "#define %s_%s_NUMELEM %d\n",
                        info.name.toUpperCase(),
                        curInfo.name.toUpperCase(),
                        curInfo.numElements));
            }
            enums.append("\n");
        }
        outInclude = outInclude.replace("$(DATAFIELDINFO)", enums);

        // Replace the $(INITFIELDS) tag
        StringBuilder initfields = new StringBuilder();
        for (Parser.FieldInfo curInfo : info.fields) {
            if (!curInfo.defaultValues.isEmpty()) {
                // For non-array fields
                if (curInfo.numElements == 1) {
                    if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                        initfields.append(String.format("    data.%s = %s;\n",
                                curInfo.name,
                                curInfo.options.indexOf(curInfo.defaultValues.get(0))));
                    } else if (curInfo.type == Parser.FieldType.FIELDTYPE_FLOAT32) {
                        initfields.append(String.format(
                        		Locale.ENGLISH,
                        		"    data.%s = %.6ef;\n",
                                curInfo.name,
                                Float.valueOf(curInfo.defaultValues.get(0)).floatValue()));
                    } else {
                        initfields.append(String.format("    data.%s = %d;\n",
                                curInfo.name,
                                Integer.valueOf(curInfo.defaultValues.get(0)).intValue()));
                    }
                } else {
                    // Initialize all fields in the array
                    for (int idx = 0; idx < curInfo.numElements; ++idx) {
                        if (curInfo.elementNames.get(0).compareTo("0") == 0) {
                            initfields.append(String.format("    data.%s[%d] = ",
                                    curInfo.name, idx));
                        } else {
                            initfields.append(String.format("    data.%s.%s = ",
                                    curInfo.name, curInfo.elementNames.get(idx)));
                        }

                        if (curInfo.type == Parser.FieldType.FIELDTYPE_ENUM) {
                            initfields.append(String.format("%d;\n",
                                    curInfo.options.indexOf(curInfo.defaultValues.get(idx))));
                        } else if (curInfo.type == Parser.FieldType.FIELDTYPE_FLOAT32) {
                            initfields.append(String.format(
                            		Locale.ENGLISH,
                            		"%.6ef;\n",
                                    Float.valueOf(curInfo.defaultValues.get(idx)).floatValue()));
                        } else {
                            initfields.append(String.format("%d;\n",
                                    Integer.valueOf(curInfo.defaultValues.get(idx)).intValue()));
                        }
                    }
                }
            }
        }
        outCode = outCode.replace("$(INITFIELDS)", initfields);

        // Replace the $(SETGETFIELDS) tag
        StringBuilder setgetfields = new StringBuilder();
        for (int n = 0; n < info.fields.size(); ++n) {
            Parser.FieldInfo curInfo = info.fields.get(n);
            if (curInfo.numElements == 1) {
                /* Set */
                setgetfields.append(String.format("void %s%sSet(%s *New%s)\n",
                        info.name, curInfo.name,
                        typeList[n], curInfo.name));
                setgetfields.append("{\n");
                setgetfields.append(String.format("    UAVObjSetDataField(%sBase(), (void *)New%s, offsetof(%sData, %s), sizeof(%s));\n",
                        info.name, curInfo.name, info.name, curInfo.name, typeList[n]));
                setgetfields.append(("}\n"));

                /* Get */
                setgetfields.append(String.format("void %s%sGet(%s *New%s)\n",
                        info.name, curInfo.name,
                        typeList[n], curInfo.name));
                setgetfields.append("{\n");
                setgetfields.append(String.format("    UAVObjGetDataField(%sBase(), (void *)New%s, offsetof(%sData, %s), sizeof(%s));\n",
                        info.name, curInfo.name, info.name, curInfo.name, typeList[n]));
                setgetfields.append(("}\n"));
            } else {
                String suffix = "";

                if (curInfo.elementNames.get(0).compareTo("0") != 0) {
                    String structTypeName = String.format("%s%sData", info.name, curInfo.name);
                    /* Set */
                    setgetfields.append(String.format("void %s%sSet( %s *New%s )\n",
                            info.name, curInfo.name, structTypeName, curInfo.name));
                    setgetfields.append("{\n");
                    setgetfields.append(String.format("    UAVObjSetDataField(%sBase(), (void *)New%s, offsetof(%sData, %s), %d*sizeof(%s));\n",
                            info.name, curInfo.name, info.name, curInfo.name,
                            curInfo.numElements, typeList[n]));
                    setgetfields.append("}\n");

                    /* Get */
                    setgetfields.append(String.format("void %s%sGet( %s *New%s )\n",
                            info.name, curInfo.name, structTypeName, curInfo.name));
                    setgetfields.append("{\n");
                    setgetfields.append(String.format("    UAVObjGetDataField(%sBase(), (void *)New%s, offsetof(%sData, %s), %d*sizeof(%s));\n",
                            info.name, curInfo.name, info.name, curInfo.name,
                            curInfo.numElements, typeList[n]));
                    setgetfields.append("}\n");

                    suffix = "Array";
                }

                // array based field accessor
                /* Set */
                setgetfields.append(String.format("void %s%s%sSet( %s *New%s )\n",
                        info.name, curInfo.name, suffix, typeList[n], curInfo.name));
                setgetfields.append("{\n");
                setgetfields.append(String.format("    UAVObjSetDataField(%sBase(), (void *)New%s, offsetof(%sData, %s), %d*sizeof(%s));\n",
                        info.name, curInfo.name, info.name, curInfo.name,
                        curInfo.numElements, typeList[n]));
                setgetfields.append("}\n");

                /* Get */
                setgetfields.append(String.format("void %s%s%sGet( %s *New%s )\n",
                        info.name, curInfo.name, suffix, typeList[n], curInfo.name));
                setgetfields.append("{\n");
                setgetfields.append(String.format("    UAVObjGetDataField(%sBase(), (void *)New%s, offsetof(%sData, %s), %d*sizeof(%s));\n",
                        info.name, curInfo.name, info.name, curInfo.name,
                        curInfo.numElements, typeList[n]));
                setgetfields.append("}\n");
            }
        }

        outCode = outCode.replace("$(SETGETFIELDS)", setgetfields);

        // Replace the $(SETGETFIELDSEXTERN) tag
        StringBuilder setgetfieldsextern = new StringBuilder();
        for (int n = 0; n < info.fields.size(); ++n) {
            Parser.FieldInfo curInfo = info.fields.get(n);
            String suffix = "";
            if (curInfo.elementNames.get(0).compareTo("0") != 0) {
                // struct based field accessor
                String structTypeName = String.format("%s%sData", info.name, curInfo.name);

                /* SET */
                setgetfieldsextern.append(String.format("extern void %s%sSet(%s *New%s);\n",
                        info.name, curInfo.name, structTypeName, curInfo.name));

                /* GET */
                setgetfieldsextern.append(String.format("extern void %s%sGet(%s *New%s);\n",
                        info.name, curInfo.name, structTypeName, curInfo.name));
                suffix = "Array";
            }

            /* SET */
            setgetfieldsextern.append(String.format("extern void %s%s%sSet(%s *New%s);\n",
                    info.name, curInfo.name, suffix, typeList[n], curInfo.name));

            /* GET */
            setgetfieldsextern.append(String.format("extern void %s%s%sGet(%s *New%s);\n",
                    info.name, curInfo.name, suffix, typeList[n], curInfo.name));
        }

        outInclude = outInclude.replace("$(SETGETFIELDSEXTERN)", setgetfieldsextern);

        // Write the flight code
        boolean res = Utilities.writeFileIfDiffrent(new File(flightOutputPath, "/" + info.namelc +
                ".c"), outCode);
        if (!res) {
            System.out.println("Error: Could not write flight output files");
            return false;
        }

        res = Utilities.writeFileIfDiffrent(new File(flightOutputPath, "/" + info.namelc +
                ".h"), outInclude);
        if (!res) {
            System.out.println("Error: Could not write flight include files");
            return false;
        }

        return true;
    }
}
