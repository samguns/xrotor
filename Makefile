#
# Top level Makefile for the project build system.
# Copyright David Willis (c) 2010-2016
#
# Terms of Use:
# This software is the intellectual property of author and may not be
# sold or in other ways commercially redistributed without explicit written
# permission by the author.

# This top level Makefile passes down some variables to sub-makes through
# the environment. They are explicitly exported using the export keyword.
# Lower level makefiles assume that these variables are defined. To ensure
# that a special magic variable is exported here. It must be checked for
# existence by each sub-make.
export OPENPILOT_IS_COOL := Fuck Yeah!

# It is possible to set OPENPILOT_DL_DIR and/or OPENPILOT_TOOLS_DIR environment
# variables to override local tools download and installation directorys. So the
# same toolchains can be used for all working copies. Particularly useful for CI
# server build agents, but also for local installations.
#
# If no OPENPILOT_* variables found, makefile internal DL_DIR and TOOLS_DIR paths
# will be used. They still can be overridden by the make command line parameters:
# make DL_DIR=/path/to/download/directory TOOLS_DIR=/path/to/tools/directory targets...

# Function for converting Windows style slashes into Unix style
slashfix = $(subst \,/,$(1))

# Function for converting an absolute path to one relative
# to the top of the source tree
toprel = $(subst $(realpath $(ROOT_DIR))/,,$(abspath $(1)))

# Bamboo checks to simplify bamboo integration. Bamboo appends "bamboo_" to its environment variables.
# Bamboo plans can simply set these environment variables to pre-installed tools directory, and then
# execute make all to ensure all targets compile, make package to create the package artifact, and
# make build-info to make the build information artifact.
ifdef bamboo_OPENPILOT_DL_DIR 
export OPENPILOT_DL_DIR := $(bamboo_OPENPILOT_DL_DIR)
endif

ifdef bamboo_OPENPILOT_TOOLS_DIR 
export OPENPILOT_TOOLS_DIR := $(bamboo_OPENPILOT_TOOLS_DIR)
endif

# Set up some macros for common directories within the tree
export ROOT_DIR    := $(realpath $(dir $(lastword $(MAKEFILE_LIST))))
export DL_DIR      := $(if $(OPENPILOT_DL_DIR),$(call slashfix,$(OPENPILOT_DL_DIR)),$(ROOT_DIR)/downloads)
export TOOLS_DIR   := $(if $(OPENPILOT_TOOLS_DIR),$(call slashfix,$(OPENPILOT_TOOLS_DIR)),$(ROOT_DIR)/tools)
export BUILD_DIR   := $(ROOT_DIR)/build
export PACKAGE_DIR := $(ROOT_DIR)/build/package
export DIST_DIR    := $(ROOT_DIR)/build/dist
export CHROMEGCS_DIR := $(ROOT_DIR)/xground/chrome

DIRS = $(DL_DIR) $(TOOLS_DIR) $(BUILD_DIR) $(PACKAGE_DIR) $(DIST_DIR)

# Set up default build configurations (debug | release)
GCS_BUILD_CONF		:= release
GOOGLE_API_VERSION	:= 14

# Clean out undesirable variables from the environment and command-line
# to remove the chance that they will cause problems with our build
define SANITIZE_VAR
$(if $(filter-out undefined,$(origin $(1))),
    $(info $(EMPTY) NOTE        Sanitized $(2) variable '$(1)' from $(origin $(1)))
    MAKEOVERRIDES = $(filter-out $(1)=%,$(MAKEOVERRIDES))
    override $(1) :=
    unexport $(1)
)
endef

# These specific variables can influence compilation in unexpected (and undesirable) ways
# gcc flags
SANITIZE_GCC_VARS := TMPDIR GCC_EXEC_PREFIX COMPILER_PATH LIBRARY_PATH
# preprocessor flags
SANITIZE_GCC_VARS += CPATH C_INCLUDE_PATH CPLUS_INCLUDE_PATH OBJC_INCLUDE_PATH DEPENDENCIES_OUTPUT
# make flags
SANITIZE_GCC_VARS += CFLAGS CXXFLAGS CPPFLAGS LDFLAGS LDLIBS
$(foreach var, $(SANITIZE_GCC_VARS), $(eval $(call SANITIZE_VAR,$(var),disallowed)))

# These specific variables used to be valid but now they make no sense
SANITIZE_DEPRECATED_VARS := USE_BOOTLOADER CLEAN_BUILD
$(foreach var, $(SANITIZE_DEPRECATED_VARS), $(eval $(call SANITIZE_VAR,$(var),deprecated)))

# Make sure this isn't being run as root unless installing (no whoami on Windows, but that is ok here)
ifeq ($(shell whoami 2>/dev/null),root)
    ifeq ($(filter install,$(MAKECMDGOALS)),)
        ifndef FAKEROOTKEY
            $(error You should not be running this as root)
        endif
    endif
endif

# Decide on a verbosity level based on the V= parameter
export AT := @
ifndef V
    export V0    :=
    export V1    := $(AT)
else ifeq ($(V), 0)
    export V0    := $(AT)
    export V1    := $(AT)
else ifeq ($(V), 1)
endif

# Make sure we know few things about the architecture before including
# the tools.mk to ensure that we download/install the right tools.
UNAME := $(shell uname)
ARCH  := $(shell uname -m)
# Here and everywhere if not Linux or Mac then assume Windows
ifeq ($(filter Linux Darwin, $(UNAME)), )
    UNAME := Windows
endif

# Include tools installers
include $(ROOT_DIR)/make/tools.mk

# Include third party builders if available
-include $(ROOT_DIR)/make/3rdparty/3rdparty.mk

# We almost need to consider autoconf/automake instead of this
ifeq ($(UNAME), Linux)
    QT_SPEC = linux-g++
    OLD_UAVOBJGENERATOR = $(BUILD_DIR)/uavobjgenerator/uavobjgenerator
else ifeq ($(UNAME), Darwin)
    QT_SPEC = macx-g++
    OLD_UAVOBJGENERATOR = $(BUILD_DIR)/uavobjgenerator/uavobjgenerator
else ifeq ($(UNAME), Windows)
    QT_SPEC = win32-g++
    OLD_UAVOBJGENERATOR = $(BUILD_DIR)/uavobjgenerator/uavobjgenerator.exe
endif

UAVOBJGENERATOR = $(ROOT_DIR)/uavobjectgenerator/build/libs/uavobjectgenerator.jar

##############################
#
# All targets
#
##############################

.PHONY: all
all: uavobjects all_flight all_ground

.PHONY: all_clean
all_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR))"
	$(V1) [ ! -d "$(BUILD_DIR)" ] || $(RM) -rf "$(BUILD_DIR)"

.PONY: clean
clean: all_clean


##############################
#
# UAVObjects
#
##############################

UAVOBJGENERATOR_DIR = $(ROOT_DIR)/uavobjectgenerator
DIRS += $(UAVOBJGENERATOR_DIR)

.PHONY: uavobjgenerator
uavobjgenerator $(UAVOBJGENERATOR): | $(UAVOBJGENERATOR_DIR)
	$(V1) cd $(UAVOBJGENERATOR_DIR) && $(MAKE) --no-print-directory -w

UAVOBJ_TARGETS := flight json java js

.PHONY: uavobjects
uavobjects:  $(addprefix uavobjects_, $(UAVOBJ_TARGETS))

UAVOBJ_XML_DIR := $(ROOT_DIR)/shared/uavobjectdefinition
UAVOBJ_OUT_DIR := $(BUILD_DIR)/uavobject-synthetics
UAVOBJ_TEMPLATE_DIR := $(UAVOBJGENERATOR_DIR)/templates

uavobjects_%:  uavobjgenerator
	@$(MKDIR) -p $(UAVOBJ_OUT_DIR)/$*
	$(V1) ( cd $(UAVOBJ_OUT_DIR)/$* && \
	    java -jar $(UAVOBJGENERATOR) -$* $(UAVOBJ_XML_DIR) $(UAVOBJ_TEMPLATE_DIR) ; \
	)

uavobjects_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(UAVOBJ_OUT_DIR))"
	$(V1) [ ! -d "$(UAVOBJ_OUT_DIR)" ] || $(RM) -r "$(UAVOBJ_OUT_DIR)"
    
##############################
#
# Flight related components
#
##############################

# Define some pointers to the various important pieces of the flight code
# to prevent these being repeated in every sub makefile
export PROJECT_XFLIGHT := $(ROOT_DIR)/flight
export PIOS          := $(ROOT_DIR)/flight/pios
export FLIGHTLIB     := $(ROOT_DIR)/flight/libraries
export OPMODULEDIR   := $(ROOT_DIR)/flight/modules
export OPNEWLIBSDIR    := $(ROOT_DIR)/flight/newlibs
export OPUAVOBJ      := $(ROOT_DIR)/flight/uavobjects
export OPUAVTALK     := $(ROOT_DIR)/flight/uavtalk
export OPUAVSYNTHDIR := $(BUILD_DIR)/uavobject-synthetics/flight
export OPGCSSYNTHDIR := $(BUILD_DIR)/openpilotgcs-synthetics
export MINIMOSD      := $(BUILD_DIR)/minimosd

DIRS += $(OPGCSSYNTHDIR)

# Define supported board lists
ALL_BOARDS    := oplinkmini revolution simposix gpsplatinum revonano minimosd

# Short names of each board (used to display board name in parallel builds)
oplinkmini_short       := 'oplm'
revolution_short       := 'revo'
revonano_short         := 'revn'
simposix_short         := 'posx'
gpsplatinum_short      := 'gps9'
minimosd_short         := 'mini'

# SimPosix only builds on Linux so drop it from the list for
# all other platforms.
ifneq ($(UNAME), Linux)
    ALL_BOARDS := $(filter-out simposix, $(ALL_BOARDS))
endif

# Start out assuming that we'll build fw, bl and bu for all boards
FW_BOARDS  := $(ALL_BOARDS)
BL_BOARDS  := $(ALL_BOARDS)
BU_BOARDS  := $(ALL_BOARDS)
UT_BOARDS  := $(ALL_BOARDS)
EF_BOARDS  := $(ALL_BOARDS)

# SimPosix doesn't have a BL, BU or EF target so we need to
# filter them out to prevent errors on the all_flight target.
BL_BOARDS  := $(filter-out simposix minimosd, $(BL_BOARDS))
BU_BOARDS  := $(filter-out simposix gpsplatinum minimosd, $(BU_BOARDS))
UT_BOARDS  := $(filter-out simposix gpsplatinum minimosd oplinkmini revolution, $(BU_BOARDS))
EF_BOARDS  := $(filter-out simposix minimosd, $(EF_BOARDS))

# Generate the targets for whatever boards are left in each list
FW_TARGETS := $(addprefix fw_, $(FW_BOARDS))
BL_TARGETS := $(addprefix bl_, $(BL_BOARDS))
BU_TARGETS := $(addprefix bu_, $(BU_BOARDS))
UT_TARGETS := $(addprefix ut_, $(UT_BOARDS))
EF_TARGETS := $(addprefix ef_, $(EF_BOARDS))

# When building any of the "all_*" targets, tell all sub makefiles to display
# additional details on each line of output to describe which build and target
# that each line applies to. The same applies also to all, opfw_resource,
# package targets
ifneq ($(strip $(filter all_% all opfw_resource ,$(MAKECMDGOALS))),)
    export ENABLE_MSG_EXTRA := yes
endif

# When building more than one goal in a single make invocation, also
# enable the extra context for each output line
ifneq ($(word 2,$(MAKECMDGOALS)),)
    export ENABLE_MSG_EXTRA := yes
endif

# TEMPLATES (used to generate build rules)

# $(1) = Canonical board name all in lower case (e.g. coptercontrol)
# $(2) = Short name for board (e.g cc)
define FW_TEMPLATE
.PHONY: $(1) fw_$(1)
$(1): fw_$(1)_opfw
fw_$(1): fw_$(1)_opfw

fw_$(1)_%: uavobjects_flight
	$(V1) $$(ARM_GCC_VERSION_CHECK_TEMPLATE)
	$(V1) $$(AVR_GCC_VERSION_CHECK_TEMPLATE)
	$(V1) $(MKDIR) -p $(BUILD_DIR)/fw_$(1)/dep
	$(V1) cd $(ROOT_DIR)/flight/targets/boards/$(1)/firmware && \
		$$(MAKE) -r --no-print-directory \
		BUILD_TYPE=fw \
		BOARD_NAME=$(1) \
		BOARD_SHORT_NAME=$(2) \
		TOPDIR=$(ROOT_DIR)/flight/targets/boards/$(1)/firmware \
		OUTDIR=$(BUILD_DIR)/fw_$(1) \
		TARGET=fw_$(1) \
		$$*

.PHONY: $(1)_clean
$(1)_clean: fw_$(1)_clean
fw_$(1)_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR)/fw_$(1))"
	$(V1) $(RM) -fr $(BUILD_DIR)/fw_$(1)
endef

# $(1) = Canonical board name all in lower case (e.g. coptercontrol)
# $(2) = Short name for board (e.g cc)
define UT_TEMPLATE
.PHONY: $(1) ut_$(1)
$(1): ut_$(1)_opfw
ut_$(1): ut_$(1)_opfw

ut_$(1)_%: uavobjects_flight
	$(V1) $$(ARM_GCC_VERSION_CHECK_TEMPLATE)
	$(V1) $$(AVR_GCC_VERSION_CHECK_TEMPLATE)
	$(V1) $(MKDIR) -p $(BUILD_DIR)/ut_$(1)/dep
	$(V1) cd $(ROOT_DIR)/flight/targets/boards/$(1)/unittest && \
		$$(MAKE) -r --no-print-directory \
		BUILD_TYPE=ut \
		BOARD_NAME=$(1) \
		BOARD_SHORT_NAME=$(2) \
		TOPDIR=$(ROOT_DIR)/flight/targets/boards/$(1)/unittest \
		OUTDIR=$(BUILD_DIR)/ut_$(1) \
		TARGET=ut_$(1) \
		$$*

.PHONY: $(1)_clean
$(1)_clean: ut_$(1)_clean
ut_$(1)_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR)/ut_$(1))"
	$(V1) $(RM) -fr $(BUILD_DIR)/ut_$(1)
endef



# $(1) = Canonical board name all in lower case (e.g. coptercontrol)
# $(2) = Short name for board (e.g cc)
define BL_TEMPLATE
.PHONY: bl_$(1)
bl_$(1): bl_$(1)_bin
bl_$(1)_bino: bl_$(1)_bin

bl_$(1)_%:
	$(V1) $$(ARM_GCC_VERSION_CHECK_TEMPLATE)
	$(V1) $(MKDIR) -p $(BUILD_DIR)/bl_$(1)/dep
	$(V1) cd $(ROOT_DIR)/flight/targets/boards/$(1)/bootloader && \
		$$(MAKE) -r --no-print-directory \
		BUILD_TYPE=bl \
		BOARD_NAME=$(1) \
		BOARD_SHORT_NAME=$(2) \
		TOPDIR=$(ROOT_DIR)/flight/targets/boards/$(1)/bootloader \
		OUTDIR=$(BUILD_DIR)/bl_$(1) \
		TARGET=bl_$(1) \
		$$*

.PHONY: unbrick_$(1)
unbrick_$(1): bl_$(1)_hex
$(if $(filter-out undefined,$(origin UNBRICK_TTY)),
	$(V0) @$(ECHO) " UNBRICK    $(1) via $$(UNBRICK_TTY)"
	$(V1) $(STM32FLASH_DIR)/stm32flash \
		-w $(BUILD_DIR)/bl_$(1)/bl_$(1).hex \
		-g 0x0 \
		$$(UNBRICK_TTY)
,
	$(V0) @$(ECHO)
	$(V0) @$(ECHO) "ERROR: You must specify UNBRICK_TTY=<serial-device> to use for unbricking."
	$(V0) @$(ECHO) "       eg. $$(MAKE) $$@ UNBRICK_TTY=/dev/ttyUSB0"
)

.PHONY: bl_$(1)_clean
bl_$(1)_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR)/bl_$(1))"
	$(V1) $(RM) -fr $(BUILD_DIR)/bl_$(1)
endef

# $(1) = Canonical board name all in lower case (e.g. coptercontrol)
# $(2) = Short name for board (e.g cc)
define BU_TEMPLATE
.PHONY: bu_$(1)
bu_$(1): bu_$(1)_opfw

bu_$(1)_%: bl_$(1)_bino
	$(V1) $(MKDIR) -p $(BUILD_DIR)/bu_$(1)/dep
	$(V1) cd $(ROOT_DIR)/flight/targets/common/bootloader_updater && \
		$$(MAKE) -r --no-print-directory \
		BUILD_TYPE=bu \
		BOARD_NAME=$(1) \
		BOARD_SHORT_NAME=$(2) \
		TOPDIR=$(ROOT_DIR)/flight/targets/common/bootloader_updater \
		OUTDIR=$(BUILD_DIR)/bu_$(1) \
		TARGET=bu_$(1) \
		$$*

.PHONY: bu_$(1)_clean
bu_$(1)_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR)/bu_$(1))"
	$(V1) $(RM) -fr $(BUILD_DIR)/bu_$(1)
endef

# $(1) = Canonical board name all in lower case (e.g. coptercontrol)
# $(2) = Short name for board (e.g cc)
define EF_TEMPLATE
.PHONY: ef_$(1)
ef_$(1): ef_$(1)_bin

ef_$(1)_%: bl_$(1)_bin fw_$(1)_opfw
	$(V1) $(MKDIR) -p $(BUILD_DIR)/ef_$(1)
	$(V1) cd $(ROOT_DIR)/flight/targets/common/entire_flash && \
		$$(MAKE) -r --no-print-directory \
		BUILD_TYPE=ef \
		BOARD_NAME=$(1) \
		BOARD_SHORT_NAME=$(2) \
		DFU_CMD="$(DFUUTIL_DIR)/bin/dfu-util" \
		TOPDIR=$(ROOT_DIR)/flight/targets/common/entire_flash \
		OUTDIR=$(BUILD_DIR)/ef_$(1) \
		TARGET=ef_$(1) \
		$$*

.PHONY: ef_$(1)_clean
ef_$(1)_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR)/ef_$(1))"
	$(V1) $(RM) -fr $(BUILD_DIR)/ef_$(1)
endef

# $(1) = Canonical board name all in lower case (e.g. coptercontrol)
define BOARD_PHONY_TEMPLATE
.PHONY: all_$(1)
all_$(1): $$(filter fw_$(1), $$(FW_TARGETS))
all_$(1): $$(filter bl_$(1), $$(BL_TARGETS))
all_$(1): $$(filter bu_$(1), $$(BU_TARGETS))
all_$(1): $$(filter ut_$(1), $$(UT_TARGETS))
all_$(1): $$(filter ef_$(1), $$(EF_TARGETS))

.PHONY: all_$(1)_clean
all_$(1)_clean: $$(addsuffix _clean, $$(filter fw_$(1), $$(FW_TARGETS)))
all_$(1)_clean: $$(addsuffix _clean, $$(filter bl_$(1), $$(BL_TARGETS)))
all_$(1)_clean: $$(addsuffix _clean, $$(filter bu_$(1), $$(BU_TARGETS)))
all_$(1)_clean: $$(addsuffix _clean, $$(filter ut_$(1), $$(UT_TARGETS)))
all_$(1)_clean: $$(addsuffix _clean, $$(filter ef_$(1), $$(EF_TARGETS)))
endef

# Generate flight build rules
.PHONY: all_fw all_fw_clean
all_fw:        $(addsuffix _opfw,  $(FW_TARGETS))
all_fw_clean:  $(addsuffix _clean, $(FW_TARGETS))

.PHONY: all_bl all_bl_clean
all_bl:        $(addsuffix _bin,   $(BL_TARGETS))
all_bl_clean:  $(addsuffix _clean, $(BL_TARGETS))

.PHONY: all_bu all_bu_clean
all_bu:        $(addsuffix _opfw,  $(BU_TARGETS))
all_bu_clean:  $(addsuffix _clean, $(BU_TARGETS))

.PHONY: all_ut all_ut_clean
all_ut:        $(addsuffix _opfw,  $(UT_TARGETS))
all_ut_clean:  $(addsuffix _clean, $(UT_TARGETS))

.PHONY: all_ef all_ef_clean
all_ef:        $(EF_TARGETS)
all_ef_clean:  $(addsuffix _clean, $(EF_TARGETS))

.PHONY: all_flight all_flight_clean
all_flight:       all_fw all_bl all_bu all_ef all_ut
all_flight_clean: all_fw_clean all_bl_clean all_bu_clean all_ef_clean all_ut_clean

# Expand the groups of targets for each board
$(foreach board, $(ALL_BOARDS), $(eval $(call BOARD_PHONY_TEMPLATE,$(board))))

# Expand the firmware rules
$(foreach board, $(ALL_BOARDS), $(eval $(call FW_TEMPLATE,$(board),$($(board)_short))))

# Expand the firmware rules
$(foreach board, $(ALL_BOARDS), $(eval $(call UT_TEMPLATE,$(board),$($(board)_short))))

# Expand the bootloader rules
$(foreach board, $(ALL_BOARDS), $(eval $(call BL_TEMPLATE,$(board),$($(board)_short))))

# Expand the bootloader updater rules
$(foreach board, $(ALL_BOARDS), $(eval $(call BU_TEMPLATE,$(board),$($(board)_short))))

# Expand the entire-flash rules
$(foreach board, $(ALL_BOARDS), $(eval $(call EF_TEMPLATE,$(board),$($(board)_short))))

.PHONY: sim_win32
sim_win32: sim_win32_exe

sim_win32_%: uavobjects_flight
	$(V1) $(MKDIR) -p $(BUILD_DIR)/sitl_win32
	$(V1) $(MAKE) --no-print-directory \
		-C $(ROOT_DIR)/flight/targets/OpenPilot --file=$(ROOT_DIR)/flight/targets/OpenPilot/Makefile.win32 $*

.PHONY: sim_osx
sim_osx: sim_osx_elf

sim_osx_%: uavobjects_flight
	$(V1) $(MKDIR) -p $(BUILD_DIR)/sim_osx
	$(V1) $(MAKE) --no-print-directory \
		-C $(ROOT_DIR)/flight/targets/SensorTest --file=$(ROOT_DIR)/flight/targets/SensorTest/Makefile.osx $*

##############################
#
# GCS related components
#
##############################

.PHONY: all_ground
all_ground: all_flight androidgcs


################################
#
# Android GCS related components
#
################################

ANDROIDGCS_BUILD_CONF ?= debug

# Build the output directory for the Android GCS build
ANDROIDGCS_OUT_DIR := $(ROOT_DIR)/xground/android/OpenPilotAndroidGCS/build/outputs/apk

.PHONY: androidgcs
androidgcs: uavobjects $(ANDROIDGCS_OUT_DIR)/OpenPilotAndroidGCS-$(ANDROIDGCS_BUILD_CONF).apk

$(ANDROIDGCS_OUT_DIR)/OpenPilotAndroidGCS-$(ANDROIDGCS_BUILD_CONF).apk: 
	$(V1) make -f $(ROOT_DIR)/xground/android/OpenPilotAndroidGCS/Makefile androidgcs-$(ANDROIDGCS_BUILD_CONF)

.PHONY: androidgcs_clean
androidgcs_clean:
	$(V1) make -f $(ROOT_DIR)/xground/android/OpenPilotAndroidGCS/Makefile androidgcs_clean

.PHONY: chromegcs
chromegcs: uavobjects_js chromegcs_build

chromegcs_build: chromegcs_uavos
	$(V1) $(MAKE) -C $(CHROMEGCS_DIR)

chromegcs_uavos: chromegcs_clean
	$(V1) $(CP) -R \
		$(ROOT_DIR)/build/uavobject-synthetics/js $(CHROMEGCS_DIR)/src/app/uavobjects

.PHONY: chromegcs_clean
chromegcs_clean: chromegcs_uavos_clean
	$(V1) $(MAKE) -C $(CHROMEGCS_DIR) clean

chromegcs_uavos_clean:
	$(RM) -rf $(CHROMEGCS_DIR)/src/app/uavobjects


.PHONY: gcs

gcs: 
	$(V1) $(MAKE) -f ground/Makefile gcs

gcs_clean: 
	$(V1) $(MAKE) -f ground/Makefile gcs_clean




##############################
#
# Unit Tests
#
##############################

ALL_UNITTESTS := logfs math lednotification


##############################
#
# Packaging components
#
##############################

# Firmware files to package
PACKAGE_FW_EXCLUDE  := fw_simposix
PACKAGE_FW_TARGETS  := $(filter-out $(PACKAGE_FW_EXCLUDE), $(FW_TARGETS))
PACKAGE_ELF_TARGETS := $(filter     fw_simposix, $(FW_TARGETS))

# Rules to generate GCS resources used to embed firmware binaries into the GCS.
# They are used later by the vehicle setup wizard to update board firmware.
# To open a firmware image use ":/firmware/fw_coptercontrol.opfw"
OPFW_RESOURCE := $(OPGCSSYNTHDIR)/opfw_resource.qrc
OPFW_RESOURCE_PREFIX := ../../
OPFW_FILES := $(foreach fw_targ, $(PACKAGE_FW_TARGETS), $(call toprel, $(BUILD_DIR)/$(fw_targ)/$(fw_targ).opfw))
OPFW_CONTENTS := \
<!DOCTYPE RCC><RCC version="1.0"> \
    <qresource prefix="/firmware"> \
        $(foreach fw_file, $(OPFW_FILES), <file alias="$(notdir $(fw_file))">$(OPFW_RESOURCE_PREFIX)$(fw_file)</file>) \
    </qresource> \
</RCC>

.PHONY: opfw_resource
opfw_resource: $(OPFW_RESOURCE)

$(OPFW_RESOURCE): $(FW_TARGETS) | $(OPGCSSYNTHDIR)
	@$(ECHO) Generating OPFW resource file $(call toprel, $@)
	$(V1) $(ECHO) $(QUOTE)$(OPFW_CONTENTS)$(QUOTE) > $@

# If opfw_resource or all firmware are requested, GCS should depend on the resource
ifneq ($(strip $(filter opfw_resource all all_fw all_flight ,$(MAKECMDGOALS))),)
$(OPENPILOTGCS_MAKEFILE): $(OPFW_RESOURCE)
endif

# Packaging targets: package
#  - builds all firmware, opfw_resource, gcs
#  - copies firmware into a package directory
#  - calls paltform-specific packaging script

# Define some variables
PACKAGE_LBL       := $(shell $(VERSION_INFO) --format=\$${LABEL})
PACKAGE_NAME      := OpenPilot
PACKAGE_SEP       := -
PACKAGE_FULL_NAME := $(PACKAGE_NAME)$(PACKAGE_SEP)$(PACKAGE_LBL)

# Source distribution is never dirty because it uses git archive
DIST_NAME := $(DIST_DIR)/$(subst dirty-,,$(PACKAGE_FULL_NAME)).tar

##############################
#
# Source code formatting
#
##############################

UNCRUSTIFY_TARGETS := flight ground

# $(1) = Uncrustify target (e.g flight or ground)
# $(2) = Target root directory
define UNCRUSTIFY_TEMPLATE

.PHONY: uncrustify_$(1)
uncrustify_$(1):
	@$(ECHO) "Auto-formatting $(1) source code"
	$(V1) UNCRUSTIFY_CONFIG="$(ROOT_DIR)/make/uncrustify/uncrustify.cfg" $(SHELL) make/scripts/uncrustify.sh $(call toprel, $(2))
endef

$(foreach uncrustify_targ, $(UNCRUSTIFY_TARGETS), $(eval $(call UNCRUSTIFY_TEMPLATE,$(uncrustify_targ),$(ROOT_DIR)/$(uncrustify_targ))))

.PHONY: uncrustify_all
uncrustify_all: $(addprefix uncrustify_,$(UNCRUSTIFY_TARGETS))

##############################
#
# Doxygen documentation
#
# Each target should have own Doxyfile.$(target) with build directory build/docs/$(target),
# proper source directory (e.g. $(target)) and appropriate other doxygen options.
#
##############################

DOCS_TARGETS := flight ground uavobjects

# $(1) = Doxygen target (e.g flight or ground)
define DOXYGEN_TEMPLATE

.PHONY: docs_$(1)
docs_$(1): docs_$(1)_clean
	@$(ECHO) "Generating $(1) documentation"
	$(V1) $(MKDIR) -p $(BUILD_DIR)/docs/$(1)
	$(V1) $(DOXYGEN) $(ROOT_DIR)/make/doxygen/Doxyfile.$(1)

.PHONY: docs_$(1)_clean
docs_$(1)_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR)/docs/$(1))"
	$(V1) [ ! -d "$(BUILD_DIR)/docs/$(1)" ] || $(RM) -r "$(BUILD_DIR)/docs/$(1)"

endef

$(foreach docs_targ, $(DOCS_TARGETS), $(eval $(call DOXYGEN_TEMPLATE,$(docs_targ))))

.PHONY: docs_all
docs_all: $(addprefix docs_,$(DOCS_TARGETS))

.PHONY: docs_all_clean
docs_all_clean:
	@$(ECHO) " CLEAN      $(call toprel, $(BUILD_DIR)/docs)"
	$(V1) [ ! -d "$(BUILD_DIR)/docs" ] || $(RM) -rf "$(BUILD_DIR)/docs"

##############################
#
# Build info
#
##############################

.PHONY: build-info
build-info: | $(BUILD_DIR)
	@$(ECHO) " BUILD-INFO $(call toprel, $(BUILD_DIR)/$@.txt)"
	$(V1) $(VERSION_INFO) \
		--uavodir=$(ROOT_DIR)/shared/uavobjectdefinition \
		--template="make/templates/$@.txt" \
		--outfile="$(BUILD_DIR)/$@.txt"

##############################
#
# Source for distribution
#
##############################

DIST_VER_INFO := $(DIST_DIR)/version-info.json

$(DIST_VER_INFO): .git/index | $(DIST_DIR)
	$(V1) $(VERSION_INFO) --jsonpath="$(DIST_DIR)"


$(DIST_NAME).gz: $(DIST_VER_INFO) .git/index | $(DIST_DIR)
	@$(ECHO) " SOURCE FOR DISTRIBUTION $(call toprel, $(DIST_NAME).gz)"
	$(V1) git archive --prefix="$(PACKAGE_NAME)/" -o "$(DIST_NAME)" HEAD
	$(V1) tar --append --file="$(DIST_NAME)" \
		--transform='s,.*version-info.json,$(PACKAGE_NAME)/version-info.json,' \
		$(call toprel, "$(DIST_VER_INFO)")
	$(V1) gzip -f "$(DIST_NAME)"

.PHONY: dist
dist: $(DIST_NAME).gz


##############################
#
# Directories
#
##############################

$(DIRS):
	$(V1) $(MKDIR) -p $@


##############################
#
# Help message, the default Makefile goal
#
##############################

.DEFAULT_GOAL := help

.PHONY: help
help:
	@$(ECHO)
	@$(ECHO) "   This Makefile is known to work on Linux and Mac in a standard shell environment."
	@$(ECHO) "   It also works on Windows by following the instructions given on this wiki page:"
	@$(ECHO) "       http://wiki.openpilot.org/display/Doc/Windows%3A+Building+and+Packaging"
	@$(ECHO)
	@$(ECHO) "   Here is a summary of the available targets:"
	@$(ECHO)
	@$(ECHO) "   [Source tree preparation]"
	@$(ECHO) "     prepare              - Install GIT commit message template"
	@$(ECHO) "   [Tool Installers]"
	@$(ECHO) "     arm_sdk_install      - Install the GNU ARM gcc toolchain"
	@$(ECHO) "     avr_sdk_install      - Install the GNU AVR gcc toolchain"
	@$(ECHO) "     qt_sdk_install       - Install the QT development tools"
	@$(ECHO) "     nsis_install         - Install the NSIS Unicode (Windows only)"
	@$(ECHO) "     sdl_install          - Install the SDL library (Windows only)"
	@$(ECHO) "     mesawin_install      - Install the OpenGL32 DLL (Windows only)"
	@$(ECHO) "     openssl_install      - Install the OpenSSL libraries (Windows only)"
	@$(ECHO) "     uncrustify_install   - Install the Uncrustify source code beautifier"
	@$(ECHO) "     doxygen_install      - Install the Doxygen documentation generator"
	@$(ECHO) "     gtest_install        - Install the GoogleTest framework"
	@$(ECHO) "   These targets are not updated yet and are probably broken:"
	@$(ECHO) "     openocd_install      - Install the OpenOCD JTAG daemon"
	@$(ECHO) "     stm32flash_install   - Install the stm32flash tool for unbricking F1-based boards"
	@$(ECHO) "     dfuutil_install      - Install the dfu-util tool for unbricking F4-based boards"
	@$(ECHO) "   Install all available tools:"
	@$(ECHO) "     all_sdk_install      - Install all of above (platform-dependent)"
	@$(ECHO) "     build_sdk_install    - Install only essential for build tools (platform-dependent)"
	@$(ECHO)
	@$(ECHO) "   Other tool options are:"
	@$(ECHO) "     <tool>_version       - Display <tool> version"
	@$(ECHO) "     <tool>_clean         - Remove installed <tool>"
	@$(ECHO) "     <tool>_distclean     - Remove downloaded <tool> distribution file(s)"
	@$(ECHO)
	@$(ECHO) "   [Big Hammer]"
	@$(ECHO) "     all                  - Generate UAVObjects, build openpilot firmware and gcs"
	@$(ECHO) "     all_flight           - Build all firmware, bootloaders and bootloader updaters"
	@$(ECHO) "     all_fw               - Build only firmware for all boards"
	@$(ECHO) "     all_bl               - Build only bootloaders for all boards"
	@$(ECHO) "     all_bu               - Build only bootloader updaters for all boards"
	@$(ECHO)
	@$(ECHO) "     all_clean            - Remove your build directory ($(BUILD_DIR))"
	@$(ECHO) "     all_flight_clean     - Remove all firmware, bootloaders and bootloader updaters"
	@$(ECHO) "     all_fw_clean         - Remove firmware for all boards"
	@$(ECHO) "     all_bl_clean         - Remove bootloaders for all boards"
	@$(ECHO) "     all_bu_clean         - Remove bootloader updaters for all boards"
	@$(ECHO)
	@$(ECHO) "     all_<board>          - Build all available images for <board>"
	@$(ECHO) "     all_<board>_clean    - Remove all available images for <board>"
	@$(ECHO)
	@$(ECHO) "     all_ut               - Build all unit tests"
	@$(ECHO) "     all_ut_tap           - Run all unit tests and capture all TAP output to files"
	@$(ECHO) "     all_ut_run           - Run all unit tests and dump TAP output to console"
	@$(ECHO)
	@$(ECHO) "   [Firmware]"
	@$(ECHO) "     <board>              - Build firmware for <board>"
	@$(ECHO) "                            Supported boards are ($(ALL_BOARDS))"
	@$(ECHO) "     fw_<board>           - Build firmware for <board>"
	@$(ECHO) "                            Supported boards are ($(FW_BOARDS))"
	@$(ECHO) "     fw_<board>_clean     - Remove firmware for <board>"
	@$(ECHO) "     fw_<board>_program   - Use OpenOCD + JTAG to write firmware to <board>"
	@$(ECHO)
	@$(ECHO) "   [Bootloader]"
	@$(ECHO) "     bl_<board>           - Build bootloader for <board>"
	@$(ECHO) "                            Supported boards are ($(BL_BOARDS))"
	@$(ECHO) "     bl_<board>_clean     - Remove bootloader for <board>"
	@$(ECHO) "     bl_<board>_program   - Use OpenOCD + JTAG to write bootloader to <board>"
	@$(ECHO)
	@$(ECHO) "   [Entire Flash]"
	@$(ECHO) "     ef_<board>           - Build entire flash image for <board>"
	@$(ECHO) "                            Supported boards are ($(EF_BOARDS))"
	@$(ECHO) "     ef_<board>_clean     - Remove entire flash image for <board>"
	@$(ECHO) "     ef_<board>_program   - Use OpenOCD + JTAG to write entire flash image to <board>"
	@$(ECHO)
	@$(ECHO) "   [Bootloader Updater]"
	@$(ECHO) "     bu_<board>           - Build bootloader updater for <board>"
	@$(ECHO) "                            Supported boards are ($(BU_BOARDS))"
	@$(ECHO) "     bu_<board>_clean     - Remove bootloader updater for <board>"
	@$(ECHO)
	@$(ECHO) "   [Unbrick a board]"
	@$(ECHO) "     unbrick_<board>      - Use the STM32's built in boot ROM to write a bootloader to <board>"
	@$(ECHO) "                            Supported boards are ($(BL_BOARDS))"
	@$(ECHO) "   [Unittests]"
	@$(ECHO) "     ut_<test>            - Build unit test <test>"
	@$(ECHO) "     ut_<test>_xml        - Run test and capture XML output into a file"
	@$(ECHO) "     ut_<test>_run        - Run test and dump output to console"
	@$(ECHO)
	@$(ECHO) "   [Simulation]"
	@$(ECHO) "     sim_osx              - Build OpenPilot simulation firmware for OSX"
	@$(ECHO) "     sim_osx_clean        - Delete all build output for the osx simulation"
	@$(ECHO) "     sim_win32            - Build OpenPilot simulation firmware for Windows"
	@$(ECHO) "                            using mingw and msys"
	@$(ECHO) "     sim_win32_clean      - Delete all build output for the win32 simulation"
	@$(ECHO)
	@$(ECHO) "   [GCS]"
	@$(ECHO) "     gcs                  - Build the Ground Control System (GCS) application (debug|release)"
	@$(ECHO) "                            Compile specific directory: MAKE_DIR=<dir>"
	@$(ECHO) "                            Example: make gcs MAKE_DIR=src/plugins/coreplugin"
	@$(ECHO) "     gcs_qmake            - Run qmake for the Ground Control System (GCS) application (debug|release)"
	@$(ECHO) "     gcs_clean            - Remove the Ground Control System (GCS) application (debug|release)"
	@$(ECHO) "                            Supported build configurations: GCS_BUILD_CONF=debug|release (default is $(GCS_BUILD_CONF))"
	@$(ECHO)
	@$(ECHO)
	@$(ECHO) "   [UAVObjects]"
	@$(ECHO) "     uavobjects           - Generate source files from the UAVObject definition XML files"
	@$(ECHO) "     uavobjects_test      - Parse xml-files - check for valid, duplicate ObjId's, ..."
	@$(ECHO) "     uavobjects_<group>   - Generate source files from a subset of the UAVObject definition XML files"
	@$(ECHO) "                            Supported groups are ($(UAVOBJ_TARGETS))"
	@$(ECHO)
	@$(ECHO) "   [Packaging]"
	@$(ECHO) "     package              - Build and package the OpenPilot platform-dependent package (no clean)"
	@$(ECHO) "     opfw_resource        - Generate resources to embed firmware binaries into the GCS"
	@$(ECHO) "     dist                 - Generate source archive for distribution"
	@$(ECHO) "     install              - Install GCS to \"DESTDIR\" with prefix \"prefix\" (Linux only)"
	@$(ECHO)
	@$(ECHO) "   [Code Formatting]"
	@$(ECHO) "     uncrustify_<source>  - Reformat <source> code according to the project's standards"
	@$(ECHO) "                            Supported sources are ($(UNCRUSTIFY_TARGETS))"
	@$(ECHO) "     uncrustify_all       - Reformat all source code"
	@$(ECHO)
	@$(ECHO) "   [Code Documentation]"
	@$(ECHO) "     docs_<source>        - Generate HTML documentation for <source>"
	@$(ECHO) "                            Supported sources are ($(DOCS_TARGETS))"
	@$(ECHO) "     docs_all             - Generate HTML documentation for all"
	@$(ECHO) "     docs_<source>_clean  - Delete generated documentation for <source>"
	@$(ECHO) "     docs_all_clean       - Delete all generated documentation"
	@$(ECHO)
	@$(ECHO) "   Hint: Add V=1 to your command line to see verbose build output."
	@$(ECHO)
	@$(ECHO) "  Notes: All tool distribution files will be downloaded into $(DL_DIR)"
	@$(ECHO) "         All tools will be installed into $(TOOLS_DIR)"
	@$(ECHO) "         All build output will be placed in $(BUILD_DIR)"
	@$(ECHO)
	@$(ECHO) "  Tool download and install directories can be changed using environment variables:"
	@$(ECHO) "         OPENPILOT_DL_DIR        full path to downloads directory [downloads if not set]"
	@$(ECHO) "         OPENPILOT_TOOLS_DIR     full path to installed tools directory [tools if not set]"
	@$(ECHO) "  More info: http://wiki.openpilot.org/display/Doc/OpenPilot+Build+System+Overview"
	@$(ECHO)
